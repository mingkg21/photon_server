package cc.ccplay.booster.common.service;

import cc.ccplay.booster.base.api.common.CommonRedisService;
import cc.ccplay.booster.base.api.common.SmsService;
import cc.ccplay.booster.base.enums.RedisCacheKeyEnum;
import cc.ccplay.booster.base.enums.SmsBizTypeEnum;
import cc.ccplay.booster.base.util.RandomIDUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.cloopen.rest.sdk.CCPRestSDK;
import org.apache.log4j.Logger;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.HashMap;

@org.springframework.stereotype.Service
@Service(interfaceClass = SmsService.class)
public class SmsServiceImpl implements SmsService {
	private final static Logger loger = Logger.getLogger(SmsServiceImpl.class);

	/**
	 * sms config and im config
	 */
	@Value("${yuntongxun.serverIp}")
	protected String yuntongxunServerIp;

	@Value("${yuntongxun.serverPort}")
	protected String yuntongxunServerPort;

	@Value("${yuntongxun.accountSid}")
	protected String yuntongxunAccountSid;

	@Value("${yuntongxun.accountToken}")
	protected String yuntongxunAccountToken;

	@Value("${sms.appId}")
	protected String smsAppId;

	@Value("${sms.validateCode.templateId}")
	protected String smsValidateCodeTemplateId;

	@Value("${sms.validateCode.validMinutes}")
	protected int smsValidateCodeValidMinutes;

	@Value("${sms.validateCode.length}")
	protected int smsValidateCodeLength;

	@Value("${sms.warning.templateId}")
	protected String smsWarningTemplateId;

	@Value("${sms.warning.phones}")
	protected String smsWarningPhones;

	@Value("${sms.recovery.templateId}")
	protected String smsRecoveryTemplateId;

	@Autowired
	private CommonRedisService redisService;

	@Override
	public void sendSms(SmsBizTypeEnum smsBizTypeEnum, String smsTo, String... smsContent) {
		if (SmsBizTypeEnum.validate_code.name().equals(smsBizTypeEnum.name())) {
			String validateCode = RandomIDUtil.getNumber(this.smsValidateCodeLength);
			sendAllSms(this.smsValidateCodeTemplateId, smsTo, validateCode, String.valueOf(this.smsValidateCodeValidMinutes));
			redisService.set(RedisCacheKeyEnum.sms_validatecode.getKey(smsTo), validateCode, this.smsValidateCodeValidMinutes * 60L);
		}else{
			throw new GenericException("短信类型有误");
		}
	}

	@Override
	public boolean verifySmsCode(String smsTo, String inputCode) {
		String validateCode = redisService.get(RedisCacheKeyEnum.sms_validatecode.getKey(smsTo));
		if (!StringUtils.isEmpty(validateCode) && validateCode.equals(inputCode)) {
			return true;
		}
		return false;
	}

	private void sendAllSms(String templateId, String smsTo, String... smsContent) {
		if (StringUtil.isEmpty(smsTo)) {
			throw new GenericException("接收手机号不能为空");
			//return false;
		}
		CCPRestSDK restAPI = new CCPRestSDK();
		restAPI.init(this.yuntongxunServerIp, this.yuntongxunServerPort);// 初始化服务器地址和端口，格式如下，服务器地址不需要写https://
		restAPI.setAccount(this.yuntongxunAccountSid, this.yuntongxunAccountToken);// 初始化主帐号名称和主帐号令牌
		restAPI.setAppId(this.smsAppId);// 初始化应用ID
		String justSent = redisService.get(RedisCacheKeyEnum.sms_interval.getKey(smsTo));
		if (StringUtil.isNotEmpty(justSent)) {// 1分钟之内，同一手机号只能发送一次
			throw new GenericException("请勿频繁操作（1分钟只能发送一次）");
			//return false;
		}
		HashMap<String, Object> result = restAPI.sendTemplateSMS(smsTo, templateId, smsContent);
		loger.debug("sendSms result=" + result);
		redisService.set(RedisCacheKeyEnum.sms_interval.getKey(smsTo), smsTo, 60L);
		if (!"000000".equals(result.get("statusCode"))) {
			// 异常返回输出错误码和错误信息
			loger.error("错误码=" + result.get("statusCode") + " 错误信息= " + result.get("statusMsg"));
			throw new GenericException(result.get("statusMsg").toString());
		}
	}

	@Override
	public void sendWarningNotice(String serverName,String ip,String dateStr,String successPercent) {
		if (StringUtil.isEmpty(smsWarningPhones)) {
			return;
		}
		CCPRestSDK restAPI = new CCPRestSDK();
		restAPI.init(this.yuntongxunServerIp, this.yuntongxunServerPort);// 初始化服务器地址和端口，格式如下，服务器地址不需要写https://
		restAPI.setAccount(this.yuntongxunAccountSid, this.yuntongxunAccountToken);// 初始化主帐号名称和主帐号令牌
		restAPI.setAppId(this.smsAppId);// 初始化应用ID

		serverName = serverName.replaceAll("【","[").replaceAll("】","]");
		HashMap<String, Object> result = restAPI.sendTemplateSMS(smsWarningPhones, this.smsWarningTemplateId, new String[]{serverName,ip,dateStr,successPercent});
		loger.debug("sendSms result=" + result);
		if (!"000000".equals(result.get("statusCode"))) {
			// 异常返回输出错误码和错误信息
			loger.error("错误码=" + result.get("statusCode") + " 错误信息= " + result.get("statusMsg"));
			throw new GenericException("发送号码:("+smsWarningPhones+"),"+result.get("statusMsg").toString());
		}
	}


	@Override
	public void sendRecoveryNotice(String serverName, String ip, String dateStr, String successPercent) {
		if (StringUtil.isEmpty(smsWarningPhones)) {
			return;
		}
		CCPRestSDK restAPI = new CCPRestSDK();
		restAPI.init(this.yuntongxunServerIp, this.yuntongxunServerPort);// 初始化服务器地址和端口，格式如下，服务器地址不需要写https://
		restAPI.setAccount(this.yuntongxunAccountSid, this.yuntongxunAccountToken);// 初始化主帐号名称和主帐号令牌
		restAPI.setAppId(this.smsAppId);// 初始化应用ID

		serverName = serverName.replaceAll("【","[").replaceAll("】","]");
		HashMap<String, Object> result = restAPI.sendTemplateSMS(smsWarningPhones, this.smsRecoveryTemplateId, new String[]{serverName,ip,dateStr,successPercent});
		loger.debug("sendSms result=" + result);
		if (!"000000".equals(result.get("statusCode"))) {
			// 异常返回输出错误码和错误信息
			loger.error("错误码=" + result.get("statusCode") + " 错误信息= " + result.get("statusMsg"));
			throw new GenericException("发送号码:("+smsWarningPhones+"),"+result.get("statusMsg").toString());
		}
	}

	public static void main(String []args){
		CCPRestSDK restAPI = new CCPRestSDK();
		restAPI.init("app.cloopen.com", "8883");// 初始化服务器地址和端口，格式如下，服务器地址不需要写https://
		restAPI.setAccount("8a48b55149d5792d0149e063bc3705e4", "a4cab57a70374530a1c8f979f34dfb7f");// 初始化主帐号名称和主帐号令牌
		restAPI.setAppId("8aaf07086619653001661f33854a04ff");// 初始化应用ID
		//【CC加速器】{1}于{2}时{3}分出现异常，清注意
		Date date = new Date();
		HashMap<String, Object> result = restAPI.sendTemplateSMS("15160088911,18559208029", "412664", new String[]{"【万物云联】韩国2",date.getHours()+"",date.getMinutes()+""});
		loger.debug("sendSms result=" + result);
		if (!"000000".equals(result.get("statusCode"))) {
			// 异常返回输出错误码和错误信息
			loger.error("错误码=" + result.get("statusCode") + " 错误信息= " + result.get("statusMsg"));
			throw new GenericException(result.get("statusMsg").toString());
		}
	}

}
