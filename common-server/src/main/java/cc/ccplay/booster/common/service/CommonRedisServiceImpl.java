package cc.ccplay.booster.common.service;

import cc.ccplay.booster.base.api.common.CommonRedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@Service
public class CommonRedisServiceImpl implements CommonRedisService {

	@Autowired
	private RedisTemplate<String, String> template;

	@Resource(name = "redisTemplate")
    private ValueOperations<String, String> valueOps;

    @Resource(name = "redisTemplate")
	private ListOperations<String, String> listOps;

	@Resource(name = "redisTemplate")
	private SetOperations<String, String> cleanSetOps;

	@Override
	public void set(String key, String value) {
		valueOps.set(key, value);
	}

	@Override
	public void set(String key, String value, Long second) {
		valueOps.set(key, value, second, TimeUnit.SECONDS);
	}

	@Override
	public String get(String key) {
		return valueOps.get(key);
	}

	@Override
	public void push(String key, String value) {
		listOps.rightPush(key, value);
	}

	@Override
	public String pop(String key) {
		return listOps.leftPop(key, 0, TimeUnit.SECONDS);
	}

	@Override
	public void sendMsg(String channel, Object msg) {
		template.convertAndSend(channel, msg);
	}

}
