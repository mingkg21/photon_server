package cc.ccplay.booster.log.service;

import org.easyj.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.dubbo.config.annotation.Service;

import cc.ccplay.booster.base.api.log.UserLoginLogService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.log.UserLoginLog;
import cc.ccplay.booster.log.dao.UserLoginLogDao;

@org.springframework.stereotype.Service
@Service(interfaceClass = UserLoginLogService.class)
public class UserLoginLogServiceImpl implements UserLoginLogService {

	@Autowired
	private UserLoginLogDao userLoginLogDao;

	@Override
	public void insertLoginLog(SystemParam sp, Long userId, Integer packageId) {
		UserLoginLog log = new UserLoginLog();
		log.setClientChannelName(sp.getAppHeaderInfo().getClientChannelName());
		log.setClientPackageName(sp.getAppHeaderInfo().getClientPackageName());
		log.setClientVersionCode(StringUtil.isEmpty(sp.getAppHeaderInfo().getClientVersionCode()) ? null
				: Long.parseLong(sp.getAppHeaderInfo().getClientVersionCode()));
		log.setClientVersionName(sp.getAppHeaderInfo().getClientVersionName());
		log.setUserId(userId);
		log.setPackageId(packageId);
		log.setDeviceNo(sp.getAppHeaderInfo().getDeviceNo());
		log.setIp(sp.getClientIp());
		log.setUserAgent(sp.getAppHeaderInfo().getUserAgent());
		log.setImeiMac(sp.getAppHeaderInfo().getMacCode());
		log.setMacAddress(sp.getAppHeaderInfo().getDeviceNo() + "---" + sp.getAppHeaderInfo().getMacCode());
		userLoginLogDao.save(log);
	}

}
