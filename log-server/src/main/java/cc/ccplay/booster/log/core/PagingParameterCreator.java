package cc.ccplay.booster.log.core;

import cc.ccplay.booster.base.core.PagingParameter;
import cc.ccplay.booster.base.core.SortInfo;
import cc.ccplay.booster.base.core.SystemParam;
import org.easyj.frame.jdbc.support.QueryWebParameter;
import org.easyj.frame.jdbc.support.QueryWebParameterCreator;
import org.easyj.frame.jdbc.support.QueryWebUtils;
import org.easyj.frame.util.SysUtil;

import javax.servlet.ServletRequest;
import java.util.Collections;

public class PagingParameterCreator implements QueryWebParameterCreator{

    /**
     * 生成查询参数
     *
     * @param request
     * @return
     */
    @Override
    public QueryWebParameter generate(ServletRequest request) {
        SystemParam param = (SystemParam) SysUtil.getCustomData();
        String column = null;
        String sortType = null;
        int pageNo = 1;
        int pageSize = 20;
        if(param != null){
            SortInfo sortInfo = param.getSortInfo();
            if (sortInfo != null){
                column = sortInfo.getColumn();
                sortType = sortInfo.getSortType();
            }

           PagingParameter pagingParameter = param.getPagingParameter();
           if(pagingParameter != null){
               pageNo = pagingParameter.getPageNo();
               pageSize = pagingParameter.getPageSize();
           }
        }
        return 	QueryWebUtils.generateQueryWebParameter(Collections.emptyMap(), pageNo, pageSize,column, sortType);
    }
}
