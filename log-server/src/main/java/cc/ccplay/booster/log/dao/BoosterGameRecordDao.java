package cc.ccplay.booster.log.dao;

import cc.ccplay.booster.base.model.log.BoosterGameRecord;
import org.easyj.frame.jdbc.BaseDao;
import org.springframework.stereotype.Repository;

@Repository
public class BoosterGameRecordDao extends BaseDao<BoosterGameRecord> {
    
}
