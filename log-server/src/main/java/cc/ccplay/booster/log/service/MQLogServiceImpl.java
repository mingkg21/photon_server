package cc.ccplay.booster.log.service;

import cc.ccplay.booster.base.enums.MQMessageType;
import cc.ccplay.booster.base.model.log.AppDownloadRecord;
import cc.ccplay.booster.log.dao.AppDownloadRecordDao;
import cc.ccplay.booster.log.dao.AppDownloadCompleteLogDao;
import org.apache.log4j.Logger;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.remoting.common.RemotingHelper;
import org.easyj.frame.util.JsonUtil;
import org.easyj.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MQLogServiceImpl {

    private final static Logger logger = Logger.getLogger(MQLogServiceImpl.class);

    @Autowired
    private AppDownloadCompleteLogDao appDownloadCompleteLogDao;

    @Autowired
    private AppDownloadRecordDao appDownloadRecordDao;


    public void saveLogs(List<MessageExt> messages){
        int size = messages.size();
        for (int i = 0; i < size; i++) {
            MessageExt ext = messages.get(i);
            String tag = ext.getTags();
            if(StringUtil.isEmpty(tag)){
                continue;
            }
            String content = null;
            try {
                content = new String(ext.getBody(), RemotingHelper.DEFAULT_CHARSET);
            }catch (Exception e){
                logger.warn(e.getMessage(),e);
            }
            if(tag.equals(MQMessageType.LOG_APP_DOWNLOAD.getTag())){ //下载量
                AppDownloadRecord appDownloadRecord = JsonUtil.toBean(content, AppDownloadRecord.class);
                appDownloadRecordDao.save(appDownloadRecord);
            }
        }
    }
}
