package cc.ccplay.booster.log.service;

import cc.ccplay.booster.base.api.log.AppDownloadLogService;
import cc.ccplay.booster.log.dao.AppDownloadRecordDao;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.model.log.AppDownloadRecord;
import org.springframework.beans.factory.annotation.Autowired;

@org.springframework.stereotype.Service
@Service(interfaceClass = AppDownloadLogService.class)
public class AppDownloadLogServiceImpl extends BaseLogService implements AppDownloadLogService {

    @Autowired
    private AppDownloadRecordDao appDownloadRecordDao;

    @Override
    public void saveDownloadLog(AppDownloadRecord appDownloadRecord) {
        appDownloadRecordDao.save(appDownloadRecord);
    }
}
