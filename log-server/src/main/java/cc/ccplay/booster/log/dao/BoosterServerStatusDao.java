package cc.ccplay.booster.log.dao;

import cc.ccplay.booster.base.dto.ChartReportDto;
import cc.ccplay.booster.base.model.log.BoosterServerStatus;
import cc.ccplay.booster.base.util.DateTimeUtil;
import org.apache.commons.lang3.time.DateUtils;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Repository
public class BoosterServerStatusDao extends BaseDao<BoosterServerStatus> {

    public List<ChartReportDto> getHourReport(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = null;
        try {
            startDate = sdf.parse(sdf.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date endDate = DateUtils.addDays(startDate,1);

        StringBuffer sql = new StringBuffer();
        sql.append(" select substring(log_time from 1 for 13) as label,max(person_count) as value from ");
        sql.append(" ( ");
        sql.append(" select ");
        sql.append(" to_char(create_time, 'YYYY-MM-DD HH24:MI') as log_time, ");
        sql.append(" sum(person_count) as person_count ");
        sql.append(" from booster_server_status ");
        sql.append(" where create_time > :startDate ");
        sql.append(" and create_time < :endDate ");
        sql.append(" group by to_char(create_time, 'YYYY-MM-DD HH24:MI') ");
        sql.append(" ) a ");
        sql.append(" group by substring(log_time from 1 for 13) ");
        sql.append(" order by substring(log_time from 1 for 13) ");
        return super.query2Model(sql.toString(), MixUtil.newHashMap("startDate",startDate,"endDate",endDate),ChartReportDto.class);
    }



    public List<ChartReportDto> getDayReport(Date startDate,Date endDate){
        StringBuffer sql = new StringBuffer();
        sql.append(" select substring(log_time from 1 for 10) as label,max(person_count) as value from ");
        sql.append(" ( ");
        sql.append(" select ");
        sql.append(" to_char(create_time, 'YYYY-MM-DD HH24:MI') as log_time, ");
        sql.append(" sum(person_count) as person_count ");
        sql.append(" from booster_server_status ");
        sql.append(" where create_time > :startDate ");
        sql.append(" and create_time < :endDate ");
        sql.append(" group by to_char(create_time, 'YYYY-MM-DD HH24:MI') ");
        sql.append(" ) a ");
        sql.append(" group by substring(log_time from 1 for 10) ");
        sql.append(" order by substring(log_time from 1 for 10) ");
        return super.query2Model(sql.toString(), MixUtil.newHashMap("startDate",startDate,"endDate",endDate),ChartReportDto.class);
    }



}
