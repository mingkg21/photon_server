package cc.ccplay.booster.log.dao;

import cc.ccplay.booster.base.dto.log.BoosterSuccessLoginDto;
import cc.ccplay.booster.base.model.log.BoosterLoginLog;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

@Repository
public class BoosterLoginLogDao extends BaseDao<BoosterLoginLog> {

    public BoosterSuccessLoginDto getSuccessCount(long serverId,int size){
        StringBuilder sql =  new StringBuilder(" select count(1) as total_count,sum(status) as success_count from ");
        sql.append(" (select status from booster_login_log where server_id = :serverId order by id desc limit :size) as tmp ");
        return super.query21Model(sql.toString(), MixUtil.newHashMap("serverId",serverId,"size",size),BoosterSuccessLoginDto.class);
    }

}
