package cc.ccplay.booster.log.service;

import cc.ccplay.booster.base.api.log.AppDayCountService;
import cc.ccplay.booster.base.util.DateTimeUtil;
import cc.ccplay.booster.log.dao.AppDayCountDao;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.core.SystemParam;
import org.apache.commons.lang3.time.DateUtils;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Calendar;
import java.util.Date;

@Service(interfaceClass = AppDayCountService.class)
@org.springframework.stereotype.Service
public class AppDayCountServiceImpl extends BaseLogService implements AppDayCountService {

    @Autowired
    private AppDayCountDao appDayCountDao;


    private static Date getStartTime() {
        Calendar todayStart = Calendar.getInstance();
        todayStart.set(Calendar.HOUR_OF_DAY, 0);
        todayStart.set(Calendar.MINUTE, 0);
        todayStart.set(Calendar.SECOND, 0);
        todayStart.set(Calendar.MILLISECOND, 0);
        return todayStart.getTime();
    }

    @Override
    public void countToday() {
        Date startDate = getStartTime();
        System.out.println(DateTimeUtil.formatDateTime(startDate));
        Date endDate = DateUtils.addDays(startDate,1);
        appDayCountDao.deleteByDate(startDate);
        appDayCountDao.countDay(startDate,endDate);
    }

    @Override
    public void countPreDay() {
        Date now = new Date();
        Calendar c = Calendar.getInstance();
        c.setLenient(false);
        c.setTime(now);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        Date endDate = c.getTime();
        Date startDate = DateUtils.addDays(endDate,-1);
        appDayCountDao.deleteByDate(startDate);
        appDayCountDao.countDay(startDate,endDate);
    }

    @Override
    public Page getPage(SystemParam param,Date startDate, Date endDate, Long appId) {
        return appDayCountDao.getPage(startDate,endDate,appId);
    }

    @Override
    public Page getTotalPage(SystemParam param, Date startDate, Date endDate) {
        return appDayCountDao.getTotalPage(startDate,endDate);
    }
}
