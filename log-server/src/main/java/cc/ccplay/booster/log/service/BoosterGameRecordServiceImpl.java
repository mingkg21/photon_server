package cc.ccplay.booster.log.service;

import cc.ccplay.booster.base.api.log.BoosterGameRecordService;
import cc.ccplay.booster.base.model.log.BoosterGameRecord;
import cc.ccplay.booster.log.dao.BoosterGameRecordDao;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service(interfaceClass = BoosterGameRecordService.class)
@org.springframework.stereotype.Service
public class BoosterGameRecordServiceImpl extends BaseLogService implements BoosterGameRecordService{

    @Autowired
    private BoosterGameRecordDao boosterGameRecordDao;

    @Override
    public void save(BoosterGameRecord record) {
        boosterGameRecordDao.save(record);
    }

}
