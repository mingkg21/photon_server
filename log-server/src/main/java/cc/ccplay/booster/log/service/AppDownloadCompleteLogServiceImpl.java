package cc.ccplay.booster.log.service;

import cc.ccplay.booster.log.dao.AppDownloadCompleteLogDao;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.api.log.AppDownloadCompleteLogService;
import cc.ccplay.booster.base.model.log.AppDownloadCompleteRecord;
import org.springframework.beans.factory.annotation.Autowired;


@org.springframework.stereotype.Service
@Service(interfaceClass = AppDownloadCompleteLogService.class)
public class AppDownloadCompleteLogServiceImpl extends BaseLogService implements AppDownloadCompleteLogService{

    @Autowired
    private AppDownloadCompleteLogDao appDownloadCompleteLogDao;

    @Override
    public void save(AppDownloadCompleteRecord record) {
        appDownloadCompleteLogDao.save(record);
    }
}
