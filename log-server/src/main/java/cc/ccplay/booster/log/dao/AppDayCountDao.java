package cc.ccplay.booster.log.dao;

import cc.ccplay.booster.base.model.log.AppDayCount;
import cc.ccplay.booster.base.util.DateTimeUtil;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.jdbc.support.Aggregation;
import org.easyj.frame.jdbc.support.AggregationFunction;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public class AppDayCountDao extends BaseDao<AppDayCount> {

    public Page getPage(Date startDate, Date endDate, Long appId){
        StringBuffer sql = new StringBuffer(" select *, ");
        sql.append(" ( case when download_count = 0 then 1 else round(download_complete_count::numeric/download_count::numeric,4) end  ) as complete_percent ");
        sql.append(" from app_day_count where 1 = 1  ");


        Map paramMap = MixUtil.newHashMap();
        if(startDate != null){
            sql.append(" and count_day >= :startDate ");
            paramMap.put("startDate",startDate);
        }

        if(endDate != null){
            sql.append(" and count_day <= :endDate ");
            paramMap.put("endDate",endDate);
        }

        if(appId != null){
            sql.append(" and app_id = :appId ");
            paramMap.put("appId",appId);
        }
        sql.append(" order by count_day desc ");
        List<Aggregation> aggs = new ArrayList<>();
        aggs.add(new Aggregation(AggregationFunction.SUM,"download_count","downloadCount"));
        aggs.add(new Aggregation(AggregationFunction.SUM,"download_complete_count","downloadCompleteCount"));
        return super.page2CamelMap(sql.toString(),paramMap,aggs);
    }


    public Page getTotalPage(Date startDate,Date endDate){
        Map paramMap = MixUtil.newHashMap();
        StringBuffer sql = new StringBuffer(" select count_day, ");
        sql.append(" sum(download_count) as download_count, ");
        sql.append(" sum(download_complete_count) as download_complete_count, ");
        sql.append("  ( case when sum(download_count) = 0 then 1 else round(sum(download_complete_count)::numeric/sum(download_count),4)::numeric end ) as complete_percent ");
        sql.append(" from app_day_count ");
        sql.append(" where 1 = 1 ");
        if(startDate != null){
            sql.append(" and count_day >= :startDate ");
            paramMap.put("startDate",startDate);
        }
        if(endDate != null){
            sql.append(" and count_day <= :endDate ");
            paramMap.put("endDate",endDate);
        }
        sql.append(" GROUP BY count_day ");
        sql.append(" order by count_day desc ");
        return super.page2CamelMap(sql.toString(),paramMap);
    }



    public void deleteByDate(Date date){
        String sql = " delete from app_day_count where count_day = :date ";
        super.update(sql,MixUtil.newHashMap("date",date));
    }


    public void countDay(Date startDate,Date endDate){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        StringBuffer sql = new StringBuffer();
        sql.append("insert into app_day_count(count_day,app_id,download_count,download_complete_count,update_time,view_count) ");
        sql.append(" select '"+sdf.format(startDate)+"', ");
        sql.append(" case when a.app_id is not null ");
        sql.append(" then a.app_id ");
        sql.append(" else b.app_id end, ");
        sql.append(" COALESCE(b.download_count,0), ");
        sql.append(" COALESCE(a.complete_count,0), ");
        sql.append("'"+ DateTimeUtil.formatDateTime(new Date())+"',");
        sql.append("0");
        sql.append(" from ( ");
        sql.append(" select count(1) as complete_count,app_id ");
        sql.append(" from app_download_complete_record ");
        sql.append(" where app_id is not null ");
        sql.append(" and create_time > :startDate ");
        sql.append(" and create_time < :endDate ");
        sql.append(" group by app_id) a ");
        sql.append(" FULL  JOIN ");
        sql.append(" (select count(1) as download_count,app_id ");
        sql.append(" from app_download_record ");
        sql.append(" where app_id is not null ");
        sql.append(" and  create_time > :startDate ");
        sql.append(" and create_time < :endDate ");
        sql.append("  group by app_id) b ");
        sql.append(" on a.app_id = b.app_id ");
        sql.append(" where a.app_id is not null ");
        super.update(sql.toString(), MixUtil.newHashMap("startDate",startDate,"endDate",endDate));
    }
}
