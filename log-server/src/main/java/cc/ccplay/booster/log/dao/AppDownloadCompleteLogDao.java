package cc.ccplay.booster.log.dao;

import cc.ccplay.booster.base.model.log.AppDownloadCompleteRecord;
import org.easyj.frame.jdbc.BaseDao;
import org.springframework.stereotype.Repository;

@Repository
public class AppDownloadCompleteLogDao extends BaseDao<AppDownloadCompleteRecord> {

}
