package cc.ccplay.booster.log.service;

import cc.ccplay.booster.base.api.log.BoosterLoginLogService;
import cc.ccplay.booster.base.dto.log.BoosterSuccessLoginDto;
import cc.ccplay.booster.base.model.log.BoosterLoginLog;
import cc.ccplay.booster.log.dao.BoosterLoginLogDao;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service(interfaceClass = BoosterLoginLogService.class)
@org.springframework.stereotype.Service
public class BoosterLoginLogServiceImpl extends BaseLogService implements BoosterLoginLogService {

    @Autowired
    private BoosterLoginLogDao boosterLoginLogDao;

    @Override
    public void save(BoosterLoginLog loginLog) {
        boosterLoginLogDao.save(loginLog);
    }

    @Override
    public BoosterSuccessLoginDto getSuccessCount(long serverId,int size) {
        BoosterSuccessLoginDto dto = boosterLoginLogDao.getSuccessCount(serverId,size);
        dto.setServerId(serverId);
        return dto;
    }

}
