package cc.ccplay.booster.log.dao;

import cc.ccplay.booster.base.model.log.AppDownloadRecord;
import org.easyj.frame.jdbc.BaseDao;
import org.springframework.stereotype.Repository;

@Repository
public class AppDownloadRecordDao extends BaseDao<AppDownloadRecord> {

}
