package cc.ccplay.booster.log.service;

import cc.ccplay.booster.base.api.log.BoosterServerStatusService;
import cc.ccplay.booster.base.dto.ChartReportDto;
import cc.ccplay.booster.base.model.log.BoosterServerStatus;
import cc.ccplay.booster.log.dao.BoosterServerStatusDao;
import org.apache.commons.lang3.time.DateUtils;
import org.easyj.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@com.alibaba.dubbo.config.annotation.Service(interfaceClass = BoosterServerStatusService.class)
public class BoosterServerStatusServiceImpl extends BaseLogService implements BoosterServerStatusService{

    @Autowired
    private BoosterServerStatusDao boosterServerStatusDao;

    /**
     * 记录服务器状态
     */
    @Override
    public void batchSave(List<BoosterServerStatus> list){
        boosterServerStatusDao.batchInsert(list);
    }


    @Override
    public ChartReportDto[] getHourReport(Date date) {
        List<ChartReportDto> list = boosterServerStatusDao.getHourReport(date);
        ChartReportDto[] result = new ChartReportDto[24];
        int size = list.size();

        for (int i = 0; i < size; i++) {
            ChartReportDto dto = list.get(i);
            String label = dto.getLabel();
            String hour = label.substring(11);
            dto.setLabel(hour+":00");
            if(hour.startsWith("0")){
                result[StringUtil.toInt(hour.substring(1))] = dto;
            }else{
                result[StringUtil.toInt(hour)] = dto;
            }
        }

        if(size != 24){
            for (int i = 0; i < 24; i++) {
                if(result[i] == null){
                    ChartReportDto dto = new ChartReportDto();
                    if(i < 10){
                        dto.setLabel("0"+i+":00");
                    }else{
                        dto.setLabel(i+":00");
                    }
                    dto.setValue(0L);
                    result[i] = dto;
                }
            }
        }
        return result;
    }

    /**
     * 获取多天的在线人数报表  (一天一条)
     *
     * @param startDate
     * @param endDate
     * @return
     */
    @Override
    public List<ChartReportDto> getDayReport(Date startDate, Date endDate) {
        return boosterServerStatusDao.getDayReport(startDate,endDate);
    }

}
