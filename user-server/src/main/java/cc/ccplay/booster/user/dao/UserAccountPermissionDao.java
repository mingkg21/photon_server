package cc.ccplay.booster.user.dao;

import cc.ccplay.booster.base.model.user.UserAccountPermission;
import org.easyj.frame.jdbc.BaseDao;
import org.springframework.stereotype.Repository;

@Repository
public class UserAccountPermissionDao extends BaseDao<UserAccountPermission> {

}
