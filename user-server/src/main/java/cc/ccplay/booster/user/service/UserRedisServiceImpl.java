package cc.ccplay.booster.user.service;

import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.api.user.UserRedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.ValueOperations;


import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@org.springframework.stereotype.Service
@Service(interfaceClass = UserRedisService.class)
public class UserRedisServiceImpl extends BaseUserService implements UserRedisService {

    @Autowired
    private RedisTemplate<String, String> template;

    @Resource(name = "redisTemplate")
    private ValueOperations<String, String> valueOps;

    @Resource(name = "redisTemplate")
    private ListOperations<String, String> listOps;

    @Resource(name = "redisTemplate")
    private SetOperations<String, String> cleanSetOps;

    @Override
    public void set(String key, String value) {
        valueOps.set(key, value);
    }

    @Override
    public void set(String key, String value, Long second) {
        valueOps.set(key, value, second, TimeUnit.SECONDS);
    }

    @Override
    public String get(String key) {
        return valueOps.get(key);
    }

    @Override
    public void push(String key, String value) {
        listOps.rightPush(key, value);
    }

    @Override
    public String pop(String key) {
        return listOps.leftPop(key, 0, TimeUnit.SECONDS);
    }


    @Override
    public void delete(String key) {
        template.delete(key);
    }

    @Override
    public boolean hasKey(String key) {
        return template.hasKey(key);
    }
}
