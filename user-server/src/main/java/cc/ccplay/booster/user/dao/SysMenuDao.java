package cc.ccplay.booster.user.dao;

import cc.ccplay.booster.base.enums.DeleteFlag;
import cc.ccplay.booster.base.model.user.SysMenu;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SysMenuDao extends BaseDao<SysMenu> {

    public Page getPage(long parentId){
        String sql = "SELECT * FROM sys_menu "
                + " WHERE parent_id = :parentId "
                + " and delete_flag = "+ DeleteFlag.NO_DELETE
                + " ORDER BY sort " ;
        return super.page2CamelMap(sql, MixUtil.newHashMap("parentId",parentId));
    }

    public List<SysMenu> getAllMenu(){
        String sql = "select * from sys_menu where delete_flag = "+ DeleteFlag.NO_DELETE+" and status <> " + SysMenu.STATUS_DISABLED ;
        return super.query2Model(sql);
    }

    public List<SysMenu> getMenuList(long roleId){
        String sql = " select b.* from sys_role_menu a " +
                " left join sys_menu b on(a.menu_id = b.id) " +
                " where b.delete_flag = "+ DeleteFlag.NO_DELETE +
                " and status <> " + SysMenu.STATUS_DISABLED +
                " and a.role_id = :roleId ";
        return super.query2Model(sql,MixUtil.newHashMap("roleId",roleId));
    }
}
