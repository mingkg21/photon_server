package cc.ccplay.booster.user.dao;

import cc.ccplay.booster.base.enums.DeleteFlag;
import cc.ccplay.booster.base.model.user.SysUser;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

@Repository
public class SysUserDao extends BaseDao<SysUser> {

    public Page getList(){
        String sql = "select a.*,b.name as role_name from sys_user a " +
                " left join sys_role b on(a.role_id = b.id) "+
                " where a.delete_flag = "+ DeleteFlag.NO_DELETE
                +" order by id desc ";
        return super.page2CamelMap(sql);
    }

    public SysUser getUserByAccount(String account){
        String sql = "select * from sys_user where account = :account and delete_flag = "+ DeleteFlag.NO_DELETE + " limit 1 ";
        return super.query21Model(sql, MixUtil.newHashMap("account",account));
    }

    public Page<SysUser> getPage(){
        String sql = "select * from sys_user where delete_flag = "+ DeleteFlag.NO_DELETE;
        return super.paged2Obj(sql);
    }

    public int getCountByRoleId(long roleId){
        String sql = "select count(1) from sys_user where delete_flag = "+ DeleteFlag.NO_DELETE+ " role_id = :roleId ";
        return queryForInt(sql,MixUtil.newHashMap("roleId",roleId));
    }
}
