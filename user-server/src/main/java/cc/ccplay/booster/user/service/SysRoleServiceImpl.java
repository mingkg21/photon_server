package cc.ccplay.booster.user.service;

import cc.ccplay.booster.user.dao.SysRoleDao;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.api.user.SysRoleService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.user.SysRole;
import cc.ccplay.booster.base.model.user.SysRoleMenu;
import cc.ccplay.booster.user.dao.SysRoleMenuDao;
import cc.ccplay.booster.user.dao.SysUserDao;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@Service(interfaceClass=SysRoleService.class)
@org.springframework.stereotype.Service
public class SysRoleServiceImpl implements SysRoleService{

    @Autowired
    private SysRoleDao sysRoleDao;

    @Autowired
    private SysUserDao sysUserDao;

    @Autowired
    private SysRoleMenuDao sysRoleMenuDao;

    @Override
    public SysRole getRole(SystemParam systemParam, long roleId) {
        return sysRoleDao.get(roleId);
    }

    @Override
    public List<SysRole> getAll(SystemParam systemParam) {
        return sysRoleDao.getAll();
    }

    @Override
    public int logicDelete(SystemParam systemParam, long id) {
        int size = sysUserDao.getCountByRoleId(id);
        if(size > 0){
            throw new GenericException("删除失败，原因：当前角色已被"+size+"个用户引用");
        }
        sysRoleMenuDao.deleteByRoleId(id);
        return sysRoleDao.logicDelete(id);
    }

    @Override
    public SysRole saveOrUpdate(SystemParam systemParam, SysRole role) {
        return sysRoleDao.saveOrUpdate(role);
    }

    @Override
    public Page getPage(SystemParam systemParam) {
        return sysRoleDao.getPage();
    }

    @Override
    public void savePermission(SystemParam systemParam, String[] menuIds, long roleId) {
        //删除所有
        sysRoleMenuDao.deleteByRoleId(roleId);
        if(menuIds == null || menuIds.length == 0){
            return;
        }
        List<SysRoleMenu> rels = new ArrayList<SysRoleMenu>();
        for (int i = 0; i < menuIds.length; i++) {
            Long menuId = StringUtil.toLong(menuIds[i]);
            if(menuId.longValue() == 0L){
                continue;
            }
            SysRoleMenu sysRoleMenu = new SysRoleMenu();
            sysRoleMenu.setId(roleId,menuId);
            rels.add(sysRoleMenu);
        }
        sysRoleMenuDao.batchInsert(rels);
    }
}
