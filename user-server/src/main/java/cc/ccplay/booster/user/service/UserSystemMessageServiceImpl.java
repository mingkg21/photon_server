package cc.ccplay.booster.user.service;

import cc.ccplay.booster.user.dao.UserPushRefDao;
import cc.ccplay.booster.user.dao.UserSystemMessageDao;
import cn.jpush.api.JPushClient;
import cn.jpush.api.common.resp.APIConnectionException;
import cn.jpush.api.common.resp.APIRequestException;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.Message;
import cn.jpush.api.push.model.Options;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.notification.AndroidNotification;
import cn.jpush.api.push.model.notification.IosNotification;
import cn.jpush.api.push.model.notification.Notification;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.api.user.UserSystemMessageService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.user.UserPushRef;
import cc.ccplay.booster.base.model.user.UserSystemMessage;
import org.apache.log4j.Logger;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;


@org.springframework.stereotype.Service
@Service(interfaceClass = UserSystemMessageService.class)
public class UserSystemMessageServiceImpl extends BaseUserService implements UserSystemMessageService {

    private Logger logger = Logger.getLogger(UserSystemMessageServiceImpl.class);

    @Autowired
    private UserSystemMessageDao userSystemMessageDao;

    @Autowired
    private UserPushRefDao userPushRefDao;

    @Override
    public void pushNotification2One(UserSystemMessage message) {
        Long userId = message.getUserId();
        if(userId == null){
            throw new GenericException("发送对象不能为空");
        }
        List<UserPushRef> refs = userPushRefDao.selectPushDeviceIdsByUserId(message.getUserId());
        int count = refs.size();
        if(count == 0){
            return;
        }
        String [] registrationIds = new String[count];
        for (int i = 0; i < count; i++) {
            UserPushRef ref = refs.get(i);
            registrationIds[i] = ref.getPushDeviceId();
        }

        Date now = new Date();
        message.setPushStatus(UserSystemMessage.PUSH_STATUS_SENT);
        message.setPushTime(now);
        message.setCreateTime(now);
        userSystemMessageDao.save(message);

        String msgTitle = message.getMsgTitle();
        String msgContent = message.getMsgContent();
        String pushMsg = message.getPushMsg();
        PushPayload payload = PushPayload
                .newBuilder()
                .setPlatform(Platform.android())
                .setAudience(Audience.registrationId(registrationIds))
                .setOptions(Options.newBuilder().setApnsProduction(true).build())
                .setNotification(
                        Notification.newBuilder().addPlatformNotification(IosNotification.newBuilder().setAlert(msgContent).addExtra("pushMsg", pushMsg).build())
                                .addPlatformNotification(AndroidNotification.newBuilder().setTitle(msgTitle).setAlert(msgContent).addExtra("pushMsg", pushMsg).build()).build()).build();
        payload.resetOptionsTimeToLive(864000);
        pushMsg(payload);
    }


    /**
     * 通知
     *
     * @param message
     */
    @Override
    public void pushMsg2One(UserSystemMessage message) {
        Long userId = message.getUserId();
        if(userId == null){
            throw new GenericException("发送对象不能为空");
        }
        List<UserPushRef> refs = userPushRefDao.selectPushDeviceIdsByUserId(message.getUserId());
        int count = refs.size();
        if(count == 0){
            return;
        }
        String [] registrationIds = new String[count];
        for (int i = 0; i < count; i++) {
            UserPushRef ref = refs.get(i);
            registrationIds[i] = ref.getPushDeviceId();
        }

        Date now = new Date();
        message.setPushStatus(UserSystemMessage.PUSH_STATUS_SENT);
        message.setPushTime(now);
        message.setCreateTime(now);
        userSystemMessageDao.save(message);
        //String msgTitle = message.getMsgTitle();
        //String msgContent = message.getMsgContent();
        String pushMsg = message.getPushMsg();
        PushPayload payload = PushPayload
                .newBuilder()
                .setPlatform(Platform.android())
                .setAudience(Audience.registrationId(registrationIds))
                .setOptions(Options.newBuilder().setApnsProduction(true).build())
                .setMessage(Message.content(pushMsg)).build();
        payload.resetOptionsTimeToLive(864000);
        pushMsg(payload);
    }


    @Override
    public void pushNotification2All(UserSystemMessage message) {
        String msgTitle = message.getMsgTitle();
        String msgContent = message.getMsgContent();
        String pushMsg = message.getPushMsg();

        Date now = new Date();
        message.setPushStatus(UserSystemMessage.PUSH_STATUS_SENT);
        message.setPushTime(now);
        message.setCreateTime(now);
        message.setUserId(null);
        userSystemMessageDao.save(message);

        PushPayload payload = PushPayload
                .newBuilder()
                .setPlatform(Platform.android())
                .setAudience(Audience.all())
                .setOptions(Options.newBuilder().setApnsProduction(true).build())
                .setNotification(
                        Notification.newBuilder().addPlatformNotification(IosNotification.newBuilder().setAlert(msgContent).addExtra("pushMsg", pushMsg).build())
                                .addPlatformNotification(AndroidNotification.newBuilder().setTitle(msgTitle).setAlert(msgContent).addExtra("pushMsg", pushMsg).build()).build()).build();
        payload.resetOptionsTimeToLive(864000);
        this.pushMsg(payload);
    }


    @Override
    public void pushMsg2All(UserSystemMessage message) {
        String pushMsg = message.getPushMsg();
        Date now = new Date();
        message.setPushStatus(UserSystemMessage.PUSH_STATUS_SENT);
        message.setPushTime(now);
        message.setCreateTime(now);
        message.setUserId(null);
        userSystemMessageDao.save(message);

        PushPayload payload = PushPayload
                .newBuilder()
                .setPlatform(Platform.android())
                .setAudience(Audience.all())
                .setOptions(Options.newBuilder().setApnsProduction(true).build())
                .setMessage(Message.content(pushMsg)).build();
        payload.resetOptionsTimeToLive(864000);
        this.pushMsg(payload);
    }

    private void pushMsg(PushPayload payload) {
        JPushClient jpushClient = new JPushClient(this.jpushMasterSecret, this.jpushAppKey);
        try {
            PushResult result = jpushClient.sendPush(payload);
            logger.info("Got result - " + result);
        } catch (APIConnectionException e) {
            // Connection error, should retry later
            logger.error("Connection error, should retry later", e);
        } catch (APIRequestException e) {
            // Should review the error, and fix the request
            logger.error("Should review the error, and fix the request", e);
            logger.info("HTTP Status: " + e.getStatus());
            logger.info("Error Code: " + e.getErrorCode());
            logger.info("Error Message: " + e.getErrorMessage());
        }
    }

    @Override
    public Page<UserSystemMessage> getPageByUserId(SystemParam systemParam,long userId) {
        return userSystemMessageDao.getPageByUserId(userId);
    }
}
