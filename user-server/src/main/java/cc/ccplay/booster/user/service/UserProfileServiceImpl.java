package cc.ccplay.booster.user.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.api.common.SmsService;
import cc.ccplay.booster.base.dto.user.SimpleUserDto;
import cc.ccplay.booster.base.reader.dto.UserInfo;
import cc.ccplay.booster.base.util.RegexUtil;
import cc.ccplay.booster.user.dao.UserAccountDao;
import cc.ccplay.booster.user.dao.UserFollowDao;
import cc.ccplay.booster.user.dao.UserProfileDao;
import cc.ccplay.booster.base.api.user.UserProfileService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.user.UserFollowDto;
import cc.ccplay.booster.base.model.user.UserAccount;
import cc.ccplay.booster.base.model.user.UserFollow;
import cc.ccplay.booster.base.model.user.UserProfile;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;

@org.springframework.stereotype.Service
@Service(interfaceClass = UserProfileService.class)
public class UserProfileServiceImpl extends BaseUserService implements UserProfileService {

	@Autowired
	private UserProfileDao userProfileDao;

	@Autowired
	private UserAccountDao userAccountDao;

	@Autowired
	private UserFollowDao userFollowDao;

	@Reference
	private SmsService smsService;

	@Override
	public SimpleUserDto updateUserProfile(SystemParam SP, String headIcon, Integer sex, String nickName, String area, String signature) {
		if (StringUtil.isEmpty(headIcon) && sex == null && StringUtil.isEmpty(nickName) && StringUtil.isEmpty(area)) {
			throw new GenericException("无任何修改！");
		}
		long userId = SP.getUserId();
		int count = 0;
		UserProfile dto = new UserProfile();
		dto.setUserId(userId);
		dto.setHeadIcon(headIcon);
		dto.setSex(sex);
		dto.setSignature(signature);
		dto.setArea(area);
		count = userProfileDao.updateNotNull(dto);

		if (!StringUtil.isEmpty(nickName)) {
			UserAccount ua = new UserAccount();
			ua.setId(SP.getUserId());
			ua.setNickName(nickName);
			count = userAccountDao.updateNotNull(ua);
		}

		if(count == 0){
			throw new GenericException("更新用户信息失败");
		}
		return getSimpleuserDto(userId);
	}

	@Override
	public void follow(SystemParam param, long beFollowUserId) {
		if(userAccountDao.get(beFollowUserId) == null){
			throw new GenericException("被关注的用户不存在");
		}
		Long fansUserId = param.getUserId();
		UserFollow.Id id = new UserFollow.Id(fansUserId,beFollowUserId);

		if(StringUtil.equals(beFollowUserId,fansUserId)){
			throw new GenericException("不能关注自己");
		}

		if(userFollowDao.get(id) != null){
			throw new GenericException("已经关注过该用户");
		}
		UserFollow userFollow = new UserFollow();
		userFollow.setId(id);
		userFollowDao.save(userFollow);

		this.userProfileDao.addFansCount(beFollowUserId,1);
		this.userProfileDao.addFollowCount(fansUserId,1);
	}

	@Override
	public void unfollow(SystemParam param, long beFollowUserId) {
		Long fansUserId = param.getUserId();
		UserFollow.Id id = new UserFollow.Id(fansUserId,beFollowUserId);
		if(userFollowDao.delete(id) > 0) {
			this.userProfileDao.addFansCount(beFollowUserId, -1);
			this.userProfileDao.addFollowCount(fansUserId, -1);
		}
	}

	@Override
	public Page<UserFollowDto> getMyFollow(SystemParam param) {
		return userFollowDao.getFollowPage(param.getUserId());
	}

	@Override
	public Page<UserFollowDto> getMyFans(SystemParam param) {
		return userFollowDao.getFansPage(param.getUserId());
	}

	@Override
	public SimpleUserDto getUserInfo(SystemParam param) {
		Long userId = param.getUserId();
		return getSimpleuserDto(userId);
	}

	private SimpleUserDto getSimpleuserDto(Long userId){
		UserInfo userInfo = new UserInfo();
		UserAccount userAccount = userAccountDao.get(userId);
		if(userAccount == null){
			throw new GenericException("用户信息不存在");
		}
		userInfo.setUserAccount(userAccount);
		UserProfile userProfile = userProfileDao.get(userId);
		if(userProfile == null){
			throw new GenericException("用户信息不存在");
		}
		userInfo.setUserProfile(userProfile);

		SimpleUserDto dto = new SimpleUserDto(userInfo);
		dto.setPhone(userAccount.getPhone());
		return dto;
	}

	@Override
	public void bindPhone(SystemParam param,String phone, String validateCode) {
		UserAccount account = userAccountDao.get(param.getUserId());
		if(StringUtil.isNotEmpty(account.getPhone())){
			throw new GenericException("该账号已经绑定了手机");
		}

		if (!RegexUtil.isMobile(phone)) {
			throw new GenericException("手机号不正确！");
		}

		if (!smsService.verifySmsCode(phone, validateCode)) {
			throw new GenericException("验证码错误!");
		}

		UserAccount newAccount = new UserAccount();
		newAccount.setPhone(phone);
		newAccount.setId(param.getUserId());

		if(userAccountDao.isNotUnique(newAccount,"phone")){
			throw new GenericException("手机号已被其他账号绑定");
		}

		userAccountDao.updateNotNull(newAccount);
	}
}
