package cc.ccplay.booster.user.service;

import cc.ccplay.booster.base.api.user.UserFlowService;
import cc.ccplay.booster.base.dto.content.booster.AddUserFlowDto;
import cc.ccplay.booster.base.enums.AddUserFlowType;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.user.dao.UserAccountDao;
import cc.ccplay.booster.user.dao.UserProfileDao;
import cc.ccplay.booster.user.dao.UserRegisterLoginDao;
import cc.ccplay.booster.base.api.common.SmsService;
import cc.ccplay.booster.base.api.user.LoginService;
import cc.ccplay.booster.base.api.user.RegisterService;
import cc.ccplay.booster.base.constants.Constant;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.user.LoginResultDto;
import cc.ccplay.booster.base.enums.SmsBizTypeEnum;
import cc.ccplay.booster.base.enums.UserStatusEnum;
import cc.ccplay.booster.base.model.user.UserAccount;
import cc.ccplay.booster.base.model.user.UserProfile;
import cc.ccplay.booster.base.model.user.UserRegisterLogin;
import cc.ccplay.booster.base.util.Md5Util;
import cc.ccplay.booster.base.util.RandomIDUtil;
import cc.ccplay.booster.base.util.RegexUtil;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@org.springframework.stereotype.Service
@Service(interfaceClass = RegisterService.class)
public class RegisterServiceImpl extends BaseUserService implements RegisterService {

	@Reference
	private SmsService smsService;

	@Autowired
	private UserAccountDao userAccountDao;

	@Autowired
	private LoginService loginService;

	@Autowired
	private UserRegisterLoginDao userRegisterLoginDao;

	@Autowired
	private UserProfileDao userProfileDao;

	@Autowired
	private UserFlowService userFlowService;

	@Override
	public void sendRegisterSms(SystemParam sp, String phone) {
		if (StringUtil.isEmpty(phone)) {
			throw new GenericException("手机号不能为空！");
		}
		// todo 设备、ip及图形验证码防刷，待实现！
		smsService.sendSms(SmsBizTypeEnum.validate_code, phone);
	}

	@Override
	public LoginResultDto registerByPhone(SystemParam sp, String phone, String password, String validateCode) {
		if (StringUtil.isEmpty(phone) || StringUtil.isEmpty(password) || StringUtil.isEmpty(validateCode)) {
			throw new GenericException("缺少参数!");
		}
		if (!RegexUtil.isMobile(phone)) {
			throw new GenericException("手机号不正确！");
		}
		if (!(password.toString().length() >= 6 && password.toString().length() <= 16)) {
			throw new GenericException("密码长度6~16位~");
		}
		if (!smsService.verifySmsCode(phone, validateCode)) {
			throw new GenericException("验证码错误!");
		}
		int count = userAccountDao.countUserByPhone(phone);
		if (count > 0) {
			throw new GenericException("手机号已注册，请前往登录~");
		}

		Lock lock = new ReentrantLock();
		Long userId = null;
		try {
			lock.lock();
			userId = register(sp, "phone", phone, password);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			lock.unlock();
		}

		if (userId != null) {
			LoginResultDto dto = loginService.afterLogin(sp, userId, phone);
			return dto;
		} else {
			throw new GenericException("失败!");
		}
	}

	@Override
	public boolean resetPassword(SystemParam sp, String phone, String password, String validateCode) {
		if (StringUtil.isEmpty(phone) || StringUtil.isEmpty(password) || StringUtil.isEmpty(validateCode)) {
			throw new GenericException("缺少参数!");
		}
		if (!RegexUtil.isMobile(phone)) {
			throw new GenericException("手机号不正确！");
		}
		if (!(password.toString().length() >= 6 && password.toString().length() <= 16)) {
			throw new GenericException("密码长度6~16位~");
		}
		if (!smsService.verifySmsCode(phone, validateCode)) {
			throw new GenericException("验证码错误!");
		}
		UserAccount ua = userAccountDao.getUserByPhone(phone);
		if (ua == null) {
			throw new GenericException("用户不存在!");
		}
		int count = userAccountDao.updatePassword(ua.getId(), _generatePwd(password));
		return count > 0 ? true : false;
	}

	/**
	 * 后台重置密码
	 *
	 * @param sp
	 * @param userId
	 */
	@Override
	public boolean resetPassword(SystemParam sp, long userId) {
		return userAccountDao.updatePassword(userId, _generatePwd(Constant.USER_DEFAULT_PASSWORD)) > 0;
	}


	@Override
	public Long register(SystemParam sp, String signupType, String phone, String password) {
		UserAccount ua = new UserAccount();
		ua.setNickName(_generateNickName());
		ua.setPhone(phone);
		ua.setStatus(UserStatusEnum.actived.name());
		ua.setUsername(generateUserName());
		ua.setPassword(_generatePwd(password));
		ua.setRegisterType(UserAccount.REGISTER_TYPE_PHONE);
		userAccountDao.save(ua);

		UserProfile up = new UserProfile();
		up.setUserId(ua.getId());
		up.setHeadIcon(Constant.USER_DEFAULT_HEAD_ICON);
		userProfileDao.save(up);

		UserRegisterLogin url = new UserRegisterLogin();
		url.setUserId(ua.getId());
		url.setRegisterIp(sp.getClientIp());
		url.setRegisterDeviceNo(sp.getAppHeaderInfo().getDeviceNo());
		url.setRegisterOsVersion(sp.getAppHeaderInfo().getOsVersion());
		url.setRegisterWay(signupType);
		url.setLastLoginIp(sp.getClientIp());
		url.setLastLoginDeviceNo(sp.getAppHeaderInfo().getDeviceNo());
		userRegisterLoginDao.save(url);

		//送流量
		AddUserFlowDto flowDto = new AddUserFlowDto();
		flowDto.setTotal(10L * 1024 * 1024 * 1024); //10G
		flowDto.setUserId(ua.getId());
		flowDto.setType(AddUserFlowType.REGISTER);
		flowDto.setRemark("充值赠送10G");
		userFlowService.addFlow(flowDto);

		return ua.getId();
	}

	/**
	 * 后台注册账号  或者 更新账号信息
	 *
	 * @param sp
	 * @param userAccount
	 * @param userProfile
	 */
	@Override
	public void registerOrUpdate(SystemParam sp, UserAccount userAccount, UserProfile userProfile) {
		Long userId = userAccount.getId();
		boolean regist = userId == null;

		if(regist){
			String password = userAccount.getPassword();
			if(StringUtil.isEmpty(password)){
				password = Constant.USER_DEFAULT_PASSWORD;
			}
			userAccount.setPassword(this._generatePwd(password));
		}
		if(StringUtil.isNotEmpty(userAccount.getPhone())) {
			if(userAccountDao.isNotUnique(userAccount,"phone")){
				throw new GenericException("手机号["+userAccount.getPhone()+"]已被注册");
			}
		}
		userAccountDao.saveOrUpdate(userAccount);
		if(regist){
			userId = userAccount.getId();
			userProfile.setUserId(userId);
			userProfileDao.save(userProfile);

			UserRegisterLogin url = new UserRegisterLogin();
			url.setUserId(userId);
			url.setRegisterWay("backend");
			userRegisterLoginDao.save(url);
		}else{
			userProfileDao.update(userProfile);
		}

	}

	private String _generateNickName() {
		String userName = "CC" + RandomIDUtil.getNumber(6);
		int count = userAccountDao.countUserByUserName(userName);
		if (count > 0) {
			userName = generateUserName();
		}
		return userName;
	}

	public static String generateUserName() {
		return "CC" + RandomIDUtil.getNumber(6);
	}

}
