package cc.ccplay.booster.user.service;

import cc.ccplay.booster.user.dao.SysMenuDao;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.api.user.SysMenuService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.user.SysMenu;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service(interfaceClass=SysMenuService.class)
@org.springframework.stereotype.Service
public class SysMenuServiceImpl implements SysMenuService {

    @Autowired
    private SysMenuDao sysMenuDao;

    @Override
    public Page getPage(SystemParam systemParam,long parentId) {
        return sysMenuDao.getPage(parentId);
    }

    @Override
    public int delete(SystemParam systemParam, long id) {
        return sysMenuDao.logicDelete(id);
    }

    @Override
    public SysMenu getMenu(SystemParam systemParam, long id) {
        return sysMenuDao.get(id);
    }

    @Override
    public SysMenu saveOrUpdate(SystemParam systemParam, SysMenu menu) {
        if(menu.getId() != null){ //update
            //不更新字段
            menu.setParentId(null);
            menu.setLevel(null);
        }
        return sysMenuDao.saveOrUpdate(menu);
    }

    @Override
    public int updateStatus(SystemParam systemParam, long id, long status) {
        SysMenu sysMenu = new SysMenu();
        sysMenu.setId(id);
        sysMenu.setStatus(status);
        return sysMenuDao.updateNotNull(sysMenu);
    }

    @Override
    public List<SysMenu> getMenuList(long roleId) {
        return sysMenuDao.getMenuList(roleId);
    }

    @Override
    public List<SysMenu> getAllMenuList() {
        return sysMenuDao.getAllMenu();
    }
}
