package cc.ccplay.booster.user.dao;

import cc.ccplay.booster.base.model.user.UserProfile;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

@Repository
public class UserProfileDao extends BaseDao<UserProfile> {

    public void addFollowCount(long userId,int value){
        String sql = " update user_profile set follow_count = follow_count + :value where user_id = :userId " ;
        super.update(sql, MixUtil.newHashMap("userId",userId,"value",value));
    }

    public void addFansCount(long userId,int value){
        String sql = " update user_profile set fans_count = fans_count + :value where user_id = :userId " ;
        super.update(sql, MixUtil.newHashMap("userId",userId,"value",value));
    }
}
