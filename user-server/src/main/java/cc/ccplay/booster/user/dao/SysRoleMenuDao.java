package cc.ccplay.booster.user.dao;

import cc.ccplay.booster.base.model.user.SysRoleMenu;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

@Repository
public class SysRoleMenuDao extends BaseDao<SysRoleMenu>{

    public int deleteByRoleId(long roleId){
        String sql = "delete from sys_role_menu where role_id = :roleId ";
        return super.update(sql, MixUtil.newHashMap("roleId",roleId));
    }

}
