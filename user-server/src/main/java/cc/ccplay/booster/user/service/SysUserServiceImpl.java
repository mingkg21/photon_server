package cc.ccplay.booster.user.service;

import cc.ccplay.booster.user.dao.SysUserDao;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.api.user.SysUserService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.user.SysUser;
import cc.ccplay.booster.base.util.Md5Util;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;

@Service(interfaceClass=SysUserService.class)
@org.springframework.stereotype.Service
public class SysUserServiceImpl implements SysUserService{

    @Autowired
    private SysUserDao sysUserDao;

    @Override
    public SysUser getUser(SystemParam parameter,long userId) {
        return sysUserDao.get(userId);
    }

    @Override
    public int logicDelete(SystemParam parameter,long userId) {
        return sysUserDao.logicDelete(userId);
    }

    @Override
    public SysUser doLogin(SystemParam parameter, String account, String pwd) {
        SysUser user = sysUserDao.getUserByAccount(account);
        if(user == null){
            throw new GenericException("密码或者账号不存在");
        }
        String password = getPassword(pwd,account);
        if(!password.equals( user.getPassword())){
            throw new GenericException("密码或者账号不存在");
        }

        if(StringUtil.equals(user.getStatus(),SysUser.STATUS_DISABLED)){
            throw new GenericException("账号已被禁用");
        }

        return user;
    }

    @Override
    public Page getList(SystemParam parameter) {
        return sysUserDao.getList();
    }

    @Override
    public SysUser saveOrUpdate(SystemParam parameter, SysUser user) {
        String pwd = user.getPassword();
        if(pwd != null){
            user.setPassword(this.getPassword(pwd,user.getAccount()));
        }
        if(user.getId() == null) {
            if (sysUserDao.isNotUnique(user, "account")) {
                throw new GenericException("账号[" + user.getAccount() + "]已经存在");
            }
        }else{
            //账号注册之后就不能修改
            user.setAccount(null);
        }

        return  sysUserDao.saveOrUpdate(user);
    }

    @Override
    public void updatePassword(SystemParam parameter, long userId,String oldPassword,String newPassword) {
        SysUser user = sysUserDao.get(userId);
        if(user == null){
            throw new GenericException("用户不存在");
        }
        String old = this.getPassword(oldPassword,user.getAccount());
        if(!old.equals(user.getPassword())){
            throw new GenericException("旧密码不正确");
        }
        String curent = this.getPassword(newPassword,user.getAccount());
        SysUser updateUser = new SysUser();
        updateUser.setId(user.getId());
        updateUser.setPassword(curent);
        sysUserDao.updateNotNull(updateUser);
    }

    @Override
    public void resetPassword(SystemParam parameter, long userId, String password) {
        SysUser user = sysUserDao.get(userId);
        if(user == null){
            throw new GenericException("用户不存在");
        }
        String pwd = this.getPassword(password,user.getAccount());

        SysUser updateUser = new SysUser();
        updateUser.setId(user.getId());
        updateUser.setPassword(pwd);
        sysUserDao.updateNotNull(updateUser);
    }

    @Override
    public int updateStatus(SystemParam parameter, long userId, long status) {
        SysUser updateUser = new SysUser();
        updateUser.setId(userId);
        updateUser.setStatus(status);
        return sysUserDao.updateNotNull(updateUser);
    }

    private String getPassword(String pwd,String account){
        return Md5Util.md5(account+"_"+pwd+"_xianyo-backend-user");
    }


    /**
     * 七牛上传工具 登录
     *
     * @param account
     * @param pwd
     * @param useMd5
     * @return
     */
    @Override
    public SysUser doLoginForUploader(String account, String pwd, boolean useMd5) {
        SysUser user = sysUserDao.getUserByAccount(account);
        if(user == null){
            throw new GenericException("用户不存在");
        }

        String checkPwd = pwd;
        if(!useMd5){
            checkPwd = getPassword(pwd,account);
        }

        if(!checkPwd.equals(user.getPassword())){
            throw new GenericException("密码不正确");
        }
        return user;
    }
}
