package cc.ccplay.booster.user.dao;

import cc.ccplay.booster.base.model.user.UserSystemMessage;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserSystemMessageDao extends BaseDao<UserSystemMessage> {

    public Page<UserSystemMessage> getPageByUserId(long userId){
        String sql = " select * from user_system_message " +
                "  where (user_id = :userId or user_id is null) " +
                " and push_status = "+UserSystemMessage.PUSH_STATUS_SENT
                +" order by id desc ";
        return super.paged2Obj(sql, MixUtil.newHashMap("userId",userId));
    }

}
