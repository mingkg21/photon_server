package cc.ccplay.booster.user.dao;

import cc.ccplay.booster.base.dto.user.UserFollowDto;
import cc.ccplay.booster.base.model.user.UserAccount;
import cc.ccplay.booster.base.model.user.UserFollow;
import cc.ccplay.booster.base.model.user.UserProfile;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

@Repository
public class UserFollowDao extends BaseDao<UserFollow> {

    public Page<UserFollowDto> getFollowPage(long fansUserId){
        StringBuffer sql = new StringBuffer(" select ");
        sql.append(super.getSelectColStr(UserAccount.class,"c","userAccount")+",");
        sql.append(super.getSelectColStr(UserProfile.class,"d","userProfile")+",");
        sql.append(" CASE WHEN b.fans_user_id is not NULL THEN 1 ELSE 0 END as follow_type ");
        sql.append(" FROM user_follow a ");
        sql.append(" LEFT JOIN user_follow b ");
        sql.append(" ON(a.be_follow_user_id = b.fans_user_id and a.fans_user_id = :fansUserId) ");
        sql.append(" LEFT JOIN user_account c ");
        sql.append(" ON(a.be_follow_user_id = c.id) ");
        sql.append(" LEFT JOIN user_profile d ");
        sql.append(" ON(a.be_follow_user_id = d.user_id) ");
        sql.append(" WHERE a.fans_user_id = :fansUserId ");
        return super.paged2Obj(sql.toString(), MixUtil.newHashMap("fansUserId",fansUserId),UserFollowDto.class);
    }

    public Page<UserFollowDto> getFansPage(long befollowUserId){
        StringBuffer sql = new StringBuffer(" select ");
        sql.append(super.getSelectColStr(UserAccount.class,"c","userAccount")+",");
        sql.append(super.getSelectColStr(UserProfile.class,"d","userProfile")+",");
        sql.append(" CASE WHEN b.fans_user_id is not NULL THEN 1 ELSE 0 END as follow_type ");
        sql.append(" FROM user_follow a ");
        sql.append(" LEFT JOIN user_follow b ");
        sql.append(" ON(a.fans_user_id = b.be_follow_user_id and a.be_follow_user_id = :befollowUserId) ");
        sql.append(" LEFT JOIN user_account c ");
        sql.append(" ON(a.fans_user_id = c.id) ");
        sql.append(" LEFT JOIN user_profile d ");
        sql.append(" ON(a.fans_user_id = d.user_id) ");
        sql.append(" WHERE a.be_follow_user_id = :befollowUserId ");
        return super.paged2Obj(sql.toString(), MixUtil.newHashMap("befollowUserId",befollowUserId),UserFollowDto.class);
    }

}
