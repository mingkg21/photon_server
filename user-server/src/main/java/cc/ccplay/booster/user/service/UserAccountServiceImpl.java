package cc.ccplay.booster.user.service;

import cc.ccplay.booster.base.api.user.UserFlowService;
import cc.ccplay.booster.base.constants.Constant;
import cc.ccplay.booster.base.dto.content.booster.AddUserFlowDto;
import cc.ccplay.booster.base.enums.AddUserFlowType;
import cc.ccplay.booster.base.model.user.ThirdPlaformUser;
import cc.ccplay.booster.base.model.user.UserProfile;
import cc.ccplay.booster.base.util.RandomIDUtil;
import cc.ccplay.booster.user.dao.ThirdPlaformUserDao;
import cc.ccplay.booster.user.dao.UserAccountDao;
import cc.ccplay.booster.user.dao.UserProfileDao;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.api.user.UserAccountService;
import cc.ccplay.booster.base.api.user.UserRedisService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.enums.RedisCacheKeyEnum;
import cc.ccplay.booster.base.model.user.UserAccount;
import cc.ccplay.booster.base.reader.dto.UserInfo;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service(interfaceClass = UserAccountService.class)
@org.springframework.stereotype.Service
public class UserAccountServiceImpl extends BaseUserService implements UserAccountService{

    @Autowired
    private UserAccountDao userAccountDao;

    @Autowired
    private UserRedisService userRedisService;


    @Autowired
    private ThirdPlaformUserDao thirdPlaformUserDao;

    @Autowired
    private UserProfileDao userProfileDao;

    @Autowired
    private UserFlowService userFlowService;

    @Override
    public List<UserInfo> getUserInfo(Long... ids) {
        return userAccountDao.getUserInfo(ids);
    }

    @Override
    public UserInfo getUserInfo(Long id) {
        if(id == null){
            return null;
        }
        List<UserInfo> list = userAccountDao.getUserInfo(id);
        if(list.size() == 0){
            return null;
        }
        return list.get(0);
    }

    @Override
    public Page getPage(SystemParam param, UserAccount searchModel) {
        return userAccountDao.getPage(searchModel);
    }

    @Override
    public boolean updateStatus(SystemParam param, long userId, String status) {
        String idTokenKey = RedisCacheKeyEnum.user_id_token.getKey(userId);
        String oldTokenKey = userRedisService.get(idTokenKey);
        if(StringUtil.isNotEmpty(oldTokenKey)){//单客户端登录
            userRedisService.delete(oldTokenKey);//踢出上次登录的token
        }
        UserAccount updateModel = new UserAccount();
        updateModel.setStatus(status);
        updateModel.setId(userId);
        return userAccountDao.updateNotNull(updateModel) > 0;
    }

    @Override
    public UserAccount getAccountByPhone(String phone) {
        return userAccountDao.getUserByPhone(phone);
    }


    @Override
    public UserAccount getAccountByOpenId(String openId, long platform) {
        return userAccountDao.getUserByOpenId(openId,platform);
    }

    @Override
    public UserAccount createOrGetByOpenId(String openId, long platform) {
        UserAccount userAccount = userAccountDao.getUserByOpenId(openId,platform);
        if(userAccount != null){
            return userAccount;
        }

        userAccount = new UserAccount();
        String uname = RegisterServiceImpl.generateUserName();
        userAccount.setUsername(uname);
        userAccount.setNickName(uname);
        if(platform == ThirdPlaformUser.PLATFORM_18HANHUA) {
            userAccount.setRegisterType(UserAccount.REGISTER_TYPE_18HANHUA);
        }else if(platform == ThirdPlaformUser.PLATFORM_QQ){
            userAccount.setRegisterType(UserAccount.REGISTER_TYPE_QQ);
        }else if(platform == ThirdPlaformUser.PLATFORM_WECHAT){
            userAccount.setRegisterType(UserAccount.REGISTER_TYPE_WECHAT);
        }
        userAccountDao.save(userAccount);


        UserProfile up = new UserProfile();
        up.setUserId(userAccount.getId());
        up.setHeadIcon(Constant.USER_DEFAULT_HEAD_ICON);
        up.setSex(UserProfile.SEX_BOY);
        userProfileDao.save(up);

        ThirdPlaformUser thirdUser = new ThirdPlaformUser();
        thirdUser.setPlatform(platform);
        thirdUser.setOpenId(openId);
        thirdUser.setUnionid(openId);
        thirdUser.setUserId(userAccount.getId());
        thirdUser.setNickname(uname);
        thirdPlaformUserDao.save(thirdUser);


        //送流量
        AddUserFlowDto flowDto = new AddUserFlowDto();
        flowDto.setTotal(10L * 1024 * 1024 * 1024); //10G
        flowDto.setUserId(userAccount.getId());
        flowDto.setType(AddUserFlowType.REGISTER);
        flowDto.setRemark("充值赠送10G");
        userFlowService.addFlow(flowDto);
        return userAccount;
    }


    @Override
    public void modifyPassword(long userId, String oldPwd, String newPwd) {
        if(userAccountDao.modifyPassword(userId,_generatePwd(oldPwd),_generatePwd(newPwd)) == 0){
            throw new GenericException("旧密码不正确");
        }
    }
}
