package cc.ccplay.booster.user.dao;

import cc.ccplay.booster.base.model.user.UserFlow;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class UserFlowDao extends BaseDao<UserFlow> {

    public int addUsedFlow(long userId,long upload,long download){
        StringBuilder sb = new StringBuilder();
        Map paramMap = MixUtil.newHashMap("userId",userId);
        sb.append(" update user_flow set ");
        boolean flag = false;
        if(upload > 0){
            sb.append(" used_upload_flow = used_upload_flow + :upload ");
            flag = true;
            paramMap.put("upload",upload);
        }
        if(download > 0){
            if(flag){
                sb.append(",");
            }
            sb.append(" used_download_flow = used_download_flow + :download ");
            paramMap.put("download",download);
        }
        sb.append(" where user_id = :userId ");
        return super.update(sb.toString(),paramMap);
    }

    public int addTotalFlow(long userId,long flow){
        String sql = "update user_flow set total_flow = total_flow + :flow where user_id = :userId  ";
        return super.update(sql,MixUtil.newHashMap("flow",flow,"userId",userId));
    }
}
