package cc.ccplay.booster.user.dao;

import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import cc.ccplay.booster.base.model.user.UserPushRef;

import java.util.List;

@Repository
public class UserPushRefDao extends BaseDao<UserPushRef> {

	public int countByPushDeviceId(String pushDeviceId) {
		String sql = "select count(1) from user_push_ref where push_device_id = :pushDeviceId";
		return queryForInt(sql, MixUtil.newHashMap("pushDeviceId", pushDeviceId));
	}

	public void updatePushDevice(String pushDeviceId, Long userId, String deviceNo, String userAgent, String ip) {
        String sql =
            "UPDATE user_push_ref " +
                "SET " +
                "  update_datetime = now()," +
                "  user_id         = :userId," +
                "  device_no       = :deviceNo," +
                "  user_agent      = :userAgent," +
                "  ip              = :ip " +
                " WHERE push_device_id = :pushDeviceId ";
		update(sql, MixUtil.newHashMap("pushDeviceId", pushDeviceId, "userId", userId, "deviceNo", deviceNo, "userAgent", userAgent, "ip", ip));
	}

	public List<UserPushRef> selectPushDeviceIdsByUserId(long userId){
		String sql = "select push_device_id from user_push_ref where user_id = :userId ";
		return super.query2Model(sql,MixUtil.newHashMap("userId",userId));
	}

}
