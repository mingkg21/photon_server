package cc.ccplay.booster.user.service;

import cc.ccplay.booster.base.api.user.UserFlowService;
import cc.ccplay.booster.base.dto.content.booster.AddUserFlowDto;
import cc.ccplay.booster.base.enums.AddUserFlowType;
import cc.ccplay.booster.user.dao.*;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.model.user.*;
import cc.ccplay.booster.base.api.common.SmsService;
import cc.ccplay.booster.base.api.log.UserLoginLogService;
import cc.ccplay.booster.base.api.user.LoginService;
import cc.ccplay.booster.base.api.user.UserPushRefService;
import cc.ccplay.booster.base.api.user.UserRedisService;
import cc.ccplay.booster.base.constants.Constant;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.user.LoginResultDto;
import cc.ccplay.booster.base.enums.RedisCacheKeyEnum;
import cc.ccplay.booster.base.util.Md5Util;
import cc.ccplay.booster.base.util.RegexUtil;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.UUID;

@org.springframework.stereotype.Service
@Service(interfaceClass = LoginService.class)
public class LoginServiceImpl extends BaseUserService implements LoginService {

	@Autowired
	private UserAccountDao userAccountDao;

	@Autowired
	private UserRegisterLoginDao userRegisterLoginDao;

	@Autowired
	private UserRedisService userRedisService;

	@Autowired
	private UserPushRefService userPushRefService;

	@Reference
	private UserLoginLogService userLoginLogService;

	@Autowired
	private UserProfileDao userProfileDao;

	@Autowired
	private ThirdPlaformUserDao wechatUserDao;


	@Autowired
	private UserFlowService userFlowService;


	@Reference
	private SmsService smsService;


	@Override
	public LoginResultDto loginByPhone(SystemParam sp, String phone, String password) {
		if (StringUtil.isEmpty(phone) || StringUtil.isEmpty(password)) {
			throw new GenericException("缺少参数!");
		}
		if (!RegexUtil.isMobile(phone)) {
			throw new GenericException("手机号格式错误!");
		}

		UserAccount ua = userAccountDao.getUserByPhone(phone);
		if (ua == null) {
			throw new GenericException("用户不存在!");
		}

		String pwd = _generatePwd(password);
		if (!pwd.equals(ua.getPassword())) {
			throw new GenericException("用户密码不正确!");
		}

		if("locked".equals(ua.getStatus())){
			throw new GenericException("账号已被禁止登陆!");
		}

		LoginResultDto dto = afterLogin(sp, ua.getId(), phone);
		dto.setDisplayName(ua.getNickName());
		return dto;
	}

	@Override
	public LoginResultDto loginByCode(SystemParam sp, String phone, String validateCode) {
		if (StringUtil.isEmpty(phone) || StringUtil.isEmpty(validateCode)) {
			throw new GenericException("缺少参数!");
		}
		if (RegexUtil.isMobile(phone)) {
			throw new GenericException("手机号格式错误!");
		}
		String code = userRedisService.get(RedisCacheKeyEnum.sms_validatecode.getKey(phone));
		if (!smsService.verifySmsCode(phone, validateCode)) {
			throw new GenericException("验证码错误!");
		}
		UserAccount ua = userAccountDao.getUserByPhone(phone);
		if (ua == null) {
			throw new GenericException("用户不存在!");
		}

		if("locked".equals(ua.getStatus())){
			throw new GenericException("账号已被禁止登陆!");
		}

		LoginResultDto dto = afterLogin(sp, ua.getId(),ua.getPhone());
		dto.setDisplayName(ua.getNickName());
		return dto;
	}


	private String createUserToken(){
		while(true) {
			String userToken = UUID.randomUUID().toString();
			userToken = userToken.replaceAll("-","");
			if(!userRedisService.hasKey(userToken)){
				return userToken;
			}
		}
	}

	@Override
	public LoginResultDto afterLogin(SystemParam sp, Long userId,String phone) {
		String userToken = createUserToken();
		// 更新最后登录时间
		UserRegisterLogin record = new UserRegisterLogin();
		record.setUserId(userId);
		record.setLastLoginDatetime(new Date());
		record.setLastLoginIp(sp.getClientIp());
		record.setLoginCountPlus(Boolean.TRUE);
		record.setLastLoginDeviceNo(sp.getAppHeaderInfo().getDeviceNo());
		if(userRegisterLoginDao.updateNotNull(record) == 0){
			userRegisterLoginDao.save(record);
		}
		String newToken = RedisCacheKeyEnum.user_token_key.getKey(userToken);
		String idTokenKey = RedisCacheKeyEnum.user_id_token.getKey(userId);
		String oldTokenKey = userRedisService.get(idTokenKey);
		if(StringUtil.isNotEmpty(oldTokenKey)){//单客户端登录
			userRedisService.delete(oldTokenKey);//踢出上次登录的token
		}
		userRedisService.set(idTokenKey,newToken);//设置新token
		userRedisService.set(newToken, String.valueOf(userId));

		try {
			// 插入登录日志
			userLoginLogService.insertLoginLog(sp, userId, null);
			if (!StringUtils.isEmpty(sp.getAppHeaderInfo().getPushDeviceId())) {
				userPushRefService.saveUserPushRef(sp.getAppHeaderInfo().getPushDeviceId(), userId, sp.getAppHeaderInfo().getDeviceNo(),
						sp.getAppHeaderInfo().getUserAgent(), sp.getClientIp());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		LoginResultDto loginResultDto = new LoginResultDto(userToken, userId);
		loginResultDto.setBindPhone(phone == null ? 0 : 1);
		return loginResultDto;
	}

	@Override
	public LoginResultDto loginByThirdPlaftformOpenId(SystemParam sp, String openId,long plaftorm) {
		UserAccount ua = userAccountDao.getUserByOpenId(openId,plaftorm);
		if(ua == null){
			return null;
		}
		if("locked".equals(ua.getStatus())){
			throw new GenericException("账号已被禁止登陆!");
		}

		LoginResultDto dto = afterLogin(sp, ua.getId(),ua.getPhone());
		dto.setDisplayName(ua.getNickName());
		return dto;
	}

	/**
	 * 创建用户同时登录
	 *
	 * @param sp
	 * @param user
	 * @return
	 */
	@Override
	public LoginResultDto loginByThirdPlaftform(SystemParam sp, ThirdPlaformUser user) {
		UserAccount userAccount = new UserAccount();
		userAccount.setUsername(RegisterServiceImpl.generateUserName());
		userAccount.setNickName(user.getNickname());

		if(StringUtil.equals(user.getPlatform(),ThirdPlaformUser.PLATFORM_QQ)){
			userAccount.setRegisterType(UserAccount.REGISTER_TYPE_QQ);
		}else if(StringUtil.equals(user.getPlatform(),ThirdPlaformUser.PLATFORM_WECHAT)){
			userAccount.setRegisterType(UserAccount.REGISTER_TYPE_WECHAT);
		}else if(StringUtil.equals(user.getPlatform(),ThirdPlaformUser.PLATFORM_18HANHUA)){
			userAccount.setRegisterType(UserAccount.REGISTER_TYPE_18HANHUA);
		}
		userAccountDao.save(userAccount);

		UserProfile up = new UserProfile();
		up.setUserId(userAccount.getId());
		up.setHeadIcon(user.getHeadimg());


		if("0".equals(user.getSex())) {
			up.setSex(UserProfile.SEX_BOY);
		}else if("1".equals(user.getSex())){
			up.setSex(UserProfile.SEX_GIRL);
		}else{
			up.setSex(UserProfile.SEX_SECRECY);
		}

		userProfileDao.save(up);
		user.setUserId(userAccount.getId());
		wechatUserDao.save(user);

		//送流量
		AddUserFlowDto flowDto = new AddUserFlowDto();
		flowDto.setTotal(10L * 1024 * 1024 * 1024); //10G
		flowDto.setUserId(userAccount.getId());
		flowDto.setType(AddUserFlowType.REGISTER);
		flowDto.setRemark("充值赠送10G");
		userFlowService.addFlow(flowDto);



		LoginResultDto dto = afterLogin(sp, userAccount.getId(), null);
		dto.setDisplayName(user.getNickname());
		return dto;
	}
}
