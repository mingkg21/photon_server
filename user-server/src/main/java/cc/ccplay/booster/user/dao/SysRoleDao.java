package cc.ccplay.booster.user.dao;

import cc.ccplay.booster.base.enums.DeleteFlag;
import cc.ccplay.booster.base.model.user.SysRole;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SysRoleDao extends BaseDao<SysRole> {
    public List<SysRole> getAll(){
        String sql = "select * from sys_role where delete_flag = "+ DeleteFlag.NO_DELETE+" order by id desc ";
        return super.query2Model(sql);
    }

    public Page<SysRole> getPage(){
        String sql = "select * from sys_role where delete_flag ="+ DeleteFlag.NO_DELETE+" order by id desc ";
        return super.paged2Obj(sql);
    }
}
