package cc.ccplay.booster.user.service;

import cc.ccplay.booster.base.api.user.UserFlowService;
import cc.ccplay.booster.base.dto.content.booster.AddUserFlowDto;
import cc.ccplay.booster.base.enums.AddUserFlowType;
import cc.ccplay.booster.base.model.user.UserFlow;
import cc.ccplay.booster.base.model.user.UserFlowRecord;
import cc.ccplay.booster.user.dao.UserFlowDao;
import cc.ccplay.booster.user.dao.UserFlowRecordDao;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service(interfaceClass = UserFlowService.class)
@org.springframework.stereotype.Service
public class UserFlowServiceImpl extends BaseUserService implements UserFlowService {

    @Autowired
    private UserFlowDao userFlowDao;


    @Autowired
    private UserFlowRecordDao userFlowRecordDao;

//    @Override
//    public UserFlow addFlow(long userId, long upload, long download) {
//        UserFlow userFlow = userFlowDao.get(userId);
//        if(userFlow == null){
//            return null;
//        }
//        //更新流量
//        userFlowDao.addUsedFlow(userId,upload,download);
//        return userFlow;
//    }

    @Override
    public UserFlow getFlow(long userId) {
        return userFlowDao.get(userId);
    }

    @Override
    public UserFlow addFlow(AddUserFlowDto dto) {
        Long userId = dto.getUserId();
        UserFlow userFlow = userFlowDao.get(userId);
        AddUserFlowType type = dto.getType();
        if(type.isAdd()){ //增加流量
            Long total = dto.getTotal();
            if(total == null || total.longValue() <= 0){
                return userFlow;
            }
            boolean flag = false;
            if(userFlow == null){
                userFlow = new UserFlow();
                userFlow.setTotalFlow(total);
                userFlow.setUsedDownloadFlow(0L);
                userFlow.setUsedUploadFlow(0L);
                userFlow.setUserId(userId);
                userFlowDao.save(userFlow);
                flag = true;
            }else {
                flag = userFlowDao.addTotalFlow(userId,total) > 0;
            }

            if(flag){
                UserFlowRecord record = new UserFlowRecord();
                record.setFlow(total);
                record.setType(type.getValue());
                record.setUserId(userId);
                record.setCreateTime(dto.getTime());
                record.setRemark(dto.getRemark());
                userFlowRecordDao.save(record);
            }
        }else{//扣费
            if(userFlow == null){
                return null;
            }
            Long upload = dto.getUpload();
            Long download = dto.getDownload();
            if(upload == null && download == null){
                return userFlow;
            }

            if(upload == null){
                upload = 0L;
            }
            if(download == null){
                download = 0L;
            }

            if(userFlowDao.addUsedFlow(userId,upload,download) > 0 ){
                UserFlowRecord record = new UserFlowRecord();
                record.setFlow(upload.longValue() + download.longValue());
                record.setType(type.getValue());
                record.setUserId(userId);
                record.setCreateTime(dto.getTime());
                record.setRemark(dto.getRemark());
                userFlowRecordDao.save(record);
            }
        }
        return userFlow;
    }
}
