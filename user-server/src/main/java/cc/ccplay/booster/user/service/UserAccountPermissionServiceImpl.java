package cc.ccplay.booster.user.service;

import cc.ccplay.booster.user.dao.UserAccountPermissionDao;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.api.user.UserAccountPermissionService;
import cc.ccplay.booster.base.api.user.UserAccountService;
import cc.ccplay.booster.base.dto.user.UserAccountPermissionDto;
import cc.ccplay.booster.base.model.user.UserAccountPermission;
import cc.ccplay.booster.base.reader.dto.UserInfo;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;

@Service(interfaceClass = UserAccountPermissionService.class)
@org.springframework.stereotype.Service
public class UserAccountPermissionServiceImpl extends BaseUserService implements UserAccountPermissionService {

    @Autowired
    private UserAccountPermissionDao userAccountPermissionDao;

    @Autowired
    private UserAccountService userAccountService;

    @Override
    public void saveOrUpdate(UserAccountPermission uap) {
        Long userId = uap.getUserId();
        if(userAccountPermissionDao.get(userId) == null){
            userAccountPermissionDao.save(uap);
        }else{
            userAccountPermissionDao.updateNotNull(uap);
        }
    }

    @Override
    public boolean hasGameCommentPermission(long userId) {
        UserAccountPermission uap = userAccountPermissionDao.get(userId);
        if(uap == null){
            return true;
        }
        return StringUtil.equals(1L,uap.getAllowGameComment());
    }

    @Override
    public boolean hasSaveRecordCommentPermission(long userId) {
        UserAccountPermission uap = userAccountPermissionDao.get(userId);
        if(uap == null){
            return true;
        }
        return StringUtil.equals(1L,uap.getAllowSaveRecordComment());
    }


    @Override
    public UserAccountPermissionDto getDto(long userId) {
        UserInfo userInfo = userAccountService.getUserInfo(userId);
        if(userInfo == null){
            throw new GenericException("用户不存在");
        }
        UserAccountPermissionDto dto = new UserAccountPermissionDto();
        dto.setUserInfo(userInfo);
        dto.setUserAccountPermission(userAccountPermissionDao.get(userId));
        return dto;
    }
}
