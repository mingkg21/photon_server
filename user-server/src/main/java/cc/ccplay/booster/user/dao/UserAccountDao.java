package cc.ccplay.booster.user.dao;

import cc.ccplay.booster.base.model.user.UserProfile;
import cc.ccplay.booster.base.reader.dto.UserInfo;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.easyj.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import cc.ccplay.booster.base.model.user.UserAccount;

import java.util.*;

@Repository
public class UserAccountDao extends BaseDao<UserAccount> {

	public Page getPage(UserAccount searchModel){
		Map paramMap = MixUtil.newHashMap();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT a.*,b.head_icon,c.register_datetime,c.last_login_datetime,c.last_login_ip ");
		sql.append(" ,d.used_download_flow,d.used_upload_flow,d.total_flow ");
		sql.append(" FROM user_account a  ");
		sql.append(" LEFT JOIN user_profile b ");
		sql.append(" ON(a.id = b.user_id ) ");
		sql.append(" LEFT JOIN user_register_login c ");
		sql.append(" ON(a.id = c.user_id ) ");
		sql.append(" LEFT JOIN user_flow d ");
		sql.append(" ON(d.user_id = a.id ) ");
		sql.append(" WHERE 1 = 1  ");
		if(searchModel != null){
			Long id = searchModel.getId();
			if(id != null){
				sql.append(" and a.id = :id ");
				paramMap.put("id",id);
			}
			String phone = searchModel.getPhone();
			if(StringUtil.isNotEmpty(phone)){
				sql.append(" and a.phone = :phone ");
				paramMap.put("phone",phone);
			}
			String nickName = searchModel.getNickName();
			if(StringUtil.isNotEmpty(nickName)){
				sql.append(" and a.nick_name like :nickName ");
				paramMap.put("nickName","%"+nickName+"%");
			}

			Integer fakeFlag = searchModel.getFakeFlag();
			if(fakeFlag != null){
				sql.append(" and a.fake_flag = :fakeFlag ");
				paramMap.put("fakeFlag",fakeFlag);
			}

			String status = searchModel.getStatus();
			if(StringUtil.isNotEmpty(status)){
				sql.append(" and a.status = :status ");
				paramMap.put("status",status);
			}

			Long registerType = searchModel.getRegisterType();
			if(registerType != null){
				sql.append(" and a.register_type = :registerType ");
				paramMap.put("registerType",registerType);
			}
		}
		sql.append(" order by a.id desc");
		return super.page2CamelMap(sql.toString(),paramMap);
	}

	public int countUserByPhone(String phone) {
		String sql = "select count(1) from user_account where phone = :phone and delete_flag = 0";
		return queryForInt(sql, MixUtil.newHashMap("phone", phone));
	}

	public UserAccount getUserByPhone(String phone) {
		String sql = "select * from user_account where phone = :phone and delete_flag = 0";
		return query21Model(sql, MixUtil.newHashMap("phone", phone));
	}


	public UserAccount getUserByOpenId(String openId,long platform){
		String sql = "select b.* from  third_platform_user a " +
				" left join user_account b  " +
				" on(a.user_id = b.id ) "+
				" where a.open_id = :openId " +
				" and a.platform = :platform "+
				" and b.delete_flag = 0";
		return query21Model(sql, MixUtil.newHashMap("openId", openId,"platform",platform));
	}

	public int countUserByUserName(String userName) {
		String sql = "select count(1) from user_account where username = :userName and delete_flag = 0";
		return queryForInt(sql, MixUtil.newHashMap("userName", userName));
	}

	public int updatePassword(Long userId, String password) {
		String sql = "update user_account set password = :password where id = :userId";
		return update(sql, MixUtil.newHashMap("password", password, "userId", userId));
	}

	public List<UserInfo> getUserInfo(Long... ids){
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT ");
		sql.append(super.getSelectColStr(UserAccount.class,"a","userAccount"));
		sql.append(",");
		sql.append(super.getSelectColStr(UserProfile.class,"b","userProfile"));
		sql.append(" FROM user_account a ");
		sql.append(" LEFT JOIN user_profile b ");
		sql.append(" ON(a.id = b.user_id) ");
		sql.append(" WHERE delete_flag = 0 and ");
		if(ids.length == 1){
			sql.append( " a.id = :userId ");
			return super.query2Model(sql.toString(),MixUtil.newHashMap("userId",ids[0]),UserInfo.class);
		}
		sql.append(" a.id in(");
		Set<Long> idSet = new HashSet<>();
		boolean first = true;
		for (int i = 0; i < ids.length; i++) {
			Long id = ids[i];
			if(idSet.contains(id) || id == null){
				continue;
			}
			if(!first){
				sql.append(",");
			}else{
				first = false;
			}
			sql.append(id);
			idSet.add(id);
		}
		if(idSet.size() == 0){
			return new ArrayList<>();
		}
		sql.append(" ) ");
		return  super.query2Model(sql.toString(),UserInfo.class);
	}


	public int modifyPassword(long userId,String oldPwd,String newPwd){
		String sql = "update user_account set password = :newPwd where id = :userId and password = :oldPwd";
		return super.update(sql,MixUtil.newHashMap("newPwd",newPwd,"oldPwd",oldPwd,"userId",userId));
	}
}
