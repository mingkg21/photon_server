package cc.ccplay.booster.user.service;

import cc.ccplay.booster.user.dao.UserPushRefDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;

import com.alibaba.dubbo.config.annotation.Service;

import cc.ccplay.booster.base.api.user.UserPushRefService;
import cc.ccplay.booster.base.model.user.UserPushRef;

@org.springframework.stereotype.Service
@Service(interfaceClass = UserPushRefService.class)
public class UserPushRefServiceImpl extends BaseUserService implements UserPushRefService {

	@Autowired
	private TaskExecutor myExecutor;

	@Autowired
	private UserPushRefDao userPushRefDao;

	@Override
	public void saveUserPushRef(final String pushDeviceId, final Long userId, final String deviceNo, final String userAgent, final String ip) {
		myExecutor.execute(new Runnable() {
			@Override
			public void run() {
				UserPushRef record = new UserPushRef();
				record.setPushDeviceId(pushDeviceId);
				record.setUserId(userId);
				record.setDeviceNo(deviceNo);
				record.setUserAgent(userAgent);
				record.setIp(ip);
				int count = userPushRefDao.countByPushDeviceId(pushDeviceId);
				if (count > 0) {
					userPushRefDao.updatePushDevice(pushDeviceId, userId, deviceNo, userAgent, ip);
				} else {
					userPushRefDao.save(record);
				}
			}
		});
	}
}
