<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String base = request.getContextPath();
%>
<!doctype html>
<html>
<head>
    <title>接口测试页面</title>
    <script src="<%=base%>/common/js/jquery-1.12.4.min.js"></script>
    <script src="<%=base%>/common/js/artTemplate/template.js"></script>
</head>
<body>
<form id="editForm" name="editForm" action="<%=base%>/apitest/doApiTest" method="post">
    <table>
        <tbody>
        <tr>
            <td>API 类目</td>
            <td>
                <select id="catalog" name="catalog" onchange="selectClass(this)" style="width:385px">
                    <option value=""></option>
                </select>
            </td>
        </tr>

        <tr>
            <td>API 列表</td>
            <td>
                <select id="method" name="method" onchange="selectMethod(this)" style="width:385px">
                    <option value=""></option>
                </select>
                <button type="button" onclick="copyArticle()">复制API地址</button>
                <input type="text"  id="copyContent" style="opacity:0"/>
            </td>
        </tr>
        </tbody>
    </table>

    <div id="param_container">

    </div>

    <script id="PARAM_TPL" type="text/html">
        <table style="margin-left:100px">
            <tbody>
            <tr>
                <td width="150px" align="center">参数描述</td>
                <td width="150px" align="center">参数名称</td>
                <td width="150px" align="center">值</td>
            </tr>
            {{if needLogin}}
            <tr>
                <td><font color="red">*</font>用户登录授权</td>
                <td>user_token</td>
                <td><input type="text" name="user_token" value="{{userToken}}" size="50"></td>
            </tr>
            {{/if}}

            {{if !needLogin}}
            <tr>
                <td>用户登录授权</td>
                <td>user_token</td>
                <td><input type="text" name="user_token" value="{{userToken}}" size="50"></td>
            </tr>
            {{/if}}
            {{each parameters as item}}
            <tr>
                <td>{{if item.notNull}}<font color="red">*</font>{{/if}}{{item.remark}}</td>
                <td>{{item.paramterName}}</td>
                <td><input type="text" name="{{item.paramterName}}" value="" size="50"></td>
            </tr>
            {{/each}}

            {{if paging}}
            <tr>
                <td>每页记录数</td>
                <td>pageSize</td>
                <td><input type="text" name="pageSize" value="" size="50"></td>
            </tr>

            <tr>
                <td>页码</td>
                <td>page</td>
                <td><input type="text" name="page" value="" size="50"></td>
            </tr>
            {{/if}}
            </tbody>
        </table>
        {{if !parameters || parameters.length == 0}}
        <b>无需任何参数</b>
        {{/if}}
    </script>
    <button id="btnSubmit" type="button" >提交</button>
    <br>
    <textarea id="apiRsp" rows="5" cols="5" style="width: 700px;height: 500px; resize: none;" readonly="readonly"  ></textarea>
</form>
<script type="text/javascript" src="<%=base %>/common/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<%=base %>/common/js/common.js"></script>
</body>
</html>
<script language="JavaScript">
    var data = ${classes};
    for (var key in data) {
        $("#catalog").append("<option value='" + key + "'>" + data[key].name + "</option>");
    }

    function selectClass(el) {
        var val = el.value;
        $("#method").html('<option value=""></option>');
        if (!val) {
            return;
        }
        var clazz = data[val];
        var methods = clazz.methods;
        for (var key in methods) {
            $("#method").append("<option value='" + methods[key].url + "' key='"+key+"'>" + methods[key].name + "(" + methods[key].url + ")</option>");
        }
    }

    function selectMethod(el) {
        var clazz = $("#catalog").val();
        var method = $(el).find("option:selected").attr("key");
        var pd = (data[clazz].methods)[method];
        var token = $("#param_container").find("input[name='user_token']").val();
        if(token){
            pd["userToken"] = token;
        }else{
            delete pd.userToken;
        }
        var html = template('PARAM_TPL', pd);

        $("#param_container").html(html);
    }

    var successCallback=function(event,param){
        $("#apiRsp").val(JSON.stringify(param.data, null, '\t'));
    };

    $("#editForm").validate({
        errorPlacement:function(error,element){
            var errorT = element.parent().next();
            error.appendTo(errorT);
        },
        rules:{
            // apiKey:{
            //     required:true
            // },
            // apiSecret:{
            //     required:true
            // },
            catalog:{
                required:true
            },
            method:{
                required:true
            }
        },
        onkeyup: false,
        submitHandler:function(form){
            $("#apiRsp").val("");
            $(form).ajaxForm({
                success:successCallback
            });
        }
    });

    $("#btnSubmit").click(function(){
        $("#editForm").submit();
    });

    function copyArticle(){
        var clazz = $("#catalog").val();
        var method = $("#method").find("option:selected").attr("key");
        if(!clazz || !method){
            alert("请先选择接口");
        }
        var input = document.getElementById('copyContent');
        var pd = (data[clazz].methods)[method];
        var url = $.trim(pd.url);
        input.value = url.substring(1);
//        const range = document.createRange();
//        range.selectNode(input);
//        const selection = window.getSelection();
//        if(selection.rangeCount > 0) selection.removeAllRanges();
//        selection.addRange(range);

        input.focus();
        input.select();
        document.execCommand('copy');
    }

</script>

