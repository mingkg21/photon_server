package cc.ccplay.booster.app.web;

import cc.ccplay.booster.base.api.content.*;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import cc.ccplay.booster.base.annotation.Cache;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.constants.Constant;
import cc.ccplay.booster.base.core.AppHeaderInfo;
import cc.ccplay.booster.base.core.Header;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.content.CustomArea;
import cc.ccplay.booster.base.model.content.GameHotWord;
import cc.ccplay.booster.base.model.content.OpenScreen;
import cc.ccplay.booster.base.test.annotation.ApiClass;
import cc.ccplay.booster.base.test.annotation.ApiMethod;
import org.easyj.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Map;

@Controller
@RequestMapping("/config")
@ApiClass("配置API")
public class ConfigController extends BaseAppController  {

    @Reference
    private CustomAreaService customAreaService;

    @Reference
    private GameHotWordService gameHotWordService;

    @Resource(name = "userRedisTemplate")
    private ValueOperations<String,String> valueOperations;

    @Reference
    private ContentRedisCacheService contentRedisCacheService;

    @Reference
    private OpenScreenService openScreenService;

    @Value("${domain.wap}")
    private String wapUrl;



    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/getOpenScreenConf")
    @ApiMethod("获取开屏信息")
    @Cache(cacheTimeout = 10 * 60)
    public void getOpenScreenConf(){
        OpenScreen openScreen = openScreenService.getLastest();
        super.renderJson(openScreen);
    }


    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/getBaseConfig")
    //,headerParams = {Header.CLIENT_CHANNEL_NAME,Header.CLIENT_VERSION_CODE}
    @Cache(cacheTimeout = 10 * 60)
    @ApiMethod("获取基础配置")
    public void getBaseConfig(){
        JSONObject config = new JSONObject();
        config.put("wap.domain",wapUrl);//"http://39.104.60.127/wap"
        config.put("booster.telnet.switch","0");
        config.put("official.icon","http://qiniu.guangzijiasu.com/official.png");
        Object cfg = config;
        super.renderJson(cfg);
    }

    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/getArea")
    @Cache(cacheTimeout = 1 * 60 * 60)
    @ApiMethod("获取基础配置")
    public void getArea(){
        super.renderJson(customAreaService.getAllAreaEnabled());
    }
}
