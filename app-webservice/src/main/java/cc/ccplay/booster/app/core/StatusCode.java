package cc.ccplay.booster.app.core;

import org.easyj.frame.exception.AppException;

public enum StatusCode {

    SUCCESS(100000,"成功"),
    FAILURE(200000,"失败"),
    NO_LOGIN(200001,"没有登录"),
    NOT_AUTHORITY(200002,"没有权限"),
    BIZ_EXCETION(200003,"业务异常"),
    SYSTEM_ERROR(200004,"系统后台异常"),
    TOKEN_EXPIRE(200005,"token过期失效");
    private int  code;
    private String msg;

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    private StatusCode(int code , String defaultMsg){
        this.code = code;
        this.msg = defaultMsg;
    }

    public String getOutputErrorMsg(AppException ex){
        if(this == BIZ_EXCETION){
            return ex.getErrorMessage();
        }
        return this.getMsg();
    }
}
