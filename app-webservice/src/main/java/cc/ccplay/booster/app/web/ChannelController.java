package cc.ccplay.booster.app.web;

import cc.ccplay.booster.base.annotation.Cache;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.api.content.AppChannelManageService;
import cc.ccplay.booster.base.model.content.AppChannelManage;
import cc.ccplay.booster.base.test.annotation.ApiClass;
import cc.ccplay.booster.base.test.annotation.ApiMethod;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/channel")
@ApiClass("渠道控制API")
public class ChannelController extends BaseAppController {

    @Reference
    private AppChannelManageService appChannelManageService;

    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/getChannelInfo")
    //,headerParams = {Header.CLIENT_CHANNEL_NAME,Header.CLIENT_VERSION_CODE}
    @Cache(params = {"channelCode"}, cacheTimeout = 10 * 60)
    @ApiMethod("获取渠道")
    public void getBaseConfig(@NotNull @Param(remark = "渠道号") String channelCode){

        long publishingVersionCode = 0;
        AppChannelManage appChannelManage = appChannelManageService.findAppChannelManage(channelCode);
        if (appChannelManage != null) {
            publishingVersionCode = appChannelManage.getVersionCode();
        }

        JSONObject config = new JSONObject();
        config.put("publishing_version_code", publishingVersionCode);
        Object cfg = config;
        super.renderJson(cfg);
    }

}
