package cc.ccplay.booster.app.web;

import cc.ccplay.booster.base.api.user.UserAccountService;
import cc.ccplay.booster.base.model.user.UserAccount;
import cc.ccplay.booster.base.util.RegexUtil;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.dubbo.config.annotation.Reference;

import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.api.user.RegisterService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.user.LoginResultDto;
import cc.ccplay.booster.base.test.annotation.ApiClass;
import cc.ccplay.booster.base.test.annotation.ApiMethod;

@ApiClass("注册API")
@RequestMapping("/register")
@Controller
public class RegisterController extends BaseAppController {

	@Reference
	private RegisterService registerService;

	@Reference
	private UserAccountService userAccountService;

	@NotNeedLogin
	@ResponseBody
	@RequestMapping(value = "/sendRegisterSms", method = RequestMethod.POST)
	@ApiMethod("发送短信")
	public void sendRegisterSms(@Param(remark = "手机号") @NotNull String phone,
								@Param(remark = "短信类型 login:登录,register:注册,forget:忘记密码,bind:绑定手机号码") String smsType) {

		if (!RegexUtil.isMobile(phone)) {
			throw new GenericException("手机号不正确！");
		}

		if("login".equals(smsType) || "forget".equals(smsType)){
			UserAccount userAccount = userAccountService.getAccountByPhone(phone);
			if(userAccount == null){
				throw new GenericException("该手机号未注册账号");
			}
		}else if("register".equals(smsType) || "bind".equals(smsType)){
			UserAccount userAccount = userAccountService.getAccountByPhone(phone);
			if(userAccount != null){
				throw new GenericException("该手机号已注册过账号");
			}
		}
		SystemParam param = getSystemParam();
		registerService.sendRegisterSms(param, phone);
		renderSuccess();
	}

	@NotNeedLogin
	@ResponseBody
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	@ApiMethod("注册账号")
	public void register(@Param(remark = "注册账号") @NotNull String phone, @Param(remark = "账号密码") @NotNull String password,
			@Param(remark = "手机验证码") @NotNull String validateCode) {
		SystemParam param = getSystemParam();
		LoginResultDto dto = registerService.registerByPhone(param, phone, password, validateCode);
		renderJson(dto);
	}

	@NotNeedLogin
	@ResponseBody
	@RequestMapping(value = "/resetPassword", method = RequestMethod.POST)
	@ApiMethod("重置账号密码")
	public void resetPassword(@Param(remark = "注册账号") @NotNull String phone, @Param(remark = "账号密码") @NotNull String password,
			@Param(remark = "手机验证码") @NotNull String validateCode) {
		SystemParam param = getSystemParam();
		boolean flag = registerService.resetPassword(param, phone, password, validateCode);
		if(flag){
			renderSuccess();
		}else{
			renderFailure();
		}
	}
}
