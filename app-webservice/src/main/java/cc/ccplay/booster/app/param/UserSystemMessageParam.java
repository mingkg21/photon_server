package cc.ccplay.booster.app.param;

import cc.ccplay.booster.base.model.user.UserSystemMessage;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;

public class UserSystemMessageParam {

    public UserSystemMessage toModel(){
        UserSystemMessage msg = new UserSystemMessage();
        msg.setMsgTitle(this.title);
        msg.setMsgContent(this.content);
        msg.setContentType(this.contentType);
        msg.setObjectId(this.objectId);
        msg.setObjectTitle(this.objectTitle);
        msg.setPushMsg(this.pushMsg);
        return msg;
    }

    @NotNull
    @Param(remark = "标题")
    private String title;

    @NotNull
    @Param(remark = "正文")
    private String content;

    @NotNull
    @Param(remark = "内容类型")
    private Long contentType;

    @NotNull
    @Param(remark = "消息类型")
    private Long msgType;

    @Param(remark = "附加ID")
    private Long objectId;

    @Param(remark = "附加标题")
    private String objectTitle;

    @NotNull
    @Param(remark = "推送内容")
    private String pushMsg;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getContentType() {
        return contentType;
    }

    public void setContentType(Long contentType) {
        this.contentType = contentType;
    }

    public Long getMsgType() {
        return msgType;
    }

    public void setMsgType(Long msgType) {
        this.msgType = msgType;
    }

    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    public String getObjectTitle() {
        return objectTitle;
    }

    public void setObjectTitle(String objectTitle) {
        this.objectTitle = objectTitle;
    }

    public String getPushMsg() {
        return pushMsg;
    }

    public void setPushMsg(String pushMsg) {
        this.pushMsg = pushMsg;
    }
}
