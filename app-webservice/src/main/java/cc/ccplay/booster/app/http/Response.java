package cc.ccplay.booster.app.http;

import org.apache.http.Header;

public class Response {
	
	private int code;
	private String content;
	private Header[] headers;
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Header[] getHeaders() {
		return headers;
	}
	public void setHeaders(Header[] headers) {
		this.headers = headers;
	}


	public String getHeader(String key){
		for (int i = 0; i < headers.length; i++) {
			Header h = headers[i];
			if(key.equals(h.getName())){
				return h.getValue();
			}
		}
		return null;
	}
}
