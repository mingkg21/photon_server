package cc.ccplay.booster.app.exception;

import cc.ccplay.booster.app.core.Const;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.util.JsonUtil;

import java.util.HashMap;
import java.util.Map;

public class TokenExpireException extends GenericException {

    public TokenExpireException() {
        super("身份验证过期,请重新登录");
    }

    public TokenExpireException(String msg) {
        super(msg);
    }

    public TokenExpireException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public TokenExpireException(Throwable cause) {
        super(cause);
    }

    @Override
    public String toJsonMsg() {
        Map result = new HashMap();
        result.put("msg", "身份验证过期,请重新登录");
        result.put("success", false);
        return JsonUtil.toJson(result);
    }

    @Override
    public String[] getResponseHeader() {
        return new String[] {Const.HEADER_CEIPSTATE_TOKEN_EXPIRE};
    }

    @Override
    public boolean logStackTrace() {
        return false;
    }
}
