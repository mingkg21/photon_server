package cc.ccplay.booster.app.web.api;

import cc.ccplay.booster.app.web.BaseAppController;
import cc.ccplay.booster.base.annotation.InnerServerApi;
import cc.ccplay.booster.base.api.content.GamePackageMappingService;
import com.alibaba.dubbo.config.annotation.Reference;
import org.easyj.frame.validation.annotation.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@RequestMapping("/innerServerApi/pkgNameMapping")
@Controller
@InnerServerApi
public class ApiGamePackageMappingController extends BaseAppController{


    @Reference
    private GamePackageMappingService gamePackageMappingService;

    @RequestMapping("/getMappingList")
    @ResponseBody
    public void getMappingList(@NotNull long startId,@NotNull int size){
        super.renderJson(gamePackageMappingService.getMappingList(startId,size));
    }
}
