package cc.ccplay.booster.app.exception;

import org.easyj.frame.exception.GenericException;

public class NetworkException extends GenericException{

	public NetworkException(Throwable cause){
		this.cause = cause;
	}

	private Throwable cause = null;

	@Override
	public String getErrorMessage(){
		return "网络异常";
	}

	public Throwable getCause() {
		return cause;
	}

	public void setCause(Throwable cause) {
		this.cause = cause;
	}

}
