package cc.ccplay.booster.app.web;

import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.api.content.GiftInfoService;
import cc.ccplay.booster.base.model.content.GiftNumber;
import cc.ccplay.booster.base.test.annotation.ApiClass;
import cc.ccplay.booster.base.test.annotation.ApiMethod;
import cc.ccplay.booster.base.util.UserUtil;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/gift")
@ApiClass("游戏礼包")
public class GiftInfoController extends BaseAppController{

    @Reference
    private GiftInfoService giftInfoService;


    @NotNeedLogin
    @RequestMapping("/list")
    @ResponseBody
    @ApiMethod(value = "获取礼包")
    //@Cache(params = {"gameId"},cacheTimeout = 5 * 60)
    public void list(@NotNull @Param(remark = "游戏ID") long gameId){
        super.renderJson(giftInfoService.getGameGiftPage(super.getSystemParam(),gameId,UserUtil.getUserId()));
    }

    @RequestMapping("/receive")
    @ResponseBody
    @ApiMethod(value = "领取")
    public void receive(@NotNull @Param(remark = "礼包ID") long giftId){
        GiftNumber giftNumber = giftInfoService.receive(super.getSystemParam(), UserUtil.getUserId(),giftId);
        super.renderJson(giftNumber);
    }

    @RequestMapping("/tao")
    @ResponseBody
    @ApiMethod(value = "淘号")
    public void tao(@NotNull @Param(remark = "礼包ID") long giftId){
        GiftNumber giftNumber = giftInfoService.tao(super.getSystemParam(),UserUtil.getUserId(),giftId);
        super.renderJson(giftNumber);
    }

}
