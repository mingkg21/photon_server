package cc.ccplay.booster.app.web;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONArray;
import cc.ccplay.booster.base.annotation.Cache;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.api.content.GameCommentService;
import cc.ccplay.booster.base.api.user.UserAccountPermissionService;
import cc.ccplay.booster.base.core.AppHeaderInfo;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.game.GameCommentDto;
import cc.ccplay.booster.base.dto.content.game.GameCommentRecordDto;
import cc.ccplay.booster.base.model.adapter.CdnImageListAdapter;
import cc.ccplay.booster.base.model.content.GameComment;
import cc.ccplay.booster.base.model.content.GameCommentStarRange;
import cc.ccplay.booster.base.test.annotation.ApiClass;
import cc.ccplay.booster.base.test.annotation.ApiMethod;
import cc.ccplay.booster.base.util.UserUtil;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.StringUtil;
import org.easyj.frame.validation.annotation.MaxValue;
import org.easyj.frame.validation.annotation.MinValue;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/game/comment")
@ApiClass("游戏点评")
public class GameCommentController extends BaseAppController{

    @Reference
    private GameCommentService gameCommentService;

    @Reference
    private UserAccountPermissionService userAccountPermissionService;

    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/commentList")
    @ApiMethod(value = "评论列表",paging = true)
    public void commentList(@Param(remark = "游戏ID") @NotNull long gameId,
                            @Param(remark = "排序方式 0:时间 1:热门（默认时间）") Long orderType){
        GameComment searchParam = new GameComment();
        searchParam.setGameId(gameId);
        Page<GameCommentDto> page = gameCommentService.getCommentPage(super.getSystemParam(),searchParam,null,orderType);
        super.renderJson(page);
    }

    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/replyList")
    @ApiMethod(value = "回复列表",paging = true)
    public void replyList(@Param(remark = "评论ID") @NotNull long commentId,
                          @Param(remark = "排序方式 0:时间 1:热门（默认时间）") Long orderType){
        Page<GameCommentDto> page = gameCommentService.getReplyPage(super.getSystemParam(),commentId,orderType);
        super.renderJson(page);
    }


    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/getComment")
    @ApiMethod(value = "获取回复/评论信息")
    public void getComment(@Param(remark = "回复ID") @NotNull long commentId){
        super.renderJson(gameCommentService.getComment(super.getSystemParam(),commentId));
    }


    @ResponseBody
    @RequestMapping("/comment")
    @ApiMethod("评论")
    public void comment(@Param(remark = "游戏ID") @NotNull long gameId,
             @Param(remark = "星级(2,4,6,8,10)") @MinValue(2) @MaxValue(10) @NotNull long star,
             @Param(remark = "内容") @NotNull String content,
             @Param( remark = "图片（JSONArray格式）") String imgs){

        if(userAccountPermissionService.hasGameCommentPermission(UserUtil.getUserId()) == false){
            throw new GenericException("当前账号已被禁止评论");
        }

        if(star % 2 != 0){
            throw new GenericException("星级值有误，必须是2,4,6,8,10)其中一个");
        }
        GameComment gameComment = new GameComment();
        gameComment.setUserId(UserUtil.getUserId());
        gameComment.setGameId(gameId);
        gameComment.setContent(content);
        SystemParam param = super.getSystemParam();
        gameComment.setImgs(CdnImageListAdapter.createImage(toImages(imgs),null));
        AppHeaderInfo headerInfo = param.getAppHeaderInfo();
        if(headerInfo != null) {
            gameComment.setModelName(headerInfo.getModelName());
            gameComment.setOsVersion(headerInfo.getOsVersion());
        }
        GameComment comment = gameCommentService.comment(gameComment,star);
        super.renderJson(comment);
    }

    private JSONArray toImages(String imgs){
        if (StringUtil.isEmpty(imgs)) {
            return new JSONArray();
        }
        JSONArray arr = null;
        try {
            arr = JSONArray.parseArray(imgs);
            return arr;
        }catch (Exception e){
            throw new GenericException("[图片]数据格式有误");
        }
    }


    @ResponseBody
    @RequestMapping("/reply")
    @ApiMethod("回复")
    public void replyComment(@Param(remark = "评论或回复ID") @NotNull long commentId,
                             @Param(remark = "内容") @NotNull String content,
                             @Param(remark = "图片（JSONArray格式）") String imgs){

        GameComment reply = new GameComment();

        reply.setUserId(UserUtil.getUserId());
        reply.setContent(content);
        reply.setImgs(CdnImageListAdapter.createImage(toImages(imgs),null));
        SystemParam param = super.getSystemParam();
        AppHeaderInfo headerInfo = param.getAppHeaderInfo();
        if(headerInfo != null) {
            reply.setModelName(headerInfo.getModelName());
            reply.setOsVersion(headerInfo.getOsVersion());
        }

        GameComment comment = gameCommentService.reply(commentId,reply);
        super.renderJson(comment);
    }

    @ResponseBody
    @RequestMapping("/praise")
    @ApiMethod("点赞")
    public void praiseComment(@Param(remark = "评论或回复ID") @NotNull long commentId){
        gameCommentService.praiseComment(super.getSystemParam(),commentId);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/cancelPraise")
    @ApiMethod("取消点赞")
    public void cancelPraiseComment(@Param(remark = "评论或回复ID") @NotNull long commentId){
        gameCommentService.cancelPraiseComment(super.getSystemParam(),commentId);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/getCommentRecord")
    @ApiMethod("获取评论记录")
    public void getCommentRecord(@Param(remark = "游戏ID") @NotNull long gameId){
       GameCommentRecordDto dto = gameCommentService.getCommentRecord(super.getSystemParam(),gameId);
       super.renderJson(dto);
    }

    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/getStarRange")
    @ApiMethod("获取评星范围")
    public void getGameCommentStarRange(@Param(remark = "游戏ID") @NotNull long gameId){
        GameCommentStarRange starRange = gameCommentService.getGameCommentStarRange(super.getSystemParam(),gameId);
        super.renderJson(starRange);
    }

    @NotNeedLogin
    @ApiMethod(value = "获取热门置顶评论")
    @ResponseBody
    @RequestMapping("/getHotGameComment")
    @Cache(cacheTimeout = 10 * 60)
    public void getHotGameComment(){
        List<GameCommentDto> list = gameCommentService.getHotGameComment(5);
        super.renderJson(list);
    }
}
