package cc.ccplay.booster.app.core;

import cc.ccplay.booster.app.web.BaseAppController;
import com.alibaba.fastjson.JSONObject;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.easyj.frame.exception.AppException;
import org.easyj.frame.exception.ExceptionHandler;
import org.easyj.frame.exception.ExceptionUtil;
import org.easyj.frame.exception.PageNotFoundException;
import org.easyj.frame.util.ActionUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

public class SystemExceptionHandler implements ExceptionHandler {

    private Logger logger = Logger.getLogger(SystemExceptionHandler.class);

    static Map<String,StatusCode> statusCodeMap = new HashMap<>();
    static {
        statusCodeMap.put(ActionUtil.HEADER_CEIPSTATE_BIZ,StatusCode.BIZ_EXCETION);
        statusCodeMap.put(ActionUtil.HEADER_CEIPSTATE_ERROR,StatusCode.SYSTEM_ERROR);
        statusCodeMap.put(ActionUtil.HEADER_CEIPSTATE_NO_LOGIN,StatusCode.NO_LOGIN);
        statusCodeMap.put(ActionUtil.HEADER_CEIPSTATE_NO_PERMISSION,StatusCode.NOT_AUTHORITY);
        statusCodeMap.put(Const.HEADER_CEIPSTATE_TOKEN_EXPIRE,StatusCode.TOKEN_EXPIRE);
    }

    @Override
    public void resolveException(Exception exception, HttpServletResponse response, HttpServletRequest request) {
        AppException appEx = ExceptionUtil.handle(exception);

        //class cc.ccplay.booster.base.exception.PageNotFoundException

        if(appEx.getClass() == PageNotFoundException.class){
            response.setStatus(404);
            ActionUtil.render("application/json","{\"error\": \"Document not found\"}", appEx.getResponseHeader(), response);
            return;
        }

        try{
            doLog(appEx);
        }catch (Exception e){
            e.printStackTrace();
        }

        StatusCode statusCode = StatusCode.SYSTEM_ERROR;
        String[] headers = appEx.getResponseHeader();
        if (headers != null || headers.length > 0) {
            StatusCode code = statusCodeMap.get(headers[0]);
            if (code != null) {
                statusCode = code;
            }
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put(BaseAppController.PARAM_CODE,statusCode.getCode());
        jsonObject.put(BaseAppController.PARAM_MSG,statusCode.getOutputErrorMsg(appEx));
        ActionUtil.render("application/json",jsonObject.toJSONString(), appEx.getResponseHeader(), response);
    }

    private void doLog(AppException appEx){
        Exception e = (Exception)appEx;
        String msg = appEx.getErrorMessage()+":"+appEx.getStackTraceMessage();

        //tomcat catalina.out文件
        if(appEx.logStackTrace()){
            Throwable cause = appEx.getCause();
            if(cause != null){
                cause.printStackTrace();//控制台输出堆栈信息
            }else if(appEx instanceof Exception){
                e.printStackTrace();
            }
        }else{
            System.err.println(msg);
        }

        //日志文件
        String fullMessage = appEx.getFullMessage();
        if(appEx.logStackTrace() == false){
            e = null;		   //不输出堆栈
            fullMessage = msg;//输入堆栈第一行信息
        }
        Level level = appEx.getLogLevel();
        if(level == Level.DEBUG){
            logger.debug(fullMessage,e);
        }else if(level == Level.INFO){
            logger.info(fullMessage,e);
        }else if(level == Level.WARN){
            logger.warn(fullMessage,e);
        }else if(level == Level.ERROR){
            logger.error(fullMessage,e);
        }else if(level == Level.FATAL){
            logger.fatal(fullMessage,e);
        }
    }
}