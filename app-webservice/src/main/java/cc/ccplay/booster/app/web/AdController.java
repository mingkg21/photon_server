package cc.ccplay.booster.app.web;


import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.annotation.Cache;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.api.content.AdAreaItemService;
import cc.ccplay.booster.base.model.content.AdAreaItem;
import cc.ccplay.booster.base.test.annotation.ApiClass;
import cc.ccplay.booster.base.test.annotation.ApiMethod;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/content/ad")
@ApiClass("广告API")
public class AdController extends BaseAppController{

    @Reference
    private AdAreaItemService adAreaItemService;


    @ApiMethod(value="获取广告信息",paging = true)
    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/getPageByCode")
    @Cache(params = {"page","pageSize","code"},cacheTimeout = 10 * 60)
    public void getPageByCode(@Param(remark = "广告编码") @NotNull String code){
        Page<AdAreaItem> page = adAreaItemService.getPageByCode(super.getSystemParam(),code);
        super.renderJson(page);
    }

}
