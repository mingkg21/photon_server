package cc.ccplay.booster.app.web;

import cc.ccplay.booster.base.api.content.*;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import cc.ccplay.booster.base.annotation.Cache;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.core.AppHeaderInfo;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.game.GameInfoDto;
import cc.ccplay.booster.base.enums.ComplaintType;
import cc.ccplay.booster.base.model.content.Complaint;
import cc.ccplay.booster.base.test.annotation.ApiClass;
import cc.ccplay.booster.base.test.annotation.ApiMethod;
import cc.ccplay.booster.base.util.UserUtil;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.StringUtil;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collections;
import java.util.List;

@ApiClass("游戏API")
@RequestMapping("/game/info")
@Controller
public class GameController extends BaseAppController{

    @Reference
    private GameInfoService gameInfoService;

    @Reference
    private ComplaintService complaintService;

    @Reference
    private GameCategoryService gameCategoryService;

    @Reference
    private GameRelatedService gameRelatedService;

    @Reference
    private GamePackageMappingService gamePackageMappingService;

    @NotNeedLogin
    @ApiMethod(value = "通过标签获取游戏(V1.1)",paging = true)
    @ResponseBody
    @RequestMapping("/getGamesByTag")
    @Cache(params = {"page","pageSize","tagId","categoryId","orderType"},cacheTimeout = 2 * 60)
    public void getGamesByTag(@NotNull @Param(remark = "标签ID") long tagId,
                              @Param(remark = "排序方式,releaseTime按发布时间倒序,默认按热度排序") String orderType,
                              @Param(remark = "游戏分类ID") Long categoryId){
        Page<GameInfoDto> games = gameInfoService.getGameByTag(super.getSystemParam(),tagId,null,categoryId,orderType);
        super.renderJson(games);
    }

    @NotNeedLogin
    @ApiMethod(value = "通过标签获取游戏(Since.V1.2)",paging = true)
    @ResponseBody
    @RequestMapping("/getGamesByTagV2")
    @Cache(params = {"page","pageSize","mainTagId","tagId","orderType"},cacheTimeout = 2 * 60)
    public void getGamesByTagV2(@NotNull @Param(remark = "主标签ID") long mainTagId,
                              @Param(remark = "标签ID") Long tagId,
                              @Param(remark = "排序方式,releaseTime按发布时间倒序,默认按热度排序") String orderType,
                              @Param(remark = "游戏分类ID") Long categoryId){
//        Page<GameInfoDto> page = null;
//        if(tagId != null) {
//            page = gameInfoService.getGameByTag(super.getSystemParam(), tagId, categoryId, orderType);
//        }else {
        Page<GameInfoDto> page = gameInfoService.getGameByTag(super.getSystemParam(), mainTagId,tagId, categoryId, orderType);
//        }
        super.renderJson(page);
    }



    @NotNeedLogin
    @ApiMethod(value = "搜索游戏",paging = true)
    @ResponseBody
    @RequestMapping("/search")
    @Cache(params = {"page","pageSize","name"},cacheTimeout = 10 * 60)
    public void search(@NotNull @Param(remark = "游戏名称") String name){
        Page<GameInfoDto> games = gameInfoService.searchGames(super.getSystemParam(),name);
        super.renderJson(games);
    }



    @NotNeedLogin
    @ApiMethod(value = "游戏详情")
    @ResponseBody
    @RequestMapping("/getGameInfo")
    @Cache(params = {"gameId"},cacheTimeout = 10 * 60)
    public void getGameInfo(@Param(remark = "游戏ID") @NotNull long gameId){
        GameInfoDto gameInfoDto = gameInfoService.getGameInfoDto(super.getSystemParam(),gameId);
        if(gameInfoDto == null){
            throw new GenericException("游戏不存在");
        }
        super.renderJson(gameInfoDto);
    }

    @ApiMethod(value = "关注游戏")
    @ResponseBody
    @RequestMapping("/follow")
    public void follow(@Param(remark = "游戏ID") @NotNull long gameId){
        gameInfoService.follow(super.getSystemParam(),gameId);
        super.renderSuccess();
    }


    @ApiMethod(value = "点赞")
    @ResponseBody
    @RequestMapping("/praise")
    public void praise(@Param(remark = "游戏ID") @NotNull long gameId){
        gameInfoService.praise(super.getSystemParam(),gameId);
        super.renderSuccess();
    }


    @ApiMethod(value = "取消关注游戏")
    @ResponseBody
    @RequestMapping("/cancelFollow")
    public void cancelFollow(@Param(remark = "游戏ID") @NotNull long gameId){
        gameInfoService.cancelFollow(super.getSystemParam(),gameId);
        super.renderSuccess();
    }


    @NotNeedLogin
    @ApiMethod(value = "获取同类型游戏",paging = true)
    @ResponseBody
    @RequestMapping("/getSamllTypeGames")
    @Cache(params = {"page","pageSize","gameId"})
    public void getSamllTypeGames(@Param(remark = "游戏ID") @NotNull long gameId){
        Page<GameInfoDto> games = gameInfoService.getSameTypeGames(super.getSystemParam(),gameId);
        super.renderJson(games);
    }


    @NotNeedLogin
    @ApiMethod(value = "获取相关游戏")
    @ResponseBody
    @RequestMapping("/getRelatedGames")
    @Cache(params = {"gameId"})
    public void getRelatedGames(@Param(remark = "游戏ID") @NotNull long gameId){
        List<GameInfoDto> list = gameRelatedService.getRelatedList(gameId,10);
        super.renderJson(list);
    }


    @NotNeedLogin
    @ApiMethod(value = "获取应用更新日志",paging = true)
    @ResponseBody
    @RequestMapping("/getUpdateLogs")
    @Cache(params = {"page","pageSize","gameId"})
    public void getUpdateLogs(@Param(remark = "游戏ID") @NotNull long gameId){
        super.renderJson(gameInfoService.getUpdateLogs(super.getSystemParam(),gameId));
    }

    @NotNeedLogin
    @ApiMethod(value = "获取举报类型")
    @ResponseBody
    @RequestMapping("/getComplaintTypes")
    @Cache(cacheTimeout = 12 * 60 * 60)
    public void getComplaintTypes(){
        ComplaintType [] cts = ComplaintType.values();
        JSONArray arr = new JSONArray();
        for (int i = 0; i < cts.length; i++) {
            ComplaintType ct = cts[i];
            JSONObject item = new JSONObject();
            item.put("id",ct.getValue());
            item.put("name",ct.getName());
            arr.add(item);
        }
        super.renderJson(arr);
    }

    @ApiMethod(value = "举报")
    @ResponseBody
    @RequestMapping("/complaint")
    public void complaint(@NotNull @Param(remark = "游戏ID") long gameId,
                          @NotNull @Param(remark = "投诉类型ID") int complaintId,
                          @NotNull @Param(remark = "内容") String content,
                          @Param(remark = "联系方式") String concatType,
                          @Param(remark = "联系号码") String concatNumber
                       ){


        ComplaintType ct = ComplaintType.getByValue(complaintId);

        if(ct == null){
            throw new GenericException("投诉类型有误");
        }

//        if(complaintId == ComplaintType.OTHER_VALUE && StringUtil.isEmpty(content)){
//            throw new GenericException("投诉类型为[其他]时,内容不能为空");
//        }

        SystemParam param = super.getSystemParam();
        AppHeaderInfo headerInfo = param.getAppHeaderInfo();

        Complaint complaint = new Complaint();
        complaint.setGameId(gameId);
        complaint.setModelName(headerInfo.getModelName());
        complaint.setTypeId(new Long(complaintId));
        complaint.setTypeDesc(ct.getName());
        complaint.setOsVersion(headerInfo.getOsVersion());
        complaint.setComplaintContent(content);
        complaint.setUserId(UserUtil.getUserId());
        if(StringUtil.isNotEmpty(concatNumber)) {
            complaint.setConcatType(concatType);
            complaint.setConcatNumber(concatNumber);
        }
        this.complaintService.save(param,complaint);
        super.renderSuccess();
    }


    @NotNeedLogin
    @ApiMethod(value = "获取专区游戏",paging = true)
    @ResponseBody
    @RequestMapping("/getAreaGame")
    @Cache(params = {"page","pageSize","code","gameName"},cacheTimeout = 10 * 60)
    public void getAreaGame(@NotNull @Param(remark = "专区编码") String code,@Param(remark = "游戏名")String gameName){
        super.renderJson(gameInfoService.getPageByAreaCode(super.getSystemParam(),code,gameName));
    }


    @NotNeedLogin
    @ApiMethod(value = "通过包名获取最新版本信息",paging = true)
    @ResponseBody
    @RequestMapping("/getLastestVersionInfoByPackageName")
    //@Cache(params = {"page","pageSize","packageNames","needTags"},cacheTimeout = 30 * 60)
    public void getLastestVersionInfoByPackageName(@NotNull @Param(remark = "包名(json数组)") String packageNames,
                                                   @Param(remark = "是否需要读取tag") Boolean needTags){
        List<String> names = JSONArray.parseArray(packageNames,String.class);
        if(names.size() == 0){
            super.renderJson(Collections.emptyList());
            return;
        }


        super.renderJson(gamePackageMappingService.getMappingGame(names));


//        List<GameInfoDto> games = gameInfoService.getLastestVersionInfoByPackageName(super.getSystemParam(),names,needTags);
//        super.renderJson(games);
    }


    @NotNeedLogin
    @ApiMethod(value = "获取前12个游戏分类")
    @ResponseBody
    @RequestMapping("/getTopCategory")
    @Cache(cacheTimeout = 10 * 60)
    public void getTopCategory(){
        super.renderJson(gameCategoryService.getTopCategory(12));
    }



    @NotNeedLogin
    @ApiMethod(value = "通过分类获取游戏",paging = true)
    @ResponseBody
    @RequestMapping("/getGameByCategory")
    @Cache(cacheTimeout = 10 * 60,params = {"page","pageSize","categoryId"})
    public void getGameByCategory(@NotNull @Param(remark = "分类ID") long categoryId){
       Page<GameInfoDto> page = gameInfoService.getGameByCategory(super.getSystemParam(),categoryId);
       super.renderJson(page);
    }

    @ApiMethod(value = "获取游戏关注状态",paging = true)
    @ResponseBody
    @RequestMapping("/getGameFollowStatus")
    public void getGameFollowStatus(@NotNull @Param(remark = "应用ID") long gameId){
        Long userId = UserUtil.getUserId();
        boolean status = gameInfoService.getGameFollowStatus(userId,gameId);
        super.renderJson(status);
    }
}
