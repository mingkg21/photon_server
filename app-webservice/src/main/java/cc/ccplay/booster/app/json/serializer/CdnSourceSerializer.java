package cc.ccplay.booster.app.json.serializer;

import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.ObjectSerializer;
import com.alibaba.fastjson.serializer.SerializeWriter;
import cc.ccplay.booster.base.model.adapter.CdnSource;

import java.io.IOException;
import java.lang.reflect.Type;

public class CdnSourceSerializer implements ObjectSerializer {

    @Override
    public void write(JSONSerializer serializer, Object object, Object fieldName, Type fieldType, int features) throws IOException {
        SerializeWriter out = serializer.getWriter();
        if (object.getClass() != CdnSource.class) {
            return;
        }
        CdnSource cdnSource = (CdnSource)object;
        out.write("\"" + cdnSource.getCndSrc() + "\"");
    }
}
