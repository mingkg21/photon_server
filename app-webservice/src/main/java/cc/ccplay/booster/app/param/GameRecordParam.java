package cc.ccplay.booster.app.param;

import cc.ccplay.booster.base.model.content.SaveRecord;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;

public class GameRecordParam {

    public SaveRecord toModel(){
        SaveRecord record = new SaveRecord();
        record.setUrl(this.url);
        record.setVersionName(this.versionName);
        record.setVersionCode(this.versionCode);
        record.setId(this.recordId);
        record.setGameId(this.gameId);
        record.setFileSize(this.fileSize);
        record.setName(this.name);
        record.setRemark(this.remark);
        return record;
    }


    @Param(remark = "记录ID")
    private Long recordId;

    @NotNull
    @Param(remark = "版本号")
    private Long versionCode;

    @NotNull
    @Param(remark = "版本名")
    private String versionName;

    @NotNull
    @Param(remark = "存档地址")
    private String url;

    @NotNull
    @Param(remark = "文件大小")
    private Long fileSize;

    @NotNull
    @Param(remark = "游戏ID")
    private Long gameId;


    @NotNull
    @Param(remark = "存档名字")
    private String name;

    @Param(remark = "备注")
    private String remark;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public Long getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(Long versionCode) {
        this.versionCode = versionCode;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }
}
