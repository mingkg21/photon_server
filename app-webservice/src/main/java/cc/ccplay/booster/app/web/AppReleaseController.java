package cc.ccplay.booster.app.web;


import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.annotation.Cache;
import cc.ccplay.booster.base.annotation.NoSignature;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.api.content.AppReleaseService;
import cc.ccplay.booster.base.model.adapter.CdnSource;
import cc.ccplay.booster.base.model.content.AppRelease;
import cc.ccplay.booster.base.test.annotation.ApiClass;
import cc.ccplay.booster.base.test.annotation.ApiMethod;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;

@Controller
@RequestMapping("/content/release")
@ApiClass("版本API")
public class AppReleaseController extends BaseAppController {

    @Reference
    private AppReleaseService appReleaseService;

    @ApiMethod("获取最新版本")
    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/getLastestVersion")
    @Cache(params = {},cacheTimeout = 10 * 60)
    public void getLastestVersion(){
        AppRelease release = appReleaseService.getLastestVersion();
        if(release != null) {
            CdnSource cdnSource = new CdnSource("/content/release/download");
            cdnSource.setCndSrc("/content/release/download");
            release.setDownloadUrl(cdnSource);
        }
        super.renderJson(release);
    }

    @NotNeedLogin
    @NoSignature
    @RequestMapping("/download")
    public void download(HttpServletResponse response) throws IOException {
        AppRelease release = appReleaseService.getLastestVersion();
        if(release == null){
            response.setStatus(404);
            super.renderJson("{\"error\": \"Document not found\"}");
            return;
        }

        CdnSource source = release.getDownloadUrl();
        if(source == null){
            response.setStatus(404);
            super.renderJson("{\"error\": \"Document not found\"}");
            return;
        }
        String url = source.getCndSrc();
        String downloadUrl = URLEncoder.encode(url, "utf-8").replaceAll("\\+", "%20");
        // 编码之后的路径中的“/”也变成编码的东西了 所有还有将其替换回来 这样才是完整的路径
        downloadUrl = downloadUrl.replaceAll("%3A", ":").replaceAll("%2F", "/");
        response.sendRedirect(response.encodeRedirectURL(downloadUrl));
    }
}
