package cc.ccplay.booster.app.web;

import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.annotation.Cache;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.api.content.AppTopicService;
import cc.ccplay.booster.base.dto.content.AppTopicDto;
import cc.ccplay.booster.base.test.annotation.ApiClass;
import cc.ccplay.booster.base.test.annotation.ApiMethod;
import org.easyj.frame.jdbc.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/content/appTopic")
@ApiClass("精选专题")
public class AppTopicController extends BaseAppController {


    @Reference
    private AppTopicService appTopicService;

    @ApiMethod(value = "获取专题", paging = true)
    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/getPage")
    @Cache(params = {"page", "pageSize"}, cacheTimeout = 10 * 60)
    public void getPage() {
        Page<AppTopicDto> page = appTopicService.getDtoPage(super.getSystemParam());
        super.renderJson(page);
    }


}
