package cc.ccplay.booster.app.json;

import cc.ccplay.booster.app.json.serializer.CdnImageListSerializer;
import cc.ccplay.booster.app.json.serializer.CdnImageSerializer;
import cc.ccplay.booster.app.json.serializer.CdnSourceSerializer;
import com.alibaba.fastjson.serializer.SerializeConfig;
import cc.ccplay.booster.base.model.adapter.CdnImage;
import cc.ccplay.booster.base.model.adapter.CdnImageList;
import cc.ccplay.booster.base.model.adapter.CdnSource;


public  class JSONConfig {
    private static SerializeConfig globalInstance = SerializeConfig.getGlobalInstance();

    static {
        globalInstance.put(CdnSource.class,new CdnSourceSerializer());
        globalInstance.put(CdnImage.class,new CdnImageSerializer());
        globalInstance.put(CdnImageList.class,new CdnImageListSerializer());
    }
}
