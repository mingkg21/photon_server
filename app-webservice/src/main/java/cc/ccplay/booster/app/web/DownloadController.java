package cc.ccplay.booster.app.web;

import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.annotation.NoSignature;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.api.content.GameInfoService;
import cc.ccplay.booster.base.api.content.GameVersionService;
import cc.ccplay.booster.base.api.log.AppDownloadCompleteLogService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.content.GameVersion;
import cc.ccplay.booster.base.model.log.AppDownloadCompleteRecord;
import cc.ccplay.booster.base.test.annotation.ApiClass;
import cc.ccplay.booster.base.test.annotation.ApiMethod;
import org.easyj.frame.exception.PageNotFoundException;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;

@Controller
@RequestMapping("/download")
@ApiClass("应用下载")
public class DownloadController extends BaseAppController{

    @Reference
    private GameInfoService gameInfoService;

    @Reference
    private GameVersionService gameVersionService;

    @Reference
    private AppDownloadCompleteLogService appDownloadCompleteLogService;

    @NotNeedLogin
    @NoSignature
    @RequestMapping(value = "/apk/version/{versionId}")
    public void downloadApk(@PathVariable("versionId") Long versionId, HttpServletResponse rsp) throws IOException {
        boolean first = true;
        String firstStr = super.getString("first");
        if("0".equals(firstStr)){
            first = false;
        }
        if(versionId == null){
            throw new PageNotFoundException();
        }
        String url = gameInfoService.createDownloadUrl(super.getSystemParam(),versionId,first);
        if(url == null){
            throw new PageNotFoundException();
        }
        String downloadUrl = URLEncoder.encode(url, "utf-8").replaceAll("\\+", "%20");
        // 编码之后的路径中的“/”也变成编码的东西了 所有还有将其替换回来 这样才是完整的路径
        downloadUrl = downloadUrl.replaceAll("%3A", ":").replaceAll("%2F", "/");
        rsp.sendRedirect(rsp.encodeRedirectURL(downloadUrl));
    }

    @ResponseBody
    @RequestMapping("/complete")
    @NotNeedLogin
    @ApiMethod("下载完成")
    public void complete(@Param(remark = "版本id") @NotNull Long versionId,
                         @NotNull @Param(remark = "1:普通应用 2:18汉化助手") long type){
        if(type == 1){
            SystemParam param = super.getSystemParam();
            GameVersion gameVersion = gameVersionService.get(versionId);
            AppDownloadCompleteRecord record = new AppDownloadCompleteRecord();
            record.setBaseLogValue(param);
            record.setVersionId(versionId);
            if(gameVersion != null){
                record.setAppId(gameVersion.getGameId());
                record.setVersionName(gameVersion.getVersionName());
            }
            appDownloadCompleteLogService.save(record);
        }
        super.renderSuccess();
    }
}
