package cc.ccplay.booster.app.http;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import cc.ccplay.booster.app.exception.HttpResponseException;
import cc.ccplay.booster.app.exception.NetworkException;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

public abstract class RequestProxy {

	/**
	 * 浏览器
	 */
	public static final String USER_AGENT = "Mozilla/4.0 (compatible; MSIE 8.0; Windows XP)";

	private Logger logger = Logger.getLogger(RequestProxy.class);

	private HttpRequestBase requestBase = null;

	private PostType postType = null;
	
	public Response curl(String url, PostType type){
		Response res = new Response();
		postType = type;
		HttpEntity httpEntity = null;
		try {
			HttpResponse httpResponse = null;
			if (type == PostType.GET) {
				httpResponse = this.doGet(url);
			} else if (type == PostType.PUT) {
				httpResponse = this.doPut(url);
			} else {
				httpResponse = this.doPost(url);
			}
			StatusLine statusLine = httpResponse.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			res.setCode(statusCode);
			httpEntity = httpResponse.getEntity();
			String content = getContent(httpEntity);
			res.setContent(content);
			res.setHeaders(httpResponse.getAllHeaders());
			return res;
		} catch (URISyntaxException e) {
			throw new HttpResponseException(404, "URL[" + url + "]异常");
		} catch (HttpHostConnectException | ConnectTimeoutException | UnknownHostException e) {
			throw new HttpResponseException(404, e.getClass().getName() + ":" + e.getMessage());
		} catch (IOException e) {
			logger.error("链接异常 url=" + url, e);
			throw new NetworkException(e);
		} finally {
			if (requestBase != null) {
				requestBase.abort();
			}
			if (httpEntity != null) {
				try {
					EntityUtils.consume(httpEntity);
				} catch (IOException e) {

				}
			}
		}
	}
	
	public String doRequest(String url, PostType type) {
		Response res = curl(url, type);
		int statusCode = res.getCode();
		String content = res.getContent();
		if (statusCode == 200) {
			return content;
		} else if (statusCode > 500) {
			throw new HttpResponseException(statusCode, content);
		}
		throw new HttpResponseException(statusCode, content);
	}

	private String getContent(HttpEntity httpEntity) throws ParseException, IOException {
		if (httpEntity == null) {
			return "";
		}
		return EntityUtils.toString(httpEntity, "UTF-8");
	}

	public String doRequest(String url) throws ClientProtocolException, IOException, URISyntaxException {
		return doRequest(url, PostType.GET);
	}

	private HttpResponse doGet(String url) throws ClientProtocolException, IOException, URISyntaxException {
		HttpGet httpGet = new HttpGet(url);

		// 请求头部信息
		List<Header> headerList = this.getHeaders();
		if (headerList != null && headerList.size() > 0) {
			httpGet.setHeaders(headerList.toArray(new Header[] {}));
		}
		// 请求参数
		List<NameValuePair> parameters = this.getParameters();
		if (parameters != null && parameters.size() > 0) {
			String paramsStr = EntityUtils.toString(new UrlEncodedFormEntity(parameters));
			httpGet.setURI(new URI(httpGet.getURI().toString() + "?" + paramsStr));
		}
		logger.info("url=" + httpGet.getURI());
		requestBase = httpGet;
		HttpClient httpClient = getClient();
		
		return httpClient.execute(httpGet);
	}

	private HttpResponse doPut(String url) throws ClientProtocolException, IOException {
		HttpPut httpPut = new HttpPut(url);
		// 请求头部信息
		List<Header> headerList = this.getHeaders();
		if (headerList != null && headerList.size() > 0) {
			httpPut.setHeaders(headerList.toArray(new Header[] {}));
		}
		String content = this.getXml();
		logger.info(content);
		StringEntity entity = new StringEntity(content, HTTP.UTF_8);
		httpPut.setEntity(entity);
		requestBase =httpPut;
		HttpClient httpClient = getClient();
		return httpClient.execute(httpPut);
	}

	private HttpResponse doPost(String url) throws ClientProtocolException, IOException {
		HttpPost httpPost = new HttpPost(url);
		// 请求头部信息
		List<Header> headerList = this.getHeaders();
		if (headerList != null && headerList.size() > 0) {
			httpPost.setHeaders(headerList.toArray(new Header[] {}));
		}
		if (this.postType == PostType.POST_XML) {
			String xml = this.getXml();
			if (xml != null) {
				StringEntity entity = new StringEntity(xml, HTTP.UTF_8);
				httpPost.setEntity(entity);
			}
		} else {
			// 请求参数
			List<NameValuePair> parameters = this.getParameters();
			if (parameters != null && parameters.size() > 0) {
				HttpEntity entity = new UrlEncodedFormEntity(parameters, HTTP.UTF_8);
				httpPost.setEntity(entity);
			}
//			logger.info("parameters=" + JSON.toJSONString(parameters));
		}
//		logger.info("url=" + httpPost.getURI());
		requestBase = httpPost;
		HttpClient httpClient = getClient();
		return httpClient.execute(httpPost);
	}

	public List<Header> getHeaders() {
		List<Header> headerList = new ArrayList<Header>();
		headerList.add(new BasicHeader("User-Agent", USER_AGENT));
		headerList.add(new BasicHeader("Connection", "close")); // 短链接
		headerList.add(new BasicHeader("Accept-Encoding", "identity")); // 短链接
		return headerList;
	}

	/**
	 * 请求参数
	 * 
	 * @return
	 */
	public List<NameValuePair> getParameters() {
		return null;
	}

	public String getXml() {
		return null;
	}
	
	public HttpClient getHttpClient(){
		return null;
	}
	
	private HttpClient getClient(){
		HttpClient ht = this.getHttpClient();
		if(ht != null){
			return ht;
		}
		return HttpManager.getHttpClient();
	}
}
