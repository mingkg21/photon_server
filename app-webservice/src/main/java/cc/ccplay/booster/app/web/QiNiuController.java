package cc.ccplay.booster.app.web;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONArray;
import cc.ccplay.booster.base.api.content.QiNiuService;
import cc.ccplay.booster.base.dto.QiNiuAuth;
import cc.ccplay.booster.base.test.annotation.ApiClass;
import cc.ccplay.booster.base.test.annotation.ApiMethod;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping("/upload")
@ApiClass("七牛授权")
public class QiNiuController extends BaseAppController{

    @Reference
    private QiNiuService qiNiuService;

//    @ResponseBody
//    @RequestMapping("/getUploadImageToken")
//    @ApiMethod("获取上传图片授权")
//    public void getUploadImageToken(@NotNull @Param(remark = "文件名") String fileName){
//        QiNiuAuth auth = qiNiuService.getUploadImageToken(super.getSystemParam(),fileName);
//        super.renderJson(auth);
//    }

    @ResponseBody
    @RequestMapping("/getManyUploadImageTokens")
    @ApiMethod("获取多个上传图片授权")
    public void getManyUploadImageTokens(@NotNull @Param(remark = "文件名（JsonArray）") String fileNames){
        JSONArray arr = null;
        try {
            arr = JSONArray.parseArray(fileNames);
        }catch (Exception e){
            throw new GenericException("参数[fileNames]格式有误");
        }
        int size = arr.size();
        if(size == 0){
            super.renderJson(Collections.emptyList());
            return;
        }
        List<QiNiuAuth> list = new ArrayList<>();
        for (int i = 0; i < arr.size(); i++) {
            QiNiuAuth auth = qiNiuService.getUploadImageToken(super.getSystemParam(),arr.getString(i));
            list.add(auth);
        }
        super.renderJson(list);
    }

    @ResponseBody
    @RequestMapping("/getUploadVideoToken")
    @ApiMethod("获取上传视频授权")
    public void getUploadVideoToken(@NotNull @Param(remark = "文件名") String fileName){
        QiNiuAuth auth = qiNiuService.getUploadVideoToken(super.getSystemParam(),fileName);
        super.renderJson(auth);
    }

//    @ResponseBody
//    @RequestMapping("/getUploadFileToken")
//    @ApiMethod("获取上传普通文件授权")
//    public void getUploadFileToken(@NotNull @Param(remark = "文件名") String fileName){
//        QiNiuAuth auth = qiNiuService.getUploadFileToken(getSystemParam(),fileName);
//        super.renderJson(auth);
//    }


    @ResponseBody
    @RequestMapping("/getManyUploadFileTokens")
    @ApiMethod("获取上传普通文件授权")
    public void getUploadFileTokens(@NotNull @Param(remark = "文件名（JsonArray）") String fileNames){
        JSONArray arr = null;
        try {
            arr = JSONArray.parseArray(fileNames);
        }catch (Exception e){
            throw new GenericException("参数[fileNames]格式有误");
        }
        int size = arr.size();
        if(size == 0){
            super.renderJson(Collections.emptyList());
            return;
        }
        List<QiNiuAuth> list = new ArrayList<>();
        for (int i = 0; i < arr.size(); i++) {
            QiNiuAuth auth = qiNiuService.getUploadFileToken(super.getSystemParam(),arr.getString(i));
            list.add(auth);
        }
        super.renderJson(list);
    }
}
