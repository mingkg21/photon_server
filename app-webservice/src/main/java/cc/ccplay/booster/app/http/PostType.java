package cc.ccplay.booster.app.http;

public enum PostType {
	POST,
	GET,
	PUT,
	DELETE,
	POST_XML
}
