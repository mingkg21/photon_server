package cc.ccplay.booster.app.web;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.api.content.GameRecruitFeedbackService;
import cc.ccplay.booster.base.api.content.GameRecruitService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.game.GameRecruitDto;
import cc.ccplay.booster.base.dto.content.game.GameRecruitFeedbackDto;
import cc.ccplay.booster.base.model.content.GameRecruitAnswerRecord;
import cc.ccplay.booster.base.test.annotation.ApiClass;
import cc.ccplay.booster.base.test.annotation.ApiMethod;
import cc.ccplay.booster.base.util.UserUtil;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.exception.ParameterException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.StringUtil;
import org.easyj.frame.validation.annotation.MaxLength;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/game/recruit")
@ApiClass("玩家招募")
public class GameRecruitController extends BaseAppController{

    @Reference
    private GameRecruitService gameRecruitService;

    @Reference
    private GameRecruitFeedbackService gameRecruitFeedbackService;

    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/list")
    @ApiMethod(value = "游戏列表",paging = true)
    public void list(){
        Page<GameRecruitDto> page = gameRecruitService.getDtoPage(super.getSystemParam());
        super.renderJson(page);
    }

    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/getRecruitInfo")
    @ApiMethod(value = "招募详情")
    public void getRecruitInfo(@Param("招募ID") @NotNull long recruitId){
        GameRecruitDto dto = gameRecruitService.getGameRecruitDtoForApp(super.getSystemParam(),recruitId);
        if(dto == null){
            throw new GenericException("招募信息不存在");
        }
        super.renderJson(dto);
    }

    @ResponseBody
    @RequestMapping("/takeTask")
    @ApiMethod(value = "领取招募任务")
    public void takeTask(@Param("招募ID") @NotNull long recruitId){
        gameRecruitService.takeTask(super.getSystemParam(),recruitId);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/saveAnswers")
    @ApiMethod(value = "保存答题")
    public void saveAnswers(@Param("招募ID") @NotNull  long recruitId,
                            @Param("答题列表,JSON结构：[{questionId:1,answerId:2,questionText:'',answerText:''},{}...]") @NotNull String answers){
        List<GameRecruitAnswerRecord> answerRecords = new ArrayList<>();
        JSONArray array = null;
        try{
            array = JSONArray.parseArray(answers);
        }catch (JSONException e){
            e.printStackTrace();
            throw new ParameterException("answers值结构有误");
        }

        int size = array.size();
        if(size == 0){
            throw new GenericException("答题内容不能为空");
        }
        for (int i = 0; i < size; i++) {
            JSONObject item = array.getJSONObject(i);
            GameRecruitAnswerRecord record = new GameRecruitAnswerRecord();
            Long answerId = item.getLong("answerId");
            Long questionId = item.getLong("questionId");
            String questionText = item.getString("questionText");
            String answerText = item.getString("answerText");
            if(answerId == null){
                throw new ParameterException("字段[answerId]不能为空");
            }

            if(questionId == null){
                throw new ParameterException("字段[questionId]不能为空");
            }

            if(StringUtil.isEmpty(questionText)){
                throw new ParameterException("字段[questionText]不能为空");
            }

            if(StringUtil.isEmpty(answerText)){
                throw new ParameterException("字段[answerText]不能为空");
            }
            record.setAnswerId(answerId);
            record.setQuestionId(questionId);
            record.setAnswerText(answerText);
            record.setQuestionText(questionText);
            record.setRecruitId(recruitId);
            record.setUserId(UserUtil.getUserId());
            answerRecords.add(record);
        }
        gameRecruitService.saveAnswers(super.getSystemParam(),recruitId,answerRecords);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/feedback")
    @ApiMethod(value = "反馈")
    public void feedback(@Param("招募ID") @NotNull long recruitId, @NotNull @MaxLength(30) @Param("标题") String title, @NotNull @Param("内容") String content){
        gameRecruitFeedbackService.feedback(super.getSystemParam(),recruitId,title,content);
        super.renderSuccess();
    }

    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/getMyFeedback")
    @ApiMethod(value = "我的反馈")
    public void getMyFeedback(@Param("招募ID") @NotNull long recruitId){
        GameRecruitFeedbackDto dto = null;
        SystemParam param = super.getSystemParam();
        if(param.getUserId() != null){
            dto = gameRecruitFeedbackService.getMyFeedback(param,recruitId);
        }
        super.renderJson(dto);
    }

    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/getFeedbackList")
    @ApiMethod(value = "反馈列表",paging = true)
    public void getFeedbackList(@Param("招募ID") @NotNull long recruitId){
        Page<GameRecruitFeedbackDto> page = gameRecruitFeedbackService.getDtoPage(super.getSystemParam(),recruitId);
        super.renderJson(page);
    }

}
