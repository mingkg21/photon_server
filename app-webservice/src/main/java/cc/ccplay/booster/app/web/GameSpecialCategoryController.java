package cc.ccplay.booster.app.web;

import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.annotation.Cache;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.api.content.GameSpecialCategoryService;
import cc.ccplay.booster.base.test.annotation.ApiClass;
import cc.ccplay.booster.base.test.annotation.ApiMethod;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("/game/specialCategory")
@ApiClass("游戏综合分类")
@Controller
public class GameSpecialCategoryController extends BaseAppController {

    @Reference
    private GameSpecialCategoryService gameSpecialCategoryService;

    @NotNeedLogin
    @RequestMapping("/getList")
    @ResponseBody
    @ApiMethod(value = "获取分类")
    @Cache(cacheTimeout = 1 * 60 * 60)
    public void getList(){
        super.renderJson(gameSpecialCategoryService.getList(3));
    }

    @NotNeedLogin
    @RequestMapping("/getTagList")
    @ResponseBody
    @ApiMethod(value = "获取标签",paging = true)
    @Cache(params = {"page","pageSize","categoryId"},cacheTimeout = 10 * 60)
    public void getTagList(@NotNull @Param(remark = "综合分类ID") long categoryId){
        super.renderJson(gameSpecialCategoryService.getTagList(super.getSystemParam(),categoryId));
    }
}
