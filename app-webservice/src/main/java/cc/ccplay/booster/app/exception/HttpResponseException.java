package cc.ccplay.booster.app.exception;

import org.easyj.frame.exception.GenericException;
import org.easyj.frame.util.JsonUtil;
import org.easyj.frame.util.StringUtil;

import java.util.HashMap;
import java.util.Map;


public class HttpResponseException extends GenericException{
	
	private int code;
	
	private String content;
	
    public String toJsonMsg() {
    	Map result = new HashMap(); 
		result.put("msg", "网络响应异常["+this.code+"]");
		result.put("success", false);
	    return JsonUtil.toJson(result);
    }
	
	public HttpResponseException(int code){
		this.code = code;
	}
	
	public HttpResponseException(int code,String content){
		this.code = code;
		this.content = content;
	}
	
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getErrorMessage() {
		return "[404]网络响应异常";
	}

}
