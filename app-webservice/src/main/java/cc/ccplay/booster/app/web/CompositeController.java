package cc.ccplay.booster.app.web;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import cc.ccplay.booster.app.core.Const;
import cc.ccplay.booster.app.http.PostType;
import cc.ccplay.booster.app.http.RequestProxy;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.core.AppHeaderInfo;
import cc.ccplay.booster.base.core.SystemParam;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import org.easyj.frame.exception.ParameterException;
import org.easyj.frame.util.StringUtil;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.CountDownLatch;


@Controller
public class CompositeController extends BaseAppController{

    private static final ExecutorService executorService = Executors.newCachedThreadPool();


    @NotNeedLogin
    @RequestMapping("/composite")
    @ResponseBody
    public void composite(HttpServletRequest request,@Param("param") @NotNull String json) throws InterruptedException {
        JSONArray arr = null;
        try {
            arr = JSONArray.parseArray(json);
        }catch(Exception e){
            throw new ParameterException("请求数据格式有误");
        }
        int size = arr.size();
        if(size == 0){
            throw new ParameterException("请求参数为空");
        }
        String baseUrl = "http://127.0.0.1:"+request.getServerPort()+request.getContextPath();
        CountDownLatch latch = new CountDownLatch(size);
        SystemParam systemParam = super.getSystemParam();
        String [] result = new String [size];
        for (int i = 0; i < size; i++) {
            JSONObject item = arr.getJSONObject(i);
            String url = item.getString("url");
            JSONObject data = item.getJSONObject("data");
            if(StringUtil.isEmpty(url)){
                throw new ParameterException("请求参数URL为空");
            }
            InvokeHttp invokeHttp = new InvokeHttp(latch,baseUrl+url,data,systemParam.getAppHeaderInfo(),i,result);
            executorService.execute(invokeHttp);
        }
        latch.await();
        StringBuffer sb = new StringBuffer("{\"success\":true,\"data\":[");
        for (int i = 0; i < size; i++) {
            if(i != 0) {
                sb.append(",");
            }
            sb.append(result[i]);
        }
        sb.append("]}");
        super.renderJson(sb.toString());
    }
}

class InvokeHttp extends RequestProxy implements Runnable{
    private static final Logger logger = Logger.getLogger(RequestProxy.class);

    private String url;
    private JSONObject data;
    private AppHeaderInfo headerInfo;
    private int index;
    private String [] result;
    private CountDownLatch latch;

    public InvokeHttp(CountDownLatch latch,String url,JSONObject data,AppHeaderInfo headerInfo,int index,String [] result){
         this.url = url;
         this.data = data;
         this.headerInfo = headerInfo;
         this.index = index;
         this.result = result;
         this.latch = latch;
    }

    @Override
    public List<Header> getHeaders() {
        List<Header> headerList = new ArrayList<Header>();
        headerList.add(new BasicHeader( "X-Requested-With", "XMLHttpRequest"));
        cc.ccplay.booster.base.core.Header[] hs  = cc.ccplay.booster.base.core.Header.values();
        for (int i = 0; i < hs.length; i++) {
            cc.ccplay.booster.base.core.Header h = hs[i];
            String key = h.getKey();
            if(cc.ccplay.booster.base.core.Header.API_KEY.equals(key)){
                continue;
            }
            String val = this.headerInfo.getHeader(h);
            if(StringUtil.isNotEmpty(val)){
                headerList.add(new BasicHeader(key, val));
            }
        }
        //子请求不校验签名
        headerList.add(new BasicHeader(cc.ccplay.booster.base.core.Header.API_KEY.name(), Const.testAppKey));
        return headerList;
    }

    @Override
    public List<NameValuePair> getParameters() {
        if(data == null){
            return Collections.emptyList();
        }
        List<NameValuePair> params = new ArrayList<>();
        for(String key : data.keySet()){
            BasicNameValuePair param = new BasicNameValuePair(key,data.getString(key));
            params.add(param);
        }
        return params;
    }


    @Override
    public void run(){
        try {
            String cotnent = this.doRequest(this.url, PostType.POST);
            result[this.index] = cotnent;
        }catch(Exception e){
            logger.info("接口请求失败[url]:"+this.url);
            e.printStackTrace();
            JSONObject json = new JSONObject();
            json.put("success",false);
            json.put("msg","服务器异常");
            result[this.index] = json.toJSONString();
        }finally {
            latch.countDown();
        }
    }
 }