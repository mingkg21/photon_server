package cc.ccplay.booster.app.web;

import cc.ccplay.booster.app.http.RequestProxy;
import cc.ccplay.booster.app.core.Const;
import cc.ccplay.booster.app.http.PostType;
import cc.ccplay.booster.app.http.Response;
import cc.ccplay.booster.base.annotation.NoSignature;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.constants.Constant;
import cc.ccplay.booster.base.test.TestUtil;
import cc.ccplay.booster.base.util.AesUtil;
import cc.ccplay.booster.base.util.RSAUtils;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.util.JsonUtil;
import org.easyj.frame.util.StringUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@Controller
@RequestMapping("/apitest")
public class AppTestApiController extends BaseAppController {


	@NotNeedLogin
	@RequestMapping()
	@NoSignature
	//@Cache(params = {}) //jsp 缓存实例
	@ResponseBody
	public void index(HttpServletRequest request) {
		WebApplicationContext rootWac = ContextLoader.getCurrentWebApplicationContext();
		// 获取servletContext
		ServletContext servletContext = rootWac.getServletContext();
		// 获取子容器
		WebApplicationContext wac = WebApplicationContextUtils.getWebApplicationContext(servletContext, "org.springframework.web.servlet.FrameworkServlet.CONTEXT.springmvc");
		request.setAttribute("classes", JsonUtil.toJson(TestUtil.getApiClass(wac)));
		try {
			request.getRequestDispatcher("/WEB-INF/page/test/index.jsp").forward(request,super.getResponse());
		}catch (Exception e){
			e.printStackTrace();
		}
		//return null;
	}

	// @NotNeedLogin
	// @ResponseBody
	// @RequestMapping("/getFunctions")
	// public void getFunctions(@NotNull String className) {
	// Map<String,TestMethod> map = TestUtil.getApiMethod(className);
	// super.renderJson(map);
	// }
	//
	// @NotNeedLogin
	// @ResponseBody
	// @RequestMapping("/getParameters")
	// public void getParameters(@NotNull String className,@NotNull String methodName){
	// List<TestMethodParameter> parameters = TestUtil.getApiMethodParameter(className,methodName);
	// super.renderJson(MixUtil.newHashMap("parameters",parameters));
	// }

	@NoSignature
	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/doApiTest")
	public void doApiTest(HttpServletRequest req, HttpServletResponse rsp) {
		Enumeration<String> enumeration = req.getParameterNames();
		final List<NameValuePair> params = new ArrayList<>();
		String url = null;
		String userToken = null;
		while (enumeration.hasMoreElements()) {
			String key = enumeration.nextElement();
			if(cc.ccplay.booster.base.core.Header.USER_TOKEN.getKey().equals(key)){
				userToken = req.getParameter(key);
			} else if ("method".equals(key)) {
				url = req.getParameter(key);
			} else if (!"catalog".equals(key)) {
				params.add(new BasicNameValuePair(key,req.getParameter(key)));
			}
		}
		if(url == null){
			throw new GenericException("缺少必要参数method");
		}

		final String ut = userToken;
		url = req.getScheme() + "://" + req.getServerName() + ":" + req.getServerPort() + req.getContextPath() + url;
		Response res = new RequestProxy(){
			public List<NameValuePair> getParameters() {
				return params;
			}
			public List<Header> getHeaders(){
				List<Header> headerList = new ArrayList<>();
				headerList.add(new BasicHeader("api_key", Const.testAppKey));
				if(StringUtil.isNotEmpty(ut)){
					headerList.add(new BasicHeader(cc.ccplay.booster.base.core.Header.USER_TOKEN.getKey(),ut));
				}
				return headerList;
			}
		}.curl(url,PostType.POST);

		if(res.getHeader(Constant.HEADER_ENCRYPT_STATUS_KEY) != null){
			String content = res.getContent();
			try {
				String json = RSAUtils.privateDecrypt(content, RSAUtils.getPrivateKey(Constant.RSA_PRIVATE_KEY));
				JSONObject jsonObject = JSONObject.parseObject(json);
				jsonObject.put("encrypt",content);
				super.renderJson(jsonObject.toJSONString());
			}catch (Exception e){
				throw new GenericException("RSA解密失败");
			}
		}else{
			super.renderJson(res.getContent());
		}
	}
}
