package cc.ccplay.booster.app.web;

import cc.ccplay.booster.base.api.common.SmsService;
import cc.ccplay.booster.base.api.content.BoosterApiService;
import cc.ccplay.booster.base.api.content.CrawlGameService;
import cc.ccplay.booster.base.api.log.BoosterLoginLogService;
import cc.ccplay.booster.base.dto.log.BoosterSuccessLoginDto;
import cc.ccplay.booster.base.model.content.BoosterServer;
import cc.ccplay.booster.base.util.DateTimeUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.api.content.CustomAreaService;
import cc.ccplay.booster.base.api.content.DebugService;
import cc.ccplay.booster.base.api.log.AppDayCountService;
import cc.ccplay.booster.base.test.annotation.ApiClass;
import cc.ccplay.booster.base.test.annotation.ApiMethod;
import org.easyj.frame.util.JsonUtil;
import org.easyj.frame.util.Validator;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.List;

@Controller
@RequestMapping("/debug")
@ApiClass("DEBUG测试接口")
public class DebugController extends BaseAppController {


    @Reference
    private DebugService debugService;

    @Reference
    private CustomAreaService customAreaService;

    @Reference
    private AppDayCountService appDayCountService;

    @Reference
    private static CrawlGameService crawlGameService;

    @Reference
    private BoosterApiService boosterApiService;

    @Reference
    private BoosterLoginLogService boosterLoginLogService;

    @Reference
    private SmsService smsService;

    @NotNeedLogin
    @RequestMapping("/updateCrawlGame")
    @ResponseBody
    @ApiMethod("更新抓包平台数据")
    public void updateCrawlGame(){
        crawlGameService.batchUpdateCralGame();
        super.renderSuccess();
    }


    @NotNeedLogin
    @RequestMapping("/countTodayAppDownloadInfo")
    @ResponseBody
    @ApiMethod("统计当天应用下载量")
    public void countDay(){
        appDayCountService.countToday();
        super.renderSuccess();
    }

    @NotNeedLogin
    @RequestMapping("/countPredayAppDownloadInfo")
    @ResponseBody
    @ApiMethod("统计前一天应用下载量")
    public void countPreDay(){
        appDayCountService.countPreDay();
        super.renderSuccess();
    }


    @NotNeedLogin
    @RequestMapping("/resetDownloadRankingList")
    @ResponseBody
    @ApiMethod("重置游戏排行榜")
    public void resetDownloadRankingList(){
        customAreaService.refreshNewGameDownloadRankingList();
        customAreaService.refreshDownloadRankingList();
        customAreaService.refreshSingleGameRankingList();
        customAreaService.refreshOnlineGameRankingList();
        super.renderSuccess();
    }

    @NotNeedLogin
    @RequestMapping("/hello")
    @ResponseBody
    @ApiMethod("tomcat直接响应")
    public void hello(){
        super.renderSuccess();
    }

    @NotNeedLogin
    @RequestMapping("/readDataBySql")
    @ResponseBody
    @ApiMethod("tomcat直接响应")
    public void readDataBySql(@Param(remark = "sql") @NotNull String sql){
        super.renderJson(debugService.readData(sql));
    }

    @NotNeedLogin
    @RequestMapping("/readData")
    @ResponseBody
    @ApiMethod("tomcat直接响应")
    public void readData(){
        super.renderJson(debugService.readData());
    }



    @NotNeedLogin
    @RequestMapping("/countSuccess")
    @ResponseBody
    @ApiMethod("统计加速器成功率")
    public void countSuccess(){
        int calSize = 50;
        List<BoosterServer> servers = boosterApiService.getAllServer();
        int size = servers.size();
        for (int i = 0; i < size; i++) {
            BoosterServer server = servers.get(i);
            BoosterSuccessLoginDto dto = boosterLoginLogService.getSuccessCount(server.getId(),calSize);
            System.out.println(JsonUtil.toJson(dto));
            if(Validator.isNull(dto.getTotalCount())){
                continue;
            }
            BigDecimal successPercent = (new BigDecimal(dto.getSuccessCount()).divide(new BigDecimal(dto.getTotalCount()),2, BigDecimal.ROUND_DOWN));
            double sp = successPercent.doubleValue();
            long waring = sp > 0.5 ? 0 : 1;
            boolean flag = boosterApiService.updateSuccessPercent(server.getId(),sp,waring);
            Long oldWarning = server.getFailWarning();
            if(dto.getTotalCount() == calSize && flag  && oldWarning != null){
                if(oldWarning == 1 && waring == 0){ //成功率从正常变成异常
                    smsService.sendWarningNotice(server.getName(),server.getIp(), DateTimeUtil.getNow(),(sp * 100)+"");
                }else if(oldWarning == 0 && waring == 1){//成功率从异常变成正常
                    smsService.sendRecoveryNotice(server.getName(),server.getIp(), DateTimeUtil.getNow(),(sp * 100)+"");
                }
            }
        }
        super.renderSuccess();
    }
}
