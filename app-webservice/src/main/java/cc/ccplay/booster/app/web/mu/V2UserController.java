package cc.ccplay.booster.app.web.mu;

import cc.ccplay.booster.app.web.BaseAppController;
import cc.ccplay.booster.base.annotation.NoSignature;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import com.alibaba.fastjson.JSONObject;
import org.apache.log4j.Logger;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;


/**
 * for test
 */
@RequestMapping("/v2/mu")
@Controller
public class V2UserController extends BaseAppController {
    private static final Logger logger = Logger.getLogger(V2UserController.class);

    @NoSignature
    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/nodes/{id}/online_count")
    public void onlineCount(HttpServletRequest request, @PathVariable("id") long nodeId, @Param Long count){
        //logger.error("MUAPI V2 [节点："+nodeId+"]在线人数"+count);
        super.renderSuccess();
    }

    @NoSignature
    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/nodes/{id}/info")
    public void info(HttpServletRequest request, @PathVariable("id") long nodeId){
        //"load":["0.00 0.01 0.05 1/100 21341"],"uptime":["1451064.42"]
        //logger.error("MUAPI V2 [节点info："+nodeId+"]"+ JSONObject.toJSONString(request.getParameterMap()));
        super.renderSuccess();
    }

    @NoSignature
    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/users/{id}/traffic")
    public void traffic(@PathVariable("id") long userId, @Param Long d, @Param Long u, @Param Long node_id){
        //logger.error("MUAPI V2 userId:"+userId+",download:"+d+",u"+u+",node:"+node_id);
        super.renderSuccess();
    }

    @NoSignature
    @NotNeedLogin
    @RequestMapping("/users")
    public String getUserList(@NotNull String key, @NotNull long node){
        return "/mu/users";
    }

}
