package cc.ccplay.booster.app.core;

import cc.ccplay.booster.base.core.AppHeaderInfo;
import cc.ccplay.booster.base.core.Header;
import cc.ccplay.booster.base.core.PagingParameter;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.enums.RedisCacheKeyEnum;
import cc.ccplay.booster.base.enums.SourceType;
import cc.ccplay.booster.base.model.user.UserAccount;
import cc.ccplay.booster.base.util.SpringUtil;
import cc.ccplay.booster.base.util.UserUtil;
import org.easyj.frame.filter.DataCallback;
import org.easyj.frame.filter.SysThreadData;
import org.easyj.frame.util.StringUtil;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import javax.servlet.http.HttpServletRequest;

public class SystemDataCallback implements DataCallback {


	private static ValueOperations<String, String> redisOpt;


	public static ValueOperations<String, String> getReidsOpt(){
		if(redisOpt == null){
			redisOpt = ((StringRedisTemplate)SpringUtil.getBean("userRedisTemplate")).opsForValue();
		}
		return redisOpt;
	}

	public Object initData(SysThreadData threadData) {
		SystemParam customData = new SystemParam();
		customData.setSourceType(SourceType.APP);
		HttpServletRequest request = threadData.getRequest();
		int pageNo = 1;// 默认第一页
		int pageSize = 20; // 默认20条
		String pageNoParam = request.getParameter("page");
		if(pageNoParam != null){
			pageNo = StringUtil.toInt(pageNoParam);
		}
		String pageSizeParam = request.getParameter("pageSize");
		if (pageSizeParam != null) {
			pageSize = StringUtil.toInt(pageSizeParam);
		}
		PagingParameter pagingParameter = new PagingParameter();
		pagingParameter.setPageNo(pageNo);
		pagingParameter.setPageSize(pageSize);
		customData.setPagingParameter(pagingParameter);
		AppHeaderInfo appHeaderInfo = getHeaderInfo(threadData);
		customData.setAppHeaderInfo(appHeaderInfo);


		String userToken = appHeaderInfo.getUserToken();
		if (!StringUtil.isEmpty(userToken)) {
			String key = RedisCacheKeyEnum.user_token_key.getKey(userToken);
			String userId = getReidsOpt().get(key);
			if (!StringUtil.isEmpty(userId)) {
				UserAccount ua = new UserAccount();
				ua.setId(Long.parseLong(userId));
				customData.setUser(ua);
				threadData.setSysUser(ua);
			}
		}
		// 获取客户端IP
		String ip = getRequestIp(UserUtil.getRequest());
		customData.setClientIp(ip);

		customData.setReferer(request.getHeader("referer"));
		customData.setUseragent(request.getHeader("user-agent"));
		return customData;
	}

	private AppHeaderInfo getHeaderInfo(SysThreadData threadData) {
		HttpServletRequest request = threadData.getRequest();
		AppHeaderInfo info = new AppHeaderInfo();
		Header[] hs = Header.values();
		for (int i = 0; i < hs.length; i++) {
			Header header = hs[i];
			String val = request.getHeader(header.getKey());
			if (Header.USER_TOKEN == header && StringUtil.isEmpty(val)) {
				info.addHeader(header, request.getParameter(header.getKey()));
			} else {
				info.addHeader(header, val);
			}
		}
		return info;
	}

	/**
	 * 获取当前操作人IP
	 *
	 * @param request
	 * @return
	 */
	private String getRequestIp(HttpServletRequest request) {
		// return request.getRemoteAddr();
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}
}
