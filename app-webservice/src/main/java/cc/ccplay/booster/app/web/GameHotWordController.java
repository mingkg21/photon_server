package cc.ccplay.booster.app.web;

import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.annotation.Cache;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.api.content.GameHotWordService;
import cc.ccplay.booster.base.core.PagingParameter;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.content.GameHotWord;
import cc.ccplay.booster.base.test.annotation.ApiClass;
import cc.ccplay.booster.base.test.annotation.ApiMethod;
import org.easyj.frame.jdbc.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@ApiClass("游戏热词")
@RequestMapping("/game/hotword")
@Controller
public class GameHotWordController extends BaseAppController{

    @Reference
    private GameHotWordService gameHotWordService;


    @NotNeedLogin
    @ApiMethod(value = "获取默认热词")
    @ResponseBody
    @RequestMapping("/getDefaultHotword")
    @Cache(cacheTimeout = 10 * 60)
    public void getDefaultHotword(){
        GameHotWord gameHotWord = gameHotWordService.getDefaultHotword();
        super.renderJson(gameHotWord);
    }

    @NotNeedLogin
    @ApiMethod(value = "获取热词列表",paging = true)
    @ResponseBody
    @RequestMapping("/getList")
    @Cache(cacheTimeout = 10 * 60)
    //params = {"page","pageSize"},
    public void getList(){
        SystemParam systemParam = super.getSystemParam();
        PagingParameter pagingParameter = new PagingParameter(1,30);
        systemParam.setPagingParameter(pagingParameter);
        Page<GameHotWord> page = gameHotWordService.getList(systemParam);
        super.renderJson(page);
    }
}
