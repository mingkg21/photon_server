package cc.ccplay.booster.app.web;


import cc.ccplay.booster.app.param.UserSystemMessageParam;
import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.api.user.UserSystemMessageService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.user.UserSystemMessage;
import cc.ccplay.booster.base.test.annotation.ApiClass;
import cc.ccplay.booster.base.test.annotation.ApiMethod;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@ApiClass("消息API")
@RequestMapping("/msg")
@Controller
public class SystemMessageController extends BaseAppController {

    @Reference
    private UserSystemMessageService userSystemMessageService;

    @ResponseBody
    @RequestMapping(value = "/getMessage")
    @ApiMethod(value = "获取消息",paging = true)
    public void getMessage(){
        SystemParam param = super.getSystemParam();
        Page<UserSystemMessage> page = userSystemMessageService.getPageByUserId(param,param.getUserId());
        super.renderJson(page);
    }


    @ResponseBody
    @RequestMapping(value = "/pushMsg2One")
    @ApiMethod("推送一条信息[测试使用]")
    public void pushMsg2One(@Param UserSystemMessageParam msg, @NotNull @Param(remark = "用户ID") long userId){
        UserSystemMessage message = msg.toModel();
        message.setUserId(userId);
        userSystemMessageService.pushMsg2One(message);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping(value = "/pushNotification2All")
    @ApiMethod("给所有人推送通知[测试使用]")
    public void pushMsg2All(@Param UserSystemMessageParam msg){
        UserSystemMessage message = msg.toModel();
        message.setMsgType(UserSystemMessage.MSG_TYPE_SYSTEM);
        userSystemMessageService.pushNotification2All(message);
        super.renderSuccess();
    }
}
