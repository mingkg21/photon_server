package cc.ccplay.booster.app.web;

import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.annotation.Cache;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.api.content.GameTagService;
import cc.ccplay.booster.base.api.content.TagTagItemService;
import cc.ccplay.booster.base.core.Header;
import cc.ccplay.booster.base.model.content.GameTag;
import cc.ccplay.booster.base.test.annotation.ApiClass;
import cc.ccplay.booster.base.test.annotation.ApiMethod;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/game/tag")
@ApiClass("游戏标签")
public class GameTagController extends BaseAppController {


    @Reference
    private GameTagService gameTagService;

    @Reference
    private TagTagItemService tagTagItemService;


    @NotNeedLogin
    @RequestMapping("/list")
    @ResponseBody
    @ApiMethod(value = "特色标签列表",paging = true)
    @Cache(params = {"page","pageSize"},headerParams={Header.CLIENT_VERSION_CODE},cacheTimeout = 10 * 60)
    public void getList(){
        Page<GameTag> page = gameTagService.getList(super.getSystemParam(),GameTag.TYPE_SPECIAL);
        super.renderJson(page);
    }

    @NotNeedLogin
    @RequestMapping("/getChildTagList")
    @ResponseBody
    @ApiMethod(value = "获取子标签",paging = true)
    @Cache(params = {"page","pageSize","tagId"},cacheTimeout = 10 * 60)
    public void getChildTagList(@Param(remark="主标签ID") @NotNull Long tagId ){
        super.renderJson(tagTagItemService.getChildTagList(tagId,12));
    }

    @NotNeedLogin
    @RequestMapping("/getRecommendGames")
    @ResponseBody
    @ApiMethod(value = "获取推荐游戏",paging = true)
    @Cache(params = {"page","pageSize"},cacheTimeout = 2 * 60)
    public void getRecommendGames(){
        super.renderJson(gameTagService.getTagRecommendGame(super.getSystemParam()));
    }

}
