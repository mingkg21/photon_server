package cc.ccplay.booster.app.json.serializer;

import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.ObjectSerializer;
import com.alibaba.fastjson.serializer.SerializeWriter;
import cc.ccplay.booster.base.model.adapter.CdnImageList;
import org.easyj.frame.util.JsonUtil;

import java.io.IOException;
import java.lang.reflect.Type;

public class CdnImageListSerializer implements ObjectSerializer {

    @Override
    public void write(JSONSerializer serializer, Object object, Object fieldName, Type fieldType, int features) throws IOException {
        if (object.getClass() != CdnImageList.class) {
            return;
        }
        SerializeWriter out = serializer.getWriter();
        CdnImageList imageList = (CdnImageList)object;
        out.write(JsonUtil.toJson(imageList.getCdnImages()));
    }
}
