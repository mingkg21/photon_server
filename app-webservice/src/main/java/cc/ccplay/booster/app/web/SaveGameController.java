package cc.ccplay.booster.app.web;

import cc.ccplay.booster.base.api.content.*;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import cc.ccplay.booster.app.param.GameRecordParam;
import cc.ccplay.booster.base.annotation.Cache;
import cc.ccplay.booster.base.annotation.NoSignature;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.api.content.*;
import cc.ccplay.booster.base.constants.Constant;
import cc.ccplay.booster.base.core.AppHeaderInfo;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.enums.SaveRecordFeedbackType;
import cc.ccplay.booster.base.model.content.SaveGame;
import cc.ccplay.booster.base.model.content.SaveRecord;
import cc.ccplay.booster.base.model.content.SaveRecordFeedback;
import cc.ccplay.booster.base.test.annotation.ApiClass;
import cc.ccplay.booster.base.test.annotation.ApiMethod;
import cc.ccplay.booster.base.util.UserUtil;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.util.StringUtil;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@ApiClass("存档API")
@RequestMapping("/save/game")
@Controller
public class SaveGameController extends BaseAppController {

    @Reference
    private SaveGameService saveGameService;

    @Reference
    private SaveHopingService saveHopingService;

    @Reference
    private SaveRecordService saveRecordService;

    @Reference
    private GameInfoService gameInfoService;

    @Reference
    private SaveRecordFeedbackService saveRecordFeedbackService;

    @NoSignature
    @NotNeedLogin
    @ResponseBody
    @ApiMethod("获取游戏配置信息")
    @RequestMapping("/getConfig")
    public void getConfig(@NotNull @Param(remark = "游戏ID") long gameId,@NotNull @Param(remark = "版本号") long versionCode){
        SaveGame saveGame = saveGameService.get(gameId);
        if(saveGame == null){
            throw new GenericException("记录不存在");
        }
        saveGame.setCreateTime(null);
        saveGame.setUpdateTime(null);
        super.renderJson(saveGame);
    }

    @NoSignature
    @ResponseBody
    @NotNeedLogin
    @ApiMethod("通过包名获取游戏配置信息")
    @RequestMapping("/getConfigByPackageName")
    public void getConfigByPackageName(@NotNull @Param(remark = "包名") String packageName,@NotNull @Param(remark = "版本号") long versionCode){
        SaveGame saveGame = saveGameService.getByPackageName(packageName);
        if(saveGame == null){
            throw new GenericException("记录不存在");
        }
        saveGame.setCreateTime(null);
        saveGame.setUpdateTime(null);
        super.renderJson(saveGame);
    }

    @ApiMethod("游戏许愿")
    @RequestMapping("/hoping")
    @ResponseBody
    public void hoping(@NotNull @Param(remark = "游戏ID") long gameId){
        long hoppingCount = saveHopingService.hoping(gameId, UserUtil.getUserId());
        super.renderJson(hoppingCount);
    }


    @ApiMethod("保存存档")
    @RequestMapping("/saveGameRecord")
    @ResponseBody
    public void saveGameRecord(@Param GameRecordParam gameRecordParam){
       SaveRecord saveRecord = gameRecordParam.toModel();
       saveRecord.setOpen(SaveRecord.OPEN_FALSE);
       saveRecord.setUserId(UserUtil.getUserId());
       saveRecord.setModelName(super.getModelName());
       SaveRecord newRecord = saveRecordService.uploadRecord(saveRecord);
       super.renderJson(newRecord);
    }

    @ApiMethod("修改存档名字")
    @RequestMapping("/modifyRecordName")
    @ResponseBody
    public void modifyRecordName(
            @Param(remark = "存档ID") @NotNull long recordId,
            @Param(remark = "存档名字") @NotNull String name,
            @Param(remark = "存档备注") String remark){
        SaveRecord saveRecord = new SaveRecord();
        saveRecord.setId(recordId);
        saveRecord.setName(name);
        saveRecord.setRemark(remark);
        saveRecord.setUserId(UserUtil.getUserId());
        saveRecordService.uploadRecord(saveRecord);
        super.renderSuccess();
    }

    @ApiMethod("获取玩家存档")
    @RequestMapping("/getUserRecord")
    @ResponseBody
    public void getUserRecord(@Param(remark = "游戏ID") @NotNull long gameId){
        List<SaveRecord> list = saveRecordService.getUserRecord(UserUtil.getUserId(),gameId);
        super.renderJson(list);
    }

    @NotNeedLogin
    @ApiMethod(value = "获取推荐存档",paging = true)
    @RequestMapping("/getRecommendRecord")
    @ResponseBody
    public void getRecommendRecord(@Param(remark = "游戏ID") @NotNull long gameId){
        super.renderJson(saveRecordService.getRecommendRecord(super.getSystemParam(),gameId));
    }

    @ApiMethod("读取存档")
    @RequestMapping("/getRecord")
    @ResponseBody
    public void getRecord(@Param(remark = "存档ID") @NotNull long recordId){
        SaveRecord saveRecord = saveRecordService.get(recordId);
        if(saveRecord == null){
            throw new GenericException("存档不存在");
        }

        if(StringUtil.equals(saveRecord.getOpen(),SaveRecord.OPEN_FALSE) &&
                !StringUtil.equals(UserUtil.getUserId(),saveRecord.getUserId())){
            throw new GenericException("存档不存在");
        }

        if(StringUtil.equals(saveRecord.getStatus(), Constant.STATUS_DISABLED)){
            throw new GenericException("存档不可用");
        }
        super.renderJson(saveRecord);
    }

    @ApiMethod(value = "获取热门存档游戏",paging = true)
    @RequestMapping("/getRecommendGame")
    @ResponseBody
    @NotNeedLogin
    @Cache(params = {"page","pageSize"})
    public void getRecommendGame(){
        super.renderJson(gameInfoService.getRecommendSaveGame(super.getSystemParam()));
    }

    @ApiMethod(value = "获取我的存档",paging = true)
    @RequestMapping("/getMySaveGame")
    @ResponseBody
    public void getMySaveGame(){
        super.renderJson(gameInfoService.getMySaveGame(super.getSystemParam(),UserUtil.getUserId()));
    }

    @ApiMethod("删除存档")
    @RequestMapping("/deleteRecord")
    @ResponseBody
    public void deleteRecord(@NotNull @Param(remark = "存档ID") long recordId){
        saveRecordService.logicDelete(UserUtil.getUserId(),recordId);
        super.renderSuccess();
    }

    @NotNeedLogin
    @ApiMethod(value = "获取反馈类型")
    @ResponseBody
    @RequestMapping("/getFeedbackTypes")
    @Cache(params = {},cacheTimeout = 60 * 12)
    public void getFeedbackTypes(){
        SaveRecordFeedbackType [] types = SaveRecordFeedbackType.values();
        JSONArray arr = new JSONArray();
        for (int i = 0; i < types.length; i++) {
            SaveRecordFeedbackType ct = types[i];
            JSONObject item = new JSONObject();
            item.put("id",ct.getValue());
            item.put("name",ct.getName());
            arr.add(item);
        }
        super.renderJson(arr);
    }

    @ApiMethod(value = "反馈")
    @ResponseBody
    @RequestMapping("/feedback")
    public void complaint(@NotNull @Param(remark = "记录ID") long recordId,
                          @NotNull @Param(remark = "反馈类型ID") int typeId,
                          @NotNull @Param(remark = "内容") String content,
                          @Param(remark = "联系方式") String concatType,
                          @Param(remark = "联系号码") String concatNumber
    ){


        SaveRecordFeedbackType ct = SaveRecordFeedbackType.getByValue(typeId);

        if(ct == null){
            throw new GenericException("投诉类型有误");
        }


        SystemParam param = super.getSystemParam();
        AppHeaderInfo headerInfo = param.getAppHeaderInfo();
        SaveRecordFeedback feedback = new SaveRecordFeedback();
        feedback.setRecordId(recordId);
        feedback.setModelName(headerInfo.getModelName());
        feedback.setTypeId(new Long(typeId));
        feedback.setTypeDesc(ct.getName());
        feedback.setOsVersion(headerInfo.getOsVersion());
        feedback.setComplaintContent(content);
        feedback.setUserId(UserUtil.getUserId());
        if(StringUtil.isNotEmpty(concatNumber)) {
            feedback.setConcatType(concatType);
            feedback.setConcatNumber(concatNumber);
        }
        saveRecordFeedbackService.save(feedback);
        //this.complaintService.save(param,complaint);
        super.renderSuccess();
    }

}
