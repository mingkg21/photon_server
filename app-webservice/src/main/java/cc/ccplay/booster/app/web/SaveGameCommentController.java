package cc.ccplay.booster.app.web;


import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONArray;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.api.content.SaveRecordCommentService;
import cc.ccplay.booster.base.api.content.SaveRecordService;
import cc.ccplay.booster.base.api.user.UserAccountPermissionService;
import cc.ccplay.booster.base.dto.content.save.SaveRecordCommentDto;
import cc.ccplay.booster.base.model.adapter.CdnImageListAdapter;
import cc.ccplay.booster.base.model.content.SaveRecordComment;
import cc.ccplay.booster.base.test.annotation.ApiClass;
import cc.ccplay.booster.base.test.annotation.ApiMethod;
import cc.ccplay.booster.base.util.UserUtil;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.StringUtil;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@ApiClass("存档点评")
@RequestMapping("/save/comment")
@Controller
public class SaveGameCommentController extends BaseAppController{

    @Reference
    private SaveRecordService saveRecordService;

    @Reference
    private SaveRecordCommentService saveRecordCommentService;

    @Reference
    private UserAccountPermissionService userAccountPermissionService;

    @ApiMethod("点赞")
    @RequestMapping("/praiseRecord")
    @ResponseBody
    public void praiseRecord(@NotNull @Param(remark = "存档ID") long recordId){
        saveRecordService.praise(UserUtil.getUserId(),recordId);
        super.renderSuccess();
    }



    @ResponseBody
    @RequestMapping("/comment")
    @ApiMethod("评论")
    public void comment(@Param(remark = "存档ID") @NotNull long recordId,
                        @Param(remark = "内容") @NotNull String content,
                        @Param( remark = "图片（JSONArray格式）") String imgs){

        if(userAccountPermissionService.hasSaveRecordCommentPermission(UserUtil.getUserId()) == false){
            throw new GenericException("当前账号已被禁止评论");
        }
        SaveRecordComment comment = new SaveRecordComment();
        comment.setRecordId(recordId);
        comment.setUserId(UserUtil.getUserId());
        comment.setContent(content);
        comment.setImgs(CdnImageListAdapter.createImage(toImages(imgs),null));
        comment.setOsVersion(super.getOsVersion());
        comment.setModelName(super.getModelName());
        SaveRecordComment result = saveRecordCommentService.comment(comment);
        super.renderJson(result);
    }

    @ResponseBody
    @RequestMapping("/reply")
    @ApiMethod("回复")
    public void replyComment(@Param(remark = "评论或回复ID") @NotNull long commentId,
                             @Param(remark = "内容") @NotNull String content,@Param(remark = "图片（JSONArray格式）") String imgs){
        SaveRecordComment comment = new SaveRecordComment();
        comment.setUserId(UserUtil.getUserId());
        comment.setOsVersion(super.getOsVersion());
        comment.setModelName(super.getModelName());
        comment.setContent(content);
        comment.setParentId(commentId);
        comment.setImgs(CdnImageListAdapter.createImage(toImages(imgs),null));
        SaveRecordComment result = saveRecordCommentService.reply(comment);
        super.renderJson(result);
    }


    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/getComment")
    @ApiMethod(value = "获取回复/评论信息")
    public void getComment(@Param(remark = "回复ID") @NotNull long commentId){
        super.renderJson(saveRecordCommentService.getComment(commentId));
    }

    private JSONArray toImages(String imgs){
        if (StringUtil.isEmpty(imgs)) {
            return new JSONArray();
        }
        JSONArray arr = null;
        try {
            arr = JSONArray.parseArray(imgs);
            return arr;
        }catch (Exception e){
            throw new GenericException("[图片]数据格式有误");
        }
    }


    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/commentList")
    @ApiMethod(value = "评论列表",paging = true)
    public void commentList(@Param(remark = "存档ID") @NotNull long recordId){
        Page<SaveRecordCommentDto> page = saveRecordCommentService.getCommentPage(super.getSystemParam(),recordId);
        super.renderJson(page);
    }

    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/replyList")
    @ApiMethod(value = "回复列表",paging = true)
    public void replyList(@Param(remark = "评论ID") @NotNull long commentId){
        Page<SaveRecordCommentDto> page = saveRecordCommentService.getReplyPage(super.getSystemParam(),commentId);
        super.renderJson(page);
    }
}
