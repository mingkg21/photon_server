package cc.ccplay.booster.app.web;


import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.api.content.*;
import cc.ccplay.booster.base.api.log.BoosterGameRecordService;
import cc.ccplay.booster.base.api.log.BoosterLoginLogService;
import cc.ccplay.booster.base.api.user.UserFlowService;
import cc.ccplay.booster.base.dto.content.BoosterAuthDto;
import cc.ccplay.booster.base.model.content.BoosterGameFeedback;
import cc.ccplay.booster.base.model.log.BoosterGameRecord;
import cc.ccplay.booster.base.model.log.BoosterLoginLog;
import cc.ccplay.booster.base.model.user.UserFlow;
import cc.ccplay.booster.base.test.annotation.ApiClass;
import cc.ccplay.booster.base.test.annotation.ApiMethod;
import cc.ccplay.booster.base.util.UserUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@Controller
@RequestMapping("/booster/server")
@ApiClass("加速器服务")
public class BoosterServerController extends BaseAppController{

    @Reference
    private BoosterServerService boosterServerService;

    @Reference
    private BoosterGameFeedbackService boosterGameFeedbackService;

    @Reference
    private BoosterApiService boosterApiService;

    @Reference
    private UserFlowService userFlowService;

    @Reference
    private GamePackageMappingService gamePackageMappingService;

    @Reference
    private BoosterGameRecordService boosterGameRecordService;

    @Reference
    private BoosterServerOnlineService boosterServerOnlineService;

    @Reference
    private BoosterLoginLogService boosterLoginLogService;

    //内部测试使用
    @RequestMapping("/getAllServer")
    @ResponseBody
    @ApiMethod(value = "获取所有加速服务器(内部测试使用)")
    public void getAllServer(){
        super.renderJson(boosterApiService.getAllServer());
    }

    //内部测试使用
    @RequestMapping("/getSuperAuth")
    @ResponseBody
    @ApiMethod(value = "获取加速通道(内部测试使用)")
    public void getSuperAuth(@NotNull @Param(remark = "服务器ID") long serverId){
        BoosterAuthDto authDto = boosterApiService.getSuperAuth(UserUtil.getUserId(),serverId);
        super.renderJson(authDto);
    }


    @RequestMapping("/getServerAuth")
    @ResponseBody
    @ApiMethod(value = "获取加速通道授权",encrypt = true)
    public void getServerAuth(@NotNull @Param(remark = "游戏ID") long gameId,
                              @Param(remark = "serverIds") String serverIds){
        Long userId = UserUtil.getUserId();
        UserFlow userFlow = userFlowService.getFlow(userId);
//        if(userFlow == null){
//            throw new GenericException("您的体验流量已用完，请联系官方Q群~");
//        }
//        long total = userFlow.getTotalFlow().longValue();
//        long upload = userFlow.getUsedUploadFlow().longValue();
//        long download = userFlow.getUsedDownloadFlow().longValue();
//        if(total <= upload + download){
//            throw new GenericException("您的体验流量已用完，请联系官方Q群~");
//        }
        BoosterAuthDto authDto = boosterApiService.login(userFlow,gameId,serverIds);
        //保存加速记录
        try{
            BoosterGameRecord record = new BoosterGameRecord();
            record.setGameId(gameId);
            record.setServerId(authDto.getServerId());
            record.setUserId(userId);
            boosterGameRecordService.save(record);
        }catch (Exception e){
            e.printStackTrace();
        }
        super.renderJson(authDto);
    }

    @RequestMapping("/offline")
    @ResponseBody
    @ApiMethod("断开连接")
    public void offline(){
        boosterApiService.offline(UserUtil.getUserId());
        super.renderSuccess();
    }

    @RequestMapping("/ack")
    @ResponseBody
    @ApiMethod("心跳包")
    public void ack(@NotNull @Param(remark = "SS服务器ID") long serverId,
                    @NotNull @Param(remark = "端口号") long port){
        boolean flag = boosterApiService.ack(UserUtil.getUserId(),serverId,port);
        JSONObject result = new JSONObject();
        result.put("online",flag);
        Object ret = result;
        super.renderJson(ret);
    }

    @NotNeedLogin
    @RequestMapping("/feedback")
    @ResponseBody
    @ApiMethod("反馈找不到加速游戏")
    public void feedback(
                         @NotNull @Param(remark = "游戏名") String name,
                         @Param(remark = "包名") String packageName,
                         @Param(remark = "版本名") String versionName,
                         @Param(remark = "版本号") Long versionCode){
        BoosterGameFeedback feedback = new BoosterGameFeedback();
        feedback.setName(name);
        feedback.setPackageName(packageName);
        feedback.setUserId(UserUtil.getUserId());
        feedback.setVersionCode(versionCode);
        feedback.setVersionName(versionName);
        boosterGameFeedbackService.saveOrUpdate(feedback);
        super.renderSuccess();
    }


    @RequestMapping("/getMyGame")
    @ResponseBody
    @ApiMethod("获取我的游戏")
    @NotNeedLogin
    public void getMyGame(@NotNull @Param(remark = "包名(json数组)") String packageNames){
        List<String> names = JSONArray.parseArray(packageNames,String.class);
        if(names.size() == 0){
            super.renderJson(Collections.emptyList());
            return;
        }
        super.renderJson(gamePackageMappingService.getMappingGame(names));
    }

    @RequestMapping("/uploadConnectedStatus")
    @ResponseBody
    @ApiMethod("上报连接状态")
    public void uploadConnectedStatus(@NotNull @Param(remark = "游戏ID") long gameId,
                                      @NotNull @Param(remark = "服务器ID") long serverId,
                                      @NotNull @Param(remark = "连接状态") long status){
        BoosterLoginLog log = new BoosterLoginLog();
        log.setServerId(serverId);
        log.setUserId(UserUtil.getUserId());
        log.setGameId(gameId);
        if(status == 0){
            log.setStatus(status);
        }else{
            log.setStatus(1L);
        }
        boosterLoginLogService.save(log);
        super.renderSuccess();
    }
}
