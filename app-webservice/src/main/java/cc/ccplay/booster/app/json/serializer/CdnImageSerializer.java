package cc.ccplay.booster.app.json.serializer;

import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.ObjectSerializer;
import com.alibaba.fastjson.serializer.SerializeWriter;
import cc.ccplay.booster.base.model.adapter.CdnImage;

import java.io.IOException;
import java.lang.reflect.Type;

public class CdnImageSerializer implements ObjectSerializer {
    @Override
    public void write(JSONSerializer serializer, Object object, Object fieldName, Type fieldType, int features) throws IOException {
        if (object.getClass() != CdnImage.class) {
            return;
        }
        SerializeWriter out = serializer.getWriter();
        CdnImage cdnImage = (CdnImage)object;
        out.write("\"" + cdnImage.getCndSrc() + "\"");
    }
}
