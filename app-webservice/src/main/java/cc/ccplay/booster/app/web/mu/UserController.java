package cc.ccplay.booster.app.web.mu;

import cc.ccplay.booster.app.web.BaseAppController;
import cc.ccplay.booster.base.annotation.NoSignature;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.api.content.BoosterApiService;
import cc.ccplay.booster.base.api.user.UserFlowService;
import cc.ccplay.booster.base.dto.content.booster.AddUserFlowDto;
import cc.ccplay.booster.base.dto.user.UserFollowDto;
import cc.ccplay.booster.base.enums.AddUserFlowType;
import cc.ccplay.booster.base.enums.BoosterCacheKey;
import cc.ccplay.booster.base.model.user.UserFlow;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import org.apache.log4j.Logger;
import org.easyj.frame.util.StringUtil;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Map;


@RequestMapping("/mu")
@Controller
public class UserController extends BaseAppController {


    private static final Logger logger = Logger.getLogger(UserController.class);

    @Reference
    private UserFlowService userFlowService;

    @Reference
    private BoosterApiService boosterApiService;


    @Resource(name = "boosterRedisTemplate")
    private RedisTemplate<String,String> redisTemplate;

    @NoSignature
    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/users")
    public void getUserList(@NotNull String key,@NotNull long node){
        JSONObject result = new JSONObject();
        result.put("msg","ok");
        result.put("data",boosterApiService.getUsers(node));
        super.renderJson(result);
    }

    @NoSignature
    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/users/{id}/traffic")
    public void traffic(@PathVariable("id") long userId, @Param Long d, @Param Long u, @Param Long node_id){

        AddUserFlowDto dto = new AddUserFlowDto();
        dto.setDownload(d);
        dto.setUpload(u);
        dto.setUserId(userId);
        dto.setRemark("服务器ID:"+node_id);
        dto.setType(AddUserFlowType.USED);
        dto.setTime(new Date());
        UserFlow userFlow = userFlowService.addFlow(dto);
        if(userFlow == null){
            //offline(userId);
            boosterApiService.offline(userId);
            super.renderSuccess();
            return;
        }

        try{
            long totalFlow = userFlow.getTotalFlow().longValue();
            long usedDownload = userFlow.getUsedDownloadFlow().longValue() + d;
            long usedUpload  = userFlow.getUsedUploadFlow().longValue() + u;
            if(totalFlow < usedDownload + usedUpload){ //踢下线
                boosterApiService.offline(userId);
                super.renderSuccess();
                return;
            }

            //更新流量
            String onlineUserKey = BoosterCacheKey.ONLINE_USER.getKey(userId);
            if(redisTemplate.hasKey(onlineUserKey)){
                HashOperations<String,String,String> hashOpt = redisTemplate.opsForHash();
                hashOpt.increment(onlineUserKey,"d",d);
                hashOpt.increment(onlineUserKey,"u",u);
            }

        }catch (Exception e){
            logger.error(e);
        }
        //logger.error("userId:"+userId+",download:"+d+",u"+u+",node:"+node_id);
        super.renderSuccess();
    }

//    private void offline(long userId){
//        String onlineUserKey = BoosterCacheKey.ONLINE_USER.getKey(userId);
//        HashOperations<String,String,String> hashOpt = redisTemplate.opsForHash();
//        Map<String,String> map = hashOpt.entries(onlineUserKey);
//        if(map != null){
//            String serverId = map.get("node_id");
//            String port = map.get("port");
//            if(StringUtil.isNotEmpty(serverId) && StringUtil.isNotEmpty(port)) {
//                String portSetKey = BoosterCacheKey.SERVER_ONLINE_PORT_SET.getKey(serverId);
//                redisTemplate.opsForSet().remove(portSetKey, port);//删除端口引用
//            }
//        }
//        redisTemplate.delete(onlineUserKey);
//    }

}
