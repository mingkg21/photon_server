package cc.ccplay.booster.app.web;

import cc.ccplay.booster.app.http.PostType;
import cc.ccplay.booster.app.http.RequestProxy;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.api.user.LoginService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.user.LoginResultDto;
import cc.ccplay.booster.base.model.user.ThirdPlaformUser;
import cc.ccplay.booster.base.test.annotation.ApiClass;
import cc.ccplay.booster.base.test.annotation.ApiMethod;
import org.apache.log4j.Logger;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.util.StringUtil;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.MessageFormat;

@Controller
@ApiClass("登录API")
public class LoginController extends BaseAppController {

	//微信授权信息
	public static final String WX_AUTH_LOGIN_URL = "https://api.weixin.qq.com/sns/oauth2/access_token";
	public static final String WX_USERINFO_URL = "https://api.weixin.qq.com/sns/userinfo";
	public static final String WX_APP_ID = "wx942b7737fb5e870a";
	public static final String WX_APP_KEY = "67745a6dfdaea428280c308ee25005db";

	//QQ授权信息
	public static final String QQ_APP_ID = "1107902927";
	public static final String QQ_APP_KEY = "0UV281qWG1L8Xyow";

	//public static final String QQ_USER_INFO_URL ="https://graph.qq.com/oauth2.0/me?access_token={0}&unionid=1";
	//public static final String QQ_USER_TOKEN = "https://graph.qq.com/oauth2.0/token?grant_type=authorization_code&client_id={0}&client_secret={1}&code={2}&redirect_uri={3}";
	public static final String QQ_USER_INFO_URL = "https://graph.qq.com/user/get_user_info?access_token={0}&oauth_consumer_key={1}&openid={2}";

	private static Logger logger = Logger.getLogger(LoginController.class);


	@Reference
	private LoginService loginService;

	@NotNeedLogin
	@ResponseBody
	@RequestMapping(value = "/doLoginByPwd", method = RequestMethod.POST)
	@ApiMethod("通过密码登录")
	public void doLoginByPwd(@Param(remark = "手机号") @NotNull String phone, @Param(remark = "密码") @NotNull String password) {
		SystemParam param = getSystemParam();
        LoginResultDto dto = loginService.loginByPhone(param, phone, password);
        renderJson(dto);
	}

	@NotNeedLogin
	@ResponseBody
	@RequestMapping(value = "/doLoginByCode", method = RequestMethod.POST)
	@ApiMethod("通过验证码登录")
	public void doLoginByCode(@Param(remark = "手机号") @NotNull String phone, @Param(remark = "验证码") @NotNull String validateCode) {
		SystemParam param = getSystemParam();
        LoginResultDto dto = loginService.loginByCode(param, phone, validateCode);
		renderJson(dto);
	}

	public static void main(String []args){
		String url = MessageFormat.format(QQ_USER_INFO_URL, "88317E6A5C3DA62A53B1591C996C0E83",QQ_APP_ID,
				"FE45E93CCA5265C522BCDE431C476EB3" );
		String json = new RequestProxy() {
		}.doRequest(url, PostType.GET);

		System.out.println(json);
	}

	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/doLoginByQQ")
	@ApiMethod("通过QQ登录")
	public void doLoginByQQ(@Param(remark = "token") @NotNull String token,@Param(remark = "openid") @NotNull String openid){
		String url = MessageFormat.format(QQ_USER_INFO_URL, token,QQ_APP_ID,openid);
		String json = null;
		try {
			json = new RequestProxy() {
			}.doRequest(url, PostType.GET);
		}catch (Exception e){
			if(json != null) {
				logger.error("QQ用户信息获取异常,响应json:" + json);
			}
			logger.error(e);
			throw new GenericException("QQ用户信息获取异常");
		}
		JSONObject retObj = JSONObject.parseObject(json);
		int retCode = retObj.getIntValue("ret");
		String msg = retObj.getString("msg");
		if(retCode != 0){
			logger.error("QQ信息获取失败：" + json);
			throw new GenericException("QQ用户信息获取失败");
		}

		ThirdPlaformUser user = new ThirdPlaformUser();
		user.setPlatform(ThirdPlaformUser.PLATFORM_QQ);
		String sex = retObj.getString("gender");
		if(sex.equals("男")){
			user.setSex("1");
		}else{
			user.setSex("0");
		}

		String bigHead = retObj.getString("figureurl_qq_2");
		if(StringUtil.isNotEmpty(bigHead)){
			user.setHeadimg(bigHead);
		}else{
			String smallHead = retObj.getString("figureurl_qq_1");
			user.setHeadimg(smallHead);
		}
		user.setUserInfoJson(json);
		user.setOpenId(openid);
		//user.setUnionid(null);
		//user.setSex(sex);
		user.setNickname(retObj.getString("nickname"));
		user.setPlatform(ThirdPlaformUser.PLATFORM_QQ);

		LoginResultDto loginResultDto = loginService.loginByThirdPlaftformOpenId(super.getSystemParam(),openid,ThirdPlaformUser.PLATFORM_QQ);
		if(loginResultDto != null) {
			super.renderJson(loginResultDto);
			return;
		}

		loginResultDto = loginService.loginByThirdPlaftform(super.getSystemParam(),user);
		super.renderJson(loginResultDto);
	}


	@NotNeedLogin
	@ResponseBody
	@RequestMapping("/doLoginByWechat")
	@ApiMethod("通过微信登录")
	public void doLoginByWechat(@Param(remark = "微信Code") @NotNull String code){
		String loginUrl = WX_AUTH_LOGIN_URL+"?appid="+WX_APP_ID+"&secret="+WX_APP_KEY+"&code="+code+"&grant_type=authorization_code";

		String content = new RequestProxy(){}.doRequest(loginUrl, PostType.GET);
		JSONObject grantObj  = JSONObject.parseObject(content);
		String errcode = grantObj.getString("errcode");
		String openId = grantObj.getString("openid");
		if (StringUtil.isNotEmpty(errcode) || StringUtil.isEmpty(openId)) {
			logger.error("login weixin error:"+content);
			super.renderFailure("登录失败");
			return;
		}
		String accessToken = grantObj.getString("access_token");
		String expiresIn = grantObj.getString("expires_in");
		String refreshToken = grantObj.getString("refresh_token");
		String scope = grantObj.getString("scope");

		LoginResultDto loginResultDto = loginService.loginByThirdPlaftformOpenId(super.getSystemParam(),openId,ThirdPlaformUser.PLATFORM_WECHAT);
		if(loginResultDto != null) {
			super.renderJson(loginResultDto);
			return;
		}

		//获取用户信息
		String userUrl = WX_USERINFO_URL+"?access_token="+accessToken+"&openid="+openId;
		String userRet = new RequestProxy(){}.doRequest(userUrl,PostType.GET);

		JSONObject userObj = JSONObject.parseObject(userRet);
		ThirdPlaformUser user = new ThirdPlaformUser();
		String nickname = userObj.getString("nickname");
		String sex = userObj.getString("sex");
		String userImg = userObj.getString("headimgurl");
		String unionid = userObj.getString("unionid");
		user.setHeadimg(userImg);
		user.setOpenId(openId);
		user.setUnionid(unionid);
		user.setSex(sex);
		user.setNickname(nickname);
		user.setPlatform(ThirdPlaformUser.PLATFORM_WECHAT);
		user.setUserInfoJson(userRet);
		loginResultDto = loginService.loginByThirdPlaftform(super.getSystemParam(),user);
		super.renderJson(loginResultDto);
	}

}
