package cc.ccplay.booster.app.web;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONException;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.api.content.GameEvaluationService;
import cc.ccplay.booster.base.model.content.GameEvaluation;
import cc.ccplay.booster.base.test.annotation.ApiClass;
import cc.ccplay.booster.base.test.annotation.ApiMethod;
import org.easyj.frame.exception.ParameterException;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/game/evaluation")
@Controller
@ApiClass("游戏评测")
public class GameEvaluationController extends BaseAppController {

    @Reference
    private GameEvaluationService gameEvaluationService;

    @RequestMapping("/saveEvaluation")
    @ApiMethod("保存评测")
    public void saveEvaluation(@NotNull @Param("游戏ID") long gameId ,@NotNull @Param(remark = "标题") String title,
                               @NotNull @Param(remark = "正文") String content,
                               @Param(remark = "图片(json数组格式)")  String images){
        JSONArray imagesArr = null;
        try{
            imagesArr = JSONArray.parseArray(images);
        }catch (JSONException e){
            e.printStackTrace();
            throw new ParameterException("字段[images]数据格式有误");
        }
        GameEvaluation evaluation = new GameEvaluation();
        evaluation.setType(GameEvaluation.TYPE_USER);
        evaluation.setTitle(title);
        evaluation.setContent(content);
        evaluation.setGameId(gameId);
        evaluation.setImages(imagesArr);
        gameEvaluationService.saveEvaluation(super.getSystemParam(),evaluation);
        super.renderSuccess();
    }

    @NotNeedLogin
    @RequestMapping("/getUserEvaluations")
    @ApiMethod(value = "获取用户评测",paging = true)
    public void getUserEvaluations(@NotNull @Param(remark = "游戏ID") long gameId){

    }

    @NotNeedLogin
    @RequestMapping("/getOfficialEvaluations")
    @ApiMethod(value = "获取官方评测",paging = true)
    public void getOfficialEvaluations(){

    }
}
