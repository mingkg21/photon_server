package cc.ccplay.booster.app.web;


import cc.ccplay.booster.base.api.content.SuggestionFeedbackService;
import cc.ccplay.booster.base.model.content.SuggestionFeedback;
import cc.ccplay.booster.base.test.annotation.ApiClass;
import cc.ccplay.booster.base.test.annotation.ApiMethod;
import cc.ccplay.booster.base.util.UserUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@ApiClass("意见反馈")
@Controller
@RequestMapping("/suggestion")
public class SuggestionFeedbackController extends BaseAppController   {

    @Reference
    private SuggestionFeedbackService suggestionFeedbackService;

    @ApiMethod("反馈")
    @RequestMapping("/feedback")
    @ResponseBody
    public void feedback(@NotNull @Param(remark = "反馈类型,1:加速器 2:游戏问题 3:其他") long feedbackType,
                         @NotNull @Param(remark = "反馈内容") String content){
        SuggestionFeedback feedback = new SuggestionFeedback();
        feedback.setIsReply(0L);
        feedback.setContent(content);
        feedback.setFeedbackType(feedbackType);
        feedback.setUserId(UserUtil.getUserId());
        suggestionFeedbackService.saveOrUpdate(feedback);
        super.renderSuccess();
    }

    @ApiMethod("获取反馈历史列表")
    @RequestMapping("/getHistory")
    @ResponseBody
    public void getHistory(){
        super.renderJson(suggestionFeedbackService.getHistory(UserUtil.getUserId()));
    }

}
