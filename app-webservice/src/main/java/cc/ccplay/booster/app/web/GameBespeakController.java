package cc.ccplay.booster.app.web;

import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.api.content.GameBespeakService;
import cc.ccplay.booster.base.test.annotation.ApiClass;
import cc.ccplay.booster.base.test.annotation.ApiMethod;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/game/bespeak")
@ApiClass("游戏预约")
public class GameBespeakController extends BaseAppController{

    @Reference
    private GameBespeakService gameBespeakService;

    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/list")
    @ApiMethod("游戏预约列表")
    public void list(){
       Page page = gameBespeakService.getDtoPage(super.getSystemParam());
       super.renderJson(page);
    }

    @ResponseBody
    @RequestMapping("/bespeak")
    @ApiMethod("预约游戏")
    public void bespeak(@Param(remark = "预约ID") @NotNull long bespeakId){
        gameBespeakService.bespeak(super.getSystemParam(),bespeakId);
        super.renderSuccess();
    }

}
