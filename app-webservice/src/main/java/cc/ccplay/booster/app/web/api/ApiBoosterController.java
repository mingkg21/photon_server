package cc.ccplay.booster.app.web.api;

import cc.ccplay.booster.app.web.BaseAppController;
import cc.ccplay.booster.base.annotation.InnerServerApi;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.api.content.BoosterApiService;
import cc.ccplay.booster.base.api.content.BoosterGameFeedbackService;
import cc.ccplay.booster.base.api.content.GamePackageMappingService;
import cc.ccplay.booster.base.api.log.BoosterGameRecordService;
import cc.ccplay.booster.base.api.log.BoosterLoginLogService;
import cc.ccplay.booster.base.api.user.UserAccountService;
import cc.ccplay.booster.base.api.user.UserFlowService;
import cc.ccplay.booster.base.dto.content.BoosterAuthDto;
import cc.ccplay.booster.base.model.content.BoosterGameFeedback;
import cc.ccplay.booster.base.model.log.BoosterGameRecord;
import cc.ccplay.booster.base.model.log.BoosterLoginLog;
import cc.ccplay.booster.base.model.user.UserAccount;
import cc.ccplay.booster.base.model.user.UserFlow;
import cc.ccplay.booster.base.test.annotation.ApiMethod;
import cc.ccplay.booster.base.util.UserUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.logging.Logger;


@RequestMapping("/innerServerApi/booster")
@Controller
@InnerServerApi
public class ApiBoosterController extends BaseAppController {

    @Reference
    private GamePackageMappingService gamePackageMappingService;

    @Reference
    private UserFlowService userFlowService;

    @Reference
    private BoosterGameRecordService boosterGameRecordService;

    @Reference
    private BoosterApiService boosterApiService;

    @Reference
    private UserAccountService userAccountService;

    @Reference
    private BoosterGameFeedbackService boosterGameFeedbackService;


    @Reference
    private BoosterLoginLogService boosterLoginLogService;


    /**
     * 获取可加速游戏
     * @param packageNames
     */
    @RequestMapping("/getBoosterGame")
    @ResponseBody
    public void getBoosterGame(@NotNull String packageNames){

        //super.renderJson(list);
    }



    @RequestMapping("/getServerAuth")
    @ResponseBody
    @ApiMethod(value = "获取加速通道授权",encrypt = true)
    public void getServerAuth(
            @NotNull String openId,
            @NotNull long platform,
            @NotNull @Param(remark = "游戏ID") long gameId,
                              @Param(remark = "serverIds") String serverIds){

        UserAccount userAccount = userAccountService.createOrGetByOpenId(openId,platform);
        Long userId = userAccount.getId();
        UserFlow userFlow = userFlowService.getFlow(userId);
//        if(userFlow == null){
//            throw new GenericException("您的体验流量已用完，请联系官方Q群~");
//        }
//        long total = userFlow.getTotalFlow().longValue();
//        long upload = userFlow.getUsedUploadFlow().longValue();
//        long download = userFlow.getUsedDownloadFlow().longValue();
//        if(total <= upload + download){
//            throw new GenericException("您的体验流量已用完，请联系官方Q群~");
//        }
        BoosterAuthDto authDto = boosterApiService.login(userFlow,gameId,serverIds);
        //authDto.setDomains(domains);
        //保存加速记录
        try{
            BoosterGameRecord record = new BoosterGameRecord();
            record.setGameId(gameId);
            record.setServerId(authDto.getServerId());
            record.setUserId(userId);
            boosterGameRecordService.save(record);
        }catch (Exception e){
            e.printStackTrace();
        }
        //authDto.setTestDomain("https://www.baidus.com");
        super.renderJson(authDto);
    }

    @RequestMapping("/offline")
    @ResponseBody
    public void offline(@NotNull String openId,@NotNull long platform){
        UserAccount userAccount = userAccountService.createOrGetByOpenId(openId,platform);
        Long userId = userAccount.getId();
        boosterApiService.offline(userId);
        super.renderSuccess();
    }

    @RequestMapping("/ack")
    @ResponseBody
    public void ack(@NotNull String openId,@NotNull long platform,@NotNull @Param(remark = "SS服务器ID") long serverId,
                    @NotNull @Param(remark = "端口号") long port){
        UserAccount userAccount = userAccountService.createOrGetByOpenId(openId,platform);
        Long userId = userAccount.getId();
        boolean flag = boosterApiService.ack(userId,serverId,port);
        JSONObject result = new JSONObject();
        result.put("online",flag);
        Object ret = result;
        super.renderJson(ret);
    }

    //"反馈找不到加速游戏"
    @RequestMapping("/feedback")
    @ResponseBody
    public void feedback(
            @NotNull String openId,
            @NotNull long platform,
            @NotNull @Param(remark = "游戏名") String name,
            @Param(remark = "包名") String packageName,
            @Param(remark = "版本名") String versionName,
            @Param(remark = "版本号") Long versionCode){

        UserAccount userAccount = userAccountService.createOrGetByOpenId(openId,platform);
        Long userId = userAccount.getId();
        BoosterGameFeedback feedback = new BoosterGameFeedback();
        feedback.setName(name);
        feedback.setPackageName(packageName);
        feedback.setUserId(userId);
        feedback.setVersionCode(versionCode);
        feedback.setVersionName(versionName);
        boosterGameFeedbackService.saveOrUpdate(feedback);
        super.renderSuccess();
    }

    @RequestMapping("/uploadConnectedStatus")
    @ResponseBody
    public void uploadConnectedStatus(@NotNull String openId,
                                      @NotNull long platform,
                                      @NotNull @Param(remark = "游戏ID") long gameId,
                                      @NotNull @Param(remark = "服务器ID") long serverId,
                                      @NotNull @Param(remark = "连接状态") long status){
        UserAccount userAccount = userAccountService.createOrGetByOpenId(openId,platform);
        Long userId = userAccount.getId();
        BoosterLoginLog log = new BoosterLoginLog();
        log.setServerId(serverId);
        log.setUserId(userId);
        log.setGameId(gameId);
        if(status == 0){
            log.setStatus(status);
        }else{
            log.setStatus(1L);
        }
        boosterLoginLogService.save(log);
        super.renderSuccess();
    }
}
