package cc.ccplay.booster.app.web;

import cc.ccplay.booster.base.api.user.UserAccountService;
import com.alibaba.fastjson.JSONArray;
import cc.ccplay.booster.base.api.content.GameInfoService;
import cc.ccplay.booster.base.api.content.UserGameListService;
import cc.ccplay.booster.base.dto.content.game.GameInfoDto;
import cc.ccplay.booster.base.dto.user.SimpleUserDto;
import cc.ccplay.booster.base.util.UserUtil;
import org.apache.log4j.Logger;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.dubbo.config.annotation.Reference;

import cc.ccplay.booster.base.api.user.UserProfileService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.test.annotation.ApiClass;
import cc.ccplay.booster.base.test.annotation.ApiMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/user")
@ApiClass("用户API")
public class UserProfileController extends BaseAppController {

	private static final Logger log = Logger.getLogger(UserProfileController.class);

	@Reference
	private UserProfileService userProfileService;

	@Reference
	private GameInfoService gameInfoService;

	@Reference
	private UserGameListService userGameListService;

	@Reference
	private UserAccountService userAccountService;

	@ResponseBody
	@RequestMapping("/updateProfile")
	@ApiMethod("修改用户信息")
	public void updateProfile(@Param(remark = "头像") String headIcon,
							  @Param(remark = "性别") Integer sex,
							  @Param(remark = "昵称") String nickName,
							  @Param(remark = "所在地") String area,
							  @Param(remark = "签名") String signature) {
		SystemParam param = getSystemParam();
		SimpleUserDto userInfo = userProfileService.updateUserProfile(param, headIcon, sex, nickName, area, signature);
		super.renderJson(userInfo);
	}

	@ResponseBody
	@RequestMapping("/getUserInfo")
	@ApiMethod("获取用户信息")
	public void getUserInfo(HttpServletRequest request){
		SystemParam systemParam = super.getSystemParam();
//		Long userId = systemParam.getUserId();
//		String token = systemParam.getAppHeaderInfo().getUserToken();
//		String headerToken = request.getHeader("user_token");
//		String paramToken = request.getParameter("user_token");
//		log.error("[jay] userId : "+userId +", token :"+token+",headerToken:"+headerToken+",paramToken:"+paramToken);
		SimpleUserDto userInfo = userProfileService.getUserInfo(systemParam);
		super.renderJson(userInfo);
	}

	@ResponseBody
	@RequestMapping("/follow")
	@ApiMethod("关注")
	public void follow(@Param(remark = "被关注用户ID") @NotNull long beFollowUserId ){
		userProfileService.follow(super.getSystemParam(),beFollowUserId);
		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/unfollow")
	@ApiMethod("取消关注")
	public void unfollow(@Param(remark = "被关注用户ID") @NotNull long beFollowUserId ){
		userProfileService.unfollow(super.getSystemParam(),beFollowUserId);
		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/getMyFollow")
	@ApiMethod(value = "我关注的",paging = true)
	public void getMyFollow(){
		super.renderJson(userProfileService.getMyFollow(super.getSystemParam()));
	}

	@ResponseBody
	@RequestMapping("/getMyFans")
	@ApiMethod(value = "我的粉丝",paging = true)
	public void getMyFans(){
		super.renderJson(userProfileService.getMyFans(super.getSystemParam()));
	}

	@ResponseBody
	@RequestMapping("/getMyFollowGames")
	@ApiMethod(value = "我收藏的游戏",paging = true)
	public void getMyFollowGames(){
		super.renderJson(gameInfoService.getUserFllowGames(super.getSystemParam(), UserUtil.getUserId()));
	}


	@ResponseBody
	@RequestMapping("/bindPhone")
	@ApiMethod(value = "绑定手机号码")
	public void bindPhone(@Param(remark = "手机号") @NotNull String phone,
						  @Param(remark = "验证码") @NotNull String validateCode){
		userProfileService.bindPhone(super.getSystemParam(),phone,validateCode);
		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/deletePlayGame")
	@ApiMethod(value = "删除玩过的游戏")
	public void deletePlayGame(@Param(remark = "应用ID") @NotNull Long gameId){
		userGameListService.delete(UserUtil.getUserId(),gameId);
		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/getPlayGames")
	@ApiMethod(value = "获取玩过的游戏",paging = true)
	public void getPlayGames(){
		Page<GameInfoDto> page = userGameListService.getPlayGamePage(super.getSystemParam(),UserUtil.getUserId());
		super.renderJson(page);
	}

	@ResponseBody
	@RequestMapping("/submitPlayGames")
	@ApiMethod(value = "提交玩过的游戏")
	public void submitPlayGames(@NotNull @Param(remark = "包名(json数组)") String packageNames){
		List<String> names = JSONArray.parseArray(packageNames,String.class);
		if(names.size() != 0){
			userGameListService.submitAppList(UserUtil.getUserId(),names);
		}
		super.renderSuccess();
	}

	@ResponseBody
	@RequestMapping("/modifyPassword")
	@ApiMethod(value = "修改密码")
	public void modifyPassword(@NotNull @Param(remark = "旧密码") String oldPwd,@NotNull @Param(remark = "新密码") String newPwd){
		userAccountService.modifyPassword(UserUtil.getUserId(),oldPwd,newPwd);
		super.renderSuccess();
	}
}
