package cc.ccplay.booster.app.web.mu;

import cc.ccplay.booster.app.web.BaseAppController;
import cc.ccplay.booster.base.annotation.NoSignature;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.api.content.BoosterServerOnlineService;
import cc.ccplay.booster.base.dto.content.booster.ServerPortStatusDto;
import cc.ccplay.booster.base.dto.content.booster.ServerStatusDto;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.log4j.Logger;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RequestMapping("/mu")
@Controller
public class NodeController extends BaseAppController {

    @Reference
    private BoosterServerOnlineService boosterServerOnlineService;

    private static final Logger logger = Logger.getLogger(NodeController.class);

    @NoSignature
    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/nodes/{id}/online_count")
    public void onlineCount(HttpServletRequest request, @PathVariable("id") long nodeId, @Param Long count){
        //boosterServerOnlineService.updateOlineCount(nodeId,count);
        //logger.error("[节点："+nodeId+"]在线人数"+count);
        super.renderSuccess();
    }

    @NoSignature
    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/nodes/{id}/info")
    public void info(HttpServletRequest request, @PathVariable("id") long nodeId){
        //"load":["0.00 0.01 0.05 1/100 21341"],"uptime":["1451064.42"]
        //logger.error("[节点info："+nodeId+"]"+JSONObject.toJSONString(request.getParameterMap()));
        super.renderSuccess();
    }


    @NoSignature
    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/nodes/{id}/serverStatus")
    public void serverStatus(HttpServletRequest request,@NotNull String pass,
        @Param String data, @Param Long pid,@Param String uptime,
        @PathVariable("id") long nodeId){
        //logger.error("[节点info："+nodeId+"]"+JSONObject.toJSONString(request.getParameterMap()));
        ServerStatusDto statusDto = new ServerStatusDto();
        statusDto.setUptime(uptime);
        statusDto.setServerId(nodeId);
        if(pid != null){
            List<ServerPortStatusDto> portList = JSONArray.parseArray(data,ServerPortStatusDto.class);
            statusDto.setPorts(portList);
            statusDto.setPid(pid);
        }
        boosterServerOnlineService.updateSeverStatus(statusDto);
        super.renderSuccess();
    }
}
