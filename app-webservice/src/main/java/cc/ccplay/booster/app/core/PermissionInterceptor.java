package cc.ccplay.booster.app.core;

import cc.ccplay.booster.app.exception.TokenExpireException;
import cc.ccplay.booster.base.annotation.InnerServerApi;
import cc.ccplay.booster.base.annotation.NoSignature;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.annotation.SortMapping;
import cc.ccplay.booster.base.core.AppHeaderInfo;
import cc.ccplay.booster.base.core.Header;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.util.Md5Util;
import cc.ccplay.booster.base.util.UserUtil;
import org.apache.log4j.Logger;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.exception.NoLoginException;
import org.easyj.frame.exception.PermissionCheckException;
import org.easyj.frame.util.StringUtil;
import org.easyj.frame.util.SysUtil;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

public class PermissionInterceptor extends HandlerInterceptorAdapter {

	private final static Logger log = Logger.getLogger(PermissionInterceptor.class);

	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
	}

	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object arg2, ModelAndView view)
			throws Exception {

	}

	private void createPaggingSortParamter(SortMapping sm,HttpServletRequest request){
		String mapping[] = sm.mapping();
		String sortType = request.getParameter("sortOrder");
		String sortName = request.getParameter("sortName");
		String defColumn = sm.defColumn();
		String defSortType = sm.defSortType();
		if(StringUtil.isEmpty(sortName)){//前端未传参数过来，使用默认排序字段
			if(StringUtil.isNotEmpty(defColumn)){
				UserUtil.setSortInfo(defColumn, defSortType);
			}
		}else{
			int size = mapping.length / 2;
			for (int i = 0; i < size ; i++) {
				String sn = mapping[i * 2];
				if(org.apache.commons.lang3.StringUtils.equals(sn, sortName)){
					UserUtil.setSortInfo(mapping[i * 2 + 1], sortType);
					break;
				}
			}
		}
	}
	
	private void checkInnerServerApi(HttpServletRequest request, HttpServletResponse response){
		String sign = request.getHeader("innerServerApi");
		if("dDQOjmXpfegYpp75".equals(sign) == false){
			throw new GenericException("授权失败");
		}
	}


	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		// 处理Permission Annotation，实现方法级权限控制
		HandlerMethod method = (HandlerMethod) handler;

		Class beanType = method.getBeanType();
		if(beanType.getAnnotation(InnerServerApi.class) != null){
			checkInnerServerApi(request,response);
			return true;
		}
		NoSignature noSignature = method.getMethodAnnotation(NoSignature.class);

		//校验签名
		if(noSignature == null) {
			checkSign(request);
		}

		//创建分页参数
		SortMapping sm = method.getMethodAnnotation(SortMapping.class);
		if(sm != null){
			createPaggingSortParamter(sm,request);
		}
		// 需登陆才能访问
		NotNeedLogin nnl = method.getMethodAnnotation(NotNeedLogin.class);
		if (nnl == null && !SysUtil.isLogin()) {
			SystemParam param = (SystemParam) SysUtil.getCustomData();
			if(param == null){
				throw new NoLoginException();
			}
			AppHeaderInfo headerInfo = param.getAppHeaderInfo();
			if(headerInfo == null){
				throw new NoLoginException();
			}
			//token过期
			if(StringUtil.isNotEmpty(headerInfo.getUserToken())){
				throw new TokenExpireException();
			}else{
				throw new NoLoginException();
			}
		}
		return true;
	}


	private void checkSign(HttpServletRequest request){
		String action = request.getServletPath();
//		if(action.equals("/apitest") || action.equals("/apitest/doApiTest")){
//			return;
//		}
		SystemParam param = (SystemParam) SysUtil.getCustomData();
		AppHeaderInfo headerInfo = param.getAppHeaderInfo();
		//内部测试使用，不校验签名
		if(Const.testAppKey.equals(headerInfo.getApiKey())){
			return;
		}

		String postSign = headerInfo.getApiSign();
		if(StringUtil.isEmpty(postSign)) {
			postSign = request.getParameter(Header.API_SIGN.getKey());
		}

		if(StringUtil.isEmpty(postSign)){
			throw new PermissionCheckException("签名有误");
		}

		Map<String,String[]> map = request.getParameterMap();

		Map<String, String[]> treeMap = new TreeMap<String, String[]>(
			new Comparator<String>() {
				public int compare(String obj1, String obj2) {
					// 降序排序
					return obj1.compareTo(obj2);
				}
		});

		for (String key : map.keySet()){
			if(Header.API_SIGN.getKey().equals(key)){
				continue;
			}
			String [] values = map.get(key);
			if(values == null || values.length == 0){
				continue;
			}
			if(values.length == 1){
				String v = values[0];
				if(v.length() == 0){
					continue;
				}
			}
			treeMap.put(key,map.get(key));
		}
		treeMap.put("apiKey", new String[]{Const.apiKey});

		StringBuilder sb = new StringBuilder();

		for(String key : treeMap.keySet()){
			String [] values = treeMap.get(key);
			if(values.length == 1) {
				sb.append(key + "=" + values[0]+"&");
			}else{
				for (int i = 0; i < values.length; i++) {
					String v = values[i];
					if(v.length() == 0){
						sb.append(key + "=" + v+"&");
					}
				}
			}
		}
		sb.append(action);
		sb.append("&");
		String userToken = headerInfo.getUserToken();
		if(StringUtil.isNotEmpty(userToken)){
			sb.append(headerInfo.getUserToken());
			sb.append("&");
		}
		sb.append(Const.appSecret);
		String sign = Md5Util.md5(sb.toString());
		if(!sign.equalsIgnoreCase(postSign)){
			log.error("[签名错误] 加密串： "+sb.toString() + " , 生成sign："+ sign+",请求sign: "+postSign);
			throw new PermissionCheckException("签名有误");
		}
	}

}
