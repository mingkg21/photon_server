package cc.ccplay.booster.app.web;

//import net.xianyo.backend.util.UserUtil;

import cc.ccplay.booster.app.core.StatusCode;
import cc.ccplay.booster.base.core.AppHeaderInfo;
import cc.ccplay.booster.base.core.SystemParam;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.ActionUtil;
import org.easyj.frame.util.MixUtil;
import org.easyj.frame.util.SysUtil;

import java.util.HashMap;
import java.util.Map;

public class BaseAppController extends ActionUtil {

	public static final String PARAM_DATA = "data";

	public static final String PARAM_MSG = "msg";

	public static final String PARAM_CODE = "code";

	//public static final String PARAM_RESPONSE_TIME = "time";

	public SystemParam getSystemParam() {
		SystemParam param = (SystemParam) SysUtil.getCustomData();
		return param;
	}

	public static void renderSuccess() {
		renderJson(MixUtil.newHashMap(
				PARAM_MSG, "成功",
				PARAM_CODE, StatusCode.SUCCESS.getCode()
		));
	}

	public static void renderJson(Page page) {
		Map info = new HashMap();
		info.put(PARAM_MSG, "成功");
		info.put(PARAM_CODE, StatusCode.SUCCESS.getCode());

		Map pageMap = new HashMap();
		pageMap.put("list", page.getList());
		pageMap.put("totalCount",page.getTotalCount());
		pageMap.put("totalPage",page.getTotalPageCount());
		pageMap.put("currentPage",page.getCurrentPageNo());
		pageMap.put("hasNext",page.isHasNext() ? 1 : 0);
		//pageMap.put(FrameProperites.PAGING_JSON_TOTAL, page.getTotalCount());
		Map<String, Object> aggsData = page.getAggsData();
		if (aggsData != null && aggsData.size() > 0) {
			pageMap.put("aggsData", aggsData);
		}
		info.put(PARAM_DATA, pageMap);
		//info.put(PARAM_RESPONSE_TIME,System.currentTimeMillis());
		renderJson(info);
	}

	public static void renderJson(final Object object) {
		Map info = new HashMap();
		info.put(PARAM_MSG, "成功");
		info.put(PARAM_CODE, StatusCode.SUCCESS.getCode());
		//info.put(PARAM_RESPONSE_TIME,System.currentTimeMillis());
		info.put(PARAM_DATA, object);
		renderJson(info);
	}

	public static void renderFailure(String msg) {
		Map info = new HashMap();
		info.put(PARAM_MSG, msg);
		info.put(PARAM_CODE, StatusCode.FAILURE.getCode());
		//info.put(PARAM_RESPONSE_TIME,System.currentTimeMillis());
		renderJson(info);
	}

	/**
	 * 输出失败响应
	 */
	public static void renderFailure() {
		renderFailure("操作失败");
	}

	/**
	 * 获取设备名
	 */
	public String getModelName(){
		SystemParam param = this.getSystemParam();
		if(param == null){
			return null;
		}
		AppHeaderInfo appHeaderInfo = param.getAppHeaderInfo();
		if(appHeaderInfo == null){
			return null;
		}
		return appHeaderInfo.getModelName();
	}

	/**
	 * 获取设备名
	 */
	public String getOsVersion(){
		SystemParam param = this.getSystemParam();
		if(param == null){
			return null;
		}
		AppHeaderInfo appHeaderInfo = param.getAppHeaderInfo();
		if(appHeaderInfo == null){
			return null;
		}
		return appHeaderInfo.getOsVersion();
	}

}
