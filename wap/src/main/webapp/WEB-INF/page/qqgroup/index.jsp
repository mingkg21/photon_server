<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no,minimal-ui">
	<title>18游戏盒QQ群</title>
	<link rel="stylesheet" href="${base}/css/qqgroup.css?v=20180806">
	<script src="${base}/js/font.js"></script>
</head>
<body>
	<div class="wrap">
		<div class="main">
			<div class="list">

				<c:forEach var="item" items="${list}">
					<div class="item">
						<div class="item-wrap">
							<img src="${cdn}/${item.icon}" alt="">
							<h2>${item.title}</h2>
							<p>QQ群：${item.groupNumber}</p>
							<p>${item.description}</p>
							<div class="item-btn">
								<a href="javascript:void(0)" class="btn-join" data-qq="${item.key}"></a>
								<a href="javascript:void(0)" class="btn-copy" data-clipboard-text="${item.groupNumber}">复制</a>
							</div>
						</div>
					</div>
				</c:forEach>

			</div>
		</div>
	</div>

	<div class="tips">复制成功</div>

	<script src="${base}/js/zepto.min.js"></script>
	<script src="${base}/js/clipboard.min.js"></script>
</body>
<script>
	$(document).ready(function(){  
		$(".btn-join").on("click", function(){
			var key = $(this).attr("data-qq");
			if (window.shiba_qqgroup) {
				window.shiba_qqgroup.join(key);
			}
		})

		var timer = null;
		$(".btn-copy").each(function(){
			var clipboard = null;
			clearInterval(timer);
			clipboard = new Clipboard($(this)[0]);  
	        clipboard.on('success', function(e) {
				clearInterval(timer);
				$(".tips").text("复制成功").show();
				timer = setTimeout(function(){
					$(".tips").hide();
				}, 1000)
		    });
		    clipboard.on('error', function(e) {
		       	$(".tips").text("复制失败").show();
		    });	
		})
	})
</script>
</html>