<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="renderer" content="webkit">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<title>关于我们</title>
<script>
	;(function (doc, win) {
	    var docEl = doc.documentElement,
	        resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize';
	    function recalc() {
	        var clientWidth = docEl.clientWidth;
	        if (!clientWidth) return;
	        var size = (clientWidth / 720) * 100;
	        docEl.style.fontSize = (size < 100 ? size : 100) + 'px';
	        // docEl.style.fontSize = (clientWidth / 640) * 100 + 'px';
	        
	        var realfz = window.getComputedStyle(document.getElementsByTagName("html")[0]).fontSize.replace('px','')
	        if (size !== realfz) {
	            document.getElementsByTagName("html")[0].style.cssText = 'font-size: ' + size * (size / realfz) +"px";
	        }
	    };
	     
	    if (!doc.addEventListener) return;
	    win.addEventListener(resizeEvt, recalc, false);
	    doc.addEventListener('DOMContentLoaded', recalc, false);
	    recalc();
	})(document, window);
</script>
<style>
	*{margin: 0;padding: 0;}
	body {color:#333; -webkit-user-select: none; -webkit-text-size-adjust: none; font-family: HelveticaNenu, Helvetica, sans-serif; moz-user-select: -moz-none; -moz-user-select: none; -o-user-select: none; -khtml-user-select: none; -webkit-user-select: none; -ms-user-select: none; user-select: none; line-height:140%;font-size:0.28rem;}
	.about {
		padding: 1.34rem 0 0 0;
		min-height: 4.23rem;
		background: url(${base}/images/app/bg-about.png) center top no-repeat;
		background-size: 100%;
		text-align: center;
	}
	.logo {
		display: block;
		margin: 0 auto 0.28rem;
		width: 2rem;
		height: 2rem;
	}
	.name {
		margin: 0 0 0.12rem;
		font-size: 0.3333rem;
		color: #d33d3e;
		line-height: 1.2;
	}
	.version {
		font-size: 0.2667rem;
		color: #999999;
		line-height: 1.2;
	}
	.about-txt {
		padding: 0.38rem;
		margin: 1.3rem auto 0;
		width: 84%;
		border: 1px solid #f6d8d8;
		border-radius: 0.3rem;
		box-sizing: border-box;
	}
	.about-txt p {
		font-size: 0.3067rem;
		color: #333;
		line-height: 0.44rem;
	}
</style>
</head>
<body>
	<div class="about">
		<img src="${base}/images/app/logo.png" alt="LOGO" class="logo">
		<p class="name">光子加速器</p>
		<c:if test="${versionCode != null}">
			<p class="version">V${versionCode}</p>
		</c:if>
		<div class="about-txt">
			<p>光子加速器为国内用户提供畅通的外服内服竞技对战类游戏加速</p>
		</div>
	</div>
</body>
<script>
	
</script>
</html>
