<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <title>用户服务协议</title>
    <style>
        body {
            color: #333;
            -webkit-user-select: none;
            -webkit-text-size-adjust: none;
            font-size: 90%;
            font-family: HelveticaNenu, Helvetica, sans-serif;
            moz-user-select: -moz-none;
            -moz-user-select: none;
            -o-user-select: none;
            -khtml-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
            user-select: none;
            line-height: 140%
        }

        .red {
            color: #F00
        }

        #btn,
        #joinQQ {
            color: #d33d3e;
        }

        .faq_answer {
            /*display: none;*/

        }

        .faq_answer dd {
            margin-left: 0;
            color: #333;
            line-height: 24px;
            font-weight: normal;
        }

        .faq_item {
            overflow: hidden;
            height: 40px;
            font-weight: bold;
            margin-left: 0;
            cursor: hand;
            cursor: pointer;
            transition: all 500ms ease-in;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        .faq_item_open {
            height: auto;
        }

        .faq_item dt {
            text-overflow: ellipsis;
            overflow: hidden;
            white-space: nowrap;
            height: 40px;
            line-height: 40px;
        }

        .faq_item:hover {

        }
    </style>
</head>

<body>
<div id="app">
    <div class="faq_item faq_item_open">
        <dt>1.加速器是什么？为什么游戏速度没有变快？</dt>
        <div class="faq_answer">
            <dd>光子加速器采用智能的网络加速技术，为您解决移动设备下的网络质量问题。加速器可以为您解决跨区域、跨线路的穿透，更多的体现在线路的稳定上，使您更稳定，更顺畅的体验游戏。</dd>
        </div>
    </div>

    <div class="faq_item faq_item_open">
        <dt>2.遇到了问题，如何反馈？</dt>
        <div class="faq_answer">
            <dd>可以点击右下角【我的】，登录后，选择【意见反馈】把问题描述反馈给我们。</dd>
        </div>
    </div>

    <div class="faq_item faq_item_open">
        <dt>3.游戏必须在光子加速器中下载才能体验游戏加速功能吗？</dt>
        <div class="faq_answer">
            <dd>不是的，光子加速器游戏库并不丰富，您可以在google play或者国内任意的应用商店下载游戏，下载安装后打开加速器到【加速】菜单中即可看到可支持加速的游戏。</dd>
        </div>
    </div>

    <div class="faq_item faq_item_open">
        <dt>4.找不到想玩的游戏怎么办？</dt>
        <div class="faq_answer">
            <dd>您可以在google play或者国内任意的应用商店下载游戏，下载完游戏后，若加速器支持，则在【加速】菜单里会出现这款游戏，如果不支持，可通过【我的】，在底部找到【告诉我们】，点击进入填写告诉我们。</dd>
        </div>
    </div>

    <div class="faq_item faq_item_open">
        <dt>5.加速一直不成功怎么办？</dt>
        <div class="faq_answer">
            <dd>如果您使用wifi加速，请确认您连接的wifi有外网访问权限。如果您使用的是公共wifi（如移动CMCC），可能需要联系wifi供应商获取外网访问权限，一般是打开一个网页填写认证信息。<br>
                如果您使用移动数据加速，请确认您的花费套餐当月还有流量或者余额可以使用。若您没有打开wifi或者移动数据，请到系统设置中打开至少其中一个。如果不是以上任何情况，可能您处的位置网络信号较差，建议您转移到信号较好的地方或用wifi/移动数据其中信号较好的一种方式来加速。<br>
                如果您打开了其他VPN类的应用，请在后台关闭这些应用，同时在关闭后确认您当前的系统VPN连接是关闭的，然后再打开光子加速器进行加速。
            </dd>
        </div>
    </div>

    <div class="faq_item faq_item_open">
        <dt>6.游戏掉线怎么办？</dt>
        <div class="faq_answer">
            <dd>掉线时请确认光子加速器是否在后台运行，如果加速器在后台被关闭，请在应用管理设置中“允许光子加速器后台运行”。</dd>
        </div>
    </div>

    <div class="faq_item faq_item_open">
        <dt>7.游戏黑屏、停留在初始页面、闪退怎么办？</dt>
        <div class="faq_answer">
            <dd>1）请先确认安卓系统是否符合游戏最低配置，部分游戏有机型要求，请确认机型是否合适。</dd>
            <dd>2）请确认是否有安装谷歌三件套。部分游戏需要谷歌账号和谷歌框架才能进行游戏。</dd>
        </div>
    </div>

    <div class="faq_item faq_item_open">
        <dt>8.如何确定光子加速器在后台还在运行？</dt>
        <div class="faq_answer">
            <dd>请查看手机通知栏是否有钥匙图标或VPN图标。若没有则表示光子加速器已退出后台。</dd>
        </div>
    </div>

    <div class="faq_item faq_item_open">
        <dt>9.无法扫描本地游戏怎么办？</dt>
        <div class="faq_answer">
            <dd>1）请确认是否开启“读取应用列表”权限。（设置-应用管理-光子加速器-权限管理-“允许扫描本地应用/读取应用列表”）</dd>
            <dd>2）若游戏没有出现在【加速】菜单中，可能该游戏不在光子支持的加速名单中，请反馈给我们，我们一般会在12个小时内完成测试加上。</dd>
        </div>
    </div>

    <div class="faq_item faq_item_open">
        <dt>10.为什么无法用facebook/twitter登录游戏？</dt>
        <div class="faq_answer">
            <dd>光子加速器仅提供游戏加速服务，不提供国外社交网络加速服务。若需要国外社交账号登录，请先用其他VPN登录游戏，再用光子加速器加速游戏。</dd>
        </div>
    </div>

    <p>若有其他问题，可以<a href="javascript:void(0)" id="btn">意见反馈</a>告诉我们<br>也可以加入光子加速器用户群：<a href="javascript:void(0)" id="joinQQ">718763122</a></p>
</div>

</body>
<script src="http://i3.resource.ccplay.cc/developer/static/js/jQuery.js"></script>
<script>
    document.getElementById("btn").onclick = function () {
        window.cc_speed.feedback();
    }

    document.getElementById("joinQQ").onclick = function () {
        var str = "mqqopensdkapi://bizAgent/qm/qr?url=http%3A%2F%2Fqm.qq.com%2Fcgi-bin%2Fqm%2Fqr%3Ffrom%3Dapp%26p%3Dandroid%26k%3DRqBlqLODePeHYJ2qpW3bIslX52y1izs6";
        window.cc_speed.joinQQGroup(str);
    }
    $(".faq_item").each(function () {
        $(this).on("click", function (e) {
            e.preventDefault();
            var status = $(this).hasClass("faq_item_open");
            if (!status) {
                $(this).addClass("faq_item_open");
            } else {
                $(this).removeClass("faq_item_open");
            }
        })
    })
</script>

</html>