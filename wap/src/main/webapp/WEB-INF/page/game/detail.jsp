<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="keywords" content="">
	<meta name="description" content=" ">
	<meta name="viewport" content="width=device-width,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no,minimal-ui">
	<meta content="yes" name="apple-mobile-web-app-capable" /> 
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
	<meta content="telephone=no" name="format-detection" /> 
	<title>游戏详情</title>
	<link rel="stylesheet" href="${base}/css/swiper.min.css">
	<link rel="stylesheet" href="${base}/css/game_detail.css?v=6">
	<!-- <link rel="shortcut icon" href="images/pics.jpgon.ico"> -->
	<script src="${base}/js/font.js"></script>
</head>
<body>
	<div class="wrap">

		<c:if test="${dto.gameInfo.video == null}">
			<div class="banner" style="background:url(${dto.gameInfo.banner.cndSrc}) center no-repeat;background-size:cover;">

			</div>
		</c:if>

		<c:if test="${dto.gameInfo.video != null}">
			<div class="banner">
				<video poster="${dto.gameInfo.banner.cndSrc}" src="${dto.gameInfo.video.cndSrc}" controls="controls"></video>
			</div>
		</c:if>

		<div class="main">
			<div class="game-top">
				<div class="game-head">

					<img class="game-pic" src="${dto.versionInfo.icon.cndSrc}" alt="">

					<p class="game-name text-overflow">${dto.gameInfo.name}</p>
					<p class="game-opts">
						<span class="file-size" data-value="${dto.versionInfo.fileSize}"></span> /
						<span>${dto.versionInfo.lang}</span>
					</p>
					<p class="game-source">${dto.publisher.name}</p>

					<div class="game-score center" data-value="${dto.gameInfo.score}">
					</div>
				</div>

				<div class="game-text overflow">
					<p>${dto.gameInfo.devRecommend}</p>
				</div>
			</div>

			<div class="game-detail">
				<div class="game-detail-title center">游戏详情</div>
				<c:if test="${not empty dto.versionInfo.adapterInfo}">
					<div class="game-detail-jr center">${dto.versionInfo.adapterInfo}</div>
				</c:if>
				
				<div class="game-version">
					<div class="game-alert">
						<p class="">${dto.gameInfo.editorRecommend}</p>
					</div>

					<div class="common-tit">
						版本说明 <span class="sm">V${dto.versionInfo.versionName}</span> <span class="sm"><fmt:formatDate value="${dto.versionInfo.createTime}" pattern="yyyy-MM-dd"/></span>
					</div>

					<c:if test="${not empty dto.versionInfo.updateLog}" >
						<div class="version-txt game-info-txt three-overflow">
							<div class="text-wrap">
								<pre>${dto.versionInfo.updateLog}</pre>
							</div>

							<a href="javascript:void(0)" class="btn-open">展开</a>
						</div>
					</c:if>
					<div class="pic-box">

						<c:if test="${!flag}">
							<div class="swiper-container hp">
								<div class="swiper-wrapper">
									<c:forEach items="${dto.pictures}" var="item">
										<div class="swiper-slide">
											<img src="${item.src.cndSrc}" alt="">
										</div>
									</c:forEach>
								</div>
							</div>
						</c:if>

						<c:if test="${flag}">
							<div class="swiper-container sp">
								<div class="swiper-wrapper">
									<c:forEach items="${dto.pictures}" var="item">
										<div class="swiper-slide">
											<img src="${item.src.cndSrc}" alt="">
										</div>
									</c:forEach>
								</div>
							</div>
						</c:if>
					</div>

					<div class="game-labels clearfix">
						<c:forEach items="${dto.tags}" var="tag">
							<a href="${base}/game/listByTag/${tag.id}?tagName=${tag.name}" class="center fl">${tag.name}</a>
						</c:forEach>
					</div>
				</div>	

				<div class="game-version">
					<div class="common-tit">
						游戏介绍
					</div>

					<div class="desc-txt game-info-txt three-overflow">
						<div class="text-wrap">
							<pre>${dto.gameInfo.description}</pre>
						</div>
						<a href="javascript:void(0)" class="btn-open">展开</a>
					</div>
				</div>

				<c:if test="${fn:length(relatedList) > 0}">
					<div class="game-relate">
						<div class="common-tit">
							相关游戏
						</div>

						<div class="game-box">
							<c:forEach items="${relatedList}" var="gameDto">
								<div class="game-item">
									<div class="game-item-wrap">
										<img src="${gameDto.versionInfo.icon.cndSrc}" class="game-item-pic" alt="">
										<div class="game-item-content">
											<div class="game-item-name text-overflow">${gameDto.gameInfo.name}</div>
											<div class="game-item-opts clearfix">
												<span class="game-item-label center fl">${gameDto.versionInfo.lang}</span>
												<span class="game-item-label center fl file-size" data-value="${gameDto.versionInfo.fileSize}"></span>
											</div>
											<div class="game-item-desc text-overflow">${gameDto.gameInfo.devRecommend}</div>
										</div>
										<div class="game-download">
											<a href="javascript:void(0);" onclick="doDownload('${fn:replace(gameDto.gameInfo.name,'\'','\\\'')}')" class="btn-game-download center">下载</a>
											<p class="comment-num">${gameDto.gameInfo.commentCount}评价</p>
										</div>
									</div>
								</div>
							</c:forEach>
						</div>
					</c:if>
				</div>
			</div>
		</div>
		<a href="javascript:void(0);" onclick="doDownload('${fn:replace(dto.gameInfo.name,'\'','\\\'')}')" class="btn-fixed center">立即下载</a>
	</div>

	<script src="${base}/js/zepto.min.js"></script>
	<script src="${base}/js/swiper.min.js"></script>
	<jsp:include page="../../include/download.jsp"></jsp:include>
</body>
<script>


    function formatFileSize(fileByte){
        if(!fileByte){
            return "-";
        }
        var fileSizeByte = fileByte;
        var fileSizeMsg = "";
        if (fileSizeByte < 1048576) fileSizeMsg = (fileSizeByte / 1024).toFixed(2) + "KB";
        else if (fileSizeByte == 1048576) fileSizeMsg = "1MB";
        else if (fileSizeByte > 1048576 && fileSizeByte < 1073741824) fileSizeMsg = (fileSizeByte / (1024 * 1024)).toFixed(2) + "MB";
        else if (fileSizeByte > 1048576 && fileSizeByte == 1073741824) fileSizeMsg = "1GB";
        else if (fileSizeByte > 1073741824 && fileSizeByte < 1099511627776) fileSizeMsg = (fileSizeByte / (1024 * 1024 * 1024)).toFixed(2) + "GB";
        else fileSizeMsg = "超过1TB";
        return fileSizeMsg;
    }

	$(".file-size").each(function () {
		var $span = $(this);
        var fileSize = parseInt($span.data("value"));
        $span.html(formatFileSize(fileSize));
	})

	var $gameScore = $(".game-score");
    var score = parseFloat($gameScore.data("value"));
    $gameScore.html('<span>'+parseInt(score)+'</span>'+
	'.<span class="sm" >'+(parseInt(score * 10) % 10)+'</span>');





	<c:if test="${flag}">
    var type = 2;
    </c:if>
	<c:if test="${!flag}">
	var type = 1;
    </c:if>
	if (type == 1) {
		$(".sp").hide();
		$(".hp").show();
			// 横屏轮播图
		var mySwiper = new Swiper ('.swiper-container', {
		    //loop: true,
		    autoplay: 3000,
		    slidesPerView: 1.1,
		    spaceBetween: 10,
		    paginationClickable :true,
		    autoplayDisableOnInteraction: false,
		    preloadImages: true
		}) 	
	} else {
		$(".sp").show();
		$(".hp").hide();
			// 竖屏轮播图
		var mySwiper = new Swiper ('.swiper-container', {
		    //loop: true,
		    autoplay: 3000,
		    slidesPerView: 1.75,
		    spaceBetween: 18,
		    paginationClickable :true,
		    autoplayDisableOnInteraction: false,
		    preloadImages: true
		})
	}

	;(function(){
		$(".game-info-txt").each(function(){
			var lineHeight = parseInt($(this).find('pre').css("line-height"));
			var h = parseInt($(this).find('pre').height());
			//console.log(h / lineHeight);
			if (h / lineHeight > 3) {
				$(this).find(".btn-open").show();
			} else {
				$(this).find(".btn-open").hide();
			}
		})
		
	})()

	  

	$(".btn-open").on("click", function(){
		$(this).parents(".game-info-txt").removeClass("three-overflow");
		$(this).remove();
	})
</script>
</html>























