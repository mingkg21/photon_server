<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="keywords" content="">
    <meta name="description" content=" ">
    <meta name="viewport"
          content="width=device-width,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no,minimal-ui">
    <meta content="yes" name="apple-mobile-web-app-capable"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta content="telephone=no" name="format-detection"/>
    <title>游戏列表</title>
    <link rel="stylesheet" href="${base}/css/game_detail.css?v=6">
    <script src="${base}/js/font.js"></script>
    <script src="${base}/js/util.js"></script>
    <script src="${base}/js/artTemplate/template.js"></script>
</head>
<body>
    <div class="wrap list-wrap">
        <div class="common-head">
            <a href="javascript:history.go(-1)" class="btn-back"></a>
            <h1>${tagName}</h1>
        </div>

        <div class="main">
            <div class="game-detail">
                <div class="game-relate">
                    <div class="game-box">

                    </div>

                    <div class="game-more">
                        <a id="readMore" href="javascript:void(0)">点击加载更多</a>
                        <a id="nodata" style="display: none;" href="javascript:void(0)">无更多数据</a>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <script src="${base}/js/zepto.min.js"></script>
    <jsp:include page="../../include/download.jsp"></jsp:include>
</body>
</html>


<script id="game_tpl" type="text/html">
    {{each rows as item i}}
        <div class="game-item">
            <div class="game-item-wrap">
                <a href="${base}/game/detail/{{item.id}}"><img src="http://cdn.18hanhua.com/{{item.icon}}-160" class="game-item-pic" alt=""></a>
                <div class="game-item-content">
                    <div class="game-item-name text-overflow">{{item.name}}</div>
                    <div class="game-item-opts clearfix">
                        <span class="game-item-label center fl">{{item.lang}}</span>
                        <span class="game-item-label center fl">{{$formatSize item.fileSize}}</span>
                    </div>
                    <div class="game-item-desc text-overflow">{{item.devRecommend}}</div>
                </div>
                <div class="game-download">
                    <a href="javascript:void(0)" onclick="doDownload('{{item.name}}')" class="btn-game-download center">下载</a>
                    <p class="comment-num">{{item.commentCount}} 评价</p>
                </div>
            </div>
        </div>
    {{/each}}
</script>


<script language="JavaScript">
    template.helper('$formatSize', function (size) {
        return Util.formatFileSize(size);
    });

    var loading = false;
    var currentPage = 0;
    function loadNextPage(){
        var pageSize = 10;
        if(loading){
            return;
        }
        var pageNo = ++currentPage;
        loading = true;
        $("#readMore").html("正在加载中...");
        $.ajax({
            type:'POST',
            url : '${base}/game/getGamesByTag/${tagId}',
            data:{
                pageNumber: pageNo,
                pageSize:pageSize
            },
            dataType:'json',
            success:function(data){
                currentPage = pageNo;
                var html = template('game_tpl', data);
                $(".game-box").append(html);
               // document.getElementById('content').innerHTML = html;

                if(pageSize * pageNo >= data.total) {
                    $("#nodata").show();
                    $("#readMore").hide();
                }

            },
            errer:function(){
                alert("数据加载失败");
            },
            complete:function(){
                loading = false;
                $("#readMore").html("点击加载更多");
            }
        })
    }
    loadNextPage();
    $("#readMore").click(function(){
        loadNextPage();
    });
</script>



