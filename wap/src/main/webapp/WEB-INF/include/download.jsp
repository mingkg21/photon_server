<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
    String ua = request.getHeader("User-Agent").toLowerCase();
    boolean isWechat = ua.indexOf("micromessenger") > -1;
    request.setAttribute("isWechat",isWechat);
%>

<div class="modal" id="download-modal">
    <div class="modal-mask"></div>
    <div class="modal-content">
        <div class="download-modal">
            <div class="d-tit">请安装18游戏盒下载该游戏</div>
            <p class="d-txt">在18游戏盒中搜索<strong class="game-name"></strong>，然后下载该游戏</p>
            <div class="d-con">
                <img src="${base}/images/qrcode.png?v=1" alt="">
                <div class="d-sb">
                    <h3>扫描二维码下载18游戏盒</h3>
                    <p>
                        <i class="icon icon-phone"></i>游戏爱好者的APP
                    </p>
                    <p>
                        <i class="icon icon-file"></i>每天推荐精选游戏
                    </p>
                    <p>
                        <i class="icon icon-newgame"></i>抢先体验内测新游
                    </p>
                </div>
            </div>

            <div class="d-btn">
                <a onclick="window.top.location.href='http://app.18hanhua.com'" href="javascript:void(0)" class="center fl">了解18游戏盒</a>

                <c:if test="${isWechat}">
                    <a href="http://sj.qq.com/myapp/detail.htm?apkName=com.gamebox.shiba" class="center fr">立即下载</a>
                </c:if>
                <c:if test="${!isWechat}">
                    <a href="${base}/download" class="center fr">立即下载</a>
                </c:if>
            </div>

            <a href="javascript:void(0)" class="btn-close"></a>
        </div>
    </div>
</div>

<script>

    var $modal = $("#download-modal");
    function doDownload(title){
        $modal.find(".game-name").html(title);
        $modal.show();
    }

    $modal.find(".btn-close").on("click", function(){
        $modal.hide();
    });

</script>