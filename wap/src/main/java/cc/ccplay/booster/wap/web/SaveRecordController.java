package cc.ccplay.booster.wap.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/save/record")
public class SaveRecordController extends BaseWapController {

    @RequestMapping("/share/{recordId}")
    public String share(@PathVariable long recordId){
        return "forward:/";
    }
}
