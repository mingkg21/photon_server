package cc.ccplay.booster.wap.core;

import cc.ccplay.booster.base.core.PagingParameter;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.enums.SourceType;
import org.easyj.frame.filter.DataCallback;
import org.easyj.frame.filter.SysThreadData;
import org.easyj.frame.util.StringUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SystemDataCallback implements DataCallback {

    public Object initData(SysThreadData threadData) {
        HttpSession session = threadData.getSession();
        SystemParam customData = new SystemParam();
        customData.setSourceType(SourceType.WAP);

        HttpServletRequest request = threadData.getRequest();

        int pageNo = 1;//默认第一页
        int pageSize = 10; //默认20条
        String pageNoParam = request.getParameter("pageNumber");
        String pageSizeParam = request.getParameter("pageSize");
        if(pageNoParam != null && pageSizeParam != null){
            pageNo = StringUtil.toInt(pageNoParam);
            pageSize = StringUtil.toInt(pageSizeParam);
        }
        PagingParameter pagingParameter = new PagingParameter();
        pagingParameter.setPageNo(pageNo);
        pagingParameter.setPageSize(pageSize);
        customData.setPagingParameter(pagingParameter);
        customData.setReferer(request.getHeader("referer"));
        customData.setUseragent(request.getHeader("user-agent"));
        return customData;
    }

}
