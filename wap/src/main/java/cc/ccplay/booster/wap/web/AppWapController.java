package cc.ccplay.booster.wap.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/app")
public class AppWapController extends BaseWapController {


    @RequestMapping("/aboutus")
    public String aboutus(HttpServletRequest request){
        String versionCode = request.getParameter("versionCode");
        request.setAttribute("versionCode",versionCode);
        return "/app/aboutus";
    }

    @RequestMapping("/help")
    public String help(){
        return "/app/help";
    }


    @RequestMapping("/userAgreement")
    public String userAgreement(){
        return "/app/userAgreement";
    }
}
