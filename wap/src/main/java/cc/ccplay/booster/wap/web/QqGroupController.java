package cc.ccplay.booster.wap.web;

import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.api.content.QqGroupService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/qqgroup")
public class QqGroupController extends BaseWapController {


    @Value("${cdn.server}")
    private String cdnServer;

    @Reference
    private QqGroupService qqGroupService;

    @RequestMapping("/index")
    public String index(HttpServletRequest request){
        List list  = qqGroupService.getAll();
        request.setAttribute("cdn",cdnServer);
        request.setAttribute("list",list);
        return "/qqgroup/index";
    }

}
