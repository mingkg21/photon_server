package cc.ccplay.booster.wap.web;

import cc.ccplay.booster.base.api.content.GameInfoService;
import cc.ccplay.booster.base.dto.content.game.GameInfoDto;
import cc.ccplay.booster.base.model.content.GameVersionPicture;
import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.api.content.GameRelatedService;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.GameInfo;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/game")
public class GameController extends BaseWapController{

    @Reference
    private GameInfoService gameInfoService;

    @Reference
    private GameRelatedService gameRelatedService;

    @RequestMapping("/detail/{gameId}")
    public String detail(HttpServletRequest request, @PathVariable long gameId){
        GameInfoDto gameInfoDto = gameInfoService.getGameInfoDto(super.getSystemParam(),gameId);
        if(gameInfoDto == null){
            throw new GenericException("游戏不存在");
        }
        List<GameInfoDto> relatedList = gameRelatedService.getRelatedList(gameId,5);
        request.setAttribute("dto",gameInfoDto);
        request.setAttribute("relatedList",relatedList);
        boolean flag = true;//竖屏显示
        List<GameVersionPicture> imgs = gameInfoDto.getPictures();
        if(imgs.size() > 0){
            GameVersionPicture gvp = imgs.get(0);
            long height = gvp.getHeight().longValue();
            long width = gvp.getWidth().longValue();
            if(width > height){
                flag = false; //横屏
            }
        }
        request.setAttribute("flag",flag);
        return "/game/detail";
    }

    @RequestMapping("/listByTag/{tagId}")
    public String listByTag(HttpServletRequest request, @NotNull String tagName, @PathVariable long tagId){
        request.setAttribute("tagName",tagName);
        return "/game/list_by_tag";
    }

    @RequestMapping("/getGamesByTag/{tagId}")
    @ResponseBody
    public void getGamesByTag(@PathVariable long tagId){
        GameInfo searchInfo = new GameInfo();
        searchInfo.setStatus(Status.ENABLED.getValue());
        Page page = gameInfoService.getPage(super.getSystemParam(),searchInfo,tagId,null,null);
        super.renderJson(page);
    }
}
