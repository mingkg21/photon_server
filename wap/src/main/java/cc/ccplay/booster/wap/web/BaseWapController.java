package cc.ccplay.booster.wap.web;

import cc.ccplay.booster.base.util.UserUtil;
import cc.ccplay.booster.base.core.SystemParam;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.ActionUtil;

public class BaseWapController extends ActionUtil{

    public SystemParam getSystemParam(){
        SystemParam param = (SystemParam) UserUtil.getCustomData();
        return param;
    }

    public static void renderJson(Page page){
        renderJson(page.toJsonStr());
    }
}
