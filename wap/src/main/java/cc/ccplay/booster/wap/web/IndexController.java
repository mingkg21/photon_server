package cc.ccplay.booster.wap.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class IndexController extends BaseWapController{

    @RequestMapping({"","/"})
    public String index(HttpServletRequest request){
//        String userAgent = request.getHeader("user-agent").toLowerCase();
//        if(userAgent.indexOf("micromessenger")>-1){//微信客户端
//            return "redirect:http://sj.qq.com/myapp/detail.htm?apkName=com.gamebox.shiba";
//        }
        //http://sj.qq.com/myapp/detail.htm?apkName=com.gamebox.shiba
        return "/index";
    }

}
