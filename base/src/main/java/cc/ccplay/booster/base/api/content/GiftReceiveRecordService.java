package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.content.GiftReceiveRecord;
import cc.ccplay.booster.base.dto.content.gift.GiftReceiveRecordPageDto;
import org.easyj.frame.jdbc.Page;

public interface GiftReceiveRecordService {

    public Page<GiftReceiveRecordPageDto> getPage(SystemParam param, Long gameId, String giftName);

    public GiftReceiveRecord save(SystemParam param, GiftReceiveRecord receiveRecord);
}
