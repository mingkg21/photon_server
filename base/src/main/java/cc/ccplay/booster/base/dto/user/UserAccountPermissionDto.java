package cc.ccplay.booster.base.dto.user;

import cc.ccplay.booster.base.model.user.UserAccountPermission;
import cc.ccplay.booster.base.reader.dto.UserInfo;

import java.io.Serializable;

public class UserAccountPermissionDto implements Serializable {

    private UserAccountPermission userAccountPermission;

    private UserInfo userInfo;

    public UserAccountPermission getUserAccountPermission() {
        return userAccountPermission;
    }

    public void setUserAccountPermission(UserAccountPermission userAccountPermission) {
        this.userAccountPermission = userAccountPermission;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }
}
