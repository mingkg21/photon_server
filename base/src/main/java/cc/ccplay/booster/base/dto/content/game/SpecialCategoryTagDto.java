package cc.ccplay.booster.base.dto.content.game;


import cc.ccplay.booster.base.model.adapter.CdnImage;

public class SpecialCategoryTagDto implements java.io.Serializable {

    private long tagId;

    private String tagName;

    private String gameName;

    private CdnImage icon;

    public long getTagId() {
        return tagId;
    }

    public void setTagId(long tagId) {
        this.tagId = tagId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public CdnImage getIcon() {
        return icon;
    }

    public void setIcon(CdnImage icon) {
        this.icon = icon;
    }
}
