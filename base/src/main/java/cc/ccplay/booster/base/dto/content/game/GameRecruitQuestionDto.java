package cc.ccplay.booster.base.dto.content.game;

import cc.ccplay.booster.base.model.content.GameRecruitQuestion;
import cc.ccplay.booster.base.model.content.GameRecruitAnswer;

import java.io.Serializable;
import java.util.List;

public class GameRecruitQuestionDto implements Serializable{

    private GameRecruitQuestion gameRecruitQuestion;

    private List<GameRecruitAnswer> answers;

    public GameRecruitQuestion getGameRecruitQuestion() {
        return gameRecruitQuestion;
    }

    public void setGameRecruitQuestion(GameRecruitQuestion gameRecruitQuestion) {
        this.gameRecruitQuestion = gameRecruitQuestion;
    }

    public List<GameRecruitAnswer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<GameRecruitAnswer> answers) {
        this.answers = answers;
    }

}
