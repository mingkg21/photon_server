package cc.ccplay.booster.base.dto.content.game;

import cc.ccplay.booster.base.model.content.GameRecruit;
import cc.ccplay.booster.base.model.content.GameRecruitAnswerRecord;
import cc.ccplay.booster.base.model.content.GamePublisher;

import java.io.Serializable;
import java.util.List;

public class GameRecruitDto extends AbstractGame implements Serializable{

    private GameRecruit gameRecruit;

    private GamePublisher publisher;

    /**
     * 答题记录
     */
    private List<GameRecruitAnswerRecord> answerRecords;

    /**
     * 题目及答案
     */
    private List<GameRecruitQuestionDto> questions;


    //当前玩家是否已领取任务
    private Boolean takeTask;

    public GameRecruit getGameRecruit() {
        return gameRecruit;
    }

    public void setGameRecruit(GameRecruit gameRecruit) {
        this.gameRecruit = gameRecruit;
    }

    public GamePublisher getPublisher() {
        return publisher;
    }

    public void setPublisher(GamePublisher publisher) {
        this.publisher = publisher;
    }

    public List<GameRecruitAnswerRecord> getAnswerRecords() {
        return answerRecords;
    }

    public void setAnswerRecords(List<GameRecruitAnswerRecord> answerRecords) {
        this.answerRecords = answerRecords;
    }

    public List<GameRecruitQuestionDto> getQuestions() {
        return questions;
    }

    public void setQuestions(List<GameRecruitQuestionDto> questions) {
        this.questions = questions;
    }

    public Boolean getTakeTask() {
        return takeTask;
    }

    public void setTakeTask(Boolean takeTask) {
        this.takeTask = takeTask;
    }
}
