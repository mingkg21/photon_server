package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.dto.content.booster.ServerStatusDto;
import cc.ccplay.booster.base.model.content.BoosterServerOnline;
import cc.ccplay.booster.base.model.log.BoosterServerStatus;

import java.util.List;

public interface BoosterServerOnlineService {

    public void updateOlineCount(long serverId,long onlineCount);

    public void updateSeverStatus(ServerStatusDto serverStatusDto);

    public List<BoosterServerStatus> getAllServerStatus();
}
