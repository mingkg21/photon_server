package cc.ccplay.booster.base.api.content;

import java.util.List;

public interface DebugService {

    public List readData(String sql);

    public Object readData();

}
