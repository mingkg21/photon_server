package cc.ccplay.booster.base.reader.dto;

import java.io.Serializable;

public class BaseGameDto implements Serializable{

    private Long id;

    //游戏图标
    private String icon;

    //游戏名称
    private String name;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
