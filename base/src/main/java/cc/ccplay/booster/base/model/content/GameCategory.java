package cc.ccplay.booster.base.model.content;

import com.alibaba.fastjson.annotation.JSONField;
import cc.ccplay.booster.base.enums.Status;
import org.easyj.frame.jdbc.annotation.*;
import org.easyj.frame.jdbc.annotation.value.LongValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;

import java.io.Serializable;
import java.util.Date;

@Table(name="game_category")
public class GameCategory implements Serializable {

    @Id
    @GeneratedValue(type = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name="name")
    private String name;

    @NowDateValue(insert = true)
    @Column(name="create_time")
    private Date createTime;

    @NowDateValue(update = true,insert = true)
    @Column(name="update_time")
    private Date updateTime;

    @JSONField(serialize = false)
    @LogicDelete("1")
    @Where(clause = "delete_flag=0")
    @LongValue(insert = true,insertValue = 0)
    @Column(name = "delete_flag")
    private Long deleteFlag;

    @Column(name="status")
    private Status status;

    @Column(name="ordering")
    private Long ordering;


    public Long getOrdering() {
        return ordering;
    }

    public void setOrdering(Long ordering) {
        this.ordering = ordering;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Long getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Long deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
}
