package cc.ccplay.booster.base.dto.content;

import cc.ccplay.booster.base.model.content.CustomAreaItem;
import cc.ccplay.booster.base.model.content.GameInfo;
import cc.ccplay.booster.base.model.content.GameVersion;

public class CustomAreaItemDto implements java.io.Serializable {

    private CustomAreaItem item;

    private GameInfo gameInfo;

    private GameVersion versionInfo;

    public CustomAreaItem getItem() {
        return item;
    }

    public void setItem(CustomAreaItem item) {
        this.item = item;
    }

    public GameInfo getGameInfo() {
        return gameInfo;
    }

    public void setGameInfo(GameInfo gameInfo) {
        this.gameInfo = gameInfo;
    }

    public GameVersion getVersionInfo() {
        return versionInfo;
    }

    public void setVersionInfo(GameVersion versionInfo) {
        this.versionInfo = versionInfo;
    }
}
