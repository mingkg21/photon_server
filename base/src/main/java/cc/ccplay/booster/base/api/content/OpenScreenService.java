package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.OpenScreenDto;
import cc.ccplay.booster.base.model.content.OpenScreen;
import cc.ccplay.booster.base.enums.Status;
import org.easyj.frame.jdbc.Page;

public interface OpenScreenService {

    public Page queryPage(SystemParam systemParam);

    public void delete(long id);

    public void updateStatus(long id,Status status);

    public void saveOrUpdate(OpenScreen openScreen);

    public OpenScreenDto getDto(long id);

    /**
     * 获取最新
     * @return
     */
    public OpenScreen getLastest();

}
