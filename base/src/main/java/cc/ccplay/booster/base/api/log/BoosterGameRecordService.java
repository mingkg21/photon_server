package cc.ccplay.booster.base.api.log;

import cc.ccplay.booster.base.model.log.BoosterGameRecord;

public interface BoosterGameRecordService {

    public void save(BoosterGameRecord record);

}
