package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.gift.GiftInfoDto;
import cc.ccplay.booster.base.model.content.GiftInfo;
import cc.ccplay.booster.base.model.content.GiftNumber;
import org.easyj.frame.jdbc.Page;

public interface GiftInfoService {

    public Page getPage(SystemParam param, Long gameId, String giftName);

    public GiftInfo saveOrUpdate(SystemParam param, GiftInfo giftInfo);

    public GiftInfoDto getGiftInfoDto(SystemParam param, long id);

    public Page<GiftInfoDto> getGameGiftPage(SystemParam param, long gameId,Long userId);

    public GiftNumber receive(SystemParam param, long userId, long giftId);

    public GiftNumber tao(SystemParam param,long userId,long giftId);
}
