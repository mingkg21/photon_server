package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.*;

import java.io.Serializable;

@Table(name="bucket_image")
public class BucketImage extends BaseFile implements Serializable{

    @Column(name="width")
    private Long width;

    @Column(name="height")
    private Long height;

    @Column(name="type")
    private String type;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getWidth() {
        return width;
    }

    public void setWidth(Long width) {
        this.width = width;
    }

    public Long getHeight() {
        return height;
    }

    public void setHeight(Long height) {
        this.height = height;
    }
}
