package cc.ccplay.booster.base.model.adapter;

import cc.ccplay.booster.base.util.SpringUtil;
import org.easyj.frame.exception.GenericException;

public class CdnRoute {

    private static CdnRoute cdnRoute = null;

    public static CdnRoute getInstance(){
        if(cdnRoute == null){
            cdnRoute = SpringUtil.getBean(CdnRoute.class);
        }
        return cdnRoute;
    }

    private String [] urls;

    public String getCndUrl(){
        if(urls == null || urls.length == 0){
            throw new GenericException("缺少CDN服务器配置");
        }
        if(urls.length == 1){
            return urls[0];
        }
        int index = (int)(Math.random() * urls.length);
        return urls[index];
    }

    public String[] getUrls() {
        return urls;
    }

    public void setUrls(String[] urls) {
        this.urls = urls;
    }

}
