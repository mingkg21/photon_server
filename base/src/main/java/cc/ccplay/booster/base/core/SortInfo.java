package cc.ccplay.booster.base.core;

public class SortInfo implements java.io.Serializable {
	
	public SortInfo(String column,String sortType){
		this.column = column;
		this.sortType = sortType;
	}
	
	private String column;
	
	private String sortType;

	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public String getSortType() {
		return sortType;
	}

	public void setSortType(String sortType) {
		this.sortType = sortType;
	}
}
