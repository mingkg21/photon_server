package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.*;

import java.io.Serializable;

@Table(name="bucket_package")
public class BucketPackage extends BaseFile implements Serializable{

    @Column(name = "package_name")
    private String packageName;

    @Column(name = "version_code")
    private Long versionCode;

    @Column(name = "version_name")
    private String versionName;

    @Column(name="update_log")
    private String updateLog;

    @Column(name="adapter_info")
    private String adapterInfo;

    /**
     * apk包签名
     */
    @Column(name="sign")
    private String sign;

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getUpdateLog() {
        return updateLog;
    }

    public void setUpdateLog(String updateLog) {
        this.updateLog = updateLog;
    }

    public String getAdapterInfo() {
        return adapterInfo;
    }

    public void setAdapterInfo(String adapterInfo) {
        this.adapterInfo = adapterInfo;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public Long getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(Long versionCode) {
        this.versionCode = versionCode;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }
}
