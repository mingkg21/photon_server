package cc.ccplay.booster.base.enums;

import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.adapter.AbstractAdapter;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;

public enum Status {

    DISABLED(1,"禁用"),
    ENABLED(2,"启用");

    private long value;

    private String name;

    private Status(long val,String name){
        this.name = name;
        this.value = val;
    }

    public static Status getNotNull(long val){
        Status s = get(val);
        if(s == null){
            throw new GenericException("无效状态值["+val+"]");
        }
        return s;
    }

    public static Status get(Long val){
        if(val == null){
            return null;
        }
        Status [] ss = Status.values();
        for(int i = 0;i < ss.length;i++){
            Status s = ss[i];
            if(s.getValue() == val.longValue()){
                return s;
            }
        }
        return null;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static class StatusAdapter extends AbstractAdapter {
        @Override
        protected Object getColumnValue(Field field, ResultSet rs, String colName) throws SQLException, IllegalAccessException {
            long val = rs.getLong(colName);
            return Status.get(val);
        }

        public Object getObjectValue(Field field, Method getMethod, Object obj) throws IllegalAccessException,InvocationTargetException {
            Status status = (Status) super.getObjectValue(field,getMethod,obj);
            if(status == null){
                return null;
            }
            return status.getValue();
        }

        @Override
        public Object formatValue(Object val){
            Status status = (Status)val;
            if(status == null){
                return null;
            }
            return status.getValue();
        }
    }
}
