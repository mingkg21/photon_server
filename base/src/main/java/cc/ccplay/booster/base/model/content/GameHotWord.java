package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.*;
import org.easyj.frame.jdbc.annotation.value.IntValue;

import java.io.Serializable;

@Table(name="game_hot_word")
public class GameHotWord implements Serializable{

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;


	@Column(name = "name")
	private String name;

	@Column(name = "ordering")
	private Long ordering;

	/**
	 默认搜索词 1：是 0:否
	 */
	@IntValue(insertValue = 0,insert = true)
	@Column(name = "default_word")
	private Long defaultWord;


	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public String getName(){
		return this.name;
	}

	public void setName(String name){
		this.name = name;
	}

	public Long getOrdering(){
		return this.ordering;
	}

	public void setOrdering(Long ordering){
		this.ordering = ordering;
	}

	public Long getDefaultWord(){
		return this.defaultWord;
	}

	public void setDefaultWord(Long defaultWord){
		this.defaultWord = defaultWord;
	}


}

