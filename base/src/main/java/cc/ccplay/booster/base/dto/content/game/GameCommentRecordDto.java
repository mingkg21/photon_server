package cc.ccplay.booster.base.dto.content.game;

import cc.ccplay.booster.base.model.content.GameComment;

import java.io.Serializable;
import java.util.List;

public class GameCommentRecordDto implements Serializable{

    //前三条
    private List<GameComment> comments;

    //评星
    private Long star;


    public List<GameComment> getComments() {
        return comments;
    }

    public void setComments(List<GameComment> comments) {
        this.comments = comments;
    }

    public Long getStar() {
        return star;
    }

    public void setStar(Long star) {
        this.star = star;
    }
}
