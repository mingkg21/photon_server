package cc.ccplay.booster.base.model.adapter;

import org.easyj.frame.jdbc.adapter.AbstractAdapter;
import org.easyj.frame.util.StringUtil;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CdnSourceAdapter extends AbstractAdapter {

    @Override
    protected Object getColumnValue(Field field, ResultSet rs, String colName) throws SQLException, IllegalAccessException {
        String src = rs.getString(colName);
        if(StringUtil.isEmpty(src)){
            return null;
        }
        CdnSource source = new CdnSource();
        source.setSrc(src);
        String cnd = CdnRoute.getInstance().getCndUrl();
        source.setCndSrc(cnd + src);
        return source;
    }

    public Object getObjectValue(Field field, Method getMethod, Object obj) throws IllegalAccessException,InvocationTargetException {
        CdnSource source = (CdnSource) super.getObjectValue(field,getMethod,obj);
        if(source == null){
            return null;
        }
        return source.getSrc();
    }

    @Override
    public Object formatValue(Object val){
        CdnSource source = (CdnSource)val;
        if(source == null){
            return null;
        }
        return source.getSrc();
    }
}
