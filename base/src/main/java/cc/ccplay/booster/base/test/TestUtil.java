package cc.ccplay.booster.base.test;

import cc.ccplay.booster.base.test.annotation.ApiClass;
import cc.ccplay.booster.base.test.annotation.ApiMethod;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import org.apache.log4j.Logger;
import org.easyj.frame.validation.ValidationRegister;
import org.easyj.frame.validation.annotation.*;
import org.easyj.frame.validation.processor.DefaultModelProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

public class TestUtil {

    private final static Logger logger = Logger.getLogger(TestUtil.class);

    private static Map<String,TestClass> classes = null;

    public static synchronized Map<String,TestClass> getApiClass(ApplicationContext context){
        if(classes != null){
            return classes;
        }
        classes = new LinkedHashMap<>();
        Map<String, Object> classMap = context.getBeansWithAnnotation(ApiClass.class);
        for (String key : classMap.keySet()){
            Class clazz = classMap.get(key).getClass();
            ApiClass apiClazz = (ApiClass) clazz.getAnnotation(ApiClass.class);
            if(apiClazz == null){
                clazz = clazz.getSuperclass();
                apiClazz = (ApiClass) clazz.getAnnotation(ApiClass.class);
            }
            if(apiClazz == null || apiClazz.open() == false){
                continue;
            }
            TestClass testClass = new TestClass();
            testClass.setClazz(clazz);
            testClass.setName(apiClazz.value());
            RequestMapping requestMapping = (RequestMapping)clazz.getAnnotation(RequestMapping.class);
            if(requestMapping != null) {
                String[] urls = requestMapping.value();
                if (urls.length > 0) {
                    //取第一个地址
                    testClass.setRequestMapping(urls[0]);
                }
            }
            getApiMethod(testClass);
            classes.put(clazz.getName(),testClass);
        }
        return classes;
    }

    public static Map<String,TestMethod> getApiMethod(String className){
        TestClass clazz  = classes.get(className);
        return getApiMethod(clazz);
    }

    public static List<TestMethodParameter> getApiMethodParameter(String className,String methodName){
        Map<String,TestMethod> methodMap = getApiMethod(className);
        TestMethod method = methodMap.get(methodName);
        return getApiMethodParameter(method);
    }

    public static Map<String,TestMethod> getApiMethod(TestClass testClass){
        Map<String,TestMethod> methods = testClass.getMethods();
        if(methods != null){
            return methods;
        }
        Class apiClazz = testClass.getClazz();
        methods = new LinkedHashMap<>();
        Method[] ms = apiClazz.getMethods();
        for (int i = 0; i < ms.length; i++) {
            Method method = ms[i];
            ApiMethod apiMethod = method.getAnnotation(ApiMethod.class);
            if(apiMethod == null || apiMethod.open() == false){
                continue;
            }
            NotNeedLogin notNeedLogin = method.getAnnotation(NotNeedLogin.class);
            RequestMapping requestMapping = (RequestMapping)method.getAnnotation(RequestMapping.class);
            if(requestMapping == null) {
                continue;
            }
            TestMethod testMethod = new TestMethod();
            testMethod.setMethod(method);
            testMethod.setParentClass(testClass);
            testMethod.setName(apiMethod.value());
            testMethod.setNeedLogin(notNeedLogin == null);
            testMethod.setPaging(apiMethod.paging());

            String[] urls = requestMapping.value();
            if (urls.length > 0) {
                //取第一个地址
                testMethod.setRequestMapping(urls[0]);
            }
            testMethod.setUrl(getApiUrl(testClass.getRequestMapping(),testMethod.getRequestMapping()));
            methods.put(getMethodName(method),testMethod);
            //methods.add();
            getApiMethodParameter(testMethod);
        }
        testClass.setMethods(methods);
        return methods;
    }


    private static String getApiUrl(String parentUrl,String methodUrl ){
        if(parentUrl == null){
            parentUrl = "";
        }
        if(methodUrl == null){
            methodUrl = "";
        }
        return parentUrl.trim() + methodUrl.trim();
    }

    private static String getMethodName(Method method){
       StringBuffer sb = new StringBuffer();
       sb.append(method.getName()+"(");
       Class [] types = method.getParameterTypes();
       int size = types.length;
       for (int i = 0; i < size; i++) {
           if(i != 0){
               sb.append(",");
           }
           sb.append(types[i].getName());
       }
       sb.append(")");
       return sb.toString();
    }

    public static List<TestMethodParameter> getApiMethodParameter(TestMethod testMethod){
        List<TestMethodParameter> parameters = testMethod.getParameters();
        if(parameters != null){
            return parameters;
        }

        parameters = new ArrayList<>();
        Method method = testMethod.getMethod();
        List<Parameter> ps =  TestMethod.getParameter(method) ;
        int psl = ps.size();
        for (int i = 0; i < psl; i++) {
            Parameter parameter = ps.get(i);
            parameters.addAll(getModelParameters(parameter,testMethod));
        }
        testMethod.setParameters(parameters);
        return parameters;
    }



    private static List<TestMethodParameter> getModelParameters(Parameter parameter,TestMethod method){
        Class type  = parameter.getType();
        Param param = parameter.getAnnotation(Param.class);
        MinValue minValue = parameter.getAnnotation(MinValue.class);
        MaxValue maxValue = parameter.getAnnotation(MaxValue.class);
        MaxLength maxLength = parameter.getAnnotation(MaxLength.class);
        MinLength minLength = parameter.getAnnotation(MinLength.class);
        NotNull notNull = parameter.getAnnotation(NotNull.class);

        if(param == null && minValue == null && maxValue == null && maxLength == null && minLength == null){
            return Collections.emptyList();
        }
        if(ValidationRegister.getProcess(type).getClass() != DefaultModelProcessor.class){

            TestMethodParameter p = new TestMethodParameter();
            if(notNull != null){
                p.setNotNull(true);
            }
            if(minValue != null){
                p.setMinValue(minValue.value());
            }
            if(maxValue != null){
                p.setMaxValue(maxValue.value());
            }
            if(minLength != null){
                p.setMinLength(minLength.value());
            }
            if(maxLength != null){
                p.setMaxLength(maxLength.value());
            }
            if(param != null){
                String name = "".equals(param.value()) ? parameter.getName() : param.value();
                p.setParamterName(name);
                p.setRemark(param.remark());
            }else{
                p.setParamterName(parameter.getName());
            }
            p.setParentClass(method.getParentClass());
            p.setParentMethod(method);
            p.setType(type);
            List<TestMethodParameter> list = new ArrayList<TestMethodParameter>();
            list.add(p);
            return list;
        }
        List<Field> fields = new ArrayList<>();
        Set<String> nameSet = new HashSet<>();
        getAllField(type, fields, nameSet);
        int fieldSize = fields.size();
        if(fieldSize == 0){
            return Collections.emptyList();
        }
        List<TestMethodParameter> list = new ArrayList<TestMethodParameter>();
        for (int i = 0; i < fieldSize; i++) {
            TestMethodParameter p = getFieldParameter(fields.get(i));
            if(p != null){
                p.setParentMethod(method);
                p.setParentClass(method.getParentClass());
                list.add(p);
            }
        }
        return list;
    }

    private static TestMethodParameter getFieldParameter(Field field){
        Param param = field.getAnnotation(Param.class);
        MinValue minValue = field.getAnnotation(MinValue.class);
        MaxValue maxValue = field.getAnnotation(MaxValue.class);
        MaxLength maxLength = field.getAnnotation(MaxLength.class);
        MinLength minLength = field.getAnnotation(MinLength.class);
        NotNull notNull = field.getAnnotation(NotNull.class);

        if(notNull == null && param == null && minValue == null &&
                maxValue == null && maxLength == null && minLength == null){
            return null;
        }

        TestMethodParameter p = new TestMethodParameter();

        if(notNull != null){
            p.setNotNull(true);
        }

        if(minValue != null){
            p.setMinValue(minValue.value());
        }
        if(maxValue != null){
            p.setMaxValue(maxValue.value());
        }
        if(minLength != null){
            p.setMinLength(minLength.value());
        }
        if(maxLength != null){
            p.setMaxLength(maxLength.value());
        }
        if(param != null){
            String name = "".equals(param.value()) ? field.getName() : param.value();
            p.setParamterName(name);
            p.setRemark(param.remark());
        }else{
            p.setParamterName(field.getName());
        }
        p.setType(field.getType());
        return p;
    }

    private static boolean isNeedCheck(Field field){
        Class [] clazz = Const.ALL_ANNOTATION_CLASS;
        for (int i = 0; i < clazz.length; i++) {
            if(field.getAnnotation(clazz[i]) != null){
                return true;
            }
        }
        return false;
    }

    private static void getAllField(Class clazz, List<Field> list, Set<String> nameSet){
        if(clazz == Object.class || clazz == null) {
            return;
        }
        Field [] fs = clazz.getDeclaredFields();
        for (int i = 0; i < fs.length; i++) {
            Field field = fs[i];
            String fieldName = field.getName();
            if(nameSet.contains(fieldName)){
                continue;
            }
            if(isNeedCheck(field)){
                list.add(field);
                nameSet.add(fieldName);
            }
        }
        clazz = clazz.getSuperclass();
        getAllField(clazz, list, nameSet);
    }

    public static void getParameterNames(Class clazz){
        LocalVariableTableParameterNameDiscoverer discoverer = new LocalVariableTableParameterNameDiscoverer();
        for (Method method : clazz.getDeclaredMethods()) {
            for (String s : discoverer.getParameterNames(method)) {
                System.out.print("parm: "+s+"  ");
            }
            System.out.println("methodName:  "+method.getName());
        }
    }
}
