package cc.ccplay.booster.base.dto.content.booster;

import cc.ccplay.booster.base.api.content.BoosterApiService;
import com.alibaba.fastjson.annotation.JSONField;

import java.util.HashMap;
import java.util.Map;

public class BoosterUserInfoDto implements java.io.Serializable {

//    public static void main(String []args){
//        BoosterUserInfoDto dto = new BoosterUserInfoDto();
//        dto.setS(1L);
//        dto.setTransferEnable(111L);
//        System.out.println(JsonUtil.toJson(dto));
//    }

    public HashMap<String,String> toRedisData(){
        HashMap<String,String> data = new HashMap<>();
        data.put("port",this.getPort()+"");
        data.put("passwd",this.getPasswd());
        data.put("id",this.getId()+"");
        data.put("t",this.getT()+"");
        data.put("u",this.getU()+"");
        data.put("d",this.getD()+"");
        data.put("transfer_enable",this.getTransferEnable()+"");
        data.put("email",this.getEmail());
        data.put("switch",this.getS()+"");
        data.put("enable",this.getEnable()+"");
        data.put("method",this.getMethod()+"");
        data.put("node",this.getNode()+"");
        data.put("ip",this.getIp()+"");
        data.put(BoosterApiService.KEY_LAST_ACK_TIME,this.getLastAckTime()+"");
        return data;
    }


    public static BoosterUserInfoDto toModel(Map<String,String> data){
        BoosterUserInfoDto userInfoDto = new BoosterUserInfoDto();
        userInfoDto.setPort(Long.parseLong(data.get("port")));
        userInfoDto.setPasswd(data.get("passwd"));
        userInfoDto.setId(Long.parseLong(data.get("id")));
        userInfoDto.setT(Long.parseLong(data.get("t")));
        userInfoDto.setU(Long.parseLong(data.get("u")));
        userInfoDto.setD(Long.parseLong(data.get("d")));
        userInfoDto.setTransferEnable(Long.parseLong(data.get("transfer_enable")));
        userInfoDto.setEmail(data.get("email"));
        userInfoDto.setS(Long.parseLong(data.get("switch")));
        userInfoDto.setEnable(Long.parseLong(data.get("enable")));
        userInfoDto.setMethod(data.get("method"));
        userInfoDto.setNode(Long.parseLong(data.get("node")));
        userInfoDto.setIp(data.get("ip"));
        userInfoDto.setLastAckTime(Long.parseLong(data.get(BoosterApiService.KEY_LAST_ACK_TIME)));
        return userInfoDto;
    }


    //id
    private Long id;

    //密码
    private String passwd;

    //最后登录时间
    private Long t;

    //上传
    private Long u;

    //下载
    private Long d;

    //switch
    @JSONField(name="switch")
    private Long s;

    //启用状态
    private Long enable;

    //加密方式
    private String method;

    //总可用流量
    @JSONField(name = "transfer_enable")
    private Long transferEnable;

    //端口
    private Long port;

    //服务器节点ID   serverId
    @JSONField(serialize = false)
    private Long node;

    //最后应答时间
    @JSONField(serialize = false)
    private Long lastAckTime;

    @JSONField(serialize = false)
    private String ip;

    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Long getLastAckTime() {
        return lastAckTime;
    }

    public void setLastAckTime(Long lastAckTime) {
        this.lastAckTime = lastAckTime;
    }

    public Long getTransferEnable() {
        return transferEnable;
    }

    public void setTransferEnable(Long transferEnable) {
        this.transferEnable = transferEnable;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public Long getT() {
        return t;
    }

    public void setT(Long t) {
        this.t = t;
    }

    public Long getU() {
        return u;
    }

    public void setU(Long u) {
        this.u = u;
    }

    public Long getD() {
        return d;
    }

    public void setD(Long d) {
        this.d = d;
    }

    public Long getS() {
        return s;
    }

    public void setS(Long s) {
        this.s = s;
    }

    public Long getEnable() {
        return enable;
    }

    public void setEnable(Long enable) {
        this.enable = enable;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Long getPort() {
        return port;
    }

    public void setPort(Long port) {
        this.port = port;
    }

    public Long getNode() {
        return node;
    }

    public void setNode(Long node) {
        this.node = node;
    }
}
