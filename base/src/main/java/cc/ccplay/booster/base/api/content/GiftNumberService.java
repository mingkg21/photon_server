package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.model.content.GiftNumber;
import cc.ccplay.booster.base.core.SystemParam;
import org.easyj.frame.jdbc.Page;

import java.util.List;

public interface GiftNumberService {

    public void batchSave(SystemParam param,long giftId, List<GiftNumber> list);

    public Page getPage(SystemParam param,long giftId);

}
