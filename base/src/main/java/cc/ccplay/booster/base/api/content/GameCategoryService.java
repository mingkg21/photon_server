package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.SelectizeDto;
import cc.ccplay.booster.base.model.content.GameCategory;
import cc.ccplay.booster.base.enums.Status;
import org.easyj.frame.jdbc.Page;

import java.util.List;

public interface GameCategoryService {

    public Page getPage(SystemParam param, String name);

    public GameCategory saveOrUpdate(SystemParam param, GameCategory category);

    public int updateStatus(SystemParam param,long id,Status status);

    public GameCategory get(SystemParam param, long id);

    public List<SelectizeDto> getForSelectize(SystemParam param, String name);

    public List<GameCategory> getTopCategory(int size);

}
