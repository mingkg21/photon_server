package cc.ccplay.booster.base.dto.content.game;

import cc.ccplay.booster.base.model.content.GameInfo;
import cc.ccplay.booster.base.model.content.GamePackageMapping;
import cc.ccplay.booster.base.model.content.GameVersion;

public class GamePackageMappingDto implements java.io.Serializable {

    private GamePackageMapping mapping;

    private GameInfo gameInfo;

    private GameVersion versionInfo;


    public GamePackageMapping getMapping() {
        return mapping;
    }

    public void setMapping(GamePackageMapping mapping) {
        this.mapping = mapping;
    }

    public GameInfo getGameInfo() {
        return gameInfo;
    }

    public void setGameInfo(GameInfo gameInfo) {
        this.gameInfo = gameInfo;
    }

    public GameVersion getVersionInfo() {
        return versionInfo;
    }

    public void setVersionInfo(GameVersion versionInfo) {
        this.versionInfo = versionInfo;
    }
}
