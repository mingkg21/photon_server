package cc.ccplay.booster.base.util;

import org.apache.commons.io.IOUtils;

import java.io.ByteArrayOutputStream;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;


import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;

public class RSAUtils {

    public static final String CHARSET = "UTF-8";
    public static final String RSA_ALGORITHM = "RSA";

    /**
     * 得到公钥
     *
     * @param publicKey
     *            密钥字符串（经过base64编码）
     * @throws Exception
     */
    public static RSAPublicKey getPublicKey(String publicKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
        // 通过X509编码的Key指令获得公钥对象
        KeyFactory keyFactory = KeyFactory.getInstance(RSA_ALGORITHM);
        X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(Base64Util.decode(publicKey));
        RSAPublicKey key = (RSAPublicKey) keyFactory.generatePublic(x509KeySpec);
        return key;
    }

    /**
     * 得到私钥
     *
     * @param privateKey
     *            密钥字符串（经过base64编码）
     * @throws Exception
     */
    public static RSAPrivateKey getPrivateKey(String privateKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
        // 通过PKCS#8编码的Key指令获得私钥对象
        KeyFactory keyFactory = KeyFactory.getInstance(RSA_ALGORITHM);
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(Base64Util.decode(privateKey));
        RSAPrivateKey key = (RSAPrivateKey) keyFactory.generatePrivate(pkcs8KeySpec);
        return key;
    }

    /**
     * 公钥加密
     *
     * @param data
     * @param publicKey
     * @return
     */
    public static String publicEncrypt(String data, RSAPublicKey publicKey) {
        try {
            Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            return Base64Util.encode(rsaSplitCodec(cipher, Cipher.ENCRYPT_MODE, data.getBytes(CHARSET), publicKey.getModulus().bitLength()));
        } catch (Exception e) {
            throw new RuntimeException("加密字符串[" + data + "]时遇到异常", e);
        }
    }

    /**
     * 私钥解密
     *
     * @param data
     * @param privateKey
     * @return
     */

    public static String privateDecrypt(String data, RSAPrivateKey privateKey) {
        try {
            Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            return new String(rsaSplitCodec(cipher, Cipher.DECRYPT_MODE, Base64Util.decode(data), privateKey.getModulus().bitLength()), CHARSET);
        } catch (Exception e) {
            throw new RuntimeException("解密字符串[" + data + "]时遇到异常", e);
        }
    }

    /**
     * 私钥加密
     *
     * @param data
     * @param privateKey
     * @return
     */

//    public static String privateEncrypt(String data, RSAPrivateKey privateKey) {
//        try {
//            Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);
//            cipher.init(Cipher.ENCRYPT_MODE, privateKey);
//            return Base64.getEncoder().encodeToString(rsaSplitCodec(cipher, Cipher.ENCRYPT_MODE, data.getBytes(CHARSET), privateKey.getModulus().bitLength()));
//        } catch (Exception e) {
//            throw new RuntimeException("加密字符串[" + data + "]时遇到异常", e);
//        }
//    }

    /**
     * 公钥解密
     *
     * @param data
     * @param publicKey
     * @return
     */

    public static String publicDecrypt(String data, RSAPublicKey publicKey) {
        try {
            Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, publicKey);
            return new String(rsaSplitCodec(cipher, Cipher.DECRYPT_MODE, Base64Util.decode(data), publicKey.getModulus().bitLength()), CHARSET);
        } catch (Exception e) {
            throw new RuntimeException("解密字符串[" + data + "]时遇到异常", e);
        }
    }

    private static byte[] rsaSplitCodec(Cipher cipher, int opmode, byte[] datas, int keySize) throws IllegalBlockSizeException, BadPaddingException {
        int maxBlock = 0;
        if(opmode == Cipher.DECRYPT_MODE){
            maxBlock = keySize / 8;
        }else{
            maxBlock = keySize / 8 - 11;
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int offSet = 0;
        byte[] buff;
        int i = 0;
        try{
            while(datas.length > offSet){
                if(datas.length-offSet > maxBlock){
                    buff = cipher.doFinal(datas, offSet, maxBlock);
                }else{
                    buff = cipher.doFinal(datas, offSet, datas.length-offSet);
                }
                out.write(buff, 0, buff.length);
                i++;
                offSet = i * maxBlock;
            }
        }catch(Exception e){
            throw new RuntimeException("加解密阀值为["+maxBlock+"]的数据时发生异常", e);
        }
        byte[] resultDatas = out.toByteArray();
        IOUtils.closeQuietly(out);
        return resultDatas;
    }

    public static void main(String[] args) throws Exception {
        // Map<String, String> keyMap = RSAUtils.createKeys(1024);
        String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDXCd2igyow+g/03GBA8Cpwm1hByRMdbja4b6aZisjGw67nKTzs9EbtNBkkuseX0lxmchcg0wAeDmcQ1bFX2GEjCizucuAQ4NOXtjcUyD8COiccUgOu7fyoB4HGiwqNH9MQP9lGWJKTm9sfjRSN6/ry95buBJI1GOSohMUgSwdqAQIDAQAB";
        String privateKey = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBANcJ3aKDKjD6D/TcYEDwKnCbWEHJEx1uNrhvppmKyMbDrucpPOz0Ru00GSS6x5fSXGZyFyDTAB4OZxDVsVfYYSMKLO5y4BDg05e2NxTIPwI6JxxSA67t/KgHgcaLCo0f0xA/2UZYkpOb2x+NFI3r+vL3lu4EkjUY5KiExSBLB2oBAgMBAAECgYBl7S840KM3A9B8Z9jX7v6u0XNL+1ssIAmf8owtSzNPw4Er4MgYKNFVrdQdLMtekz9o77s2u8zackk4GZIt6UpgywwYZ4lp/KprT8bxfTZtWf0+MYh5G/0WuNUn7SXMuRAoc4yYHZKYQuhs5NWAQZFaFnUxsrc2h3ar0KTCn0fAgQJBAOvdQR/RnfKBd4xVLViPqvfexuK7ZYbqXflytj3hL5TF90t03CI5IPHKFx3VqWDgFG16rlR9M+GZ7ic86o62nS0CQQDpZXiNQZjKJ92frYvktWKYXRmED5EHsBl7mZKwVi5p6jV4mmFLStRkC38g1knRLHEqdEMNCJmWYjzHA0V1jgylAkAI7OZ5/cSsWJndhZuhrk4Z8yQzkEPXRycnOWAMF2llh9hD0rhB00eb3rnhNyShtdkQC4RtTg+YieqpMXTu0ZpBAkEAlcfix3QTY5iV58VuA4ZMEc+dclyzDCX9FI8HzlZgTuRZEF6ylakeCF5AZYhfsvc8YKxf41tjhVjh/C2jQ7+3aQJBAI9HXAEXbFevBbORpKARKskxzEJUvW7XfRB9Cegl+PxMBQIudaog8Kru3eosL92hmOBtXCQqMkk0GpKGd4Jy8Oo=";
        System.out.println("公钥: \n\r" + publicKey);
        System.out.println("私钥： \n\r" + privateKey);

        System.out.println("公钥加密——私钥解密");
        String str = "[{\"msg\":\"成功\",\"code\":100000,\"data\":{\"ip\":\"47.75.152.52\",\"method\":\"aes-256-cfb\",\"password\":\"V6uL6f4jY0\",\"port\":25317,\"serverId\":1}}]";
        System.out.println("\r明文：\r\n" + str);
        System.out.println("\r明文大小：\r\n" + str.getBytes().length);
        String encodedData = RSAUtils.publicEncrypt(str, RSAUtils.getPublicKey(publicKey));
        System.out.println("密文：\r\n" + encodedData);
        encodedData = "VIGRoF/xcjjUgbfouyMC1AIBc7R5bCtZc538bPNDDLQXovIelZMjIskSHr8NgDDVNpwY8t3sguNMsRLTCUi8YA6iW9VhDqr5UsHMmI2JNAaSGYt/kL37nd4Qr1K84tvAk0b7EsX+/w9VmDuvgD8N4py+IMkKKlx/uDU+xgStCGc=";
        String decodedData = RSAUtils.privateDecrypt(encodedData, RSAUtils.getPrivateKey(privateKey));
        System.out.println("解密后文字: \r\n" + decodedData);
    }

}
