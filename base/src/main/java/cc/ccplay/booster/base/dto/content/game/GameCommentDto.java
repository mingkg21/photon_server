package cc.ccplay.booster.base.dto.content.game;

import cc.ccplay.booster.base.dto.user.SimpleUserDto;
import cc.ccplay.booster.base.model.content.GameComment;
import cc.ccplay.booster.base.reader.ManyUserInfoReader;
import cc.ccplay.booster.base.reader.UserInfoReader;
import cc.ccplay.booster.base.reader.dto.UserInfo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GameCommentDto  extends AbstractGame implements ManyUserInfoReader,Serializable {

    private GameComment comment;

    private SimpleUserDto commentUser;

    private SimpleUserDto beRepliedUser;

    private Long star;

    public Long getStar() {
        return star;
    }

    public void setStar(Long star) {
        this.star = star;
    }

    public SimpleUserDto getCommentUser() {
        return commentUser;
    }

    public void setCommentUser(SimpleUserDto commentUser) {
        this.commentUser = commentUser;
    }

    public SimpleUserDto getBeRepliedUser() {
        return beRepliedUser;
    }

    public void setBeRepliedUser(SimpleUserDto beRepliedUser) {
        this.beRepliedUser = beRepliedUser;
    }

    public GameComment getComment() {
        return comment;
    }

    public void setComment(GameComment comment) {
        this.comment = comment;
    }


    @Override
    public List<UserInfoReader> getReaderList() {
        if(this.comment == null){
            return null;
        }
        final GameCommentDto dto = this;
        final Long userId = this.comment.getUserId();
        final Long beRepliedUserId = this.comment.getBeRepliedUserId();
        List<UserInfoReader> list = new ArrayList<>();
        if(userId != null){
            list.add(new UserInfoReader() {
                @Override
                public Long getUserId() {
                    return userId;
                }

                @Override
                public void callback(UserInfo userInfo) {
                    dto.setCommentUser(new SimpleUserDto(userInfo));
                }
            });
        }
        if(beRepliedUserId != null){
            list.add(new UserInfoReader() {
                @Override
                public Long getUserId() {
                    return beRepliedUserId;
                }

                @Override
                public void callback(UserInfo userInfo) {
                    dto.setBeRepliedUser(new SimpleUserDto(userInfo));
                }
            });
        }
        return list;
    }
}
