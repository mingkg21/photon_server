package cc.ccplay.booster.base.api.user;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.user.SysMenu;
import org.easyj.frame.jdbc.Page;

import java.util.List;

public interface SysMenuService {

    public Page getPage(SystemParam systemParam, long parentId);

    public int delete(SystemParam systemParam,long id);

    public SysMenu getMenu(SystemParam systemParam, long id);

    public SysMenu saveOrUpdate(SystemParam systemParam,SysMenu menu);

    public int updateStatus(SystemParam systemParam,long id,long status);

    public List<SysMenu> getMenuList(long roleId);

    public List<SysMenu> getAllMenuList();

}
