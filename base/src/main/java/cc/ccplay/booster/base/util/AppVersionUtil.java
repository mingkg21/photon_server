package cc.ccplay.booster.base.util;

import cc.ccplay.booster.base.core.AppHeaderInfo;
import org.easyj.frame.util.StringUtil;

public class AppVersionUtil {
    // V1.1
    public static final long V1_1 = 2018071619;

    // V1.2
    public static final long V1_2 = 2018081212;


    public static boolean greater(AppHeaderInfo info, long versionCode){
        long clientVersionCode = StringUtil.toLong(info.getClientVersionCode());
        return greater(clientVersionCode,versionCode);
    }

    public static boolean greater(long currentVersionCode,long versionCode){
        return currentVersionCode > versionCode;
    }

}
