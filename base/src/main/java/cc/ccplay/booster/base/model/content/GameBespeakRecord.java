package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="game_bespeak_record")
public class GameBespeakRecord implements Serializable{

	@CompositeId
	private Id id;

	@Column(name = "game_id")
	private Long gameId;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@Column(name = "version_id")
	private Long versionId;

	public Id getId(){
		return id;
	}

	public void setId(Id id){
		this.id = id;
	}

	public Long getGameId(){
		return this.gameId;
	}

	public void setGameId(Long gameId){
		this.gameId = gameId;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Long getVersionId(){
		return this.versionId;
	}

	public void setVersionId(Long versionId){
		this.versionId = versionId;
	}

	public static class Id{

		public Id(){}

		public Id(Long bespeakId,Long accountId){
			this.bespeakId = bespeakId;
			this.accountId = accountId;
		}

		@Column(name = "bespeak_id")
		private Long bespeakId;


		@Column(name = "account_id")
		private Long accountId;

		public Long getBespeakId() {
			return bespeakId;
		}

		public void setBespeakId(Long bespeakId) {
			this.bespeakId = bespeakId;
		}

		public Long getAccountId(){
			return this.accountId;
		}

		public void setAccountId(Long accountId){
			this.accountId = accountId;
		}

	}
}

