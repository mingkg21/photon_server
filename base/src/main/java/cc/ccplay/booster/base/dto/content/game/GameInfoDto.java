package cc.ccplay.booster.base.dto.content.game;
import cc.ccplay.booster.base.model.content.*;

import java.util.List;

public class GameInfoDto extends AbstractGame implements java.io.Serializable{

    private List<GameTag> tags;

    private GameCategory category;

    private GamePublisher publisher;

    private List<GameVersionPicture> pictures;


    private CustomAreaItem areaItem;

    /**
     * 游戏存档记录
     */
    private List<SaveRecord> saveRecords;

    /**
     * 0 ：没有存档游戏
     *
     * 1： 存档游戏
     */
    private Long hasSave;

    /**
     * 许愿人数
     */
    private Long hopingCount;

    private GameTabDto tabs;


    /**
     * 映射规则
     */
    private List<GamePackageMapping> mapping;


    /**
     * 加速状态
     * 0：不走加速
     * 1： 走加速
     */
    private Long boosterStatus;


    public Long getBoosterStatus() {
        return boosterStatus;
    }

    public void setBoosterStatus(Long boosterStatus) {
        this.boosterStatus = boosterStatus;
    }

    public List<GamePackageMapping> getMapping() {
        return mapping;
    }

    public void setMapping(List<GamePackageMapping> mapping) {
        this.mapping = mapping;
    }

    public CustomAreaItem getAreaItem() {
        return areaItem;
    }

    public void setAreaItem(CustomAreaItem areaItem) {
        this.areaItem = areaItem;
    }

    public List<SaveRecord> getSaveRecords() {
        return saveRecords;
    }

    public void setSaveRecords(List<SaveRecord> saveRecords) {
        this.saveRecords = saveRecords;
    }

    public GameTabDto getTabs() {
        return tabs;
    }

    public void setTabs(GameTabDto tabs) {
        this.tabs = tabs;
    }

    public List<GameVersionPicture> getPictures() {
        return pictures;
    }

    public void setPictures(List<GameVersionPicture> pictures) {
        this.pictures = pictures;
    }

    public List<GameTag> getTags() {
        return tags;
    }

    public void setTags(List<GameTag> tags) {
        this.tags = tags;
    }

    public GameCategory getCategory() {
        return category;
    }

    public void setCategory(GameCategory category) {
        this.category = category;
    }

    public GamePublisher getPublisher() {
        return publisher;
    }

    public void setPublisher(GamePublisher publisher) {
        this.publisher = publisher;
    }

    public Long getHasSave() {
        return hasSave;
    }

    public void setHasSave(Long hasSave) {
        this.hasSave = hasSave;
    }

    public Long getHopingCount() {
        return hopingCount;
    }

    public void setHopingCount(Long hopingCount) {
        this.hopingCount = hopingCount;
    }

}
