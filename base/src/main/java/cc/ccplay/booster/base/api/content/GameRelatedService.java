package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.dto.content.game.GameRelatedDto;
import cc.ccplay.booster.base.dto.content.game.GameInfoDto;
import cc.ccplay.booster.base.model.content.GameRelated;
import org.easyj.frame.jdbc.Page;

import java.util.List;

public interface GameRelatedService {

    public void saveOrUpdate(GameRelated gameRelated);

    public GameRelatedDto getRelatedDto(long gameId, long relatedGameId);

    public void delete(long gameId,long relatedGameId);

    public Page getPage(long gameId);

    public List<GameInfoDto> getRelatedList(long gameId, int maxSize);

}
