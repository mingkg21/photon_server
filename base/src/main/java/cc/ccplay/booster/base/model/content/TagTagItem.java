package cc.ccplay.booster.base.model.content;
import java.io.Serializable;

import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;

@Table(name="tag_tag_item")
public class TagTagItem implements Serializable{

	@CompositeId
	private Id id;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@Column(name = "ordering")
	private Long ordering;



	public Id getId(){
		return id;
	}

	public void setId(Id id){
		this.id = id;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Long getOrdering(){
		return this.ordering;
	}

	public void setOrdering(Long ordering){
		this.ordering = ordering;
	}


	public static class Id implements Serializable {

		public Id(){}

		public Id(Long mainTagId,Long tagId){
			this.mainTagId = mainTagId;
			this.tagId = tagId;
		}

		@Column(name = "main_tag_id")
		private Long mainTagId;

		@Column(name = "tag_id")
		private Long tagId;


		public Long getMainTagId(){
			return this.mainTagId;
		}

		public void setMainTagId(Long mainTagId){
			this.mainTagId = mainTagId;
		}

		public Long getTagId(){
			return this.tagId;
		}

		public void setTagId(Long tagId){
			this.tagId = tagId;
		}

	}
}

