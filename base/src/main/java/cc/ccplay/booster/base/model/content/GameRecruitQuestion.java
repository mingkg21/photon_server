package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.*;
import java.io.Serializable;

@Table(name="game_recruit_question")
public class GameRecruitQuestion implements Serializable{

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;


	@Column(name = "recruit_id")
	private Long recruitId;

	@Column(name = "question")
	private String question;



	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getRecruitId(){
		return this.recruitId;
	}

	public void setRecruitId(Long recruitId){
		this.recruitId = recruitId;
	}

	public String getQuestion(){
		return this.question;
	}

	public void setQuestion(String question){
		this.question = question;
	}


}

