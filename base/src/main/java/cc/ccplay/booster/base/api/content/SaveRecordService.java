package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.save.SaveRecordDto;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.SaveRecord;
import org.easyj.frame.jdbc.Page;

import java.util.List;

public interface SaveRecordService {

    public Page getPage(SystemParam param,Long open,Long gameId,String name);

    public SaveRecord get(long id);

    public void saveOrUpdate(SaveRecord record);

    public void delete(long id);

    public void updateStatus(long id,Status status);

    public SaveRecordDto getDto(long id);

    /**
     * 客户端保存存档
     * @param record
     */
    public SaveRecord uploadRecord(SaveRecord record);


    public List<SaveRecord> getUserRecord(long userId,long gameId);

    /**
     * 获取推荐存档
     * @param gameId
     * @return
     */
    public Page<SaveRecord> getRecommendRecord(SystemParam param,long gameId);

    /**
     * 点赞
     */
    public void praise(long userId,long recordId);

    /**
     * 用户删除存档 逻辑删除
     * @param recordId
     */
    public void logicDelete(long userId,long recordId);
}
