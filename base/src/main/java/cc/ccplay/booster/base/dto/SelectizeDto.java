package cc.ccplay.booster.base.dto;

import java.io.Serializable;

public class SelectizeDto implements Serializable{

    public SelectizeDto(){}

    public SelectizeDto(Object id,String name){
        this.id = id;
        this.name = name;
    }

    /**
     * 隐藏值
     */
    private Object id;

    /**
     * 显示值
     */
    private String name;


    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
