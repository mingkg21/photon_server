package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.dto.content.game.GameRecruitFeedbackDto;
import cc.ccplay.booster.base.core.SystemParam;
import org.easyj.frame.jdbc.Page;

public interface GameRecruitFeedbackService {

    public GameRecruitFeedbackDto getMyFeedback(SystemParam param, long recruitId);

    public Page<GameRecruitFeedbackDto> getDtoPage(SystemParam param,long recruitId);

    public void feedback(SystemParam param,long recruitId,String title,String content);
}
