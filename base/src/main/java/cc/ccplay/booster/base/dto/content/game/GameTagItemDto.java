package cc.ccplay.booster.base.dto.content.game;

import cc.ccplay.booster.base.model.content.GameVersion;
import cc.ccplay.booster.base.model.content.GameInfo;
import cc.ccplay.booster.base.model.content.GameTagItem;

public class GameTagItemDto implements java.io.Serializable {

    private GameInfo gameInfo;

    private GameVersion versionInfo;

    private GameTagItem gameTagItem;

    public GameInfo getGameInfo() {
        return gameInfo;
    }

    public void setGameInfo(GameInfo gameInfo) {
        this.gameInfo = gameInfo;
    }

    public GameVersion getVersionInfo() {
        return versionInfo;
    }

    public void setVersionInfo(GameVersion versionInfo) {
        this.versionInfo = versionInfo;
    }

    public GameTagItem getGameTagItem() {
        return gameTagItem;
    }

    public void setGameTagItem(GameTagItem gameTagItem) {
        this.gameTagItem = gameTagItem;
    }
}
