package cc.ccplay.booster.base.model.content;

import cc.ccplay.booster.base.model.adapter.CdnImage;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import org.easyj.frame.util.StringUtil;

import java.util.Date;
import java.io.Serializable;

@Table(name="ad_area_item")
public class AdAreaItem implements Serializable{

	/**
	 * 标签
	 */
	public final static String OBJECT_TYPE_TAG = "tag";


	/**
	 * 应用
	 */
	public final static String OBJECT_TYPE_GAME = "app";

	/**
	 * 特殊标签
	 */
	public final static String OBJECT_TYPE_SPECIAL_TAG = "special_tag";

	/**
	 * 游戏类型
	 */
	public final static String OBJECT_TYPE_CATEGORY = "category";




	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;


	@Column(name = "ad_id")
	private Long adId;

	@Column(name = "title")
	private String title;

	@Column(name = "object_type")
	private String objectType;

	@Column(name = "object_id")
	private Long objectId;

	@Column(name = "start_time")
	private Date startTime;

	@Column(name = "end_time")
	private Date endTime;

	@Column(name = "pic_url")
	private CdnImage picUrl;

	@Column(name = "ordering")
	private Long ordering;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@NowDateValue(update = true,insert = true)
	@Column(name = "update_time")
	private Date updateTime;



	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getAdId(){
		return this.adId;
	}

	public void setAdId(Long adId){
		this.adId = adId;
	}

	public String getTitle(){
		return this.title;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getObjectType(){
		return this.objectType;
	}

	public void setObjectType(String objectType){
		this.objectType = objectType;
	}

	public Long getObjectId(){
		return this.objectId;
	}

	public void setObjectId(Long objectId){
		this.objectId = objectId;
	}

	public Date getStartTime(){
		return this.startTime;
	}

	public void setStartTime(Date startTime){
		this.startTime = startTime;
	}

	public Date getEndTime(){
		return this.endTime;
	}

	public void setEndTime(Date endTime){
		this.endTime = endTime;
	}

	public CdnImage getPicUrl(){
		return this.picUrl;
	}

	public void setPicUrl(String picUrl) {
		if(StringUtil.isEmpty(picUrl)){
			this.picUrl = new CdnImage("");
		}else{
			this.picUrl = new CdnImage(picUrl);
		}
	}

	public void setPicUrl(CdnImage picUrl){
		this.picUrl = picUrl;
	}

	public Long getOrdering(){
		return this.ordering;
	}

	public void setOrdering(Long ordering){
		this.ordering = ordering;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Date getUpdateTime(){
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime){
		this.updateTime = updateTime;
	}


}

