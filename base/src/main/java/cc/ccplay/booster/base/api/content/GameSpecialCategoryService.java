package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.game.SpecialCategoryTagDto;
import cc.ccplay.booster.base.model.content.GameSpecialCategory;
import cc.ccplay.booster.base.enums.Status;
import org.easyj.frame.jdbc.Page;

import java.util.List;


public interface GameSpecialCategoryService {

    public Page getPage(SystemParam param, String name);

    public GameSpecialCategory saveOrUpdate(GameSpecialCategory category);

    public int updateStatus(long id,Status status);

    public GameSpecialCategory get(long id);

    public List<GameSpecialCategory> getList(int limitSize);

    public Page<SpecialCategoryTagDto> getTagList(SystemParam param, long categoryId);
}
