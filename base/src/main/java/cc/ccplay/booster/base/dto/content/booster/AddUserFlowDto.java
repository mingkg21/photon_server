package cc.ccplay.booster.base.dto.content.booster;

import cc.ccplay.booster.base.enums.AddUserFlowType;

import java.io.Serializable;
import java.util.Date;

public class AddUserFlowDto implements Serializable {

    private Long download;

    private Long upload;

    private Long total;

    private Date time;

    private String remark;

    private AddUserFlowType type;

    private Long userId;


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public AddUserFlowType getType() {
        return type;
    }

    public void setType(AddUserFlowType type) {
        this.type = type;
    }

    public Long getDownload() {
        return download;
    }

    public void setDownload(Long download) {
        this.download = download;
    }

    public Long getUpload() {
        return upload;
    }

    public void setUpload(Long upload) {
        this.upload = upload;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
