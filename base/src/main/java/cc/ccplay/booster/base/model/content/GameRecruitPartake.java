package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="game_recruit_partake")
public class GameRecruitPartake implements Serializable{

	@CompositeId
	private Id id;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;



	public Id getId(){
		return id;
	}

	public void setId(Id id){
		this.id = id;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}


	public static class Id{

		public Id(){}

		public Id(Long recruitId,Long userId){
			this.recruitId = recruitId;
			this.userId = userId;
		}

		/**
		 招募ID
		 */
		@Column(name = "recruit_id")
		private Long recruitId;

		@Column(name = "user_id")
		private Long userId;


		public Long getRecruitId(){
			return this.recruitId;
		}

		public void setRecruitId(Long recruitId){
			this.recruitId = recruitId;
		}

		public Long getUserId(){
			return this.userId;
		}

		public void setUserId(Long userId){
			this.userId = userId;
		}

	}
}

