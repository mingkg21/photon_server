package cc.ccplay.booster.base.enums;

public enum SaveRecordFeedbackType {

    CANNOT_READ(1,"无法读取"),
    CANNOT_UPLOAD(2,"无法上传"),
    CANNOT_RESET(3,"无法清空"),
    OTHER(SaveRecordFeedbackType.OTHER_VALUE,"其他");

    public static final int OTHER_VALUE = 99;

    private int value;

    private String name;


    private SaveRecordFeedbackType(int value,String name){
        this.value = value;
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public String getName() {
        return name;
    }


    public static SaveRecordFeedbackType getByValue(int value){
        SaveRecordFeedbackType [] cts = SaveRecordFeedbackType.values();
        for (int i = 0; i < cts.length; i++) {
            SaveRecordFeedbackType ct = cts[i];
            if(ct.value == value){
                return ct;
            }
        }
        return null;
    }

}
