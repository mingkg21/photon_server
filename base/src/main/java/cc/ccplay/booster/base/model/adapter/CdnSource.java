package cc.ccplay.booster.base.model.adapter;

public class CdnSource implements java.io.Serializable{

    public CdnSource(){

    }
    public CdnSource(String src){
        this.src = src;
    }

    private String cndSrc;

    private String src;

    public String getCndSrc() {
        return cndSrc;
    }

    public void setCndSrc(String cndSrc) {
        this.cndSrc = cndSrc;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    @Override
    public String toString(){
        return this.src;
    }
}
