package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="game_recruit_answer_record")
public class GameRecruitAnswerRecord implements Serializable{

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;


	@Column(name = "recruit_id")
	private Long recruitId;

	@Column(name = "user_id")
	private Long userId;

	/**
	 答案ID
	 */
	@Column(name = "answer_id")
	private Long answerId;

	/**
	 问题ID
	 */
	@Column(name = "question_id")
	private Long questionId;

	@Column(name = "answer_text")
	private String answerText;

	@Column(name = "question_text")
	private String questionText;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;



	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getRecruitId(){
		return this.recruitId;
	}

	public void setRecruitId(Long recruitId){
		this.recruitId = recruitId;
	}

	public Long getUserId(){
		return this.userId;
	}

	public void setUserId(Long userId){
		this.userId = userId;
	}

	public Long getAnswerId(){
		return this.answerId;
	}

	public void setAnswerId(Long answerId){
		this.answerId = answerId;
	}

	public Long getQuestionId(){
		return this.questionId;
	}

	public void setQuestionId(Long questionId){
		this.questionId = questionId;
	}

	public String getAnswerText(){
		return this.answerText;
	}

	public void setAnswerText(String answerText){
		this.answerText = answerText;
	}

	public String getQuestionText(){
		return this.questionText;
	}

	public void setQuestionText(String questionText){
		this.questionText = questionText;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}


}

