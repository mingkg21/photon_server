package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.*;
import org.easyj.frame.jdbc.annotation.value.LongValue;

import java.io.Serializable;

@Table(name="game_comment_star_range")
public class GameCommentStarRange implements Serializable{

	@Id
	@Column(name = "game_id")
	private Long gameId;

	@LongValue(insert = true,insertValue = 0)
	@Column(name = "star1")
	private Long star1;

	@LongValue(insert = true,insertValue = 0)
	@Column(name = "star2")
	private Long star2;

	@LongValue(insert = true,insertValue = 0)
	@Column(name = "star3")
	private Long star3;

	@LongValue(insert = true,insertValue = 0)
	@Column(name = "star4")
	private Long star4;

	@LongValue(insert = true,insertValue = 0)
	@Column(name = "star5")
	private Long star5;



	public Long getGameId(){
		return this.gameId;
	}

	public void setGameId(Long gameId){
		this.gameId = gameId;
	}

	public Long getStar1(){
		return this.star1;
	}

	public void setStar1(Long star1){
		this.star1 = star1;
	}

	public Long getStar2(){
		return this.star2;
	}

	public void setStar2(Long star2){
		this.star2 = star2;
	}

	public Long getStar3(){
		return this.star3;
	}

	public void setStar3(Long star3){
		this.star3 = star3;
	}

	public Long getStar4(){
		return this.star4;
	}

	public void setStar4(Long star4){
		this.star4 = star4;
	}

	public Long getStar5(){
		return this.star5;
	}

	public void setStar5(Long star5){
		this.star5 = star5;
	}


}

