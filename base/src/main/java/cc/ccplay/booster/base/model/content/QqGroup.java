package cc.ccplay.booster.base.model.content;

import cc.ccplay.booster.base.constants.Constant;
import cc.ccplay.booster.base.model.adapter.CdnImage;
import cc.ccplay.booster.base.model.adapter.Icon;
import org.easyj.frame.jdbc.annotation.value.LongValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import org.easyj.frame.util.StringUtil;

import java.util.Date;
import java.io.Serializable;

@Table(name="qq_group")
public class QqGroup implements Serializable{

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;


	@Icon("50")
	@Column(name="icon")
	private CdnImage icon;

	@Column(name = "title")
	private String title;

	@Column(name = "group_number")
	private String groupNumber;

	@Column(name = "description")
	private String description;

	@Column(name = "ordering")
	private Long ordering;

	@LongValue(insert = true,insertValue = Constant.STATUS_ENABLED)
	@Column(name = "status")
	private Long status;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@Column(name = "key")
	private String key;



	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public CdnImage getIcon() {
		return icon;
	}

	public void setIcon(CdnImage icon) {
		this.icon = icon;
	}

	public void setIcon(String icon){
		if(StringUtil.isNotEmpty(icon)){
			this.icon = new CdnImage(icon);
		}else{
			this.icon = null;
		}
	}


	public String getTitle(){
		return this.title;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getGroupNumber(){
		return this.groupNumber;
	}

	public void setGroupNumber(String groupNumber){
		this.groupNumber = groupNumber;
	}

	public String getDescription(){
		return this.description;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public Long getOrdering(){
		return this.ordering;
	}

	public void setOrdering(Long ordering){
		this.ordering = ordering;
	}

	public Long getStatus(){
		return this.status;
	}

	public void setStatus(Long status){
		this.status = status;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public String getKey(){
		return this.key;
	}

	public void setKey(String key){
		this.key = key;
	}


}

