package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.*;

import java.io.Serializable;

@Table(name="app_channel")
public class AppChannel implements Serializable {

    @Id
    @GeneratedValue(type= GenerationType.AUTO)
    @Column(name="id")
    private Long id;

    /**
     *  渠道名称
     */
    @Column(name="name")
    private String name;

    /**
     *  渠道标识
     */
    @Column(name="code")
    private String code;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
