package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.dto.SelectizeDto;
import cc.ccplay.booster.base.dto.content.game.GameTagItemDto;
import cc.ccplay.booster.base.model.content.GameTag;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.GameTagItem;
import cc.ccplay.booster.base.model.content.TagRecommendGame;
import org.easyj.frame.jdbc.Page;

import java.util.List;

public interface GameTagService {

    public Page getPage(SystemParam param, GameTag searchObj);

    public GameTag saveOrUpdate(SystemParam param, GameTag tag);

    public int updateStatus(SystemParam param,long id,Status status);

    public GameTag get(SystemParam param, long id);

    public List<SelectizeDto> getForSelectize(SystemParam param, GameTag searchParam);

    public Page<GameTag> getList(SystemParam param,Long tagType);

    public Page<TagRecommendGame> getTagRecommendGame(SystemParam param);

    public void bindGame(long tagId,long gameId,long ordering);

    public void unbindGame(long tagId,long gameId);

    public GameTagItemDto getItemDto(GameTagItem.Id itemId);



    /**
     * 通过特色标签获取所有游戏绑定标签
     * 再将这些普通标签绑定到特色标签下
     * @param count
     */
    public int batchBindTag(long specialTagId,int count);
}
