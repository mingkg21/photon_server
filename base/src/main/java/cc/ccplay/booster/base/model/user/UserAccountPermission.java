package cc.ccplay.booster.base.model.user;

import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="user_account_permission")
public class UserAccountPermission implements Serializable{

	@Id
	@Column(name = "user_id")
	private Long userId;


	/**
	 游戏评论
	 */
	@Column(name = "allow_game_comment")
	private Long allowGameComment;

	/**
	 存档评论
	 */
	@Column(name = "allow_save_record_comment")
	private Long allowSaveRecordComment;

	@NowDateValue(update = true,insert = true)
	@Column(name = "update_time")
	private Date updateTime;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;



	public Long getUserId(){
		return this.userId;
	}

	public void setUserId(Long userId){
		this.userId = userId;
	}

	public Long getAllowGameComment(){
		return this.allowGameComment;
	}

	public void setAllowGameComment(Long allowGameComment){
		this.allowGameComment = allowGameComment;
	}

	public Long getAllowSaveRecordComment(){
		return this.allowSaveRecordComment;
	}

	public void setAllowSaveRecordComment(Long allowSaveRecordComment){
		this.allowSaveRecordComment = allowSaveRecordComment;
	}

	public Date getUpdateTime(){
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime){
		this.updateTime = updateTime;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}


}

