package cc.ccplay.booster.base.model.content;

import cc.ccplay.booster.base.constants.Constant;
import org.easyj.frame.jdbc.annotation.value.LongValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="ad_area")
public class AdArea implements Serializable{

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;


	@Column(name = "name")
	private String name;

	@Column(name = "code")
	private String code;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@Column(name = "suport_types")
	private String suportTypes;

	@Column(name = "status")
	@LongValue(insert = true,insertValue = Constant.STATUS_ENABLED)
	private Long status;

	@Column(name = "remark")
	private String remark;


	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public String getName(){
		return this.name;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getCode(){
		return this.code;
	}

	public void setCode(String code){
		this.code = code;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public String getSuportTypes(){
		return this.suportTypes;
	}

	public void setSuportTypes(String suportTypes){
		this.suportTypes = suportTypes;
	}

	public Long getStatus(){
		return this.status;
	}

	public void setStatus(Long status){
		this.status = status;
	}


}

