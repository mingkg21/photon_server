package cc.ccplay.booster.base.model.adapter;

public class CdnImage implements java.io.Serializable{

    public CdnImage(){}
    public CdnImage(String src){
        this.src = src;
    }

    private String icon;

    private String cndSrc;

    private String src;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getCndSrc() {
        return cndSrc;
    }

    public void setCndSrc(String cndSrc) {
        this.cndSrc = cndSrc;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    @Override
    public String toString(){
        return this.src;
    }
}
