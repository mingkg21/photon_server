package cc.ccplay.booster.base.model.adapter;

import com.alibaba.fastjson.JSONArray;

import java.io.Serializable;
import java.util.List;

public class CdnImageList implements Serializable{

    public CdnImageList(){}

    public CdnImageList(JSONArray images){
        this.images = images;
    }

    private String icon;

    private JSONArray images;

    private List<String> cdnImages;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public JSONArray getImages() {
        return images;
    }

    public void setImages(JSONArray images) {
        this.images = images;
    }

    public List<String> getCdnImages() {
        return cdnImages;
    }

    public void setCdnImages(List<String> cdnImages) {
        this.cdnImages = cdnImages;
    }

    @Override
    public String toString(){
        if(images == null){
            return null;
        }
        return images.toJSONString();
    }
}
