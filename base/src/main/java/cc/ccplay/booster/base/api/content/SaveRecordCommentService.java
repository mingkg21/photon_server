package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.save.SaveRecordCommentDto;
import cc.ccplay.booster.base.model.content.SaveRecordComment;
import org.easyj.frame.jdbc.Page;

public interface SaveRecordCommentService {

    public SaveRecordComment comment(SaveRecordComment comment);

    public SaveRecordComment reply(SaveRecordComment reply);

    public Page<SaveRecordCommentDto> getCommentPage(SystemParam param, long recordId);

    public Page<SaveRecordCommentDto> getReplyPage(SystemParam param, long commentId);

    public SaveRecordCommentDto getComment(long commentId);

}
