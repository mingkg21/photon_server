package cc.ccplay.booster.base.model.content;

import com.alibaba.fastjson.annotation.JSONField;
import org.easyj.frame.jdbc.annotation.value.LongValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="gift_info")
public class GiftInfo implements Serializable{

	//新手卡
	public static final long TYPE_NEW_PLAYER_CARD = 0;
	//礼包
	public static final long TYPE_GIFT = 1;
	//激活码
	public static final long TYPE_ACTIVATION_CODE = 2;

	//关闭
	public static final long STATUS_CLOSE = 0;
	//开启
	public static final long STATUS_OPEN = 1;

	public static final long WASH_STATUS_CLOSE = 0;
	public static final long WASH_STATUS_OPEN = 1;


	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;


	/**
	 礼包类型 0:新手卡 1:礼包  2:激活码
	 */
	@Column(name = "type")
	private Long type;

	@Column(name = "game_id")
	private Long gameId;


	/**
	 领取开启时间
	 */
	@Column(name = "start_time")
	private Date startTime;

	/**
	 领取结束时间
	 */
	@Column(name = "end_time")
	private Date endTime;

	/**
	 礼包详情
	 */
	@Column(name = "description")
	private String description;

	/**
	 淘号状态 0:关闭  1:开启
	 */
	@JSONField(serialize = false)
	@Column(name = "wash_status")
	@LongValue(insert = true,insertValue = GiftInfo.WASH_STATUS_OPEN)
	private Long washStatus;

	/**
	 发布状态 0:关闭 1:开启
	 */
	@Column(name = "status")
	private Long status;

	/**
	 使用说明
	 */
	@Column(name = "instructions")
	private String instructions;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@NowDateValue(update = true,insert = true)
	@Column(name = "update_time")
	private Date updateTime;

	@Column(name = "name")
	private String name;

	@LongValue(insert = true)
	@Column(name = "gift_count")
	private Long giftCount;

	@LongValue(insert = true)
	@Column(name = "receive_count")
	private Long receiveCount;


	public Long getGiftCount() {
		return giftCount;
	}

	public void setGiftCount(Long giftCount) {
		this.giftCount = giftCount;
	}

	public Long getReceiveCount() {
		return receiveCount;
	}

	public void setReceiveCount(Long receiveCount) {
		this.receiveCount = receiveCount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getType(){
		return this.type;
	}

	public void setType(Long type){
		this.type = type;
	}

	public Long getGameId(){
		return this.gameId;
	}

	public void setGameId(Long gameId){
		this.gameId = gameId;
	}

	public Date getStartTime(){
		return this.startTime;
	}

	public void setStartTime(Date startTime){
		this.startTime = startTime;
	}

	public Date getEndTime(){
		return this.endTime;
	}

	public void setEndTime(Date endTime){
		this.endTime = endTime;
	}

	public String getDescription(){
		return this.description;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public Long getWashStatus(){
		return this.washStatus;
	}

	public void setWashStatus(Long washStatus){
		this.washStatus = washStatus;
	}

	public Long getStatus(){
		return this.status;
	}

	public void setStatus(Long status){
		this.status = status;
	}

	public String getInstructions(){
		return this.instructions;
	}

	public void setInstructions(String instructions){
		this.instructions = instructions;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Date getUpdateTime(){
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime){
		this.updateTime = updateTime;
	}
}

