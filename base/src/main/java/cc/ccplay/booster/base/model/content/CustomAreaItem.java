package cc.ccplay.booster.base.model.content;

import cc.ccplay.booster.base.model.adapter.CdnImage;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import org.easyj.frame.util.StringUtil;

import java.util.Date;
import java.io.Serializable;

@Table(name="custom_area_item")
public class CustomAreaItem implements Serializable{

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;


	@Column(name = "area_id")
	private Long areaId;

	@Column(name = "game_id")
	private Long gameId;

	@Column(name = "ordering")
	private Long ordering;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@Column(name = "banner")
	private CdnImage banner;


	public CdnImage getBanner() {
		return banner;
	}

	public void setBanner(CdnImage benner) {
		this.banner = benner;
	}

	public void setBanner(String banner){
		if(StringUtil.isNotEmpty(banner)){
			this.banner = new CdnImage(banner);
		}else{
			this.banner = new CdnImage("");
		}
	}

	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getAreaId(){
		return this.areaId;
	}

	public void setAreaId(Long areaId){
		this.areaId = areaId;
	}

	public Long getGameId(){
		return this.gameId;
	}

	public void setGameId(Long gameId){
		this.gameId = gameId;
	}

	public Long getOrdering(){
		return this.ordering;
	}

	public void setOrdering(Long ordering){
		this.ordering = ordering;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}


}

