package cc.ccplay.booster.base.api.user;

public interface UserRedisService {

    void set(String key, String value);

    void delete(String key);

    void set(String key, String value, Long second);

    String get(String key);

    void push(String key, String value);

    String pop(String key);

    boolean hasKey(String key);

}
