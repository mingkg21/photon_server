package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.model.content.BucketFile;
import cc.ccplay.booster.base.core.SystemParam;
import org.easyj.frame.jdbc.Page;

public interface BucketFileService {

    public BucketFile save(SystemParam param, BucketFile bucketFile);

    public Page queryPage(SystemParam param, BucketFile searchObj);
}
