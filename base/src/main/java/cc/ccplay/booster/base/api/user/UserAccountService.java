package cc.ccplay.booster.base.api.user;

import cc.ccplay.booster.base.reader.dto.UserInfo;
import cc.ccplay.booster.base.model.user.UserAccount;
import cc.ccplay.booster.base.core.SystemParam;
import org.easyj.frame.jdbc.Page;

import java.util.List;

public interface UserAccountService {

    public List<UserInfo> getUserInfo(Long... ids);

    public UserInfo getUserInfo(Long id);

    public Page getPage(SystemParam param, UserAccount searchModel);

    public boolean updateStatus(SystemParam param,long userId,String status);

    public UserAccount getAccountByPhone(String phone);

    public UserAccount getAccountByOpenId(String openId,long platform);

    public UserAccount createOrGetByOpenId(String openId,long platform);

    public void modifyPassword(long userId,String oldPwd,String newPwd);

}

