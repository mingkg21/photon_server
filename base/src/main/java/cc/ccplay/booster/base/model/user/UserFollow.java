package cc.ccplay.booster.base.model.user;

import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="user_follow")
public class UserFollow implements Serializable{

	@CompositeId
	private Id id;

	@NowDateValue(insert = true)
	@Column(name = "create_datetime")
	private Date createDatetime;



	public Id getId(){
		return id;
	}

	public void setId(Id id){
		this.id = id;
	}

	public Date getCreateDatetime(){
		return this.createDatetime;
	}

	public void setCreateDatetime(Date createDatetime){
		this.createDatetime = createDatetime;
	}


	public static class Id{

		public Id(){}

		public Id(Long fansUserId,Long beFollowUserId){
			this.fansUserId = fansUserId;
			this.beFollowUserId = beFollowUserId;
		}

		/**
		 关注者(粉丝)
		 */
		@Column(name = "fans_user_id")
		private Long fansUserId;

		/**
		 被关注者
		 */
		@Column(name = "be_follow_user_id")
		private Long beFollowUserId;


		public Long getFansUserId(){
			return this.fansUserId;
		}

		public void setFansUserId(Long fansUserId){
			this.fansUserId = fansUserId;
		}

		public Long getBeFollowUserId(){
			return this.beFollowUserId;
		}

		public void setBeFollowUserId(Long beFollowUserId){
			this.beFollowUserId = beFollowUserId;
		}

	}
}

