package cc.ccplay.booster.base.reader.dto;

import cc.ccplay.booster.base.model.user.UserAccount;
import com.alibaba.fastjson.annotation.JSONField;
import cc.ccplay.booster.base.model.adapter.CdnImage;
import cc.ccplay.booster.base.model.user.UserProfile;

public class UserInfo implements java.io.Serializable {

    private UserAccount userAccount;

    private UserProfile userProfile;

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    @JSONField(serialize = false)
    public String getUsername(){
        if(userAccount == null){
            return null;
        }
        return userAccount.getUsername();
    }

    @JSONField(serialize = false)
    public String getNickName(){
        if(userAccount == null){
            return null;
        }
        return userAccount.getNickName();
    }

    @JSONField(serialize = false)
    public CdnImage getHeadIcon(){
        if(userProfile == null){
            return null;
        }
        return userProfile.getHeadIcon();
    }

}
