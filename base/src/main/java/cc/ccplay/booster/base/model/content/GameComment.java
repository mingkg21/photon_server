package cc.ccplay.booster.base.model.content;

import cc.ccplay.booster.base.model.adapter.CdnImageList;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.annotation.JSONField;
import cc.ccplay.booster.base.model.adapter.Icon;
import org.easyj.frame.jdbc.annotation.value.LongValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="game_comment")
public class GameComment implements Serializable{

	/**
	 * 置顶
	 */
	public static final long TOP_STATUS_STICK = 999L;

	/**
	 * 普通帖子
	 */
	public static final long TOP_STATUS_COMMON = 1L;

	/**
	 * 普通用户回复
	 */
	public static final long COMMENT_TYPE_COMMON = 1L;
	/**
	 * 官方回复
	 */
	public static final long COMMENT_TYPE_OFFICIAL = 2L;

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;


	/**
	 回复内容
	 */
	@Column(name = "content")
	private String content;

	@Column(name = "user_id")
	private Long userId;

	@Column(name = "game_id")
	private Long gameId;

	/**
	 回复数量
	 */
	@LongValue(insert = true,insertValue = 0)
	@Column(name = "reply_count")
	private Long replyCount;

	/**
	 点赞数量
	 */
	@LongValue(insert = true,insertValue = 0)
	@Column(name = "praise_count")
	private Long praiseCount;

	/**
	 手机型号
	 */
	@Column(name = "model_name")
	private String modelName;
	/**
	 * 评论ID
	 */
	@Column(name = "parent_id")
	private Long parentId;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	/**
	 被回复用户ID
	 */
	@Column(name = "be_replied_user_id")
	private Long beRepliedUserId;

	/**
	 官方回复内容
	 */
	@Column(name = "official_content")
	private String officialContent;

	/**
	 官方回复时间
	 */
	@Column(name = "official_time")
	private Date officialTime;

	@Column(name = "os_version")
	private String osVersion;

	/**
	 * 游戏版本
	 */
	@Column(name = "version_name")
	private String versionName;

	@Icon("subjectDetail")
	@Column(name = "imgs")
	private CdnImageList imgs;

	@Icon("subjectDetail")
	@Column(name = "official_imgs")
	private CdnImageList officialImgs;

	@JSONField(serialize = false)
	@LogicDelete("1")
	@Where(clause = "delete_flag=0")
	@LongValue(insert = true,insertValue = 0)
	@Column(name = "delete_flag")
	private Long deleteFlag;

	@Column(name = "comment_type")
	@LongValue(insert = true,insertValue = COMMENT_TYPE_COMMON)
	private Long commentType;

	@LongValue(insert = true,insertValue = TOP_STATUS_COMMON)
	@Column(name = "top_status")
	private Long topStatus;


	public CdnImageList getOfficialImgs() {
		return officialImgs;
	}

	public void setOfficialImgs(CdnImageList officialImgs) {
		this.officialImgs = officialImgs;
	}

	public void setOfficialImgs(JSONArray imgs){
		this.officialImgs = new CdnImageList(imgs);
	}

	public Date getOfficialTime() {
		return officialTime;
	}

	public void setOfficialTime(Date officialTime) {
		this.officialTime = officialTime;
	}

	public String getVersionName() {
		return versionName;
	}

	public Long getCommentType() {
		return commentType;
	}

	public void setCommentType(Long commentType) {
		this.commentType = commentType;
	}

	public Long getTopStatus() {
		return topStatus;
	}

	public void setTopStatus(Long topStatus) {
		this.topStatus = topStatus;
	}

	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}

	public String getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

	public CdnImageList getImgs() {
		return imgs;
	}

	public void setImgs(JSONArray imgs){
		this.imgs = new CdnImageList(imgs);
	}


	public void setImgs(CdnImageList imgs) {
		this.imgs = imgs;
	}

	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public String getContent(){
		return this.content;
	}

	public void setContent(String content){
		this.content = content;
	}

	public Long getUserId(){
		return this.userId;
	}

	public void setUserId(Long userId){
		this.userId = userId;
	}

	public Long getGameId(){
		return this.gameId;
	}

	public void setGameId(Long gameId){
		this.gameId = gameId;
	}

	public Long getReplyCount(){
		return this.replyCount;
	}

	public void setReplyCount(Long replyCount){
		this.replyCount = replyCount;
	}

	public Long getPraiseCount(){
		return this.praiseCount;
	}

	public void setPraiseCount(Long praiseCount){
		this.praiseCount = praiseCount;
	}

	public String getModelName(){
		return this.modelName;
	}

	public void setModelName(String modelName){
		this.modelName = modelName;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Long getBeRepliedUserId(){
		return this.beRepliedUserId;
	}

	public void setBeRepliedUserId(Long beRepliedUserId){
		this.beRepliedUserId = beRepliedUserId;
	}

	public String getOfficialContent(){
		return this.officialContent;
	}

	public void setOfficialContent(String officialContent){
		this.officialContent = officialContent;
	}

	public Long getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(Long deleteFlag) {
		this.deleteFlag = deleteFlag;
	}
}

