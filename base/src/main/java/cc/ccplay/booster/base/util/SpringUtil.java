package cc.ccplay.booster.base.util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.context.ContextLoader;

import java.util.Map;

public class SpringUtil implements ApplicationContextAware {
    private static ApplicationContext appContext = null;

    public static Object getBean(String beanId) {
        return getAppContext().getBean(beanId);
    }

    public static <T> T getBean(Class<T> clazz) {
        return getAppContext().getBean(clazz);
    }

    @Override
    public void setApplicationContext(ApplicationContext appContext ){
        this.appContext = appContext;
    }

    public static ApplicationContext getAppContext() {
        if(appContext == null){
            appContext = ContextLoader.getCurrentWebApplicationContext();
        }
        return appContext;
    }
}
