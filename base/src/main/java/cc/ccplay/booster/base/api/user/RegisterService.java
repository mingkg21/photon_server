package cc.ccplay.booster.base.api.user;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.user.LoginResultDto;
import cc.ccplay.booster.base.model.user.UserAccount;
import cc.ccplay.booster.base.model.user.UserProfile;

public interface RegisterService {

	void sendRegisterSms(SystemParam sp, String phone);

	LoginResultDto registerByPhone(SystemParam sp, String phone, String password, String validateCode);

	Long register(SystemParam sp, String signupType, String phone, String password);

	/**
	 * 后台注册账号  或者 更新账号信息
	 * @param sp
	 * @param userAccount
	 * @param userProfile
	 */
	void registerOrUpdate(SystemParam sp, UserAccount userAccount, UserProfile userProfile);

	/**
	 * 通过手机短信重置密码
	 */
	boolean resetPassword(SystemParam sp, String phone, String password, String validateCode);

	/**
	 *  后台重置密码
	 */
	boolean resetPassword(SystemParam sp, long userId);
}
