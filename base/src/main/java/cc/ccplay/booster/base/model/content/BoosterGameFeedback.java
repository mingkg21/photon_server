package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="booster_game_feedback")
public class BoosterGameFeedback implements Serializable{

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "package_name")
	private String packageName;

	@Column(name = "version_code")
	private Long versionCode;

	@Column(name = "version_name")
	private String versionName;

	@Column(name = "user_id")
	private Long userId;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public String getPackageName(){
		return this.packageName;
	}

	public void setPackageName(String packageName){
		this.packageName = packageName;
	}

	public Long getVersionCode(){
		return this.versionCode;
	}

	public void setVersionCode(Long versionCode){
		this.versionCode = versionCode;
	}

	public String getVersionName(){
		return this.versionName;
	}

	public void setVersionName(String versionName){
		this.versionName = versionName;
	}

	public Long getUserId(){
		return this.userId;
	}

	public void setUserId(Long userId){
		this.userId = userId;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}


}

