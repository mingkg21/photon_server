package cc.ccplay.booster.base.enums;

import java.text.MessageFormat;

public enum BoosterCacheKey {

    /**
     *  数据库结构  HASH
     *  在线用户信息 {0}用户ID
     */
    ONLINE_USER("online_user:{0}"),


    /**
     * hash结构
     * 离线端口号
     * {0} 服务器ID
     * {1} 端口号
     */
    OFFLINE_PORT("offline_port:{0}:{1}"),


    /**
     * 数据结构 SET
     * 服务器在线用户id {0} 服务器ID
     */
    SERVER_ONLINE_USER_SET("server_online_user_set_{0}"),

    /**
     * 服务器离线端口集
     * {0}  服务器ID
     */
    SERVER_OFFLINE_PORT_SET("server_offline_port_set_{0}"),

    /**
     * 数据结构 SET
     * 在线端口列表 {0} 服务器ID
     */
    SERVER_ONLINE_PORT_SET("server_online_port_set_{0}"),


    /**
     * 包名对应的游戏ID
     */
    PACKAGE_NAME_MAPPING("package_name_mapping:{0}");


    private String keyTemplate;

    private BoosterCacheKey(String keyTemplate){
        this.keyTemplate = keyTemplate;
    }


    public String getKey(Object ...params){
        String ps []  = new String [params.length];
        for (int i = 0; i < params.length ; i++) {
            ps[i] = params[i].toString();
        }
        return MessageFormat.format(this.keyTemplate,ps);
    }

    public String getKeyTemplate() {
        return keyTemplate;
    }
}
