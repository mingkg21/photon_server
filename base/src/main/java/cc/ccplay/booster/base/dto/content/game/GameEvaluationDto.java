package cc.ccplay.booster.base.dto.content.game;

import cc.ccplay.booster.base.reader.UserInfoReader;
import cc.ccplay.booster.base.reader.dto.UserInfo;
import com.alibaba.fastjson.annotation.JSONField;
import cc.ccplay.booster.base.model.content.GameEvaluation;

import java.io.Serializable;

public class GameEvaluationDto extends AbstractGame implements UserInfoReader,Serializable {

    private GameEvaluation evaluation;

    private UserInfo userInfo;

    @JSONField(serialize = false)
    @Override
    public Long getUserId() {
        if(evaluation != null){
            return evaluation.getUserId();
        }
        return null;
    }

    @Override
    public void callback(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public GameEvaluation getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(GameEvaluation evaluation) {
        this.evaluation = evaluation;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }
}
