package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="game_comment_praise")
public class GameCommentPraise implements Serializable{

	@CompositeId
	private Id id;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;



	public Id getId(){
		return id;
	}

	public void setId(Id id){
		this.id = id;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}


	public static class Id{

		public Id(){}

		public Id(Long commentId,Long userId){
			this.commentId = commentId;
			this.userId = userId;
		}

		@Column(name = "comment_id")
		private Long commentId;

		@Column(name = "user_id")
		private Long userId;


		public Long getCommentId(){
			return this.commentId;
		}

		public void setCommentId(Long commentId){
			this.commentId = commentId;
		}

		public Long getUserId(){
			return this.userId;
		}

		public void setUserId(Long userId){
			this.userId = userId;
		}

	}
}

