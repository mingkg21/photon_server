package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.dto.content.game.GameEvaluationDto;
import cc.ccplay.booster.base.model.content.GameEvaluation;
import cc.ccplay.booster.base.core.SystemParam;
import org.easyj.frame.jdbc.Page;

public interface GameEvaluationService {

    public void saveEvaluation(SystemParam param, GameEvaluation evaluation);

    public Page<GameEvaluationDto> getUserEvaluationPage(SystemParam param, long gameId);

}
