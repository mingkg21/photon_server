package cc.ccplay.booster.base.test;

import java.lang.annotation.Annotation;
import java.util.Map;

public class Parameter {

    private Class type;

    private String name;

    private Map<Class,Annotation> annotations;

    public Parameter(Map<Class,Annotation> annotationMap,String name ,Class type){
        this.annotations = annotationMap;
        this.type = type;
        this.name = name;
    }

    public <T extends Annotation> T getAnnotation(Class<T> t){
        return (T)annotations.get(t);
    }

    public Class getType() {
        return type;
    }

    public String getName() {
        return name;
    }

}
