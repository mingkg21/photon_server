package cc.ccplay.booster.base.enums;

public enum MQMessageType {


    /**
     * 应用下载统计
     */
    LOG_APP_DOWNLOAD(MQMessageType.LOG_TOPIC,"app_download"),

    /**
     * 应用下载完成统计
     */
    //LOG_APP_DOWNLOAD_COMPLETE(MQMessageType.LOG_TOPIC,"app_download_complete")

    ;

    private MQMessageType(String topic,String tag){
        this.topic = topic;
        this.tag = tag;
    }

    public static final String LOG_TOPIC = "booster_log_topic";

    private String topic;

    private String tag;

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
