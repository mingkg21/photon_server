package cc.ccplay.booster.base.model.user;

import cc.ccplay.booster.base.model.adapter.CdnImage;
import org.easyj.frame.jdbc.annotation.Column;
import org.easyj.frame.jdbc.annotation.Id;
import org.easyj.frame.jdbc.annotation.Table;
import org.easyj.frame.jdbc.annotation.value.DateValue;
import org.easyj.frame.jdbc.annotation.value.IntValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.value.StringValue;
import org.easyj.frame.util.StringUtil;

import java.io.Serializable;
import java.util.Date;

@Table(name = "user_profile")
public class UserProfile implements Serializable{

	//性别 女
	public static final int SEX_GIRL = 0;
	//性别 男
	public static final int SEX_BOY = 1;
	//性别  保密
	public static final int SEX_SECRECY = 2;

	@Id
	@Column(name = "user_id")
	private Long userId;

	@Column(name = "head_icon")
	private CdnImage headIcon;
	/**
	 * 总积分 用于换算等级
	 */
	@IntValue(insert = true)
	@Column(name = "insight_level")
	private Integer insightLevel;
	/**
	 * 可用积分
	 */
	@IntValue(insert = true)
	@Column(name = "insight_point")
	private Integer insightPoint;
	/**
	 * 总积分 用于换算等级
	 */
	@IntValue(insert = true)
	@Column(name = "creativity_level")
	private Integer creativityLevel;
	/**
	 * 可用积分
	 */
	@IntValue(insert = true)
	@Column(name = "creativity_point")
	private Integer creativityPoint;

	@StringValue(insertValue = "这家伙很懒，什么都没留下！", insert = true)
	@Column(name = "signature")
	private String signature;

	@DateValue(insertValue="1990-01-01",format= "yyyy-MM-dd",insert = true)
	@Column(name = "birthday")
	private Date birthday;

	@Column(name = "area")
	private String area;

	@IntValue(insert = true,insertValue = SEX_SECRECY)
	@Column(name = "sex")
	private Integer sex;

	@IntValue(insert = true)
	@Column(name = "receive_praise_count")
	private Integer receivePraiseCount;

	@IntValue(insert = true)
	@Column(name = "follow_count")
	private Integer followCount;

	@IntValue(insert = true)
	@Column(name = "fans_count")
	private Integer fansCount;

	@IntValue(insert = true)
	@Column(name = "comment_count")
	private Integer commentCount;

	@IntValue(insert = true)
	@Column(name = "article_count")
	private Integer articleCount;

	@Column(name = "user_title")
	private String userTitle;

	@IntValue(insert = true)
	@Column(name = "verify_type")
	private Integer verifyType;

	@NowDateValue(update = true, insert = true)
	@Column(name = "update_datetime")
	private Date updateDatetime;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Integer getInsightPoint() {
		return insightPoint;
	}

	public void setInsightPoint(Integer insightPoint) {
		this.insightPoint = insightPoint;
	}

	public Integer getCreativityPoint() {
		return creativityPoint;
	}

	public void setCreativityPoint(Integer creativityPoint) {
		this.creativityPoint = creativityPoint;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public Integer getReceivePraiseCount() {
		return receivePraiseCount;
	}

	public void setReceivePraiseCount(Integer receivePraiseCount) {
		this.receivePraiseCount = receivePraiseCount;
	}

	public Integer getFollowCount() {
		return followCount;
	}

	public void setFollowCount(Integer followCount) {
		this.followCount = followCount;
	}

	public Integer getFansCount() {
		return fansCount;
	}

	public void setFansCount(Integer fansCount) {
		this.fansCount = fansCount;
	}

	public Integer getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(Integer commentCount) {
		this.commentCount = commentCount;
	}

	public Integer getArticleCount() {
		return articleCount;
	}

	public void setArticleCount(Integer articleCount) {
		this.articleCount = articleCount;
	}

	public String getUserTitle() {
		return userTitle;
	}

	public void setUserTitle(String userTitle) {
		this.userTitle = userTitle;
	}

	public Integer getVerifyType() {
		return verifyType;
	}

	public void setVerifyType(Integer verifyType) {
		this.verifyType = verifyType;
	}

	public Date getUpdateDatetime() {
		return updateDatetime;
	}

	public void setUpdateDatetime(Date updateDatetime) {
		this.updateDatetime = updateDatetime;
	}


	public void setHeadIcon(String headIcon) {
		if(StringUtil.isEmpty(headIcon)){
			return;
		}
		this.headIcon = new CdnImage(headIcon);
	}

	public CdnImage getHeadIcon() {
		return headIcon;
	}

	public void setHeadIcon(CdnImage headIcon) {
		this.headIcon = headIcon;
	}


	public Integer getInsightLevel() {
		return insightLevel;
	}

	public void setInsightLevel(Integer insightLevel) {
		this.insightLevel = insightLevel;
	}

	public Integer getCreativityLevel() {
		return creativityLevel;
	}

	public void setCreativityLevel(Integer creativityLevel) {
		this.creativityLevel = creativityLevel;
	}
}