package cc.ccplay.booster.base.dto.content.booster;


import cc.ccplay.booster.base.model.adapter.CdnImage;
import cc.ccplay.booster.base.model.adapter.Icon;

import java.util.Date;

public class SyncPackageMappingDto implements java.io.Serializable{

    private Long id;

    private Long ccGameId;

    private String packageName;

    private Long type;

    private Long gameId;

    private String ccGameName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Icon("160")
    private CdnImage ccGameIcon;

    private Date createTime;

    public Long getCcGameId() {
        return ccGameId;
    }

    public void setCcGameId(Long ccGameId) {
        this.ccGameId = ccGameId;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    public String getCcGameName() {
        return ccGameName;
    }

    public void setCcGameName(String ccGameName) {
        this.ccGameName = ccGameName;
    }

    public CdnImage getCcGameIcon() {
        return ccGameIcon;
    }

    public void setCcGameIcon(CdnImage ccGameIcon) {
        this.ccGameIcon = ccGameIcon;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
