package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.AppChannelManage;
import org.easyj.frame.jdbc.Page;

public interface AppChannelManageService {

    public AppChannelManage getChannelManage(SystemParam parameter, long id);

    public Page getList(SystemParam parameter);

    public AppChannelManage saveOrUpdate(SystemParam parameter, AppChannelManage channel);

    public void updateStatus(SystemParam param, long id, Status status);

    public AppChannelManage findAppChannelManage(String code);

}
