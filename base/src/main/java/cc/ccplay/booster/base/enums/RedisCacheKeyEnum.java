package cc.ccplay.booster.base.enums;

import java.text.MessageFormat;

public enum RedisCacheKeyEnum {

	/**
	 * 短信验证码
	 */
	sms_validatecode("SMS_VALIDATECODE_{0}"),

	/**
	 * 短信间隔
	 */
	sms_interval("SMS_INTERVAL_{0}"),

	/**
	 * 用户Token key   {0} token
	 */
	user_token_key("user:token:{0}"),

	/**
	 * 存储用户id对应的登录token  {0} 用户ID
	 */
	user_id_token("user:id_token_key:{0}")
	;

	private String keyTemplate;

	private RedisCacheKeyEnum(String keyTemplate) {
		this.keyTemplate = keyTemplate;
	}

	public String getKey(Object... args) {
		String [] params = new String[args.length];
		for (int i = 0; i < args.length; i++) {
			if(args[i] != null){
				params[i] = args[i].toString();
			}
		}
		return MessageFormat.format(this.keyTemplate, params);
	}

	public String getKeyTemplate() {
		return keyTemplate;
	}

	public void setKeyTemplate(String keyTemplate) {
		this.keyTemplate = keyTemplate;
	}
}
