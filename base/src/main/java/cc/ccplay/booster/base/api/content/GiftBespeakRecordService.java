package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.gift.GiftBespeakRecordPageDto;
import cc.ccplay.booster.base.model.content.GiftBespeakRecord;
import org.easyj.frame.jdbc.Page;

public interface GiftBespeakRecordService {

    public Page<GiftBespeakRecordPageDto> getPage(SystemParam param, Long gameId, String giftName);

    public GiftBespeakRecord save(SystemParam param, GiftBespeakRecord bespeakRecord);

}
