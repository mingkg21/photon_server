package cc.ccplay.booster.base.dto.content.booster;

import java.io.Serializable;
import java.util.List;

public class ServerStatusDto implements Serializable {

    private List<ServerPortStatusDto> ports;

    private Long pid;

    private Long serverId;

    private String uptime;

    public String getUptime() {
        return uptime;
    }

    public Long getServerId() {
        return serverId;
    }

    public void setServerId(Long serverId) {
        this.serverId = serverId;
    }

    public void setUptime(String uptime) {
        this.uptime = uptime;
    }

    public List<ServerPortStatusDto> getPorts() {
        return ports;
    }

    public void setPorts(List<ServerPortStatusDto> ports) {
        this.ports = ports;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }
}
