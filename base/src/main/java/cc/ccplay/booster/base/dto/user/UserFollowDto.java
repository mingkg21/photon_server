package cc.ccplay.booster.base.dto.user;

import cc.ccplay.booster.base.model.user.UserAccount;
import cc.ccplay.booster.base.model.user.UserProfile;
import cc.ccplay.booster.base.reader.dto.UserInfo;
import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;

public class UserFollowDto implements Serializable {

    @JSONField(serialize = false)
    private UserAccount userAccount;

    @JSONField(serialize = false)
    private UserProfile userProfile;

    private Long followType;

    /**
     * 以UserInfo对象渲染 json
     * @return
     */
    public UserInfo getUserInfo() {
        if(userAccount == null){
            return null;
        }
        UserInfo userInfo = new UserInfo();
        userInfo.setUserAccount(this.userAccount);
        userInfo.setUserProfile(this.userProfile);
        return userInfo;
    }


    public UserAccount getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public Long getFollowType() {
        return followType;
    }

    public void setFollowType(Long followType) {
        this.followType = followType;
    }
}
