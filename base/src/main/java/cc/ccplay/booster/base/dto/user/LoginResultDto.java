package cc.ccplay.booster.base.dto.user;

import java.io.Serializable;

public class LoginResultDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 624299433696633428L;


	private String userToken;
	private Long userId;
	private String displayName;

	/**
	 * 0:未绑定
	 * 1：绑定
	 */
	private Integer bindPhone;

	public LoginResultDto() {

	}

	public LoginResultDto(String userToken, Long userId) {
		super();
		this.userToken = userToken;
		this.userId = userId;
	}

	public LoginResultDto(String userToken, Long userId, String displayName) {
		super();
		this.userToken = userToken;
		this.userId = userId;
		this.displayName = displayName;
	}

	public String getUserToken() {
		return userToken;
	}

	public void setUserToken(String userToken) {
		this.userToken = userToken;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Integer getBindPhone() {
		return bindPhone;
	}

	public void setBindPhone(Integer bindPhone) {
		this.bindPhone = bindPhone;
	}
}
