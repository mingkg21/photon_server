package cc.ccplay.booster.base.dto;

public class ChartReportDto implements java.io.Serializable {

    private String label;

    private Long value;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

}
