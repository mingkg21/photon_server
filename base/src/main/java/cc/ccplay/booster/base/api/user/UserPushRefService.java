package cc.ccplay.booster.base.api.user;

public interface UserPushRefService {
	
	void saveUserPushRef(String pushDeviceId, Long userId, String deviceNo, String userAgent, String ip);

}
