package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.model.content.AdAreaItem;
import cc.ccplay.booster.base.core.SystemParam;
import org.easyj.frame.jdbc.Page;

public interface AdAreaItemService {

    public void saveOrUpdate(AdAreaItem adAreaItem);

    public Page getPage(SystemParam param,long adId);

    public void delete(long id);

    public AdAreaItem get(long id);

    public Page<AdAreaItem> getPageByCode(SystemParam param,String code);
}
