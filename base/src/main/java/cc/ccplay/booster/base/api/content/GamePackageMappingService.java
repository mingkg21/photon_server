package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.game.GameInfoDto;
import cc.ccplay.booster.base.dto.content.game.GamePackageMappingDto;
import cc.ccplay.booster.base.model.content.GamePackageMapping;
import org.easyj.frame.jdbc.Page;

import java.util.List;

public interface GamePackageMappingService {

    public void saveOrUpdate(GamePackageMapping mapping);

    public Page getPage(SystemParam param,GamePackageMapping searchObj);

    public void delete(long id);

    public GamePackageMappingDto getDto(long id);

    public void batchSave(List<GamePackageMapping> mappings);

    public List<GameInfoDto> getMappingGame(List<String> packageNames);

    public List getMappingList(long startId,int size);

}


