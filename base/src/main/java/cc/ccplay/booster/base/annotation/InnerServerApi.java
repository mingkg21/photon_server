package cc.ccplay.booster.base.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//内部服务器API
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface InnerServerApi {

}