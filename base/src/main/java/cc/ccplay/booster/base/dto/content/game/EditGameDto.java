package cc.ccplay.booster.base.dto.content.game;

import cc.ccplay.booster.base.dto.SelectizeDto;
import cc.ccplay.booster.base.model.content.GameInfo;

import java.io.Serializable;
import java.util.List;

public class EditGameDto implements Serializable{

    private GameInfo gameInfo;
    private SelectizeDto publisher;
    private SelectizeDto category;
    private List<SelectizeDto> tags;

    private List<SelectizeDto> specialTags;

    public List<SelectizeDto> getSpecialTags() {
        return specialTags;
    }

    public void setSpecialTags(List<SelectizeDto> specialTags) {
        this.specialTags = specialTags;
    }

    public GameInfo getGameInfo() {
        return gameInfo;
    }

    public void setGameInfo(GameInfo gameInfo) {
        this.gameInfo = gameInfo;
    }

    public SelectizeDto getPublisher() {
        return publisher;
    }

    public void setPublisher(SelectizeDto publisher) {
        this.publisher = publisher;
    }

    public SelectizeDto getCategory() {
        return category;
    }

    public void setCategory(SelectizeDto category) {
        this.category = category;
    }

    public List<SelectizeDto> getTags() {
        return tags;
    }

    public void setTags(List<SelectizeDto> tags) {
        this.tags = tags;
    }
}
