package cc.ccplay.booster.base.model.content;

import cc.ccplay.booster.base.model.adapter.CdnImage;
import cc.ccplay.booster.base.enums.Status;
import com.alibaba.fastjson.annotation.JSONField;
import org.easyj.frame.jdbc.annotation.*;
import org.easyj.frame.jdbc.annotation.value.LongValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.util.StringUtil;

import java.io.Serializable;
import java.util.Date;

@Table(name="game_tag")
public class GameTag implements Serializable{

    //普通标签
    public static final long TYPE_COMMON = 0;
    //特色标签
    public static final long TYPE_SPECIAL = 1;

    @Id
    @GeneratedValue(type = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name="name")
    private String name;

    @NowDateValue(insert = true)
    @Column(name="create_time")
    private Date createTime;

    @NowDateValue(update = true,insert = true)
    @Column(name="update_time")
    private Date updateTime;

    @JSONField(serialize = false)
    @LogicDelete("1")
    @Where(clause = "delete_flag=0")
    @LongValue(insert = true,insertValue = 0)
    @Column(name = "delete_flag")
    private Long deleteFlag;

    @Column(name="status")
    private Status status;

    @Column(name="description")
    private String description;

    @Column(name="image")
    private CdnImage image;

    @Column(name="icon")
    private CdnImage icon;

    /**
     * 顶部排序
     */
    @Column(name="ordering")
    private Long ordering;


    /**
     * 推荐排序
     */
    @Column(name="recommend_ordering")
    private Long recommendOrdering;


    @Column(name="type")
    private Long type;

    @LongValue(insert = true,insertValue = 0)
    @Column(name="app_count")
    private Long appCount;

    @LongValue(insert = true,insertValue = 0)
    @Column(name="tag_count")
    private Long tagCount;

    @Column(name="code")
    private String code;


    public Long getTagCount() {
        return tagCount;
    }

    public void setTagCount(Long tagCount) {
        this.tagCount = tagCount;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getAppCount() {
        return appCount;
    }

    public void setAppCount(Long appCount) {
        this.appCount = appCount;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Long getRecommendOrdering() {
        return recommendOrdering;
    }

    public void setRecommendOrdering(Long recommendOrdering) {
        this.recommendOrdering = recommendOrdering;
    }

    public CdnImage getIcon() {
        return icon;
    }

    public void setIcon(CdnImage icon) {
        this.icon = icon;
    }

    public void setIcon(String icon){
        if(icon == null){
            this.icon = null;
        }else{
            this.icon = new CdnImage(icon);
        }
    }

    public Long getOrdering() {
        return ordering;
    }

    public void setOrdering(Long ordering) {
        this.ordering = ordering;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CdnImage getImage() {
        return image;
    }

    public void setImage(CdnImage image) {
        this.image = image;
    }

    public void setImage(String image) {
        if(StringUtil.isNotEmpty(image)){
            this.image = new CdnImage(image);
        }else{
            this.image = null;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Long deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
