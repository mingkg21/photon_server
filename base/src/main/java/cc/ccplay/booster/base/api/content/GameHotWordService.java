package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.content.GameHotWord;
import org.easyj.frame.jdbc.Page;

public interface GameHotWordService {

    public void saveOrUpdate(SystemParam param, GameHotWord hotWord);

    public void setHotWord(SystemParam param,long wordId);

    public void delete(SystemParam param,long wordId);

//    public HotWordsDto getHotWords(SystemParam param);


    public GameHotWord getDefaultHotword();

    public Page<GameHotWord> getList(SystemParam param);

    public Page queryPage(SystemParam param,String name);

    public GameHotWord get(SystemParam param,long id);
}
