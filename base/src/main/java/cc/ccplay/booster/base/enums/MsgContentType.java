package cc.ccplay.booster.base.enums;

import java.text.MessageFormat;

public enum MsgContentType {

    /**
     * 游戏评论被回复
     */
    GAME_COMMENT_BEREPLIED(1,"{0}回复你的在《{1}》评论"),

    /**
     * 游戏更新
     */
    GAME_UPDATED(2,"游戏《{0}》已更新"),

    /**
     * 存档评论被回复
     */
    SAVE_GAME_COMMENT_BEREPLIED(3,"{0}回复你的在存档《{1}》评论"),

    /**
     * 内链
     */
    URL_LINKED(4,"{0}"),


    /**
     * 官方回复反馈意见
     */
    SUGGESTION_FEEDBACK_REPLY(5,"官方回复了你的意见反馈"),


    /**
     *  跳转到信息中心
     */
    JUMP_TO_MESSAGE_CENTER(6,"{0}"),

    /**
     * 外链 通过浏览器打开
     */
    OUT_URL_LINKED(7,"{0}");



    private MsgContentType(int value, String titleTpl){
        this.value = value;
        this.titleTpl = titleTpl;
    }

    private String titleTpl;
    private int value;

    public String getTitleTpl() {
        return titleTpl;
    }

    public int getValue() {
        return value;
    }

    public String getTitle(Object ...args){
       return MessageFormat.format(this.titleTpl,args);
    }

    public MsgContentType getMsgTypeByValue(int value){
        MsgContentType [] msgTypes = MsgContentType.values();
        for (int i = 0; i < msgTypes.length; i++) {
            MsgContentType mt = msgTypes[i];
            if(mt.value == value){
                return mt;
            }
        }
        return null;
    }
}
