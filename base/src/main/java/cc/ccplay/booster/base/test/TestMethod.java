package cc.ccplay.booster.base.test;

import com.alibaba.fastjson.annotation.JSONField;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestMethod {

    private static final LocalVariableTableParameterNameDiscoverer parameterNameDiscoverer = new LocalVariableTableParameterNameDiscoverer();

    /**
     * 获取方法所有参数名
     * @param method
     * @return
     */
    private static String[] getParameterNames(Method method) {
        return parameterNameDiscoverer.getParameterNames(method);
    }

    private String name;

    @JSONField(serialize = false)
    private Method method;

    private boolean paging;

    private boolean needLogin;

    @JSONField(serialize = false)
    private String requestMapping;

    @JSONField(serialize = false)
    private TestClass parentClass;

    private String url;

    private List<TestMethodParameter> parameters;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public TestClass getParentClass() {
        return parentClass;
    }

    public void setParentClass(TestClass parentClass) {
        this.parentClass = parentClass;
    }

    public List<TestMethodParameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<TestMethodParameter> parameters) {
        this.parameters = parameters;
    }

    public String getRequestMapping() {
        return requestMapping;
    }

    public void setRequestMapping(String requestMapping) {
        this.requestMapping = requestMapping;
    }

    public boolean isPaging() {
        return paging;
    }

    public void setPaging(boolean paging) {
        this.paging = paging;
    }

    public boolean isNeedLogin() {
        return needLogin;
    }

    public void setNeedLogin(boolean needLogin) {
        this.needLogin = needLogin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public static List<Parameter> getParameter(Method method){
        List<Parameter> list = new ArrayList<>();
        Annotation [][] annotations = method.getParameterAnnotations();
        Class [] types = method.getParameterTypes();
        String [] parameterNames = getParameterNames(method);
        for (int i = 0; i < annotations.length; i++) {
            Class type = types[i];
            Map<Class,Annotation> annotationMap = new HashMap<>();
            for (int j = 0; j < annotations[i].length; j++) {
                Annotation ann = annotations[i][j];
                annotationMap.put(ann.annotationType(),ann);
            }
            list.add(new Parameter(annotationMap,parameterNames[i],type));
        }
        return list;
    }
}
