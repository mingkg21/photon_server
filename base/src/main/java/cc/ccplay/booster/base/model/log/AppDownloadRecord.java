package cc.ccplay.booster.base.model.log;

import org.easyj.frame.jdbc.annotation.*;

@Table(name="app_download_record")
public class AppDownloadRecord extends BaseLogModel implements java.io.Serializable {

    @Id
    @GeneratedValue(type = GenerationType.AUTO)
    private Long id;

    @Column(name="app_id")
    private Long appId;

    @Column(name="version_name")
    private String versionName;

//    @Column(name="package_name")
//    private String packageName;

    @Column(name="version_id")
    private Long versionId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAppId() {
        return appId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

//    public String getPackageName() {
//        return packageName;
//    }
//
//    public void setPackageName(String packageName) {
//        this.packageName = packageName;
//    }

    public Long getVersionId() {
        return versionId;
    }

    public void setVersionId(Long versionId) {
        this.versionId = versionId;
    }
}
