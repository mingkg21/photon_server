package cc.ccplay.booster.base.model.content;

import cc.ccplay.booster.base.enums.Status;
import org.easyj.frame.jdbc.annotation.value.LongValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import com.alibaba.fastjson.annotation.JSONField;
import java.util.Date;
import java.io.Serializable;

@Table(name="game_test_type")
public class GameTestType implements Serializable{

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;


	@Column(name = "name")
	private String name;

	@JSONField(serialize = false)
	@LogicDelete("1")
	@Where(clause = "delete_flag=0")
	@LongValue(insert = true,insertValue = 0)
	@Column(name = "delete_flag")
	private Long deleteFlag;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@Column(name = "status")
	private Status status;

	@NowDateValue(update = true,insert = true)
	@Column(name = "update_time")
	private Date updateTime;



	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public String getName(){
		return this.name;
	}

	public void setName(String name){
		this.name = name;
	}

	public Long getDeleteFlag(){
		return this.deleteFlag;
	}

	public void setDeleteFlag(Long deleteFlag){
		this.deleteFlag = deleteFlag;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Status getStatus(){
		return this.status;
	}

	public void setStatus(Status status){
		this.status = status;
	}

	public Date getUpdateTime(){
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime){
		this.updateTime = updateTime;
	}


}

