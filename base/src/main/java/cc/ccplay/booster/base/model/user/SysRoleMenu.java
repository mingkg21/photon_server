package cc.ccplay.booster.base.model.user;

import org.easyj.frame.jdbc.annotation.Column;
import org.easyj.frame.jdbc.annotation.CompositeId;
import org.easyj.frame.jdbc.annotation.Table;

import java.io.Serializable;

@Table(name="sys_role_menu")
public class SysRoleMenu implements Serializable {

    @CompositeId
    private Id id;

    public Id getId() {
        return id;
    }

    public void setId(Id id) {
        this.id = id;
    }

    public void setId(Long roleId,Long menuId){
        Id id = new Id(menuId,roleId);
        this.id = id;
    }

    public static class Id{

        public Id(){}

        public Id(Long menuId,Long roleId){
            this.menuId = menuId;
            this.roleId = roleId;
        }

        @Column(name="menu_id")
        private Long menuId;

        @Column(name="role_id")
        private Long roleId;

        public Long getMenuId() {
            return menuId;
        }

        public void setMenuId(Long menuId) {
            this.menuId = menuId;
        }

        public Long getRoleId() {
            return roleId;
        }

        public void setRoleId(Long roleId) {
            this.roleId = roleId;
        }
    }
}
