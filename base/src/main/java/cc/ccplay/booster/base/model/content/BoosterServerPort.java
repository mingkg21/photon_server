package cc.ccplay.booster.base.model.content;

import cc.ccplay.booster.base.constants.Constant;
import org.easyj.frame.jdbc.annotation.value.LongValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="booster_server_port")
public class BoosterServerPort implements Serializable{

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;


	@Column(name = "port")
	private Long port;

	@Column(name = "password")
	private String password;

	@Column(name = "status")
	@LongValue(insert = true,insertValue = Constant.STATUS_ENABLED)
	private Long status;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@Column(name = "server_id")
	private Long serverId;



	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getPort(){
		return this.port;
	}

	public void setPort(Long port){
		this.port = port;
	}

	public String getPassword(){
		return this.password;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public Long getStatus(){
		return this.status;
	}

	public void setStatus(Long status){
		this.status = status;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Long getServerId(){
		return this.serverId;
	}

	public void setServerId(Long serverId){
		this.serverId = serverId;
	}


}

