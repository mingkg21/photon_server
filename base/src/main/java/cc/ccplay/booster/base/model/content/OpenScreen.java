package cc.ccplay.booster.base.model.content;

import cc.ccplay.booster.base.constants.Constant;
import cc.ccplay.booster.base.model.adapter.CdnImage;
import org.easyj.frame.jdbc.annotation.value.LongValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import org.easyj.frame.util.StringUtil;

import java.util.Date;
import java.io.Serializable;

@Table(name="open_screen")
public class OpenScreen implements Serializable{


	public static final long AUTO_DOWNLOAD_NO = 1;

	public static final long AUTO_DOWNLOAD_YES = 2;

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;


	@Column(name = "title")
	private String title;

	@Column(name = "img_url")
	private CdnImage imgUrl;

	@Column(name = "app_id")
	private Long appId;

	@LongValue(insert = true,insertValue = Constant.STATUS_DISABLED)
	@Column(name = "status")
	private Long status;

	@Column(name = "auto_download")
	private Long autoDownload;

	@Column(name = "showtime")
	private Long showtime;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;



	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public String getTitle(){
		return this.title;
	}

	public void setTitle(String title){
		this.title = title;
	}


	public CdnImage getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(CdnImage imgUrl) {
		this.imgUrl = imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		if(StringUtil.isEmpty(imgUrl)){
			this.imgUrl = new CdnImage("");
		}else{
			this.imgUrl = new CdnImage(imgUrl);
		}

	}

	public Long getAppId(){
		return this.appId;
	}

	public void setAppId(Long appId){
		this.appId = appId;
	}

	public Long getStatus(){
		return this.status;
	}

	public void setStatus(Long status){
		this.status = status;
	}

	public Long getAutoDownload(){
		return this.autoDownload;
	}

	public void setAutoDownload(Long autoDownload){
		this.autoDownload = autoDownload;
	}

	public Long getShowtime(){
		return this.showtime;
	}

	public void setShowtime(Long showtime){
		this.showtime = showtime;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}


}

