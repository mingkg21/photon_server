package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.value.LongValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import com.alibaba.fastjson.annotation.JSONField;
import java.util.Date;
import java.io.Serializable;

@Table(name="gift_number")
public class GiftNumber implements Serializable{

	/**
	 * 已经使用
	 */
	public static final long STATUS_USED = 1L;

	/**
	 * 未使用
	 */
	public static final long STATUS_UNUSED = 0L;

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;


	@Column(name = "gift_id")
	private Long giftId;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@Column(name = "code")
	private String code;

	@Column(name="status")
	@LongValue(insert = true,insertValue = STATUS_UNUSED)
	private Long status;

	@JSONField(serialize = false)
	@LogicDelete("1")
	@Where(clause = "delete_flag=0")
	@LongValue(insert = true,insertValue = 0)
	@Column(name = "delete_flag")
	private Long deleteFlag;


	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getGiftId(){
		return this.giftId;
	}

	public void setGiftId(Long giftId){
		this.giftId = giftId;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public String getCode(){
		return this.code;
	}

	public void setCode(String code){
		this.code = code;
	}

	public Long getDeleteFlag(){
		return this.deleteFlag;
	}

	public void setDeleteFlag(Long deleteFlag){
		this.deleteFlag = deleteFlag;
	}


}

