package cc.ccplay.booster.base.model.content;

import cc.ccplay.booster.base.constants.Constant;
import cc.ccplay.booster.base.model.adapter.CdnImage;
import org.easyj.frame.jdbc.annotation.value.LongValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import org.easyj.frame.util.StringUtil;

import java.util.Date;
import java.io.Serializable;

@Table(name="app_topic")
public class AppTopic implements Serializable{

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;


	@Column(name = "title")
	private String title;

	@Column(name = "status")
	@LongValue(insert = true,insertValue = Constant.STATUS_ENABLED)
	private Long status;

	@Column(name = "pic_url")
	private CdnImage picUrl;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@Column(name = "ordering")
	private Long ordering;



	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public String getTitle(){
		return this.title;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public Long getStatus(){
		return this.status;
	}

	public void setStatus(Long status){
		this.status = status;
	}

	public CdnImage getPicUrl(){
		return this.picUrl;
	}

	public void setPicUrl(CdnImage image){
		this.picUrl = image;
	}


	public void setPicUrl(String picUrl){
		if(StringUtil.isEmpty(picUrl)){
			this.picUrl = new CdnImage("");
		}else{
			this.picUrl = new CdnImage(picUrl);
		}

	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Long getOrdering(){
		return this.ordering;
	}

	public void setOrdering(Long ordering){
		this.ordering = ordering;
	}


}

