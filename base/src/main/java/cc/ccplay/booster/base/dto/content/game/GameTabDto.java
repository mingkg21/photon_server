package cc.ccplay.booster.base.dto.content.game;

public class GameTabDto implements java.io.Serializable{

    private int details = 0;

    private int comment = 0;

    private int save = 0;
    /**
     * 0不显示  其他显示数量
     */
    private int gift = 0;

    public int getDetails() {
        return details;
    }

    public void setDetails(int details) {
        this.details = details;
    }

    public int getComment() {
        return comment;
    }

    public void setComment(int comment) {
        this.comment = comment;
    }

    public int getSave() {
        return save;
    }

    public void setSave(int save) {
        this.save = save;
    }

    public int getGift() {
        return gift;
    }

    public void setGift(int gift) {
        this.gift = gift;
    }
}
