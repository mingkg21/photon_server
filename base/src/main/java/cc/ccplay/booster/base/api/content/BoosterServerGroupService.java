package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.SelectizeDto;
import cc.ccplay.booster.base.model.content.BoosterServerGroup;
import org.easyj.frame.jdbc.Page;

import java.util.List;

public interface BoosterServerGroupService {

    public Page getPage(SystemParam param,String name);

    public void saveOrUpdate(BoosterServerGroup boosterServerGroup);

    public BoosterServerGroup get(long id);

    public List<SelectizeDto> getForSelectize(SystemParam param,String key);

}
