package cc.ccplay.booster.base.dto.content.booster;

import cc.ccplay.booster.base.model.content.BoosterServer;
import cc.ccplay.booster.base.model.content.BoosterServerGroupRef;

import java.io.Serializable;

public class BoosterServerGroupRefDto implements Serializable {

    private BoosterServerGroupRef ref;

    private BoosterServer server;

    public BoosterServerGroupRef getRef() {
        return ref;
    }

    public void setRef(BoosterServerGroupRef ref) {
        this.ref = ref;
    }

    public BoosterServer getServer() {
        return server;
    }

    public void setServer(BoosterServer server) {
        this.server = server;
    }
}
