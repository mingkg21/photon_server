package cc.ccplay.booster.base.api;

import cc.ccplay.booster.base.reader.UserInfoReader;
import cc.ccplay.booster.base.reader.dto.UserInfo;
import cc.ccplay.booster.base.util.SpringUtil;
import cc.ccplay.booster.base.api.user.UserAccountService;
import cc.ccplay.booster.base.dto.SelectizeDto;
import cc.ccplay.booster.base.reader.ManyUserInfoReader;
import org.apache.commons.beanutils.BeanUtils;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.StringUtil;

import java.lang.reflect.InvocationTargetException;
import java.util.*;

public class BaseService {

    private static UserAccountService userAccountService;

    private static UserAccountService getUserAccountService(){
        if(userAccountService == null){
            userAccountService = SpringUtil.getBean(UserAccountService.class);
        }
        return userAccountService;
    }


    protected List<SelectizeDto> toSelectizeData(List list,String idKey,String nameKey) {
        int size = list.size();
        List<SelectizeDto> result = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            Object row = list.get(i);
            SelectizeDto dto = new SelectizeDto();
            if(row instanceof Map){
                Map rowMap = (Map)list.get(i);
                dto.setId(rowMap.get(idKey));
                dto.setName(StringUtil.trim2Empty(rowMap.get(nameKey)));
            }else{
                try {
                    dto.setId(BeanUtils.getProperty(row, idKey));
                    dto.setName(BeanUtils.getProperty(row,nameKey));
                } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                    e.printStackTrace();
                    throw new GenericException("数据格式转化过程出错");
                }
            }
            result.add(dto);
        }
        return result;
    }

   protected List<SelectizeDto> toSelectizeData(Page page,String idKey,String nameKey){
        return this.toSelectizeData(page.getList(),idKey,nameKey);
    }


    protected  void readManyUserInfo(ManyUserInfoReader reader){
       List<UserInfoReader> readers = reader.getReaderList();
       if(readers != null){
           readUserInfo(readers);
       }
    }

    protected void readManyUserInfo(List<? extends ManyUserInfoReader> readers){
        int size = 0;
        if(readers == null || (size = readers.size()) == 0){
            return;
        }
        List<UserInfoReader> rs = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            ManyUserInfoReader reader = readers.get(i);
            List<UserInfoReader> list = reader.getReaderList();
            if(list != null && list.size() > 0){
                rs.addAll(list);
            }
        }
        readUserInfo(rs);
    }


    protected void readUserInfo(UserInfoReader reader){
        if(reader == null){
            return;
        }
        long userId = reader.getUserId();
        UserInfo userInfo = getUserAccountService().getUserInfo(userId);
        if(userInfo != null){
            reader.callback(userInfo);
        }
    }

    protected void readUserInfo(List<? extends UserInfoReader> readers){
        int size = 0;
        if(readers == null || (size = readers.size()) == 0){
            return;
        }
        Set<Long> idSet = new HashSet<>();
        for (int i = 0; i < size; i++) {
            UserInfoReader reader = readers.get(i);
            Long userId = reader.getUserId();
            if(userId == null){
                continue;
            }
            idSet.add(userId);
        }
        Long [] arr = new Long[idSet.size()];
        Long [] ids = idSet.toArray(arr);
        List<UserInfo> userInfoList = getUserAccountService().getUserInfo(ids);
        Map<Long,UserInfo> userMap = toUserMap(userInfoList);
        for (int i = 0; i < size; i++) {
            UserInfoReader reader = readers.get(i);
            Long userId = reader.getUserId();
            if(userId == null){
                continue;
            }
            UserInfo userInfo = userMap.get(userId);
            if(userInfo != null){
                reader.callback(userInfo);
            }
        }
    }

    protected void readUserInfo(Page<? extends UserInfoReader> readerPage){
        this.readUserInfo(readerPage.getList());
    }

    private Map<Long,UserInfo> toUserMap( List<UserInfo> userInfoList){
        Map<Long,UserInfo> map = new HashMap<>();
        int size = userInfoList.size();
        for (int i = 0; i < size; i++) {
            UserInfo userInfo = userInfoList.get(i);
            Long userId = userInfo.getUserAccount().getId();
            map.put(userId,userInfo);
        }
        return map;
    }
}
