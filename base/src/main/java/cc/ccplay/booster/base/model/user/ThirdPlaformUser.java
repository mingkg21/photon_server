package cc.ccplay.booster.base.model.user;

import org.easyj.frame.jdbc.annotation.*;
import java.io.Serializable;

@Table(name="third_platform_user")
public class ThirdPlaformUser implements Serializable{

	//微信
	public static long PLATFORM_WECHAT = 1;

	//QQ
	public static long PLATFORM_QQ = 2;

	//18汉化
	public static long PLATFORM_18HANHUA = 3;

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "open_id")
	private String openId;

	@Column(name = "unionid")
	private String unionid;

	@Column(name = "sex")
	private String sex;

	@Column(name = "headimg")
	private String headimg;

	/**
	 对应user_account表的ID
	 */
	@Column(name = "user_id")
	private Long userId;

	@Column(name = "nickname")
	private String nickname;

	@Column(name = "platform")
	private Long platform;

	@Column(name = "user_info_json")
	private String userInfoJson;


	public String getUserInfoJson() {
		return userInfoJson;
	}

	public void setUserInfoJson(String userInfoJson) {
		this.userInfoJson = userInfoJson;
	}

	public Long getPlatform() {
		return platform;
	}

	public void setPlatform(Long platform) {
		this.platform = platform;
	}

	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public String getOpenId(){
		return this.openId;
	}

	public void setOpenId(String openId){
		this.openId = openId;
	}

	public String getUnionid(){
		return this.unionid;
	}

	public void setUnionid(String unionid){
		this.unionid = unionid;
	}

	public String getSex(){
		return this.sex;
	}

	public void setSex(String sex){
		this.sex = sex;
	}

	public String getHeadimg(){
		return this.headimg;
	}

	public void setHeadimg(String headimg){
		this.headimg = headimg;
	}

	public Long getUserId(){
		return this.userId;
	}

	public void setUserId(Long userId){
		this.userId = userId;
	}

	public String getNickname(){
		return this.nickname;
	}

	public void setNickname(String nickname){
		this.nickname = nickname;
	}


}

