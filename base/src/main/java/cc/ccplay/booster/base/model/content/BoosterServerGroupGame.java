package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="booster_server_group_game")
public class BoosterServerGroupGame implements Serializable{

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;


	@Column(name = "game_id")
	private Long gameId;

	@Column(name = "group_id")
	private Long groupId;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;



	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getGameId(){
		return this.gameId;
	}

	public void setGameId(Long gameId){
		this.gameId = gameId;
	}

	public Long getGroupId(){
		return this.groupId;
	}

	public void setGroupId(Long groupId){
		this.groupId = groupId;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}


}

