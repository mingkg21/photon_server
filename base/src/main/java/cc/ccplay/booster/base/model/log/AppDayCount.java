package cc.ccplay.booster.base.model.log;

import org.easyj.frame.jdbc.annotation.value.LongValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="app_day_count")
public class AppDayCount implements Serializable{

	@CompositeId
	private Id id;

	@Column(name = "download_count")
	@LongValue(insertValue = 0,insert = true)
	private Long downloadCount;

	@Column(name = "download_complete_count")
	@LongValue(insertValue = 0,insert = true)
	private Long downloadCompleteCount;

	@Column(name = "view_count")
	@LongValue(insertValue = 0,insert = true)
	private Long viewCount;

	@NowDateValue(update = true,insert = true)
	@Column(name = "update_time")
	private Date updateTime;


	public Id getId(){
		return id;
	}

	public void setId(Id id){
		this.id = id;
	}

	public Long getDownloadCount(){
		return this.downloadCount;
	}

	public void setDownloadCount(Long downloadCount){
		this.downloadCount = downloadCount;
	}

	public Long getDownloadCompleteCount(){
		return this.downloadCompleteCount;
	}

	public void setDownloadCompleteCount(Long downloadCompleteCount){
		this.downloadCompleteCount = downloadCompleteCount;
	}

	public Long getViewCount(){
		return this.viewCount;
	}

	public void setViewCount(Long viewCount){
		this.viewCount = viewCount;
	}

	public Date getUpdateTime(){
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime){
		this.updateTime = updateTime;
	}


	public static class Id implements Serializable {

		public Id(){}

		public Id(Long appId,Date countDay){
			this.appId = appId;
			this.countDay = countDay;
		}

		@Column(name = "app_id")
		private Long appId;

		@Column(name = "count_day")
		private Date countDay;


		public Long getAppId(){
			return this.appId;
		}

		public void setAppId(Long appId){
			this.appId = appId;
		}

		public Date getCountDay(){
			return this.countDay;
		}

		public void setCountDay(Date countDay){
			this.countDay = countDay;
		}

	}
}

