package cc.ccplay.booster.base.dto.save;

import cc.ccplay.booster.base.dto.content.game.AbstractGame;
import cc.ccplay.booster.base.model.content.SaveRecord;

public class SaveRecordDto extends AbstractGame implements java.io.Serializable{

    private SaveRecord saveRecord;

    public SaveRecord getSaveRecord() {
        return saveRecord;
    }

    public void setSaveRecord(SaveRecord saveRecord) {
        this.saveRecord = saveRecord;
    }
}
