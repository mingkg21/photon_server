package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="game_user_follow")
public class GameUserFollow implements Serializable{

	@CompositeId
	private Id id;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;



	public Id getId(){
		return id;
	}

	public void setId(Id id){
		this.id = id;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}


	public static class Id{

		public Id(){}

		public Id(Long gameId,Long userId){
			this.gameId = gameId;
			this.userId = userId;
		}

		@Column(name = "game_id")
		private Long gameId;

		@Column(name = "user_id")
		private Long userId;


		public Long getGameId(){
			return this.gameId;
		}

		public void setGameId(Long gameId){
			this.gameId = gameId;
		}

		public Long getUserId(){
			return this.userId;
		}

		public void setUserId(Long userId){
			this.userId = userId;
		}

	}
}

