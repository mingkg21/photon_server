package cc.ccplay.booster.base.dto.content.game;

import cc.ccplay.booster.base.model.content.GameRecruitFeedback;
import cc.ccplay.booster.base.reader.UserInfoReader;
import cc.ccplay.booster.base.reader.dto.UserInfo;

import java.io.Serializable;

public class GameRecruitFeedbackDto implements UserInfoReader,Serializable{

    private GameRecruitFeedback feedback;

    private UserInfo userInfo;

    public GameRecruitFeedback getFeedback() {
        return feedback;
    }

    public void setFeedback(GameRecruitFeedback feedback) {
        this.feedback = feedback;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    @Override
    public Long getUserId() {
        if(feedback == null){
            return null;
        }
        return feedback.getUserId();
    }

    @Override
    public void callback(UserInfo userInfo) {
        this.userInfo = userInfo;
    }
}
