package cc.ccplay.booster.base.api.log;

import cc.ccplay.booster.base.dto.log.BoosterSuccessLoginDto;
import cc.ccplay.booster.base.model.log.BoosterLoginLog;

public interface BoosterLoginLogService {

    public void save(BoosterLoginLog loginLog);

    public BoosterSuccessLoginDto getSuccessCount(long serverId,int size);
}
