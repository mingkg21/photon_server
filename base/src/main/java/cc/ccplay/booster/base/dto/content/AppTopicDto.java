package cc.ccplay.booster.base.dto.content;

import cc.ccplay.booster.base.model.content.AppTopic;

import java.util.List;

public class AppTopicDto extends AppTopic implements java.io.Serializable {

    private List<AppTopicItemDto> items;

    public List<AppTopicItemDto> getItems() {
        return items;
    }

    public void setItems(List<AppTopicItemDto> items) {
        this.items = items;
    }
}
