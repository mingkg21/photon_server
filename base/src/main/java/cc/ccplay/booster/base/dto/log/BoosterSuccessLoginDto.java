package cc.ccplay.booster.base.dto.log;

import java.io.Serializable;

public class BoosterSuccessLoginDto implements Serializable {

    private Long serverId;

    private Long totalCount;

    private Long successCount;


    public Long getServerId() {
        return serverId;
    }

    public void setServerId(Long serverId) {
        this.serverId = serverId;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }

    public Long getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(Long successCount) {
        this.successCount = successCount;
    }
}
