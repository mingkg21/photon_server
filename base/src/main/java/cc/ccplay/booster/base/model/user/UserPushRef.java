package cc.ccplay.booster.base.model.user;

import org.easyj.frame.jdbc.annotation.*;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;

import java.util.Date;

@Table(name = "user_push_ref")
public class UserPushRef {

    @Id
    @GeneratedValue(type = GenerationType.AUTO)
    @Column(name="id")
    private Integer id;

    @Column(name="push_device_id")
    private String pushDeviceId;

    @Column(name="user_id")
    private Long userId;

    @Column(name="device_no")
    private String deviceNo;

    @Column(name="user_agent")
    private String userAgent;

    @Column(name="ip")
    private String ip;

    @NowDateValue(insert = true)
    @Column(name="create_datetime")
    private Date createDatetime;

	@NowDateValue(update = true, insert = true)
	@Column(name = "update_datetime")
	private Date updateDatetime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPushDeviceId() {
        return pushDeviceId;
    }

    public void setPushDeviceId(String pushDeviceId) {
        this.pushDeviceId = pushDeviceId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getDeviceNo() {
        return deviceNo;
    }

    public void setDeviceNo(String deviceNo) {
        this.deviceNo = deviceNo;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Date getCreateDatetime() {
        return createDatetime;
    }

    public void setCreateDatetime(Date createDatetime) {
        this.createDatetime = createDatetime;
    }

    public Date getUpdateDatetime() {
        return updateDatetime;
    }

    public void setUpdateDatetime(Date updateDatetime) {
        this.updateDatetime = updateDatetime;
    }
}