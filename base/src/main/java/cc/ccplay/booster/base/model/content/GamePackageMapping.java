package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="game_package_mapping")
public class GamePackageMapping implements Serializable{

	//精确匹配
	public final static long TYPE_ACCURATE = 1L;

	//前缀匹配
	public final static long TYPE_PREFIX = 2L;


	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;


	@Column(name = "package_name")
	private String packageName;

	@Column(name = "type")
	private Long type;

	@Column(name = "game_id")
	private Long gameId;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@NowDateValue(insert = true,update = true)
	@Column(name = "update_time")
	private Date updateTime;

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public String getPackageName(){
		return this.packageName;
	}

	public void setPackageName(String packageName){
		this.packageName = packageName;
	}

	public Long getType(){
		return this.type;
	}

	public void setType(Long type){
		this.type = type;
	}

	public Long getGameId(){
		return this.gameId;
	}

	public void setGameId(Long gameId){
		this.gameId = gameId;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}


}

