package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.content.SuggestionFeedback;
import org.easyj.frame.jdbc.Page;

import java.util.List;

public interface SuggestionFeedbackService {

    public void saveOrUpdate(SuggestionFeedback feedback);

    public Page getPage(SystemParam param,Long userId);

    public void reply(SuggestionFeedback feedback);

    public List<SuggestionFeedback> getHistory(long userId);
}
