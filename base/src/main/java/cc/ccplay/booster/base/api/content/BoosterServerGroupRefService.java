package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.booster.BoosterServerGroupRefDto;
import cc.ccplay.booster.base.model.content.BoosterServerGroupRef;
import org.easyj.frame.jdbc.Page;

public interface BoosterServerGroupRefService {

    public void saveOrUpdate(BoosterServerGroupRef ref);

    public void delete(long refId);

    public Page getPage(SystemParam param, long groupId);

    public BoosterServerGroupRefDto getDto(long id);
}
