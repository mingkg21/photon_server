package cc.ccplay.booster.base.enums;

public class MsgTagType {

    /**
     * 普通的文本
     */
    public final static int TEXT = 1;


    /**
     * 外链
     */
    public final static int URL_LINKED = 2;


    /**
     * 游戏详情
     */
    public final static int GAME_INFO = 3;

    /**
     * 用户详情
     */
    public final static int USER_INFO = 4;


    /**
     * 游戏评论详情
     */
    @Deprecated
    public final static int GAME_COMMENT_INFO = 5;


    /**
     * 游戏评论回复详情
     */
    public final static int GAME_REPLY_INFO = 6;

    /**
     * 存档评论
     */
    @Deprecated
    public final static int SAVE_COMMENT_INFO = 7;


    /**
     * 存档评论回复详情
     */
    public final static int SAVE_REPLY_INFO = 8;

}
