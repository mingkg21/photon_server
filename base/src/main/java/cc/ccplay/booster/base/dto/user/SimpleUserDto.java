package cc.ccplay.booster.base.dto.user;

import cc.ccplay.booster.base.reader.dto.UserInfo;
import cc.ccplay.booster.base.model.user.UserAccount;
import cc.ccplay.booster.base.model.adapter.CdnImage;
import cc.ccplay.booster.base.model.user.UserProfile;

import java.io.Serializable;

public class SimpleUserDto implements Serializable{

    public SimpleUserDto(){}

    public SimpleUserDto(UserInfo userInfo){
        if(userInfo == null){
            return;
        }
        UserAccount userAccount = userInfo.getUserAccount();
        this.id = userAccount.getId();
        this.nickName = userAccount.getNickName();
        this.username = userAccount.getUsername();
        UserProfile userProfile = userInfo.getUserProfile();
        this.headIcon = userInfo.getHeadIcon();
    }

    private Long id;

    private CdnImage headIcon;

    private String nickName;

    private String username;

    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CdnImage getHeadIcon() {
        return headIcon;
    }

    public void setHeadIcon(CdnImage headIcon) {
        this.headIcon = headIcon;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
