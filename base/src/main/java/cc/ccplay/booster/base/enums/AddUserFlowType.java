package cc.ccplay.booster.base.enums;

public enum AddUserFlowType {

    //使用流量
    USED("使用消耗",false,1),

    //后台增加总流量
    BACKEND_ADD("后台添加",true,2),

    //充值增加总流量
    RECHARGE("充值",true,3),

    REGISTER("注册赠送",true,4)
    ;

    private AddUserFlowType(String name,boolean add,long value){
        this.value = value;
        this.add = add;
        this.name = name;
    }

    private String name;

    private boolean add;

    private long value;


    public String getName() {
        return name;
    }

    public long getValue() {
        return value;
    }

    public boolean isAdd() {
        return add;
    }

}
