package cc.ccplay.booster.base.model.content;

import cc.ccplay.booster.base.dto.content.game.GameInfoDto;

import java.io.Serializable;
import java.util.List;

public class TagRecommendGame implements Serializable{

    private GameTag gameTag;
    private List<GameInfoDto> games;

    public GameTag getGameTag() {
        return gameTag;
    }

    public void setGameTag(GameTag gameTag) {
        this.gameTag = gameTag;
    }

    public List<GameInfoDto> getGames() {
        return games;
    }

    public void setGames(List<GameInfoDto> games) {
        this.games = games;
    }
}
