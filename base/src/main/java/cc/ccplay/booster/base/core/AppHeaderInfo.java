package cc.ccplay.booster.base.core;

import java.io.Serializable;
import java.util.HashMap;

public class AppHeaderInfo implements Serializable {
	private HashMap<String, String> keyMap;

	public AppHeaderInfo() {
		this.keyMap = new HashMap<>();
	}

	public String getHeader(Header key) {
		return keyMap.get(key.getKey());
	}

	public void addHeader(Header key, String value) {
		keyMap.put(key.getKey(), value);
	}

	public String getUserToken() {
		return keyMap.get(Header.USER_TOKEN.getKey());
	}

	public String getApiKey() {
		return keyMap.get(Header.API_KEY.getKey());
	}

	public String getApiSign() {
		return keyMap.get(Header.API_SIGN.getKey());
	}

	public String getAppVersion() {
		return keyMap.get(Header.APP_VERSION.getKey());
	}

	public String getMacCode() {
		return keyMap.get(Header.MAC_CODE.getKey());
	}

	public String getDeviceNo() {
		return keyMap.get(Header.DEVICE_NO.getKey());
	}

	public String getClientChannelName() {
		return keyMap.get(Header.CLIENT_CHANNEL_NAME.getKey());
	}

	public String getClientPackageName() {
		return keyMap.get(Header.CLIENT_PACKAGE_NAME.getKey());
	}

	public String getClientVersionName() {
		return keyMap.get(Header.CLIENT_VERSION_NAME.getKey());
	}

	public String getClientVersionCode() {
		return keyMap.get(Header.CLIENT_VERSION_CODE.getKey());
	}

	public String getPushDeviceId() {
		return keyMap.get(Header.PUSH_DEVICE_ID.getKey());
	}

	public String getOsVersion() {
		return keyMap.get(Header.OS_VERSION.getKey());
	}

	public String getUserAgent() {
		return keyMap.get(Header.USER_AGENT.getKey());
	}

	public String getModelName(){
		return keyMap.get(Header.MODEL_NAME.getKey());
	}

	public String getDeviceImei(){
		return keyMap.get(Header.IMEI.getKey());
	}

	public String getDeviceImsi(){
		return keyMap.get(Header.IMSI.getKey());
	}

	public HashMap<String, String> getKeyMap() {
		return keyMap;
	}
}
