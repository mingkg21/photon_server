package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="booster_server_online")
public class BoosterServerOnline implements Serializable{

	@Id
	@Column(name = "server_id")
	private Long serverId;

	/**
	 在线总人数
	 */
	@Column(name = "person_count")
	private Long personCount;

	/**
	 * 是否正常运行
	 */
	@Column(name = "working")
	private Long working;


	@Column(name = "contected_count")
	private Long contectedCount;


	@Column(name = "load1")
	private Double load1;

	@Column(name = "load5")
	private Double load5;

	@Column(name = "load15")
	private Double load15;


	@NowDateValue(update = true,insert = true)
	@Column(name = "update_time")
	private Date updateTime;


	public Long getWorking() {
		return working;
	}

	public void setWorking(Long working) {
		this.working = working;
	}

	public Long getContectedCount() {
		return contectedCount;
	}

	public void setContectedCount(Long contectedCount) {
		this.contectedCount = contectedCount;
	}

	public Double getLoad1() {
		return load1;
	}

	public void setLoad1(Double load1) {
		this.load1 = load1;
	}

	public Double getLoad5() {
		return load5;
	}

	public void setLoad5(Double load5) {
		this.load5 = load5;
	}

	public Double getLoad15() {
		return load15;
	}

	public void setLoad15(Double load15) {
		this.load15 = load15;
	}

	public Long getServerId(){
		return this.serverId;
	}

	public void setServerId(Long serverId){
		this.serverId = serverId;
	}

	public Long getPersonCount(){
		return this.personCount;
	}

	public void setPersonCount(Long personCount){
		this.personCount = personCount;
	}

	public Date getUpdateTime(){
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime){
		this.updateTime = updateTime;
	}


}

