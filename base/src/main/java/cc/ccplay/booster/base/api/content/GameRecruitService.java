package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.dto.content.game.GameRecruitDto;
import cc.ccplay.booster.base.model.content.GameRecruit;
import cc.ccplay.booster.base.model.content.GameRecruitAnswerRecord;
import cc.ccplay.booster.base.core.SystemParam;
import org.easyj.frame.jdbc.Page;

import java.util.List;

public interface GameRecruitService {

    public Page getPage(SystemParam param);

    public void updateStatus(SystemParam param,long id,long status);

    public GameRecruit saveOrUpdate(SystemParam param, GameRecruit gameRecruit);

    public GameRecruitDto getGameRecruitDto(SystemParam param, long id);

    public Page<GameRecruitDto> getDtoPage(SystemParam param);

    public GameRecruitDto getGameRecruitDtoForApp(SystemParam param,long id);

    public void takeTask(SystemParam param,long id);

    public void saveAnswers(SystemParam param,long recruitId,List<GameRecruitAnswerRecord> answers);

}
