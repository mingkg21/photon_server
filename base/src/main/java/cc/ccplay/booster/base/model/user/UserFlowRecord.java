package cc.ccplay.booster.base.model.user;

import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="user_flow_record")
public class UserFlowRecord implements Serializable{

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "flow")
	private Long flow;

	@Column(name = "type")
	private Long type;

	@Column(name = "remark")
	private String remark;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@Column(name = "user_id")
	private Long userId;



	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getFlow(){
		return this.flow;
	}

	public void setFlow(Long flow){
		this.flow = flow;
	}

	public Long getType(){
		return this.type;
	}

	public void setType(Long type){
		this.type = type;
	}

	public String getRemark(){
		return this.remark;
	}

	public void setRemark(String remark){
		this.remark = remark;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Long getUserId(){
		return this.userId;
	}

	public void setUserId(Long userId){
		this.userId = userId;
	}


}

