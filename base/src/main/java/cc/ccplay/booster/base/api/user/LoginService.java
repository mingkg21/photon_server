package cc.ccplay.booster.base.api.user;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.user.LoginResultDto;
import cc.ccplay.booster.base.model.user.ThirdPlaformUser;

public interface LoginService {

	LoginResultDto loginByPhone(SystemParam sp, String phone, String password);

	LoginResultDto loginByCode(SystemParam sp, String phone, String validateCode);

	LoginResultDto afterLogin(SystemParam sp, Long userId,String phone);

	LoginResultDto loginByThirdPlaftformOpenId(SystemParam sp, String openId,long plaftorm);

	/**
	 * 创建用户同时登录
	 * @param sp
	 * @param wechatUser
	 * @return
	 */
	LoginResultDto loginByThirdPlaftform(SystemParam sp, ThirdPlaformUser wechatUser);


}
