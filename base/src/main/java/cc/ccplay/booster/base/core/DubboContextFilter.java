package cc.ccplay.booster.base.core;


import com.alibaba.dubbo.common.Constants;
import com.alibaba.dubbo.common.extension.Activate;
import com.alibaba.dubbo.rpc.*;
import org.apache.log4j.Logger;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.filter.SysThreadData;
import org.easyj.frame.filter.ThreadVariable;


@Activate(
        group = {Constants.PROVIDER},
        order = -99999999
)
public class DubboContextFilter implements Filter {

    private static Logger logger = Logger.getLogger(DubboContextFilter.class);

    /**
     * do invoke filter.
     * <p>
     * <code>
     * // before filter
     * Result result = invoker.invoke(invocation);
     * // after filter
     * return result;
     * </code>
     *
     * @param invoker    service
     * @param invocation invocation.
     * @return invoke result.
     * @throws RpcException
     * @see Invoker#invoke(Invocation)
     */
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        SysThreadData data = ThreadVariable.get();
        boolean exist = (data != null);

        if(!exist){
            data = new SysThreadData();
            ThreadVariable.set(data);
            Object [] params = invocation.getArguments();
            for (int i = 0; i < params.length; i++) {
                if(params[i] == null){
                    continue;
                }
                if(params[i].getClass() == SystemParam.class){
                    SystemParam systemParam = (SystemParam)params[i];
                    data.setSysData(systemParam);
                    if(systemParam != null) {
                        data.setSysUser(systemParam.getUser());
                    }
                    logger.debug("【成功初始化线程变量SystemParam】");
                    break;
                }
            }
        }else{
            logger.debug("【线程变量SystemParam已存在】");
        }

        try {
            Result result = invoker.invoke(invocation);
            Throwable exception = result.getException();
            if(exception instanceof GenericException){
                return result;
            }
            return result;
        } finally {
            if(!exist){
                ThreadVariable.clear();
                logger.debug("【释放线程变量SystemParam】");
            }
        }
    }
}
