package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.Column;
import org.easyj.frame.jdbc.annotation.CompositeId;
import org.easyj.frame.jdbc.annotation.Table;

import java.io.Serializable;

@Table(name="game_tag_item")
public class GameTagItem implements Serializable {

    @CompositeId
    private Id id;

    @Column(name="ordering")
    private long ordering;

    public Id getId() {
        return id;
    }

    public void setId(Id id) {
        this.id = id;
    }

    public long getOrdering() {
        return ordering;
    }

    public void setOrdering(long ordering) {
        this.ordering = ordering;
    }

    public void setId(long gameId, long tagId){
        this.id = new Id(gameId, tagId);
    }

    public static class Id implements Serializable{

        public Id(){ }

        public Id(long gameId, long tagId) {
            this.gameId = gameId;
            this.tagId = tagId;
        }

        @Column(name="game_id")
        private long gameId;

        @Column(name="tag_id")
        private long tagId;

        public Long getGameId() {
            return gameId;
        }

        public void setGameId(Long gameId) {
            this.gameId = gameId;
        }

        public Long getTagId() {
            return tagId;
        }

        public void setTagId(Long tagId) {
            this.tagId = tagId;
        }
    }
}
