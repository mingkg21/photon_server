package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.content.Complaint;

public interface ComplaintService {
    public void save(SystemParam param, Complaint complaint);
}
