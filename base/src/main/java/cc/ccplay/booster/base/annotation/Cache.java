package cc.ccplay.booster.base.annotation;

import cc.ccplay.booster.base.core.Header;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Cache {

    /**
     * merge = true
     *   合并所有请求，只能有一个请求去读取缓,其他线程都处于等待状态
     * merge = false
     * 	 有缓存读取缓存，没缓存直接读数据库
     * @return
     */
    boolean merge() default true;

    /**
     * merge = true时，配置生效(单位毫秒)
     * 最长等待时间
     * @return
     */
    long waitTime() default 4 * 1000;

    /**
     * merge = true时，配置生效(单位毫秒)
     * 间隔多长时间去读取一次缓存，若缓存有值直接返回
     * @return
     */
    long spaceTime() default 1000;


    /**
     *
     * 具体需要哪些参数生成
     * @return
     */
    String [] params() default {};


    /**
     * 系统参数
     *
     clientChannelName;
     clientPackageName;
     clientVersionName;
     clientVersionCode;
     apiSource;
     * @return
     */
    Header [] headerParams()default {};

    /**
     * 默认缓存失效时间(单位秒)
     * cacheTimeout <= 0 则不失效
     * @return
     */
    int cacheTimeout() default 60 * 30 ;
}
