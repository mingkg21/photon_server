package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.dto.SelectizeDto;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.GamePublisher;
import org.easyj.frame.jdbc.Page;

import java.util.List;

public interface GamePublisherService {

    public Page getPage(SystemParam param, String name);

    public GamePublisher saveOrUpdate(SystemParam param, GamePublisher publisher);

    public int updateStatus(SystemParam param,long id,Status status);

    public GamePublisher get(SystemParam param, long id);

    public List<SelectizeDto> getForSelectize(SystemParam param, String name);


}
