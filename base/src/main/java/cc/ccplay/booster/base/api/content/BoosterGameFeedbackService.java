package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.content.BoosterGameFeedback;
import org.easyj.frame.jdbc.Page;

public interface BoosterGameFeedbackService {

    public void saveOrUpdate(BoosterGameFeedback feedback);

    public Page getPage(SystemParam param, BoosterGameFeedback searchModel,boolean containPackageName);

    public void deleteByPackageName(String packageName);

    public void deleteByName(String name);

}
