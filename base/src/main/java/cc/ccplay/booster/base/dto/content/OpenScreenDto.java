package cc.ccplay.booster.base.dto.content;

import cc.ccplay.booster.base.model.content.GameVersion;
import cc.ccplay.booster.base.model.content.OpenScreen;
import cc.ccplay.booster.base.model.content.GameInfo;

public class OpenScreenDto implements java.io.Serializable {

    private OpenScreen openScreen;

    private GameInfo gameInfo;

    private GameVersion versionInfo;

    public OpenScreen getOpenScreen() {
        return openScreen;
    }

    public void setOpenScreen(OpenScreen openScreen) {
        this.openScreen = openScreen;
    }

    public GameInfo getGameInfo() {
        return gameInfo;
    }

    public void setGameInfo(GameInfo gameInfo) {
        this.gameInfo = gameInfo;
    }

    public GameVersion getVersionInfo() {
        return versionInfo;
    }

    public void setVersionInfo(GameVersion versionInfo) {
        this.versionInfo = versionInfo;
    }
}
