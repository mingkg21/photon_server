package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.dto.save.SaveGameDto;
import cc.ccplay.booster.base.model.content.SaveGame;
import cc.ccplay.booster.base.core.SystemParam;
import org.easyj.frame.jdbc.Page;

public interface SaveGameService {

    public Page getPage(SystemParam param,Long gameId,String packageName,String name);

    public void save(SaveGame game);

    public void update(long gameId,String config);

    public void delete(long id);

    public SaveGame get(long id);

    public SaveGameDto getDto(long id);

    public SaveGame getByPackageName(String packageName);
}
