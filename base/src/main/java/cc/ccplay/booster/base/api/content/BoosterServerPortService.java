package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.BoosterServerPort;
import org.easyj.frame.jdbc.Page;

public interface BoosterServerPortService {

    public BoosterServerPort get(long id);

    public void saveOrUpdate(BoosterServerPort port);

    public void delete(long id);

    public Page getPage(SystemParam param,long serverId);

    public void updateStatus(long id, Status status);

}