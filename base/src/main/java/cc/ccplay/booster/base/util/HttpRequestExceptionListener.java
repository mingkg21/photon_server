package cc.ccplay.booster.base.util;

import java.util.Map;

public interface HttpRequestExceptionListener {
	void exceptionData(Map<String, String> data);
}
