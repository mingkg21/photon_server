package cc.ccplay.booster.base.cache;

import cc.ccplay.booster.base.annotation.Cache;
import cc.ccplay.booster.base.constants.Constant;
import cc.ccplay.booster.base.core.Header;
import cc.ccplay.booster.base.test.annotation.ApiMethod;
import cc.ccplay.booster.base.util.AesUtil;
import cc.ccplay.booster.base.util.Md5Util;
import cc.ccplay.booster.base.util.RSAUtils;
import cc.ccplay.booster.base.util.UserUtil;
import com.alibaba.fastjson.JSONObject;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.reflect.MethodSignature;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.exception.SystemError;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
public class CacheAopAction {

    private final static Logger log = Logger.getLogger(CacheAopAction.class);

    @Resource(name = "cacheRedisTemplate")
    private RedisTemplate<String,String> redisTemplate;

//    @Pointcut("execution(* cc.ccplay.booster.*.web.*.*(..))")
//    private void controllerAspect(){
//
//    }

    public Object aroundMethod(ProceedingJoinPoint pjp) throws Throwable{
        // 拦截的实体类，就是当前正在执行的controller
        Object target = pjp.getTarget();
        // 拦截的方法名称。当前正在执行的方法
        String methodName = pjp.getSignature().getName();
        // 拦截的方法参数
        Object[] args = pjp.getArgs();
        // 拦截的放参数类型
        Signature sig = pjp.getSignature();
        MethodSignature msig =  (MethodSignature) sig;
        Class[] parameterTypes = msig.getMethod().getParameterTypes();

        Method method = null;
        try {
            method = target.getClass().getMethod(methodName, parameterTypes);
        } catch (NoSuchMethodException | SecurityException e1) {
            throw new SystemError(e1);
        }

        Cache cache = method.getAnnotation(Cache.class);
        ApiMethod apiMethod = method.getAnnotation(ApiMethod.class);
        boolean encrypt = apiMethod != null && apiMethod.encrypt() == true;
        if(cache == null){
            Object object = pjp.proceed();
            if(encrypt){
                encryptResponseContent();
            }
            return object;
        }

        String keyJson = createKeyObject(cache).toJSONString();
        HttpServletRequest request = UserUtil.getRequest();
        String url = request.getServletPath();

        String functionCode = null;
        if(url.startsWith("/")){
            functionCode = url.substring(1);
        }
        functionCode = functionCode.replaceAll("/",":");
        String cacheKey =  functionCode + ":" + Md5Util.md5(keyJson);
        if(getAndOutputCache(cacheKey)){ //读取到缓存并输出
            return null;
        }

        HttpServletResponse response = UserUtil.getResponse();
        if (!cache.merge()) { //同时请求数据库
            Object obj = pjp.proceed();
            updateCacheFromResponse(encrypt,cacheKey,keyJson,cache.cacheTimeout());
            return obj;
        }

        //并发合并请求
        String lockKey = cacheKey + ":" + "lock";


        if (setnx(lockKey, System.currentTimeMillis() + "")) {
            try {
                log.info(url + ":开始读取merge redis缓存");
                redisTemplate.expire(lockKey,2 * 60, TimeUnit.SECONDS);
                //请求数据
                Object obj = pjp.proceed();
                updateCacheFromResponse(encrypt,cacheKey,keyJson,cache.cacheTimeout());
                log.info(functionCode + ":成功设置merge redis缓存");
                return obj;
            } finally {
                //请求异常 需要删除锁值 以便后面线程能重新读取
                redisTemplate.delete(lockKey);
            }
        }

        //其他等待主线程获取数据
        long waitTime = cache.waitTime();
        long spaceTime = cache.spaceTime();
        while (waitTime > 0) {
            try {
                Thread.sleep(spaceTime);
            } catch (Exception e) {
            }
            if (getAndOutputCache(cacheKey)) {
                return null;
            }
            waitTime = waitTime - spaceTime;
        }
        throw new GenericException("数据请求超时");
    }

    private String encryptResponseContent(){
        HttpServletResponse response = this.getResponse();
        String content = this.getResponseData();
        try {
            String encryptContent = RSAUtils.publicEncrypt(content,RSAUtils.getPublicKey(Constant.RSA_PUBLIC_KEY));
            if(encryptContent != null) {
                response.resetBuffer();
                response.setHeader(Constant.HEADER_ENCRYPT_STATUS_KEY,"1");
                response.getWriter().write(encryptContent);
                return encryptContent;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return content;
    }


    private void updateCacheFromResponse(boolean encrypt ,String cacheKey,String params,int timeout){
        HttpServletResponse response = getResponse();
        Map<String, String> cacheMap = new HashMap<String, String>();
        String content = encrypt ? encryptResponseContent() : this.getResponseData();
        cacheMap.put("cache", content);
        cacheMap.put("params", params);
        String contentType = response.getContentType();
        if(contentType != null) {
            cacheMap.put("contentType", contentType);
        }
        hmset(cacheKey, cacheMap, timeout);
    }

    private HttpServletResponse getResponse(){
        HttpServletResponse response = UserUtil.getResponse();
        if(response == null){
            response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
        }
        return response;
    }


    private String getResponseData(){
        HttpServletResponse rsp = getResponse();
        if(rsp instanceof CacheResponseWrapper == false){
            log.error("由于response不是CacheResponseWrapper无法读取到response中的响应数据");
            return null;
        }
        CacheResponseWrapper response = (CacheResponseWrapper)rsp;
        String data = null;
        try {
            data = response.getResult();
        }catch (Exception e){
            log.error(e);
        }
        return data;
    }

    private boolean getAndOutputCache(String cacheKey){
        List<String> values = hmget(cacheKey, "cache","contentType");
        String cacheValue = values.get(0);
        if(cacheValue == null){
           return false;
        }

        HttpServletResponse rsp = UserUtil.getResponse();
        String contentType = values.get(1);
        if(contentType != null) {
            rsp.setContentType(contentType);
        }

        try {
            rsp.getWriter().print(cacheValue);
        }catch (IOException e){
            log.error(e);
        }
        return true;
    }


    private JSONObject createKeyObject(Cache cache) {
        String[] keys = new String[2];
        JSONObject json = new JSONObject();
        HttpServletRequest request = UserUtil.getRequest();
        String[] paramKeys = cache.params();
        for (int i = 0; i < paramKeys.length; i++) {
            String key = paramKeys[i];
            String value = request.getParameter(key);
            if(value == null){
                value = "null";
            }
            json.put(key, value);
        }

        Header[] headerKeys = cache.headerParams();
        for (int i = 0; i < headerKeys.length; i++) {
            Header h = headerKeys[i];
            String key = h.getKey();
            String value = request.getHeader(key);
            if(value == null){
                value = "null";
            }
            json.put("header."+key, value);
        }
        return json;
    }

    public List<String> hmget(final String key,final String... propertys){
        final RedisSerializer<String> serializer = redisTemplate.getStringSerializer();
        final byte[][] proKeys = new byte[propertys.length][];
        for(int i = 0 ;i< propertys.length; i++){
            proKeys[i] = serializer.serialize(propertys[i]);
        }

        List<String> values = redisTemplate.execute(new RedisCallback<List<String>>() {
            @Override
            public List<String> doInRedis(RedisConnection connection) throws DataAccessException {

                byte[] bkey = serializer.serialize(key);

                byte[] propertyKey = null;
                List<byte[]> values = connection.hMGet(bkey,proKeys);
                if(values == null ){
                    return null;
                }
                List<String>result = new ArrayList<>();
                int size = values.size();
                for(int i = 0 ;i< size; i++){
                    result.add(serializer.deserialize(values.get(i)));
                }
                return result;
            }
       });
       return values;
    }

    public void hmset(final String key,Map<String,String> data,final int second){
        final RedisSerializer<String> serializer = redisTemplate.getStringSerializer();
        final Map<byte[],byte[]> bdata = new HashMap<>();
        for(String k : data.keySet()){
            bdata.put(serializer.serialize(k),serializer.serialize(data.get(k)));
        }
        redisTemplate.execute(new RedisCallback() {
            @Override
            public Object doInRedis(RedisConnection connection) throws DataAccessException {
                byte[] bkey = serializer.serialize(key);
                connection.hMSet(bkey,bdata);
                if(second > 0) {
                    connection.expire(bkey, second);
                }
                return null;
            }
        });
    }

    public Boolean setnx(final String lockKey,final String value){
        return redisTemplate.execute(new RedisCallback<Boolean>() {
            @Override
            public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
                RedisSerializer<String> serializer = redisTemplate.getStringSerializer();
                return connection.setNX(serializer.serialize(lockKey), serializer.serialize(value));
            }
        });
    }
}
