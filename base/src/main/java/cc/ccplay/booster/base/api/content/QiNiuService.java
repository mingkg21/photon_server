package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.QiNiuAuth;
import cc.ccplay.booster.base.model.content.BaseFile;

public interface QiNiuService {

    /**
     * 获取上传图片 *
     * @param param
     * @return
     */
    public QiNiuAuth getUploadImageToken(SystemParam param);


    public QiNiuAuth getUploadImageToken(SystemParam param,String fileName);

    /**
     * 获取上传视频token
     * @param param
     * @return
     */
    public QiNiuAuth getUploadVideoToken(SystemParam param);


    public QiNiuAuth getUploadVideoToken(SystemParam param,String fileName);

    /**
     * 获取上传应用包 token
     * @param param
     * @return
     */
    public QiNiuAuth getUploadPkgToken(SystemParam param);


    /**
     * 获取七牛cdn 地址
     * @param param
     * @return
     */
    public String getCdnUrl(SystemParam param);


    /**
     * 获取上传普通文件
     * @param param
     * @return
     */
    public QiNiuAuth getUploadFileToken(SystemParam param);

    public QiNiuAuth getUploadFileToken(SystemParam param,String fileName);

    public void saveToolUploadFile(BaseFile baseFile);

}
