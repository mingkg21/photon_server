package cc.ccplay.booster.base.api.user;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.user.SysRole;
import org.easyj.frame.jdbc.Page;

import java.util.List;

public interface SysRoleService {

    public SysRole getRole(SystemParam systemParam, long roleId);

    public List<SysRole> getAll(SystemParam systemParam);

    public int logicDelete(SystemParam systemParam,long id);

    public SysRole saveOrUpdate(SystemParam systemParam,SysRole role);

    public Page getPage(SystemParam systemParam);

    public void savePermission(SystemParam systemParam,String [] menuIds,long roleId);
}
