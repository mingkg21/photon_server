package cc.ccplay.booster.base.dto.content;

import cc.ccplay.booster.base.model.content.GameHotWord;

import java.io.Serializable;
import java.util.List;

public class HotWordsDto implements Serializable{

    private List<GameHotWord> words;

    private GameHotWord defaultWord;


    public List<GameHotWord> getWords() {
        return words;
    }

    public void setWords(List<GameHotWord> words) {
        this.words = words;
    }

    public GameHotWord getDefaultWord() {
        return defaultWord;
    }

    public void setDefaultWord(GameHotWord defaultWord) {
        this.defaultWord = defaultWord;
    }
}
