package cc.ccplay.booster.base.api.log;

import cc.ccplay.booster.base.core.SystemParam;
import org.easyj.frame.jdbc.Page;

import java.util.Date;

public interface AppDayCountService {

    public Page getPage(SystemParam param, Date startDate, Date endDate, Long appId);

    public Page getTotalPage(SystemParam param,Date startDate,Date endDate);

    //汇总当天数据
    public void countToday();

    //汇总前一天数据
    public void countPreDay();


}
