package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.dto.content.game.TagTagItemDto;
import cc.ccplay.booster.base.model.content.TagTagItem;
import cc.ccplay.booster.base.core.SystemParam;
import org.easyj.frame.jdbc.Page;

import java.util.List;

public interface TagTagItemService  {

    public void saveOrUpdate(TagTagItem item);

    public void delete(long mainTagId,long tagId);

    public Page getTagPage(SystemParam param,long mainTagId, String name);

    public List getChildTagList(long mainTagId,int topSize);

    public TagTagItemDto getDto(long mainTagId, long tagId);

}
