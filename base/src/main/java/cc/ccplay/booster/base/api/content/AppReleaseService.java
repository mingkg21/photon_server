package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.content.AppRelease;
import cc.ccplay.booster.base.enums.Status;
import org.easyj.frame.jdbc.Page;

public interface AppReleaseService {

    public void saveOrUpdate(SystemParam param, AppRelease release);

    public void updateStatus(SystemParam param,long id,Status status);

    public Page getPage(SystemParam param);

    public AppRelease get(long id);

    public AppRelease getLastestVersion();
}
