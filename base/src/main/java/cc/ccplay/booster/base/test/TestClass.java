package cc.ccplay.booster.base.test;

import java.util.Map;

public class TestClass {

    private String name;

    private Class clazz;

    private String requestMapping;

    private Map<String,TestMethod> methods;

    public String getRequestMapping() {
        return requestMapping;
    }

    public void setRequestMapping(String requestMapping) {
        this.requestMapping = requestMapping;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Class getClazz() {
        return clazz;
    }

    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }

    public Map<String,TestMethod> getMethods() {
        return methods;
    }

    public void setMethods(Map<String,TestMethod> methods) {
        this.methods = methods;
    }
}
