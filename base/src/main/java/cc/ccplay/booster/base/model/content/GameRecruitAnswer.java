package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.*;
import java.io.Serializable;

@Table(name="game_recruit_answer")
public class GameRecruitAnswer implements Serializable{

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "question_id")
	private Long questionId;

	@Column(name = "answer")
	private String answer;

	public Long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}

	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public String getAnswer(){
		return this.answer;
	}

	public void setAnswer(String answer){
		this.answer = answer;
	}


}

