package cc.ccplay.booster.base.model.adapter;

import org.easyj.frame.jdbc.adapter.AbstractAdapter;
import org.easyj.frame.util.StringUtil;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CdnImageAdapter extends AbstractAdapter {

    @Override
    protected Object getColumnValue(Field field, ResultSet rs, String colName) throws SQLException, IllegalAccessException {
        Icon rule = field.getAnnotation(Icon.class);
        String ruleValue = rule != null ? rule.value() : null;

        String src = rs.getString(colName);
        if(StringUtil.isEmpty(src)){
            return null;
        }

        CdnImage image = new CdnImage();

        if(src.startsWith("http://") || src.startsWith("https://")){
            image.setSrc(src);
            image.setCndSrc(src);
        }else{
            image.setIcon(ruleValue);
            image.setSrc(src);
            String cnd = CdnRoute.getInstance().getCndUrl();
            if(ruleValue != null){
                image.setCndSrc(cnd + src + "-"+ruleValue);
            }else{
                image.setCndSrc(cnd + src);
            }
        }
        return image;
    }

    public Object getObjectValue(Field field, Method getMethod, Object obj) throws IllegalAccessException,InvocationTargetException {
        CdnImage image = (CdnImage) super.getObjectValue(field,getMethod,obj);
        if(image == null){
            return null;
        }
        return image.getSrc();
    }

    @Override
    public Object formatValue(Object val){
        CdnImage img = (CdnImage)val;
        if(img == null){
            return null;
        }
        return img.getSrc();
    }
}
