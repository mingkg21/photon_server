package cc.ccplay.booster.base.model.content;

import cc.ccplay.booster.base.model.adapter.CdnImageList;
import com.alibaba.fastjson.JSONArray;
import org.easyj.frame.jdbc.annotation.value.LongValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import com.alibaba.fastjson.annotation.JSONField;
import java.util.Date;
import java.io.Serializable;

@Table(name="game_evaluation")
public class GameEvaluation implements Serializable{

	/**
	 0：用户评测
	 */
	public final static long TYPE_USER = 0;
	/**
	 *  1：官方评测
	 */
	public final static long TYPE_OFFICIAL = 1;

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;


	@Column(name = "game_id")
	private Long gameId;

	@Column(name = "user_id")
	private Long userId;

	@Column(name = "images")
	private CdnImageList images;

	@Column(name = "content")
	private String content;

	@Column(name = "title")
	private String title;

	@LongValue(insertValue = 0,insert = true)
	@Column(name = "praise_count")
	private Long praiseCount;

	@LongValue(insertValue = 0,insert = true)
	@Column(name = "follow_count")
	private Long followCount;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@JSONField(serialize = false)
	@LogicDelete("1")
	@Where(clause = "delete_flag=0")
	@LongValue(insert = true,insertValue = 0)
	@Column(name = "delete_flag")
	private Long deleteFlag;

	/**
	 0：用户评测  1：官方评测
	 */
	@Column(name = "type")
	private Long type;

	@Column(name = "score")
	private Long score;

	/**
	 音画
	 */
	@Column(name = "music_score")
	private Long musicScore;

	/**
	 玩法
	 */
	@Column(name = "play_score")
	private Long playScore;

	/**
	 操作
	 */
	@Column(name = "operation_score")
	private Long operationScore;

	/**
	 剧情
	 */
	@Column(name = "story_score")
	private Long storyScore;

	/**
	 耐玩
	 */
	@Column(name = "lasting_score")
	private Long lastingScore;



	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getGameId(){
		return this.gameId;
	}

	public void setGameId(Long gameId){
		this.gameId = gameId;
	}

	public Long getUserId(){
		return this.userId;
	}

	public void setUserId(Long userId){
		this.userId = userId;
	}

	public CdnImageList getImages(){
		return this.images;
	}

	public void setImages(JSONArray images){
		this.images = new CdnImageList(images);
	}

	public void setImages(CdnImageList images){
		this.images = images;
	}

	public String getContent(){
		return this.content;
	}

	public void setContent(String content){
		this.content = content;
	}

	public String getTitle(){
		return this.title;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public Long getPraiseCount(){
		return this.praiseCount;
	}

	public void setPraiseCount(Long praiseCount){
		this.praiseCount = praiseCount;
	}

	public Long getFollowCount(){
		return this.followCount;
	}

	public void setFollowCount(Long followCount){
		this.followCount = followCount;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Long getDeleteFlag(){
		return this.deleteFlag;
	}

	public void setDeleteFlag(Long deleteFlag){
		this.deleteFlag = deleteFlag;
	}

	public Long getType(){
		return this.type;
	}

	public void setType(Long type){
		this.type = type;
	}

	public Long getScore(){
		return this.score;
	}

	public void setScore(Long score){
		this.score = score;
	}

	public Long getMusicScore(){
		return this.musicScore;
	}

	public void setMusicScore(Long musicScore){
		this.musicScore = musicScore;
	}

	public Long getPlayScore(){
		return this.playScore;
	}

	public void setPlayScore(Long playScore){
		this.playScore = playScore;
	}

	public Long getOperationScore(){
		return this.operationScore;
	}

	public void setOperationScore(Long operationScore){
		this.operationScore = operationScore;
	}

	public Long getStoryScore(){
		return this.storyScore;
	}

	public void setStoryScore(Long storyScore){
		this.storyScore = storyScore;
	}

	public Long getLastingScore(){
		return this.lastingScore;
	}

	public void setLastingScore(Long lastingScore){
		this.lastingScore = lastingScore;
	}


}

