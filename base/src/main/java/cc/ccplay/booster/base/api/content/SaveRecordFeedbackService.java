package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.model.content.SaveRecordFeedback;

public interface SaveRecordFeedbackService {

    public void save(SaveRecordFeedback feedback);

}
