package cc.ccplay.booster.base.model.adapter;

import java.lang.annotation.*;

@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Icon {
    String value();
}
