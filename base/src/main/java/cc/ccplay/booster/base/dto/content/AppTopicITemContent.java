package cc.ccplay.booster.base.dto.content;

import java.io.Serializable;

public class AppTopicITemContent implements Serializable {

    private String color;

    private Long fontSize;

    private Long bold;

    private String link;//点击外链

    private String text;//文本

    private String videoUrl;//视频地址

    private String imgUrl;//图片地址

    private Long height; //图片高

    private Long width;//图片宽

    public Long getHeight() {
        return height;
    }

    public void setHeight(Long height) {
        this.height = height;
    }

    public Long getWidth() {
        return width;
    }

    public void setWidth(Long width) {
        this.width = width;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Long getFontSize() {
        return fontSize;
    }

    public void setFontSize(Long fontSize) {
        this.fontSize = fontSize;
    }

    public Long getBold() {
        return bold;
    }

    public void setBold(Long bold) {
        this.bold = bold;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String  link) {
        this.link = link;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
