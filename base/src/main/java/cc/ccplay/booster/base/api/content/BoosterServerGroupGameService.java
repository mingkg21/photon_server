package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.booster.BoosterServerGroupGameDto;
import cc.ccplay.booster.base.model.content.BoosterServerGroupGame;
import org.easyj.frame.jdbc.Page;

public interface BoosterServerGroupGameService {

    public void saveOrUpdate(BoosterServerGroupGame route);

    public void delete(long id);

    public Page getPage(SystemParam param, Long gameId, Long groupId,String gameName);

    public BoosterServerGroupGame get(long id);

    public BoosterServerGroupGameDto getDto(long id);

}
