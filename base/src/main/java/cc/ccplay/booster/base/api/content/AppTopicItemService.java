package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.AppTopicItemDto;
import cc.ccplay.booster.base.model.content.AppTopicItem;
import org.easyj.frame.jdbc.Page;

public interface AppTopicItemService {

    public void saveOrUpdate(AppTopicItem item);

    public Page getPage(SystemParam param, long topicId);

    public void delete(long itemId);

    public AppTopicItem get(long itemId);

    public AppTopicItemDto getDto(long itemId);

}
