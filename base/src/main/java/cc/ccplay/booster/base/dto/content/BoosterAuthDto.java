package cc.ccplay.booster.base.dto.content;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BoosterAuthDto implements Serializable{


    //不需要经过加速的域名
    private static final List<String> DOMAINS = new ArrayList<>();
    static {
        DOMAINS.add("39.108.104.78");
        DOMAINS.add("qiniu.guangzijiasu.com");
        DOMAINS.add("api.guangzijiasu.com");
    }

    public BoosterAuthDto(){
        this.domains = DOMAINS;
    }


    private Long port;
    /**
     * 连接IP
     */
    private String ip;

    private String password;

    private String method;

    private Long serverId;
    /**
     * SS服务器IP
     */
    private String realIp;

    private List<String> domains;

    private String testDomain;


    public String getTestDomain() {
        return testDomain;
    }

    public void setTestDomain(String testDomain) {
        this.testDomain = testDomain;
    }

    public String getRealIp() {
        return realIp;
    }

    public void setRealIp(String realIp) {
        this.realIp = realIp;
    }

    public List<String> getDomains() {
        return domains;
    }

    public void setDomains(List<String> domains) {
        this.domains = domains;
    }

    public Long getServerId() {
        return serverId;
    }

    public void setServerId(Long serverId) {
        this.serverId = serverId;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Long getPort() {
        return port;
    }

    public void setPort(Long port) {
        this.port = port;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
