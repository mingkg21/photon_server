package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="user_game_list")
public class UserGameList implements Serializable{

	@CompositeId
	private Id id;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;



	public Id getId(){
		return id;
	}

	public void setId(Id id){
		this.id = id;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}


	public static class Id implements Serializable {

		public Id(){}

		public Id(Long userId,Long gameId){
			this.userId = userId;
			this.gameId = gameId;
		}

		@Column(name = "user_id")
		private Long userId;

		@Column(name = "game_id")
		private Long gameId;


		public Long getUserId(){
			return this.userId;
		}

		public void setUserId(Long userId){
			this.userId = userId;
		}

		public Long getGameId(){
			return this.gameId;
		}

		public void setGameId(Long gameId){
			this.gameId = gameId;
		}

	}
}

