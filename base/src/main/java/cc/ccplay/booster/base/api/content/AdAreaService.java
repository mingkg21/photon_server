package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.content.AdArea;
import cc.ccplay.booster.base.enums.Status;
import org.easyj.frame.jdbc.Page;

public interface AdAreaService {

    public void saveOrUpdate(AdArea adArea);

    public void updateStatus(long id, Status status);

    public Page getPage(SystemParam param);

    public AdArea get(long id);

}
