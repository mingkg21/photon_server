package cc.ccplay.booster.base.api.log;

import cc.ccplay.booster.base.dto.ChartReportDto;
import cc.ccplay.booster.base.model.log.BoosterServerStatus;

import java.util.Date;
import java.util.List;

public interface BoosterServerStatusService {

    /**
     * 记录服务器状态
     */
    public void batchSave(List<BoosterServerStatus> list);

    /**
     * 获取一天在线人数报表 （一小时一条）
     * @param date
     * @return
     */
    public ChartReportDto[] getHourReport(Date date);

    /**
     * 获取多天的在线人数报表  (一天一条)
     * @param startDate
     * @param endDate
     * @return
     */
    public List<ChartReportDto> getDayReport(Date startDate,Date endDate);

}
