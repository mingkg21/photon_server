package cc.ccplay.booster.base.test;

import com.alibaba.fastjson.annotation.JSONField;

public class TestMethodParameter {

    private boolean notNull;

    private String remark;

    private String paramterName;

    private Class type;

    private Long maxValue;

    private Long minValue;

    private Integer minLength;

    private Integer maxLength;

    @JSONField(serialize = false)
    private TestClass parentClass;

    @JSONField(serialize = false)
    private TestMethod parentMethod;

    public TestClass getParentClass() {
        return parentClass;
    }

    public void setParentClass(TestClass parentClass) {
        this.parentClass = parentClass;
    }

    public TestMethod getParentMethod() {
        return parentMethod;
    }

    public void setParentMethod(TestMethod parentMethod) {
        this.parentMethod = parentMethod;
    }

    public Class getType() {
        return type;
    }

    public void setType(Class type) {
        this.type = type;
    }

    public Long getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(Long maxValue) {
        this.maxValue = maxValue;
    }

    public Long getMinValue() {
        return minValue;
    }

    public void setMinValue(Long minValue) {
        this.minValue = minValue;
    }

    public Integer getMinLength() {
        return minLength;
    }

    public void setMinLength(Integer minLength) {
        this.minLength = minLength;
    }

    public Integer getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(Integer maxLength) {
        this.maxLength = maxLength;
    }

    public String getParamterName() {
        return paramterName;
    }

    public void setParamterName(String paramterName) {
        this.paramterName = paramterName;
    }

    public boolean isNotNull() {
        return notNull;
    }

    public void setNotNull(boolean notNull) {
        this.notNull = notNull;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

}
