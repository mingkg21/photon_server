package cc.ccplay.booster.base.core;

import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.adapter.*;
import org.easyj.frame.jdbc.adapter.AdapterRegister;

public class BaseAdapter {
    public static void initAdapter(){
        AdapterRegister.reg(Status.class,new Status.StatusAdapter());
        AdapterRegister.reg(CdnImage.class,new CdnImageAdapter());
        AdapterRegister.reg(CdnSource.class,new CdnSourceAdapter());
        AdapterRegister.reg(CdnImageList.class,new CdnImageListAdapter());
    }
}
