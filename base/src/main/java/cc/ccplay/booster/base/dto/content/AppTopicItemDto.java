package cc.ccplay.booster.base.dto.content;

import cc.ccplay.booster.base.dto.content.game.AbstractGame;
import cc.ccplay.booster.base.model.content.AppTopicItem;

public class AppTopicItemDto extends AbstractGame implements java.io.Serializable{

    private AppTopicItem appTopicItem;

    public AppTopicItem getAppTopicItem() {
        return appTopicItem;
    }

    public void setAppTopicItem(AppTopicItem appTopicItem) {
        this.appTopicItem = appTopicItem;
    }
}
