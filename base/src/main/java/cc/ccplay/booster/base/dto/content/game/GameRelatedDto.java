package cc.ccplay.booster.base.dto.content.game;

import cc.ccplay.booster.base.model.content.GameVersion;
import cc.ccplay.booster.base.model.content.GameRelated;
import cc.ccplay.booster.base.model.content.GameInfo;

import java.io.Serializable;

public class GameRelatedDto implements Serializable{

    private GameRelated gameRelated;

    private GameInfo gameInfo;

    private GameVersion versionInfo;

    public GameRelated getGameRelated() {
        return gameRelated;
    }

    public void setGameRelated(GameRelated gameRelated) {
        this.gameRelated = gameRelated;
    }

    public GameInfo getGameInfo() {
        return gameInfo;
    }

    public void setGameInfo(GameInfo gameInfo) {
        this.gameInfo = gameInfo;
    }

    public GameVersion getVersionInfo() {
        return versionInfo;
    }

    public void setVersionInfo(GameVersion versionInfo) {
        this.versionInfo = versionInfo;
    }
}
