package cc.ccplay.booster.base.reader;

import cc.ccplay.booster.base.reader.dto.UserInfo;

public interface UserInfoReader {

    public Long getUserId();

    public void callback(UserInfo userInfo);

}
