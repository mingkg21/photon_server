package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="save_record_praise")
public class SaveRecordPraise implements Serializable{

	@CompositeId
	private Id id;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;



	public Id getId(){
		return id;
	}

	public void setId(Id id){
		this.id = id;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}


	public static class Id{

		public Id(){}

		public Id(Long userId,Long recordId){
			this.userId = userId;
			this.recordId = recordId;
		}

		@Column(name = "user_id")
		private Long userId;

		@Column(name = "record_id")
		private Long recordId;


		public Long getUserId(){
			return this.userId;
		}

		public void setUserId(Long userId){
			this.userId = userId;
		}

		public Long getRecordId(){
			return this.recordId;
		}

		public void setRecordId(Long recordId){
			this.recordId = recordId;
		}

	}
}

