package cc.ccplay.booster.base.model.user;

import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="user_flow")
public class UserFlow implements Serializable{

	@Id
	@Column(name = "user_id")
	private Long userId;


	@Column(name = "total_flow")
	private Long totalFlow;

	@Column(name = "used_download_flow")
	private Long usedDownloadFlow;

	@Column(name = "used_upload_flow")
	private Long usedUploadFlow;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@NowDateValue(update = true,insert = true)
	@Column(name = "update_time")
	private Date updateTime;



	public Long getUserId(){
		return this.userId;
	}

	public void setUserId(Long userId){
		this.userId = userId;
	}

	public Long getTotalFlow(){
		return this.totalFlow;
	}

	public void setTotalFlow(Long totalFlow){
		this.totalFlow = totalFlow;
	}

	public Long getUsedDownloadFlow(){
		return this.usedDownloadFlow;
	}

	public void setUsedDownloadFlow(Long usedDownloadFlow){
		this.usedDownloadFlow = usedDownloadFlow;
	}

	public Long getUsedUploadFlow(){
		return this.usedUploadFlow;
	}

	public void setUsedUploadFlow(Long usedUploadFlow){
		this.usedUploadFlow = usedUploadFlow;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Date getUpdateTime(){
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime){
		this.updateTime = updateTime;
	}


}

