package cc.ccplay.booster.base.model.content;

import cc.ccplay.booster.base.constants.Constant;
import org.easyj.frame.jdbc.annotation.*;
import org.easyj.frame.jdbc.annotation.value.LongValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;

import java.io.Serializable;
import java.util.Date;

@Table(name="app_channel_manage")
public class AppChannelManage implements Serializable {

    @Id
    @GeneratedValue(type= GenerationType.AUTO)
    @Column(name="id")
    private Long id;

    /**
     *  渠道标识
     */
    @Column(name="code")
    private String code;

    /**
     *  版本号
     */
    @Column(name = "version_code")
    private Long versionCode;

    @LongValue(insert = true,insertValue = Constant.STATUS_DISABLED)
    @Column(name = "status")
    private Long status;

    @NowDateValue(insert = true)
    @Column(name = "create_time")
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(Long versionCode) {
        this.versionCode = versionCode;
    }

    public Long getStatus(){
        return this.status;
    }

    public void setStatus(Long status){
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
