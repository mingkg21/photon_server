package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.game.SpecialCategoryTagItemDto;
import cc.ccplay.booster.base.model.content.SpecialCategoryTagItem;
import org.easyj.frame.jdbc.Page;

public interface SpecialCategoryTagItemService {

    public SpecialCategoryTagItemDto getDto(long categroyId, long tagId);

    public void saveOrUpdate(SpecialCategoryTagItem item);

    public void delete(long categroyId, long tagId);

    public Page getTagPage(SystemParam param, long categroyId, String tagName);
}
