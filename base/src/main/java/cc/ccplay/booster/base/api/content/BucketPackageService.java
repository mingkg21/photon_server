package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.model.content.BucketPackage;
import cc.ccplay.booster.base.core.SystemParam;
import org.easyj.frame.jdbc.Page;

public interface BucketPackageService {

    public BucketPackage save(SystemParam param, BucketPackage bucketPackage);

    public Page queryPage(SystemParam param, BucketPackage searchObj);

    public void update(BucketPackage bucketPackage);

    public BucketPackage get(long id);

    public void delete(long id);
}
