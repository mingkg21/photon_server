package cc.ccplay.booster.base.enums;

public enum SourceType {
    APP(1),
    WWW(2),
    WAP(3),
    BACKEND(4);

    private int value;

    public int getValue() {
        return value;
    }

    private SourceType(int value){
        this.value = value;
    }
}
