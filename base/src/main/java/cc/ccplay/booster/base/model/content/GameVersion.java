package cc.ccplay.booster.base.model.content;

import cc.ccplay.booster.base.constants.Constant;
import cc.ccplay.booster.base.model.adapter.CdnImage;
import cc.ccplay.booster.base.model.adapter.Icon;
import com.alibaba.fastjson.annotation.JSONField;
import org.easyj.frame.jdbc.annotation.*;
import org.easyj.frame.jdbc.annotation.value.LongValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.util.StringUtil;

import java.io.Serializable;
import java.util.Date;

@Table(name="game_version")
public class GameVersion implements Serializable {

    /**
     * 测试版  不含应用包
     */
    public static final long VERSION_TYPE_TEST = 1;

    /**
     * 正式版  含应用包
     */
    public static final long VERSION_TYPE_RELEASE = 2;

    @Id
    @GeneratedValue(type = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name="version_name")
    private String versionName;

    @Column(name="version_code")
    private Long versionCode;

    /**
     * 不暴露真正的下载地址
     */
    @JSONField(serialize = false)
    @Column(name="download_url")
    private String downloadUrl;

    @Column(name="file_size")
    private Long fileSize;

    @Column(name="download_count")
    @LongValue(insertValue = 0,insert = true)
    private Long downloadCount;

    @Column(name="game_id")
    private Long gameId;

    @Column(name="lang")
    private String lang;

    @Column(name="create_time")
    @NowDateValue(insert = true)
    private Date createTime;

    @Column(name="update_time")
    @NowDateValue(insert = true,update = true)
    private Date updateTime;

    @Column(name="version_type")
    private Long versionType;

    @Icon("160")
    @Column(name="icon")
    private CdnImage icon;

    @Column(name="adapter_info")
    private String adapterInfo;

    @Column(name="update_log")
    private String updateLog;

    @Column(name="package_name")
    private String packageName;

    @Column(name="release_time")
    @NowDateValue(insert = true)
    private Date releaseTime;

    @Column(name="version_tags")
    private String versionTags;


    @Column(name="status")
    @LongValue(insert = true,insertValue = Constant.STATUS_ENABLED)
    private Long status;


    /**
     * apk包签名
     */
    @Column(name="sign")
    private String sign;

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getVersionTags() {
        return versionTags;
    }

    public void setVersionTags(String versionTags) {
        this.versionTags = versionTags;
    }

    public Date getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(Date releaseTime) {
        this.releaseTime = releaseTime;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getAdapterInfo() {
        return adapterInfo;
    }

    public void setAdapterInfo(String adapterInfo) {
        this.adapterInfo = adapterInfo;
    }

    public String getUpdateLog() {
        return updateLog;
    }

    public void setUpdateLog(String updateLog) {
        this.updateLog = updateLog;
    }

    public CdnImage getIcon() {
        return icon;
    }

    public void setIcon(CdnImage icon) {
        this.icon = icon;
    }

    public void setIcon(String icon){
        if(StringUtil.isNotEmpty(icon)){
            this.icon = new CdnImage(icon);
        }else{
            this.icon = null;
        }
    }

    public Long getVersionType() {
        return versionType;
    }

    public void setVersionType(Long versionType) {
        this.versionType = versionType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public Long getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(Long versionCode) {
        this.versionCode = versionCode;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public Long getDownloadCount() {
        return downloadCount;
    }

    public void setDownloadCount(Long downloadCount) {
        this.downloadCount = downloadCount;
    }

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
