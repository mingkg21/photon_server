package cc.ccplay.booster.base.model.log;

import org.easyj.frame.jdbc.annotation.*;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;

import java.util.Date;

@Table(name = "user_login_log")
public class UserLoginLog {

    @Id
    @GeneratedValue(type = GenerationType.AUTO)
    @Column(name="id")
    private Integer id;

    @Column(name="client_channel_name")
    private String clientChannelName;

    @Column(name="client_package_name")
    private String clientPackageName;

    @Column(name="client_version_name")
    private String clientVersionName;

    @Column(name="client_version_code")
    private Long clientVersionCode;

    @Column(name="user_id")
    private Long userId;

    @Column(name="package_id")
    private Integer packageId;

    @Column(name="model_name")
    private String modelName;

    @Column(name="ip")
    private String ip;

    @Column(name="user_agent")
    private String userAgent;

    @Column(name="device_no")
    private String deviceNo;

    @Column(name="mac_address")
    private String macAddress;

    @Column(name="imei_mac")
    private String imeiMac;

    @NowDateValue(insert = true)
    @Column(name="create_datetime")
    private Date createDatetime;

    @NowDateValue(insert = true)
    @Column(name="update_datetime")
    private Date updateDatetime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClientChannelName() {
        return clientChannelName;
    }

    public void setClientChannelName(String clientChannelName) {
        this.clientChannelName = clientChannelName;
    }

    public String getClientPackageName() {
        return clientPackageName;
    }

    public void setClientPackageName(String clientPackageName) {
        this.clientPackageName = clientPackageName;
    }

    public String getClientVersionName() {
        return clientVersionName;
    }

    public void setClientVersionName(String clientVersionName) {
        this.clientVersionName = clientVersionName;
    }

    public Long getClientVersionCode() {
        return clientVersionCode;
    }

    public void setClientVersionCode(Long clientVersionCode) {
        this.clientVersionCode = clientVersionCode;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getDeviceNo() {
        return deviceNo;
    }

    public void setDeviceNo(String deviceNo) {
        this.deviceNo = deviceNo;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getImeiMac() {
        return imeiMac;
    }

    public void setImeiMac(String imeiMac) {
        this.imeiMac = imeiMac;
    }

    public Date getCreateDatetime() {
        return createDatetime;
    }

    public void setCreateDatetime(Date createDatetime) {
        this.createDatetime = createDatetime;
    }

    public Date getUpdateDatetime() {
        return updateDatetime;
    }

    public void setUpdateDatetime(Date updateDatetime) {
        this.updateDatetime = updateDatetime;
    }
}