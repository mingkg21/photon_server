package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;

import java.util.Date;
import java.io.Serializable;

@Table(name="save_game")
public class SaveGame implements Serializable{

	@Id
	@Column(name = "game_id")
	private Long gameId;

	@NowDateValue(update = true,insert = true)
	@Column(name = "update_time")
	private Date updateTime;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@Column(name = "config")
	private String config;

	public Long getGameId(){
		return this.gameId;
	}

	public void setGameId(Long gameId){
		this.gameId = gameId;
	}

	public Date getUpdateTime(){
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime){
		this.updateTime = updateTime;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public String getConfig() {
		return config;
	}

	public void setConfig(String config) {
		this.config = config;
	}
}

