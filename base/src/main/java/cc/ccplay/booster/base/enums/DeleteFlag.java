package cc.ccplay.booster.base.enums;

public class DeleteFlag {
    //已删除
    public static final long DELETE = 1;
    //未删除
    public static final long NO_DELETE = 0;
}
