package cc.ccplay.booster.base.exception;

import org.easyj.frame.exception.GenericException;
import org.easyj.frame.util.JsonUtil;

import java.util.HashMap;
import java.util.Map;

public class NotSamePackageNameException extends GenericException{

    private String currentPackageName;

    private String newPackageName;

    public NotSamePackageNameException(String currentPackageName,String newPackageName){
        this.currentPackageName = currentPackageName;
        this.newPackageName = newPackageName;
    }

    @Override
    public String toJsonMsg() {
        Map result = new HashMap();
        result.put("msg", getErrorMessage());
        result.put("code","1");
        result.put("currentPackageName",this.currentPackageName);
        result.put("newPackageName",this.newPackageName);
        result.put("success", false);
        return JsonUtil.toJson(result);
    }


    @Override
    public String getErrorMessage() {
        return "包名不一致";
    }

    public String getCurrentPackageName() {
        return currentPackageName;
    }

    public void setCurrentPackageName(String currentPackageName) {
        this.currentPackageName = currentPackageName;
    }

    public String getNewPackageName() {
        return newPackageName;
    }

    public void setNewPackageName(String newPackageName) {
        this.newPackageName = newPackageName;
    }
}
