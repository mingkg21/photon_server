package cc.ccplay.booster.base.dto.content.gift;

import cc.ccplay.booster.base.reader.UserInfoReader;
import cc.ccplay.booster.base.reader.dto.UserInfo;
import cc.ccplay.booster.base.model.adapter.CdnImage;
import cc.ccplay.booster.base.model.user.UserAccount;

import java.io.Serializable;
import java.util.Date;

public class GiftBespeakRecordPageDto implements UserInfoReader,Serializable{

    private Long userId;

    private Long id;

    private String giftName;

    private String nickName;

    private CdnImage headIcon;

    private Date createTime;

    private String gameName;

    private String gameId;

    private Long type;

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public CdnImage getHeadIcon() {
        return headIcon;
    }

    public void setHeadIcon(CdnImage headIcon) {
        this.headIcon = headIcon;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    @Override
    public Long getUserId() {
        return this.userId;
    }

    @Override
    public void callback(UserInfo userInfo) {
        this.nickName = userInfo.getNickName();
        this.headIcon = userInfo.getHeadIcon();
        UserAccount account = userInfo.getUserAccount();
        if(account != null){
            this.userId = account.getId();
        }
    }
}
