package cc.ccplay.booster.base.api.log;

import cc.ccplay.booster.base.model.log.AppDownloadCompleteRecord;

public interface AppDownloadCompleteLogService {

    public void save(AppDownloadCompleteRecord record);
}
