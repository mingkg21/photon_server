package cc.ccplay.booster.base.model.content;

import com.alibaba.fastjson.annotation.JSONField;
import org.easyj.frame.jdbc.annotation.value.LongValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="custom_area")
public class CustomArea implements Serializable{

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "code")
	private String code;

	@LongValue(insert = true,insertValue = 2)
	@JSONField(serialize = false)
	@Column(name = "status")
	private Long status;

	@LongValue(insert = true,insertValue = 0)
	@Column(name = "app_count")
	private Long appCount;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@NowDateValue(update = true,insert = true)
	@Column(name = "update_time")
	private Date updateTime;

	@Column(name = "remark")
	private String remark;

	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public String getName(){
		return this.name;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getCode(){
		return this.code;
	}

	public void setCode(String code){
		this.code = code;
	}

	public Long getStatus(){
		return this.status;
	}

	public void setStatus(Long status){
		this.status = status;
	}

	public Long getAppCount(){
		return this.appCount;
	}

	public void setAppCount(Long appCount){
		this.appCount = appCount;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Date getUpdateTime(){
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime){
		this.updateTime = updateTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}

