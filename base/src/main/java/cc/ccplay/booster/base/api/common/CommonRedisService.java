package cc.ccplay.booster.base.api.common;

/**
 * redis 操作
 */
public interface CommonRedisService {

	void set(String key, String value);

	void set(String key, String value, Long second);

	String get(String key);

	void push(String key, String value);

	String pop(String key);

	void sendMsg(String channel, Object msg);

}
