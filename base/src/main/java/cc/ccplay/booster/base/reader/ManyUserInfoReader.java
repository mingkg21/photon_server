package cc.ccplay.booster.base.reader;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

public interface ManyUserInfoReader {
    @JSONField(serialize = false)
    public List<UserInfoReader> getReaderList();
}
