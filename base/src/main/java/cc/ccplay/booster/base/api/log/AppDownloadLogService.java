package cc.ccplay.booster.base.api.log;

import cc.ccplay.booster.base.model.log.AppDownloadRecord;

public interface AppDownloadLogService {

    public void saveDownloadLog(AppDownloadRecord appDownloadRecord);

}
