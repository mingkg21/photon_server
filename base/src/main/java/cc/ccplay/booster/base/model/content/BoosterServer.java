package cc.ccplay.booster.base.model.content;

import cc.ccplay.booster.base.constants.Constant;
import org.easyj.frame.jdbc.annotation.value.LongValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;

import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;

@Table(name="booster_server")
public class BoosterServer implements Serializable{

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;


	@Column(name = "ip")
	private String ip;

	@Column(name = "name")
	private String name;

	@Column(name = "status")
	@LongValue(insert = true,insertValue = Constant.STATUS_ENABLED)
	private Long status;

	@Column(name = "remark")
	private String remark;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@Column(name = "method")
	private String method;

	@Column(name = "max_connections")
	private Long maxConnections;

	@Column(name = "start_port")
	private Long startPort;

	@Column(name = "end_port")
	private Long endPort;

	@Column(name = "real_ip")
	private String realIp;

	/**
	 * 默认服务器
	 */
	@Column(name = "def_server")
	private Long defServer;


	@Column(name = "telnet_status")
	private Long telnetStatus;

	@Column(name = "success_percent")
	private Double successPercent;

	@Column(name = "fail_warning")
	private Long failWarning;

	public Double getSuccessPercent() {
		return successPercent;
	}

	public void setSuccessPercent(Double successPercent) {
		this.successPercent = successPercent;
	}

	public Long getFailWarning() {
		return failWarning;
	}

	public void setFailWarning(Long failWarning) {
		this.failWarning = failWarning;
	}

	public Long getTelnetStatus() {
		return telnetStatus;
	}

	public void setTelnetStatus(Long telnetStatus) {
		this.telnetStatus = telnetStatus;
	}

	public Long getDefServer() {
		return defServer;
	}

	public void setDefServer(Long defServer) {
		this.defServer = defServer;
	}

	public Long getStartPort() {
		return startPort;
	}

	public void setStartPort(Long startPort) {
		this.startPort = startPort;
	}

	public Long getEndPort() {
		return endPort;
	}

	public void setEndPort(Long endPort) {
		this.endPort = endPort;
	}

	public String getRealIp() {
		return realIp;
	}

	public void setRealIp(String realIp) {
		this.realIp = realIp;
	}

	public Long getMaxConnections() {
		return maxConnections;
	}

	public void setMaxConnections(Long maxConnections) {
		this.maxConnections = maxConnections;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public String getIp(){
		return this.ip;
	}

	public void setIp(String ip){
		this.ip = ip;
	}

	public String getName(){
		return this.name;
	}

	public void setName(String name){
		this.name = name;
	}

	public Long getStatus(){
		return this.status;
	}

	public void setStatus(Long status){
		this.status = status;
	}

	public String getRemark(){
		return this.remark;
	}

	public void setRemark(String remark){
		this.remark = remark;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}


}

