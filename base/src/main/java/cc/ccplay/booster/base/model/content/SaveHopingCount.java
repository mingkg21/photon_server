package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="save_hoping_count")
public class SaveHopingCount implements Serializable{

	@Id
	@Column(name = "game_id")
	private Long gameId;

	/**
	 许愿人数
	 */
	@Column(name = "person_count")
	private Long personCount;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@NowDateValue(insert = true,update = true)
	@Column(name = "last_hopping_time")
	private Date lastHoppingTime;



	public Long getGameId(){
		return this.gameId;
	}

	public void setGameId(Long gameId){
		this.gameId = gameId;
	}

	public Long getPersonCount(){
		return this.personCount;
	}

	public void setPersonCount(Long personCount){
		this.personCount = personCount;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Date getLastHoppingTime(){
		return this.lastHoppingTime;
	}

	public void setLastHoppingTime(Date lastHoppingTime){
		this.lastHoppingTime = lastHoppingTime;
	}


}

