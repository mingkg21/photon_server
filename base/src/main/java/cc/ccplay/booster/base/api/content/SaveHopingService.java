package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import org.easyj.frame.jdbc.Page;

public interface SaveHopingService {

    public Page getPage(SystemParam param);

    public long hoping(long gameId,long userId);

}
