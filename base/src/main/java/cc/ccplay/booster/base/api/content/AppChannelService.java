package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.SelectizeDto;
import cc.ccplay.booster.base.model.content.AppChannel;
import org.easyj.frame.jdbc.Page;

import java.util.List;

public interface AppChannelService {

    public AppChannel getChannel(SystemParam parameter, long id);

    public AppChannel getChannel(String channelCode);

    public Page getList(SystemParam parameter);

    public AppChannel saveOrUpdate(SystemParam parameter, AppChannel channel);

    public List<SelectizeDto> getForSelectize(SystemParam param, String key);

}
