package cc.ccplay.booster.base.dto.content.booster;

import java.io.Serializable;

public class ServerPortStatusDto implements Serializable {

    private Long port;

    private Long listen;

    private Long established;


    public Long getPort() {
        return port;
    }

    public void setPort(Long port) {
        this.port = port;
    }

    public Long getListen() {
        return listen;
    }

    public void setListen(Long listen) {
        this.listen = listen;
    }

    public Long getEstablished() {
        return established;
    }

    public void setEstablished(Long established) {
        this.established = established;
    }

}
