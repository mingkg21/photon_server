package cc.ccplay.booster.base.model.content;

import cc.ccplay.booster.base.constants.Constant;
import cc.ccplay.booster.base.model.adapter.CdnSource;
import org.easyj.frame.jdbc.annotation.value.LongValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;

import java.util.Date;
import java.io.Serializable;

@Table(name="app_release")
public class AppRelease implements Serializable{

	//强制更新
	public static final long FORCE_UPDATE_YES = 1;
	//非强制更新
	public static final long FORCE_UPDATE_NO = 0;

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "package_name")
	private String packageName;

	@Column(name = "version_name")
	private String versionName;

	@Column(name = "version_code")
	private Long versionCode;

	@Column(name = "update_log")
	private String updateLog;

	@LongValue(insert = true,insertValue = Constant.STATUS_DISABLED)
	@Column(name = "status")
	private Long status;

	@Column(name = "release_time")
	private Date releaseTime;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@NowDateValue(update = true,insert = true)
	@Column(name = "update_time")
	private Date updateTime;

	@Column(name = "download_url")
	private CdnSource downloadUrl;

	@Column(name = "file_size")
	private Long fileSize;

	@Column(name = "force_update")
	private Long forceUpdate;

	public Long getForceUpdate() {
		return forceUpdate;
	}

	public void setForceUpdate(Long forceUpdate) {
		this.forceUpdate = forceUpdate;
	}

	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	public void setDownloadUrl(String downloadUrl) {
		if(downloadUrl == null){
			this.downloadUrl = null;
		}else{
			this.downloadUrl = new CdnSource(downloadUrl);
		}
	}

	public CdnSource getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(CdnSource downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public String getPackageName(){
		return this.packageName;
	}

	public void setPackageName(String packageName){
		this.packageName = packageName;
	}

	public String getVersionName(){
		return this.versionName;
	}

	public void setVersionName(String versionName){
		this.versionName = versionName;
	}

	public Long getVersionCode(){
		return this.versionCode;
	}

	public void setVersionCode(Long versionCode){
		this.versionCode = versionCode;
	}

	public String getUpdateLog(){
		return this.updateLog;
	}

	public void setUpdateLog(String updateLog){
		this.updateLog = updateLog;
	}

	public Long getStatus(){
		return this.status;
	}

	public void setStatus(Long status){
		this.status = status;
	}

	public Date getReleaseTime(){
		return this.releaseTime;
	}

	public void setReleaseTime(Date releaseTime){
		this.releaseTime = releaseTime;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Date getUpdateTime(){
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime){
		this.updateTime = updateTime;
	}


}

