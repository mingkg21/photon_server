package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="special_category_tag_item")
public class SpecialCategoryTagItem implements Serializable{

	@CompositeId
	private Id id;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@Column(name = "ordering")
	private Long ordering;



	public Id getId(){
		return id;
	}

	public void setId(Id id){
		this.id = id;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Long getOrdering(){
		return this.ordering;
	}

	public void setOrdering(Long ordering){
		this.ordering = ordering;
	}


	public static class Id implements Serializable{

		public Id(){}

		public Id(Long categoryId,Long tagId){
			this.categoryId = categoryId;
			this.tagId = tagId;
		}

		@Column(name = "category_id")
		private Long categoryId;

		@Column(name = "tag_id")
		private Long tagId;


		public Long getCategoryId(){
			return this.categoryId;
		}

		public void setCategoryId(Long categoryId){
			this.categoryId = categoryId;
		}

		public Long getTagId(){
			return this.tagId;
		}

		public void setTagId(Long tagId){
			this.tagId = tagId;
		}

	}
}

