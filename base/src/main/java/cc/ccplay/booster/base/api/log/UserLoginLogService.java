package cc.ccplay.booster.base.api.log;

import cc.ccplay.booster.base.core.SystemParam;

public interface UserLoginLogService {

	void insertLoginLog(SystemParam sp, Long userId, Integer packageId);

}
