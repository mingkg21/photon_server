package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.model.content.QqGroup;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.enums.Status;
import org.easyj.frame.jdbc.Page;

import java.util.List;

public interface QqGroupService {

    public Page getPage(SystemParam param);

    public void saveOrUpdate(QqGroup group);

    public void updateStatus(long id , Status status);

    public QqGroup get(long id);

    public List getAll();

}
