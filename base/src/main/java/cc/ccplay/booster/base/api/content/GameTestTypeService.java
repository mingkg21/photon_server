package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.GameTestType;
import org.easyj.frame.jdbc.Page;

import java.util.List;

public interface GameTestTypeService {

    public GameTestType saveOrUpdate(SystemParam param,GameTestType testType);

    public Page getPage(SystemParam param,String name);

    public int updateStatus(SystemParam param,long id,Status status);

    public GameTestType get(SystemParam param, long id);

    public List<GameTestType> getAll4Selector(SystemParam param);

}
