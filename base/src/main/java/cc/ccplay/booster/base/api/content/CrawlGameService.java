package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.model.content.CrawlGame;

public interface CrawlGameService {

    public void batchUpdateCralGame();

    public void updateCrawlInfo(CrawlGame game);

}
