package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.value.LongValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import com.alibaba.fastjson.annotation.JSONField;
import java.util.Date;
import java.io.Serializable;

@Table(name="game_recruit_feedback")
public class GameRecruitFeedback implements Serializable{

	//已审核
	public final static long STATUS_CHECKED = 1;
	//未审核
	public final static long STATUS_UNCHECKED = 0;


	//打赏类型 创造力
	public final static long PRIZE_TYPE_CREATIVITY = 0;
	//打赏类型 洞察力
	public final static long PRIZE_TYPE_INSIGHT = 1;

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;


	@Column(name = "content")
	private String content;

	@Column(name = "title")
	private String title;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@Column(name = "user_id")
	private Long userId;

	@Column(name = "recruit_id")
	private Long recruitId;

	/**
	 	0:未审核
		1:审核通过
	 */
	@Column(name = "status")
	private Long status;

	@JSONField(serialize = false)
	@LogicDelete("1")
	@Where(clause = "delete_flag=0")
	@LongValue(insert = true,insertValue = 0)
	@Column(name = "delete_flag")
	private Long deleteFlag;

	/**
	 * 奖品类型：0：创造力  1：洞察力
	 */
	@Column(name = "prize_type")
	private Long prizeType;

	/**
	 * 奖励分值
	 */
	@Column(name = "prize_score")
	private Long prizeScore;

	public Long getPrizeType() {
		return prizeType;
	}

	public void setPrizeType(Long prizeType) {
		this.prizeType = prizeType;
	}

	public Long getPrizeScore() {
		return prizeScore;
	}

	public void setPrizeScore(Long prizeScore) {
		this.prizeScore = prizeScore;
	}

	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public String getContent(){
		return this.content;
	}

	public void setContent(String content){
		this.content = content;
	}

	public String getTitle(){
		return this.title;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Long getUserId(){
		return this.userId;
	}

	public void setUserId(Long userId){
		this.userId = userId;
	}

	public Long getRecruitId(){
		return this.recruitId;
	}

	public void setRecruitId(Long recruitId){
		this.recruitId = recruitId;
	}

	public Long getStatus(){
		return this.status;
	}

	public void setStatus(Long status){
		this.status = status;
	}

	public Long getDeleteFlag(){
		return this.deleteFlag;
	}

	public void setDeleteFlag(Long deleteFlag){
		this.deleteFlag = deleteFlag;
	}


}

