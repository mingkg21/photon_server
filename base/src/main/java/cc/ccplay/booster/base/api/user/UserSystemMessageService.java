package cc.ccplay.booster.base.api.user;

import cc.ccplay.booster.base.model.user.UserSystemMessage;
import cc.ccplay.booster.base.core.SystemParam;
import org.easyj.frame.jdbc.Page;


public interface UserSystemMessageService {


    /**
     * 通知
     * @param message
     */
    public void pushNotification2One(UserSystemMessage message);

    /**
     * 信息
     * @param message
     */
    public void pushMsg2One(UserSystemMessage message);

    public void pushNotification2All(UserSystemMessage message);


    public void pushMsg2All(UserSystemMessage message);


    public Page<UserSystemMessage> getPageByUserId(SystemParam systemParam,long userId);

}
