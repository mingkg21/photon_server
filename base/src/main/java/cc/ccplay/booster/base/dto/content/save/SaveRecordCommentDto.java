package cc.ccplay.booster.base.dto.content.save;

import cc.ccplay.booster.base.dto.user.SimpleUserDto;
import cc.ccplay.booster.base.model.content.SaveRecordComment;
import cc.ccplay.booster.base.reader.ManyUserInfoReader;
import cc.ccplay.booster.base.reader.UserInfoReader;
import cc.ccplay.booster.base.reader.dto.UserInfo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SaveRecordCommentDto implements ManyUserInfoReader,Serializable {

    private SaveRecordComment comment;

    private SimpleUserDto commentUser;

    private SimpleUserDto beRepliedUser;

    public SaveRecordComment getComment() {
        return comment;
    }

    public void setComment(SaveRecordComment comment) {
        this.comment = comment;
    }

    public SimpleUserDto getCommentUser() {
        return commentUser;
    }

    public void setCommentUser(SimpleUserDto commentUser) {
        this.commentUser = commentUser;
    }

    public SimpleUserDto getBeRepliedUser() {
        return beRepliedUser;
    }

    public void setBeRepliedUser(SimpleUserDto beRepliedUser) {
        this.beRepliedUser = beRepliedUser;
    }

    @Override
    public List<UserInfoReader> getReaderList() {
        if(this.comment == null){
            return null;
        }
        final SaveRecordCommentDto dto = this;
        final Long userId = this.comment.getUserId();
        final Long beRepliedUserId = this.comment.getBeRepliedUserId();
        List<UserInfoReader> list = new ArrayList<>();
        if(userId != null){
            list.add(new UserInfoReader() {
                @Override
                public Long getUserId() {
                    return userId;
                }

                @Override
                public void callback(UserInfo userInfo) {
                    dto.setCommentUser(new SimpleUserDto(userInfo));
                }
            });
        }
        if(beRepliedUserId != null){
            list.add(new UserInfoReader() {
                @Override
                public Long getUserId() {
                    return beRepliedUserId;
                }

                @Override
                public void callback(UserInfo userInfo) {
                    dto.setBeRepliedUser(new SimpleUserDto(userInfo));
                }
            });
        }
        return list;
    }
}
