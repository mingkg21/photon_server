package cc.ccplay.booster.base.model.content;

import cc.ccplay.booster.base.constants.Constant;
import org.easyj.frame.jdbc.annotation.value.LongValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="app_topic_item")
public class AppTopicItem implements Serializable{

	//文本
	public final static long OBJECT_TYPE_TEXT = 1;
	//应用
	public final static long OBJECT_TYPE_APP = 2;
	//图片
	public final static long OBJECT_TYPE_IMAGE = 3;
	//视频
	public final static long OBJECT_TYPE_VIDEO = 4;

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "topic_id")
	private Long topicId;

	@Column(name = "content")
	private String content;

	@Column(name = "status")
	@LongValue(insert = true,insertValue = Constant.STATUS_ENABLED)
	private Long status;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@Column(name = "ordering")
	private Long ordering;

	@Column(name = "object_type")
	private Long objectType;

	@Column(name = "object_id")
	private Long objectId;


	public Long getObjectType() {
		return objectType;
	}

	public void setObjectType(Long objectType) {
		this.objectType = objectType;
	}

	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public Long getOrdering() {
		return ordering;
	}

	public void setOrdering(Long ordering) {
		this.ordering = ordering;
	}

	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getTopicId(){
		return this.topicId;
	}

	public void setTopicId(Long topicId){
		this.topicId = topicId;
	}

	public String getContent(){
		return this.content;
	}

	public void setContent(String content){
		this.content = content;
	}

	public Long getStatus(){
		return this.status;
	}

	public void setStatus(Long status){
		this.status = status;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}


}

