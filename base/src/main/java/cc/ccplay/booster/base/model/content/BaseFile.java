package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.*;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;

import java.util.Date;

@ParentModel
public class BaseFile implements java.io.Serializable {
    @Id
    @GeneratedValue(type = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;


    @Column(name = "file_name")
    private String fileName;

    @Column(name = "src")
    private String src;

    @Column(name = "size")
    private Long size;

    @NowDateValue(insert = true)
    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "account")
    private String account;

    @Column(name = "hash")
    private String hash;

    @Column(name = "create_user_id")
    private Long createUserId;



    public Long getId(){
        return this.id;
    }

    public void setId(Long id){
        this.id = id;
    }

    public String getFileName(){
        return this.fileName;
    }

    public void setFileName(String fileName){
        this.fileName = fileName;
    }

    public String getSrc(){
        return this.src;
    }

    public void setSrc(String src){
        this.src = src;
    }

    public Long getSize(){
        return this.size;
    }

    public void setSize(Long size){
        this.size = size;
    }

    public Date getCreateTime(){
        return this.createTime;
    }

    public void setCreateTime(Date createTime){
        this.createTime = createTime;
    }

    public String getAccount(){
        return this.account;
    }

    public void setAccount(String account){
        this.account = account;
    }

    public String getHash(){
        return this.hash;
    }

    public void setHash(String hash){
        this.hash = hash;
    }

    public Long getCreateUserId(){
        return this.createUserId;
    }

    public void setCreateUserId(Long createUserId){
        this.createUserId = createUserId;
    }
}
