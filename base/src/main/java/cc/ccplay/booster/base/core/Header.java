package cc.ccplay.booster.base.core;

public enum Header {
	/**
	 * 用户TOKEN
	 */
	USER_TOKEN("user_token"),

	/**
	 * apiKey
	 */
	API_KEY("api_key"),

	/**
	 * api密钥
	 */
	API_SIGN("api_sign"),

	/**
	 * api版本
	 */
	APP_VERSION("app_version"),

	/**
	 * mac地址
	 */
	MAC_CODE("mac_code"),

	/**
	 * 设备号
	 */
	DEVICE_NO("device_no"),

	/**
	 * 客户端渠道号
	 */
	CLIENT_CHANNEL_NAME("client_channel_name"),

	/**
	 * 客户端包名
	 */
	CLIENT_PACKAGE_NAME("client_package_name"),

	/**
	 * 客户端版本名
	 */
	CLIENT_VERSION_NAME("client_version_name"),

	/**
	 * 客户端版本号
	 */
	CLIENT_VERSION_CODE("client_version_code"),

	/**
	 * 客户端系统版本
	 */
	OS_VERSION("os_version"),

	/**
	 * 极光推送设备ID
	 */
	PUSH_DEVICE_ID("push_device_id"),

	/**
	 *
	 */
	USER_AGENT("user_agent"),

	/**
	 * 手机型号
	 */
	MODEL_NAME("model_name"),


	IMEI("device_imei"),


	IMSI("device_imsi"),
	;

	private Header(String key) {
		this.key = key;
	}

	private String key;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
}
