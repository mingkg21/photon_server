package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.model.content.BucketVideo;
import cc.ccplay.booster.base.core.SystemParam;
import org.easyj.frame.jdbc.Page;

public interface BucketVideoService {

    public BucketVideo save(SystemParam param, BucketVideo video);

    public Page queryPage(SystemParam param, BucketVideo searchObj);

}
