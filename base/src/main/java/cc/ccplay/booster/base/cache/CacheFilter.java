package cc.ccplay.booster.base.cache;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CacheFilter implements Filter {

    public void destroy() {

    }

    public void doFilter(ServletRequest req, ServletResponse resp,
                         FilterChain chain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest)req;
        HttpServletResponse response = (HttpServletResponse) resp;

        String url = request.getRequestURI();
        //url.endsWith(".jsp") ||
        if(url.endsWith(".css") || url.endsWith(".js")){
            chain.doFilter(req, resp);
            return;
        }
        CacheResponseWrapper mr = new CacheResponseWrapper(response);
        chain.doFilter(req, mr);
        response.getOutputStream().write(mr.getBytes());
    }

    @Override
    public void init(FilterConfig filterconfig) throws ServletException {

    }
}
