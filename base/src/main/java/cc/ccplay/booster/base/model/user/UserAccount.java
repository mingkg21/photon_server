package cc.ccplay.booster.base.model.user;

import com.alibaba.fastjson.annotation.JSONField;
import org.easyj.frame.jdbc.annotation.*;
import org.easyj.frame.jdbc.annotation.value.IntValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.value.StringValue;
import org.easyj.frame.jdbc.model.User;

import java.io.Serializable;
import java.util.Date;

@Table(name = "user_account")
public class UserAccount implements Serializable,User {

	//可用状态
	public static final String STATUS_ACTIVITY = "actived";

	//冻结
	public static final String STATUS_LOCKED = "locked";


	public static final long REGISTER_TYPE_PHONE = 1;
	public static final long REGISTER_TYPE_QQ = 2;
	public static final long REGISTER_TYPE_WECHAT = 3;
	public static final long REGISTER_TYPE_18HANHUA = 4;

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@JSONField(serialize = false)
	@Column(name = "password")
	private String password;

	@Column(name = "username")
	private String username;

	@JSONField(serialize = false)
	@Column(name = "email")
	private String email;

	@JSONField(serialize = false)
	@Column(name = "phone")
	private String phone;

	@Column(name = "nick_name")
	private String nickName;

	@JSONField(serialize = false)
	@StringValue(insert = true,insertValue = UserAccount.STATUS_ACTIVITY)
	@Column(name = "status")
	private String status;

	@JSONField(serialize = false)
	@LogicDelete("1")
	@Where(clause = "delete_flag=0")
	@IntValue(insert = true)
	@Column(name = "delete_flag")
	private Integer deleteFlag;

	@JSONField(serialize = false)
	@IntValue(insert = true)
	@Column(name = "fake_flag")
	private Integer fakeFlag;

	@NowDateValue(insert = true)
	@Column(name = "create_datetime")
	private Date createDatetime;

	@Column(name = "register_type")
	private Long registerType;

	@JSONField(serialize = false)
	@Override
	public Long getSysUserId() {
		return this.id;
	}

	@JSONField(serialize = false)
	@Override
	public String getLoginAccount() {
		return this.username;
	}


	public Long getRegisterType() {
		return registerType;
	}

	public void setRegisterType(Long registerType) {
		this.registerType = registerType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(Integer deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public Integer getFakeFlag() {
		return fakeFlag;
	}

	public void setFakeFlag(Integer fakeFlag) {
		this.fakeFlag = fakeFlag;
	}

	public Date getCreateDatetime() {
		return createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}
}