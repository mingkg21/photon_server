package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="booster_server_group_ref")
public class BoosterServerGroupRef implements Serializable{

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;


	@Column(name = "server_id")
	private Long serverId;

	@Column(name = "group_id")
	private Long groupId;

	@Column(name = "ordering")
	private Long ordering;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;


	public Long getOrdering() {
		return ordering;
	}

	public void setOrdering(Long ordering) {
		this.ordering = ordering;
	}

	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getServerId(){
		return this.serverId;
	}

	public void setServerId(Long serverId){
		this.serverId = serverId;
	}

	public Long getGroupId(){
		return this.groupId;
	}

	public void setGroupId(Long groupId){
		this.groupId = groupId;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}


}

