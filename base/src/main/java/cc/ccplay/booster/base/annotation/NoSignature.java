package cc.ccplay.booster.base.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)  
@Target(ElementType.METHOD)  
/**
  * 加此标签  无需校验签名
 */
public @interface NoSignature {
	
}
