package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.dto.content.CustomAreaItemDto;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.CustomArea;
import cc.ccplay.booster.base.model.content.CustomAreaItem;
import org.easyj.frame.jdbc.Page;

import java.util.List;
import java.util.Map;

public interface CustomAreaService {

    public Page getPage(SystemParam param);

    public CustomArea saveOrUpdate(SystemParam param, CustomArea customArea);

    public int updateStatus(SystemParam param,long id,Status status);

    public CustomArea get(SystemParam param, long id);

    public Page getAreaGamePage(SystemParam param,long areaId,Long gameId,String packageName,String gameName);

    public void saveAreaItem(SystemParam param, CustomAreaItem item);

    public void deleteAreaItem(SystemParam param,long itemId);

    public CustomAreaItemDto getAreaItem(SystemParam param, long itemId);

    public Map<String,CustomArea> getByCode(String... code);

    public void refreshDownloadRankingList();

    public void refreshNewGameDownloadRankingList();

    public void refreshSingleGameRankingList();

    public void refreshOnlineGameRankingList();

    public List<CustomArea> getAllAreaEnabled();

}
