package cc.ccplay.booster.base.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
/**
 * 用于springmvc controller方法
 * @author admin
 *
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface SortMapping {
	
	public final static String SORT_DESC = "desc";
	public final static String SORT_ASC = "asc";
	
	String [] mapping();
	
	/**
	 * sql中若指定了order字段和排序方式，注解中又加了默认排序字段跟方式。 
	 * 系统将优先使用注解中的
	 * @return
	 */
	String defColumn() default "";
	String defSortType() default SORT_DESC;
	
}
