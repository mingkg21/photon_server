package cc.ccplay.booster.base.model.log;

import org.easyj.frame.jdbc.annotation.*;

@Table(name="app_download_complete_record")
public class AppDownloadCompleteRecord extends BaseLogModel {

    @Id
    @GeneratedValue(type = GenerationType.AUTO)
    private Long id;

    @Column(name="app_id")
    private Long appId;

    @Column(name="version_name")
    private String versionName;

    @Column(name="version_id")
    private Long versionId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAppId() {
        return appId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public Long getVersionId() {
        return versionId;
    }

    public void setVersionId(Long versionId) {
        this.versionId = versionId;
    }
}
