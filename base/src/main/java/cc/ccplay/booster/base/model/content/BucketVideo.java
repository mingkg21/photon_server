package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.*;

import java.io.Serializable;

@Table(name="bucket_video")
public class BucketVideo extends BaseFile implements Serializable{

    @Column(name="m3u8")
    private String m3u8;

    @Column(name="type")
    private String type;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getM3u8() {
        return m3u8;
    }

    public void setM3u8(String m3u8) {
        this.m3u8 = m3u8;
    }
}
