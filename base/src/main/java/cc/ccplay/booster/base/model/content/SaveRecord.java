package cc.ccplay.booster.base.model.content;

import cc.ccplay.booster.base.constants.Constant;
import cc.ccplay.booster.base.model.adapter.CdnSource;
import com.alibaba.fastjson.annotation.JSONField;
import org.easyj.frame.jdbc.annotation.value.LongValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import org.easyj.frame.util.StringUtil;

import java.util.Date;
import java.io.Serializable;

@Table(name="save_record")
public class SaveRecord implements Serializable{

	public static final long OPEN_TRUE = 1;//公开存档

	public static final long OPEN_FALSE = 0;//私有存档

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "game_id")
	private Long gameId;

	@Column(name = "name")
	private String name;

	@Column(name = "user_id")
	private Long userId;

	@Column(name = "file_size")
	private Long fileSize;

	@Column(name = "url")
	private CdnSource url;

	@Column(name = "version_code")
	private Long versionCode;

	@Column(name = "version_name")
	private String versionName;

	@Column(name = "open")
	private Long open;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@NowDateValue(update = true,insert = true)
	@Column(name = "update_time")
	private Date updateTime;

	@LongValue(insert = true,insertValue = Constant.STATUS_ENABLED)
	@Column(name = "status")
	private Long status;

	@LongValue(insert = true,insertValue = 0)
	@Column(name = "praise_count")
	private Long praiseCount;

	@LongValue(insert = true,insertValue = 0)
	@Column(name = "comment_count")
	private Long commentCount;

	@Column(name = "model_name")
	private String modelName;

	//适配机型
	@Column(name = "adaptation")
	private String adaptation;


	//备注信息
	@Column(name = "remark")
	private String remark;


	@LogicDelete("1")
	@Where(clause = "delete_flag=0")
	@LongValue(insert = true,insertValue = 0)
	@Column(name="delete_flag")
	@JSONField(serialize = false)
	private Long deleteFlag;


	public String getAdaptation() {
		return adaptation;
	}

	public void setAdaptation(String adaptation) {
		this.adaptation = adaptation;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(Long deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public Long getPraiseCount() {
		return praiseCount;
	}

	public void setPraiseCount(Long praiseCount) {
		this.praiseCount = praiseCount;
	}

	public Long getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(Long commentCount) {
		this.commentCount = commentCount;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getGameId(){
		return this.gameId;
	}

	public void setGameId(Long gameId){
		this.gameId = gameId;
	}

	public String getName(){
		return this.name;
	}

	public void setName(String name){
		this.name = name;
	}

	public Long getUserId(){
		return this.userId;
	}

	public void setUserId(Long userId){
		this.userId = userId;
	}

	public Long getFileSize(){
		return this.fileSize;
	}

	public void setFileSize(Long fileSize){
		this.fileSize = fileSize;
	}

	public CdnSource getUrl(){
		return this.url;
	}

	public void setUrl(CdnSource url){
		this.url = url;
	}

	public void setUrl(String url){
		if(StringUtil.isEmpty(url)){
			this.url = null;
		}else{
			this.url = new CdnSource(url);
		}
	}

	public Long getVersionCode(){
		return this.versionCode;
	}

	public void setVersionCode(Long versionCode){
		this.versionCode = versionCode;
	}

	public String getVersionName(){
		return this.versionName;
	}

	public void setVersionName(String versionName){
		this.versionName = versionName;
	}

	public Long getOpen(){
		return this.open;
	}

	public void setOpen(Long open){
		this.open = open;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Date getUpdateTime(){
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime){
		this.updateTime = updateTime;
	}

	public Long getStatus(){
		return this.status;
	}

	public void setStatus(Long status){
		this.status = status;
	}


}

