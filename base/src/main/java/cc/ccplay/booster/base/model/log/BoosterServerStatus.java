package cc.ccplay.booster.base.model.log;

import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="booster_server_status")
public class BoosterServerStatus implements Serializable{

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;


	@Column(name = "server_id")
	private Long serverId;

	@Column(name = "load")
	private Double load;

	@Column(name = "person_count")
	private Long personCount;

	@Column(name = "contected_count")
	private Long contectedCount;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@Column(name = "last_ack_time")
	private Date lastAckTime;

	@Column(name = "working")
	private Long working;

	public Date getLastAckTime() {
		return lastAckTime;
	}

	public void setLastAckTime(Date lastAckTime) {
		this.lastAckTime = lastAckTime;
	}

	public Long getWorking() {
		return working;
	}

	public void setWorking(Long working) {
		this.working = working;
	}

	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getServerId(){
		return this.serverId;
	}

	public void setServerId(Long serverId){
		this.serverId = serverId;
	}

	public Double getLoad(){
		return this.load;
	}

	public void setLoad(Double load){
		this.load = load;
	}

	public Long getPersonCount(){
		return this.personCount;
	}

	public void setPersonCount(Long personCount){
		this.personCount = personCount;
	}

	public Long getContectedCount(){
		return this.contectedCount;
	}

	public void setContectedCount(Long contectedCount){
		this.contectedCount = contectedCount;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}


}

