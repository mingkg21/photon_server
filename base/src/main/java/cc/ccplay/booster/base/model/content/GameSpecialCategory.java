package cc.ccplay.booster.base.model.content;

import cc.ccplay.booster.base.enums.Status;
import org.easyj.frame.jdbc.annotation.value.LongValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="game_special_category")
public class GameSpecialCategory implements Serializable{

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;


	@Column(name = "name")
	private String name;

	@Column(name = "status")
	private Status status;

	@LongValue(insert = true,insertValue = 0)
	@Column(name = "count")
	private Long count;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;


	@Column(name = "ordering")
	private Long ordering;


	public Long getOrdering() {
		return ordering;
	}

	public void setOrdering(Long ordering) {
		this.ordering = ordering;
	}

	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public String getName(){
		return this.name;
	}

	public void setName(String name){
		this.name = name;
	}


	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Long getCount(){
		return this.count;
	}

	public void setCount(Long count){
		this.count = count;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}


}

