package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="complaint")
public class Complaint implements Serializable{

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;


	@Column(name = "type_id")
	private Long typeId;

	@Column(name = "type_desc")
	private String typeDesc;

	@Column(name = "complaint_content")
	private String complaintContent;

	/**
	 qq || mobile
	 */
	@Column(name = "concat_type")
	private String concatType;

	@Column(name = "concat_number")
	private String concatNumber;

	@Column(name = "game_id")
	private Long gameId;

	@Column(name = "user_id")
	private Long userId;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@Column(name = "model_name")
	private String modelName;

	@Column(name = "os_version")
	private String osVersion;

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getTypeId(){
		return this.typeId;
	}

	public void setTypeId(Long typeId){
		this.typeId = typeId;
	}

	public String getTypeDesc(){
		return this.typeDesc;
	}

	public void setTypeDesc(String typeDesc){
		this.typeDesc = typeDesc;
	}

	public String getComplaintContent(){
		return this.complaintContent;
	}

	public void setComplaintContent(String complaintContent){
		this.complaintContent = complaintContent;
	}

	public String getConcatType(){
		return this.concatType;
	}

	public void setConcatType(String concatType){
		this.concatType = concatType;
	}

	public String getConcatNumber(){
		return this.concatNumber;
	}

	public void setConcatNumber(String concatNumber){
		this.concatNumber = concatNumber;
	}

	public Long getGameId(){
		return this.gameId;
	}

	public void setGameId(Long gameId){
		this.gameId = gameId;
	}

	public Long getUserId(){
		return this.userId;
	}

	public void setUserId(Long userId){
		this.userId = userId;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}


}

