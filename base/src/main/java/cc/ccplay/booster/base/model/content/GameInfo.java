package cc.ccplay.booster.base.model.content;

import cc.ccplay.booster.base.constants.Constant;
import cc.ccplay.booster.base.model.adapter.CdnImage;
import cc.ccplay.booster.base.model.adapter.CdnSource;
import com.alibaba.fastjson.annotation.JSONField;
import cc.ccplay.booster.base.enums.Status;
import org.easyj.frame.jdbc.annotation.*;
import org.easyj.frame.jdbc.annotation.value.LongValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.util.StringUtil;

import java.io.Serializable;
import java.util.Date;

@Table(name="game_info")
public class GameInfo implements Serializable{
    @Id
    @GeneratedValue(type = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @LongValue(insert = true,insertValue = Constant.STATUS_DISABLED)
    @Column(name="status")
    private Long status;

    @Column(name="name")
    private String name;

    @Column(name="name_en")
    private String nameEn;

    @Column(name="package_name")
    private String packageName;

    @Column(name="category_id")
    private Long categoryId;

    @Column(name="publisher_id")
    private Long publisherId;

    @Column(name="score")
    private Double score;

    @Column(name ="total_score")
    @LongValue(insert = true,insertValue = 0)
    private Long totalScore;

    @Column(name="score_count")
    @LongValue(insert = true,insertValue = 0)
    private Long scoreCount;

    @Column(name="description")
    private String description;

    @Column(name="editor_recommend")
    private String editorRecommend;

    @Column(name="dev_recommend")
    private String devRecommend;

    @Column(name="video")
    private CdnSource video;

    @Column(name="video_raw")
    private CdnSource videoRaw;

    @Column(name="create_time")
    @NowDateValue(insert = true)
    private Date createTime;

    @Column(name="update_time")
    @NowDateValue(insert = true,update = true)
    private Date updateTime;

    @Column(name="create_user_id")
    private Long createUserId;
    @Column(name="lastest_version_id")
    private Long lastestVersionId;

    @LogicDelete("1")
    @Where(clause = "delete_flag=0")
    @LongValue(insert = true,insertValue = 0)
    @Column(name="delete_flag")
    @JSONField(serialize = false)
    private Long deleteFlag;

    /**
     * 收藏数量
     */
    @Column(name="follow_count")
    @LongValue(insert = true,insertValue = 0)
    private Long followCount;

    /**
     * 评论数量
     */
    @Column(name="comment_count")
    @LongValue(insert = true,insertValue = 0)
    private Long commentCount;

    /**
     * 测试数量
     */
    @Column(name="test_count")
    @LongValue(insert = true,insertValue = 0)
    private Long testCount;

    @Column(name="download_count")
    @LongValue(insert = true,insertValue = 0)
    private Long downloadCount;


    @Column(name="banner")
    private CdnImage banner;

    @LongValue(insert = true,insertValue = 0)
    @Column(name="praise_count")
    private Long praiseCount;

    @Column(name="cover_image")
    private CdnImage coverImage;

    private String localPackageName;

    public void setCoverImage(String coverImage){
        if(StringUtil.isNotEmpty(coverImage)){
            this.coverImage = new CdnImage(coverImage);
        }else{
            this.coverImage = null;
        }
    }

    public CdnImage getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(CdnImage coverImage) {
        this.coverImage = coverImage;
    }

    public Long getPraiseCount() {
        return praiseCount;
    }

    public void setPraiseCount(Long praiseCount) {
        this.praiseCount = praiseCount;
    }

    public CdnImage getBanner() {
        return banner;
    }

    public void setBanner(CdnImage banner) {
        this.banner = banner;
    }

    public void setBanner(String banner){
        if(StringUtil.isNotEmpty(banner)){
            this.banner = new CdnImage(banner);
        }else{
            this.banner = null;
        }
    }

    public Long getDownloadCount() {
        return downloadCount;
    }

    public void setDownloadCount(Long downloadCount) {
        this.downloadCount = downloadCount;
    }

    public Long getTestCount() {
        return testCount;
    }

    public void setTestCount(Long testCount) {
        this.testCount = testCount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(Long publisherId) {
        this.publisherId = publisherId;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Long getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(Long totalScore) {
        this.totalScore = totalScore;
    }

    public Long getScoreCount() {
        return scoreCount;
    }

    public void setScoreCount(Long scoreCount) {
        this.scoreCount = scoreCount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEditorRecommend() {
        return editorRecommend;
    }

    public void setEditorRecommend(String editorRecommend) {
        this.editorRecommend = editorRecommend;
    }

    public String getDevRecommend() {
        return devRecommend;
    }

    public void setDevRecommend(String devRecommend) {
        this.devRecommend = devRecommend;
    }

    public CdnSource getVideo() {
        return video;
    }

    public void setVideo(String video){
        if(StringUtil.isNotEmpty(video)) {
            this.video = new CdnSource(video);
        }else{
            this.video = new CdnSource("");
        }
    }

    public void setVideo(CdnSource video) {
        this.video = video;
    }

    public CdnSource getVideoRaw() {
        return videoRaw;
    }

    public Long getFollowCount() {
        return followCount;
    }

    public void setFollowCount(Long followCount) {
        this.followCount = followCount;
    }

    public Long getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(Long commentCount) {
        this.commentCount = commentCount;
    }

    public void setVideoRaw(String videoRaw){
        if(StringUtil.isNotEmpty(videoRaw)){
            this.videoRaw = new CdnSource(videoRaw);
        }else{
            this.videoRaw = new CdnSource("");
        }
    }

    public void setVideoRaw(CdnSource videoRaw) {
        this.videoRaw = videoRaw;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public Long getLastestVersionId() {
        return lastestVersionId;
    }

    public void setLastestVersionId(Long lastestVersionId) {
        this.lastestVersionId = lastestVersionId;
    }

    public Long getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Long deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getLocalPackageName() {
        return localPackageName;
    }

    public void setLocalPackageName(String localPackageName) {
        this.localPackageName = localPackageName;
    }
}
