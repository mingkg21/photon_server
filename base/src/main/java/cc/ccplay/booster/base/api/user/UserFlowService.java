package cc.ccplay.booster.base.api.user;

import cc.ccplay.booster.base.dto.content.booster.AddUserFlowDto;
import cc.ccplay.booster.base.model.user.UserFlow;

public interface UserFlowService {

//    public UserFlow addFlow(long userId, long upload, long download);

    public UserFlow addFlow(AddUserFlowDto dto);

    public UserFlow getFlow(long userId);

}
