package cc.ccplay.booster.base.dto.content.game;

import cc.ccplay.booster.base.model.content.GameTag;
import cc.ccplay.booster.base.model.content.SpecialCategoryTagItem;

import java.io.Serializable;

public class SpecialCategoryTagItemDto implements Serializable{

    private SpecialCategoryTagItem item;

    private GameTag gameTag;

    public SpecialCategoryTagItem getItem() {
        return item;
    }

    public void setItem(SpecialCategoryTagItem item) {
        this.item = item;
    }

    public GameTag getGameTag() {
        return gameTag;
    }

    public void setGameTag(GameTag gameTag) {
        this.gameTag = gameTag;
    }
}
