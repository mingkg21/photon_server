package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.value.LongValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import com.alibaba.fastjson.annotation.JSONField;
import java.util.Date;
import java.io.Serializable;

@Table(name="game_recruit")
public class GameRecruit implements Serializable{

	public final static long STATUS_OPEN = 1;
	public final static long STATUS_CLOSE = 0;

	//打赏类型 创造力
	public final static long PRIZE_TYPE_CREATIVITY = 0;
	//打赏类型 洞察力
	public final static long PRIZE_TYPE_INSIGHT = 1;

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;


	@Column(name = "game_id")
	private Long gameId;

	@Column(name = "start_time")
	private Date startTime;

	@Column(name = "end_time")
	private Date endTime;

	/**
	 0:关闭 1:开启  
	 */
	@Column(name = "status")
	@LongValue(insert = true,insertValue = STATUS_CLOSE)
	private Long status;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@JSONField(serialize = false)
	@LogicDelete("1")
	@Where(clause = "delete_flag=0")
	@LongValue(insert = true,insertValue = 0)
	@Column(name = "delete_flag")
	private Long deleteFlag;

	/**
	 参与人数
	 */
	@LongValue(insert = true,insertValue = 0)
	@Column(name = "count")
	private Long count;

	/**
	 鲜游点评
	 */
	@Column(name = "xy_comment")
	private String comment;

	/**
	 * 奖品类型：0：创造力  1：洞察力
	 */
	@Column(name = "prize_type")
	private Long prizeType;

	/**
	 * 奖励分值
	 */
	@Column(name = "prize_score")
	private Long prizeScore;


	public Long getPrizeType() {
		return prizeType;
	}

	public void setPrizeType(Long prizeType) {
		this.prizeType = prizeType;
	}

	public Long getPrizeScore() {
		return prizeScore;
	}

	public void setPrizeScore(Long prizeScore) {
		this.prizeScore = prizeScore;
	}

	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getGameId(){
		return this.gameId;
	}

	public void setGameId(Long gameId){
		this.gameId = gameId;
	}

	public Date getStartTime(){
		return this.startTime;
	}

	public void setStartTime(Date startTime){
		this.startTime = startTime;
	}

	public Date getEndTime(){
		return this.endTime;
	}

	public void setEndTime(Date endTime){
		this.endTime = endTime;
	}

	public Long getStatus(){
		return this.status;
	}

	public void setStatus(Long status){
		this.status = status;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Long getDeleteFlag(){
		return this.deleteFlag;
	}

	public void setDeleteFlag(Long deleteFlag){
		this.deleteFlag = deleteFlag;
	}

	public Long getCount(){
		return this.count;
	}

	public void setCount(Long count){
		this.count = count;
	}

	public String getComment(){
		return this.comment;
	}

	public void setComment(String comment){
		this.comment = comment;
	}
}

