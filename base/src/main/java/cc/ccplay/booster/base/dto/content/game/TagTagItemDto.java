package cc.ccplay.booster.base.dto.content.game;

import cc.ccplay.booster.base.model.content.GameTag;
import cc.ccplay.booster.base.model.content.TagTagItem;

public class TagTagItemDto implements java.io.Serializable{

    private TagTagItem item;
    /**
     * 子标签
     */
    private GameTag gameTag;

    public TagTagItem getItem() {
        return item;
    }

    public void setItem(TagTagItem item) {
        this.item = item;
    }

    public GameTag getGameTag() {
        return gameTag;
    }

    public void setGameTag(GameTag gameTag) {
        this.gameTag = gameTag;
    }
}
