package cc.ccplay.booster.base.util;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.core.SortInfo;
import org.easyj.frame.util.SysUtil;

public class UserUtil extends SysUtil{

    public static SortInfo getSortInfo(){
        SystemParam data = (SystemParam)getCustomData();
        if(data == null){
            return null;
        }
        return data.getSortInfo();
    }

    public static void setSortInfo(String column,String sortType){
        SortInfo si = new SortInfo(column, sortType);
        SystemParam data = (SystemParam)getCustomData();
        if(data == null){
            return;
        }
        data.setSortInfo(si);
    }
}
