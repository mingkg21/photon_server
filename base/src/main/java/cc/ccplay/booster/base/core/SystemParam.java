package cc.ccplay.booster.base.core;

import cc.ccplay.booster.base.enums.SourceType;
import org.easyj.frame.jdbc.model.User;

import java.io.Serializable;

public class SystemParam implements Serializable{

    /**
     *
     */
    private String clientIp;

    /**
     * app头部请求信息
     */
    private AppHeaderInfo appHeaderInfo;

    /**
     * 当前登录的用户
     */
    private User user;

    /**
     * 分页参数
     */
    private PagingParameter  pagingParameter;


    /**
     * 来源入口
     */
    private SourceType sourceType;

    /**
     * 排序信息
     */
    private SortInfo sortInfo;

    /**
     * 用户信息
     */
    private String useragent;

    /**
     * 来源路径
     */
    private String referer;


    public String getUseragent() {
        return useragent;
    }

    public void setUseragent(String useragent) {
        this.useragent = useragent;
    }

    public String getReferer() {
        return referer;
    }

    public void setReferer(String referer) {
        this.referer = referer;
    }

    public AppHeaderInfo getAppHeaderInfo() {
        return appHeaderInfo;
    }

    public void setAppHeaderInfo(AppHeaderInfo appHeaderInfo) {
        this.appHeaderInfo = appHeaderInfo;
    }

    public boolean isBackend(){
        return this.sourceType == SourceType.BACKEND;
    }

    public boolean isApp(){
        return this.sourceType == SourceType.APP;
    }

    public boolean isWww(){
        return this.sourceType == SourceType.WWW;
    }

    public boolean isWap(){
        return this.sourceType == SourceType.WAP;
    }

    /**
     * 用户前端请求
     * @return
     */
    public boolean isUserFrontend(){
        return this.sourceType == SourceType.APP || this.sourceType == SourceType.WWW || this.sourceType == SourceType.WAP;
    }

    public Long getUserId(){
        if(this.user == null){
            return null;
        }
        return this.user.getSysUserId();
    }

    public String getUserAccount(){
        if(this.user == null){
            return null;
        }
        return this.user.getLoginAccount();
    }


    public boolean isLogin(){
        return user != null;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public SourceType getSourceType() {
        return sourceType;
    }

    public void setSourceType(SourceType sourceType) {
        this.sourceType = sourceType;
    }

    public PagingParameter getPagingParameter() {
        return pagingParameter;
    }

    public void setPagingParameter(PagingParameter pagingParameter) {
        this.pagingParameter = pagingParameter;
    }

    public SortInfo getSortInfo() {
        return sortInfo;
    }

    public void setSortInfo(SortInfo sortInfo) {
        this.sortInfo = sortInfo;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }
}
