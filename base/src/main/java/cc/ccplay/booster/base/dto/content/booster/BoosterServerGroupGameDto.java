package cc.ccplay.booster.base.dto.content.booster;

import cc.ccplay.booster.base.dto.content.game.AbstractGame;
import cc.ccplay.booster.base.model.content.BoosterServerGroup;
import cc.ccplay.booster.base.model.content.BoosterServerGroupGame;

import java.io.Serializable;

public class BoosterServerGroupGameDto extends AbstractGame implements Serializable {

    private BoosterServerGroupGame boosterServerGroupGame;

    private BoosterServerGroup group;

    public BoosterServerGroup getGroup() {
        return group;
    }

    public void setGroup(BoosterServerGroup group) {
        this.group = group;
    }

    public BoosterServerGroupGame getBoosterServerGroupGame() {
        return boosterServerGroupGame;
    }

    public void setBoosterServerGroupGame(BoosterServerGroupGame boosterServerGroupGame) {
        this.boosterServerGroupGame = boosterServerGroupGame;
    }
}
