package cc.ccplay.booster.base.core;

import java.io.Serializable;

public class PagingParameter implements Serializable{

    public PagingParameter(){}

    public PagingParameter(int pageNo,int pageSize){
        this.pageNo = pageNo;
        this.pageSize = pageSize;
    }

    private int pageSize;

    private int pageNo;

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }
}
