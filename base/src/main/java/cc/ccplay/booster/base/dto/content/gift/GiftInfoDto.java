package cc.ccplay.booster.base.dto.content.gift;

import cc.ccplay.booster.base.dto.content.game.AbstractGame;
import cc.ccplay.booster.base.model.content.GiftInfo;
import cc.ccplay.booster.base.model.content.GiftReceiveRecord;
import com.alibaba.fastjson.annotation.JSONField;
import org.easyj.frame.util.StringUtil;

public class GiftInfoDto extends AbstractGame implements java.io.Serializable{

    private GiftInfo giftInfo;

    public GiftInfo getGiftInfo() {
        return giftInfo;
    }

    public void setGiftInfo(GiftInfo giftInfo) {
        this.giftInfo = giftInfo;
    }

    @JSONField(serialize = false)
    private Long type;

    private String code;


    @JSONField
    public Integer getStatus(){
        Long count = giftInfo.getGiftCount();
        Long receiveCount = giftInfo.getReceiveCount();

        if(type == null){
            if(StringUtil.equals(count,receiveCount)){
                return 1; //可淘号
            }else{
                return 0; //可领取
            }
        }else if(StringUtil.equals(type, GiftReceiveRecord.TYPE_RECEIVE)){
            return 2;//已经领取
        }else if(StringUtil.equals(type,GiftReceiveRecord.TYPE_TAO)){
            return 3; //已淘号
        }
        return null;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
