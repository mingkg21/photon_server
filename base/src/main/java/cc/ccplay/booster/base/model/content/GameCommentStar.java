package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.value.LongValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="game_comment_star")
public class GameCommentStar implements Serializable{

	@CompositeId
	private Id id;

	/**
	 评星 0 - 10
	 */
	@Column(name = "star")
	private Long star;

	@NowDateValue(update = true,insert = true)
	@Column(name = "update_time")
	private Date updateTime;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	/**
	 数据版本号
	 */
	@LongValue(insert = true,insertValue = 0)
	@Column(name = "data_version")
	private Long dataVersion;



	public Id getId(){
		return id;
	}

	public void setId(Id id){
		this.id = id;
	}

	public Long getStar(){
		return this.star;
	}

	public void setStar(Long star){
		this.star = star;
	}

	public Date getUpdateTime(){
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime){
		this.updateTime = updateTime;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Long getDataVersion(){
		return this.dataVersion;
	}

	public void setDataVersion(Long dataVersion){
		this.dataVersion = dataVersion;
	}


	public static class Id{

		public Id(){}

		public Id(Long gameId,Long userId){
			this.gameId = gameId;
			this.userId = userId;
		}

		@Column(name = "game_id")
		private Long gameId;

		@Column(name = "user_id")
		private Long userId;


		public Long getGameId(){
			return this.gameId;
		}

		public void setGameId(Long gameId){
			this.gameId = gameId;
		}

		public Long getUserId(){
			return this.userId;
		}

		public void setUserId(Long userId){
			this.userId = userId;
		}

	}
}

