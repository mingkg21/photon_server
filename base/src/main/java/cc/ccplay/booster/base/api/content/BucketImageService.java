package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.content.BucketImage;
import org.easyj.frame.jdbc.Page;

public interface BucketImageService {

    public BucketImage save(SystemParam param, BucketImage image);

    public Page queryPage(SystemParam param,BucketImage searchObj, String nameKeyword);
}
