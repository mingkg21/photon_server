package cc.ccplay.booster.base.test.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ApiMethod {

    //接口用途
    String value();

    //是否是分页
    boolean paging() default false;

    //是否开放测试
    boolean open() default true;

    //是否加密
    boolean encrypt() default false;
}
