package cc.ccplay.booster.base.model.user;

import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="user_system_message")
public class UserSystemMessage implements Serializable{

	//已经发送
	public static long PUSH_STATUS_SENT = 1;

	//未发送
	public static long PUSH_STATUS_UNSEND = 0;


	//系统消息
	public static long MSG_TYPE_SYSTEM = 0;

	//普通消息
	public static long MSG_TYPE_GENERAL = 1;

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "msg_title")
	private String msgTitle;

	@Column(name = "msg_content")
	private String msgContent;

	@Column(name = "content_type")
	private Long contentType;

	@Column(name = "object_id")
	private Long objectId;

	@Column(name = "object_title")
	private String objectTitle;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@Column(name = "user_id")
	private Long userId;

	/**
	 	0：未发送
		1：待发送
	 */
	@Column(name = "push_status")
	private Long pushStatus;

	@Column(name = "push_time")
	private Date pushTime;

	@Column(name = "push_msg")
	private String pushMsg;

	//系统消息
	@Column(name = "msg_type")
	private Long msgType;


	public Long getMsgType() {
		return msgType;
	}

	public void setMsgType(Long msgType) {
		this.msgType = msgType;
	}

	public String getPushMsg() {
		return pushMsg;
	}

	public void setPushMsg(String pushMsg) {
		this.pushMsg = pushMsg;
	}

	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public String getMsgTitle(){
		return this.msgTitle;
	}

	public void setMsgTitle(String msgTitle){
		this.msgTitle = msgTitle;
	}

	public String getMsgContent(){
		return this.msgContent;
	}

	public void setMsgContent(String msgContent){
		this.msgContent = msgContent;
	}

	public Long getContentType(){
		return this.contentType;
	}

	public void setContentType(Long contentType){
		this.contentType = contentType;
	}

	public Long getObjectId(){
		return this.objectId;
	}

	public void setObjectId(Long objectId){
		this.objectId = objectId;
	}

	public String getObjectTitle(){
		return this.objectTitle;
	}

	public void setObjectTitle(String objectTitle){
		this.objectTitle = objectTitle;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Long getUserId(){
		return this.userId;
	}

	public void setUserId(Long userId){
		this.userId = userId;
	}

	public Long getPushStatus(){
		return this.pushStatus;
	}

	public void setPushStatus(Long pushStatus){
		this.pushStatus = pushStatus;
	}

	public Date getPushTime(){
		return this.pushTime;
	}

	public void setPushTime(Date pushTime){
		this.pushTime = pushTime;
	}

}

