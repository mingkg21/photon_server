package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.dto.content.BoosterAuthDto;
import cc.ccplay.booster.base.dto.content.booster.BoosterUserInfoDto;
import cc.ccplay.booster.base.model.content.BoosterServer;
import cc.ccplay.booster.base.model.user.UserFlow;

import java.util.List;

public interface BoosterApiService {

    public static final String KEY_LAST_ACK_TIME = "last_ack_time";

    public void offline(long userId);

    public BoosterAuthDto login(UserFlow flow, long gameId,String serverIds);

    public boolean ack(long userId,long serverId,long port);

    public List<BoosterUserInfoDto> getUsers(long serverId);

    public void clearOfflinePort();

    public void clearOfflineUser();

    public List<BoosterServer> getAllServer();

    public BoosterAuthDto getSuperAuth(long userId,long serverId);

    public void telnetAllServerPortStatus();

    public boolean updateSuccessPercent(long serverId,double successPercent,long failWarning);

}
