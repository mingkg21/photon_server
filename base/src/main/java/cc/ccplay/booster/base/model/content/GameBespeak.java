package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.value.LongValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import com.alibaba.fastjson.annotation.JSONField;
import java.util.Date;
import java.io.Serializable;

@Table(name="game_bespeak")
public class GameBespeak implements Serializable{

	//关闭
	public final static long BESPEAK_STATUS_CLOSE = 0;

	//开启
	public final static long BESPEAK_STATUS_OPEN = 1;


	//推荐
	public final static long RECOMMEND_STATUS_NO = 0;

	//不推荐
	public final static long RECOMMEND_STATUS_YES = 1;


	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;


	@Column(name = "game_id")
	private Long gameId;

	@Column(name = "version_id")
	private Long versionId;

	/**
	 0:关闭 1:开启
	 */
	@Column(name = "bespeak_status")
	private Long bespeakStatus;

	/**
	 测试时间
	 */
	@Column(name = "test_time")
	private Date testTime;

	/**
	 测试类型
	 */
	@Column(name = "test_type_id")
	private Long testTypeId;

	/**
	 0:不推荐 1:推荐
	 */
	@Column(name = "recommend_status")
	private Long recommendStatus;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@NowDateValue(update = true,insert = true)
	@Column(name = "update_time")
	private Date updateTime;

	@JSONField(serialize = false)
	@LogicDelete("1")
	@Where(clause = "delete_flag=0")
	@LongValue(insert = true,insertValue = 0)
	@Column(name = "delete_flag")
	private Long deleteFlag;

	/**
	 预约人数
	 */
	@LongValue(insert = true,insertValue = 0)
	@Column(name = "bespeak_count")
	private Long bespeakCount;



	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getGameId(){
		return this.gameId;
	}

	public void setGameId(Long gameId){
		this.gameId = gameId;
	}

	public Long getVersionId(){
		return this.versionId;
	}

	public void setVersionId(Long versionId){
		this.versionId = versionId;
	}

	public Long getBespeakStatus(){
		return this.bespeakStatus;
	}

	public void setBespeakStatus(Long bespeakStatus){
		this.bespeakStatus = bespeakStatus;
	}

	public Date getTestTime(){
		return this.testTime;
	}

	public void setTestTime(Date testTime){
		this.testTime = testTime;
	}

	public Long getTestTypeId(){
		return this.testTypeId;
	}

	public void setTestTypeId(Long testTypeId){
		this.testTypeId = testTypeId;
	}

	public Long getRecommendStatus(){
		return this.recommendStatus;
	}

	public void setRecommendStatus(Long recommendStatus){
		this.recommendStatus = recommendStatus;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Date getUpdateTime(){
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime){
		this.updateTime = updateTime;
	}

	public Long getDeleteFlag(){
		return this.deleteFlag;
	}

	public void setDeleteFlag(Long deleteFlag){
		this.deleteFlag = deleteFlag;
	}

	public Long getBespeakCount(){
		return this.bespeakCount;
	}

	public void setBespeakCount(Long bespeakCount){
		this.bespeakCount = bespeakCount;
	}


}

