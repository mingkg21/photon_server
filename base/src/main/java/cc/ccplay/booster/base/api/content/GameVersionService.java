package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.content.GameVersion;
import cc.ccplay.booster.base.model.content.GameVersionPicture;
import cc.ccplay.booster.base.dto.content.game.EditGameVersionDto;
import cc.ccplay.booster.base.enums.Status;
import org.easyj.frame.jdbc.Page;

import java.util.List;

public interface GameVersionService {

    public GameVersion saveOrUpdate(GameVersion gameVersion, List<GameVersionPicture> pics, boolean replacePackageName, String packageName);

    public GameVersion get(long id);

    public Page getPage(SystemParam param, long gameId);

    public EditGameVersionDto getEditInfo(SystemParam param, long id);

    public void setLastestVersion(SystemParam param, long id);

    public List<GameVersion> getAllVersion(SystemParam param, long gameId);

    public EditGameVersionDto getLastVersionDto(long gameId);

    public  void updateStatus(long id,Status status);

}
