package cc.ccplay.booster.base.constants;

import org.easyj.frame.FrameProperites;
import org.easyj.frame.jdbc.Page;

import java.util.Collections;

public class Constant {


    public static final long STATUS_DISABLED = 1;
    public static final long STATUS_ENABLED = 2;

    /**
     * memcache key前缀,用于区分在不同项目，如果使用同一台memcache时，可能产生的key覆盖
     */
    public static final String MEMCACHE_KEY_PREFIX = "XY_";

    public static final String XIANYO_PWD_SUFFIX = "xianyo&3D921_11";

	public static final String USER_DEFAULT_HEAD_ICON = "defaultUserIcon.png";
    //公钥
    public static final String RSA_PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDXCd2igyow+g/03GBA8Cpwm1hByRMdbja4b6aZisjGw67nKTzs9EbtNBkkuseX0lxmchcg0wAeDmcQ1bFX2GEjCizucuAQ4NOXtjcUyD8COiccUgOu7fyoB4HGiwqNH9MQP9lGWJKTm9sfjRSN6/ry95buBJI1GOSohMUgSwdqAQIDAQAB";
    //私钥
    public static final String RSA_PRIVATE_KEY = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBANcJ3aKDKjD6D/TcYEDwKnCbWEHJEx1uNrhvppmKyMbDrucpPOz0Ru00GSS6x5fSXGZyFyDTAB4OZxDVsVfYYSMKLO5y4BDg05e2NxTIPwI6JxxSA67t/KgHgcaLCo0f0xA/2UZYkpOb2x+NFI3r+vL3lu4EkjUY5KiExSBLB2oBAgMBAAECgYBl7S840KM3A9B8Z9jX7v6u0XNL+1ssIAmf8owtSzNPw4Er4MgYKNFVrdQdLMtekz9o77s2u8zackk4GZIt6UpgywwYZ4lp/KprT8bxfTZtWf0+MYh5G/0WuNUn7SXMuRAoc4yYHZKYQuhs5NWAQZFaFnUxsrc2h3ar0KTCn0fAgQJBAOvdQR/RnfKBd4xVLViPqvfexuK7ZYbqXflytj3hL5TF90t03CI5IPHKFx3VqWDgFG16rlR9M+GZ7ic86o62nS0CQQDpZXiNQZjKJ92frYvktWKYXRmED5EHsBl7mZKwVi5p6jV4mmFLStRkC38g1knRLHEqdEMNCJmWYjzHA0V1jgylAkAI7OZ5/cSsWJndhZuhrk4Z8yQzkEPXRycnOWAMF2llh9hD0rhB00eb3rnhNyShtdkQC4RtTg+YieqpMXTu0ZpBAkEAlcfix3QTY5iV58VuA4ZMEc+dclyzDCX9FI8HzlZgTuRZEF6ylakeCF5AZYhfsvc8YKxf41tjhVjh/C2jQ7+3aQJBAI9HXAEXbFevBbORpKARKskxzEJUvW7XfRB9Cegl+PxMBQIudaog8Kru3eosL92hmOBtXCQqMkk0GpKGd4Jy8Oo=";

	public static final String HEADER_ENCRYPT_STATUS_KEY = "content-encrypt";
    /**
     *  用户默认密码
     */
	public static final String USER_DEFAULT_PASSWORD = "123456";

    /**
     * 空Page
     */
	public static final Page EMPTY_PAGE = new Page(0, 0, FrameProperites.PAGING_DEFAULT_PAGE_SIZE, Collections.emptyList());

}
