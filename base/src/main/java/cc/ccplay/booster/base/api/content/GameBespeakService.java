package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.game.GameBespeakDto;
import cc.ccplay.booster.base.model.content.GameBespeak;
import org.easyj.frame.jdbc.Page;


public interface GameBespeakService {

    public Page getPage(SystemParam param);

    public Page<GameBespeakDto> getDtoPage(SystemParam param);

    public GameBespeak saveOrUpdate(SystemParam param, GameBespeak bespeak);

    public GameBespeak get(SystemParam param, long id);

    public GameBespeakDto getEditInfo(SystemParam param, long id);

    /**
     * 预约
     * @param param
     * @param bespeakId
     */
    public void bespeak(SystemParam param,long bespeakId);


}
