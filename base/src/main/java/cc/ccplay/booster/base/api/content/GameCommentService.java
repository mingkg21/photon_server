package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.model.content.GameComment;
import com.alibaba.fastjson.JSONArray;
import cc.ccplay.booster.base.dto.content.game.GameCommentDto;
import cc.ccplay.booster.base.model.content.GameCommentStarRange;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.game.GameCommentRecordDto;
import org.easyj.frame.jdbc.Page;

import java.util.List;

public interface GameCommentService {

    public GameComment comment(GameComment comment, long star);

    public GameComment reply(long commentId, GameComment reply);

    public Page<GameCommentDto> getCommentPage(SystemParam param,GameComment searchObject,String gameName, Long orderType);

    public Page<GameCommentDto> getReplyPage(SystemParam param, long commentId,Long orderType);

    public void praiseComment(SystemParam param,long commentId);

    public void cancelPraiseComment(SystemParam param,long commentId);

    public GameCommentRecordDto getCommentRecord(SystemParam param, long gameId);

    public GameCommentStarRange getGameCommentStarRange(SystemParam param, long gameId);

    public GameCommentDto getComment(SystemParam param,long commentId);

    public void updateNotNull(GameComment gameComment);

    public void delete(long id);

    public int batchDelete(String ids);

    public void saveOfficialReply(long id,String content,JSONArray imgs);


    /**
     * 获取近期置顶的游戏评论
     * @param size
     * @return
     */
    public List<GameCommentDto> getHotGameComment(int size);

}
