package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.game.GameRecruitQuestionDto;
import cc.ccplay.booster.base.model.content.GameRecruitQuestion;
import cc.ccplay.booster.base.model.content.GameRecruitAnswer;
import org.easyj.frame.jdbc.Page;

import java.util.List;

public interface GameRecruitQuestionService {

    public void saveOrUpdate(SystemParam systemParam, GameRecruitQuestion question, List<GameRecruitAnswer> answers);

    public Page getPage(SystemParam systemParam,long recruitId);

    public GameRecruitQuestionDto getGameRecruitQuestionDto(SystemParam systemParam, long questionId);

}
