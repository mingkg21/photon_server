package cc.ccplay.booster.base.api.user;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.user.SimpleUserDto;
import cc.ccplay.booster.base.dto.user.UserFollowDto;
import org.easyj.frame.jdbc.Page;

public interface UserProfileService {

	SimpleUserDto updateUserProfile(SystemParam SP, String headIcon, Integer sex, String nickName, String area, String signature);

	public void follow(SystemParam param,long beFollowUserId);

	public void unfollow(SystemParam param,long beFollowUserId);

	public Page<UserFollowDto> getMyFollow(SystemParam param);

	public Page<UserFollowDto>  getMyFans(SystemParam param);

	public SimpleUserDto getUserInfo(SystemParam param);

	public void bindPhone(SystemParam param,String phone, String validateCode);
}
