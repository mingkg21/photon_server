package cc.ccplay.booster.base.model.log;

import cc.ccplay.booster.base.core.AppHeaderInfo;
import cc.ccplay.booster.base.core.SystemParam;
import org.easyj.frame.jdbc.annotation.Column;
import org.easyj.frame.jdbc.annotation.ParentModel;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;

import java.util.Date;

@ParentModel
public abstract class BaseLogModel implements java.io.Serializable {

    public void setBaseLogValue(SystemParam systemParam){
        if(systemParam == null){
            return;
        }
        this.setReferer(systemParam.getReferer());
        this.setUseragent(systemParam.getUseragent());
        this.setIp(systemParam.getClientIp());
        this.setUserId(systemParam.getUserId());
        this.setSourceType(new Long(systemParam.getSourceType().getValue()));
        AppHeaderInfo headerInfo = systemParam.getAppHeaderInfo();
        if(headerInfo != null) {
            this.setClientPackageName(headerInfo.getClientPackageName());
            this.setClientChannelName(headerInfo.getClientChannelName());
            this.setClientVersionName(headerInfo.getClientVersionName());
            this.setDeviceImei(headerInfo.getDeviceImei());
            this.setDeviceImsi(headerInfo.getDeviceImsi());
            this.setMacCode(headerInfo.getMacCode());
            this.setOsVersion(headerInfo.getOsVersion());
            this.setModelName(headerInfo.getModelName());
            this.setDeviceNo(headerInfo.getDeviceNo());
        }
    }


    @Column(name="ip")
    private String ip;

    @Column(name="device_no")
    private String deviceNo;

    @Column(name="device_imsi")
    private String deviceImsi;

    @Column(name="device_imei")
    private String deviceImei;

    @Column(name="mac_code")
    private String macCode;

    @Column(name="model_name")
    private String modelName;

    @Column(name="os_version")
    private String osVersion;

    @Column(name="client_version_name")
    private String clientVersionName;

    @Column(name="client_package_name")
    private String clientPackageName;

    @Column(name="client_channel_name")
    private String clientChannelName;

    @Column(name="user_id")
    private Long userId;

    @Column(name="create_time")
    @NowDateValue(insert = true)
    private Date createTime;

    @Column(name="useragent")
    private String useragent;

    @Column(name="source_type")
    private Long sourceType;

    @Column(name="referer")
    private String referer;

    public String getReferer() {
        return referer;
    }

    public void setReferer(String referer) {
        this.referer = referer;
    }

    public String getUseragent() {
        return useragent;
    }

    public void setUseragent(String useragent) {
        this.useragent = useragent;
    }

    public Long getSourceType() {
        return sourceType;
    }

    public void setSourceType(Long sourceType) {
        this.sourceType = sourceType;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getDeviceNo() {
        return deviceNo;
    }

    public void setDeviceNo(String deviceNo) {
        this.deviceNo = deviceNo;
    }

    public String getDeviceImsi() {
        return deviceImsi;
    }

    public void setDeviceImsi(String deviceImsi) {
        this.deviceImsi = deviceImsi;
    }

    public String getDeviceImei() {
        return deviceImei;
    }

    public void setDeviceImei(String deviceImei) {
        this.deviceImei = deviceImei;
    }

    public String getMacCode() {
        return macCode;
    }

    public void setMacCode(String macCode) {
        this.macCode = macCode;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public String getClientVersionName() {
        return clientVersionName;
    }

    public void setClientVersionName(String clientVersionName) {
        this.clientVersionName = clientVersionName;
    }

    public String getClientPackageName() {
        return clientPackageName;
    }

    public void setClientPackageName(String clientPackageName) {
        this.clientPackageName = clientPackageName;
    }

    public String getClientChannelName() {
        return clientChannelName;
    }

    public void setClientChannelName(String clientChannelName) {
        this.clientChannelName = clientChannelName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
