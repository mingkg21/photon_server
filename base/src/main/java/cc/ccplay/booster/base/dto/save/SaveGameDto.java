package cc.ccplay.booster.base.dto.save;

import cc.ccplay.booster.base.model.content.GameInfo;
import cc.ccplay.booster.base.model.content.SaveGame;

import java.io.Serializable;

public class SaveGameDto implements Serializable {

    private SaveGame saveGame;

    private GameInfo gameInfo;

    public SaveGame getSaveGame() {
        return saveGame;
    }

    public void setSaveGame(SaveGame saveGame) {
        this.saveGame = saveGame;
    }

    public GameInfo getGameInfo() {
        return gameInfo;
    }

    public void setGameInfo(GameInfo gameInfo) {
        this.gameInfo = gameInfo;
    }
}
