package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.game.GameInfoDto;
import org.easyj.frame.jdbc.Page;

import java.util.List;

public interface UserGameListService {

    public void submitAppList(long userId,List<String> packageNames);

    public void delete(long userId,long gameId);

    public Page<GameInfoDto> getPlayGamePage(SystemParam param, long userId);
}
