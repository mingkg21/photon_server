package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.model.content.GameVersion;
import cc.ccplay.booster.base.dto.SelectizeDto;
import cc.ccplay.booster.base.dto.content.game.EditGameDto;
import cc.ccplay.booster.base.dto.content.game.GameInfoDto;
import cc.ccplay.booster.base.model.content.GameInfo;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.reader.dto.BaseGameDto;
import org.easyj.frame.jdbc.Page;

import java.util.List;
import java.util.Set;

public interface GameInfoService {

    public Page getPage(SystemParam param, GameInfo searchInfo, Long tagId,Long versionType,Boolean newVersion);

    public GameInfo saveOrUpdate(SystemParam param, GameInfo gameInfo,long [] tagIds,long [] specialTagIds);

    public int updateStatus(SystemParam param, long gameId, Status status);

    public GameInfo get(SystemParam param,long id);

    public EditGameDto getEditGameInfo(SystemParam param, long id);

    public List<SelectizeDto> getForSelectize(SystemParam param, String name);

    public GameInfoDto getGameInfoDto(SystemParam param, long id);

    public void follow(SystemParam param,long gameId);

    public void praise(SystemParam param,long gameId);

    public void cancelFollow(SystemParam param,long gameId);

    //public Page<GameInfoDto> getUserFollowGames(SystemParam param);

    public Page<GameInfoDto> searchGames(SystemParam param,String name);

    public Page<GameInfoDto> getGameByTag(SystemParam param,Long mainTagId,Long childTagId,Long categoryId,String orderType);

    public Page<GameInfoDto> getSameTypeGames(SystemParam param,long gameId);

    public Page<GameVersion> getUpdateLogs(SystemParam param, long gameId);

    public String createDownloadUrl(SystemParam param,long versionId,boolean first);

    public Page<GameInfoDto> getPageByAreaCode(SystemParam param,String code,String gameName);

    public List<GameInfoDto> getLastestVersionInfoByPackageName(SystemParam param,List<String> packageNames,Boolean needTags);

    /**
     * 推荐存档游戏
     * @param param
     * @return
     */
    public Page<GameInfoDto> getRecommendSaveGame(SystemParam param);


    public Page<GameInfoDto> getMySaveGame(SystemParam param,long userId);

    /**
     * 获取用户关注的游戏
     * @param param
     * @param userId
     * @return
     */
    public Page<GameInfoDto> getUserFllowGames(SystemParam param,long userId);


    public Page<GameInfoDto> getGameByCategory(SystemParam param,long categoryId);


    public boolean getGameFollowStatus(long userId,long gameId);


    public List<BaseGameDto> getGameInfos(Set<Long> gameIds);
}
