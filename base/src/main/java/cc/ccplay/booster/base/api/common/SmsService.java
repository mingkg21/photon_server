package cc.ccplay.booster.base.api.common;

import cc.ccplay.booster.base.enums.SmsBizTypeEnum;

public interface SmsService {

    void sendSms(SmsBizTypeEnum smsBizTypeEnum, String smsTo, String... smsContent);

    boolean verifySmsCode(String smsTo, String inputCode);


    void sendWarningNotice(String serverName,String ip,String dateStr,String successPercent);

    void sendRecoveryNotice(String serverName,String ip,String dateStr,String successPercent);
}
