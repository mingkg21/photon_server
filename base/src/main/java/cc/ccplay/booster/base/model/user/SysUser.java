package cc.ccplay.booster.base.model.user;

import com.alibaba.fastjson.annotation.JSONField;
import org.easyj.frame.jdbc.annotation.*;
import org.easyj.frame.jdbc.annotation.value.LongValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.model.User;

import java.io.Serializable;
import java.util.Date;

@Table(name="sys_user")
public class SysUser implements Serializable,User{

    //禁用
    public static final long STATUS_DISABLED = 2L;

    //启动
    public static final long STATUS_ENABLED = 1L;

    private static final long serialVersionUID = -2643493921687579245L;

    @Id
    @GeneratedValue(type = GenerationType.AUTO)
    @Column(name="id")
    private Long id;

    @Column(name="account")
    private String account;

    @Column(name="password")
    private String password;

    @NowDateValue(insert = true)
    @Column(name="create_time")
    private Date createTime;

    @NowDateValue(update = true,insert = true)
    @Column(name="update_time")
    private Date updateTime;

    @LongValue(insert = true,insertValue = STATUS_ENABLED)
    @Column(name="status")
    private Long status;

    @JSONField(serialize = false)
    @LogicDelete("1")
    @Where(clause = "delete_flag=0")
    @LongValue(insert = true,insertValue = 0)
    @Column(name="delete_flag")
    private Long deleteFlag;

    @Column(name="role_id")
    private Long roleId;

    @Column(name="user_name")
    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Long deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @JSONField(serialize = false)
    public Long getSysUserId() {
        return this.id;
    }

    @JSONField(serialize = false)
    public String getLoginAccount() {
        return this.account;
    }
}
