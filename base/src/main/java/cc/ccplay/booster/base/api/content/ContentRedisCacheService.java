package cc.ccplay.booster.base.api.content;

public interface ContentRedisCacheService {

    public Long getNewGameLastupdateTime();

    public Long getHotGameRecommendLastupdateTime();

}
