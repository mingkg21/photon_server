package cc.ccplay.booster.base.dto.content.game;

import cc.ccplay.booster.base.model.content.GameVersion;
import cc.ccplay.booster.base.model.content.GameInfo;
import com.alibaba.fastjson.annotation.JSONField;
import org.easyj.frame.util.StringUtil;

public abstract class AbstractGame {

    @JSONField(serialize = false)
    private GameInfo gameInfo;

    @JSONField(serialize = false)
    private GameVersion versionInfo;

    @JSONField(serialize = false)
    private Long gameFollowStatus;

    private GameExtendDto game;

    @JSONField(serialize = false)
    public Long getGameId(){
        if(gameInfo == null){
            return null;
        }
        return gameInfo.getId();
    }

    public Long getGameFollowStatus() {
        return gameFollowStatus;
    }

    public void setGameFollowStatus(Long gameFollowStatus) {
        this.gameFollowStatus = gameFollowStatus;
    }

    public GameVersion getVersionInfo() {
        return versionInfo;
    }

    public void setVersionInfo(GameVersion versionInfo) {
        this.versionInfo = versionInfo;
    }

    public void setGame(GameExtendDto game) {
        this.game = game;
    }

    //fastjson渲染的时候会调用get方法
    //动态拼接数据结构给前端
    public GameExtendDto getGame() {
        if(game == null && gameInfo != null ){
            this.game = new GameExtendDto(gameInfo,versionInfo);
            if(gameFollowStatus != null) {
                this.game.setFollow(StringUtil.equals(gameFollowStatus,1L));
            }
        }
        return game;
    }

    public GameInfo getGameInfo() {
        return gameInfo;
    }

    public void setGameInfo(GameInfo gameInfo) {
        this.gameInfo = gameInfo;
    }

    public void setLocalPackageName(String localPackageName) {
        gameInfo.setLocalPackageName(localPackageName);
    }
}
