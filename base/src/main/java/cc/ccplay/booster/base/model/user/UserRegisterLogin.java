package cc.ccplay.booster.base.model.user;

import org.easyj.frame.jdbc.annotation.Column;
import org.easyj.frame.jdbc.annotation.Id;
import org.easyj.frame.jdbc.annotation.Table;
import org.easyj.frame.jdbc.annotation.value.IntValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;

import java.util.Date;

@Table(name = "user_register_login")
public class UserRegisterLogin {

	@Id
	@Column(name = "user_id")
	private Long userId;

	@NowDateValue(insert = true)
	@Column(name = "register_date")
	private Date registerDate;

	@NowDateValue(insert = true)
	@Column(name = "register_datetime")
	private Date registerDatetime;

	@Column(name = "register_ip")
	private String registerIp;

	@Column(name = "register_device_no")
	private String registerDeviceNo;

	@Column(name = "register_os_version")
	private String registerOsVersion;

	@Column(name = "register_way")
	private String registerWay;

	@IntValue(insert = true)
	@Column(name = "login_count")
	private Integer loginCount;

	@NowDateValue(update = true, insert = true)
	@Column(name = "last_login_datetime")
	private Date lastLoginDatetime;

	@Column(name = "last_login_ip")
	private String lastLoginIp;

	@Column(name = "last_login_device_no")
	private String lastLoginDeviceNo;

	@NowDateValue(update = true, insert = true)
	@Column(name = "update_datetime")
	private Date updateDatetime;

	private Boolean loginCountPlus;

	public Boolean getLoginCountPlus() {
		return loginCountPlus;
	}

	public void setLoginCountPlus(Boolean loginCountPlus) {
		this.loginCountPlus = loginCountPlus;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public Date getRegisterDatetime() {
		return registerDatetime;
	}

	public void setRegisterDatetime(Date registerDatetime) {
		this.registerDatetime = registerDatetime;
	}

	public String getRegisterIp() {
		return registerIp;
	}

	public void setRegisterIp(String registerIp) {
		this.registerIp = registerIp;
	}

	public String getRegisterDeviceNo() {
		return registerDeviceNo;
	}

	public void setRegisterDeviceNo(String registerDeviceNo) {
		this.registerDeviceNo = registerDeviceNo;
	}

	public String getRegisterOsVersion() {
		return registerOsVersion;
	}

	public void setRegisterOsVersion(String registerOsVersion) {
		this.registerOsVersion = registerOsVersion;
	}

	public String getRegisterWay() {
		return registerWay;
	}

	public void setRegisterWay(String registerWay) {
		this.registerWay = registerWay;
	}

	public Integer getLoginCount() {
		return loginCount;
	}

	public void setLoginCount(Integer loginCount) {
		this.loginCount = loginCount;
	}

	public Date getLastLoginDatetime() {
		return lastLoginDatetime;
	}

	public void setLastLoginDatetime(Date lastLoginDatetime) {
		this.lastLoginDatetime = lastLoginDatetime;
	}

	public String getLastLoginIp() {
		return lastLoginIp;
	}

	public void setLastLoginIp(String lastLoginIp) {
		this.lastLoginIp = lastLoginIp;
	}

	public String getLastLoginDeviceNo() {
		return lastLoginDeviceNo;
	}

	public void setLastLoginDeviceNo(String lastLoginDeviceNo) {
		this.lastLoginDeviceNo = lastLoginDeviceNo;
	}

	public Date getUpdateDatetime() {
		return updateDatetime;
	}

	public void setUpdateDatetime(Date updateDatetime) {
		this.updateDatetime = updateDatetime;
	}
}