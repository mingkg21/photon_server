package cc.ccplay.booster.base.dto.content.game;

import cc.ccplay.booster.base.model.content.GameVersion;
import cc.ccplay.booster.base.model.content.GameVersionPicture;

import java.io.Serializable;
import java.util.List;

public class EditGameVersionDto implements Serializable {

    private GameVersion gameVersion;

    private List<GameVersionPicture> pics;

    public GameVersion getGameVersion() {
        return gameVersion;
    }

    public void setGameVersion(GameVersion gameVersion) {
        this.gameVersion = gameVersion;
    }

    public List<GameVersionPicture> getPics() {
        return pics;
    }

    public void setPics(List<GameVersionPicture> pics) {
        this.pics = pics;
    }
}
