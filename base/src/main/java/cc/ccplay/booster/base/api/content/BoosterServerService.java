package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.SelectizeDto;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.BoosterServer;
import org.easyj.frame.jdbc.Page;

import java.util.List;

public interface BoosterServerService {

    public BoosterServer get(long id);

    public void updateStatus(long id,Status status);

    public void saveOrUpdate(BoosterServer boosterServer);

    public Page getPage(SystemParam param,String name,Status status);


    public List<SelectizeDto> getForSelectize(SystemParam param, String name);

    /**
     *  获取负载比较低的服务器
     * @param gameId
     * @return
     */
    public BoosterServer getLoadServer(long gameId);


    public void resetDefaultServer(long id);

}
