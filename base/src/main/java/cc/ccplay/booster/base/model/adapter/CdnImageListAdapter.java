package cc.ccplay.booster.base.model.adapter;

import com.alibaba.fastjson.JSONArray;
import org.easyj.frame.jdbc.adapter.AbstractAdapter;
import org.easyj.frame.util.StringUtil;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CdnImageListAdapter extends AbstractAdapter {

    @Override
    protected Object getColumnValue(Field field, ResultSet rs, String colName) throws SQLException, IllegalAccessException {
        Icon rule = field.getAnnotation(Icon.class);
        String ruleValue = rule != null ? rule.value() : null;

        String json = rs.getString(colName);
        if(StringUtil.isEmpty(json)){
            return null;
        }

        JSONArray images = null;
        try{
            images =  JSONArray.parseArray(json);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
        CdnImageList imageList = createImage(images,ruleValue);
        return imageList;
    }

    public static CdnImageList createImage(JSONArray images,String icon){
        if(images == null){
            return null;
        }
        CdnImageList imageList = new CdnImageList(images);
        imageList.setIcon(icon);
        int size = images.size();
        List<String> cdnImages = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            String url = images.getString(i);
            if(StringUtil.isEmpty(url)){
                cdnImages.add(null);
            }else if(url.startsWith("http://") || url.startsWith("https://")){
                cdnImages.add(url );
            }else{
                String cnd = CdnRoute.getInstance().getCndUrl();
                if(icon != null) {
                    cdnImages.add(cnd + url + "-" + icon);
                }else{
                    cdnImages.add(cnd + url );
                }
            }
        }
        imageList.setCdnImages(cdnImages);
        return imageList;
    }


    public Object getObjectValue(Field field, Method getMethod, Object obj) throws IllegalAccessException,InvocationTargetException {
        CdnImageList imageList = (CdnImageList) super.getObjectValue(field,getMethod,obj);
        if(imageList == null){
            return null;
        }
        JSONArray images = imageList.getImages();
        if(images == null){
            return "[]";
        }
        return images.toJSONString();
    }

    @Override
    public Object formatValue(Object val){
        if(val ==  null ){
            return null;
        }
        CdnImageList imageList = (CdnImageList)val;
        JSONArray images = imageList.getImages();
        if(images == null){
            return "[]";
        }
        return images.toJSONString();
    }
}
