package cc.ccplay.booster.base.enums;

public enum ComplaintType {

    PORNOGRAPHIC(1,"色情暴力"),
    LAW(2,"违法反动"),
    VIRUS(3,"携带病毒"),
    CHARGING(4,"恶意扣费"),
    BUG(5,"BUG闪退"),
    INSTALL_FAIL(6,"无法安装"),
    OTHER(ComplaintType.OTHER_VALUE,"其他")
    ;
    public static final int OTHER_VALUE = 99;


    private int value;

    private String name;

    private ComplaintType(int value,String name){
        this.value = value;
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public String getName() {
        return name;
    }


    public static ComplaintType getByValue(int value){
        ComplaintType [] cts = ComplaintType.values();
        for (int i = 0; i < cts.length; i++) {
            ComplaintType ct = cts[i];
            if(ct.value == value){
                return ct;
            }
        }
        return null;
    }

}
