package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="crawl_game")
public class CrawlGame implements Serializable{

	@Id
	@Column(name = "game_id")
	private Long gameId;


	@Column(name = "package_name")
	private String packageName;

	@Column(name = "version_code")
	private Long versionCode;

	@Column(name = "version_name")
	private String versionName;

	@NowDateValue(update = true,insert = true)
	@Column(name = "update_time")
	private Date updateTime;



	public Long getGameId(){
		return this.gameId;
	}

	public void setGameId(Long gameId){
		this.gameId = gameId;
	}

	public String getPackageName(){
		return this.packageName;
	}

	public void setPackageName(String packageName){
		this.packageName = packageName;
	}

	public Long getVersionCode(){
		return this.versionCode;
	}

	public void setVersionCode(Long versionCode){
		this.versionCode = versionCode;
	}

	public String getVersionName(){
		return this.versionName;
	}

	public void setVersionName(String versionName){
		this.versionName = versionName;
	}

	public Date getUpdateTime(){
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime){
		this.updateTime = updateTime;
	}


}

