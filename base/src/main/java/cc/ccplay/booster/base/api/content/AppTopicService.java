package cc.ccplay.booster.base.api.content;

import cc.ccplay.booster.base.dto.content.AppTopicDto;
import cc.ccplay.booster.base.model.content.AppTopic;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.enums.Status;
import org.easyj.frame.jdbc.Page;

public interface AppTopicService {

    public void saveOrUpdate(AppTopic appTopic);

    public void updateStatus(long id,Status status);

    public Page getPage(SystemParam param);

    public Page<AppTopicDto> getDtoPage(SystemParam param);

    public AppTopic get(long id);

}
