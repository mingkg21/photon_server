package cc.ccplay.booster.base.http;

import org.easyj.frame.exception.GenericException;

public class NetworkException extends GenericException {
	
	private Throwable cause;
	
	public String getErrorMessage(){
		return "网络异常:"+cause.getMessage();
	}
	
	public NetworkException(Throwable cause){
		this.cause = cause;
	}

	public Throwable getCause() {
		return cause;
	}

	public void setCause(Throwable cause) {
		this.cause = cause;
	}
	
}
