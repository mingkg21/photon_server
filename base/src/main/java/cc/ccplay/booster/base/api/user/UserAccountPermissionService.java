package cc.ccplay.booster.base.api.user;

import cc.ccplay.booster.base.dto.user.UserAccountPermissionDto;
import cc.ccplay.booster.base.model.user.UserAccountPermission;

public interface UserAccountPermissionService {

    public void saveOrUpdate(UserAccountPermission uap);

    public boolean hasGameCommentPermission(long userId);

    public boolean hasSaveRecordCommentPermission(long userId);

    public UserAccountPermissionDto getDto(long userId);

}
