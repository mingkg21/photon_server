package cc.ccplay.booster.base.model.log;

import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="booster_login_log")
public class BoosterLoginLog implements Serializable{

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;


	@Column(name = "user_id")
	private Long userId;

	@Column(name = "server_id")
	private Long serverId;

	@Column(name = "status")
	private Long status;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@Column(name = "game_id")
	private Long gameId;

	public Long getGameId() {
		return gameId;
	}

	public void setGameId(Long gameId) {
		this.gameId = gameId;
	}

	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getUserId(){
		return this.userId;
	}

	public void setUserId(Long userId){
		this.userId = userId;
	}

	public Long getServerId(){
		return this.serverId;
	}

	public void setServerId(Long serverId){
		this.serverId = serverId;
	}

	public Long getStatus(){
		return this.status;
	}

	public void setStatus(Long status){
		this.status = status;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}


}

