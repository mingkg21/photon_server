package cc.ccplay.booster.base.dto.content.game;

import cc.ccplay.booster.base.model.content.GameVersion;
import cc.ccplay.booster.base.model.content.GameInfo;
import org.apache.commons.beanutils.BeanUtils;
import org.easyj.frame.util.StringUtil;

import java.lang.reflect.InvocationTargetException;

public class GameExtendDto extends GameInfo implements java.io.Serializable {

    public GameExtendDto(){}
    private GameVersion versionInfo;
    //是否已收藏
    private Boolean follow;

    private String pkgType;


    public GameExtendDto(GameInfo gameInfo,GameVersion versionInfo){
        this.versionInfo = versionInfo;
        if(versionInfo != null){
            String url = versionInfo.getDownloadUrl();
            if(StringUtil.isNotEmpty(url)) {
                int index = url.lastIndexOf(".");
                if (index > -1) {
                    pkgType = url.substring(index + 1);
                }
            }
        }
        try {
            BeanUtils.copyProperties(this,gameInfo);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public Boolean getFollow() {
        return follow;
    }

    public String getPkgType() {
        return pkgType;
    }

    public void setPkgType(String pkgType) {
        this.pkgType = pkgType;
    }

    public GameVersion getVersionInfo() {
        return versionInfo;
    }

    public void setVersionInfo(GameVersion versionInfo) {
        this.versionInfo = versionInfo;
    }

    public Boolean isFollow() {
        return follow;
    }

    public void setFollow(Boolean follow) {
        this.follow = follow;
    }
}
