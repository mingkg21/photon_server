package cc.ccplay.booster.base.model.content;

import cc.ccplay.booster.base.model.adapter.CdnImageList;
import org.easyj.frame.jdbc.annotation.value.LongValue;
import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import com.alibaba.fastjson.annotation.JSONField;
import java.util.Date;
import java.io.Serializable;

@Table(name="save_record_comment")
public class SaveRecordComment implements Serializable{

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;


	@Column(name = "content")
	private String content;

	@Column(name = "user_id")
	private Long userId;

	@Column(name = "record_id")
	private Long recordId;

	@LongValue(insert = true,insertValue = 0)
	@Column(name = "reply_count")
	private Long replyCount;

	@LongValue(insert = true,insertValue = 0)
	@Column(name = "praise_count")
	private Long praiseCount;

	@Column(name = "model_name")
	private String modelName;

	@Column(name = "parent_id")
	private Long parentId;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@Column(name = "be_replied_user_id")
	private Long beRepliedUserId;

	@Column(name = "official_content")
	private String officialContent;

	@JSONField(serialize = false)
	@LogicDelete("1")
	@Where(clause = "delete_flag=0")
	@LongValue(insert = true,insertValue = 0)
	@Column(name = "delete_flag")
	private Long deleteFlag;

	@Column(name = "os_version")
	private String osVersion;

	@Column(name = "imgs")
	private CdnImageList imgs;


	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public String getContent(){
		return this.content;
	}

	public void setContent(String content){
		this.content = content;
	}

	public Long getUserId(){
		return this.userId;
	}

	public void setUserId(Long userId){
		this.userId = userId;
	}

	public Long getRecordId(){
		return this.recordId;
	}

	public void setRecordId(Long recordId){
		this.recordId = recordId;
	}

	public Long getReplyCount(){
		return this.replyCount;
	}

	public void setReplyCount(Long replyCount){
		this.replyCount = replyCount;
	}

	public Long getPraiseCount(){
		return this.praiseCount;
	}

	public void setPraiseCount(Long praiseCount){
		this.praiseCount = praiseCount;
	}

	public String getModelName(){
		return this.modelName;
	}

	public void setModelName(String modelName){
		this.modelName = modelName;
	}

	public Long getParentId(){
		return this.parentId;
	}

	public void setParentId(Long parentId){
		this.parentId = parentId;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Long getBeRepliedUserId(){
		return this.beRepliedUserId;
	}

	public void setBeRepliedUserId(Long beRepliedUserId){
		this.beRepliedUserId = beRepliedUserId;
	}

	public String getOfficialContent(){
		return this.officialContent;
	}

	public void setOfficialContent(String officialContent){
		this.officialContent = officialContent;
	}

	public Long getDeleteFlag(){
		return this.deleteFlag;
	}

	public void setDeleteFlag(Long deleteFlag){
		this.deleteFlag = deleteFlag;
	}

	public String getOsVersion(){
		return this.osVersion;
	}

	public void setOsVersion(String osVersion){
		this.osVersion = osVersion;
	}

	public CdnImageList getImgs(){
		return this.imgs;
	}

	public void setImgs(CdnImageList imgs){
		this.imgs = imgs;
	}

}

