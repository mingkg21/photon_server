package cc.ccplay.booster.base.model.content;

import cc.ccplay.booster.base.model.adapter.CdnImage;
import org.easyj.frame.jdbc.annotation.*;

@Table(name="game_version_picture")
public class GameVersionPicture implements java.io.Serializable {

    @Id
    @GeneratedValue(type = GenerationType.AUTO)
    @Column(name="id")
    private Long id;

    @Column(name="version_id")
    private Long versionId;

    @Column(name="game_id")
    private Long gameId;

    @Column(name="src")
    private CdnImage src;

    @Column(name="width")
    private Long width;

    @Column(name="height")
    private Long height;

    @Column(name="file_size")
    private Long size;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersionId() {
        return versionId;
    }

    public void setVersionId(Long versionId) {
        this.versionId = versionId;
    }

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    public CdnImage getSrc() {
        return src;
    }

    public void setSrc(String src) {
        if(src == null){
            this.src = null;
        }else {
            this.src = new CdnImage(src);
        }
    }

    public void setSrc(CdnImage src) {
        this.src = src;
    }


    public Long getWidth() {
        return width;
    }

    public void setWidth(Long width) {
        this.width = width;
    }

    public Long getHeight() {
        return height;
    }

    public void setHeight(Long height) {
        this.height = height;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }
}
