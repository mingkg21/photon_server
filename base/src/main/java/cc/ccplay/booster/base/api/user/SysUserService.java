package cc.ccplay.booster.base.api.user;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.user.SysUser;
import org.easyj.frame.jdbc.Page;

public interface SysUserService {

    public SysUser getUser(SystemParam parameter, long userId);

    public int logicDelete(SystemParam parameter,long userId);

    public SysUser doLogin(SystemParam parameter, String account, String pwd);

    /**
     * 七牛上传工具 登录
     * @return
     */
    public SysUser doLoginForUploader(String account,String pwd,boolean useMd5);

    public Page getList(SystemParam parameter);

    public SysUser saveOrUpdate(SystemParam parameter,SysUser user);

    public void updatePassword(SystemParam parameter,long userId,String oldPassword,String newPassword);

    public void resetPassword(SystemParam parameter,long userId,String newPassword);

    public int updateStatus(SystemParam parameter,long userId,long status);

}
