package cc.ccplay.booster.base.dto.content.game;

import cc.ccplay.booster.base.model.content.GameBespeak;
import cc.ccplay.booster.base.model.content.GameTag;

import java.util.List;

public class GameBespeakDto extends AbstractGame implements java.io.Serializable {

    private GameBespeak bespeak;

    private List<GameTag> tags;

    public List<GameTag> getTags() {
        return tags;
    }

    public void setTags(List<GameTag> tags) {
        this.tags = tags;
    }

    public GameBespeak getBespeak() {
        return bespeak;
    }

    public void setBespeak(GameBespeak bespeak) {
        this.bespeak = bespeak;
    }

}
