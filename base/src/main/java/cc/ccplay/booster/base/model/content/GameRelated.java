package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="game_related")
public class GameRelated implements Serializable{

	@CompositeId
	private Id id;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	@Column(name = "ordering")
	private Long ordering;



	public Id getId(){
		return id;
	}

	public void setId(Id id){
		this.id = id;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Long getOrdering(){
		return this.ordering;
	}

	public void setOrdering(Long ordering){
		this.ordering = ordering;
	}


	public static class Id implements Serializable {

		public Id(){}

		public Id(Long gameId,Long relatedGameId){
			this.gameId = gameId;
			this.relatedGameId = relatedGameId;
		}

		@Column(name = "game_id")
		private Long gameId;

		@Column(name = "related_game_id")
		private Long relatedGameId;


		public Long getGameId(){
			return this.gameId;
		}

		public void setGameId(Long gameId){
			this.gameId = gameId;
		}

		public Long getRelatedGameId(){
			return this.relatedGameId;
		}

		public void setRelatedGameId(Long relatedGameId){
			this.relatedGameId = relatedGameId;
		}

	}
}

