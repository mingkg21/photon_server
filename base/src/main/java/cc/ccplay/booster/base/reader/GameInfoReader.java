package cc.ccplay.booster.base.reader;

import cc.ccplay.booster.base.api.content.GameInfoService;
import cc.ccplay.booster.base.reader.dto.BaseGameDto;
import com.alibaba.dubbo.config.annotation.Reference;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class GameInfoReader {

    @Reference
    public static GameInfoService gameService;


    public List<BaseGameDto> getGameInfos(Set<Long> gameIds){
        List<BaseGameDto> gameInfos = gameService.getGameInfos(gameIds);
        return gameInfos;
    }

    public void readAnSetGameInfo(Page<Map> page,String gameIdKey){
        readAnSetGameInfo(page.getList(),gameIdKey,"gameInfo");
    }

    public void readAnSetGameInfo(List<Map> data,String gameIdKey){
        readAnSetGameInfo(data,gameIdKey,"gameInfo");
    }

    public void readAnSetGameInfo(List<Map> data,String gameIdKey,String gameInfoKey){
        int size = data.size();
        Set<Long> gameIds = new HashSet<>();
        for (int i = 0; i < size; i++) {
            Map map = data.get(i);
            long gameId = StringUtil.toLong(map.get(gameIdKey));
            if(gameId != 0){
                gameIds.add(gameId);
            }
        }
        List<BaseGameDto> gameInfos = getGameInfos(gameIds);
        int total = gameInfos.size();
        if(size == 0){
            return;
        }
        HashMap<Long,BaseGameDto> gameInfoMap = new HashMap<>();
        for (int i = 0; i < total; i++) {
            BaseGameDto dto = gameInfos.get(i);
            gameInfoMap.put(dto.getId(),dto);
        }

        for (int i = 0; i < size ; i++) {
            Map map = data.get(i);
            long gameId = StringUtil.toLong(map.get(gameIdKey));
            map.put(gameInfoKey,gameInfoMap.get(gameId));
        }
    }

}
