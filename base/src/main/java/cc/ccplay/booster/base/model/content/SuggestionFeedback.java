package cc.ccplay.booster.base.model.content;

import org.easyj.frame.jdbc.annotation.value.NowDateValue;
import org.easyj.frame.jdbc.annotation.*;
import java.util.Date;
import java.io.Serializable;

@Table(name="suggestion_feedback")
public class SuggestionFeedback implements Serializable{

	//加速问题
	public static final long FEEDBACK_TYPE_BOOSTER = 1;
	//游戏问题
	public static final long FEEDBACK_TYPE_GAME = 2;
	//其他问题
	public static final long FEEDBACK_TYPE_OTHER = 3;

	@Id
	@GeneratedValue(type = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;


	@Column(name = "user_id")
	private Long userId;

	@Column(name = "content")
	private String content;

	@Column(name = "feedback_type")
	private Long feedbackType;

	@NowDateValue(insert = true)
	@Column(name = "create_time")
	private Date createTime;

	/**
	 是否是官方回复
	 */
	@Column(name = "is_reply")
	private Long isReply;




	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getUserId(){
		return this.userId;
	}

	public void setUserId(Long userId){
		this.userId = userId;
	}

	public String getContent(){
		return this.content;
	}

	public void setContent(String content){
		this.content = content;
	}

	public Long getFeedbackType(){
		return this.feedbackType;
	}

	public void setFeedbackType(Long feedbackType){
		this.feedbackType = feedbackType;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Long getIsReply(){
		return this.isReply;
	}

	public void setIsReply(Long isReply){
		this.isReply = isReply;
	}


}

