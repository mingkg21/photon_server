package cc.ccplay.booster.www.web;

import cc.ccplay.booster.base.annotation.NotNeedLogin;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class IndexController extends BaseWwwController{


    @RequestMapping({"","/","/index"})
    public String index(){
        return "/index";
    }

    @RequestMapping("/about")
    public String about(){
        return "/about/about";
    }


    @RequestMapping("/join")
    public String join(){
        return "/about/join";
    }


    @RequestMapping("/family")
    public String family(){
        return "/about/family";
    }


    @RequestMapping("/fangchenmi")
    public String fangchenmi(){
        return "/about/fangchenmi";
    }

    @RequestMapping("/law")
    public String law(){
        return "/about/law";
    }

    @RequestMapping("/vision")
    public String vision(){
        return "/about/vision";
    }

    @RequestMapping("/contact")
    public String contact(){
        return "/about/contact";
    }

    @RequestMapping("/404")
    public String pageNotFound(HttpServletRequest request, HttpServletResponse response){
        return "/error/404";
    }

}
