package cc.ccplay.booster.www.web;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.util.UserUtil;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.ActionUtil;

public class BaseWwwController extends ActionUtil{

    public SystemParam getSystemParam(){
        SystemParam param = (SystemParam)UserUtil.getCustomData();
        return param;
    }

    public static void renderJson(Page page){
        renderJson(page.toJsonStr());
    }
}
