package cc.ccplay.booster.www.web;

import org.easyj.frame.validation.annotation.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;


@Controller
@RequestMapping("/game")
public class GameController extends BaseWwwController {


    @Value("${domain.wap}")
    private String wapDoamin;



    @RequestMapping("/detail/{gameId}")
    public String detail(HttpServletRequest request, @PathVariable long gameId){
        request.setAttribute("gameId",gameId);
        request.setAttribute("wapDoamin",wapDoamin);
        return "/game/detail";
    }

    @RequestMapping("/listByTag/{tagId}")
    public String listByTag(HttpServletRequest request, @NotNull String tagName, @PathVariable long tagId){
        request.setAttribute("wapDoamin",wapDoamin);
        request.setAttribute("tagName",tagName);
        request.setAttribute("tagId",tagId);
        return "/game/list_by_tag";
    }
}
