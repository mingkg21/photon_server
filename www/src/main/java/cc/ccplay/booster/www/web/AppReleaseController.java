package cc.ccplay.booster.www.web;


import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.annotation.NoSignature;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.api.content.AppReleaseService;
import cc.ccplay.booster.base.model.adapter.CdnSource;
import cc.ccplay.booster.base.model.content.AppRelease;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;

@Controller
public class AppReleaseController extends BaseWwwController {

    @Reference
    private AppReleaseService appReleaseService;

    @NotNeedLogin
    @NoSignature
    @RequestMapping("/download")
    public void download(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String userAgent = request.getHeader("user-agent").toLowerCase();
        if(userAgent.indexOf("micromessenger")>-1){//微信客户端
            response.sendRedirect(response.encodeRedirectURL("http://sj.qq.com/myapp/detail.htm?apkName=com.gamebox.shiba"));
        }

        AppRelease release = appReleaseService.getLastestVersion();
        if(release == null){
            response.setStatus(404);
            super.renderJson("{\"error\": \"Document not found\"}");
            return;
        }

        CdnSource source = release.getDownloadUrl();
        if(source == null){
            response.setStatus(404);
            super.renderJson("{\"error\": \"Document not found\"}");
            return;
        }
        String url = source.getCndSrc();
        String downloadUrl = URLEncoder.encode(url, "utf-8").replaceAll("\\+", "%20");
        // 编码之后的路径中的“/”也变成编码的东西了 所有还有将其替换回来 这样才是完整的路径
        downloadUrl = downloadUrl.replaceAll("%3A", ":").replaceAll("%2F", "/");
        response.sendRedirect(response.encodeRedirectURL(downloadUrl));
    }

}
