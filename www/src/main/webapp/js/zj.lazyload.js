;(function($){
	$.fn.lazyLoad = function(options){
		var elements = this;
		var defaults = {
			threshold : 0,	// 距离图片位置
			placeholder: 'data:image/gif;base64,R0lGODlhAQABAIAAAMLCwgAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==',
			aboveTop: 0,
			scrollTop: 0
		}
		var opt = $.extend(defaults, options);
		function imgLoad(){
			elements.each(function(){
				var $this = $(this);
				this.loaded = false;
				var top = $.aboveTopFunc($this, opt);
				var topScroll = $.isScroll(opt);
				var winHei = window.innerHeight ? window.innerHeight : $(window).height();

				this.loaded = (top - topScroll - opt.threshold) > winHei ? false : true;
				
				if (this.loaded) {
					$this.attr('src', $this.attr('data-src'));
				};
			})	
		}
		imgLoad();
		$(window).on("scroll", function(){
			$.throttle(imgLoad,window);
		})
	}

	$.aboveTopFunc = function(elements, opts){
		opts.aboveTop = elements.offset().top;
		return opts.aboveTop;
	}

	$.isScroll = function(opts){
		opts.scrollTop = $(window).scrollTop();
		return opts.scrollTop;
	}

	$.throttle = function(method,context){	// 函数节流
        clearTimeout(method.tId);
        method.tId=setTimeout(function(){
            method.call(context);
        },300);
    }
})(jQuery)

// $(".test").lazyLoad({
// 	threshold : 0
// });