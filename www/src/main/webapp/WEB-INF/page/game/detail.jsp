<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/9/13
  Time: 11:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="keywords" content="">
    <meta name="description" content=" ">
    <meta name="viewport" content="width=device-width,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no,minimal-ui">
    <meta content="yes" name="apple-mobile-web-app-capable" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <meta content="telephone=no" name="format-detection" />
    <title>游戏详情</title>
    <link rel="stylesheet" href="${base}/css/swiper.min.css">
    <link rel="stylesheet" href="${base}/css/basic.css?v=1">
</head>
<body>
<div class="bg"></div>

<div class="header">
    <div class="row clearfix">
        <h1 class="logo fl">
            <a href="#">
                <img src="${base}/images/logo.png" alt="">
            </a>
        </h1>

        <div class="fr h-nav">
            <a href="${base}/about" class="link-about fl">关于我们</a>
            <a href="${base}/download" class="btn-load fl">下载APP</a>
        </div>
    </div>
</div>

<div class="wrap">
    <iframe src="${wapDoamin}/game/detail/${gameId}" frameborder="0" width="100%"></iframe>
</div>

<div class="share share-l" id="shareLeft">
    <a href="https://weibo.com/6637298319/manage" class="share-sina" target="_blank"><i class="icon icon-sina"></i>关注微博</a>
    <p class="share-txt">扫码关注微信  </p>
    <img src="${base}/images/qrcode.png" alt="" class="share-pic">
</div>

<div class="share share-r" id="shareRight">
    <a href="https://weibo.com/6637298319/manage" class="share-sina" target="_blank"><i class="icon icon-sina"></i>关注微博</a>
    <p class="share-txt">扫码关注微信  </p>
    <img src="${base}/images/qrcode.png" alt="" class="share-pic">
</div>

<script src="${base}/js/zepto.min.js"></script>
<script src="${base}/js/swiper.min.js"></script>
</body>
<script>
    $(".wrap").height($(window).height() - 120);
    var type = 1;
    if (type == 1) {
        $(".sp").hide();
        $(".hp").show();
        // 横屏轮播图
        var mySwiper = new Swiper ('.swiper-container', {
            loop: true,
            autoplay: 30000,
            slidesPerView: 1.1,
            spaceBetween: 10,
            paginationClickable :true,
            autoplayDisableOnInteraction: false,
            preloadImages: true
        })
    } else {
        $(".sp").show();
        $(".hp").hide();
        // 竖屏轮播图
        var mySwiper = new Swiper ('.swiper-container', {
            // loop: true,
            autoplay: 1000,
            slidesPerView: 1.75,
            spaceBetween: 18,
            paginationClickable :true,
            autoplayDisableOnInteraction: false,
            preloadImages: true
        })
    }

    ;(function(){
        $(".game-info-txt").each(function(){
            var lineHeight = parseInt($(this).find('pre').css("line-height"));
            var h = parseInt($(this).find('pre').height());
            console.log(h / lineHeight);
            if (h / lineHeight > 3) {
                $(this).find(".btn-open").show();
            } else {
                $(this).find(".btn-open").hide();
            }
        })

    })()



    $(".btn-open").on("click", function(){
        $(this).parents(".game-info-txt").removeClass("three-overflow");
        $(this).remove();
    })
</script>
</html>

