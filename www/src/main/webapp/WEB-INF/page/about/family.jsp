<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="keywords" content="25258">
	<meta name="description" content="25258 ">
	<title>18游戏盒-家长监护工程</title>	
	<link rel="stylesheet" href="${base}/css/all.min.css?v=20180828">
	<link rel="stylesheet" href="${base}/css/common.css">
	<!-- <link rel="shortcut icon" href="images/favicon.ico"> -->
</head>
<body>
	<div class="wrap">
		<div class="header">
			<div class="row clearfix">
				<h1 class="logo fl">
					<a href="#">
						<img src="${base}/images/logo2.png" alt="">
					</a>
				</h1>

				<div class="fr h-nav">
					<a href="${base}/about" class="link-about fl">关于我们</a>
					<a href="${base}/download" class="btn-load fl">下载APP</a>
				</div>
			</div>
		</div> 

		
		<div class="container" style="padding: 12px 0 32px 0">
			<div class="bread_nav">
				<a href="${base}/index">首页</a> >
				<span>家长监护工程</span>
			</div>
			<div class="user">
				<div class="user_box clearfix">
					<div class="user_l l">
						<div class="user_tab">
							<ul>
								<li>
									<a href="${base}/about">关于我们</a>
								</li>
								<li>
									<a href="${base}/vision">发展愿景</a>
								</li>
								<li>
									<a href="${base}/contact">联系我们</a>
								</li>
								<li>
									<a href="${base}/join">加入我们</a>
								</li>
								<li>
									<a href="${base}/law">法律声明</a>
								</li>
								<li>
									<a href="${base}/fangchenmi">防沉迷系统</a>
								</li>
								<li class="tab_active">
									<a href="${base}/family">家长监护工程</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="user_r r">
						<div class="user_con">
							<div class="c_item_head clearfix">
								<h3 class="l">家长监护工程</h3>
							</div>

							<div class="c_item_body">
								<div class="company_banner">
									<h3>家长监护工程系统介绍</h3>
									<img src="images/pic_fam.jpg" alt="">
								</div>

								<div class="company_txt develop_txt">
									<p>家长监护工程充分考虑家长的实际需求，当家长发现自己的孩子玩游戏过于沉迷的时候，由家长提供合法的监护人资质证明、游戏名称账号、以及家长对于限制强度的愿望等信息，可对处于孩子游戏沉迷状态的账号采取几种限制措施，解决未成年人沉迷网游的不良现象，如限制孩子每天玩游戏的时间区间和长度，也可以限制只有周末才可以游戏，或者完全禁止。</p>
									<p>家长监护工程申请流程客服中心传真，传真号码：021-32575177. <a class="blue_txt" href="${base}/file/parental-controls.rar">服务申请表下载</a></p>
									<p><img src="${base}/images/pic_family.jpg" alt=""></p>
									<p>
										<span class="bold_txt">温馨提示</span><br>
										1、电话服务<br>
										监护人可以通过来电反馈，7*24小时，全年无休<br>
										2、接待服务<br>
										监护人可以直接上门进行申请，接待服务受理时间7*8小时，全年无休，接待地址：<br>
										3、传真服务<br>
										监护人可以通过传真发送相关信息，进行申请，传真24小时受理，传真号码：
									</p>
									<p><span class="bold_txt">家长监护工程常见问题：</span></p>
									<p>
										<span class="bold_txt">Q1："网络游戏未成年人家长监护机制工程"是什么？</span><br>
										"网络游戏未成年人家长监护工程"是一项由中华人民共和国文化部指导，旨在加强家长对未成年人参与网络游戏的监护，引导未成年人健康、绿色参与网络游戏，和谐家庭关系的社会性公益行动。它提供了一种切实可行的方法，一种家长实施监控的管道，使家长纠正部分未成年子女沉迷游戏的行为成为可能。该项社会公益行动充分反映了中国网络游戏行业高度的社会责任感，对未成年玩家合法权益的关注以及对用实际行动营造和谐社会的愿望。
									</p>
									<p>
										<span class="bold_txt">Q2：什么情况下需要申请该项服务呢？</span><br>
										在监护人发现被监护人有沉溺棋牌网络游戏的情况下，监护人可向18游戏盒公司申请发起未成年人家长监护机制。
									</p>
									<p>
										<span class="bold_txt">Q3：申请该项服务时需要提交哪些资料呢？如何提交呢？</span><br>
										申请人需通过传真或邮寄方式向我司提交《18游戏盒未成年人用户家长监控服务申请书》及其中所提及需提供的附件，包含：<br>
										附件1：申请人的身份证或户口本<br>
										附件2：被申请人的身份证或户口本<br>
										附件3：18游戏盒未成年人家长监护申请书<br>
										您在填写《18游戏盒未成年人用户家长监控服务申请书》时，请提供尽可能详尽、真实的玩家资料，以便帐号归属的判定。
									</p>
									<p>
										<span class="bold_txt">Q4：相关资料传真或寄往哪里呢？</span><br>
										请将相关资料邮寄至：（请在信封表面写上"家长监控服务申请书"的字样）<br>
										收件人：客服部 <br>
										地址：上海市普陀区中江路388号新城控股大厦B座5层 星翼空间 <br>
										邮编：200062 <br>
										客服电话：021-32575177	<br>
										传真电话：021-32575177
									</p>
									<p>
										<span class="bold_txt">Q5：提交资料后，后续处理流程怎样呢？</span><br>
										您提交相关申请资料后，我司客服人员将按照监护申请流程尽快处理您的申请，处理过程中可能需要对监护人和被监护人进行电话回访以便信息的判定，希望您的配合。
									</p>
									<p>
										<span class="bold_txt">Q6：判定帐号归属需要多久呢？</span><br>
										在判断申请材料完整的情况下，我司将联系疑似账号归属者，告知其在 15 个自然日内将按照监护人需求对其账号进行相关操作，并要求疑似账号归属者提供身份材料以便我司判定其与监护人监护关系； <br>
										若疑似账号归属者在 15 个自然日内不能提供有效身份证明或逾期提供，则默认为疑似账号归属者与被监护人身份相符。我司即按照监护人申请要求，对其游戏账号纳入防沉迷系统；  <br>
										若疑似账号归属者在 15 个自然日内提供的身份证明与被监护人相符，我司即按照监护人申请要求，对其游戏账号纳入防沉迷系统； <br>
										若疑似账号归属者在 15 个自然日内提供的身份证明与被监护人不符，我司则无法判定其与被监护人的身份关系。在此情况下，为保障用户游戏账号安全，我司将通知监护人通过公安机关出具账号找回协查证明，由我司协助被监护人找回游戏账号后再进行后续操作；
									</p>
									<p>
										<span class="bold_txt">Q7:若发现对帐号归属有疑义或账号被恶意防沉迷限制怎么办？</span><br>
										您需要在收到通知后15个自然日内提供您的身份证复印件，并通过以下地址邮寄或传真到我司，如果我司在15个自然日内未收到您的身份证复印件，我们会将您的账号纳入家长监控系统。<br>
										注1：请在身份证复印件上写明您的签名。<br>
										注2：请您确认客服官方邮箱地址为：swjs@#   ，以免受骗上当。<br>
										注3：请您注意我司提供该项服务不收取任何费用，以免上当受骗。
									</p>
									<p>
										<span class="bold_txt">温馨提示：</span><br>
										您在邮寄或传真申请书时，要记得一起提供如下资料： <br>
										附件1：<a href="${base}/file/附件一监护人信息表.doc" class="blue_txt">申请人的身份证或户口本（复印件）</a> <br>
										附件2：<a href="${base}/file/附件二被监护人信息表.doc" class="blue_txt">被申请人的身份证或户口本 （复印件） </a> <br>
										附件3：<a href="${base}/file/附件三网络游戏未成年人家长监护申请书.doc" class="blue_txt">18游戏盒未成年人家长监护申请书 （复印件）</a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<jsp:include page="/WEB-INF/include/footer.jsp"></jsp:include>
	</div>

	<script src="${base}/js/jQuery.js"></script>
	<script src="${base}/js/common.js"></script>
</body>
<script>
	$(function(){
		$(".qq").hover(function(){
			$(this).addClass("qq-active");
		}, function(){
			$(this).removeClass("qq-active");
		})

		$(".wx").hover(function(){
			$(this).addClass("wx-active");
		}, function(){
			$(this).removeClass("wx-active");
		})
	})
</script>
</html>