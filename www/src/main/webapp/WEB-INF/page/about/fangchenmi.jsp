<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="keywords" content="25258">
	<meta name="description" content="25258 ">
	<title>18游戏盒-防沉迷系统</title>	
	<link rel="stylesheet" href="${base}/css/all.min.css?v=20180828">
	<link rel="stylesheet" href="${base}/css/common.css">
	<!-- <link rel="shortcut icon" href="images/favicon.ico"> -->
</head>
<body>
	<div class="wrap">
		<div class="header">
			<div class="row clearfix">
				<h1 class="logo fl">
					<a href="#">
						<img src="${base}/images/logo2.png" alt="">
					</a>
				</h1>

				<div class="fr h-nav">
					<a href="${base}/about" class="link-about fl">关于我们</a>
					<a href="${base}/download" class="btn-load fl">下载APP</a>
				</div>
			</div>
		</div> 

		
		<div class="container" style="padding: 12px 0 32px 0">
			<div class="bread_nav">
				<a href="${base}/index">首页</a> >
				<span>防沉迷系统</span>
			</div>
			<div class="user">
				<div class="user_box clearfix">
					<div class="user_l l">
						<div class="user_tab">
							<ul>
								<li>
									<a href="${base}/about">关于我们</a>
								</li>
								<li>
									<a href="${base}/vision">发展愿景</a>
								</li>
								<li>
									<a href="${base}/contact">联系我们</a>
								</li>
								<li>
									<a href="${base}/join">加入我们</a>
								</li>
								<li>
									<a href="${base}/law">法律声明</a>
								</li>
								<li   class="tab_active">
									<a href="${base}/fangchenmi">防沉迷系统</a>
								</li>
								<li>
									<a href="${base}/family">家长监护工程</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="user_r r">
						<div class="user_con">
							<div class="c_item_head clearfix">
								<h3 class="l">防沉迷系统</h3>
							</div>

							<div class="c_item_body">
								<div class="company_banner">
									<h3>什么是实名注册和防沉迷系统？</h3>
									<img src="${base}/images/pic_fcm.jpg" alt="">
								</div>

								<div class="company_txt develop_txt">
									<p>1、根据2010年8月1日实施的《网络游戏管理暂行办法》，网络游戏用户需使用有效身份证件进行实名注册。为保证流畅游戏体验，享受健康游戏生活，请广大18游戏盒的玩家尽快实名注册。</p>
									<p>
										2、保护未成年人身心健康，<strong class="bold_txt">未满18岁的用户</strong>将受到防沉迷系统的限制：<br>
										游戏过程，会提示您的累计在线时间。<br>
										累计游戏时间超过3小时，游戏收益（经验，金钱）减半。<br>
										累计游戏时间超过5小时，游戏收益为0。
									</p>
									<p><strong class="bold_txt">18游戏盒用户个人信息及隐私保护政策</strong></p>
									<p><strong class="bold_txt">重要须知：为了贯彻执行文化部颁布的《网络游戏管理暂行办法》以及《关于贯彻实施<网络游戏管理暂行办法>的通知》，厦门普罗米修斯有限公司（以下称“18游戏盒”）特此制定本《18游戏盒用户个人信息及隐私保护政策》。</strong></p>
									<p><strong class="bold_txt">游范儿在此特别提醒用户仔细阅读本《18游戏盒用户个人信息及隐私保护政策》中的各个条款（未成年人应当在其法定监护人陪同下阅读），并选择接受或者不接受本《18游戏盒用户个人信息及隐私保护政策》。</strong></p>
									<p>1、用户同意：个人隐私信息是指那些能够对用户进行个人辨识或涉及个人通信的信息，包括下列信息：用户的姓名，有效身份证件号码，家庭地址、电话号码，IP地址，电子邮件地址信息。而非个人隐私信息是指用户对本软件的操作状态以及使用习惯等一些明确且客观反映在18游戏盒服务器端的基本记录信息和其他一切个人隐私信息范围外的普通信息。</p>
									<p>
										2、一般而言，18游戏盒公司基于下列原因需要使用到用户的信息资源：<br>
										（1） 执行软件验证服务。<br>
										（2）执行软件升级服务。<br>
										（3）网络同步服务。<br>
										（4） 提高用户的使用安全性并提供客户支持。<br>
										（5）因用户使用本软件特定功能或因用户要求游范儿或合作单位提供特定服务时，18游戏盒或合作单位则需要把用户的信息提供给与此相关联的第三方。<br>
										（6）执行18游戏盒的《隐私权声明》,用户可访问18游戏盒网站查阅该声明。<br>
										（7）其他有利于用户和18游戏盒利益的。
									</p>
									<p>
										3、18游戏盒注重对用户信息资源的保护，会使用各种安全技术和程序来保护用户信息资源不被未经授权的访问、使用和泄漏。除法律或政府要求或用户同意等原因外，18游戏盒未经用户同意不向除合作单位以外的第三方公开、 透露用户信息资源。 但因下列原因而披露给第三方的除外：<br>
										（1）基于国家法律法规的规定而对外披露；<br>
										（2）应国家司法机关及其他有关机关基于法定程序的要求而披露；<br>
										（3）为保护18游戏盒或您的合法权益而披露；<br>
										（4）在紧急情况下，为保护其他用户及第三方人身安全而披露；<br>
										（5）用户本人或用户监护人授权披露的；<br>
										（6）应用户监护人合法要求而提供用户个人身份信息时。
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<jsp:include page="/WEB-INF/include/footer.jsp"></jsp:include>
	</div>

	<script src="${base}/js/jQuery.js"></script>
	<script src="${base}/js/common.js"></script>
</body>
<script>
	$(function(){
		$(".qq").hover(function(){
			$(this).addClass("qq-active");
		}, function(){
			$(this).removeClass("qq-active");
		})

		$(".wx").hover(function(){
			$(this).addClass("wx-active");
		}, function(){
			$(this).removeClass("wx-active");
		})
	})
</script>
</html>