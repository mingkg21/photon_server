<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="keywords" content="25258">
	<meta name="description" content="25258 ">
	<title>18游戏盒-关于我们</title>	
	<link rel="stylesheet" href="${base}/css/all.min.css?v=20180828">
	<link rel="stylesheet" href="${base}/css/common.css">
	<!-- <link rel="shortcut icon" href="images/favicon.ico"> -->
</head>
<body>
	<div class="wrap">
		<div class="header">
			<div class="row clearfix">
				<h1 class="logo fl">
					<a href="#">
						<img src="${base}/images/logo2.png" alt="">
					</a>
				</h1>

				<div class="fr h-nav">
					<a href="${base}/about" class="link-about fl">关于我们</a>
					<a href="${base}/download" class="btn-load fl">下载APP</a>
				</div>
			</div>
		</div> 

		
		<div class="container" style="padding: 12px 0 32px 0">
			<div class="bread_nav">
				<a href="${base}/index">首页</a> >
				<span>关于我们</span>
			</div>
			<div class="user">
				<div class="user_box clearfix">
					<div class="user_l l">
						<div class="user_tab">
							<ul>
								<li class="tab_active">
									<a href="${base}/about">关于我们</a>
								</li>
								<li>
									<a href="${base}/vision">发展愿景</a>
								</li>
								<li>
									<a href="${base}/contact">联系我们</a>
								</li>
								<li>
									<a href="${base}/join">加入我们</a>
								</li>
								<li>
									<a href="${base}/law">法律声明</a>
								</li>
								<li>
									<a href="${base}/fangchenmi">防沉迷系统</a>
								</li>
								<li>
									<a href="${base}/family">家长监护工程</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="user_r r">
						<div class="user_con">
							<div class="c_item_head clearfix">
								<h3 class="l">关于我们</h3>
							</div>

							<div class="c_item_body">
								<div class="company_banner">
									<h3>18游戏盒</h3>
									<p>专注只玩精品游戏！传播众乐、创造价值。</p>
									<img src="${base}/images/pic_team.png" alt="">
								</div>

								<div class="company_txt">
									<p>18游戏盒是厦门普罗米修斯有限公司（Xiameng Puluomixiusi Technology Co.,Ltd）完全版权所有的手游产品及内容服务平台，18游戏盒致力于为广大智能手机用户提供手游下载、游戏资讯、游戏活动、游戏工具、道具交易 、攻略评测、玩家互动社区等全方位的移动娱乐服务。通过网站、PC助手、手机助手和论坛等产品服务于手游玩家，让广大游戏爱好者享受一键安装安卓大型游戏数据包、只玩精品游戏、随时随地下载精品游戏等快速、便捷的服务，免费玩当下最新、最好玩的精品游戏。</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<jsp:include page="/WEB-INF/include/footer.jsp"></jsp:include>
	</div>

	<script src="${base}/js/jQuery.js"></script>
	<script src="${base}/js/common.js"></script>
</body>
<script>
	$(function(){
		$(".qq").hover(function(){
			$(this).addClass("qq-active");
		}, function(){
			$(this).removeClass("qq-active");
		})

		$(".wx").hover(function(){
			$(this).addClass("wx-active");
		}, function(){
			$(this).removeClass("wx-active");
		})
	})
</script>
</html>