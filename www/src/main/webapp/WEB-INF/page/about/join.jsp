<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="keywords" content="25258">
	<meta name="description" content="25258 ">
	<title>18游戏盒-加入我们</title>	
	<link rel="stylesheet" href="${base}/css/all.min.css?v=20180828">
	<link rel="stylesheet" href="${base}/css/common.css">
	<!-- <link rel="shortcut icon" href="images/favicon.ico"> -->
</head>
<body>
	<div class="wrap">
		<div class="header">
			<div class="row clearfix">
				<h1 class="logo fl">
					<a href="#">
						<img src="${base}images/logo2.png" alt="">
					</a>
				</h1>

				<div class="fr h-nav">
					<a href="${base}/about" class="link-about fl">关于我们</a>
					<a href="${base}/download" class="btn-load fl">下载APP</a>
				</div>
			</div>
		</div> 
		
		<div class="container" style="padding: 12px 0 32px 0">
			<div class="bread_nav">
				<a href="${base}/index">首页</a> >
				<span>加入我们</span>
			</div>
			<div class="user">
				<div class="user_box clearfix">
					<div class="user_l l">
						<div class="user_tab">
							<ul>
								<li>
									<a href="${base}/about">关于我们</a>
								</li>
								<li>
									<a href="${base}/vision">发展愿景</a>
								</li>
								<li>
									<a href="${base}/contact">联系我们</a>
								</li>
								<li class="tab_active">
									<a href="${base}/join">加入我们</a>
								</li>
								<li>
									<a href="${base}/law">法律声明</a>
								</li>
								<li>
									<a href="${base}/fangchenmi">防沉迷系统</a>
								</li>
								<li>
									<a href="${base}/family">家长监护工程</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="user_r r">
						<div class="user_con">
							<div class="c_item_head clearfix">
								<h3 class="l">加入我们</h3>
							</div>

							<div class="c_item_body">
								<div class="company_banner">
									<h3>你将在挑战中迅速积累，展现价值。<br>提供富竞争力的条件，鼓励员工达致工作与生活的平衡。</h3>
									<img src="${base}/images/pic_join.png" alt="">
								</div>

								<div class="join">
									<div class="contact_part">
										<p class="contact_tit">
											<i class="icon icon_contact"></i>游戏平台运营总监
										</p>
										<p class="require">岗位要求：</p>
										<p>1、熟悉游戏行业，有过相关的成功运营经验；</p>
										<p>2、对游戏平台总体流量和收入负责；</p>
										<p>3、有较强的社交、沟通能力，思维清晰；</p>
										<p>4、有一定的商务推广资源；</p>
										<p>5、能制定比较完善的年度、季度、月度运营计划；</p>
										<p>6、熟悉主站、BBS包括WAP站点的付费、免费运营方式，提升站点的IP、UV、PV等重要指标；</p>
										<p>7、有过安卓市场，机锋市场，安智市场，应用汇，拇指玩等第三方应用商店运营者优先。</p>
										<p>注：岗位除基本3-5万的月薪外，另可针对流量绩效和收入绩效进行3-15个百分比的阶梯提成奖励以及期权奖励。简历投放：hr@#</p>
									</div>

									<div class="contact_part">
										<p class="contact_tit">
											<i class="icon icon_contact"></i>高级ios（object c/c++）工程师
										</p>
										<p class="require">岗位要求：</p>
										<p>1、负责手机应用ios平台客户端的软件开发；</p>
										<p>2、负责手机客户端与服务器端的通讯对接与开发；</p>
										<p>3、进行源代码的单元测试和质量控制；</p>
										<p>4、按照项目计划，在保质保量的前提下，按时完成开发任务；</p>
										<p>5、对 ios 平台开发技术进行研究,根据开发过程中的体验对产品提出改进建议。</p>
										<p class="require">任职要求：</p>
										<p>1、计算机相关专业本科及以上学历，1年以上ios 开发经验，熟练掌握ios手机应用软件开发技术；</p>
										<p>2、具备扎实的 C/C++ 、Objective C 编程基础，良好的编程习惯；</p>
										<p>3、熟悉HTTP协议，iPhoneSDK 、Xcode、Cocoa 相关技术开发及应用，具备独立完成项目开发的能力；</p>
										<p>4、熟悉MySQL 或 SQLite 数据库中的一种及相关数据库管理和维护工具；</p>
										<p>5、能独立进行ios平台app开发，熟悉object c内存管理和线程管理方式；</p>
										<p>6、已经拥有在“软件市场”发布程序者优先。</p>
										<p>简历投放：hr@#</p>
									</div>

									<div class="contact_part">
										<p class="contact_tit">
											<i class="icon icon_contact"></i>python/django后端工程师
										</p>
										<p class="require">岗位要求：</p>
										<p>1、负责公司旗下的管理平台开发，配合Android开发人员开发平台接口。</p>
										<p class="require">任职要求：</p>
										<p>1、3年以上WEB开发工作经验，1年以上Python开发，熟悉python并使用Django开发过一个项目以上；</p>
										<p>2、pythonic；</p>
										<p>3、必须掌握、善用UnitTest进行开发；</p>
										<p>4、必须熟悉使用Git进行代码版本管理，采用Git-Flow的工作流；</p>
										<p>5、必须熟悉并习惯在Linux/Unix下进行日常开发，了解Linux下的服务器级应用程序，如Nginx, MySQL/Postgresql, Redis, Sphinx等；</p>
										<p>6、熟悉MySQL或Postgresql, 能够编写DDL，DML；</p>
										<p>7、熟悉HTTP协议，了解RESTful；</p>
										<p>8、至少了解一些持续集成知识，要求使用过一个相关工具，如Jenkins、Travis-CI；</p>
										<p>9、对自动化测试有所了解，熟悉BDD/ATDD、TDD开发模式并有实际使用开发者优先考虑；</p>
										<p>10、了解敏捷开发并能够接受者优先考虑。</p>
										<p>简历投放：hr@#</p>
									</div>

									<div class="contact_part">
										<p class="contact_tit">
											<i class="icon icon_contact"></i>运营（网站编辑）
										</p>
										<p class="require">岗位要求：</p>
										<p>1、网站软件产品库的日常更新和维护，图文资讯的编辑，整理，更新；</p>
										<p>2、软件游戏资源的日常更新和维护，资源的测试、整理、打包、更新；</p>
										<p>3、配合运营目标，策划各种软件游戏专题以及运营活动并推动执行。</p>
										<p class="require">任职要求：</p>
										<p>1、有过相关编辑工作经验的优先；</p>
										<p>2、对安卓手机游戏/资源有一定了解，爱好手机游戏；</p>
										<p>3、具备基本的编辑能力，能熟练使用Office、Photoshop、Dreamweaver等软件；</p>
										<p>4、善于学习和接受新鲜事物，具有团队合作精神，承受一定压力的工作；</p>
										<p>5、工作严谨认真，责任心强，耐心细致，有较强的自学能力。</p>
										<p>简历投放：hr@#</p>
									</div>

									<div class="contact_part">
										<p class="contact_tit">
											<i class="icon icon_contact"></i>运营（论坛运营）
										</p>
										<p class="require">岗位要求：</p>
										<p>1、日常手游资源采集；</p>
										<p>2、论坛维护，论坛体制建设、管理；</p>
										<p>3、论坛活动制作，用户互动。</p>
										<p class="require">任职要求：</p>
										<p>1、有过移动互联网论坛运营经验；</p>
										<p>2、有过大型论坛超级版主或者更高级别职称；</p>
										<p>3、爱好移动互联网行业，热爱论坛社区工作；</p>
										<p>4、有丰富的网络用户活动策划能力，并有成功组织落地活动经验者优先；</p>
										<p>5、具有团队合作精神。</p>
										<p>简历投放：hr@#</p>
									</div>

									<div class="contact_part">
										<p class="contact_tit">
											<i class="icon icon_contact"></i>商务经理
										</p>
										<p class="require">岗位要求：</p>
										<p>1、根据公司网站战略及业务发展需求，寻找、挖掘有利于公司的合作资源，评估资源的可合作性；</p>
										<p>2、负责公司网站对外商务合作，拓展新合作渠道，探索新推广模式。通过合作为网站带来持续优质用户资源；</p>
										<p>3、渠道合作相关商务谈判及签约；建立并维护与重要渠道合作关系；深度跟踪挖掘渠道合作数据；</p>
										<p>4、负责和各种相关资源合作方、网站联络，洽谈合作，签订协议等；</p>
										<p>5、开发并整合更多更好的推广资源，熟悉网站推广流程，达成注册用户量和市场占有率等推广指标；</p>
										<p>6、负责跟进各活动项目的实施，形成定期报告提出改善建议。</p>
										<p class="require">任职要求：</p>
										<p>1、二年以上互联网商务BD拓展经验，擅长抓住客户需求，具有独立客户拓展及项目谈判能力；</p>
										<p>2、文字表达能力强，能独立完成活动方案需求梳理和撰写；</p>
										<p>3、拥有良好的沟通能力和团队协调能力，有积极解决各种难题的能力；</p>
										<p>4、具有较强的市场敏锐力、沟通谈判能力、团队管理能力与目标推进能力；</p>
										<p>5、工作中积极进取，擅长自我激励与激励他人，能承受快速发展型企业带来的工作压力；</p>
										<p>6、熟悉网站合作，有商务拓展和协调能力，具备商务文案策划能力；</p>
										<p>7、疯狂热爱和极度看好移动互联网前景的优先。</p>
										<p>简历投放：hr@#</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<jsp:include page="/WEB-INF/include/footer.jsp"></jsp:include>
	</div>

	<script src="${base}/js/jQuery.js"></script>
	<script src="${base}/js/common.js"></script>
</body>
<script>
	$(function(){
		$(".qq").hover(function(){
			$(this).addClass("qq-active");
		}, function(){
			$(this).removeClass("qq-active");
		})

		$(".wx").hover(function(){
			$(this).addClass("wx-active");
		}, function(){
			$(this).removeClass("wx-active");
		})
	})
</script>
</html>