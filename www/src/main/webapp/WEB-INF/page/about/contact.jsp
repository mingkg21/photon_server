<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="keywords" content="25258">
	<meta name="description" content="25258 ">
	<title>18游戏盒-联系我们</title>	
	<link rel="stylesheet" href="${base}/css/all.min.css?v=20180828">
	<link rel="stylesheet" href="${base}/css/common.css">
	<!-- <link rel="shortcut icon" href="images/favicon.ico"> -->
</head>
<body>
	<div class="wrap">
		<div class="header">
			<div class="row clearfix">
				<h1 class="logo fl">
					<a href="#">
						<img src="${base}/images/logo2.png" alt="">
					</a>
				</h1>

				<div class="fr h-nav">
					<a href="${base}/about" class="link-about fl">关于我们</a>
					<a href="${base}/download" class="btn-load fl">下载APP</a>
				</div>
			</div>
		</div> 

		
		<div class="container" style="padding: 12px 0 32px 0">
			<div class="bread_nav">
				<a href="${base}/index">首页</a> >
				<span>联系我们</span>
			</div>
			<div class="user">
				<div class="user_box clearfix">
					<div class="user_l l">
						<div class="user_tab">
							<ul>
								<li>
									<a href="${base}/about">关于我们</a>
								</li>
								<li>
									<a href="${base}/vision">发展愿景</a>
								</li>
								<li  class="tab_active">
									<a href="${base}/contact">联系我们</a>
								</li>
								<li>
									<a href="${base}/join">加入我们</a>
								</li>
								<li>
									<a href="${base}/law">法律声明</a>
								</li>
								<li>
									<a href="${base}/fangchenmi">防沉迷系统</a>
								</li>
								<li>
									<a href="${base}/family">家长监护工程</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="user_r r">
						<div class="user_con">
								<div class="c_item_head clearfix">
									<h3 class="l">联系我们</h3>
								</div>

								<div class="c_item_body">
									<div class="company_banner">
										<h3>在线咨询服务，<br>写邮件，我们将在24小时内主动联系您。</h3>
										<img src="${base}/images/pic_contact.png" alt="">
									</div>

									<div class="contact clearfix">
										<div class="l contact_container">
											<div class="contact_part">
												<p></p>
												<p>商务合作：274929653</p>
												<p>客服群：310822308</p>
												<p>投稿群：310822308</p>
											</div>
										</div>
									</div>
								</div>
							</div>
					</div>
				</div>
			</div>
		</div>

		<jsp:include page="/WEB-INF/include/footer.jsp"></jsp:include>
	</div>

	<script src="${base}/js/jQuery.js"></script>
	<script src="${base}/js/common.js"></script>
</body>
<script>
	$(function(){
		$(".qq").hover(function(){
			$(this).addClass("qq-active");
		}, function(){
			$(this).removeClass("qq-active");
		})

		$(".wx").hover(function(){
			$(this).addClass("wx-active");
		}, function(){
			$(this).removeClass("wx-active");
		})
	})
</script>
</html>