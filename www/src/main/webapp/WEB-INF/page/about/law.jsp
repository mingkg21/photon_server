<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="keywords" content="25258">
	<meta name="description" content="25258 ">
	<title>18游戏盒-法律声明</title>	
	<link rel="stylesheet" href="${base}/css/all.min.css?v=20180828">
	<link rel="stylesheet" href="${base}/css/common.css">
	<!-- <link rel="shortcut icon" href="images/favicon.ico"> -->
</head>
<body>
	<div class="wrap">
		<div class="header">
			<div class="row clearfix">
				<h1 class="logo fl">
					<a href="#">
						<img src="${base}/images/logo2.png" alt="">
					</a>
				</h1>

				<div class="fr h-nav">
					<a href="${base}/about" class="link-about fl">关于我们</a>
					<a href="${base}/download" class="btn-load fl">下载APP</a>
				</div>
			</div>
		</div> 

		
		<div class="container" style="padding: 12px 0 32px 0">
			<div class="bread_nav">
				<a href="${base}/index">首页</a> >
				<span>法律声明</span>
			</div>
			<div class="user">
				<div class="user_box clearfix">
					<div class="user_l l">
						<div class="user_tab">
							<ul>
								<li>
									<a href="${base}/about">关于我们</a>
								</li>
								<li>
									<a href="${base}/vision">发展愿景</a>
								</li>
								<li>
									<a href="${base}/contact">联系我们</a>
								</li>
								<li>
									<a href="${base}/join">加入我们</a>
								</li>
								<li  class="tab_active">
									<a href="${base}/law">法律声明</a>
								</li>
								<li>
									<a href="${base}/fangchenmi">防沉迷系统</a>
								</li>
								<li>
									<a href="${base}/family">家长监护工程</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="user_r r">
						<div class="user_con">
							<div class="c_item_head clearfix">
								<h3 class="l">法律声明</h3>
							</div>

							<div class="c_item_body">
								<div class="company_banner">
									<h3>如有侵犯您的版权请联系<br>我们将立刻更改或删除！</h3>
									<img src="${base}/images/pic_law.png" alt="">
								</div>

								<div class="company_txt develop_txt">
									<p>1、厦门普罗米修斯有限公司是18游戏盒（网）的所有者。</p>
									<p>2、用户理解并同意，18游戏盒电子公告服务（包括18游戏盒社区、论坛）仅为用户相互交流的一个平台。由用户上传发布提供的内容，内容所有权归版权所有人拥有。内容仅供学习使用，不得用于任何商业用途。18游戏盒本身对该服务负有监督和管理的义务，但对各用户在此发布和上传的内容的合法性以及由此引起的对第三方的责任不承担任何义务。各用户同意并保证在此发布的任何内容，包括言论和上传的任何内容没有侵犯任何第三人的合法权益。各用户同意在上传任何内容前尽力确保上传内容没有侵犯任何第三方的合法权益，并且，各用户同意因为自己所发表的言论或者上传的内容而给第三方和18游戏盒所造成的损失承担根据法律所应该承担的全部责任。 如果相关内容的当事人就内容的版权归属发生争议，相关当事人应向18游戏盒提供著作权权属证明，著作权人的身份证明和侵权情况证明等能确认内容权利归属的材料，一经确认，18游戏盒立即采取移除、停止发布等措施。如不能提供上述能够证明内容权属的证明材料，18游戏盒将则暂不采取包括移除、停止发布等相应的措施。</p>
									<p>3、除特别提示之外，游戏客户端下载均为免费，游戏运行当中出现连接网络或者下载场景，激活软件等操作，有可能被扣费。在出现上述情况之下，18游戏盒网不完全保证不被扣费。网络游戏是否收费，如何订购，退订，请与游戏运营商客服联系解决。</p>
									<p>4、除特别提示之外，18游戏盒目前提供的包括手机游戏、图片、铃声、手机电影和社区交流所有内容或者服务均为免费。用户下载内容时，只需要给相关移动运营商支付GPRS流量费用，不需要交纳其他任何费用。</p>
									<p>5、18游戏盒及其注册用户及本网页内的内容提供者拥有此网页内所有资料的版权。</p>
									<p>6、18游戏盒自行创作和提供的包括各种游戏、文章、图片、声音、文章等各种形式的内容，版权由厦门普罗米修斯有限公司自身拥有。</p>
									<p>7、18游戏盒网由内容提供商（个人）授权的提供内容，内容所有权归内容提供商（个人）拥有，18游戏盒拥有其内容在18游戏盒网的开发和发行的权力。由18游戏盒网内容版权产生的任何法律及经济责任，均由相关的内容提供商（个人）独立承担。</p>
									<p>8、严格遵守《中华人民共和国版权法》及《互联网出版管理规定》，18游戏盒网对于用户发布的内容所引发的版权、署名权的异议、纠纷不承担任何责任。传统媒体转载须事先与原作者联系。提交者发言纯属个人行为，与本网站立场无关。</p>
									<p>9、任何人非经18游戏盒网或其它相关权利人的授权，不可下载或转载本网站的服务内容。18游戏盒网可以在特定情况下，自主决定终止侵害他人知识产权或侵害本站相关利益的用户的帐号。</p>
									<p>10、18游戏盒网对于其开发的或和他人共同开发的所有内容和服务的全部知识产权拥有所有权或使用权，此等内容和服务受到适用的知识产权法律、版权、商标权、服务商标专利或其他专有权利法律的保护。</p>
									<p>11、凡侵害18游戏盒网相关版权的，受侵害人可依法追究其刑事责任。未经18游戏盒网的明确书面许可，任何人不得复制或在非18游戏盒网网站上做镜像。</p>
									<p>12、请勿擅自复制或采用18游戏盒网所创造的用以制成网页的HTML。18游戏盒网对其html享有著作权。同时18游戏盒网对其网址上的所有图标、图饰、图表、色彩、文字表述及 其组合、版面设计、数据库均享有完全的著作权，对发布的信息均享有专有的发布和使用权，未经18游戏盒网同意，不得擅自复制、使用或转载。</p>
									<p>13、18游戏盒网不保证为向用户提供便利而设置的外部链接的准确性和完整性。 如您在版权方面有问题想和18游戏盒网进行协商，请发联系我们 Email：feedback@# 18游戏盒将会积极配合您的维权行动。</p>
									<p>14、再次声明，本网站所有内容未经18游戏盒或者版权所有人许可，不得用于任何商业用途，如有侵犯您版权请联系我们，我们将立刻更改或删除 Email：feedback@#</p>
									<p>15、注册商标："18游戏盒"是厦门普罗米修斯有限公司的注册商标或实际品牌，未经厦门普罗米修斯有限公司的许可，他人不得使用。</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


		<jsp:include page="/WEB-INF/include/footer.jsp"></jsp:include>

	</div>

	<script src="${base}/js/jQuery.js"></script>
	<script src="${base}/js/common.js"></script>
</body>
<script>
	$(function(){
		$(".qq").hover(function(){
			$(this).addClass("qq-active");
		}, function(){
			$(this).removeClass("qq-active");
		})

		$(".wx").hover(function(){
			$(this).addClass("wx-active");
		}, function(){
			$(this).removeClass("wx-active");
		})
	})
</script>
</html>