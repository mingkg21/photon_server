<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>404</title>
	<link rel="stylesheet" href="http://app.18hanhua.com/css/all.min.css?v=20180828">
	<style>
		.f {
			position: absolute;
			top: 50%;
			left: 50%;
			margin: -200px 0 0 -300px;
			height: 400px;
			width: 600px;	
		}
		.f h1 {
			margin: 20px 0 12px;
			font-size: 22px;
			font-weight: bold;
			line-height: 1;
		}
		.f p {
			font-size: 18px;
			color: #666;
			line-height: 32px;
		}
		.f img {
			margin: 0 auto;
			width: 276px;
			height: 251px;
		}
		#time {
			font-weight: bold;
			color: #5aadf9;
		}
	</style>
</head>
<body>
	<div class="f text-center">
		<img src="${base}/images/404.png" alt="">
		<h1>嗷~，打不开了</h1>
		<p>别担心，18游戏盒推荐你先看看大家在玩的游戏吧，<br>将在<span id="time">10s</span>内帮你跳转至首页~</p>
	</div>
</body>
<script>
	var timer = null;
	var time = document.getElementById("time");
	var num = 10;
	timer = setInterval(function(){
		if (num > 0) {
			num--;
		} else {
			clearInterval(timer);
			window.location = "${base}" ? "${base}" : "/";
		}
		time.innerHTML = num + "s";
	}, 1000)
</script>
</html>