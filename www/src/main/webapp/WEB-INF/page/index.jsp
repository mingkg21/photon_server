<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="keywords" content="18游戏盒,18游戏盒app,18游戏盒下载,18游戏盒官方下载">
	<meta name="description" content="18游戏盒是一款分享精品游戏的app，每天分享时下热门新好游，抢先体验内测预约新游。18游戏盒app提供十八汉化组最新中文汉化游戏的免费下载。">
	<title>18游戏盒下载_18游戏盒官网_分享精品游戏</title>
	<link rel="stylesheet" href="${base}/css/all.min.css?v=20180831">
	<style>

	</style>
</head>
<body>
	<div class="wrap">
		<div class="header">
			<div class="row clearfix">
				<h1 class="logo fl">
					<a href="#">
						<img src="${base}/images/logo.png" alt="">
					</a>
				</h1>

				<div class="fr h-nav">
					<a href="${base}/about" class="link-about fl">关于我们</a>
					<a href="${base}/download" class="btn-load fl">下载APP</a>
				</div>
			</div>
		</div> 

		<div class="page page-one">
			<div class="row">
				<div class="page-load clearfix">

					<div class="qrcode">
						<img src="${base}/images/qrcode.png?v=20180907" alt="">
					</div>

					<div class="load-btn">
						<a href="${base}/download" class="btn-android">
							<i class="icon icon-android"></i>安卓版下载
						</a>
						<a href="#" class="btn-apple">
							<i class="icon icon-apple"></i>敬请期待...
						</a>
					</div>


				</div>
			</div>
		</div>

		<div class="page page-two"></div>

		<div class="page page-three"></div>

		<div class="page page-four">
			<a href="${base}/download" class="btn-android-bot">
				<i class="icon icon-android"></i>安卓版下载
			</a>
		</div>

		<jsp:include page="/WEB-INF/include/footer.jsp"></jsp:include>
	</div>
</body>
<script>
	
</script>
</html>