<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<div class="footer">
    <div class="container clearfix">
        <div class="clearfix">
            <div class="foot-link">
                <a href="${base}/about">关于我们</a>
                <a href="${base}/contact">商务合作</a>
                <a href="${base}/vision">发展愿景</a>
                <a href="${base}/fangchenmi">防沉迷游戏</a>
                <a href="${base}/family">家长监护工程</a>
                <a href="${base}/law">法律声明</a>
            </div>
        </div>

        <div class="copy_right">
            <p>Copyright  2018 Xiameng Puluomixiusi Technology Co.,Ltd All Rights Reserved. 厦门普罗米修斯 版权所有</p>
            <p>公司地址：厦门市思明区软件园二期望海路25号之二8楼    闽ICP备18001355号</p>
            <p>本游戏适合18岁以上的玩家进入 客服电话：0592-5911997    客服Q群：310822308</p>
            <p>抵制不良游戏&nbsp;&nbsp;拒绝盗版游戏&nbsp;&nbsp;注意自我保护&nbsp;&nbsp;谨防受骗上当&nbsp;&nbsp;适度游戏益脑&nbsp;&nbsp;沉迷游戏伤身&nbsp;&nbsp;合理安排时间&nbsp;&nbsp;享受健康生活</p>
        </div>
    </div>
</div>