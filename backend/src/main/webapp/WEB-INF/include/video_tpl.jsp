<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<script type="text/html" id="video_container_tpl">
    <table style="margin-top:5px;" border="0">
        <tbody>
        <tr>
            <td>
                <video height="250" controls="controls" src="{{src}}">
                    你的浏览器不支持video标签
                </video>
            </td>
            {{if hasFun}}
            <td style="vertical-align: top;">
                <a href="javascript:void(0);">
                    <span class="glyphicon glyphicon-trash" aria-hidden="true">
                    </span>
                </a>
            </td>
            {{/if}}
        </tr>
        </tbody>
    </table>
</script>


