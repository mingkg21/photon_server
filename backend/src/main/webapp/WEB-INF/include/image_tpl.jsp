<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<script type="text/html" id="image_container_tpl">
    {{each items as item i}}

    <div {{if !single}} id="{{item.dataId}}"{{/if}} style="margin-top:5px;height:{{height}}px;width:{{width+16}}px;float:left;">
        {{if item.json}}
        <input type="hidden" disabled name="image_data" data-value="{{item.json}}">
        {{/if}}
        <div style="height:{{height}}px;width:{{width}}px;float:left;">
            <a target="_blank" href="{{cdn}}{{item.src}}">
                <img width="{{width}}" height="{{height}}" src="{{item.cdnUrl}}">
            </a>
        </div>
        {{if !single || removeFn }}
        <div style="float: left">
            <a href="javascript:void(0);" {{if !single}} onclick="BaseUtil.removeImage('{{item.dataId}}')" {{/if}}>
                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
            </a>
        </div>
        {{/if}}
    </div>
    {{/each}}
</script>
