<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String tab1Id = java.util.UUID.randomUUID().toString();
    String tab2Id = java.util.UUID.randomUUID().toString();
%>
<ul class="nav nav-tabs">
    <li class="active">
        <a href="#<%=tab1Id%>" data-toggle="tab">
             文件库
        </a>
    </li>

    <li>
        <a href="#<%=tab2Id%>" data-toggle="tab">
            上传
        </a>
    </li>
</ul>

<div class="tab-content" style="height:100%">
    <div class="tab-pane fade in active" id="<%=tab1Id%>">
        <form class="fui-search table-tool" method="post">
            <div class="form-group fui-data-wrap">
                <div class="form-inline">

                    <div class="input-group">
                        <input type="text" class="form-control" name="account" value="${account}" placeholder="上传账号">
                    </div>

                    <button class="btn btn-primary fui-date-search">查询</button>
                </div>
            </div>
        </form>

        <table class="fui-default-table"></table>
    </div>
    <div class="tab-pane fade" id="<%=tab2Id%>">
        <br/>
        <button type="button" class="btn btn-primary upload-picture">上传文件</button>
        <button type="button" class="btn btn-danger upload-reset">清空</button>
        <div class="progress-container" style="width:600px;">

        </div>
    </div>
</div>


<script type="text/javascript">
    requirejs(['${base}/module/js/file/any-uploader.js?v=1.0.2'],function(obj){

        Fui.addBootstrapTable({
            url : '${base}/file/any/list',
//            queryParams : function(params){
//                params["parentId"] = parentId;
//                return params;
//            },
            columns : [ {
                field : 'fileName',
                title : '文件名',
                align : 'center',
                width : '100',
                valign : 'middle'
            },{
                field : 'hash',
                title : 'hash',
                align : 'center',
                width : '150',
                valign : 'middle'
            },{
                field : 'size',
                title : '大小',
                align : 'center',
                width : '100',
                valign : 'middle',
                formatter : Fui.formatFileSize
            },{
                field : 'account',
                title : '上传者',
                align : 'center',
                width : '100',
                valign : 'middle'
            },{
                field : 'createTime',
                title : '上传时间',
                align : 'center',
                width : '200',
                valign : 'middle',
                formatter :  Fui.formatDatetime
            },{
                title : '操作',
                align : 'center',
                width : '60',
                valign : 'middle',
                formatter : function(v,row){
                    var arr = [ '<a class="copy_link" href="javascript:void(0)" title="复制地址">' ,
                        '<i class="glyphicon glyphicon-file"></i>', '</a> '
                    ]
                    return arr.join("");
                },
                events:{
                    "click .copy_link":function(e,val,row){
                        Fui.copyText(baseInfo.cdn +row.src);
                    }
                }
            }]
        });

        var userId = "${loginUser.id}";
        var $div =  $("#<%=tab2Id%>");
        var containerId = createId();
        var $container = $div.find(".progress-container");
        var $uploadBtn = $div.find(".upload-picture");
        var $resetBtn = $div.find(".upload-reset");

        var btnId = createId();
        $uploadBtn.attr("id",btnId);
        $container.attr("id",containerId);
        var uploader = obj.getUploader(btnId,containerId,userId);

        $resetBtn.click(function(){
           // var layerLoadIndex=layer.load(2);
             layer.confirm("是否确认要清空",function(index){
                layer.close(index);
                uploader.destroy();
                 $container.html("");
                uploader = obj.getUploader(btnId,containerId,userId);
            },function(index){
                layer.close(index);
            })
        });
    });
</script>