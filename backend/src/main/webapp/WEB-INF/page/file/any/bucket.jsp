<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String tabId1 = java.util.UUID.randomUUID().toString();
    String tabId2 = java.util.UUID.randomUUID().toString();
    String picContainerId = java.util.UUID.randomUUID().toString();
    String showMeBtnId = java.util.UUID.randomUUID().toString();
    String showAllBtnId = java.util.UUID.randomUUID().toString();
%>

<div class="modal-dialog" style="width: 1000px;">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close fui-close" >&times;</button>
            <h4 class="modal-title">
                选择文件
            </h4>
        </div>

        <div class="modal-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#<%=tabId1%>" data-toggle="tab">
                        文件库
                    </a>
                </li>

                <li>
                    <a href="#<%=tabId2%>" data-toggle="tab">
                        上传
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane fade in active" id="<%=tabId1%>">
                    <div style="margin-top:5px;margin-left: 5px; ">
                        <button class="btn btn-primary" type="button" id="<%=showMeBtnId%>">只显示我的</button>
                        <button class="btn btn-default" type="button" id="<%=showAllBtnId%>">显示所有</button>
                    </div>
                    <div>
                        <table style="width:100%;" id="<%=picContainerId%>">

                        </table>
                    </div>
                </div>

                <div class="tab-pane fade" id="<%=tabId2%>">
                    <div style="margin-top: 5px;">
                        <button type="button" class="btn btn-primary upload-picture">上传文件</button>
                        <button type="button" class="btn btn-danger upload-reset">清空</button>
                    </div>
                    <div class="progress-container" style="width:600px;">

                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default fui-close">取消</button>
        </div>
    </div>
</div>

<script language="javascript">
    requirejs(["template",'${base}/module/js/file/any-uploader.js',"jquery","layer"],function(template,pic_uploader){
        var dataMap = {};
        var selectData = {};
        var picContainerId = "<%=picContainerId%>"
        var pageSize = 18;
        var showAll = false;

        $showMeBtn = $("#<%=showMeBtnId%>");
        $showAllBtn = $("#<%=showAllBtnId%>");

        $showMeBtn.click(function(){
            showAll = false;
            $showMeBtn.removeClass("btn-default");
            $showMeBtn.addClass("btn-primary");

            $showAllBtn.removeClass("btn-primary");
            $showAllBtn.addClass("btn-default");
            $("#"+picContainerId).bootstrapTable('refresh',{pageNumber:1,pageSize:pageSize})
        })
        $showAllBtn.click(function(){
            showAll = true;
            $showAllBtn.removeClass("btn-default");
            $showAllBtn.addClass("btn-primary");

            $showMeBtn.removeClass("btn-primary");
            $showMeBtn.addClass("btn-default");
            $("#"+picContainerId).bootstrapTable('refresh',{pageNumber:1,pageSize:pageSize})
        })

        function toCallbackData(data){
            var cd = {
                fileName:data.fileName,
                src : data.src,
                size: data.size,
                cdnSrc:baseInfo.cdn + data.src
            };
            return cd;
        }

        function callback(selData){
            var fn = Package.getCallback("${fnId}");
            if(fn){
               fn(toCallbackData(selData));
            }else{
                layer.msg("ERROR:找不到回调方法");
            }
        }
        var selectPic = function(data){
            callback(data);
        }


        Fui.addBootstrapTable({
            id:picContainerId,
            url : '${base}/file/any/list',
            queryParams : function(params){
                params["showAll"] = showAll;
                return params;
            },
            columns : [ {
                field : 'fileName',
                title : '文件名',
                align : 'center',
                width : '100',
                valign : 'middle'
            },{
                field : 'size',
                title : '大小',
                align : 'center',
                width : '50',
                valign : 'middle',
                formatter : Fui.formatFileSize
            },{
                field : 'account',
                title : '上传者',
                align : 'center',
                width : '50',
                valign : 'middle'
            },{
                field : 'createTime',
                title : '上传时间',
                align : 'center',
                width : '100',
                valign : 'middle',
                formatter :  Fui.formatDatetime
            }, {
                title: '操作',
                align: 'center',
                width: '50',
                formatter: function () {
                    return '<a class="select" title="选择" href="javascript:void(0);"><i class="glyphicon glyphicon-ok"></i></a>';
                },
                events: {
                    'click .select': function (e, value, row, index) {
                        var data = {
                            src : row.src,
                            fileName:row.fileName,
                            size: row.size
                        };
                        selectPic(data);
                    }
                }
            }]
        });

        /**
         * 上传部分代码
         */

        var $div =  $("#<%=tabId2%>");
        var userId = "${loginUser.id}";
        var containerId = createId();
        var $container = $div.find(".progress-container");
        var $uploadBtn = $div.find(".upload-picture");
        var $resetBtn = $div.find(".upload-reset");

        var btnId = createId();
        $uploadBtn.attr("id",btnId);
        $container.attr("id",containerId);
        var uploader = pic_uploader.getUploader(btnId,containerId,userId,selectPic);
        $resetBtn.click(function(){
            // var layerLoadIndex=layer.load(2);
            layer.confirm("是否确认要清空",function(index){
                layer.close(index);
                uploader.destroy();
                $container.html("");
                uploader = pic_uploader.getUploader(btnId,containerId,userId,selectPic);
            },function(index){
                layer.close(index);
            })
        })
    });
</script>