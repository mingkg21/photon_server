<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String formId = java.util.UUID.randomUUID().toString();
    String selectGameBtnId = java.util.UUID.randomUUID().toString();
    String selectFileBtnId = java.util.UUID.randomUUID().toString();
%>
<form id="<%=formId%>"  action="${base}/file/package/update" class="form-submit">

    <input type="hidden" value="${apk.id}" name="id"/>
    <div class="modal-dialog" style="width: 600px;">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    编辑
                </h4>
            </div>

            <div class="modal-body">
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>

                        <tr>
                            <td class="text-right" width="25%"><b style="color:red;">*</b>文件名：</td>
                            <td class="text-left">
                                <input type="text" class="form-control" value="${apk.fileName}" required name="fileName"/>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>包名：</td>
                            <td class="text-left">
                                <input type="text" class="form-control" value="${apk.packageName}" required name="packageName"/>
                            </td>
                        </tr>


                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>版本名：</td>
                            <td class="text-left">
                                <input type="text" class="form-control" value="${apk.versionName}" required name="versionName"/>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>版本号：</td>

                            <td class="text-left">
                                <input type="number" class="form-control" value="${apk.versionCode}" required name="versionCode"/>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right">兼容性：</td>
                            <td class="text-left">
                                <input type="text" class="form-control" value="${apk.adapterInfo}" name="adapterInfo"/>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right">更新日志：</td>
                            <td class="text-left">
                                <textarea rows="3"class="form-control" name="updateLog">${apk.updateLog}</textarea>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">

</script>