<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String tabId1 = java.util.UUID.randomUUID().toString();
    String tabId2 = java.util.UUID.randomUUID().toString();
    String picContainerId = java.util.UUID.randomUUID().toString();
    String showMeBtnId = java.util.UUID.randomUUID().toString();
    String showAllBtnId = java.util.UUID.randomUUID().toString();
    String uploadformId = java.util.UUID.randomUUID().toString();
%>

<div class="modal-dialog" style="width: 1000px;">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close fui-close" >&times;</button>
            <h4 class="modal-title">
                选择应用
            </h4>
        </div>

        <div class="modal-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#<%=tabId1%>" data-toggle="tab">
                        应用库
                    </a>
                </li>

                <li>
                    <a href="#<%=tabId2%>" data-toggle="tab">
                        上传
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane fade in active" id="<%=tabId1%>">

                    <!--
                    <div style="margin-top:5px;margin-left: 5px; ">
                        <button class="btn btn-primary" type="button" id="<%=showMeBtnId%>">只显示我的</button>
                        <button class="btn btn-default" type="button" id="<%=showAllBtnId%>">显示所有</button>
                    </div>-->
                    <form style="margin-top: 10px;" class="fui-search table-tool" method="post">
                        <div class="form-group fui-data-wrap">
                            <div class="form-inline">

                                <div class="input-group">
                                    <input type="text" class="form-control" name="account" value="" placeholder="上传账号">
                                </div>

                                <div class="input-group">
                                    <input type="text" class="form-control" name="fileName" value="" placeholder="文件名">
                                </div>

                                <div class="input-group">
                                    <input type="text" class="form-control" name="packageName" value="" placeholder="包名">
                                </div>

                                <button class="btn btn-primary fui-date-search">查询</button>
                            </div>
                        </div>
                    </form>


                    <div>
                        <table style="width:100%;" id="<%=picContainerId%>">

                        </table>
                    </div>
                </div>

                <div class="tab-pane fade" id="<%=tabId2%>">
                    <div style="margin-top: 5px;">
                        <form id="<%=uploadformId%>">
                            <table class="table table-bordered table-striped" style="clear: both">
                                <tbody>
                                <tr>
                                    <td width="20%" class="text-right">应用：</td>
                                    <td width="80%" class="text-left">
                                        <input type="file" accept="application/vnd.android.package-archive" name="file">
                                    </td>
                                </tr>

                                <tr>
                                    <td width="20%" class="text-right"><b style="color:red;">*</b>包名:</td>
                                    <td width="80%" class="text-left">
                                        <input type="text" class="form-control" name="packageName"/>
                                    </td>
                                </tr>

                                <tr>
                                    <td width="20%" class="text-right"><b style="color:red;">*</b>版本号:</td>
                                    <td width="80%" class="text-left">
                                        <input type="number" class="form-control" name="versionCode"/>
                                    </td>
                                </tr>

                                <tr>
                                    <td width="20%" class="text-right"><b style="color:red;">*</b>版本名：</td>
                                    <td width="80%" class="text-left">
                                        <input type="text" required class="form-control" name="versionName"/>
                                    </td>
                                </tr>


                                <tr>
                                    <td class="text-right">兼容性：</td>
                                    <td class="text-left">
                                        <input type="text" class="form-control" name="adapterInfo"/>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="text-right">更新日志：</td>
                                    <td class="text-left">
                                        <textarea rows="3"class="form-control" name="updateLog"></textarea>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                    <div style="margin-top: 5px;">
                        <!--不显示 -->
                        <button style="display: none;" type="button" class="btn btn-primary upload-picture">上传应用</button>
                        <button type="button" class="btn btn-primary upload-apk">上传应用</button>
                        <button type="button" class="btn btn-primary upload-reset-btn">重置按钮</button>
                        <button type="button" class="btn btn-danger upload-reset">清空</button>
                    </div>
                    <div class="progress-container" style="width:600px;">

                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default fui-close">取消</button>
        </div>
    </div>
</div>

<script language="javascript">
    requirejs(["template",'${base}/module/js/file/package-uploader.js?v=1.0.5',"jquery","layer"],function(template,pic_uploader){
        var dataMap = {};
        var selectData = {};
        var picContainerId = "<%=picContainerId%>"
        var pageSize = 18;
        var showAll = false;

        $showMeBtn = $("#<%=showMeBtnId%>");
        $showAllBtn = $("#<%=showAllBtnId%>");

        $showMeBtn.click(function(){
            showAll = false;
            $showMeBtn.removeClass("btn-default");
            $showMeBtn.addClass("btn-primary");

            $showAllBtn.removeClass("btn-primary");
            $showAllBtn.addClass("btn-default");
            $("#"+picContainerId).bootstrapTable('refresh',{pageNumber:1,pageSize:pageSize})
        })
        $showAllBtn.click(function(){
            showAll = true;
            $showAllBtn.removeClass("btn-default");
            $showAllBtn.addClass("btn-primary");

            $showMeBtn.removeClass("btn-primary");
            $showMeBtn.addClass("btn-default");
            $("#"+picContainerId).bootstrapTable('refresh',{pageNumber:1,pageSize:pageSize})
        })

        function toCallbackData(data){
            console.info("callback");
            console.info(data);
            var cd = {
                fileName:data.fileName,
                src : data.src,
                size: data.size,
                cdnSrc:baseInfo.cdn + data.src,
                packageName:data.packageName,
                versionCode:data.versionCode,
                versionName:data.versionName,
                updateLog: data.updateLog,
                adapterInfo:data.adapterInfo,
                sign:data.sign
            };
            return cd;
        }

        function callback(selData){
            var fn = Package.getCallback("${fnId}");
            if(fn){
               fn(toCallbackData(selData));
            }else{
                layer.msg("ERROR:找不到回调方法");
            }
        }
        var selectPic = function(data){
            callback(data);
        }


        Fui.addBootstrapTable({
            id:picContainerId,
            url : '${base}/file/package/list',
            pageSize:8,
            queryParams : function(params){
                params["showAll"] = showAll;
                return params;
            },
            columns : [ {
                field : 'fileName',
                title : '文件名',
                align : 'center',
                width : '150',
                valign : 'middle',
                formatter:function(v){
                    return "<div style='max-width:250px;word-break:break-all;'>"+v+"</div>";
                }
            },{
                field : 'packageName',
                title : '包名',
                align : 'center',
                width : '150',
                valign : 'middle',
                formatter:function(v){
                    return "<div style='max-width:250px;word-break:break-all;'>"+v+"</div>";
                }
            },{
                field : 'versionName',
                title : '版本名',
                align : 'center',
                width : '60',
                valign : 'middle'
            },{
                field : 'versionCode',
                title : '版本号',
                align : 'center',
                width : '60',
                valign : 'middle'
            },{
                field : 'size',
                title : '大小',
                align : 'center',
                width : '50',
                valign : 'middle',
                formatter : Fui.formatFileSize
            },{
                field : 'account',
                title : '上传者',
                align : 'center',
                width : '50',
                valign : 'middle'
            },{
                field : 'createTime',
                title : '上传时间',
                align : 'center',
                width : '100',
                valign : 'middle',
                formatter :  Fui.formatDatetime
            }, {
                title: '操作',
                align: 'center',
                width: '50',
                formatter: function () {
                    return '<a class="select" title="选择" href="javascript:void(0);"><i class="glyphicon glyphicon-ok"></i></a>';
                },
                events: {
                    'click .select': function (e, value, row, index) {
                        var data = {
                            src : row.src,
                            fileName:row.fileName,
                            size: row.size,
                            packageName:row.packageName,
                            versionCode:row.versionCode,
                            versionName:row.versionName,
                            updateLog: row.updateLog,
                            adapterInfo:row.adapterInfo,
                            sign:row.sign
                        };

                        if(!row.packageName || !row.versionCode || !row.versionName){
                            layer.msg("包信息不完整，请先补充完整信息再使用！");
                            return;
                        }
                        selectPic(data);
                    }
                }
            }]
        });

        /**
         * 上传部分代码
         */

        var $div =  $("#<%=tabId2%>");
        var $uploadForm = $("#<%=uploadformId%>");
        //var userId = "${loginUser.id}";


        var containerId = createId();
        var $container = $div.find(".progress-container");
        var $hideUploadBtn = $div.find(".upload-picture");
        var $resetBtn = $div.find(".upload-reset");
        var $uploadBtn = $div.find(".upload-apk");

        var btnId = createId();
        $hideUploadBtn.attr("id",btnId);
        $container.attr("id",containerId);


        var dataFn = function(){
            var data = {
                userId : "${loginUser.id}",
                packageName: $uploadForm.find("input[name='packageName']").val(),
                versionCode: $uploadForm.find("input[name='versionCode']").val(),
                versionName: $uploadForm.find("input[name='versionName']").val(),
                updateLog: $uploadForm.find("textarea[name='updateLog']").val(),
                adapterInfo:$uploadForm.find("input[name='adapterInfo']").val()
            }
            return data;
        }

        var uploader = pic_uploader.getUploader(btnId,containerId,dataFn,selectPic);
        $uploadBtn.click(function(){
            var fileInput = $uploadForm.find("input[name='file']")[0];
            if(fileInput.files.length == 0 ){
                layer.msg("请选择上传应用包");
                return;
            }

            if(!$uploadForm.find("input[name='packageName']").val()){
                layer.msg("请先填写包名");
                return;
            }

            if(!$uploadForm.find("input[name='versionName']").val()){
                layer.msg("请先填写版本名");
                return;
            }

            if(!$uploadForm.find("input[name='versionCode']").val()){
                layer.msg("请先填写版本号");
                return;
            }
            uploader.addFile(fileInput.files[0]);
            $uploadBtn.attr("disabled","disabled");
        });

        $div.find(".upload-reset-btn").click(function(){
            $uploadBtn.removeAttr("disabled");
        });


        $resetBtn.click(function(){
            layer.confirm("是否确认要清空",function(index){
                layer.close(index);
                uploader.destroy();
                $container.html("");
                $uploadForm[0].reset();
                uploader = pic_uploader.getUploader(btnId,containerId,dataFn);
                $uploadBtn.removeAttr("disabled");
            },function(index){
                layer.close(index);
            })
        })
    });
</script>