<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String tabId1 = java.util.UUID.randomUUID().toString();
    String tabId2 = java.util.UUID.randomUUID().toString();
    String picContainerId = java.util.UUID.randomUUID().toString();
    String imageTemplateId = java.util.UUID.randomUUID().toString();
    String pagingTemplateId  = java.util.UUID.randomUUID().toString();
    String paggingBarId = java.util.UUID.randomUUID().toString();
    String showMeBtnId = java.util.UUID.randomUUID().toString();
    String showAllBtnId = java.util.UUID.randomUUID().toString();
%>

<div class="modal-dialog" style="width: 1000px;">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close fui-close" >&times;</button>
            <h4 class="modal-title">
                选择视频
            </h4>
        </div>

        <div class="modal-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#<%=tabId1%>" data-toggle="tab">
                        视频库
                    </a>
                </li>

                <li>
                    <a href="#<%=tabId2%>" data-toggle="tab">
                        上传
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane fade in active" id="<%=tabId1%>">
                    <div style="margin-top:5px;margin-left: 5px; ">
                        <button class="btn btn-primary" type="button" id="<%=showMeBtnId%>">只显示我的</button>
                        <button class="btn btn-default" type="button" id="<%=showAllBtnId%>">显示所有</button>
                    </div>
                    <div>
                        <div style="height: 270px;width:100%;" id="<%=picContainerId%>">

                        </div>
                        <div id="<%=paggingBarId%>">

                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="<%=tabId2%>">
                    <div style="margin-top: 5px;">
                        <button type="button" class="btn btn-primary upload-picture">上传视频</button>
                        <button type="button" class="btn btn-danger upload-reset">清空</button>
                    </div>
                    <div class="progress-container" style="width:600px;">

                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default fui-close">取消</button>
        </div>
    </div>
</div>

<script language="javascript">
    requirejs(["template",'${base}/module/js/file/video-uploader.js',"jquery","layer"],function(template,pic_uploader){
        var dataMap = {};
        var selectData = {};
        var picContainerId = "<%=picContainerId%>"
        var pageSize = 18;
        var showAll = false;

        $showMeBtn = $("#<%=showMeBtnId%>");
        $showAllBtn = $("#<%=showAllBtnId%>");

        $showMeBtn.click(function(){
            showAll = false;
            $showMeBtn.removeClass("btn-default");
            $showMeBtn.addClass("btn-primary");

            $showAllBtn.removeClass("btn-primary");
            $showAllBtn.addClass("btn-default");
            loadData(1);
        })
        $showAllBtn.click(function(){
            showAll = true;
            $showAllBtn.removeClass("btn-default");
            $showAllBtn.addClass("btn-primary");

            $showMeBtn.removeClass("btn-primary");
            $showMeBtn.addClass("btn-default");
            loadData(1);
        })

        function toCallbackData(data){
            var cd = {
                fileName:data.fileName,
                src : data.src,
                m3u8:data.m3u8,
                cdnSrc : baseInfo.cdn+""+data.src,
                cdnM3u8: baseInfo.cdn+""+data.m3u8,
                size: data.size,
                type: data.type
            };
            return cd;
        }

        function callback(selData){
            var fn = Video.getCallback("${fnId}");
            if(fn){
               fn(toCallbackData(selData));
            }else{
                layer.msg("ERROR:找不到回调方法");
            }
        }
        var selectPic = function(data){
            callback(data);
        }

        var renderImageContainer = function(data){
            for(var i =0;i<data.rows.length;i++){
                var row = data.rows[i];
                dataMap[row.id] = row;
            }
            data.cdn = baseInfo.cdn;
            var html = template('<%=imageTemplateId%>', data);
            var $container = $("#"+picContainerId);
            $container.html(html);
            $container.find("button").click(function(){
                var id = this.getAttribute("image-id");
                var data = dataMap[id];
                selectPic(data);
            });
        }

        var renderPagingBar = function(data,pageNo){
            pageNo = parseInt(pageNo);
            var count = data.total;
            var pageCount = parseInt(count / pageSize)
            if(count > pageCount * pageSize){
                pageCount++;
            }

            var item = [];
            if(pageCount <= 15){
                for(var i=0;i<pageCount;i++){
                    item.push({
                        index:(i+1),
                        page:true
                    })
                }
            }else{
                item.push({index:1,page:true});
                var left = 6;
                var right = 6;

                if(pageNo <= (left + 1)){
                    for(var i = 2;i<= pageNo;i++){
                        item.push({index:i,page:true});
                    }
                    right += (left- pageNo);
                }else {
                    item.push({text:"...",page:false});
                    for(var i = left ;i>= 0;i--){
                        item.push({index:(pageNo - i),page:true});
                    }
                }

                if(pageCount <= right + pageNo + 1){
                    for(var i = pageNo + 1;i< pageCount;i++){
                        item.push({index:i,page:true});
                    }
                }else{
                    for(var i = pageNo + 1;i< right + pageNo + 1;i++){
                        item.push({index:i,page:true});
                    }
                    item.push({text:"...",page:false});
                }
                item.push({index:pageCount,page:true})
            }

            var renderData = {
                currentPage : pageNo,
                pageCount:pageCount,
                items:item
            }
            var html = template('<%=pagingTemplateId%>', renderData);
            var $paggingBar = $("#<%=paggingBarId%>");
            $paggingBar.html(html);
            $paggingBar.find(".page-btn").click(function(){
                var p = this.getAttribute("page");
                if(p == pageNo){
                    return;
                }
                loadData(p);
            });
        }

        var loadData = function(pageNo){
            $.ajax({
                url:"${base}/file/video/query",
                data:{
                    showAll: showAll === true,
                    pageSize :pageSize,
                    pageNumber:pageNo
                },
                success:function(data){
                    renderImageContainer(data);
                    renderPagingBar(data,pageNo);
                }
            });
        }
        loadData(1);

        /**
         * 上传部分代码
         */

        var $div =  $("#<%=tabId2%>");
        var userId = "${loginUser.id}";
        var containerId = createId();
        var $container = $div.find(".progress-container");
        var $uploadBtn = $div.find(".upload-picture");
        var $resetBtn = $div.find(".upload-reset");

        var btnId = createId();
        $uploadBtn.attr("id",btnId);
        $container.attr("id",containerId);
        var uploader = pic_uploader.getUploader(btnId,containerId,userId,selectPic);
        $resetBtn.click(function(){
            // var layerLoadIndex=layer.load(2);
            layer.confirm("是否确认要清空",function(index){
                layer.close(index);
                uploader.destroy();
                $container.html("");
                uploader = pic_uploader.getUploader(btnId,containerId,userId,selectPic);
            },function(index){
                layer.close(index);
            })
        })

    });
</script>

<style type="text/css">
    .img-container{
        height:120px;
        width: 100px;
        margin-left: 5px;
        margin-top: 5px;
        float:left;
    }

</style>

<script id="<%=imageTemplateId%>"  type="text/html">
    {{each rows as row i}}
    <div class="img-container">
        <div style="height: 100px;">
            <img src="{{cdn}}{{row.src}}?vframe/jpg/offset/2/w/100/h/100"/>
        </div>
        <button type="button" image-id="{{row.id}}" class="btn btn-default btn-xs" style="width: 100%">
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> 选择
        </button>
    </div>
    {{/each}}
</script>

<script id="<%=pagingTemplateId%>" type="text/html">
    {{if pageCount > 1}}
    <div class="bs-example" data-example-id="disabled-active-pagination">
        <nav aria-label="...">
            <ul class="pagination">

                <li {{if currentPage == 1}}class="disabled"{{/if}}>
                <a href="#" aria-label="Previous" {{if currentPage != 1}}  class="page-btn" page="{{currentPage - 1}}" {{/if}}><span aria-hidden="true">«</span></a>
                </li>
                {{each items as item}}
                <li  {{if item.index == currentPage }}class="active"{{/if}}>
                <a href="#" {{if item.page}} class="page-btn" page="{{item.index}}"{{/if}}>{{item.index||item.text}} <span class="sr-only">(current)</span></a>
                </li>
                {{/each}}
                <li {{if currentPage == pageCount }}class="disabled"{{/if}}>
                <a href="#" aria-label="Next" {{if currentPage != pageCount}}  class="page-btn" page="{{currentPage + 1}}" {{/if}}><span aria-hidden="true">»</span></a>
                </li>
            </ul>
        </nav>
    </div>
    {{/if}}
</script>
