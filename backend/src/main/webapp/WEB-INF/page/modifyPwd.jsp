<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/updpwd" class="form-submit" unReloadTable="true">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">修改密码</h4>
		</div>
		<div class="modal-body">
			<table class="table table-bordered table-striped" style="clear: both">
				<tbody>
					<tr>
						<td width="30%" class="text-right vmid">用户名：</td>
						<td ><input type="text" class="form-control" value="${loginUser.account}"disabled="disabled" /></td>
					</tr>
					<tr>
						<td class="text-right vmid">旧密码：</td>
						<td><input type="password" class="form-control" required name="oldPassword" /></td>
					</tr>
					<tr>
						<td class="text-right vmid">新密码：</td>
						<td><input type="password" minlength="6" required class="form-control" name="newPassword" /></td>
					</tr>
					<tr>
						<td class="text-right vmid">确认密码：</td>
						<td><input type="password" minlength="6" required class="form-control" name="reNewPassword" /></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default fui-close">关闭</button>
			<button type="submit" class="btn btn-primary">保存</button>
		</div>
	</div>
</div>
</form>

<script language="javascript">
requirejs(['jquery','bootstrap','Fui'],function() {
	var $form = Fui.getParentNode().find("form");
    $form.data("paramFn",function(){
        try{
			var $pwd = $form.find("[name='newPassword']");
			var $rePwd = $form.find("[name='reNewPassword']");
			var newPassword = $pwd.val();
			var reNewPassword = $rePwd.val();
			if (newPassword != reNewPassword) {
				layer.msg("新密码不一致！");
				newPassword.val("").focus();
				reNewPassword.val("");
				return false;
			}
        }catch(e){
            console.info(e.message);
            return false;
		}
        return $form.serialize();
    });
});

</script>
