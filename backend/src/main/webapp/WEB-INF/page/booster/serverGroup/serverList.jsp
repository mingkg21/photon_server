<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" class="btn btn-default open-dialog"  dialog-title="新增服务器" href="${base}/booster/serverGroup/goAddServerPage?groupId=${groupId}" >新增</button>
	</div>
</div>


<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
       // var $div = Fui.getParentNode();
        Fui.addBootstrapTable({
            url : '${base}/booster/serverGroup/getServerList?groupId=${groupId}',
            columns : [{
                field : 'serverId',
                title : '服务器ID',
                align : 'center',
                width : '50',
                valign : 'middle'
            },{
                field : 'serverName',
                title : '服务器名称',
                align : 'center',
                width : '80',
                valign : 'middle'
			},{
                field : 'ip',
                title : '服务器IP',
                align : 'center',
                width : '60',
                valign : 'middle'
			},{
                field : 'serverStatus',
                title : '服务器状态',
                align : 'center',
                width : '60',
                valign : 'middle',
                formatter : function (v,row) {
              		if(v == 2){
              		    return "<font color='green'>启用</font>";
					}
                    return "<font color='red'>禁用</font>";
                }
            },{
                field : 'ordering',
                title : '优先排序',
                align : 'center',
                width : '60',
                valign : 'middle'
			},{
                title : '操作',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : function(v,row){
                    return [
                        ' <a class="todo-ajax" href="${base}/booster/serverGroup/deleteServer?refId=',
							row.id,'" title="删除服务器">',
						'<i class="glyphicon glyphicon-trash"></i>', '</a> ',

                        '<a class="open-dialog" dialog-title="修改" href="${base}/booster/serverGroup/goEditServerPage?refId=',
                        row.id,'" title="修改">','<i class="glyphicon glyphicon-edit"></i>', '</a> '

					].join("");
				}
            }]
        });
    });
</script>

