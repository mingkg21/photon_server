<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" class="btn btn-default open-dialog"  dialog-title="新增服务器" href="${base}/booster/serverGroup/goAddPage" >新增</button>
	</div>
</div>



<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">

			<div class="input-group">
				<input type="text" class="form-control" name="name" placeholder="名称">
			</div>

			<button class="btn btn-primary fui-date-search">查询</button>
		</div>
	</div>
</form>




<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
       // var $div = Fui.getParentNode();
        Fui.addBootstrapTable({
            url : '${base}/booster/serverGroup/list',
            columns : [{
                field : 'id',
                title : 'ID',
                align : 'center',
                width : '60',
                valign : 'middle'
			},{
                field : 'name',
                title : '组名称',
                align : 'center',
                width : '200',
                valign : 'middle'
            },{
                field : 'createTime',
                title : '创建时间',
                align : 'center',
                width : '120',
                valign : 'middle',
                formatter : Fui.formatDatetime
			},{
                title : '操作',
                align : 'center',
                width : '60',
                valign : 'middle',
                formatter : function(v,row){
                    return [ '<a class="open-dialog" dialog-title="修改服务器" href="${base}/booster/serverGroup/goEditPage?id=',
							row.id,'" title="修改">',
						'<i class="glyphicon glyphicon-edit"></i>', '</a> ',
                        ' <a class="open-tab" tab-id="tab-system-booster-server_group_',row.id,'" dialog-title="服务群组（',row.name,'）"  data-icon="glyphicon glyphicon-zoom-in" href="${base}/booster/serverGroup/goServerListPage?groupId=',
                        row.id,'" title="查看服务群组">',
                        '<i class="glyphicon glyphicon-zoom-in"></i>', '</a>'
					].join("");
				}
            }]
        });
    });
</script>

