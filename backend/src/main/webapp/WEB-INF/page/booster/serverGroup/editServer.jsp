<%@ page import="java.util.UUID" %>
<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<form action="${base}/booster/serverGroup/saveOrUpdateServer" class="form-submit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    编辑服务器
                </h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" value="${ref.id}">

                <c:if test="${ref != null}">
                    <input type="hidden" name="groupId" value="${ref.groupId}">
                </c:if>
                <c:if test="${ref == null}">
                    <input type="hidden" name="groupId" value="${groupId}">
                </c:if>
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>
                        <tr>
                            <td width="20%" class="text-right"><b style="color:red;">*</b>服务器：</td>
                            <td width="80%" class="text-left">
                                <input name="serverId" type="text" required placeholder="空格触发选择" data-max-items="1"  data-url="${base}/booster/server/getForSelectize" class="fui-select" data-json-value='${serverJson}'>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" class="text-right"><b style="color:red;">*</b>优先顺序：</td>
                            <td width="80%" class="text-left">
                                <input type="number" name="ordering" class="form-control" required placeholder="优先顺序" value="${ref.ordering}"/>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>
