<%@ page import="java.util.UUID" %>
<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<form action="${base}/booster/server/saveOrUpdatePort" class="form-submit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    编辑服务器
                </h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" value="${port.id}">
                <input type="hidden" name="serverId" value="${serverId}">
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>
                        <tr>
                            <td class="text-right">端口号</td>
                            <td class="text-left">
                                <input type="number" required class="form-control" value="${port.port}" name="port" placeholder="端口号"/>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right">密码</td>
                            <td class="text-left">
                                <input type="text" required class="form-control" value="${port.password}" name="password" placeholder="密码"/>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>
