<%@ page import="java.util.UUID" %>
<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<form action="${base}/booster/server/saveOrUpdate" class="form-submit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    编辑服务器
                </h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" value="${server.id}">
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>
                        <tr>
                            <td width="20%" class="text-right"><b style="color:red;">*</b>名称：</td>
                            <td width="80%" class="text-left">
                                <input type="text" required class="form-control" value="${server.name}" name="name" placeholder="名称" />
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>连接IP</td>
                            <td class="text-left">
                                <input type="text" required class="form-control" value="${server.ip}" name="ip" placeholder="连接IP"/>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>服务IP</td>
                            <td class="text-left">
                                <input type="text" required class="form-control" value="${server.realIp}" name="realIp" placeholder="服务IP"/>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>起始端口</td>
                            <td class="text-left">
                                <input type="number" required class="form-control" value="${server.startPort}" name="startPort" placeholder="起始端口"/>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>起始端口</td>
                            <td class="text-left">
                                <input type="number" required class="form-control" value="${server.endPort}" name="endPort" placeholder="终止端口"/>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>最大连接数</td>
                            <td class="text-left">
                                <input type="text" required class="form-control" value="${server.maxConnections}" name="maxConnections" placeholder="最大连接数"/>
                            </td>
                        </tr>



                        <tr>
                            <td width="20%" class="text-right">备注：</td>
                            <td width="80%" class="text-left">
                                <textarea name="remark" class="form-control">${server.remark}</textarea>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>
