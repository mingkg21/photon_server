<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" class="btn btn-default open-dialog"  dialog-title="新增端口" href="${base}/booster/server/goAddPortPage?serverId=${serverId}" >新增</button>
	</div>
</div>


<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
       // var $div = Fui.getParentNode();
        Fui.addBootstrapTable({
            url : '${base}/booster/server/getPortList?serverId=${serverId}',
            columns : [{
                field : 'serverId',
                title : '服务器ID',
                align : 'center',
                width : '50',
                valign : 'middle'
            },{
                field : 'serverName',
                title : '服务器名称',
                align : 'center',
                width : '50',
                valign : 'middle'
			},{
                field : 'port',
                title : '端口号',
                align : 'center',
                width : '60',
                valign : 'middle'
            },{
                field : 'password',
                title : '密码',
                align : 'center',
                width : '100',
                valign : 'middle'
			},{
                field : 'status',
                title : '状态',
                align : 'center',
                width : '60',
                valign : 'middle',
                formatter : function (v,row) {
                    return Fui.statusFormatter({
                        val : v,
                        url:'${base}/booster/server/updatePortStatus?portId='+row.id+'&status='
                    });
                }
            },{
                field : 'createTime',
                title : '创建时间',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : Fui.formatDatetime
            },{
                title : '操作',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : function(v,row){
                    return [
                        '<a class="open-dialog" dialog-title="修改端口" href="${base}/booster/server/goEditPortPage?portId=',
                        row.id,'" title="修改端口">',
                        '<i class="glyphicon glyphicon-edit"></i>', '</a>',

                        ' <a class="todo-ajax" href="${base}/booster/server/deletePort?portId=',
							row.id,'" title="删除端口">',
						'<i class="glyphicon glyphicon-trash"></i>', '</a>'].join("");
				}
            }]
        });
    });
</script>

