<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" class="btn btn-default open-dialog"  dialog-title="新增服务器" href="${base}/booster/server/goAddPage" >新增</button>
	</div>
</div>


<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">

			<div class="input-group">
				<input type="text" class="form-control" name="name" placeholder="名称">
			</div>

			<div class="input-group">
				<select class="form-control" name="status">
					<option value="">--状态--</option>
					<option value="2">启用</option>
					<option value="1">禁用</option>
				</select>
			</div>

			<button class="btn btn-primary fui-date-search">查询</button>
		</div>
	</div>
</form>


<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
       // var $div = Fui.getParentNode();
        Fui.addBootstrapTable({
            url : '${base}/booster/server/list',
            columns : [{
                field : 'id',
                title : 'ID',
                align : 'center',
                width : '40',
                valign : 'middle'
			},{
                field : 'name',
                title : '名称',
                align : 'center',
                width : '120',
                valign : 'middle',
                formatter : function (v,row) {
//                    if(row.defServer == 1){
//                        return "<div style='color:blue;' title='默认服务器'>"+v+"</div>";
//					}
					return v;
                }
            },{
                field : 'ip',
                title : '连接IP/端口',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : function (v,row) {
                    return v + "</br>" +row.startPort +" - " +row.endPort;
                }
			},{
                field : 'realIp',
                title : '服务器IP',
                align : 'center',
                width : '100',
                valign : 'middle',
			},{
                field : 'status',
                title : '状态',
                align : 'center',
                width : '60',
                valign : 'middle',
                formatter : function (v,row) {
					return Fui.statusFormatter({
						val : v,
						url:'${base}/booster/server/updateStatus?id='+row.id+'&status='
					});
                }
            },
//				{
//                field : 'createTime',
//                title : '创建时间',
//                align : 'center',
//                width : '120',
//                valign : 'middle',
//                formatter : Fui.formatDatetime
//			},
			{
				field : 'working',
				title : '服务器状态',
				align : 'center',
				width : '100',
				valign : 'middle',
                formatter : function(v,row){
                    var time = row.lastResponseTime;
				    var timeout = (new Date()).getTime() - time  > 20 * 60 * 1000;
                    var result = "状态:";
                    if(timeout){
                        result += "<font color='red'>超时未响应</font>";
                    }else if(row.telnetStatus == 0){
						result += "<font color='red'>telnet异常</font>";
					}else{
                        if(v == 1){
                            result += "<font color='green'>正常</font>";
                        }else{
                            result += "<font color='red'>未启动</font>";
                        }
					}



                    result =  result + "<br/>" + Fui.formatDatetime(time) ;
					return result;
				}
			},{
				field : 'successPercent',
				title : '连接成功率',
				align : 'center',
				width : '60',
				valign : 'middle',
				formatter : function(v,row){
					if(v != 0 && !v){
						return "-";
					}
					return (v * 100 )+"%";
				}
			},{
				title : '在线/最大',
				align : 'center',
				width : '80',
				valign : 'middle',
				formatter : function(v,row){
					return row.personCount+" / "+row.maxConnections;
				}
			},{
				field : 'contectedCount',
				title : '连接人数',
				align : 'center',
				width : '60',
				valign : 'middle',
			},{
				title : '负载状态',
				align : 'left',
				width : '60',
				valign : 'middle',
				formatter : function(v,row){
					var html = "";
					if(row.load1 || row.load1 == 0){
                        html += "1分钟："+row.load1;
					}

					if(row.load5 || row.load5 == 0){
						html +="</br>5分钟："+row.load5;
                    }
                    if(row.load15 || row.load15 == 0) {
                        html += "</br>15分钟：" + row.load15;
                    }
                    return html;
				}
			},{
                title : '操作',
                align : 'center',
                width : '60',
                valign : 'middle',
                formatter : function(v,row){
                    var arr = [ '<a class="open-dialog" dialog-title="修改服务器" href="${base}/booster/server/goEditPage?id=',
							row.id,'" title="修改">',
						'<i class="glyphicon glyphicon-edit"></i>', '</a> '
					];

                    <%--if(row.defServer != 1){--%>
                        <%--arr.push(--%>
                            <%--'<a class="todo-ajax" href="${base}/booster/server/resetDefaultServer?id=',--%>
                            <%--row.id,'" title="设置默认服务器" confirm="是否确认要设置默认服务器?">',--%>
                            <%--'<i class="glyphicon glyphicon-heart"></i>', '</a> ');--%>
                    <%--}--%>
                    return arr.join("");
				}
            }]
        });
    });
</script>

