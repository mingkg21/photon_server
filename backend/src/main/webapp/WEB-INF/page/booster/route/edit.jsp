<%@ page import="java.util.UUID" %>
<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<%
    String gameContainerId = java.util.UUID.randomUUID().toString();
    String selectGameBtnId = java.util.UUID.randomUUID().toString();
    String formId = java.util.UUID.randomUUID().toString();
%>

<form id="<%=formId%>" action="${base}/booster/route/saveOrUpdate" class="form-submit">
    <input type="hidden" name="id" value="${route.id}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    编辑
                </h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped" style="clear: both">

                    <tbody>
                        <tr>
                            <td width="20%" class="text-right"><b style="color:red;">*</b>服务器群组：</td>
                            <td width="80%" class="text-left">
                                <input name="groupId" type="text" required placeholder="空格触发选择" data-max-items="1"  data-url="${base}/booster/serverGroup/getForSelectize" class="fui-select" data-json-value='${serverGroupJson}'>
                            </td>
                        </tr>


                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>游戏：</td>
                            <td class="text-left">
                                <button id="<%=selectGameBtnId%>" type="button" class="btn btn-primary btn-sm">选择</button>
                                <input type="hidden" name="gameId" value="${route.gameId}">
                                <div id="<%=gameContainerId%>">

                                </div>
                            </td>
                        </tr>

                    </tbody>

                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
        var $form = $("#<%=formId%>");
        $form.data("paramFn",function(){
            if(!$form.find("input[name='gameId']").val()){
                layer.msg("请选择游戏");
                return false;
            }
        });

        var $gameContainer = $("#<%=gameContainerId%>");
        var showGameInfo = function(icon,name){
            BaseUtil.addSingleImage($gameContainer,{src:icon},{size:"50",height:50,width:50});
            setTimeout(function(){
                $gameContainer.append("<div style='height:50px;line-height:50px;float: left'><strong>"+name+"</strong><div>")
            },200);
        }

        $("#<%=selectGameBtnId%>").click(function(){
            var gameSelector = new Game(function(data){
                $form.find("input[name='gameId']").val(data.id);
                showGameInfo(data.icon,data.name);
                gameSelector.close();
            });
            gameSelector.select();
        });

        <c:if test="${versionInfo != null}">
        showGameInfo("${versionInfo.icon.src}","${gameInfo.name}");
        </c:if>
    });
</script>
