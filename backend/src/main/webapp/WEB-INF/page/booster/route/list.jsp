<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" class="btn btn-default open-dialog"  dialog-title="新增关联配置" href="${base}/booster/route/goAddPage" >新增</button>
	</div>
</div>


<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">

			<div class="input-group">
				<input type="number" class="form-control" name="gameId" placeholder="应用ID">
			</div>

			<div class="input-group">
				<input type="text" class="form-control" name="gameName" placeholder="应用名称">
			</div>

			<div class="input-group">
				<input type="number" class="form-control" name="groupId" placeholder="服务器群组ID">
			</div>



			<button class="btn btn-primary fui-date-search">查询</button>
		</div>
	</div>
</form>

<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
       // var $div = Fui.getParentNode();
        Fui.addBootstrapTable({
            url : '${base}/booster/route/list',
            columns : [{
                title : '服务器群组(ID)',
                align : 'center',
                width : '200',
                valign : 'middle',
                formatter :function (v,row) {
					return row.groupName+"<b>("+row.groupId+")</b>";
                }
            },{
                title : '游戏(ID)',
                align : 'left',
                width : '200',
                valign : 'middle',
                formatter :function (v,row) {
                    var html = "";
                    if(row.gameIcon){
                        html += Fui.formatImage(row.gameIcon,"50") + "&nbsp;";
                    }
                    html += "<strong>"+row.gameName+"("+row.gameId+")<strong>";
                    return html;
                }
			}, {
                field : 'createTime',
                title : '创建时间',
                align : 'center',
                width : '120',
                valign : 'middle',
                formatter : Fui.formatDatetime
			},{
                title : '操作',
                align : 'center',
                width : '60',
                valign : 'middle',
                formatter : function(v,row){
                    return [ '<a class="open-dialog" dialog-title="修改关联配置" href="${base}/booster/route/goEditPage?id=',
							row.id,'" title="修改关联配置">',
						'<i class="glyphicon glyphicon-edit"></i>', '</a> ',

                        '<a class="todo-ajax" href="${base}/booster/route/delete?id=',row.id,
                        ,'" title="删除" confirm="是否确认要删除?">',
                        '<i class="glyphicon glyphicon-trash"></i>', '</a> '

					].join("");
				}
            }]
        });
    });
</script>

