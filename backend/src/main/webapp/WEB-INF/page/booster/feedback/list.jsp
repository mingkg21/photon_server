<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">

			<div class="input-group">
				<select name="containPackageName" class="form-control">
					<option value="true" selected>有包名</option>
					<option value="false">无包名</option>
				</select>
			</div>

			<div class="input-group">
				<input type="text" class="form-control" name="packageName" placeholder="包名">
			</div>

			<button class="btn btn-primary fui-date-search">查询</button>
		</div>
	</div>
</form>


<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
        Fui.addBootstrapTable({
            url : '${base}/booster/feedback/list',
            columns : [{
                field : 'name',
                title : '游戏名',
                align : 'center',
                width : '100',
                valign : 'middle'
			},{
                field : 'packageName',
                title : '包名',
                align : 'center',
                width : '250',
                valign : 'middle'
            },{
                field : 'versionCode',
                title : '版本号',
                align : 'center',
                width : '80',
                valign : 'middle'
			},{
                field : 'versionName',
                title : '版本名',
                align : 'center',
                width : '80',
                valign : 'middle'
			},{
                field : 'totalCount',
                title : '反馈总人数',
                align : 'center',
                width : '120',
                valign : 'middle'
            },{
                title : '操作',
                align : 'center',
                width : '60',
                valign : 'middle',
                formatter : function(v,row){
                    if(row.packageName){
                        return [
                            '<a class="todo-ajax" href="${base}/booster/feedback/deleteByPackageName?packageName=',
                            row.packageName,'" title="删除" confirm="是否确认要删除?">',
                            '<i class="glyphicon glyphicon-trash"></i>', '</a> '
                        ].join("");
					}

                    return [
                        '<a class="todo-ajax" href="${base}/booster/feedback/deleteByName?name=',
                        row.name,'" title="删除" confirm="是否确认要删除?">',
                        '<i class="glyphicon glyphicon-trash"></i>', '</a> '
                    ].join("");

                }
            }]
        });
    });
</script>

