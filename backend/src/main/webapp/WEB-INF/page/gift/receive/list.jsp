<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">

			<div class="input-group" style="width: 250px;">
				<input name="gameId" type="text" data-max-items="1"  placeholder="游戏" data-url="${base}/game/info/getForSelectize" class="fui-select">
			</div>

			<div class="input-group">
				<input type="text" class="form-control" name="giftName" placeholder="礼包名">
			</div>

			<button class="btn btn-primary fui-date-search">查询</button>
		</div>
	</div>
</form>


<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
        Fui.addBootstrapTable({
            url : '${base}/gift/receive/list',
            columns : [{
                title : '礼包名称',
                align : 'center',
                width : '80',
                valign : 'middle',
                field : 'giftName'
            },{
                title : '游戏',
                align : 'center',
                width : '100',
                valign : 'middle',
                field : 'gameName'
//				, formatter : function(v,row){
//                    var html = "";
//                    if(row.icon){
//                        html += Fui.formatImage(row.icon,"50");
//                    }
//                    html += "<strong>"+row.gameName+"<strong>";
//                    return html;
//                }
            },{
                field : 'nickName',
                title : '用户昵称',
                align : 'center',
                width : '50',
                valign : 'middle',
                formatter : function(v,row){
					return v+"("+row.userId+")";
                }
			},{
                field : 'type',
                title : '类型',
                align : 'center',
                width : '50',
                valign : 'middle',
                formatter : function(v,row){
                    if(v == 0){
                        return "新手卡";
					}
					if(v == 1){
                        return "礼包";
					}
					if(v == 2){
					    return "激活码";
					}
                }
			},{
                field : 'code',
                title : '卡号',
                align : 'center',
                width : '80',
                valign : 'middle'
			},{
                field : 'createTime',
                title : '预约时间',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : Fui.formatDatetime
			}]
        });
    });
</script>

