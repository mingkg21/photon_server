<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String tableId = java.util.UUID.randomUUID().toString();
    String formId = java.util.UUID.randomUUID().toString();
%>

    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    编辑礼品
                </h4>
            </div>
            <div class="modal-body">
                <form id="<%=formId%>" action="${base}/gift/info/addNumbers" class="form-submit">
                    <input type="hidden" name="giftId" value="${id}"/>
                    <table class="table" style="clear: both">
                        <tbody>
                        <tr>
                            <td width="20%" class="text-right"><b style="color:red;">*</b>礼包号串：</td>
                            <td width="80%" class="text-left">
                                <textarea class="form-control" name="numbers" style="height: 120px;"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td class="text-left">
                                <button type="submit" class="btn btn-primary">保存</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>

                <table id="<%=tableId%>" style="margin-top:10px;"></table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>




<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
        var tableId = "<%=tableId%>";
        var $form = $("#<%=formId%>");
        $form.data("callback",function () {
            layer.msg("成功新增号串");
            $("#"+tableId).bootstrapTable('refresh');
            $form.find("textarea[name='numbers']").val('');
        })
        Fui.addBootstrapTable({
            id : tableId,
            url : '${base}/gift/info/getNumberList?giftId=${id}',
            columns : [{
                title : '号码',
                align : 'center',
                width : '120',
                valign : 'middle',
                field : 'code'
            },{
                field : 'status',
                title : '类型',
                align : 'center',
                width : '50',
                valign : 'middle',
                formatter : function(v,row){
                    if(v == 0){
                        return "<font color='green'>未领取</font>";
                    }
                    if(v == 1){
                        return "<font color='red'>已领取</font>";
                    }
                }
            },{
                field : 'createTime',
                title : '创建时间',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : Fui.formatDatetime
            }]
        });
    })
</script>