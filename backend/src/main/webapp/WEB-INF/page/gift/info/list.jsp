<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" class="btn btn-default open-dialog"  dialog-title="新增礼包" href="${base}/gift/info/goAddPage" >新增</button>
	</div>
</div>

<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">

			<div class="input-group" style="width: 250px;">
				<input name="gameId" type="text" data-max-items="1"  placeholder="游戏" data-url="${base}/game/info/getForSelectize" class="fui-select">
			</div>

			<div class="input-group">
				<input type="text" class="form-control" name="giftName" placeholder="礼包名">
			</div>

			<button class="btn btn-primary fui-date-search">查询</button>
		</div>
	</div>
</form>


<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
        Fui.addBootstrapTable({
            url : '${base}/gift/info/list',
            columns : [{
                title : '游戏',
                align : 'left',
                width : '100',
                valign : 'middle',
                formatter : function(v,row){
                    var html = "";
                    if(row.icon){
                        html += Fui.formatImage(row.icon,"50");
                    }
                    html += "<strong>"+row.gameName+"<strong>";
                    return html;
				}
			},{
                title : '名称',
                align : 'center',
                width : '80',
                valign : 'middle',
                field : 'name'
            },{
                field : 'type',
                title : '类型',
                align : 'center',
                width : '50',
                valign : 'middle',
                formatter : function(v,row){
                    if(v == 0){
                        return "新手卡";
					}
					if(v == 1){
                        return "礼包";
					}
					if(v == 2){
					    return "激活码";
					}
                }
			},{
                field : 'status',
                title : '状态',
                align : 'center',
                width : '50',
                valign : 'middle',
                formatter : function(v,row){
                    if(v == 0){
                        return "<font color='red'>禁用</font>";
                    }

                    if(v == 1){
                        return "<font color='green'>启用</font>";
                    }
                }
            },{
                field : 'startTime',
                title : '上线时间',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : Fui.formatDatetime
			},{
                title : '操作',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : function(v,row){
                   var arr = [
                       '<a class="open-dialog" dialog-title="修改" href="${base}/gift/info/goEditPage?id=',
                        row.id,'" title="修改">',
                        '<i class="glyphicon glyphicon-edit"></i>',
					   '</a> ',
                       '<a class="open-dialog" dialog-title="礼包号串" href="${base}/gift/info/goAddNumberPage?id=',
                       row.id,'" title="添加查看号串">',
                       '<i class="glyphicon glyphicon-zoom-in"></i>',
                       '</a>'
				   ];
                    return arr.join("");
				}
            }]
        });
    });
</script>

