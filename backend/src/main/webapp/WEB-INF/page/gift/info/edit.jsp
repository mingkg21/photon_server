<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String gameContainerId = java.util.UUID.randomUUID().toString();
    String selectGameBtnId = java.util.UUID.randomUUID().toString();
    String formId = java.util.UUID.randomUUID().toString();
%>
<form id="<%=formId%>"  action="${base}/gift/info/saveOrUpdate" class="form-submit">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    编辑礼品
                </h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" value="${giftInfo.id}">
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>

                        <tr>
                            <td width="15%" class="text-right"><b style="color:red;">*</b>礼包名称：</td>
                            <td width="35%" class="text-left">
                                <input type="text" required class="form-control" name="name" value="${giftInfo.name}" >
                            </td>

                            <td width="15%" class="text-right"><b style="color:red;">*</b>礼包类型：</td>
                            <td width="35%" class="text-left">
                                <select class="form-control" name="type">
                                    <option value=""></option>
                                    <option value="0" <c:if test="${giftInfo.type == 0}">selected</c:if>>新手卡</option>
                                    <option value="1" <c:if test="${giftInfo.type == 1}">selected</c:if>>礼包</option>
                                    <option value="2" <c:if test="${giftInfo.type == 2}">selected</c:if>>激活码</option>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td width="15%" class="text-right"><b style="color:red;">*</b>游戏：</td>
                            <td width="85%" colspan="3" class="text-left">
                                <button id="<%=selectGameBtnId%>" type="button" class="btn btn-primary btn-sm">选择</button>
                                <input type="hidden" name="gameId" value="${giftInfo.gameId}">
                                <div id="<%=gameContainerId%>">

                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>上线时间：</td>
                            <td class="text-left">
                                <input type="text" required class="form-control fui-date" name="startTime" format="YYYY-MM-DD HH:mm:ss" value="<fmt:formatDate value="${giftInfo.startTime}" pattern="yyyy-MM-dd HH:mm:ss"/>" >
                            </td>

                            <td class="text-right"><b style="color:red;">*</b>截止日期：</td>
                            <td class="text-left">
                                <input type="text" required class="form-control fui-date" name="endTime" format="YYYY-MM-DD HH:mm:ss" value="<fmt:formatDate value="${giftInfo.endTime}" pattern="yyyy-MM-dd HH:mm:ss"/>" >
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>礼包详情：</td>
                            <td class="text-left" colspan="3">
                                <textarea class="form-control" style="height: 100px;" required name="description">${giftInfo.description}</textarea>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>使用说明：</td>
                            <td class="text-left" colspan="3">
                                <textarea class="form-control" style="height: 100px;" required name="instructions">${giftInfo.instructions}</textarea>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">礼包状态：</td>
                            <td class="text-left" colspan="3">
                                <label class="radio-inline">
                                    <input type="radio" name="status" value="0" <c:if test="${giftInfo == null || giftInfo.status == 0}">checked</c:if>> 关闭
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="status" value="1" <c:if test="${giftInfo.status == 1}">checked</c:if>> 开启
                                </label>

                            </td>
                        </tr>

                        <%--<tr>--%>
                            <%--<td class="text-right">淘号状态：</td>--%>
                            <%--<td class="text-left" colspan="3">--%>
                                <%--<label class="radio-inline">--%>
                                    <%--<input type="radio" name="washStatus" value="0" <c:if test="${giftInfo == null || giftInfo.washStatus == 0}">checked</c:if>> 关闭--%>
                                <%--</label>--%>
                                <%--<label class="radio-inline">--%>
                                    <%--<input type="radio" name="washStatus" value="1" <c:if test="${giftInfo.washStatus == 1}">checked</c:if>> 开启--%>
                                <%--</label>--%>
                            <%--</td>--%>
                        <%--</tr>--%>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    requirejs(['jquery','Fui'],function(){
        var $form = $("#<%=formId%>");
        $form.data("paramFn",function(){
            if(!$form.find("input[name='gameId']").val()){
                layer.msg("请选择游戏");
                return false;
            }
            var startTimeVal = $form.find("input[name='startTime']").val();
            var endTimeVal = $form.find("input[name='endTime']").val();
            if(startTimeVal && endTimeVal){
                var  startTime = new Date((startTimeVal).replace(new RegExp("-","gm"),"/")).getTime();
                var  endTime = new Date((endTimeVal).replace(new RegExp("-","gm"),"/")).getTime();
                if(endTime < startTime){
                    layer.msg("截止时间必须大于开始时间");
                    return false;
                }
            }
        });

        var $gameContainer = $("#<%=gameContainerId%>");
        var showGameInfo = function(icon,name){
            BaseUtil.addSingleImage($gameContainer,{src:icon},{size:"50",height:50,width:50});
            setTimeout(function(){
                $gameContainer.append("<div style='height:50px;line-height:50px;float: left'><strong>"+name+"</strong><div>")
            },200);
        }

        $("#<%=selectGameBtnId%>").click(function(){
             var gameSelector = new Game(function(data){
                 $form.find("input[name='gameId']").val(data.id);
                 showGameInfo(data.icon,data.name);
                 gameSelector.close();
             });
            gameSelector.select();
        });

       <c:if test="${giftInfo != null}">
        showGameInfo("${versionInfo.icon.src}","${gameInfo.name}");
       </c:if>
    });
</script>