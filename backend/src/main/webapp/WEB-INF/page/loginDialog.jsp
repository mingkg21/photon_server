<%@ page language="java" pageEncoding="utf-8"%>
<form method="post" action="${base}/login" unReloadTable='true' class="form-submit form-horizontal" id="login_dialog_form_id">
<div class="modal-dialog fui-box">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close fui-close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">登录超时，重新登录</h4>
		</div>
		<div class="modal-body">
			<fieldset>
				<div class="input-group input-group-lg">
					<span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
					<input name="account" type="text" class="form-control" placeholder="用户名" readonly>
				</div>
				<div class="clearfix"></div><br><input type="hidden" name="serverId">
				<div class="input-group input-group-lg">
					<span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
					<input name="password" type="password" class="form-control required" placeholder="密码" minlength="6">
				</div>
			</fieldset>
		</div>
		<div class="modal-footer">
			<button type="submit" class="btn btn-primary">登 录</button>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
$(function() {
	var $form=$("#login_dialog_form_id");
	$form.find("[name='account']").val(baseInfo.account);
});
</script>