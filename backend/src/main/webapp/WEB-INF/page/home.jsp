<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>管理后台</title>
    <meta name="renderer" content="webkit">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${base}/common/css/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="${base}/common/css/main.min.css?v=1.1">
    <link rel="stylesheet" type="text/css" href="${base}/module/css/base.css?v=1.0">
    <style type="text/css">
        .ke-icon-video{
            background-video: url('${base}/common/js/kindeditor/themes/default/default.png');
            background-position: 0px  -528px;
            width: 16px;
            height: 16px;
        }
    </style>

    <script>
        var baseInfo={
            account:"${loginUser.account}",
            baseUrl:"${base}",
            loginDialogUrl:"${base}/loginWindow",
            indexUrl:"${base}",
            navUrl:"${base}/system/menu/getMenu",
            cdn:"${cdn}",
            domainWap:"${doaminWap}"
        };
    </script>


    <script src="${base}/common/js/require.js"></script>
    <script>
        require.config({
            baseUrl: '${base}/common/js',
            paths: {
                jquery: 'jquery-1.12.4.min'
                ,jquery_validation:'jquery-validate/jquery.validate.js?v=1.0.0'
                ,bootstrap:'bootstrap/3.3.7/js/bootstrap.min'
                ,bootstrap_editable:'bootstrap/editable/js/bootstrap-editable.min'
                ,bootstrapTable:'bootstrap-table/bootstrap-table.min'
                ,moment:'moment/moment.min'
                ,moment_zh:'moment/locale/zh-cn'
                ,datetimepicker: 'bootstrap-datetimepicker/js/bootstrap-datetimepicker.min'
                ,layer:'layer3/layer.min'
                ,selectize:'selectize/selectize'
                ,Fui:'fui/fui.js?v=1.0.34'
                ,fui_table:'fui/fui-table.js?v=1.0.0'
                ,fui_form:'fui/fui-form.js?v=1.0.0'
                ,fui_datetime:'fui/fui-datetime.js?v=1.0.0'
                ,fui_switch:'fui/fui-switch.js?v=1.0.0'
                ,fui_selectize:'fui/fui-selectize.js?v=1.0.1'
                ,template:'artTemplate/template'
                ,bootstrapSwitch:'bootstrap/switch/js/bootstrap-switch.min'
                ,chart:'chart/2.7.3/Chart.min'
                ,qiniu:'cdn/qiniu.min'
                ,qiniu_plupload:'cdn/plupload.full.min'
                ,qiniu_moxie:'cdn/moxie'
                ,kindeditor:'kindeditor/kindeditor-all'
                ,fui_kindeditor:'kindeditor/fui-kindeditor.js?v=1.0.6'
            },
            map: {
                '*': {
                    'css': 'require-lib/css.min'
                    ,'text': 'require-lib/text'
                }
            },
            shim: {
                'bootstrap':['jquery']
                ,'bootstrap_editable':['jquery','bootstrap','datetimepicker','css!${base}/common/js/bootstrap/editable/css/bootstrap-editable.css']
                ,'bootstrapTable':['jquery','bootstrap','css!${base}/common/js/bootstrap-table/bootstrap-table.css']
                ,'datetimepicker':['jquery','css!${base}/common/js/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css']
                ,'bootstrapSwitch':['jquery','css!${base}/common/js/bootstrap/switch/css/bootstrap-switch.min.css']
                ,'selectize':['jquery','css!${base}/common/js/selectize/override.css?v=1.0.0']
                ,'kindeditor':['css!${base}/common/js/kindeditor/themes/default/default.css']

                //,"${base}/common/js/kindeditor/lang/zh-CN.js"
            }
        });
        require([
            "${base}/common/js/main.js?v=1.0"
            ,"${base}/module/js/window.js?v=1.0.10"
            ,"${base}/common/js/util.js?v=1.0.4"
        ],function(){


        });
    </script>
</head>
<body>

<nav class="navbar navbar-default navbar-fui" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <span class="navbar-brand logo-wrap"><i class="logo"></i>管理后台</span>
        </div>
        <div class="navbar-collapse collapse" aria-expanded="false">
            <ul class="nav navbar-nav">
                <li><div class="admin-side-toggle"><i class="fa fa-bars" aria-hidden="true"></i></div></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <%-- <li class="nav-item"><a href="?page=old">使用旧版本</a></li>
                <li class="nav-item"><a href="${base }/changeServer.do" class="open-dialog">切换服务器</a></li> --%>
                <li class="dropdown hidden-xs nav-item"><a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">${loginUser.account}&nbsp;&nbsp;<i class="fa fa-caret-down"></i>
                </a>
                    <ul class="dropdown-menu">
                        <li><a href="${base}/modifyPwd" class="open-dialog">修改密码</a></li>
                        <li class="divider"></li>
                        <li><a tabindex="-1" href="${base}/logout">退出</a></li>
                    </ul>
                    <i class="bottom-border"></i>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div id="fui_sidebar" class="fui-sidebar">
    <div class="sidebar-scroll navbar-default" id="fui_sidebar_nars"></div>
</div>
<div class="fui-content" id="fui_content_id">
    <div class="fui-tab-wrap" id="fui_tab_wrap_id"></div>
</div>
<div class="fui-footer" id="fui_footer_id">
    <div class="fui-fmain">2018 © <a href="${base}">光子游戏</a>
     <!-- for  copy -->
    <input id="copy_text_field" type="text" style="height:1px;width: 5px;opacity: 0" /></div>
</div>


<c:if test="${not empty content_page && content_page!=''}">
    <script type="text/html" class="direct_open_url" url="${base}${content_url}"><!--<jsp:include page="${content_page}"></jsp:include>//--></script>
</c:if>
</body>

</html>

<jsp:include page="../include/video_tpl.jsp"></jsp:include>
<jsp:include page="../include/image_tpl.jsp"></jsp:include>