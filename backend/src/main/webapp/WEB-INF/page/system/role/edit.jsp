<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/system/role/saveOrUpdate" class="form-submit">
	<input type="hidden" name="id" value="${role.id}">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
				<h4 class="modal-title">
					编辑用户
				</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered table-striped" style="clear: both">
					<tbody>
					<tr>
						<td width="15%" class="text-right">角色名：</td>
						<td width="35%" class="text-left">
							<input name="name" value="${role.name}" class="form-control" type="text">
						</td>
					</tr>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">保存</button>
				<button type="button" class="btn btn-default fui-close">关闭</button>
			</div>
		</div>
	</div>
</form>

<script type="text/javascript">
//    requirejs(['jquery','bootstrap','Fui'],function(){
//        var $form = Fui.getParentNode().find("form");
//
//    });
</script>

