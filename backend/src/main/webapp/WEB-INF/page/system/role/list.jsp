<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" dialog-title="新增角色" url="${base}/system/role/goAddPage"   class="btn btn-default open-dialog" >新增</button>
	</div>
</div>

<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
        //var $div = Fui.getParentNode();
        Fui.addBootstrapTable({
            url : '${base}/system/role/list',
            queryParams : function(params){
                return params;
            },
            columns : [{
                field : 'name',
                title : '角色',
                align : 'center',
                width : '200',
                valign : 'middle'
            },{
                title : '操作',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : function (value,row,index) {
                    return [ '<a class="open-dialog" dialog-title="编辑角色" href="${base}/system/role/goEditPage?id='
                        ,row.id,'" title="修改">',
                        '<i class="glyphicon glyphicon-edit"></i>', '</a> ',
                        '<a class="todo-ajax" href="${base}/system/role/delete?id=',row.id, '" confirm="是否确认要删除?" title="删除">',
                        '<i class="glyphicon glyphicon-trash"></i>', '</a> ' ]
                        .join('');
                }
            } ]
        });
    });
</script>

