<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<form action="${base}/system/permission/savePermission" unReloadTable="true" class="form-submit">
	<div class="panel panel-default">
		<div class="panel-heading">
			<div>
				<div class="form-inline">
					<select name="roleId" class="form-control" >
						<option value="">---请选择角色---</option>
						<c:forEach var="role" items="${roles}">
							<option value="${role.id}" <c:if test="${role.id == user.roleId}">checked</c:if>>${role.name}</option>
						</c:forEach>
					</select>
					<!--disabled="disabled"-->
					<button class="btn btn-primary save-btn" disabled>保存</button>
				</div>
			</div>
		</div>
		<div class="panel-body" >
			<c:forEach var="fstNode" items="${rootNode.children}">
				<div class="checkbox">
					<label>
						<input type="checkbox" name="menuId" parent="${fstNode.parentId}" value="${fstNode.id}"> ${fstNode.title}
					</label>
					<br>
					<div class="secCheckBox">
						<c:forEach var="secNode" items="${fstNode.children}">
							<label>
								<input type="checkbox" name="menuId" parent="${secNode.parentId}" value="${secNode.id}"> ${secNode.title}
							</label>
							<br>
							<div class="thdCheckBox" style="margin-left:100px;">
								<c:forEach var="thdNode" items="${secNode.children}">
									<label checkbox-inline >
										<input type="checkbox" name="menuId" class="end"  parent="${thdNode.parentId}" value="${thdNode.id}"> ${thdNode.title}
									</label>
								</c:forEach>
							</div>
						</c:forEach>
					</div>
				</div>
			</c:forEach>
		</div>
	</div>
</form>




<script type="text/javascript" >
    requirejs(['jquery','bootstrap','Fui'],function(){
        var $form = Fui.getParentNode().find("form");
        var $saveBtn = $($form.find(".save-btn"));
        var $all =   $($form.find(":checkbox"));

        var selector = $form.find("select[name=roleId]");
        $(selector).change(function(){
            $saveBtn.attr("disabled","disabled");
           	if(!this.value){
                return;
			}
            $.ajax({
				url:"${base}/system/permission/getPermission",
				data:{roleId:this.value},
				success:function(data){
                    $saveBtn.removeAttr("disabled");
                    unSelectAll();
                    for(var i=0;i<data.menus.length;i++){
                        var m = data.menus[i];
                        $all.each(function(){
                            if(this.value == m.id){
                                this.checked = true;
							}
						})
					}
				}
			})
		});

        function unSelectAll(){
            $all.each(function(){
                this.checked = false;
			})
		}

//        $saveBtn.click(function(){
//
//		});

        //绑定向上和向下的级联操作
        $all.change(function() {
            if('end' == $(this).attr('class') && !$(this).is(':checked')){
                parentMenu(this);
                return;
            }
            checkMenu(this);
        });

        function checkMenu(chk) {
            sonMenu(chk);
            parentMenu(chk);
        }

        function sonMenu(chk) {
            var val = $(chk).val();
            var curChecked = chk.checked;
            //点击当前checkbox下级的处理
            var others = $form.find(":checkbox[parent='"+val+"']");
            if (others === null || others === undefined || others.length === 0) {
                return
            }
            var len = others.length;
            others.each(function() {
                this.checked = curChecked;
                //递归调用
                sonMenu(this);
            });
        }

        function parentMenu(chk) {
            var curChecked = chk.checked;
            var par = $(chk).attr("parent")
            var others = $(":checkbox[parent='" + par + "']:checked");//同级别其他元素
            if (others === null || others === undefined) {
                return
            }
            var len = others.length;
            //点击当前checkbox上级的处理
            if ((len == 0 && !curChecked) || (len == 1 && curChecked)) {
                var chkson = $form.find(":checkbox[value='"+par+"']");
                if(chkson.length == 0){
                    return;
                }
                chkson[0].checked = curChecked;
                //递归调用
                parentMenu(chkson[0]);
            }
        }
    });
</script>



