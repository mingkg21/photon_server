<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" dialog-title="新增用户"  href="${base}/system/user/goAddPage" class="btn btn-default open-dialog">新增</button>
	</div>
</div>

<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
        //var $div = Fui.getParentNode();
        Fui.addBootstrapTable({
            url : '${base}/system/user/list',
            queryParams : function(params){
                return params;
            },
            columns : [ {
                field : 'account',
                title : '用户账号',
                align : 'center',
                width : '200',
                valign : 'middle',
            },{
                field : 'userName',
                title : '真实姓名',
                align : 'center',
                width : '200',
                valign : 'middle'
			},{
                field : 'roleName',
                title : '角色',
                align : 'center',
                width : '200',
                valign : 'middle'
            }, {
                field : 'status',
                title : '状态',
                align : 'center',
                width : '200',
                valign : 'middle',
                formatter : function (value, row, index) {
                    return Fui.statusFormatter({onVal:1,offVal:2,val:value,url:'${base}/system/user/updateStatus?id='+row.id+"&status="});
                }
            }, {
                title : '操作',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : function (value,row,index) {
                    return [ '<a class="open-dialog" dialog-title="编辑用户信息" href="${base}/system/user/goEditPage?id='
                        ,row.id,'" title="修改">',
                        '<i class="glyphicon glyphicon-edit"></i>', '</a> ',
                        '<a class="todo-ajax" href="${base}/system/user/resetPassword?userId=',row.id, '" confirm="是否确认要重置密码?" title="重置密码(888888)">',
						,'<i class="glyphicon glyphicon-refresh"></i>', '</a> ',
                        '<a class="todo-ajax" href="${base}/system/user/delete?id=',row.id, '" confirm="是否确认要删除?" title="删除">',
                        '<i class="glyphicon glyphicon-trash"></i>', '</a> ' ]
                        .join('');
                }
            } ]
        });
    });
</script>

