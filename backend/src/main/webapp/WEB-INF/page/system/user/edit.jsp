<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/system/user/saveOrUpdate" class="form-submit">
	<input type="hidden" name="id" value="${user.id}">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
				<h4 class="modal-title">
					编辑用户
				</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered table-striped" style="clear: both">
					<tbody>
					<tr>
						<td width="15%" class="text-right">用户账号：</td>

						<td width="35%" class="text-left">
							<c:if test="${user.id != null}">
								<input name="account" type="hidden" value="${user.account}">
								${user.account}
							</c:if>
							<c:if test="${user.id == null}">
								<input name="account" required class="form-control" value="${user.account}" type="text">
							</c:if>
						</td>
					</tr>
					<tr>
						<td width="15%" class="text-right">用户分组：</td>
						<td width="35%" class="text-left">
							<select name="roleId" required class="form-control">
								<option value="">---请选择---</option>
								<c:forEach var="role" items="${roles}">
									<option value="${role.id}" <c:if test="${role.id == user.roleId}">selected</c:if>>${role.name}</option>
								</c:forEach>
							</select>
						</td>
					</tr>

					<tr>
						<td width="15%" class="text-right">真实姓名：</td>
						<td width="35%" class="text-left"><input name="userName" value="${user.userName}" required class="form-control" type="text"></td>
					</tr>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">保存</button>
				<button type="button" class="btn btn-default fui-close">关闭</button>
			</div>
		</div>
	</div>
</form>

<script type="text/javascript">
//    requirejs(['jquery','bootstrap','Fui'],function(){
//        var $form = Fui.getParentNode().find("form");
//
//    });
</script>

