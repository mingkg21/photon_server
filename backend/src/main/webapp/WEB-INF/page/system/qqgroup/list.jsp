<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" class="btn btn-default open-dialog"  dialog-title="新增Q群" href="${base}/system/qqgroup/goAddPage" >新增</button>
	</div>
</div>

<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
       // var $div = Fui.getParentNode();
        Fui.addBootstrapTable({
            url : '${base}/system/qqgroup/list',
            columns : [{
                field : 'title',
                title : '标题',
                align : 'center',
                width : '80',
                valign : 'middle'
            },{
                field : 'groupNumber',
                title : '群号',
                align : 'center',
                width : '60',
                valign : 'middle'
			},{
                field : 'description',
                title : '描述',
                align : 'center',
                width : '120',
                valign : 'middle'
			},{
                field : 'status',
                title : '状态',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : function (v,row) {
					return Fui.statusFormatter({
						val : v,
						url:'${base}/system/qqgroup/updateStatus?id='+row.id+'&status='
					});
                }
            },{
                field : 'ordering',
                title : '排序',
                align : 'center',
                width : '60',
                valign : 'middle'
            },{
                title : '操作',
                align : 'center',
                width : '60',
                valign : 'middle',
                formatter : function(v,row){
                    return [ '<a class="open-dialog" dialog-title="修改Q群" href="${base}/system/qqgroup/goEditPage?id=',
							row.id,'" title="修改Q群">',
						'<i class="glyphicon glyphicon-edit"></i>', '</a>'
					].join("");
				}
            }]
        });
    });
</script>

