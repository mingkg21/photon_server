<%@ page import="java.util.UUID" %>
<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
    String selectIconBtnId = java.util.UUID.randomUUID().toString();
    String iconContainerId = java.util.UUID.randomUUID().toString();
    String formId =  java.util.UUID.randomUUID().toString();
%>


<form id="<%=formId%>" action="${base}/system/qqgroup/saveOrUpdate" class="form-submit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    编辑Q群
                </h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" value="${qqGroup.id}">
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>
                        <tr>
                            <td width="20%" class="text-right"><b style="color:red;">*</b>标题：</td>
                            <td width="80%" class="text-left">
                                <input type="text" required class="form-control" value="${qqGroup.title}" name="title"/>
                            </td>
                        </tr>

                        <tr>
                            <td width="20%" class="text-right"><b style="color:red;">*</b>图标：</td>
                            <td width="80%" class="text-left">
                                <button id="<%=selectIconBtnId%>" class="btn btn-primary btn-sm" type="button">选择</button>
                                <input type="hidden" name="icon" value="${qqGroup.icon.src}">
                                <div id="<%=iconContainerId%>">

                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td width="20%" class="text-right"><b style="color:red;">*</b>群号：</td>
                            <td width="80%" class="text-left">
                                <input type="number" required class="form-control" value="${qqGroup.groupNumber}" name="groupNumber" />
                            </td>
                        </tr>


                        <tr>
                            <td width="20%" class="text-right"><b style="color:red;">*</b>key：</td>
                            <td width="80%" class="text-left">
                                <input type="text" required class="form-control" value="${qqGroup.key}" name="key" />
                            </td>
                        </tr>

                        <tr>
                            <td width="20%" class="text-right"><b style="color:red;">*</b>排序：</td>
                            <td width="80%" class="text-left">
                                <input type="number" required class="form-control" value="${qqGroup.ordering}" name="ordering" />
                            </td>
                        </tr>

                        <tr>
                            <td width="20%" class="text-right">描述：</td>
                            <td width="80%" class="text-left">
                                <textarea class="form-control" name="description">${qqGroup.description}</textarea>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
        var $iconContainer = $("#<%=iconContainerId%>");
        var $form = $("#<%=formId%>");

        $form.data("paramFn",function() {
            if (!$form.find("input[name='icon']").val()) {
                layer.msg("请选择Q群图标");
                return false;
            }
        });

        $("#<%=selectIconBtnId%>").click(function () {
            var pic = new Picture(function(data){
                BaseUtil.addSingleImage($iconContainer,data,{width:50,height:50,size:"50"});
                $form.find("input[name='icon']").val(data.src);
                pic.close();
            },false);
            pic .select();
        });

        <c:if test="${not empty qqGroup.icon}">
        BaseUtil.addSingleImage($iconContainer,{src:"${qqGroup.icon.src}"},{width:50,height:50,size:"50"});
        </c:if>
    });
</script>
