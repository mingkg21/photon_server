<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/system/menu/saveOrUpdate" class="form-submit">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
				<h4 class="modal-title">
					编辑菜单
				</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" name="parentId" value="${parentId}">
				<input type="hidden" name="id" value="${menu.id}">
				<input type="hidden" name="level" value="${level}">
				<table class="table table-bordered table-striped" style="clear: both">
					<tbody>
					<tr>
						<td width="20%" class="text-right">上级目录：</td>
						<td width="80%" class="text-left"><span class="parentName">${parentName}</span></td>
					</tr>
					<tr>
						<td width="20%" class="text-right">名称：</td>
						<td width="80%" class="text-left"><input type="text" required class="form-control" value="${menu.name}" name="name" placeholder="名称" /></td>
					</tr>
					<tr>
						<td width="20%" class="text-right">页面URL：</td>
						<td width="80%" class="text-left"><input type="text" class="form-control" name="url" value="${menu.url}" placeholder="页面URL" /></td>
					</tr>
					<tr>
						<td width="20%" class="text-right">功能URL：</td>
						<td width="80%" class="text-left"><input type="text" class="form-control" name="modulePath" value="${menu.modulePath}" placeholder="功能URL" /></td>
					</tr>
					<tr>
						<td width="20%" class="text-right">状态：</td>
						<td width="80%" class="text-left">
							<select name="status" class="form-control">
								<option value="1" <c:if test="${menu.status == 1}">selected</c:if>>禁用</option>
								<option value="2" <c:if test="${menu.status == 2}">selected</c:if>>启用</option>
								<option value="3" <c:if test="${menu.status == 3}">selected</c:if>>隐藏</option>
							</select>
						</td>
					</tr>
					<tr>
						<td width="20%" class="text-right">排序值：</td>
						<td width="80%" class="text-left"><input type="text" required class="form-control" value="${menu.sort}" name="sort" placeholder="排序值" /></td>
					</tr>
					<tr>
						<td width="20%" class="text-right">图标：</td>
						<td width="80%" class="text-left"><input type="text" class="form-control" value="${menu.icon}" name="icon" placeholder="图标" /></td>
					</tr>
					<tr>
						<td width="20%" class="text-right">备注：</td>
						<td width="80%" class="text-left"><textarea class="form-control" value="${menu.remark}" name="remark"></textarea></td>
					</tr>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">保存</button>
				<button type="button" class="btn btn-default fui-close">关闭</button>
			</div>
		</div>
	</div>
</form>

<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
        var $form = Fui.getParentNode().find("form");

    });
</script>

