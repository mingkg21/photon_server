<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button"  class="btn btn-primary pre-level"  >返回上一级</button>
		<button type="button"  class="btn btn-default add-menu" >新增</button>
	</div>
</div>

<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
       // var $div = Fui.getParentNode();

        var ps = [ {
            "id" : 0,
            "name" : "根目录"
        } ]; //轨迹
        var parentId = 0; //当前父节点ID
        var parentName = "根目录"; //当前父节点名称
        var level = 1;

        $($.find(".add-menu")).click(function(){
            console.info(11111);
            Fui.openDialog({
				title:"新增菜单",
				url:"${base}/system/menu/goAddPage?parentId="+parentId
			})
		})

        $($.find(".pre-level")).click(function(){
            var index = ps.length - 2;
            if (parentId == 0 || index < 0) {
                layer.msg("当前已经是最顶级了");
                return;
            }
            data = ps[index];
            parentId = data.id;
            parentName = data.name;
            level = data.level;
            var nps = [];
            for (var i = 0; i < ps.length; i++) {
                if (i == ps.length - 1) {
                    break;
                }
                nps.push(ps[i]);
            }
            ps = nps;
            var $table=$(".fui-box.active").data("bootstrapTable");
            $table.bootstrapTable('refresh');

        });

        function down(id, name, lvl) {
            var data = {};
            data.id = id
            data.name = name;
            parentId = id;
            parentName = name;
            level = lvl+ 1;
            ps.push(data);
            var $table=$(".fui-box.active").data("bootstrapTable");
            $table.bootstrapTable('refresh');
        }



        var operateEvents = {
            'click .down' : function(e, value, row, index) {
                down(row.id, row.name, row.level);
            }
        };
        Fui.addBootstrapTable({
            url : '${base}/system/menu/list',
            queryParams : function(params){
                params["parentId"] = parentId;
                //params.type = $form.find("[name='type']").val();
               // params.account = $form.find("[name='account']").val();
                return params;
            },
            columns : [ {
                field : 'name',
                title : '名称',
                align : 'center',
                width : '200',
                valign : 'middle',
                formatter : nameFormatter
            }, {
                field : 'status',
                title : '状态',
                align : 'center',
                width : '200',
                valign : 'middle',
                formatter : statusFormatter
            }, {
                field : 'level',
                title : '级别',
                align : 'center',
                width : '200',
                valign : 'middle'
            }, {
                field : 'sort',
                title : '排序',
                align : 'center',
                width : '200',
                valign : 'middle'
            }, {
                title : '操作',
                align : 'center',
                width : '200',
                valign : 'middle',
                events : operateEvents,
                formatter : operateFormatter
            } ]
        });

        function nameFormatter(value, row, index) {
        	return ['<a class="open-dialog" dialog-title="编辑菜单" href="${base}/system/menu/goEditPage?parentId=',parentId,'&id=',row.id,'">',value,'</a>'].join('');
//            return [
//                "<a href='javascript:void(0);' class='link' onclick='down("
//                + row.id + ",\"" + row.name + "\"," + row.level
//                + ");'>", "</a>" ].join(value);
        }

        function statusFormatter(value, row, index) {
            if(value == 1){
                return '<span class="text-danger">禁用</span>';
            }
            if (value === 2) {
                return '<span class="text-success">启用</span>';
            }
            if(value == 3){
                return '<span class="text-danger">隐藏</span>';
            }
        }

        function operateFormatter(value, row, index) {
            return [ '<a class="down" href="javascript:void(0)" title="下级">',
                '<i class="glyphicon glyphicon-arrow-down"></i>', '</a>  ',
                '<a class="open-dialog" dialog-title="编辑菜单" href="${base}/system/menu/goEditPage?parentId=',parentId,'&id=',row.id,'" title="修改">',
                '<i class="glyphicon glyphicon-edit"></i>', '</a>  ',
                '<a class="todo-ajax" href="${base}/system/menu/updateStatus?id=',row.id,'&status=2" confirm="是否确认要启用?" title="启用">',
                '<i class="glyphicon glyphicon-ok-circle"></i>', '</a>  ',
                '<a class="todo-ajax" href="${base}/system/menu/updateStatus?id=',row.id,'&status=1" confirm="是否确认要禁用?"  title="禁用">',
                '<i class="glyphicon glyphicon-ban-circle"></i>', '</a>', ]
                .join('');
        }
    });
</script>

