<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">

			<div class="input-group">
				<input type="number" class="form-control" name="userId" placeholder="用户ID">
			</div>
			<button class="btn btn-primary fui-date-search">查询</button>
		</div>
	</div>
</form>

<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
        Fui.addBootstrapTable({
            url : '${base}/content/suggestion/list',
            columns : [{
                title : '用户ID',
                align : 'center',
                width : '50',
                valign : 'middle',
                field : 'userId'
            },{
                title : '问题类型',
                align : 'center',
                width : '60',
                valign : 'middle',
                formatter:function(v,row) {

                    if(row.isReply == 1){
                        return "<font color='blue'>官方回复</font>";
					}

                    if(row.feedbackType == 1){
                        return "加速问题";
					}else if(row.feedbackType  == 2){
                        return "游戏问题";
					}else if(row.feedbackType == 3){
					    return "其他问题";
					}
                }
            },{
                title : '内容',
                align : 'center',
                width : '200',
                valign : 'middle',
                field : 'content',
                formatter:function(v,row) {
                    return "<div style='max-width:400px;word-break:break-all;'>"+ Fui.formatXss(v)+"</div>";
                }
			},{
                field : 'createTime',
                title : '创建时间',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : Fui.formatDatetime
			},{
                title : '操作',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : function(v,row){

                    if(row.isReply == 1){
                        return;
					}

                   var arr = [
                       '<a class="open-dialog" dialog-title="回复反馈" href="${base}/content/suggestion/goReplyPage?userId=',
                        row.userId,'" title="回复反馈">',
                        '<i class="glyphicon glyphicon-edit"></i>',
					   '</a> '
				   ];
                    return arr.join("");
				}
            }]
        });
    });
</script>

