<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String formId = java.util.UUID.randomUUID().toString();
%>
<form id="<%=formId%>"  action="${base}/content/suggestion/reply" class="form-submit">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                     回复
                </h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>

                        <tr>
                            <td width="30%" class="text-right">用户ID：</td>
                            <td width="70%" class="text-left">
                                <input type="text" readonly required class="form-control" name="userId" value="${userId}" >
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>内容：</td>
                            <td class="text-left">
                                <textarea class="form-control" name="content" rows="3"></textarea>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>