<%@ page import="java.util.UUID" %>
<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%
    String formId = java.util.UUID.randomUUID().toString();

    String selectBannerBtnId = java.util.UUID.randomUUID().toString();
    String bannerContainerId = java.util.UUID.randomUUID().toString();
%>

<form id="<%=formId%>" action="${base}/content/ad/saveOrUpdateItem" class="form-submit">
    <input type="hidden" name="id" value="${item.id}">
    <input name="adId" type="hidden" value="${adId}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    创建广告项
                </h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>

                        <tr>
                            <td width="20%" class="text-right"><b style="color:red;">*</b>标题：</td>
                            <td width="80%" class="text-left">
                                <input type="text" required class="form-control" value="${item.title}" name="title" placeholder="标题" />
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>开始时间：</td>
                            <td class="text-left">
                                <input required type="text" value="<fmt:formatDate value='${item.startTime}' pattern='yyyy-MM-dd HH:mm:ss'/>" class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" name="startTime" placeholder="开始时间" />
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>结束时间：</td>
                            <td class="text-left">
                                <input required type="text" value="<fmt:formatDate value='${item.endTime}' pattern='yyyy-MM-dd HH:mm:ss'/>"  class="form-control fui-date" format="YYYY-MM-DD HH:mm:ss" name="endTime" placeholder="结束时间" />
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>广告类型：</td>
                            <td class="text-left">
                                <select class="form-control" required name="objectType">
                                    <option value="">---请选择---</option>

                                    <c:if test="${typeMap['app']}">
                                        <option value="app" <c:if test="${'app' eq item.objectType}"> selected </c:if>>应用</option>
                                    </c:if>

                                    <c:if test="${typeMap['tag']}">
                                        <option value="tag" <c:if test="${'tag' eq item.objectType}"> selected </c:if>>标签</option>
                                    </c:if>
                                    <c:if test="${typeMap['category']}">
                                        <option value="category" <c:if test="${'category' eq item.objectType}"> selected </c:if>>游戏分类</option>
                                    </c:if>
                                    <c:if test="${typeMap['special_tag']}">
                                        <option value="special_tag" <c:if test="${'special_tag' eq item.objectType}"> selected </c:if>>特色标签</option>
                                    </c:if>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>对象ID：</td>
                            <td class="text-left">
                                <input type="number" required class="form-control" value="${item.objectId}" name="objectId" placeholder="对象ID" />
                            </td>
                        </tr>


                        <tr>
                            <td width="20%" class="text-right"><b style="color:red;">*</b>图片：</td>
                            <td width="80%" class="text-left">
                                <button id="<%=selectBannerBtnId%>" class="btn btn-primary btn-sm" type="button">选择</button>
                                <input type="hidden" name="picUrl" value="${item.picUrl}">
                                <div id="<%=bannerContainerId%>">

                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td width="20%" class="text-right"><b style="color:red;">*</b>排序：</td>
                            <td width="80%" class="text-left">
                                <input type="number" required class="form-control" value="${item.ordering}" name="ordering" placeholder="排序" />
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
        var $form = $("#<%=formId%>");
        $form.data("paramFn",function(){
            if(!$form.find("input[name='picUrl']").val()){
                layer.msg("请选择图片");
                return false;
            }
            var startTime =  new Date($form.find("input[name='startTime']").val());
            var endTime = new Date($form.find("input[name='endTime']").val());
            if(endTime.getTime() <= startTime.getTime()){
                layer.msg("结束时间必须大于开始时间");
                return false;
            }
        });

        var $bannerContainer = $("#<%=bannerContainerId%>");

        $("#<%=selectBannerBtnId%>").click(function () {
            var pic = new Picture(function(data){
                BaseUtil.addSingleImage($bannerContainer,data,{width:160,height:160,size:"160"});
                $form.find("input[name='picUrl']").val(data.src);
                pic.close();
            },false);
            pic .select();
        });

        <c:if test="${not empty item.picUrl}">
        BaseUtil.addSingleImage($bannerContainer,{src:"${item.picUrl}"},{width:160,height:160,size:"160"});
        </c:if>

    });
</script>
