<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" class="btn btn-default open-dialog"  dialog-title="新增广告位" href="${base}/content/ad/goAddPage" >新增</button>
	</div>
</div>

<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
        Fui.addBootstrapTable({
            url : '${base}/content/ad/list',
            columns : [{
                field : 'name',
                title : '名称',
                align : 'center',
                width : '200',
                valign : 'middle'
            },{
                field : 'code',
                title : '编码',
                align : 'center',
                width : '150',
                valign : 'middle'
			},{
                field : 'status',
                title : '状态',
                align : 'center',
                width : '100',
                valign : 'middle',
                formatter : function (v,row) {
					return Fui.statusFormatter({
						val : v,
						url:'${base}/content/ad/updateStatus?id='+row.id+'&status='
					});
                }
            },{
                title : '操作',
                align : 'center',
                width : '200',
                valign : 'middle',
                formatter : function(v,row){

                    return [ '<a class="open-dialog" dialog-title="修改" href="${base}/content/ad/goEditPage?id=',
							row.id,'" title="修改">',
						'<i class="glyphicon glyphicon-edit"></i>', '</a>',

                        ' <a class="open-tab" tab-id="tab-system-ad-area_',row.id,'" dialog-title="广告（',row.name,'）"  data-icon="glyphicon glyphicon-zoom-in" href="${base}/content/ad/goItemListPage?adId=',
                        row.id,'" title="查看广告集">',
                        '<i class="glyphicon glyphicon-zoom-in"></i>', '</a>'

					].join("");
				}
            } ]
        });
    });
</script>

