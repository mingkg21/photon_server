<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" class="btn btn-default open-dialog"  dialog-title="新增广告项" href="${base}/content/ad/goAddItemPage?adId=${adId}" >新增</button>
	</div>
</div>

<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
       // var $div = Fui.getParentNode();
        Fui.addBootstrapTable({
            url : '${base}/content/ad/getItemList?adId=${adId}',
            columns : [{
                field : 'title',
                title : '标题',
                align : 'center',
                width : '100',
                valign : 'middle'
            },{
                field : 'objectType',
                title : '类型',
                align : 'center',
                width : '50',
                valign : 'middle',
                formatter : function(value, row, index){
                    if(value == 'app'){
                        return "应用";
					}

					if(value == 'tag'){
                        return "标签";
					}

					if(value == 'category'){
					    return '游戏分类';
					}

                    if(value == 'special_tag') {
                        return '特色标签';
                    }
                    return "未知";
                }
            },{
                field : 'picUrl',
                title : '图片',
                align : 'center',
                width : '200',
                valign : 'middle',
                formatter : function(value, row, index){
                    return "<a href='"+(baseInfo.cdn+value)+"' target = '_blank'><img title='点击查看原图' src='"+(baseInfo.cdn+value)+"-preview'/></a>";
                }
			},{
                field : 'startTime',
                title : '开始时间',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter:Fui.formatDatetime
            },{
                field : 'startTime',
                title : '结束时间',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter:Fui.formatDatetime
            },{
                field : 'ordering',
                title : '排序',
                align : 'center',
                width : '60',
                valign : 'middle'
            },{
                field : 'createTime',
                title : '创建时间',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : Fui.formatDatetime
            },{
                title : '操作',
                align : 'center',
                width : '120',
                valign : 'middle',
                formatter : function(v,row){
                    return [
                        '<a class="open-dialog" dialog-title="修改" href="${base}/content/ad/goEditItemPage?itemId=',
                        row.id,'" title="修改">',
                        '<i class="glyphicon glyphicon-edit"></i>', '</a>',

                        ' <a class="todo-ajax" confirm="是否确认要删除？" href="${base}/content/ad/deleteItem?itemId=',
							row.id,'" title="删除">',
						'<i class="glyphicon glyphicon-trash"></i>', '</a>'].join("");
				}
            }]
        });
    });
</script>

