<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" dialog-title="新增渠道"  href="${base}/content/channelManage/goAddPage" class="btn btn-default open-dialog">新增</button>
	</div>
</div>

<table class="fui-default-table"></table>
<script type="text/javascript">
	requirejs(['jquery','bootstrap','Fui'],function(){
		//var $div = Fui.getParentNode();
		Fui.addBootstrapTable({
			url : '${base}/content/channelManage/list',
			queryParams : function(params){
				return params;
			},
			columns : [ {
				field : 'name',
				title : '渠道名称',
				align : 'center',
				width : '50',
				valign : 'middle',
			},{
				field : 'code',
				title : '渠道标识',
				align : 'center',
				width : '50',
				valign : 'middle'
			},{
				field : 'versionCode',
				title : 'APP版本号',
				align : 'center',
				width : '80',
				valign : 'middle'
			},{
				field : 'createTime',
				title : '创建时间',
				align : 'center',
				width : '80',
				valign : 'middle',
				formatter : Fui.formatDatetime
			}, {
				field : 'status',
				title : '状态',
				align : 'center',
				width : '50',
				valign : 'middle',
				formatter : function (v,row) {
					return Fui.statusFormatter({
						val : v,
						url:'${base}/content/channelManage/updateStatus?id='+row.id+'&status='
					});
				}
			}, {
				title : '操作',
				align : 'center',
				width : '80',
				valign : 'middle',
				formatter : function (value,row,index) {
					return [ '<a class="open-dialog" dialog-title="编辑渠道信息" href="${base}/content/channelManage/goEditPage?id='
						,row.id,'" title="修改">',
						'<i class="glyphicon glyphicon-edit"></i>']
							.join('');
				}
			} ]
		});
	});
</script>

