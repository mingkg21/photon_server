<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" dialog-title="新增渠道"  href="${base}/content/channel/goAddPage" class="btn btn-default open-dialog">新增</button>
	</div>
</div>

<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
        //var $div = Fui.getParentNode();
        Fui.addBootstrapTable({
            url : '${base}/content/channel/list',
            queryParams : function(params){
                return params;
            },
            columns : [ {
                field : 'name',
                title : '渠道名称',
                align : 'center',
                width : '200',
                valign : 'middle',
            },{
                field : 'code',
                title : '渠道标识',
                align : 'center',
                width : '200',
                valign : 'middle'
			}, {
                title : '操作',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : function (value,row,index) {
                    return [ '<a class="open-dialog" dialog-title="编辑渠道信息" href="${base}/content/channel/goEditPage?id='
                        ,row.id,'" title="修改">',
                        '<i class="glyphicon glyphicon-edit"></i>']
                        .join('');
                }
            } ]
        });
    });
</script>

