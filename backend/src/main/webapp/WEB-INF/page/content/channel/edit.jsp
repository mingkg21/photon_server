<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="${base}/content/channel/saveOrUpdate" class="form-submit">
	<input type="hidden" name="id" value="${channel.id}">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
				<h4 class="modal-title">
					编辑渠道
				</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered table-striped" style="clear: both">
					<tbody>
					<tr>
						<td width="15%" class="text-right">渠道名称：</td>

						<td width="35%" class="text-left">
							<c:if test="${channel.id != null}">
								<input name="name" type="hidden" value="${channel.name}">
								${user.account}
							</c:if>
							<c:if test="${channel.id == null}">
								<input name="name" required class="form-control" value="${channel.name}" type="text">
							</c:if>
						</td>
					</tr>
					<tr>
						<td width="15%" class="text-right">渠道标识：</td>
						<td width="35%" class="text-left"><input name="code" value="${channel.code}" required class="form-control" type="text"></td>
					</tr>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">保存</button>
				<button type="button" class="btn btn-default fui-close">关闭</button>
			</div>
		</div>
	</div>
</form>

<script type="text/javascript">
//    requirejs(['jquery','bootstrap','Fui'],function(){
//        var $form = Fui.getParentNode().find("form");
//
//    });
</script>

