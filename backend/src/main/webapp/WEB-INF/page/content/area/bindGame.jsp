<%@ page import="java.util.UUID" %>
<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
    String gameContainerId = java.util.UUID.randomUUID().toString();
    String selectGameBtnId = java.util.UUID.randomUUID().toString();
    String formId = java.util.UUID.randomUUID().toString();

    String selectBannerBtnId = java.util.UUID.randomUUID().toString();
    String clearBannerBtnId = java.util.UUID.randomUUID().toString();
    String bannerContainerId = java.util.UUID.randomUUID().toString();
%>

<form id="<%=formId%>" action="${base}/content/area/bindGame" class="form-submit">
    <input type="hidden" name="id" value="${item.id}">
    <input name="areaId" type="hidden" value="${areaId}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    绑定游戏
                </h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>
                        <tr>
                            <td width="15%" class="text-right"><b style="color:red;">*</b>游戏：</td>
                            <td width="85%" colspan="3" class="text-left">
                                <button id="<%=selectGameBtnId%>" type="button" class="btn btn-primary btn-sm">选择</button>
                                <input type="hidden" name="gameId" value="${item.gameId}">
                                <div id="<%=gameContainerId%>">

                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td width="20%" class="text-right">banner：</td>
                            <td width="80%" class="text-left">
                                <button id="<%=selectBannerBtnId%>" class="btn btn-primary btn-sm" type="button">选择</button>
                                <button id="<%=clearBannerBtnId%>" class="btn btn-danger btn-sm" type="button">清除</button>
                                <input type="hidden" name="banner" value="${item.banner.src}">
                                <div id="<%=bannerContainerId%>">

                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td width="20%" class="text-right"><b style="color:red;">*</b>排序：</td>
                            <td width="80%" class="text-left">
                                <input type="number" required class="form-control" value="${item.ordering}" name="ordering" placeholder="排序" />
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
        var $form = $("#<%=formId%>");
        $form.data("paramFn",function(){
            if(!$form.find("input[name='gameId']").val()){
                layer.msg("请选择游戏");
                return false;
            }
        });

        var $gameContainer = $("#<%=gameContainerId%>");
        var showGameInfo = function(icon,name){
            BaseUtil.addSingleImage($gameContainer,{src:icon},{size:"50",height:50,width:50});
            setTimeout(function(){
                $gameContainer.append("<div style='height:50px;line-height:50px;float: left'><strong>"+name+"</strong><div>")
            },200);
        }

        var paramStr = null;
        <c:if test="${'outerwear-game' eq areaCode}">
            paramStr = "versionType=2";
        </c:if>
        console.info('${areaCode}');
        <c:if test="${'test-game' eq areaCode}">
            paramStr = "showAll=true";
        </c:if>
        console.info(paramStr);
        $("#<%=selectGameBtnId%>").click(function(){
            var gameSelector = new Game(function(data){
                $form.find("input[name='gameId']").val(data.id);
                showGameInfo(data.icon,data.name);
                gameSelector.close();
            },paramStr);
            gameSelector.select();
        });

        <c:if test="${versionInfo != null}">
        showGameInfo("${versionInfo.icon.src}","${gameInfo.name}");
        </c:if>

        var $bannerContainer = $("#<%=bannerContainerId%>");

        $("#<%=clearBannerBtnId%>").click(function(){
            $bannerContainer.html("");
            $form.find("input[name='banner']").val("");
        });

        $("#<%=selectBannerBtnId%>").click(function () {
            var pic = new Picture(function(data){
                BaseUtil.addSingleImage($bannerContainer,data,{width:160,height:160,size:"160"});
                $form.find("input[name='banner']").val(data.src);
                pic.close();
            },false);
            pic .select();
        });

        <c:if test="${not empty item.banner}">
        BaseUtil.addSingleImage($bannerContainer,{src:"${item.banner.src}"},{width:160,height:160,size:"160"});
        </c:if>

    });
</script>
