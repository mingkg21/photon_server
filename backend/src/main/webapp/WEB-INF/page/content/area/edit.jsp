<%@ page import="java.util.UUID" %>
<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<form action="${base}/content/area/saveOrUpdate" class="form-submit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    编辑专区
                </h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" value="${area.id}">
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>
                        <tr>
                            <td width="20%" class="text-right">名称：</td>
                            <td width="80%" class="text-left">
                                <input type="text" required class="form-control" value="${area.name}" name="name" placeholder="名称" />
                            </td>
                        </tr>

                        <tr>
                            <td width="20%" class="text-right">编码：</td>
                            <td width="80%" class="text-left">
                                <input type="text" required class="form-control" value="${area.code}" name="code" placeholder="编码" />
                            </td>
                        </tr>

                        <tr>
                            <td width="20%" class="text-right">备注：</td>
                            <td width="80%" class="text-left">
                                <input type="text" required class="form-control" value="${area.remark}" name="remark" placeholder="备注" />
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){

    });
</script>
