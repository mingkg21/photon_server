<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" class="btn btn-default open-dialog"  dialog-title="新增专区" href="${base}/content/area/goAddPage" >新增</button>
	</div>
</div>

<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
       // var $div = Fui.getParentNode();
        Fui.addBootstrapTable({
            url : '${base}/content/area/list',
            columns : [{
                field : 'name',
                title : '名称',
                align : 'center',
                width : '80',
                valign : 'middle'
            },{
                field : 'code',
                title : '编码',
                align : 'center',
                width : '150',
                valign : 'middle'
			},{
				field : 'remark',
				title : '备注',
				align : 'center',
				width : '200',
				valign : 'middle'
			},{
                field : 'status',
                title : '状态',
                align : 'center',
                width : '100',
                valign : 'middle',
                formatter : function (v,row) {
					return Fui.statusFormatter({
						val : v,
						url:'${base}/content/area/updateStatus?id='+row.id+'&status='
					});
                }
            }, {
                field : 'appCount',
                title : '应用数量',
                align : 'center',
                width : '100',
                valign : 'middle'
			},{
                title : '操作',
                align : 'center',
                width : '`00',
                valign : 'middle',
                formatter : function(v,row){

                    return [ '<a class="open-dialog" dialog-title="修改专区" href="${base}/content/area/goEditPage?id=',
							row.id,'" title="修改">',
						'<i class="glyphicon glyphicon-edit"></i>', '</a>',

                        ' <a class="open-tab" tab-id="tab-system-area-games_',row.id,'" dialog-title="专区游戏（',row.name,'）"  data-icon="glyphicon glyphicon-zoom-in" href="${base}/content/area/goGameListPage?areaId=',
                        row.id,'" title="查看专区游戏集">',
                        '<i class="glyphicon glyphicon-zoom-in"></i>', '</a>'

					].join("");
				}
            } ]
        });
    });
</script>

