<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String formId = java.util.UUID.randomUUID().toString();
    String selectImgBtnId = java.util.UUID.randomUUID().toString();
    String imgContainerId = java.util.UUID.randomUUID().toString();
    String selectGameBtnId = java.util.UUID.randomUUID().toString();
    String gameContainerId = java.util.UUID.randomUUID().toString();
    String clearGameBtnId = java.util.UUID.randomUUID().toString();
%>
<form id="<%=formId%>"  action="${base}/content/screen/saveOrUpdate" class="form-submit">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    编辑开屏信息
                </h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" value="${os.id}">
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>
                        <tr>
                            <td width="30%" class="text-right"><b style="color:red;">*</b>标题：</td>
                            <td width="70%" class="text-left">
                                <input type="text" required class="form-control" name="title" value="${os.title}" >
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>开屏时间(秒)：</td>
                            <td class="text-left">
                                <input type="number" class="form-control" required step="1" min="1" name="showtime" value="${os.showtime}">
                            </td>
                        </tr>


                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>开屏图片：</td>
                            <td class="text-left">
                                <input type="hidden" name="imgUrl" value="${os.imgUrl.src}">
                                <button type="button" id="<%=selectImgBtnId%>" class="btn btn-primary">选择图片</button>
                                <div id="<%=imgContainerId%>">

                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right">应用：</td>
                            <td class="text-left">
                                <button id="<%=selectGameBtnId%>" type="button" class="btn btn-primary btn-sm">选择</button>
                                <button id="<%=clearGameBtnId%>" type="button" class="btn btn-danger btn-sm">清除</button>
                                <input type="hidden" name="appId" value="${os.appId}">
                                <div id="<%=gameContainerId%>">

                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td width="15%" class="text-right">点击下载：</td>
                            <td width="85%" class="text-left">

                                <label class="radio-inline">
                                    <input type="radio" name="autoDownload" value="1" <c:if test="${os == null || os.autoDownload == 1}">checked</c:if>> 否
                                </label>

                                <label class="radio-inline">
                                    <input type="radio" name="autoDownload" value="2" <c:if test="${os.autoDownload == 2}">checked</c:if>> 是
                                </label>

                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    requirejs(['jquery','Fui'],function(){
        var $form = $("#<%=formId%>");
        $form.data("paramFn",function(){
            if(!$form.find("input[name='imgUrl']").val()){
                layer.msg("请选择开屏图片");
                return false;
            }
        });

        var $imgContainer = $("#<%=imgContainerId%>");
        $("#<%=selectImgBtnId%>").click(function(){
            var picSelect = new Picture(function(data){
                BaseUtil.addSingleImage($imgContainer,data,{});
                $form.find("input[name='imgUrl']").val(data.src);
                picSelect.close();
            });
            picSelect.select();
        });


        var $gameContainer = $("#<%=gameContainerId%>");
        var showGameInfo = function(icon,name){
            BaseUtil.addSingleImage($gameContainer,{src:icon},{size:"50",height:50,width:50});
            setTimeout(function(){
                $gameContainer.append("<div style='height:50px;line-height:50px;float: left'><strong>"+name+"</strong><div>")
            },200);
        }


        $("#<%=clearGameBtnId%>").click(function(){
            $form.find("input[name='appId']").val("");
            $gameContainer.html("");
        });

        $("#<%=selectGameBtnId%>").click(function(){
            var gameSelector = new Game(function(data){
                $form.find("input[name='appId']").val(data.id);
                showGameInfo(data.icon,data.name);
                gameSelector.close();
            });
            gameSelector.select();
        });

        <c:if test="${not empty os.imgUrl}">
        BaseUtil.addSingleImage($imgContainer,{src:"${os.imgUrl.src}"},{});
        </c:if>

        <c:if test="${versionInfo != null}">
        showGameInfo("${versionInfo.icon.src}","${gameInfo.name}");
        </c:if>
    });
</script>