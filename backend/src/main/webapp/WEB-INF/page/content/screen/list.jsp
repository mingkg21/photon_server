<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" class="btn btn-default open-dialog"  dialog-title="新增开屏" href="${base}/content/screen/goAddPage" >新增</button>
	</div>
</div>

<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
        Fui.addBootstrapTable({
            url : '${base}/content/screen/list',
            columns : [{
                title : '标题',
                align : 'center',
                width : '120',
                valign : 'middle',
                field : 'title'
            },{
                title : '图片',
                align : 'center',
                width : '100',
                valign : 'middle',
                field : 'imgUrl',
                formatter : function(value, row, index){
                    return "<a href='"+(baseInfo.cdn+value)+"' target = '_blank'><img title='点击查看原图' src='"+(baseInfo.cdn+value)+"-preview'/></a>";
                }
			},{
                title : '时间(秒)',
                align : 'center',
                width : '50',
                valign : 'middle',
                field : 'showtime'
			},{
                title : '应用ID',
                align : 'center',
                width : '50',
                valign : 'middle',
                field : 'appId'
			},{
                field : 'autoDownload',
                title : '直接下载',
                align : 'center',
                width : '50',
                valign : 'middle',
                formatter : function (v,row) {
                    if(v == 2){
                        return "<font color='red'>是</font>";
					}
                    return "<font color='blue'>否</font>";
                }
			},{
                field : 'status',
                title : '状态',
                align : 'center',
                width : '50',
                valign : 'middle',
                formatter : function (v,row) {
					return Fui.statusFormatter({
						val : v,
						url:'${base}/content/screen/updateStatus?id='+row.id+'&status='
					});
				}
            },{
                field : 'createTime',
                title : '创建时间',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : Fui.formatDatetime
			},{
                title : '操作',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : function(v,row){
                   var arr = [
                       '<a class="open-dialog" dialog-title="修改开屏" href="${base}/content/screen/goEditPage?id=',
                        row.id,'" title="修改开屏">',
                        '<i class="glyphicon glyphicon-edit"></i>',
					   '</a> ',
                       '<a class="todo-ajax" href="${base}/content/screen/delete?id=',row.id, '" confirm="是否确认要删除?" title="删除">',
                       '<i class="glyphicon glyphicon-trash"></i>', '</a> '
                   ];
                    return arr.join("");
				}
            }]
        });
    });
</script>

