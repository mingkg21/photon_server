<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" class="btn btn-default open-dialog"  dialog-title="新增版本" href="${base}/content/release/goAddPage" >新增</button>
	</div>
</div>

<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
        Fui.addBootstrapTable({
            url : '${base}/content/release/list',
            columns : [{
                title : '名称',
                align : 'center',
                width : '80',
                valign : 'middle',
                field : 'name'
            },{
                title : '版本名',
                align : 'center',
                width : '80',
                valign : 'middle',
                field : 'versionName'
			},{
                field : 'forceUpdate',
                title : '强制更新',
                align : 'center',
                width : '50',
                valign : 'middle',
                formatter : function (v,row) {
                    if(v == 1){
                        return "<font color='red'>是</font>";
					}
                    return "<font color='blue'>否</font>";
                }
			},{
                field : 'status',
                title : '状态',
                align : 'center',
                width : '50',
                valign : 'middle',
                formatter : function (v,row) {
					return Fui.statusFormatter({
						val : v,
						url:'${base}/content/release/updateStatus?id='+row.id+'&status='
					});
				}
            },{
                field : 'releaseTime',
                title : '发布时间',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : Fui.formatDatetime
			}, {
                field : 'fileSize',
                title : '文件大小',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : Fui.formatFileSize
			},{
                title : '操作',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : function(v,row){
                   var arr = [
                       '<a class="open-dialog" dialog-title="修改版本" href="${base}/content/release/goEditPage?id=',
                        row.id,'" title="修改版本">',
                        '<i class="glyphicon glyphicon-edit"></i>',
					   '</a> '
				   ];
                    return arr.join("");
				}
            }]
        });
    });
</script>

