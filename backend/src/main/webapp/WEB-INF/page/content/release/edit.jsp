<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String selectApkBtnId = java.util.UUID.randomUUID().toString();
    String formId = java.util.UUID.randomUUID().toString();
%>
<form id="<%=formId%>"  action="${base}/content/release/saveOrUpdate" class="form-submit">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    编辑版本信息
                </h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" value="${release.id}">
                <input type="hidden" name="fileSize" value="${release.fileSize}">
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>

                        <tr>
                            <td width="15%" class="text-right"><b style="color:red;">*</b>名称：</td>
                            <td width="35%" class="text-left">
                                <input type="text" required class="form-control" name="name" value="${release.name}" >
                            </td>

                            <td width="15%" class="text-right"><b style="color:red;">*</b>包名：</td>
                            <td width="35%" class="text-left">
                                <input type="text" required class="form-control" name="packageName" value="${release.packageName}" >
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>版本名：</td>
                            <td class="text-left">
                                <input type="text" required class="form-control" name="versionName" value="${release.versionName}" >
                            </td>

                            <td class="text-right"><b style="color:red;">*</b>版本号：</td>
                            <td class="text-left">
                                <input type="number" required class="form-control" name="versionCode" value="${release.versionCode}" >
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>发布时间：</td>
                            <td class="text-left">
                                <input type="text" required class="form-control fui-date" name="releaseTime" format="YYYY-MM-DD HH:mm:ss" value="<fmt:formatDate value="${release.releaseTime}" pattern="yyyy-MM-dd HH:mm:ss"/>" >
                            </td>

                            <td class="text-right">强制更新：</td>
                            <td class="text-left">

                                <label class="radio-inline">
                                    <input type="radio" name="forceUpdate" <c:if test="${release.forceUpdate != 1}">checked</c:if> value="0"> 否
                                </label>

                                <label class="radio-inline">
                                    <input type="radio" name="forceUpdate" value="1" <c:if test="${release.forceUpdate == 1}">checked</c:if> > 是
                                </label>

                            </td>
                        </tr>

                        <tr>
                            <td width="15%" class="text-right"><b style="color:red;">*</b>应用包：</td>
                            <td width="85%" class="text-left" colspan="3">
                                <div class="input-group" style="width: 100%">
                                    <input type="text" required="required" name="downloadUrl" value="${release.downloadUrl.src}" readonly="" class="form-control" aria-required="true">
                                    <span class="input-group-btn">
                                        <button id="<%=selectApkBtnId%>" class="btn btn-primary" type="button">选择</button>
                                    </span>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>更新日志：</td>
                            <td class="text-left" colspan="3">
                                <textarea class="form-control" style="height: 100px;" required name="updateLog">${release.updateLog}</textarea>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    requirejs(['jquery','Fui'],function(){
        var $form = $("#<%=formId%>");
        $("#<%=selectApkBtnId%>").click(function(){
            var apk = new Package(function(data){
                $form.find("input[name='downloadUrl']").val(data.src);
                $form.find("input[name='fileSize']").val(data.size);
                apk.close();
            });
            apk.select();
        })
    });
</script>