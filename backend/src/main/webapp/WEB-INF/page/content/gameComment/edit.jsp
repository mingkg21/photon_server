<%@ page import="java.util.UUID" %>
<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String selectPicBtnId = UUID.randomUUID().toString();
    String picContainerId = UUID.randomUUID().toString();
    String formId = UUID.randomUUID().toString();
%>
<form id="<%=formId%>" action="${base}/content/gameComment/saveOfficialReply" class="form-submit">
    <input type="hidden" name="id" value="${id}"/>
    <div class="modal-dialog" style="width: 600px;">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    官方回复
                </h4>
            </div>

            <div class="modal-body">
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>
                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>官方内容</td>
                            <td class="text-left">
                                <textarea class="form-control" required rows="5" name="content"></textarea>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right">图片</td>
                            <td class="text-left">

                                <input type="hidden" name="picJson">
                                <button id="<%=selectPicBtnId%>" class="btn btn-primary btn-sm" type="button">选择</button>
                                <div id="<%=picContainerId%>">

                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
        var $form = $("#<%=formId%>");
        var $picContainer = $("#<%=picContainerId%>");

        $form.data("paramFn",function(){
            var arr = BaseUtil.getImageValues($picContainer);
            var json = JSON.stringify(arr);
            $form.find("input[name='picJson']").val(json);
        });

        $("#<%=selectPicBtnId%>").click(function(){
            var pic = new Picture(function(data){
                if(data.length > 3){
                    layer.msg("最多只能发布3张图片");
                    return;
                }
                BaseUtil.addManyImage($picContainer,data);
                //console.info(data);
                pic.close();
            },true);
            pic .select();
        });
    });
</script>