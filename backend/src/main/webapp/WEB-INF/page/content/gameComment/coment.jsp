<%@ page import="java.util.UUID" %>
<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%
    String selectUserBtnId = UUID.randomUUID().toString();
    String userContainerId = UUID.randomUUID().toString();
    String gameContainerId = java.util.UUID.randomUUID().toString();
    String selectGameBtnId = java.util.UUID.randomUUID().toString();
    String formId = UUID.randomUUID().toString();
%>

<form id="<%=formId%>" action="${base}/content/gameComment/saveComment" class="form-submit">
    <div class="modal-dialog" style="width: 600px;">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    新增回复
                </h4>
            </div>

            <div class="modal-body">
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>

                        <tr>
                            <td width="15%" class="text-right"><b style="color:red;">*</b>游戏：</td>
                            <td width="85%" colspan="3" class="text-left">
                                <button id="<%=selectGameBtnId%>" type="button" class="btn btn-primary btn-sm">选择</button>
                                <input type="hidden" name="gameId" value="">
                                <div id="<%=gameContainerId%>">

                                </div>
                            </td>
                        </tr>


                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>评论人</td>
                            <td class="text-left">
                                <button id="<%=selectUserBtnId%>" type="button" class="btn btn-primary btn-sm">选择</button>
                                <input type="hidden" name="userId" value="">
                                <div id="<%=userContainerId%>">

                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>评论内容</td>
                            <td class="text-left">
                                <textarea class="form-control" required rows="5" name="content"></textarea>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>星级</td>
                            <td class="text-left">
                                <select class="form-control" name="star">
                                    <option value="2">2</option>
                                    <option value="4">4</option>
                                    <option value="6">6</option>
                                    <option value="8" selected>8</option>
                                    <option value="10">10</option>
                                </select>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
    requirejs(['jquery','Fui'],function() {

        var $form = $("#<%=formId%>");
        $form.data("paramFn",function() {
            if (!$form.find("input[name='gameId']").val()) {
                layer.msg("请选择游戏");
                return false;
            }
            if (!$form.find("input[name='userId']").val()) {
                layer.msg("请选择评论人");
                return false;
            }

        });

        var $userContainer = $("#<%=userContainerId%>");
        $("#<%=selectUserBtnId%>").click(function () {
            var userSelector = new UserSelector(function (data) {
                $form.find("input[name='userId']").val(data.id);
                showUserInfo(data.headIcon, data.nickName);
                userSelector.close();
            });
            userSelector.select();
        });

        var showUserInfo = function(icon,name){
            BaseUtil.addSingleImage($userContainer,{src:icon},{size:"50",height:50,width:50});
            setTimeout(function(){
                $userContainer.append("<div style='height:50px;line-height:50px;float: left'><strong>"+name+"</strong><div>")
            },200);
        }



        var $gameContainer = $("#<%=gameContainerId%>");
        var showGameInfo = function(icon,name){
            BaseUtil.addSingleImage($gameContainer,{src:icon},{size:"50",height:50,width:50});
            setTimeout(function(){
                $gameContainer.append("<div style='height:50px;line-height:50px;float: left'><strong>"+name+"</strong><div>")
            },200);
        }

        $("#<%=selectGameBtnId%>").click(function(){
            var gameSelector = new Game(function(data){
                $form.find("input[name='gameId']").val(data.id);
                showGameInfo(data.icon,data.name);
                gameSelector.close();
            });
            gameSelector.select();
        });


    });
</script>