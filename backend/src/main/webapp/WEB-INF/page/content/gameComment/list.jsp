<%@ page import="java.util.UUID" %>
<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
	String delBtnId = UUID.randomUUID().toString();
	String tableId = UUID.randomUUID().toString();
%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" class="btn btn-default open-dialog"  dialog-title="新增回复" href="${base}/content/gameComment/goAddPage" >新增</button>
		<button id ="<%=delBtnId%>" class="btn btn-danger">删除</button>
	</div>
</div>


<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">

			<div class="input-group">
				<input type="number" class="form-control" name="gameId" placeholder="游戏ID">
			</div>

			<div class="input-group">
				<input type="text" class="form-control" name="gameName" placeholder="游戏名">
			</div>

			<div class="input-group">
				<input type="number" class="form-control" name="userId" placeholder="用户ID">
			</div>

			<button class="btn btn-primary fui-date-search">查询</button>
		</div>
	</div>
</form>

<table class="fui-default-table" id="<%=tableId%>"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
       // var $div = Fui.getParentNode();

        $("#<%=delBtnId%>").click(function(){
            var arr = $("#<%=tableId%>").bootstrapTable("getSelections");
			if(arr.length == 0){
			 	layer.msg("请选择需要删除的记录");
			 	return;
			}
			layer.confirm('是否确认要删除'+arr.length+'条评论？', {
                btn : [ '确定', '取消' ]//按钮
            },function (index) {
			    var ids = [];
			    for(var i=0;i<arr.length;i++){
			        ids.push(arr[i].comment.id);
				}
			    $.ajax({
					url:'${base}/content/gameComment/batchDelete',
					data:{
					    ids : ids.join(",")
					},
					success:function () {
					    layer.close(index);
                        $("#<%=tableId%>").bootstrapTable('refresh');
                    }
				})
            })
		});


        Fui.addBootstrapTable({
            url : '${base}/content/gameComment/list',
            singleSelect: false,
            columns : [  {
                field: 'state',
                checkbox: 'true'
            },{
                title : '游戏',
                align : 'left',
                width : '150',
                valign : 'middle',
                formatter : function (v,row) {
                    var gameInfo = row.game;
                    if(!gameInfo){
                        return;
					}

                    var html = "";
                    if(gameInfo.versionInfo && gameInfo.versionInfo.icon){
                        html += Fui.formatImage(gameInfo.versionInfo.icon.src,"50") + "&nbsp;";
                    }
                    html += ""+gameInfo.name+"<strong>("+gameInfo.id+")<strong>";
                    return html;
                }
			},{
                title : '用户',
                align : 'left',
                width : '150',
                valign : 'middle',
                formatter : function(v,row){
                    var user = row.commentUser;
                    var html = "";
                    if(user.headIcon && user.headIcon.src){
                        html += Fui.formatImage(user.headIcon.src,"50");
                    }
                    html += "&nbsp;"+user.nickName+"<b>("+user.id+")</b>";
                    return html;
                }
            },{
                title : '评星',
                align : 'center',
                field : 'star',
                width : '50',
                valign : 'middle'
            },{
                title : '评论内容',
                align : 'center',
                width : '200',
                valign : 'middle',
                formatter:function(v,row) {
					var html = "";
					if(row.comment.topStatus == 999){
						html = "<font color='red'>[置顶]</font>";
					}
					return "<div style='max-width:400px;word-break:break-all;'>"+html + Fui.formatXss(row.comment.content)+"</div>";
                }
			},{
                title : '官方回复',
                align : 'center',
                width : '200',
                valign : 'middle',
                formatter:function(v,row) {
                    var content = row.comment.officialContent;
                    if(!content){
                        return "-";
					}
                    return "<div style='max-width:400px;word-break:break-all;'>"+ content +"</div>";
                }
			},{
                title : '图片',
                align : 'center',
                width : '200',
                valign : 'middle',
                formatter:function(v,row) {
                    var imgs = row.comment.officialImgs;

					if(!imgs || imgs.images.length == 0){
					    return "-";
					}
					var html = "";
					for(var i = 0; i < imgs.images.length;i++){
                        var img = imgs.images[i];
					    html += "<a target='_blank' href='"+baseInfo.cdn+img+"'><img height='50' src='"+baseInfo.cdn+img+"-50'></a>&nbsp;";
					}
					return html;
					//return row.comment.officialContent;
                }
			},{
                title : '创建时间',
                align : 'center',
                width : '100',
                valign : 'middle',
                formatter:function(v,row) {
					return Fui.formatDatetime(row.comment.createTime);
				}
            },{
                title : '操作',
                align : 'center',
                width : '100',
                valign : 'middle',
                formatter : function(v,row){
                    var arr = [ '<a class="open-dialog" dialog-title="官方回复" href="${base}/content/gameComment/goEditPage?id=',
                        row.comment.id,'" title="官方回复">',
                        '<i class="glyphicon glyphicon-edit"></i>', '</a> '];

                    if(row.comment.topStatus == 999){
                        arr.push(
                        '<a class="todo-ajax" href="${base}/content/gameComment/setDown?id=',row.comment.id,
                            '" confirm="是否确认取消置顶?" title="取消置顶">',
                            '<i class="glyphicon glyphicon-arrow-down"></i>', '</a> ');
                    }else{
                        arr.push( '<a class="todo-ajax" href="${base}/content/gameComment/setTop?id=',row.comment.id,
                            '" confirm="是否确认要置顶?" title="置顶">',
                            '<i class="glyphicon glyphicon-arrow-up"></i>', '</a> ');
					}
                    arr.push(
                        '<a class="todo-ajax" href="${base}/content/gameComment/delete?id=',row.comment.id,
						'" confirm="是否确认要删除?" title="删除">',
                        '<i class="glyphicon glyphicon-trash"></i>', '</a> ');

					return arr.join('');
				}
            }]
        });
    });
</script>

