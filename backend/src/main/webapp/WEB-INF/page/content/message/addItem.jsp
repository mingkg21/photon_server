<%@ page import="java.util.UUID" %>
<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%
    //String formId = UUID.randomUUID().toString();
    String urlTrId = UUID.randomUUID().toString();
    String idTrId = UUID.randomUUID().toString();
    String idNameSpanId = UUID.randomUUID().toString();
%>

<form id="${editFormId}">
    <input type="hidden" name="uuid" value="${item.uuid}"/>
    <div class="modal-dialog" style="width: 600px;">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    编辑文本信息
                </h4>
            </div>

            <div class="modal-body">
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>
                        <tr>
                            <td class="text-right" width="25%"><b style="color:red;">*</b>类型：</td>
                            <td class="text-left">
                                <select name="msgTagType" required class="form-control">
                                    <option value="1" <c:if test="${msgTagType == null || msgTagType == 1}">selected</c:if>>文本</option>
                                    <option value="2" <c:if test="${msgTagType == 2}">selected</c:if>>网页链接</option>
                                    <option value="3" <c:if test="${msgTagType == 3}">selected</c:if>>游戏信息</option>
                                    <option value="4" <c:if test="${msgTagType == 4}">selected</c:if>>用户信息</option>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>字体大小：</td>
                            <td class="text-left">
                                <c:if test="${item.fontSize == null}">
                                    <input type="number" name="fontSize" value="12" min="10" max="14" class="form-control" >
                                </c:if>
                                <c:if test="${item.fontSize != null}">
                                    <input type="number" name="fontSize" value="${item.fontSize}" min="10" max="14" class="form-control" >
                                </c:if>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>字体加粗：</td>
                            <td class="text-left">
                                <label class="checkbox-inline">
                                    <input type="radio" value="0" name="bold" <c:if test="${item.bold == null || item.bold == 0}">checked</c:if>> 不加粗
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" value="1" name="bold" <c:if test="${item.bold == 1}">checked</c:if>> 加粗
                                </label>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>字体颜色：</td>
                            <td class="text-left">
                                <select name="color" required class="form-control">
                                    <option value="0xff333333" <c:if test="${item.color eq '0xff333333'}">selected</c:if> style="background-color:#333333;color:#333333;">黑色</option>
                                    <option value="0xff999999" <c:if test="${item.color eq '0xff999999'}">selected</c:if> style="background-color:#999999;color:#999999;">灰色</option>
                                    <option value="0xffff0000" <c:if test="${item.color eq '0xffff0000'}">selected</c:if> style="background-color:#ff0000;color:#ff0000;">红色</option>
                                    <option value="0xff0000ff" <c:if test="${item.color eq '0xff0000ff'}">selected</c:if> style="background-color:#0000ff;color:#0000ff;">蓝色</option>
                                    <option value="0xff00ff00" <c:if test="${item.color eq '0xff00ff00'}">selected</c:if> style="background-color:#00ff00;color:#00ff00;">绿色</option>
                                </select>
                            </td>
                        </tr>

                        <tr id="<%=idTrId%>">
                            <td class="text-right"><b style="color:red;">*</b><span id="<%=idNameSpanId%>">关联内容ID</span></td>
                            <td class="text-left">
                                <input type="number" class="form-control" value="${item.id}" required name="id"/>
                            </td>
                        </tr>


                        <tr id="<%=urlTrId%>">
                            <td class="text-right"><b style="color:red;">*</b>外链URL</td>
                            <td class="text-left">
                                <input type="text" class="form-control" value="${item.url}" required name="url"/>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>排序</td>
                            <td class="text-left">
                                <input type="number" min="1" class="form-control" value="${item.ordering}" required name="ordering"/>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>内容</td>
                            <td class="text-left">
                                <textarea class="form-control" required rows="5" name="value">${item.value}</textarea>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">添加</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    requirejs(['jquery','Fui'],function(){
        var $idTr = $("#<%=idTrId%>");
        var $urlTr = $("#<%=urlTrId%>");
        var $form = $("#${editFormId}");
        var $nameSpan = $("#<%=idNameSpanId%>");

        function msgTagTypeChange(){
            var type = $form.find("select[name='msgTagType']").val();
            if(type == 1){
                hideTr($urlTr);
                hideTr($idTr);
                return;
            }
            if(type == 2){
                showTr($urlTr);
                hideTr($idTr);
                return;
            }
            if(type == 3 || type == 4){
                hideTr($urlTr);
                showTr($idTr);
                if(type == 3){
                    $nameSpan.html("游戏ID");
                }else if(type == 4){
                    $nameSpan.html("用户ID");
                }
                return;
            }
        }

        function hideTr($tr){
            $tr.hide();
            $tr.find("input").removeAttr("required")
        }

        function showTr($tr){
            $tr.show();
            $tr.find("input").attr("required","required");
        }

        msgTagTypeChange();
        $form.find("select[name='msgTagType']").change(function(){
            msgTagTypeChange();
        });

        $form[0].onsubmit = function(){
            var data = $form.serializeArray();
            var json = {};
            $(data).each(function () {
                if(this.value != ""){
                    json[this.name] = this.value;
                }
            });
            if(!json.uuid){
                json.uuid = Fui.uuid();
                $("#${tableId}").bootstrapTable("append",json);
            }else{
                //修改数据
                //updateRow
                var data = $("#${tableId}").bootstrapTable("getData");
                for(var i = 0;i<data.length ;i++){
                    var r = data[i];
                    if(r.uuid == json.uuid){
                        $("#${tableId}").bootstrapTable("updateRow",{index:i,row:json});
                        break;
                    }
                }
            }
            layer.closeAll();
            return false;
        }
    });
</script>