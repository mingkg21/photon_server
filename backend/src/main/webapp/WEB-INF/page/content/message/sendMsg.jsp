<%@ page import="java.util.UUID" %>
<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%
    String tableId = UUID.randomUUID().toString();
    String btnId = UUID.randomUUID().toString();
    String editFormId = UUID.randomUUID().toString();
    String formId = UUID.randomUUID().toString();
%>
<form id="<%=formId%>">
    <div class="panel panel-default">
        <div class="panel-heading">基本内容</div>
        <div class="panel-body">
            <table class="table table-bordered table-striped" style="clear: both">
                <tbody>
                <tr>
                    <td width="20%" class="text-right"><b style="color:red;">*</b>标题：</td>
                    <td width="80%" class="text-left">
                        <input type="text" required class="form-control" name="title" >
                    </td>
                </tr>

                <tr>
                    <td width="20%" class="text-right">
                        <b style="color:red;">*</b>接收人ID：
                        <div class="text-tip">0:所有人</div>
                    </td>
                    <td width="80%" class="text-left">
                        <input type="number" required class="form-control" name="userId">
                    </td>
                </tr>

                <tr>
                    <td class="text-right"><b style="color:red;">*</b>内容类型：</td>
                    <td class="text-left">
                        <select class="form-control" name="contentType">
                            <option value=""></option>
                            <!-- <option value="2">游戏更新</option>-->
                            <option value="4">内链</option>
                            <option value="7">外链</option>
                            <option value="6">跳转到信息中心</option>
                        </select>
                    </td>
                </tr>
                <!---
                <tr>
                    <td class="text-right">对象ID</td>
                    <td class="text-left">
                        <input type="number" class="form-control" name="objectId" >
                    </td>
                </tr> -->

                <tr>
                    <td class="text-right">对象标题</td>
                    <td class="text-left">
                        <input type="text" class="form-control" name="objectTitle" >
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>


    <div class="panel panel-default">
        <div class="panel-heading">信息内容</div>
        <div class="panel-body">
            <button type="button" class="btn btn-primary open-dialog" dialog-title="添加内容" href="${base}/content/message/goAddItemPage?tableId=<%=tableId%>&editFormId=<%=editFormId%>">添加</button>
            <table class="fui-default-table" id="<%=tableId%>">

            </table>
        </div>
    </div>

    <button id="<%=btnId%>" type="button" class="btn btn-primary">发布</button>
</form>
<script type="text/javascript">
    requirejs(['jquery','Fui'],function(){
       var $table = $("#<%=tableId%>");
       var $form = $("#<%=formId%>");
       Fui.addBootstrapTable({
           showRefresh:false,
           pagination:false,
           sortable: true,                     //是否启用排序
           sortOrder: "desc",                   //排序方式
           sidePagination: "client",
           columns : [{
               title : '排序',
               align : 'center',
               width : '50',
               valign : 'middle',
               field : 'ordering',
               sortable: true
           },{
                title : '内容',
                align : 'center',
                width : '120',
                valign : 'middle',
                field : 'value'
            },{
                field : 'msgTagType',
                title : '类型',
                align : 'center',
                width : '50',
                valign : 'middle',
                formatter : function(v,row){
                    if(v == 1){
                        return "文本";
                    }
                    if(v == 2){
                        return "网页链接";
                    }
                    if(v == 3){
                        return "游戏信息";
                    }

                    if(v == 4){
                        return "用户信息";
                    }
                }
            },{
                field : 'color',
                title : '字体颜色',
                align : 'center',
                width : '50',
                valign : 'middle'
            },{
                field : 'fontSize',
                title : '字体大小',
                align : 'center',
                width : '50',
                valign : 'middle'
            },{
                field : 'bold',
                title : '字体加粗',
                align : 'center',
                width : '50',
                valign : 'middle',
                formatter : function(v,row){
                    if(v == 0){
                        return "不加粗";
                    }
                    if(v == 1){
                        return "加粗";
                    }
                }
            },{
                field : 'id',
                title : '关联ID',
                align : 'center',
                width : '50',
                valign : 'middle',
            },{
                field : 'url',
                title : '外链',
                align : 'center',
                width : '80',
                valign : 'middle'
            },{
                title : '操作',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : function(v,row,index){
                    var arr = [
                        '<a href="javascript:void(0)" title="修改">',
                        '<i class="glyphicon glyphicon-edit"></i>',
                        '</a> ',
                        '<a href="javascript:void(0)" title="删除">',
                        '<i class="glyphicon glyphicon-trash"></i>',
                        '</a> '
                    ];
                    return arr.join("");
                },

                events:{
                    'click .glyphicon-trash':function (e, value, row, index) {
                        $table.bootstrapTable("remove",{field:"uuid",values:[row.uuid]});
                    },
                    'click .glyphicon-edit' :function(e, value, row, index){
                        Fui.openDialog({
                            url:"${base}/content/message/goAddItemPage?tableId=<%=tableId%>&editFormId=<%=editFormId%>&&msgTagType="+row.msgTagType
                        });

                        $editForm = $("#<%=editFormId%>");
                        if(row.uuid) {
                            $editForm.find("input[name='uuid']").val(row.uuid);
                        }
                        $editForm.find("input[name='fontSize']").val(row.fontSize);
                        $editForm.find("input[name='ordering']").val(row.ordering);
                        $editForm.find("textarea[name='value']").val(row.value);
                        $editForm.find("select[name='color']").val(row.color);
                        $editForm.find("select[name='value']").val(row.value);
                        $editForm.find("input[name='url']").val(row.url);
                        $editForm.find("input[name='id']").val(row.id);
                        if(row.bold == 1){
                            $editForm.find('input[name="bold"]').eq(0).attr("checked","checked");
                        }
                    }
                }
            }]
        });

       function getData(){
           var data = $("#<%=tableId%>").bootstrapTable("getData");
           var arr = [];
           for(var i = 0;i < data.length;i++){
               var item = $.extend({},data[i]);
               var msgTagType = item.msgTagType;
               delete item.msgTagType;
               item["type"] = msgTagType;
               item["color"] = parseInt(item["color"],16)
               arr.push(item);
           }
           return arr;
       }


        $("#<%=btnId%>").click(function () {
            $("#<%=btnId%>").attr("disabled");
            var data = getData();
            $.ajax({
               url:'${base}/content/message/sendMsg',
                data:{
                   title:$form.find("input[name='title']").val(),
                   userId:$form.find("input[name='userId']").val(),
                   contentType:$form.find("[name='contentType']").val(),
                   objectId:$form.find("[name='objectId']").val(),
                   objectTitle:$form.find("[name='objectTitle']").val(),
                   contentArrJson:JSON.stringify(data)
                },
               success:function(data){
                   layer.msg("发送成功");
               },
                complete:function(){
                    $("#<%=btnId%>").removeAttr("disabled");
                }
            });

        });

    });
</script>