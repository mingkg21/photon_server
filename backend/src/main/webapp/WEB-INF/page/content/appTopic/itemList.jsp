<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" class="btn btn-default open-dialog"  dialog-title="新增专题项" href="${base}/content/appTopic/goAddItemPage?topicId=${topicId}" >新增</button>
	</div>
</div>

<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
        Fui.addBootstrapTable({
            url : '${base}/content/appTopic/getItemList?topicId=${topicId}',
            columns : [{
                field : 'objectType',
                title : '类型',
                align : 'center',
                width : '60',
                valign : 'middle',
                formatter: function(v,row){
                    if(v == 1){
                        return "文本";
					}
					if(v == 2){
                        return "应用";
					}
					if(v == 3){
					    return "图片";
					}
					if(v == 4){
					    return "视频";
					}
					return "-";
                }
			},{
                field : 'objectId',
                title : '对象ID',
                align : 'center',
                width : '60',
                valign : 'middle'
			},{
                title : '内容',
                align : 'center',
                width : '300',
                align : 'left',
                valign : 'middle',
                formatter: function(v,row){
                    var content = {};
                    try{
                        content = JSON.parse(row.content);
					}catch(e){}

                    var type = row.objectType;
                    if(type == 1 ){
						if(content.text){
                            return "<div style='max-width:640px;word-break:break-all;'>"+ content.text +"</div>";
						}
						return "-";
					}else if(type == 2){
                        var html = "";
                        if(row.gameIcon){
                            html += Fui.formatImage(row.gameIcon,"50");
                        }
                        html += "<strong>"+row.gameName+"<strong>";
                        return html;
					}else if(type == 3 || type == 4){
                        return Fui.formatImage(content.imgUrl,"100");
                    }
					return "-";
				}
            },{
                field : 'createTime',
                title : '创建时间',
                align : 'center',
                width : '150',
                valign : 'middle',
                formatter: Fui.formatDatetime
			},{
                field : 'ordering',
                title : '排序',
                align : 'center',
                width : '60',
                valign : 'middle'
			},{
                title : '操作',
                align : 'center',
                width : '100',
                valign : 'middle',
                formatter : function(v,row){
                    return [ '<a class="open-dialog" dialog-title="修改专题项" href="${base}/content/appTopic/goEditItemPage?id=',
							row.id,'" title="修改专题项">',
						'<i class="glyphicon glyphicon-edit"></i>', '</a>',

                        ' <a class="todo-ajax" href="${base}/content/appTopic/deleteItem?itemId=',
                        row.id,'" confirm="是否确认要删除专题项?">',
                        '<i class="glyphicon glyphicon-trash"></i>', '</a>'
					].join("");
				}
            }]
        });
    });
</script>

