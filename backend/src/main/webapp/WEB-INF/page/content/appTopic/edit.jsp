<%@ page import="java.util.UUID" %>
<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%

    String selectPicBtnId = UUID.randomUUID().toString();
    String picContainerId = UUID.randomUUID().toString();
    String formId = UUID.randomUUID().toString();

%>
<form id="<%=formId%>" action="${base}/content/appTopic/saveOrUpdate" class="form-submit">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    编辑专区
                </h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" value="${appTopic.id}">
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>
                        <tr>
                            <td width="20%" class="text-right"><b style="color:red;">*</b>标题：</td>
                            <td width="80%" class="text-left">
                                <input type="text" required class="form-control" value="${appTopic.title}" name="title" placeholder="标题" />
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>图片：</td>
                            <td class="text-left">
                                <button id="<%=selectPicBtnId%>" class="btn btn-primary btn-sm" type="button">选择</button>
                                <input type="hidden" name="picUrl" value="${appTopic.picUrl}">
                                <div id="<%=picContainerId%>">

                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>排序：</td>
                            <td class="text-left">
                                <input type="number" required class="form-control" value="${appTopic.ordering}" name="ordering" placeholder="排序" />
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
    requirejs(['jquery','Fui'],function(){
        var $form = $("#<%=formId%>");
        $form.data("paramFn",function(){
            if(!$form.find("input[name='picUrl']").val()){
                layer.msg("请选择图片");
                return false;
            }
        });


        var $picContainer = $("#<%=picContainerId%>");
        
        $("#<%=selectPicBtnId%>").click(function () {
            var pic = new Picture(function(data){
                BaseUtil.addSingleImage($picContainer,data,{width:160,height:160,size:"160"});
                $form.find("input[name='picUrl']").val(data.src);
                pic.close();
            },false);
            pic .select();
        });

        <c:if test="${not empty appTopic.picUrl}">
        BaseUtil.addSingleImage($picContainer,{src:"${appTopic.picUrl}"},{width:160,height:160,size:"160"});
        </c:if>

    });
</script>
