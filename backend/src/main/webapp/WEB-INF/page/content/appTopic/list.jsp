<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" class="btn btn-default open-dialog"  dialog-title="新增专题" href="${base}/content/appTopic/goAddPage" >新增</button>
	</div>
</div>

<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
        Fui.addBootstrapTable({
            url : '${base}/content/appTopic/list',
            columns : [{
                field : 'id',
                title : 'ID',
                align : 'center',
                width : '60',
                valign : 'middle'
            },{
                field : 'title',
                title : '标题',
                align : 'center',
                width : '250',
                valign : 'middle'
            },{
                field : 'picUrl',
                title : '图片',
                align : 'center',
                width : '100',
                valign : 'middle',
				formatter:function(v){
                    return "<a href='"+(baseInfo.cdn+v)+"' target = '_blank'><img title='点击查看原图' src='"+(baseInfo.cdn+v)+"-preview'/></a>";
                }
			},{
                field : 'status',
                title : '状态',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : function (v,row) {
					return Fui.statusFormatter({
						val : v,
						url:'${base}/content/appTopic/updateStatus?id='+row.id+'&status='
					});
                }
            },{
                field : 'ordering',
                title : '排序',
                align : 'center',
                width : '80',
                valign : 'middle'
			},{
                field : 'createTime',
                title : '创建时间',
                align : 'center',
                width : '120',
                valign : 'middle',
                formatter: Fui.formatDatetime
            },{
                title : '操作',
                align : 'center',
                width : '100',
                valign : 'middle',
                formatter : function(v,row){

                    return [ '<a class="open-dialog" dialog-title="修改专题" href="${base}/content/appTopic/goEditPage?id=',
							row.id,'" title="修改专题">',
						'<i class="glyphicon glyphicon-edit"></i>', '</a>',

                        ' <a class="open-tab" tab-id="tab-system-apptopic-area_',row.id,'" dialog-title="专题（',row.id,'）"  data-icon="glyphicon glyphicon-zoom-in" href="${base}/content/appTopic/goItemListPage?topicId=',
                        row.id,'" title="查看专题子集">',
                        '<i class="glyphicon glyphicon-zoom-in"></i>', '</a>'

					].join("");
				}
            } ]
        });
    });
</script>

