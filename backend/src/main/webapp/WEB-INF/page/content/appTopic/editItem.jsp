<%@ page import="java.util.UUID" %>
<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String gameContainerId = UUID.randomUUID().toString();
    String selectGameBtnId = UUID.randomUUID().toString();
    String formId = UUID.randomUUID().toString();
    String readSizeBtn = UUID.randomUUID().toString();
%>
<form id="<%=formId%>" action="${base}/content/appTopic/saveOrUpdateItem" class="form-submit">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    编辑专区
                </h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" value="${item.id}">
                <input type="hidden" name="topicId" value="${topicId}">
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>

                        <tr>
                            <td width="20%" class="text-right">类型：</td>
                            <td width="80%" class="text-left">
                                <select class="form-control" required name="objectType">
                                    <option value=""> -- 请选择 -- </option>
                                    <option value="1" <c:if test="${item.objectType == 1}">selected</c:if>>文本</option>
                                    <option value="2" <c:if test="${item.objectType == 2}">selected</c:if>>应用</option>
                                    <option value="3" <c:if test="${item.objectType == 3}">selected</c:if>>图片</option>
                                    <option value="4" <c:if test="${item.objectType == 4}">selected</c:if>>视频</option>
                                </select>
                            </td>
                        </tr>

                        <tr class="app-tr" style="display:none;">
                            <td class="text-right"><b style="color:red;">*</b>应用：</td>
                            <td class="text-left">
                                <button id="<%=selectGameBtnId%>" type="button" class="btn btn-primary btn-sm">选择</button>
                                <input type="hidden" name="appId" value="${item.objectId}">
                                <div id="<%=gameContainerId%>">

                                </div>
                            </td>
                        </tr>

                        <tr class="text-tr" style="display:none;">
                            <td class="text-right"><b style="color:red;">*</b>文本内容：</td>
                            <td class="text-left">
                                <textarea class="form-control" rows="6" name="text" data-height="260">${content.text}</textarea>
                            </td>
                        </tr>

                        <tr class="video-tr" style="display:none;">
                            <td class="text-right"><b style="color:red;">*</b>视频地址：</td>
                            <td class="text-left">
                                <input type="url" class="form-control" value="${content.videoUrl}" name="videoUrl" placeholder="视频地址" />
                            </td>
                        </tr>

                        <tr class="image-tr" style="display:none;">
                            <td class="text-right"><b style="color:red;">*</b>图片地址：</td>
                            <td class="text-left">
                                <input type="url" class="form-control" value="${content.imgUrl}" name="imgUrl" placeholder="图片地址" />
                            </td>
                        </tr>

                        <tr class="imageSize-tr" style="display:none;">
                            <td class="text-right"><b style="color:red;">*</b>图片尺寸：</td>
                            <td class="text-left">
                                宽:<input type="number" name="width" style="width:60px" value="${content.width}" />px &nbsp;
                                高:<input type="number" name="height" style="width:60px" value="${content.height}" />px
                                <button class="btn btn-default" type="button" id="<%=readSizeBtn%>">读取</button>
                            </td>
                        </tr>


                        <tr class="link-tr" style="display:none;">
                            <td class="text-right">外链：</td>
                            <td class="text-left">
                                <input type="url" class="form-control" value="${content.link}" name="link" placeholder="外链" />
                            </td>
                        </tr>


                        <tr class="fontSize-tr" style="display:none;">
                            <td class="text-right">字体大小:</td>
                            <td class="text-left">
                                <select name="fontSize" class="form-control">
                                    <option value="28" <c:if test="${content.fontSize == 28}">selected</c:if>> 默认大小(28px)</option>
                                    <option value="56" <c:if test="${content.fontSize == 56}">selected</c:if>>标题(56px)</option>
                                </select>
                            </td>
                        </tr>

                        <tr class="color-tr" style="display:none;">
                            <td class="text-right">字体颜色：</td>
                            <td class="text-left">
                                <select name="color" class="form-control">
                                    <option value="0xff333333" <c:if test="${content.color == '0xff333333'}">selected</c:if> style="background-color:#333333;color:#333333;">黑色</option>
                                    <option value="0xff999999" <c:if test="${content.color == '0xff999999'}">selected</c:if> style="background-color:#999999;color:#999999;">灰色</option>
                                    <option value="0xffff0000" <c:if test="${content.color == '0xffff0000'}">selected</c:if> style="background-color:#ff0000;color:#ff0000;">红色</option>
                                    <option value="0xff0000ff" <c:if test="${content.color == '0xff0000ff'}">selected</c:if> style="background-color:#0000ff;color:#0000ff;">蓝色</option>
                                    <option value="0xff00ff00" <c:if test="${content.color == '0xff00ff00'}">selected</c:if> style="background-color:#00ff00;color:#00ff00;">绿色</option>
                                </select>
                            </td>
                        </tr>


                        <tr class="bold-tr" style="display:none;">
                            <td class="text-right">字体加粗：</td>
                            <td class="text-left">
                                <label class="checkbox-inline">
                                    <input type="radio" value="0" name="bold" <c:if test="${content.bold != 1 || content == null}">checked</c:if>/> 不加粗
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" value="1" <c:if test="${content.bold == 1}">checked</c:if> name="bold"> 加粗
                                </label>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>排序：</td>
                            <td class="text-left">
                                <input type="number" required class="form-control" value="${item.ordering}" name="ordering" placeholder="排序" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
    requirejs(['jquery','Fui'],function(){
        var $form = $("#<%=formId%>");
        var $objectTypeSelector = $form.find("select[name='objectType']");
        $("#<%=readSizeBtn%>").click(function(){
            var imgUrl = $form.find("input[name='imgUrl']").val();
            if(!imgUrl){
                layer.msg("请先填写图片地址");
                return;
            }
            var img = new Image();
            img.src = imgUrl;
            img.onload = function(){
                $form.find("input[name='width']").val(img.width);
                $form.find("input[name='height']").val(img.height);
            }
            img.onerror = function(){
                layer.msg("读取失败");
            }
        });
        $form.data("paramFn",function(){
            var type = $objectTypeSelector.val();
            if(type == 1){
                if(!$form.find("textarea[name='text']").val()){
                    layer.msg("请填写内容");
                    return false;
                }
            }else if(type == 2){
                if(!$form.find("input[name='appId']").val()){
                    layer.msg("请选择游戏");
                    return false;
                }
            }else if(type == 3){
                if(!$form.find("input[name='imgUrl']").val()){
                    layer.msg("请输入图片地址");
                    return false;
                }
                if(!$form.find("input[name='width']").val()){
                    layer.msg("请输入图片宽度");
                    return false;
                }
                if(!$form.find("input[name='height']").val()){
                    layer.msg("请输入图片高度");
                    return false;
                }
            }else if(type == 4){
                if(!$form.find("input[name='videoUrl']").val()){
                    layer.msg("请输入视频地址");
                    return false;
                }

                if(!$form.find("input[name='imgUrl']").val()){
                    layer.msg("请输入视频图片地址");
                    return false;
                }
            }

        });

        var $textTr = $form.find(".text-tr");
        var $videoTr = $form.find(".video-tr");
        var $imageTr = $form.find(".image-tr");
        var $appTr = $form.find(".app-tr");
        var $boldTr = $form.find(".bold-tr");
        var $colorTr = $form.find(".color-tr");
        var $fontSizeTr = $form.find(".fontSize-tr");
        var $linkTr = $form.find(".link-tr");
        var $imageSize = $form.find(".imageSize-tr");
        console.info($textTr);
        function hideAllTr(){
            $textTr.hide();
            $videoTr.hide();
            $imageTr.hide();
            $appTr.hide();
            $boldTr.hide();
            $colorTr.hide();
            $fontSizeTr.hide();
            $linkTr.hide();
            $imageSize.hide();
        }

        function changeObjectType(val){
            if(val == 1){ //文本
                hideAllTr();
                $textTr.show();
                $boldTr.show();
                $colorTr.show();
                $fontSizeTr.show();
                $linkTr.show();
            }else if(val == 2){ //应用
                hideAllTr();
                $appTr.show();
            }else if(val == 3){//图片
                hideAllTr();
                $imageTr.show();
                $imageSize.show();
            }else if(val == 4){//视频
                hideAllTr();
                $imageTr.show();
                $videoTr.show();
            }
        }

        $objectTypeSelector.change(function(){
            changeObjectType($objectTypeSelector.val());
        });



        <c:if test="${item.objectType>0}">
            changeObjectType(${item.objectType});
        </c:if>

        var $gameContainer = $("#<%=gameContainerId%>");
        var showGameInfo = function(icon,name){
            BaseUtil.addSingleImage($gameContainer,{src:icon},{size:"50",height:50,width:50});
            setTimeout(function(){
                $gameContainer.append("<div style='height:50px;line-height:50px;float: left'><strong>"+name+"</strong><div>")
            },200);
        }

        $("#<%=selectGameBtnId%>").click(function(){
            var gameSelector = new Game(function(data){
                $form.find("input[name='appId']").val(data.id);
                showGameInfo(data.icon,data.name);
                gameSelector.close();
            });
            gameSelector.select();
        });

        <c:if test="${versionInfo != null}">
        showGameInfo("${versionInfo.icon.src}","${gameInfo.name}");
        </c:if>
    });
</script>
