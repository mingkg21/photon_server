<%@ page import="java.util.UUID" %>
<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
    String chartId = UUID.randomUUID().toString();
    String formId = UUID.randomUUID().toString();
%>

<form method="post" id="<%=formId%>">
    <div class="form-group fui-data-wrap">
        <div class="form-inline">

            <div class="input-group">
                <input type="text" value="${startDate}" class="form-control fui-date" name="startDate" placeholder="开始日期" />
                <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
            </div>

            <div class="input-group">
                <input type="text" value="${startDate}" class="form-control fui-date" name="endDate" placeholder="结束日期" />
                <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
            </div>
            <button class="btn btn-primary" type="button">查询</button>

        </div>
    </div>
</form>

<div style="display: inline-block;min-height: 456px;width: 80%;">
    <canvas style="display: block; width: 770px; height: 385px;" id="<%=chartId%>"></canvas>
</div>


<script type="text/javascript">
    requirejs(['chart','jquery','Fui'],function(Chart){
        var $form = $("#<%=formId%>");
        var ctx = document.getElementById("<%=chartId%>").getContext('2d');
        var config =  {
            type: 'line',
            data: {
                //labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
                datasets: [{
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    data: [12, 19, 3, 5, 2, 3],
                    label: '在线人数'
                }]
            }
        };

        var myChart = null;

        var $startDate = $form.find("input[name='startDate']");
        var $endDate = $form.find("input[name='endDate']");
        $form.find("button").click(function(){
            var startDate = $startDate.val();
            if(!startDate){
                layer.msg("请选择开始时间");
                return;
            }

            var endDate = $endDate.val();
            if(!$endDate){
                layer.msg("请选择结束时间");
                return;
            }

            $.ajax({
                url : '${base}/log/boosterServerStatus/getReport',
                data : {
                    startDate : startDate,
                    endDate : endDate
                },
                success:function(data){
                    var labels = [];
                    var ds = [];
                    for(var i=0;i<data.length;i++){
                        labels[i] = data[i].label;
                        ds[i] =  data[i].value;
                    }
                    config.data.labels = labels;
                    config.data.datasets[0].data = ds;
                    if(myChart){
                        myChart.update();
                    }else{
                        myChart = new Chart(ctx,config);
                    }
                }
            });
        });
    });
</script>
