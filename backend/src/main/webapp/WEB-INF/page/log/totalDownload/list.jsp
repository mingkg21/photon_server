<%@ page import="java.util.UUID" %>
<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String searchBtnId = UUID.randomUUID().toString();
    String weekBtnId = UUID.randomUUID().toString();
    String monthBtnId = UUID.randomUUID().toString();
    String searchFormId = UUID.randomUUID().toString();
%>

<form id="<%=searchFormId%>" class="fui-search table-tool" method="post">
    <div class="form-group fui-data-wrap">

        <div class="form-inline" style="margin-bottom: 5px;">
            <button id="<%=weekBtnId%>" class="btn btn-default">近一周</button> <button id="<%=monthBtnId%>" class="btn btn-default">近30天</button>
        </div>

        <div class="form-inline">
            <div class="input-group">
                <input type="text" value="${startDate}"  class="form-control fui-date" name="startDate" placeholder="开始日期" />
                <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
            </div>

            <div class="input-group">
                <input type="text" value="${endDate}" class="form-control fui-date" name="endDate" placeholder="结束日期" />
                <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
            </div>
            <button id="<%=searchBtnId%>" class="btn btn-primary fui-date-search">查询</button>
        </div>
    </div>
</form>

<table class="fui-default-table"></table>

<script type="text/javascript">

    requirejs(['jquery','bootstrap','Fui'],function(){
        Fui.addBootstrapTable({
            url : '${base}/log/totalDownload/list',
            columns : [{
                title : '时间',
                align : 'center',
                width : '100',
                valign : 'middle',
                field : 'countDay',
                sortable:true,
                formatter : Fui.formatDate
            },{
                title : '下载点击量',
                align : 'center',
                width : '80',
                valign : 'middle',
                field : 'downloadCount',
                sortable:true
            },{
                title : '下载完成量',
                align : 'center',
                width : '80',
                valign : 'middle',
                sortable:true,
                field : 'downloadCompleteCount'
            },{
                title : '下载完成率',
                field : 'completePercent',
                align : 'center',
                width : '80',
                valign : 'middle',
                sortable:true,
                formatter:function(v,row){
                    if(!row.downloadCount){
                        return "100.00%";
                    }
                    var num = (row.downloadCompleteCount / row.downloadCount) * 100;
                    return num.toFixed(2)+"%";
                }
            }]
        });
    });


    var $searchForm = $("#<%=searchFormId%>");
    $("#<%=weekBtnId%>").click(function(){
        searchDay(6);
    });

    $("#<%=monthBtnId%>").click(function(){
        searchDay(30);
    });

    function searchDay(day){
        var now = new Date();
        var endTime = now.getTime();
        var startTime = endTime - (day * 24 * 60 * 60 * 1000)
        $searchForm.find("input[name='endDate']").val(Fui.formatDate(endTime));
        $searchForm.find("input[name='startDate']").val(Fui.formatDate(startTime));
        var $searchBtn = $("#<%=searchBtnId%>");
        $searchBtn.click();
    }
</script>