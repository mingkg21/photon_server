<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<form class="fui-search table-tool" method="post">
    <div class="form-group fui-data-wrap">
        <div class="form-inline">

            <div class="input-group">
                <input type="number" class="form-control" name="appId" placeholder="应用ID">
            </div>

            <div class="input-group">
                <input type="text" value="${startDate}" class="form-control fui-date" name="startDate" placeholder="开始日期" />
                <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
            </div>

            <div class="input-group">
                <input type="text" value="${startDate}" class="form-control fui-date" name="endDate" placeholder="结束日期" />
                <span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
            </div>
            <button class="btn btn-primary fui-date-search">查询</button>
        </div>
    </div>
</form>

<table class="fui-default-table"></table>

<script type="text/javascript">

    requirejs(['jquery','bootstrap','Fui'],function(){
        Fui.addBootstrapTable({
            url : '${base}/log/appDownload/list',
            showPageSummary:true,
            showAllSummary:true,
            showFooter : true,
            columns : [{
                title : '应用ID',
                align : 'center',
                width : '50',
                valign : 'middle',
                field : 'appId'
            },{
                title : '名称',
                align : 'left',
                width : '80',
                valign : 'middle',
                formatter : function (v,row) {
                    var gameInfo = row.gameInfo;
                    if(!gameInfo){
                        return "-";
                    }
                    var html = "";
                    if(gameInfo.icon){
                        html += Fui.formatImage(gameInfo.icon,"50") + "&nbsp;";
                    }
                    html += "<strong>"+gameInfo.name+"<strong>";
                    return html;
                }
            },{
                title : '时间',
                align : 'center',
                width : '100',
                valign : 'middle',
                field : 'countDay',
                sortable:true,
                formatter : Fui.formatDate,
                pageSummaryFormat:function(rows,aggsData){
                    return "小计:";
                },
                allSummaryFormat:function(rows,aggsData){
                    return "总计:";
                }
            },{
                title : '下载点击量',
                align : 'center',
                width : '80',
                valign : 'middle',
                field : 'downloadCount',
                sortable:true,
                pageSummaryFormat:function(rows,aggsData){
                    var r=0,row;
                    for(var i=rows.length-1;i>=0;i--){
                        row=rows[i];
                        if(row.downloadCount){
                            r = r+row.downloadCount;
                        }
                    }
                    return r;
                },
                allSummaryFormat:function(rows,aggsData){
                    if(!aggsData){
                        return "0"
                    }
                    return aggsData.downloadCount;
                }
            },{
                title : '下载完成量',
                align : 'center',
                width : '80',
                valign : 'middle',
                sortable:true,
                field : 'downloadCompleteCount',
                pageSummaryFormat:function(rows,aggsData){
                    var r=0,row;
                    for(var i=rows.length-1;i>=0;i--){
                        row=rows[i];
                        if(row.downloadCompleteCount){
                            r = r+row.downloadCompleteCount;
                        }
                    }
                    return r;
                },
                allSummaryFormat:function(rows,aggsData){
                    if(!aggsData){
                        return "0"
                    }
                    return aggsData.downloadCompleteCount;
                }
            },{
                field : 'completePercent',
                title : '下载完成率',
                align : 'center',
                width : '80',
                valign : 'middle',
                sortable:true,
                formatter:function(v,row){
                    if(!row.downloadCount){
                        return "100.00%";
                    }
                    var num = (row.downloadCompleteCount / row.downloadCount) * 100;
                    return num.toFixed(2)+"%";
                }
            }]
        });
    });

</script>