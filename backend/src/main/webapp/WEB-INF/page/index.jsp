<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>登录管理后台</title>
	<link id="bootstrap-css" rel="stylesheet" href="${base}/common/js/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="${base}/common/css/loginStyle.css" />
</head>
<body>
<div class="ch-container">
	<div class="row">
		<div class="row" style="margin:0px;">
			<div class="col-md-12 center login-header">
				<h2>登录管理后台</h2>
			</div>
		</div>
		<div class="row"  style="margin:0px;">
			<div class="well col-sm-8 col-sm-offset-2 col-md-offset-3 col-md-6 col-lg-offset-4 col-lg-4">
				<fieldset>
					<br/>
					<div class="input-group input-group-lg">
						<span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
						<input id="username" type="text" class="form-control" placeholder="用户名">
					</div>
					<div class="clearfix"></div><br>
					<div class="input-group input-group-lg">
						<span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
						<input id="password" type="password" class="form-control" placeholder="密码">
					</div>
					<div class="clearfix"></div><br>
					<!--
					<div class="input-group input-group-lg">
						<span class="input-group-addon"><i class="glyphicon glyphicon-check red"></i></span>
						<input id="verifyCode" class="form-control" placeholder="验证码" type="text" maxlength="4">
						<span class="input-group-addon"><img id="verifyImg" src="${base}/agent/verifycode.do" border="0" width="89" height="38" alt="点击刷新验证码"></span>
					</div> -->
					<div class="clearfix"></div><br>
					<div class="row">
						<div class="col-md-6 col-md-offset-3">
							<button type="submit" class="btn btn-primary btn-lg btn-block" id="loginBtn"><i class="glyphicon glyphicon-hand-right"></i> 登 录</button>
						</div>
					</div>
				</fieldset>
			</div>
		</div>
	</div>
</div>
</body></html>
<script src="${base}/common/js/jquery-1.12.4.min.js"></script>
<script src="${base}/common/js/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="${base}/common/js/layer3/layer.min.js"></script>
<script>
    $(function() {
        var $username=$("#username"),$pwd=$("#password"),$verifyCode=$("#verifyCode");
        function doLogin() {
            var account = $username.val().trim()
                ,pwd = $pwd.val();
            if (account == '') {
                layer.tips("请输入账号",$username);
                $username.focus();
                return;
            }
            if (pwd == '') {
                layer.tips("请输入密码",$pwd);
                $pwd.focus();
                return;
            }
//            if($verifyCode.length && !/^[0-9a-zA-Z]{4}$/.test(verifyCode)){
//                layer.tips("请输入验证码",$verifyCode);
//                $verifyCode.focus();
//                return;
//            }

            $.ajax({
                url : "${base}/login",
                data : {
                    account : account,
                    password : pwd
                },
                dataType:"json",
                type:"post",
                success : function(data, textStatus, xhr) {
                    var ceipstate = xhr.getResponseHeader("ceipstate")
                    if (!ceipstate || ceipstate == 1) {// 正常响应
                        window.location.href = "${base}/index";
                    } else {// 后台异常
                        layer.msg(data.msg||"后台异常，请联系管理员!");
                    }
                }
            });
        }

        $username.keyup(function(event){
            if(event.keyCode ==13){
                if(!$pwd.val()){
                    $pwd.focus();
                    return;
                }
                if(!$verifyCode.val()){
                    $verifyCode.focus();
                    return;
                }
                doLogin();
            }
        });
        $pwd.keyup(function(event){
            if(event.keyCode ==13){
                if(!$verifyCode.val()){
                    $verifyCode.focus();
                    return;
                }
                doLogin();
            }
        });
        $("#loginBtn").click(function(){
            doLogin();
            return false;
        });
    });
</script>