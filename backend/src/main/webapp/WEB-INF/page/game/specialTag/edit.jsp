<%@ page import="java.util.UUID" %>
<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String selectBtnId = UUID.randomUUID().toString();
    String picContainerId = UUID.randomUUID().toString();

    String selectIconBtnId = UUID.randomUUID().toString();

    String iconContainerId =  UUID.randomUUID().toString();
    String formId = UUID.randomUUID().toString();
%>
<form id="<%=formId%>" action="${base}/game/specialTag/saveOrUpdate" class="form-submit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    编辑标签
                </h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" value="${tag.id}">
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>
                        <tr>
                            <td width="20%" class="text-right"><b style="color:red;">*</b>标签名：</td>
                            <td width="80%" class="text-left">
                                <input type="text" required class="form-control" value="${tag.name}" name="name" placeholder="标签名" />
                            </td>
                        </tr>

                        <tr>
                            <td width="20%" class="text-right">描述：</td>
                            <td width="80%" class="text-left">
                                <textarea class="form-control" name="description">${tag.description}</textarea>
                            </td>
                        </tr>

                        <tr>
                            <td width="20%" class="text-right"><b style="color:red;">*</b>图标：</td>
                            <td width="80%" class="text-left">
                                <input type="hidden" name="icon" value="${tag.icon.src}"/>
                                <button id="<%=selectIconBtnId%>" type="button" class="btn btn-primary">选择</button>
                                <div id="<%=iconContainerId%>"></div>
                            </td>
                        </tr>

                        <tr>
                            <td width="20%" class="text-right"><b style="color:red;">*</b>背景图：</td>
                            <td width="80%" class="text-left">
                                <input type="hidden" name="image" value="${tag.image.src}"/>
                                <button id="<%=selectBtnId%>" type="button" class="btn btn-primary">选择</button>
                                <div id="<%=picContainerId%>"></div>
                            </td>
                        </tr>

                        <tr>
                            <td width="20%" class="text-right"><b style="color:red;">*</b>排序：</td>
                            <td width="80%" class="text-left">
                                <input type="number" required class="form-control" value="${tag.ordering}" name="ordering" />
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
        var $picContainer = $("#<%=picContainerId%>");
        var $form = $("#<%=formId%>");
        var $iconContainer = $("#<%=iconContainerId%>");

        $("#<%=selectIconBtnId%>").click(function(){
            var pic = new Picture(function(data){
                BaseUtil.addSingleImage($iconContainer,data);
                $form.find("input[name='icon']").val(data.src);
                pic.close();
            },false);
            pic .select();
        });



        $("#<%=selectBtnId%>").click(function(){
            var pic = new Picture(function(data){
                BaseUtil.addSingleImage($picContainer,data);
                $form.find("input[name='image']").val(data.src);
                pic.close();
            },false);
            pic .select();
        });

        <c:if test="${not empty tag.icon}">
        BaseUtil.addSingleImage($iconContainer,{src:"${tag.icon.src}"});
        </c:if>

        <c:if test="${not empty tag.image}">
        BaseUtil.addSingleImage($picContainer,{src:"${tag.image.src}"});
        </c:if>
    });
</script>
