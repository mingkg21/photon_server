<%@ page import="java.util.UUID" %>
<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
    String formId = java.util.UUID.randomUUID().toString();
%>

<form id="<%=formId%>" action="${base}/game/specialTag/batchBindTag" class="form-submit">
    <input name="tagId" type="hidden" value="${tagId}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    批量绑定标签
                </h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>
                         <tr>
                            <td class="text-right media-middle">
                                <b style="color:red;">*</b>导入个数:
                            </td>
                            <td colspan="3" class="text-left">
                                <input type="number" min="1" step="1" name="count" required class="form-control">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>
