<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String tableId = java.util.UUID.randomUUID().toString();
%>
<div class="form-group">
    <div class="form-inline">
        <button type="button" class="btn btn-default open-tab"
                dialog-title="新增游戏" tab-id="tab-system-game-add" data-refresh="true" data-icon="glyphicon glyphicon-th-list" href="${base}/game/info/goAddPage?tableId=<%=tableId%>" >新增</button>
    </div>
</div>

<form class="fui-search table-tool" method="post">
    <div class="form-group fui-data-wrap">
        <div class="form-inline">

            <div class="input-group">
                <input type="text" class="form-control" name="id" placeholder="应用ID">
            </div>

            <div class="input-group">
                <input type="text" class="form-control" name="name" placeholder="应用名">
            </div>

            <div class="input-group">
                <input type="text" class="form-control" name="packageName" placeholder="包名">
            </div>

            <div class="input-group">
                <select class="form-control" name="status">
                    <option value="">-- 状态 --</option>
                    <option value="2">启用</option>
                    <option value="1">禁用</option>
                </select>
            </div>

            <div class="input-group">
                <select class="form-control" name="newVersion">
                    <option value="">-- 版本更新 --</option>
                    <option value="true">有更新</option>
                </select>
            </div>

            <button class="btn btn-primary fui-date-search">查询</button>
        </div>
    </div>
</form>

<table id="<%=tableId%>" class="fui-default-table"></table>

<script type="text/javascript">


    function mutation(arr) {
    // 请把你的代码写在这里
        var a = arr[0].toLowerCase(); //第一个字符串 转小写
        var b = arr[1].toLowerCase(); //第二个字符串 转小写
        for(var i=0;i<b.length;i++){
            if(a.indexOf(b[i]) === -1){ //如果第一个字符串中没有包含第二个字符串的某个字符时，表明第一个字符串不包括第二个字符串的所有字符，返回false;
                return false;
            }
        }
        return true;
    }

    requirejs(['jquery','bootstrap','Fui'],function(){
        var tableId = "<%=tableId%>";
        Fui.addBootstrapTable({
            url : '${base}/game/info/list',
            columns : [{
                field : 'id',
                title : 'ID',
                align : 'center',
                width : '50',
                valign : 'middle',
                sortable : true
            },{
               field : 'name',
               title : '名称',
                align : 'left',
                width : '130',
                valign : 'middle',
                formatter : function (v,row) {
                   var html = "";
                   if(row.icon){
                       html += Fui.formatImage(row.icon,"50") + "&nbsp;";
                   }
                   html += "<strong>"+v+"<strong>";
                   return html;
                }
            },{
                field : 'packageName',
                title : '包名',
                align : 'center',
                width : '100',
                valign : 'middle',
                formatter : function (v,row) {
                    return "<div style='word-break: break-all;'>" + v + "</div>";
                }
            },{
                field : 'categoryName',
                title : '分类',
                align : 'center',
                width : '60',
                valign : 'middle'
            },{
                field : 'versionName',
                title : '版本名',
                align : 'center',
                width : '50',
                valign : 'middle'
            },{
                field : 'fileSize',
                title : '大小',
                align : 'center',
                width : '50',
                valign : 'middle',
                formatter : Fui.formatFileSize
            },{
                field : 'status',
                title : '状态',
                align : 'center',
                width : '40',
                valign : 'middle',
                formatter : function (v,row) {
                    return Fui.statusFormatter({
                        val : v,
                        url:'${base}/game/info/updateStatus?id='+row.id+'&status='
                    });
                }
            },{
                field : 'downloadCount',
                title : '下载量',
                align : 'center',
                width : '50',
                valign : 'middle',
                sortable : true
            },{
                title : '新版本',
                align : 'center',
                width : '50',
                valign : 'middle',
                formatter : function(v,row){
                    try{
                        var vn = row.newVersionName;
                        var vc = row.newVersionCode;
                        if(!vn){
                            return "-";
                        }
                        if(vc && vc > row.versionCode  || !row.versionName || mutation([row.versionName,vn]) == false ){
                            return "<font color='blue'>"+vn+"</font>";
                        }
                        return vn;
                    }catch (e){
                        console.info(row.id);
                        console.info(e);
                    }
                }
            },{
                field : 'createTime',
                title : '创建时间',
                align : 'center',
                width : '100',
                valign : 'middle',
                formatter:Fui.formatDatetime
            },{
                title : '操作',
                align : 'center',
                width : '40',
                valign : 'middle',
                formatter : function(v,row){

                    var arr = [ '<a class="open-tab" tab-id="tab-system-game-edit_',row.id,'" dialog-title="修改游戏:',row.name,'" data-refresh="true" data-icon="glyphicon glyphicon-th-list" href="${base}/game/info/goEditPage?tableId=',tableId,'&id=',
                        row.id,'" title="修改">' ,
                        '<i class="glyphicon glyphicon-edit"></i>', '</a> '
                    ];

                    if(row.status == 2){
                        arr.push(
                        '<a target="_blank" href="',baseInfo.domainWap,'/game/detail/',row.id,'" title="预览">' ,
                            '<i class="glyphicon glyphicon-eye-open"></i>', '</a> ');
                    }
                    return arr.join("");
                }
            }]
        });
    });
</script>