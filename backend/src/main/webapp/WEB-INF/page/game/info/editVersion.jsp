<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String selectIconBtnId = java.util.UUID.randomUUID().toString();
    String selectApkBtnId = java.util.UUID.randomUUID().toString();
    String selectGamePicBtnId = java.util.UUID.randomUUID().toString();
    String iconContainerId = java.util.UUID.randomUUID().toString();
    String gamePicContainerId = java.util.UUID.randomUUID().toString();
    String formId =  java.util.UUID.randomUUID().toString();
    String submitBtnId = java.util.UUID.randomUUID().toString();
    String dowanLoadApkId = java.util.UUID.randomUUID().toString();
%>
<form id="<%=formId%>" action="${base}/game/info/saveOrUpdateVersion" class="form-submit">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    编辑版本
                </h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="gameId" value="${gameId}">
                <input type="hidden" name="id" value="${versionInfo.id}">
                <input type="hidden" name="replacePackageName" value="false"/>
                <input type="hidden" name="packageName" value="${versionInfo.packageName}" />
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>
                        <tr>
                            <td width="10%" class="text-right">
                                <strong style="color:red;">*</strong>类型
                            </td>
                            <td colspan="3" width="80%" class="text-left">
                                <label class="radio-inline">
                                    <input type="radio" name="versionType" value="2" <c:if test="${add || versionInfo.versionType == 2}">checked</c:if>> 已发布
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="versionType" value="1" <c:if test="${versionInfo.versionType == 1}">checked</c:if>> 未上线
                                </label>
                            </td>
                        </tr>

                        <tr>
                            <td width="10%" class="text-right">
                                <strong style="color:red;">*</strong>图标
                            </td>
                            <td colspan="3" width="80%" class="text-left">
                                <button id="<%=selectIconBtnId%>" class="btn btn-primary btn-sm" type="button">选择</button>
                                <input type="hidden" name="icon" value="${versionInfo.icon.src}">
                                <div id="<%=iconContainerId%>">

                                </div>
                            </td>
                        </tr>

                        <tr class="apk-info">
                            <td class="text-right">
                                <strong style="color:red;">*</strong>应用包
                                <a href="javascript:void(0)" id="<%=dowanLoadApkId%>">下载</a>
                            </td>
                            <td colspan="3">
                                <div class="input-group" STYLE="width: 100%">
                                    <input type="text" required name="downloadUrl" value="${versionInfo.downloadUrl}" readonly class="form-control">
                                    <span class="input-group-btn">
                                        <button id="<%=selectApkBtnId%>" class="btn btn-primary" type="button">选择</button>
                                    </span>
                                </div>
                            </td>
                        </tr>


                        <tr class="apk-sign">
                            <td class="text-right">apk签名</td>
                            <td colspan="3">
                                <input type="text" class="form-control" value="${versionInfo.sign}" readonly name="sign" />
                            </td>
                        </tr>


                        <tr class="apk-info">
                            <td class="text-right">
                                <strong style="color:red;">*</strong>大小
                            </td>
                            <td>
                                <input type="number" readonly required class="form-control" value="${versionInfo.fileSize}" name="fileSize" />
                            </td>

                            <td class="text-right">
                                <strong style="color:red;">*</strong>版本号
                            </td>
                            <td>
                                <input type="text" readonly required class="form-control" value="${versionInfo.versionCode}" name="versionCode" />
                            </td>
                        </tr>

                        <tr>

                            <td class="text-right"  width="10%">
                                <strong style="color:red;">*</strong>版本名
                            </td>
                            <td  width="40%">
                                <input type="text" readonly required class="form-control" value="${versionInfo.versionName}" name="versionName" />
                            </td>

                            <td class="text-right"  width="10%">
                                <strong style="color:red;">*</strong>语言
                            </td>
                            <td  width="40%">
                                <select name="lang" required class="form-control">
                                    <option value=""></option>
                                    <option value="中文" <c:if test="${versionInfo.lang == '中文'}">selected</c:if>>中文</option>
                                    <option value="英文" <c:if test="${versionInfo.lang == '英文'}">selected</c:if>>英文</option>
                                    <option value="日文" <c:if test="${versionInfo.lang == '日文'}">selected</c:if>>日文</option>
                                    <option value="韩文" <c:if test="${versionInfo.lang == '韩文'}">selected</c:if>>韩文</option>
                                    <option value="其他" <c:if test="${versionInfo.lang == '其他'}">selected</c:if>>其他</option>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right">版本标签</td>
                            <td colspan="3">
                                <input type="text" class="form-control" value="${versionInfo.versionTags}" maxlength="9" name="versionTags" />
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right">兼容性</td>
                            <td colspan="3">
                                <input type="text" class="form-control" value="${versionInfo.adapterInfo}" name="adapterInfo" />
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right">更新日志</td>
                            <td colspan="3">
                                 <textarea class="form-control" name="updateLog">${versionInfo.updateLog}</textarea>
                            </td>
                        </tr>


                        <tr>
                            <td class="text-right">游戏截图</td>
                            <td colspan="3">
                                <input type="hidden" name="picJson">
                                <button id="<%=selectGamePicBtnId%>" class="btn btn-primary btn-sm" type="button">选择</button>
                                <div id="<%=gamePicContainerId%>">


                                </div>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button id="<%=submitBtnId%>" type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
        var $picContainer = $("#<%=gamePicContainerId%>");
        var $iconContainer = $("#<%=iconContainerId%>");
        var $form = $("#<%=formId%>");

        $form.data("errorcallback",function(data){

            if(data.code != 1){
                return;
            }

            var msg = "是否确认要用新包名["+data.newPackageName+"]替换旧包名["+data.currentPackageName+"]?";
            layer.confirm(msg,function(index){
                layer.close(index);
                $form.find("input[name='replacePackageName']").val("true");
                $("#<%=submitBtnId%>").click();
            },function(index){
                layer.close(index);
            })
        });

        $form.data("paramFn",function(){
            var arr = BaseUtil.getImageValues($picContainer);
            var json = JSON.stringify(arr);
            $form.find("input[name='picJson']").val(json);
        });
        var showFn = function(versionType){
            var $tr = $form.find(".apk-info");
            var $ts = $tr.find("input");
            var $signTr = $form.find(".apk-sign");
            var $versionNameField = $form.find("input[name='versionName']");
            if(versionType == 1){
                $tr.hide();
                $signTr.hide();
                $versionNameField.removeAttr("readonly");
                $ts.removeAttr("required");
            }else{
                $tr.show();
                $signTr.show();
                $versionNameField.attr("readonly","readonly");
                $ts.attr("required","required");
            }
        }

        $form.find("input[name='versionType']").click(function(){
            showFn(this.value);
        })

        $("#<%=selectIconBtnId%>").click(function () {
            var pic = new Picture(function(data){
                BaseUtil.addSingleImage($iconContainer,data,{width:50,height:50,size:"50"});
                $form.find("input[name='icon']").val(data.src);
                pic.close();
            },false);
            pic .select();
        });

        $("#<%=selectGamePicBtnId%>").click(function(){
            var pic = new Picture(function(data){
                BaseUtil.addManyImage($picContainer,data);
                //console.info(data);
                pic.close();
            },true);
            pic .select();
        });

        $("#<%=selectApkBtnId%>").click(function(){
             var apk = new Package(function(data){
                 console.info(data);
                 $form.find("input[name='replacePackageName']").val("false");
                 $form.find("input[name='downloadUrl']").val(data.src);
                 $form.find("input[name='fileSize']").val(data.size);
                 $form.find("input[name='packageName']").val(data.packageName);
                 $form.find("input[name='versionCode']").val(data.versionCode);
                 $form.find("input[name='versionName']").val(data.versionName);
                 $form.find("textarea[name='updateLog']").val(data.updateLog);
                 $form.find("input[name='adapterInfo']").val(data.adapterInfo);
                 $form.find("input[name='sign']").val(data.sign);
                 apk.close();
             });
             apk.select();
        });
        
        $("#<%=dowanLoadApkId%>").click(function() {
            console.info("aaaa");
            var url = $form.find("input[name='downloadUrl']").val();
            console.info($form.find("input[name='downloadUrl']"));
            if(!url){
                return;
            }
            window.open(baseInfo.cdn+url);
        })

        //console.info("VersionType:"+$form.find("input[name='versionType']").val());
        showFn($form.find("input[name='versionType']:checked").val());

       <c:if test="${not empty versionInfo.icon}">
            BaseUtil.addSingleImage($iconContainer,{src:"${versionInfo.icon.src}"},{width:50,height:50,size:"50"});
        </c:if>

        <c:if test="${not empty picJson}">
            var pics = ${picJson};
            if(!pics || pics.length == 0){
                return;
            }
            var data = [];
            for(var i=0 ; i < pics.length;i++){
                var pic = pics[i];
                data.push({src:pic.src.src,height:pic.height,width:pic.width,size:pic.size});
            }
            BaseUtil.addManyImage($picContainer,data);
        </c:if>
    });
</script>