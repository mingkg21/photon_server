<%@ page import="java.util.UUID" %>
<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
    String gameContainerId = java.util.UUID.randomUUID().toString();
    String selectGameBtnId = java.util.UUID.randomUUID().toString();
    String formId = java.util.UUID.randomUUID().toString();
%>

<form id="<%=formId%>" action="${base}/game/info/saveRelatedGame" class="form-submit">
    <input name="gameId" type="hidden" value="${gameId}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    关联游戏
                </h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>
                        <tr>
                            <td width="15%" class="text-right"><b style="color:red;">*</b>游戏：</td>
                            <td width="85%" colspan="3" class="text-left">
                                <button id="<%=selectGameBtnId%>" type="button" class="btn btn-primary btn-sm">选择</button>
                                <input type="hidden" name="relatedGameId" value="${related.id.relatedGameId}">
                                <div id="<%=gameContainerId%>">

                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td width="20%" class="text-right"><b style="color:red;">*</b>排序：</td>
                            <td width="80%" class="text-left">
                                <input type="number" required class="form-control" value="${related.ordering}" name="ordering" placeholder="排序" />
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
        var $form = $("#<%=formId%>");
        $form.data("paramFn",function(){
            if(!$form.find("input[name='relatedGameId']").val()){
                layer.msg("请选择游戏");
                return false;
            }
        });

        var $gameContainer = $("#<%=gameContainerId%>");
        var showGameInfo = function(icon,name){
            BaseUtil.addSingleImage($gameContainer,{src:icon},{size:"50",height:50,width:50});
            setTimeout(function(){
                $gameContainer.append("<div style='height:50px;line-height:50px;float: left'><strong>"+name+"</strong><div>")
            },200);
        }

        $("#<%=selectGameBtnId%>").click(function(){
            var gameSelector = new Game(function(data){
                $form.find("input[name='relatedGameId']").val(data.id);
                showGameInfo(data.icon,data.name);
                gameSelector.close();
            });
            gameSelector.select();
        });

        <c:if test="${versionInfo != null}">
        showGameInfo("${versionInfo.icon.src}","${gameInfo.name}");
        </c:if>
    });
</script>
