<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="modal-dialog" style="width: 1000px;">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close fui-close" >&times;</button>
            <h4 class="modal-title">
                选择游戏
            </h4>
        </div>

        <div class="modal-body">
            <form class="fui-search table-tool" method="post">
                <div class="form-group fui-data-wrap">
                    <div class="form-inline">

                        <div class="input-group">
                            <input type="text" class="form-control" name="name" placeholder="游戏名">
                        </div>

                        <div class="input-group">
                            <input type="text" class="form-control" name="packageName" placeholder="包名">
                        </div>

                        <button class="btn btn-primary fui-date-search">查询</button>
                    </div>
                </div>
            </form>
            <table class="fui-default-table"></table>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default fui-close">取消</button>
        </div>
    </div>
</div>

<script language="javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
        function callback(selData){
            var fn = Game.getCallback("${fnId}");
            if(fn){
                fn(selData);
            }else{
                layer.msg("ERROR:找不到回调方法");
            }
        }

        Fui.addBootstrapTable({
            url : '${base}/game/info/list4Select?versionType=${versionType}&showAll=${showAll}',
            pageSize : 8,
            columns : [{
                field : 'name',
                title : '名称',
                align : 'left',
                width : '130',
                valign : 'middle',
                formatter : function (v,row) {
                    var html = "";
                    if(row.icon){
                        html += Fui.formatImage(row.icon,"50")+"&nbsp;";
                    }
                    html += "<strong>"+v+"<strong>";
                    return html;
                }
            },{
                field : 'packageName',
                title : '包名',
                align : 'center',
                width : '100',
                valign : 'middle',
                formatter : function (v,row) {
                    return "<div style='word-break: break-all;'>" + v + "</div>";
                }
            },{
                field : 'categoryName',
                title : '分类',
                align : 'center',
                width : '60',
                valign : 'middle'
            },{
                field : 'versionName',
                title : '版本名',
                align : 'center',
                width : '60',
                valign : 'middle'
            },{
                title : '操作',
                align : 'center',
                width : '60',
                valign : 'middle',
                formatter : function(v,row){
                    return '<a class="select" title="选择" href="javascript:void(0);"><i class="glyphicon glyphicon-ok"></i></a>';
                },
                events: {
                    'click .select': function (e, value, row, index) {
                        callback(row);
                    }
                }
            } ]
        });
    });
</script>