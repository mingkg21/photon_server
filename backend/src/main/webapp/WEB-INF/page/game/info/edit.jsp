<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String tab1Id = java.util.UUID.randomUUID().toString();
    String tab2Id = java.util.UUID.randomUUID().toString();
    String tab3Id = java.util.UUID.randomUUID().toString();

    String selectVideoBtnId = java.util.UUID.randomUUID().toString();
    String formId = java.util.UUID.randomUUID().toString();
    String videoContainerId = java.util.UUID.randomUUID().toString();
    String saveBtnId = java.util.UUID.randomUUID().toString();
    String selectBannerBtnId = java.util.UUID.randomUUID().toString();
    String bannerContainerId = java.util.UUID.randomUUID().toString();

    String selectCoverBtnId = java.util.UUID.randomUUID().toString();
    String coverCotainerId = java.util.UUID.randomUUID().toString();


    String relatedTableId = java.util.UUID.randomUUID().toString();
%>

<ul class="nav nav-tabs">
    <li class="active">
        <a href="#<%=tab1Id%>" data-toggle="tab">
            游戏信息
        </a>
    </li>
<c:if test="${gameInfo != null}">
    <li>
        <a href="#<%=tab2Id%>" data-toggle="tab">
            版本信息
        </a>
    </li>

    <li>
        <a href="#<%=tab3Id%>" data-toggle="tab">
            相关游戏
        </a>
    </li>
</c:if>
</ul>

<div class="tab-content">
    <div class="tab-pane fade in active" id="<%=tab1Id%>">
        <form id="<%=formId%>" action="${base}/game/info/saveOrUpdate" class="form-submit" unReloadTable="true">
            <input type="hidden" name="id" value="${gameInfo.id}">
            <table class="table table-bordered table-striped">
                <tbody>
                <tr>
                    <td class="text-right media-middle" width="10%">
                        <b style="color:red;">*</b>游戏名称:
                    </td>
                    <td class="text-left" width="40%">
                        <input type="text" required class="form-control" name="name" value="${gameInfo.name}" >
                    </td>

                    <td class="text-right media-middle" width="10%">
                        英文名称:
                    </td>
                    <td class="text-left" width="40%">
                        <input type="text" class="form-control" name="nameEn" value="${gameInfo.nameEn}">
                    </td>
                </tr>
                <tr>
                    <td class="text-right media-middle">
                        <b style="color:red;">*</b>游戏类型:
                    </td>
                    <td class="text-left">
                         <input name="categoryId" type="text" data-max-items="1" required  placeholder="空格触发选择" data-url="${base}/game/category/getForSelectize" class="fui-select" data-json-value='${categoryJson}' />
                    </td>

                    <td class="text-right media-middle">
                        <b style="color:red;">*</b>发行商:
                    </td>
                    <td class="text-left">
                        <input name="publisherId" type="text" data-max-items="1" required  placeholder="空格触发选择" class="fui-select" data-json-value='${publisherJson}' data-url="${base}/game/publisher/getForSelectize"/>
                    </td>
                </tr>
                <tr>
                    <td class="text-right media-middle">
                        <b style="color:red;">*</b>游戏包名:</td>
                    <td colspan="3" class="text-left">
                        <input type="text" required class="form-control" value="${gameInfo.packageName}" name="packageName">
                    </td>
                </tr>
                <c:if test="${gameInfo == null}">
                <tr>
                    <td class="text-right media-middle">
                        <b style="color:red;">*</b>初始评分:</td>
                    <td colspan="3" class="text-left">
                        <input type="number" max="10" min="0" required value="6" class="form-control" name="score">
                    </td>
                </tr>
                </c:if>

                <tr>
                    <td class="text-right media-middle">
                        <b style="color:red;">*</b>游戏标签:
                    </td>
                    <td colspan="3" class="text-left">
                        <input name="tagIds" type="text" required  placeholder="空格触发选择" data-url="${base}/game/tag/getForSelectize" class="fui-select" data-json-value='${tagsJson}'>
                    </td>
                </tr>

                <tr>
                    <td class="text-right media-middle">
                        特色标签:
                    </td>
                    <td colspan="3" class="text-left">
                        <input name="specialTagIds" type="text" placeholder="空格触发选择" data-url="${base}/game/specialTag/getForSelectize" class="fui-select" data-json-value='${specialTagsJson}'>
                    </td>
                </tr>

                <tr>
                    <td class="text-right media-middle">
                        编辑寄语:
                    </td>
                    <td colspan="3" class="text-left">
                        <textarea name="editorRecommend" rows="5" class="form-control">${gameInfo.editorRecommend}</textarea>
                    </td>
                </tr>

                <tr>
                    <td class="text-right media-middle">
                        <b style="color:red;">*</b>游戏简介:
                    </td>
                    <td colspan="3" class="text-left">
                        <textarea  required name="description" rows="5"  class="form-control">${gameInfo.description}</textarea>
                    </td>
                </tr>

                <tr>
                    <td class="text-right media-middle">
                        一句话介绍:
                    </td>
                    <td colspan="3" class="text-left">
                        <textarea required name="devRecommend" class="form-control">${gameInfo.devRecommend}</textarea>
                    </td>
                </tr>
                <tr>
                    <td class="text-right media-middle">
                        游戏视频:
                    </td>
                    <td colspan="3" class="text-left">
                        <input type="hidden" name="video" value="${gameInfo.video.src}">
                        <input type="hidden" name="videoRaw" value="${gameInfo.videoRaw.src}">
                        <button type="button" id="<%=selectVideoBtnId%>" class="btn btn-primary">选择视频</button>
                        <button type="button" onclick="Fui.copyCdnUrl(this,'video')" class="btn btn-default">复制地址</button>
                        <div id="<%=videoContainerId%>">

                        </div>
                    </td>
                </tr>


                <tr>
                    <td class="text-right media-middle">
                        Banner:
                    </td>
                    <td colspan="3" class="text-left">
                        <input type="hidden" name="banner" value="${gameInfo.banner.src}">
                        <button type="button" id="<%=selectBannerBtnId%>" class="btn btn-primary">选择图片</button>
                        <button type="button" onclick="Fui.copyCdnUrl(this,'banner')" class="btn btn-default">复制地址</button>
                        <div id="<%=bannerContainerId%>">

                        </div>
                    </td>
                </tr>



                <tr>
                    <td class="text-right media-middle">
                        详情封面:
                    </td>
                    <td colspan="3" class="text-left">
                        <input type="hidden" name="coverImage" value="${gameInfo.coverImage.src}">
                        <button type="button" id="<%=selectCoverBtnId%>" class="btn btn-primary">选择图片</button>
                        <button type="button" onclick="Fui.copyCdnUrl(this,'coverImage')" class="btn btn-default">复制地址</button>
                        <div id="<%=coverCotainerId%>">

                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
            <button class="btn btn-primary" id="<%=saveBtnId%>">保存</button>
        </form>
    </div>
<c:if test="${gameInfo != null}">
    <div class="tab-pane fade" id="<%=tab2Id%>">
        <div class="input-group">
            <button type="button" url="${base}/game/info/goAddVersionPage?gameId=${gameInfo.id}"  dialog-title="新增版本" class="btn btn-default open-dialog">新增版本</button>
        </div>
        <table class="fui-default-table"></table>
    </div>

    <div class="tab-pane fade" id="<%=tab3Id%>">
        <div class="input-group">
            <button type="button" url="${base}/game/info/goAddRelatedGamePage?gameId=${gameInfo.id}"  dialog-title="添加相关游戏" class="btn btn-default open-dialog">添加相关游戏</button>
        </div>
        <table id="<%=relatedTableId%>"></table>
    </div>
</c:if>

</div>

<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){

        var $bannerContainer = $("#<%=bannerContainerId%>");
        var $coverContainer = $("#<%=coverCotainerId%>")
        var $form = $("#<%=formId%>");
        $form.data("callback",function(data){
             layer.msg("保存成功");
             //新增的时候需要关闭
            <c:if test="${gameInfo == null}">
             Fui.closeActiveTab();
            </c:if>
        });
        var $videoContainer =  $("#<%=videoContainerId%>");
        var removeVideoFn = function(){
            $form.find("input[name='video']").val("");
            $form.find("input[name='videoRaw']").val("");
            $videoContainer.html("");
        }

        <c:if test="${gameInfo.video != null}">
         BaseUtil.addVideo($videoContainer,"${gameInfo.video.cndSrc}",removeVideoFn);
        </c:if>

        $("#<%=selectVideoBtnId%>").click(function(){
           var videoSelect = new Video(function(data){
               $form.find("input[name='video']").val(data.src);
               $form.find("input[name='videoRaw']").val(data.m3u8);
               BaseUtil.addVideo($videoContainer,data.cdnSrc,removeVideoFn);
               videoSelect.close();
           });
           videoSelect.select();
        });

        $("#<%=selectBannerBtnId%>").click(function(){
            var picSelect = new Picture(function(data){
                BaseUtil.addSingleImage($bannerContainer,data,{});
                $form.find("input[name='banner']").val(data.src);
                picSelect.close();
            });
            picSelect.select();
        });


        $("#<%=selectCoverBtnId%>").click(function(){
            var picSelect = new Picture(function(data){
                BaseUtil.addSingleImage($coverContainer,data,{});
                $form.find("input[name='coverImage']").val(data.src);
                picSelect.close();
            });
            picSelect.select();
        });

        <c:if test="${gameInfo != null}">
            Fui.addBootstrapTable({
                url : '${base}/game/info/getVersionList?gameId=${gameInfo.id}',
                columns : [{
                    field : 'versionName',
                    title : '版本名',
                    align : 'center',
                    width : '60',
                    valign : 'middle',
                    formatter:function(v,row){
                        var html = "";
                        if(row.lastestVersion == 1){
                            html = '<i title="最新版" class="glyphicon glyphicon-star"></i>';
                        }
                        return html + v;
                    }
                },{
                    field : 'versionType',
                    title : '版本状态',
                    align : 'center',
                    width : '60',
                    valign : 'middle',
                    formatter:function(v){
                        if(v == 1){
                            return "未上线";
                        }
                        return "<font color=green>已发布</font>";
                    }
                },{
                    field : 'packageName',
                    title : '包名',
                    align : 'center',
                    width : '100',
                    valign : 'middle'
                },{
                    field : 'fileSize',
                    title : '大小',
                    align : 'center',
                    width : '60',
                    valign : 'middle',
                    formatter : Fui.formatFileSize
                },{
                    field : 'status',
                    title : '状态',
                    align : 'center',
                    width : '60',
                    valign : 'middle',
                    formatter : function (v,row) {
                        return Fui.statusFormatter({
                            val : v,
                            url:'${base}/game/info/updateVersionStatus?id='+row.id+'&status='
                        });
                    }
                },{
                    field : 'createTime',
                    title : '创建时间',
                    align : 'center',
                    width : '120',
                    valign : 'middle',
                    formatter:Fui.formatDatetime
                },{
                    title : '操作',
                    align : 'center',
                    width : '100',
                    valign : 'middle',
                    formatter : function(v,row){
                        var arr =  [ '<a class="open-dialog" dialog-title="修改版本" data-refresh="true" data-icon="glyphicon glyphicon-th-list" href="${base}/game/info/goEditVersionPage?id=',
                            row.id,'" title="修改">',
                            '<i class="glyphicon glyphicon-edit"></i>', '</a> ',
                            '<a class="todo-ajax" href="${base}/game/info/setLastestVersion?id=',row.id,'" title="设置成最新版本">',
                            '<i class="glyphicon glyphicon-heart"></i>', '</a>'
                        ];
                        return arr.join("");
                    }
                }]
            });

            Fui.addBootstrapTable({
                id:'<%=relatedTableId%>',
                url : '${base}/game/info/getRelatedGameList?gameId=${gameInfo.id}',
                columns : [{
                    field : 'relatedGameId',
                    title : '应用ID',
                    align : 'center',
                    width : '50',
                    valign : 'middle',
                    sortable : true
                },{
                    field : 'name',
                    title : '名称',
                    align : 'left',
                    width : '130',
                    valign : 'middle',
                    formatter : function (v,row) {
                        var html = "";
                        if(row.icon){
                            html += Fui.formatImage(row.icon,"50") + "&nbsp;";
                        }
                        html += "<strong>"+v+"<strong>";
                        return html;
                    }
                },{
                    field : 'ordering',
                    title : '排序',
                    align : 'center',
                    width : '50',
                    valign : 'middle'
                },{
                    title : '操作',
                    align : 'center',
                    width : '40',
                    valign : 'middle',
                    formatter : function(v,row){
                        return [
                            '<a class="open-dialog" dialog-title="修改" href="${base}/game/info/goEditRelatedGamePage?relatedGameId=',
                            row.relatedGameId,'&gameId=${gameInfo.id}" title="修改">',
                            '<i class="glyphicon glyphicon-edit"></i>', '</a>',

                            ' <a class="todo-ajax" href="${base}/game/info/deleteRelatedGame?relatedGameId=',
                            row.relatedGameId,'&gameId=${gameInfo.id}" title="删除">',
                            '<i class="glyphicon glyphicon-trash"></i>', '</a>'
                        ].join("");
                    }
                }]
            });

            <c:if test="${not empty gameInfo.banner}">
            BaseUtil.addSingleImage($bannerContainer,{src:"${gameInfo.banner.src}"},{});
            </c:if>

            <c:if test="${not empty gameInfo.coverImage}">
            BaseUtil.addSingleImage($coverContainer,{src:"${gameInfo.coverImage.src}"},{});
            </c:if>

        </c:if>
    });
</script>

