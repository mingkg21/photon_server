<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" class="btn btn-default open-dialog"  dialog-title="新增发行商" href="${base}/game/publisher/goAddPage" >新增</button>
	</div>
</div>

<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
       // var $div = Fui.getParentNode();
        Fui.addBootstrapTable({
            url : '${base}/game/publisher/list',
            columns : [{
                field : 'name',
                title : '发行商',
                align : 'center',
                width : '200',
                valign : 'middle'
            },{
                field : 'status',
                title : '状态',
                align : 'center',
                width : '200',
                valign : 'middle',
                formatter : function (v,row) {
					return Fui.statusFormatter({
						val : v,
						url:'${base}/game/publisher/updateStatus?id='+row.id+'&status='
					});
                }
            }, {
                title : '操作',
                align : 'center',
                width : '200',
                valign : 'middle',
                formatter : function(v,row){
                    return [ '<a class="open-dialog" dialog-title="修改发行商" href="${base}/game/publisher/goEditPage?id=',
							row.id,'" title="修改">',
						'<i class="glyphicon glyphicon-edit"></i>', '</a>'].join("");
				}
            } ]
        });
    });
</script>

