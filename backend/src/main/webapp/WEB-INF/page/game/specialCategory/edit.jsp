<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<form action="${base}/game/specialCategory/saveOrUpdate" class="form-submit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    编辑类别
                </h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" value="${category.id}">
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>
                        <tr>
                            <td width="20%" class="text-right">类别名：</td>
                            <td width="80%" class="text-left">
                                <input type="text" required class="form-control" value="${category.name}" name="name" placeholder="类别名" />
                            </td>
                        </tr>

                        <tr>
                            <td width="20%" class="text-right">排序：</td>
                            <td width="80%" class="text-left">
                                <input type="number" required class="form-control" value="${category.ordering}" name="ordering" placeholder="排序" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>
