<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" class="btn btn-default open-dialog"  dialog-title="绑定标签" href="${base}/game/specialCategory/goAddBindTagPage?categoryId=${categoryId}" >新增</button>
	</div>
</div>


<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">

			<div class="input-group">
				<input type="text" class="form-control" name="tagName" placeholder="标签名">
			</div>

			<button class="btn btn-primary fui-date-search">查询</button>
		</div>
	</div>
</form>


<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
       // var $div = Fui.getParentNode();
        Fui.addBootstrapTable({
            url : '${base}/game/specialCategory/getTagList?categoryId=${categoryId}',
            columns : [{
                field : 'name',
                title : '标签名称',
                align : 'center',
                width : '100',
                valign : 'middle'
            },{
                field : 'ordering',
                title : '排序',
                align : 'center',
                width : '60',
                valign : 'middle'
            },{
                title : '操作',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : function(v,row){
                    return [
                        ' <a class="todo-ajax" href="${base}/game/specialCategory/unbindTag?categoryId=${categoryId}&&tagId=',
							row.id,'" title="解绑标签">',
						'<i class="glyphicon glyphicon-trash"></i></a> ' ,
                        ' <a class="open-dialog" dialog-title="修改标签" href="${base}/game/specialCategory/goEditBindTagPage?categoryId=${categoryId}&tagId='
                        ,row.id,'" title="修改标签">',
                        '<i class="glyphicon glyphicon-edit"></i>', '</a> '].join("");
				}
            } ]
        });
    });
</script>

