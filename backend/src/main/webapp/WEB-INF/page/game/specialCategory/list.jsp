<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" class="btn btn-default open-dialog"  dialog-title="新增类别" href="${base}/game/specialCategory/goAddPage" >新增</button>
	</div>
</div>


<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">
			<div class="input-group">
				<input type="text" class="form-control" name="name" placeholder="游戏类别名称">
			</div>
			<button class="btn btn-primary fui-date-search">查询</button>
		</div>
	</div>
</form>


<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
       // var $div = Fui.getParentNode();
        Fui.addBootstrapTable({
            url : '${base}/game/specialCategory/list',
            columns : [{
                field : 'name',
                title : '类别名',
                align : 'center',
                width : '200',
                valign : 'middle'
            },{
                field : 'status',
                title : '状态',
                align : 'center',
                width : '200',
                valign : 'middle',
                formatter : function (v,row) {
					return Fui.statusFormatter({
						val : v,
						url:'${base}/game/specialCategory/updateStatus?id='+row.id+'&status='
					});
                }
            },{
                field : 'ordering',
                title : '排序',
                align : 'center',
                width : '80',
                valign : 'middle'
			},{
                field : 'count',
                title : '标签数量',
                align : 'center',
                width : '100',
                valign : 'middle'
			},{
                title : '操作',
                align : 'center',
                width : '200',
                valign : 'middle',
                formatter : function(v,row){
                    return [ '<a class="open-dialog" dialog-title="修改类别" href="${base}/game/specialCategory/goEditPage?id=',
							row.id,'" title="修改类别">',
						'<i class="glyphicon glyphicon-edit"></i>', '</a> ',


                        ' <a class="open-tab" tab-id="tab-system-tag-games_',row.id,'" dialog-title="综合分类标签集（',row.name,'）"  data-icon="glyphicon glyphicon-zoom-in" href="${base}/game/specialCategory/goTagListPage?categoryId=',
                        row.id,'" title="查看综合分类标签集">',
                        '<i class="glyphicon glyphicon-tags"></i>', '</a>'

					].join("");
				}
            } ]
        });
    });
</script>

