<%@ page import="java.util.UUID" %>
<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
    String formId = java.util.UUID.randomUUID().toString();
%>

<form id="<%=formId%>" action="${base}/game/specialCategory/bindTag" class="form-submit">
    <input name="categoryId" type="hidden" value="${categoryId}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    绑定标签
                </h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>
                             <tr>
                                <td class="text-right media-middle">
                                    <b style="color:red;">*</b>游戏标签:
                                </td>
                                <td colspan="3" class="text-left">
                                    <input name="tagId" type="text"  data-max-items="1"  required placeholder="空格触发选择" data-url="${base}/game/tag/getForSelectize" class="fui-select" data-json-value='${tagJson}'>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-right"><b style="color:red;">*</b>排序：</td>
                                <td colspan="3" class="text-left">
                                    <input name="ordering" required class="form-control" value="${item.ordering}" type="number"/>
                                </td>
                            </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>
