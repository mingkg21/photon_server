<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" class="btn btn-default open-dialog"  dialog-title="新增预约" href="${base}/game/bespeak/goAddPage" >新增</button>
	</div>
</div>

<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
       // var $div = Fui.getParentNode();
        Fui.addBootstrapTable({
            url : '${base}/game/bespeak/list',
            columns : [{
                title : '名称',
                align : 'left',
                width : '100',
                valign : 'middle',
                formatter : function (v,row) {
                    var html = "";
                    if(row.icon){
                        html += Fui.formatImage(row.icon,"50");
                    }
                    html += "<strong>"+row.name+"<strong>";
                    return html;
                }
            },{
                field : 'versionName',
                title : '版本',
                align : 'center',
                width : '60',
                valign : 'middle'
			},{
                field : 'categoryName',
                title : '分类',
                align : 'center',
                width : '60',
                valign : 'middle'
			},{
                field : 'testTime',
                title : '测试时间',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : Fui.formatDatetime
			},{
                field : 'testTypeName',
                title : '测试类型',
                align : 'center',
                width : '80',
                valign : 'middle'
			},{
                field : 'bespeakCount',
                title : '预约人数',
                align : 'center',
                width : '60',
                valign : 'middle'
			},{
                field : 'bespeakStatus',
                title : '状态',
                align : 'center',
                width : '60',
                valign : 'middle',
                formatter : function (v,row) {
					if(v == 1){
					    return "<font color='green'>预约中</font>";
					}
					if(row.downloadUrl){
                        return "<font color='blue'>开放下载</font>";
					}
					return "已关闭";
                }
            }, {
                title : '操作',
                align : 'center',
                width : '150',
                valign : 'middle',
                formatter : function(v,row){
                    return [ '<a class="open-dialog" dialog-title="修改预约" href="${base}/game/bespeak/goEditPage?id=',
							row.id,'" title="修改">',
						'<i class="glyphicon glyphicon-edit"></i>', '</a>'].join("");
				}
            }]
        });
    });
</script>

