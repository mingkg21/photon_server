<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String gameContainerId = java.util.UUID.randomUUID().toString();
    String selectGameBtnId = java.util.UUID.randomUUID().toString();
    String formId = java.util.UUID.randomUUID().toString();
%>
<form id="<%=formId%>"  action="${base}/game/bespeak/saveOrUpdate" class="form-submit">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    编辑预约
                </h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" value="${bespeakInfo.id}">
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>
                        <tr>
                            <td width="15%" class="text-right"><b style="color:red;">*</b>游戏：</td>
                            <td width="85%" colspan="3" class="text-left">
                                <button id="<%=selectGameBtnId%>" type="button" class="btn btn-primary btn-sm">选择</button>
                                <input type="hidden" name="gameId" value="${bespeakInfo.gameId}">
                                <div id="<%=gameContainerId%>">

                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td width="15%" class="text-right"><b style="color:red;">*</b>版本：</td>
                            <td width="35%" class="text-left">
                                <select class="form-control" name="versionId" readonly required >

                                </select>
                            </td>

                            <td width="15%" class="text-right">是否推荐：</td>
                            <td width="35%" class="text-left">
                                <label class="checkbox-inline">
                                    <input type="radio" name="recommendStatus" value="0" <c:if test="${bespeakInfo == null || bespeakInfo.recommendStatus == 0}"> checked</c:if>> 否
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" name="recommendStatus" value="1" <c:if test="${bespeakInfo.recommendStatus == 1}">checked</c:if>> 是
                                </label>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>测试类型：</td>
                            <td class="text-left">
                                <select class="form-control" name="testTypeId" required >
                                    <option value=""></option>
                                    <c:forEach items="${testTypes}" var="testType">
                                    <option value="${testType.id}" <c:if test="${testType.id == bespeakInfo.testTypeId}">selected</c:if> >${testType.name}</option>
                                    </c:forEach>
                                </select>
                            </td>

                            <td class="text-right"><b style="color:red;">*</b>测试时间：</td>
                            <td class="text-left">
                                <input type="text" required class="form-control fui-date" name="testTime" format="YYYY-MM-DD HH:mm:ss" value="<fmt:formatDate value="${bespeakInfo.testTime}" pattern="yyyy-MM-dd HH:mm:ss"/>" >
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right">预约状态：</td>
                            <td class="text-left">
                                <label class="checkbox-inline">
                                    <input type="radio" name="bespeakStatus" value="0" <c:if test="${bespeakInfo == null || bespeakInfo.bespeakStatus == 0}"> checked</c:if>> 关闭
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" name="bespeakStatus" value="1" <c:if test="${bespeakInfo.bespeakStatus == 1}">checked</c:if>> 开启
                                </label>
                            </td>

                            <td class="text-right">&nbsp;</td>
                            <td class="text-left">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    requirejs(['jquery','Fui'],function(){
        var $form = $("#<%=formId%>");
        $form.data("paramFn",function(){
            if(!$form.find("input[name='gameId']").val()){
                layer.msg("请选择游戏");
                return false;
            }
        });
        var $selector = $form.find("select[name='versionId']");
        var $gameContainer = $("#<%=gameContainerId%>");
        var showGameInfo = function(icon,name){
            BaseUtil.addSingleImage($gameContainer,{src:icon},{size:"50",height:50,width:50});
            setTimeout(function(){
                $gameContainer.append("<div style='height:50px;line-height:50px;float: left'><strong>"+name+"</strong><div>")
            },200);
        }

        var readVersionInfo = function(gameId,selectedVersionId){
            $.ajax({
                url:"${base}/game/info/getAllVersion",
                data:{
                    gameId:gameId
                },
                success:function(data){
                    $selector.append('<option value=""></option>');
                    var list = data.versionList;
                    for(var i=0;i<list.length;i++){
                        var item = list[i];
                        var selected = selectedVersionId == item.id ? "selected" : "";
                        $selector.append("<option value='"+item.id+"' "+selected+">"+item.versionName+"</option>");
                    }
                    $selector.removeAttr("readonly");
                }
            });
        }

        $("#<%=selectGameBtnId%>").click(function(){
             var gameSelector = new Game(function(data){
                 $selector.html("");
                 $selector.attr("readonly");
                 $form.find("input[name='gameId']").val(data.id);
                 showGameInfo(data.icon,data.name);
                 readVersionInfo(data.id);
                 gameSelector.close();
             });
            gameSelector.select();
        });

       <c:if test="${bespeakInfo != null}">
        showGameInfo("${versionInfo.icon.src}","${gameInfo.name}");
        readVersionInfo(${bespeakInfo.gameId},${bespeakInfo.versionId});
        </c:if>
    });
</script>