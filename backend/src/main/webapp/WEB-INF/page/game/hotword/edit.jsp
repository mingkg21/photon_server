<%@ page import="java.util.UUID" %>
<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String selectBtnId = UUID.randomUUID().toString();
    String picContainerId = UUID.randomUUID().toString();
    String formId = UUID.randomUUID().toString();
%>
<form id="<%=formId%>" action="${base}/game/hotword/saveOrUpdate" class="form-submit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    编辑标签
                </h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" value="${hotword.id}">
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>
                        <tr>
                            <td width="20%" class="text-right">热词：</td>
                            <td width="80%" class="text-left">
                                <input type="text" required class="form-control" value="${hotword.name}" name="name"/>
                            </td>
                        </tr>

                        <tr>
                            <td width="20%" class="text-right">排序：</td>
                            <td width="80%" class="text-left">
                                <input type="text" required class="form-control" value="${hotword.ordering}" name="ordering" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>
