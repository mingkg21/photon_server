<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" class="btn btn-default open-dialog"  dialog-title="新增热词" href="${base}/game/hotword/goAddPage" >新增</button>
	</div>
</div>


<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">

			<div class="input-group">
				<input type="text" class="form-control" name="name" placeholder="热词">
			</div>

			<button class="btn btn-primary fui-date-search">查询</button>
		</div>
	</div>
</form>


<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
       // var $div = Fui.getParentNode();
        Fui.addBootstrapTable({
            url : '${base}/game/hotword/list',
            columns : [{
                field : 'name',
                title : '热词',
                align : 'center',
                width : '200',
                valign : 'middle'
            },{
                field : 'defaultWord',
                title : '默认搜索词',
                align : 'center',
                width : '150',
                valign : 'middle',
                formatter : function (v,row) {
					if(v == 1){
					    return '<font color="red">是</font>';
					}
					return '否';
                }
            },{
                field : 'ordering',
                title : '排序',
                align : 'center',
                width : '100',
                valign : 'middle'
			}, {
                title : '操作',
                align : 'center',
                width : '200',
                valign : 'middle',
                formatter : function(v,row){
                    return [ '<a class="open-dialog" dialog-title="修改热词" href="${base}/game/hotword/goEditPage?id=',
							row.id,'" title="修改">',
						'<i class="glyphicon glyphicon-edit"></i>', '</a> ',
                        '<a class="todo-ajax" href="${base}/game/hotword/delete?id=',
                        row.id,'" title="删除" confirm="是否确认要删除?">',
                        '<i class="glyphicon glyphicon-trash"></i>', '</a> ',
                        '<a class="todo-ajax" href="${base}/game/hotword/resetDefaultWord?id=',
                        row.id,'" title="设置默认搜索词" confirm="是否确认要设置默认搜索词?">',
                        '<i class="glyphicon glyphicon-heart"></i>', '</a> ',
					].join("");
				}
            } ]
        });
    });
</script>

