<%@ page import="java.util.UUID" %>
<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
    String gameContainerId = java.util.UUID.randomUUID().toString();
    String selectGameBtnId = java.util.UUID.randomUUID().toString();
    String formId = java.util.UUID.randomUUID().toString();
%>
<form id="<%=formId%>" action="${base}/game/mapping/saveOrUpdate" class="form-submit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    编辑标签
                </h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" value="${mapping.id}">
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>
                        <tr>
                            <td class="text-right">匹配类型：</td>
                            <td  class="text-left">
                                <select required class="form-control" name="type">
                                    <option value="1" <c:if test="${mapping == null|| mapping.type == 1}">selected</c:if> >精确匹配</option>
                                    <option value="2" <c:if test="${mapping.type == 2}">selected</c:if>>前缀匹配</option>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>游戏：</td>
                            <td colspan="3" class="text-left">
                                <button id="<%=selectGameBtnId%>" type="button" class="btn btn-primary btn-sm">选择</button>
                                <input type="hidden" name="gameId" value="${mapping.gameId}">
                                <div id="<%=gameContainerId%>">

                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td width="20%" class="text-right">
                                <b style="color:red;">*</b>包名：
                                <c:if test="${mapping == null}">
                                <br/><font color="#" style="font-size: 10px">换行添加多个</font>
                                </c:if>
                            </td>
                            <td width="80%" class="text-left">
                                <c:if test="${mapping != null}">
                                    <input type="text" required class="form-control" value="${mapping.packageName}" name="packageName"/>
                                </c:if>

                                <c:if test="${mapping == null}">
                                    <textarea rows="6" class="form-control" name="packageName"></textarea>
                                </c:if>
                            </td>
                        </tr>


                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
        var $form = $("#<%=formId%>");
        $form.data("paramFn",function(){
            if(!$form.find("input[name='gameId']").val()){
                layer.msg("请选择游戏");
                return false;
            }
        });

        var $gameContainer = $("#<%=gameContainerId%>");
        var showGameInfo = function(icon,name){
            BaseUtil.addSingleImage($gameContainer,{src:icon},{size:"50",height:50,width:50});
            setTimeout(function(){
                $gameContainer.append("<div style='height:50px;line-height:50px;float: left'><strong>"+name+"</strong><div>")
            },200);
        }

        $("#<%=selectGameBtnId%>").click(function(){
            var gameSelector = new Game(function(data){
                $form.find("input[name='gameId']").val(data.id);
                showGameInfo(data.icon,data.name);
                gameSelector.close();
            });
            gameSelector.select();
        });

        <c:if test="${versionInfo != null}">
        showGameInfo("${versionInfo.icon.src}","${gameInfo.name}");
        </c:if>
    });
</script>