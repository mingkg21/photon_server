<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" class="btn btn-default open-dialog"  dialog-title="新增热词" href="${base}/game/mapping/goAddPage" >新增</button>
	</div>
</div>


<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">

			<div class="input-group">
				<input type="text" class="form-control" name="packageName" placeholder="包名">
			</div>


			<div class="input-group">
				<input type="text" class="form-control" name="gameId" placeholder="游戏ID">
			</div>

			<button class="btn btn-primary fui-date-search">查询</button>
		</div>
	</div>
</form>


<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
       // var $div = Fui.getParentNode();
        Fui.addBootstrapTable({
            url : '${base}/game/mapping/list',
            columns : [{
                field : 'packageName',
                title : '包名',
                align : 'center',
                width : '200',
                valign : 'middle'
            },{
                field : 'name',
                title : '游戏',
                align : 'left',
                width : '130',
                valign : 'middle',
                formatter : function (v,row) {
                    var html = "";
                    if(row.icon){
                        html += Fui.formatImage(row.icon,"50") + "&nbsp;";
                    }
                    html += "<strong>"+v+"("+row.gameId+")<strong>";
                    return html;
                }
			},{
                field : 'type',
                title : '匹配类型',
                align : 'center',
                width : '120',
                valign : 'middle',
                formatter : function (v,row) {
					if(v == 1){
					    return '精确匹配';
					}else if(v == 2){
					    return "前缀匹配";
					}
					return '';
                }
            },{
                field : 'createTime',
                title : '创建时间',
                align : 'center',
                width : '100',
                valign : 'middle',
                formatter:Fui.formatDatetime
			},{
                title : '操作',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : function(v,row){
                    return [ '<a class="open-dialog" dialog-title="修改热词" href="${base}/game/mapping/goEditPage?id=',
							row.id,'" title="修改">',
						'<i class="glyphicon glyphicon-edit"></i>', '</a> ',
                        '<a class="todo-ajax" href="${base}/game/mapping/delete?id=',
                        row.id,'" title="删除" confirm="是否确认要删除?">',
                        '<i class="glyphicon glyphicon-trash"></i>', '</a> '

					].join("");
				}
            }]
        });
    });
</script>

