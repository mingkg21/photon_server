<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String gameContainerId = java.util.UUID.randomUUID().toString();
    String selectGameBtnId = java.util.UUID.randomUUID().toString();
    String formId = java.util.UUID.randomUUID().toString();
%>
<form id="<%=formId%>"  action="${base}/game/recruit/saveOrUpdate" class="form-submit">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    编辑招募
                </h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" value="${recruitInfo.id}">
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>
                        <tr>
                            <td width="15%" class="text-right"><b style="color:red;">*</b>游戏：</td>
                            <td width="85%" colspan="3" class="text-left">
                                <button id="<%=selectGameBtnId%>" type="button" class="btn btn-primary btn-sm">选择</button>
                                <input type="hidden" name="gameId" value="${recruitInfo.gameId}">
                                <div id="<%=gameContainerId%>">

                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>打赏类型：</td>
                            <td class="text-left">
                                <select required class="form-control" name="prizeType">
                                    <option value=""></option>
                                    <option value="0" <c:if test="${recruitInfo.prizeType == 0}">selected</c:if>>创造力</option>
                                    <option value="1" <c:if test="${recruitInfo.prizeType == 1}">selected</c:if>>洞察力</option>
                                </select>
                            </td>

                            <td class="text-right"><b style="color:red;">*</b>打赏积分：</td>
                            <td class="text-left">
                                <input type="number" required class="form-control" name="prizeScore" min="1" max="100" value="${recruitInfo.prizeScore}" >
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>开始时间：</td>
                            <td class="text-left">
                                <input type="text" required class="form-control fui-date" name="startTime" format="YYYY-MM-DD HH:mm:ss" value="<fmt:formatDate value="${recruitInfo.startTime}" pattern="yyyy-MM-dd HH:mm:ss"/>" >
                            </td>

                            <td class="text-right"><b style="color:red;">*</b>结束时间：</td>
                            <td class="text-left">
                                <input type="text" required class="form-control fui-date" name="endTime" format="YYYY-MM-DD HH:mm:ss" value="<fmt:formatDate value="${recruitInfo.endTime}" pattern="yyyy-MM-dd HH:mm:ss"/>" >
                            </td>
                        </tr>

                        <tr>
                            <td width="15%" class="text-right"><b style="color:red;">*</b>鲜游点评：</td>
                            <td width="85%" colspan="3" class="text-left">
                                <textarea class="form-control" style="height: 100px" name="comment">${recruitInfo.comment}</textarea>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    requirejs(['jquery','Fui'],function(){
        var $form = $("#<%=formId%>");
        $form.data("paramFn",function(){
            if(!$form.find("input[name='gameId']").val()){
                layer.msg("请选择游戏");
                return false;
            }
        });
        var $gameContainer = $("#<%=gameContainerId%>");
        var showGameInfo = function(icon,name){
            BaseUtil.addSingleImage($gameContainer,{src:icon},{size:"50",height:50,width:50});
            setTimeout(function(){
                $gameContainer.append("<div style='height:50px;line-height:50px;float: left'><strong>"+name+"</strong><div>")
            },200);
        }

        $("#<%=selectGameBtnId%>").click(function(){
             var gameSelector = new Game(function(data){
                 $form.find("input[name='gameId']").val(data.id);
                 showGameInfo(data.icon,data.name);
                 gameSelector.close();
             });
            gameSelector.select();
        });

       <c:if test="${recruitInfo != null}">
        showGameInfo("${versionInfo.icon.src}","${gameInfo.name}");
        </c:if>
    });
</script>