<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" class="btn btn-default open-dialog"  dialog-title="新增招募" href="${base}/game/recruit/goAddPage" >新增</button>
	</div>
</div>

<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
        Fui.addBootstrapTable({
            url : '${base}/game/recruit/list',
            columns : [{
                title : '名称',
                align : 'left',
                width : '100',
                valign : 'middle',
                formatter : function (v,row) {
                    var html = "";
                    if(row.icon){
                        html += Fui.formatImage(row.icon,"50");
                    }
                    html += "<strong>"+row.name+"<strong>";
                    return html;
                }
            },
				/**
			{
                field : 'versionName',
                title : '版本',
                align : 'center',
                width : '60',
                valign : 'middle'
			},{
                field : 'categoryName',
                title : '分类',
                align : 'center',
                width : '60',
                valign : 'middle'
			}, **/
			{
				field : 'startTime',
				title : '开始时间',
				align : 'center',
				width : '100',
				valign : 'middle',
                formatter:Fui.formatDatetime
			},{
				field : 'endTime',
				title : '结束时间',
				align : 'center',
				width : '100',
				valign : 'middle',
                formatter:Fui.formatDatetime
			}, {
                field : 'count',
                title : '参与人数',
                align : 'center',
                width : '60',
                valign : 'middle'
			},{
                field : 'status',
                title : '状态',
                align : 'center',
                width : '60',
                valign : 'middle',
                formatter : function (v,row) {
                    return Fui.statusFormatter({
						onVal:1,
						offVal:0,
						onText:'开启',
						offText:'关闭',
                        val : v,
                        url:'${base}/game/recruit/updateStatus?id='+row.id+'&status='
                    });
                }
            },{
                title : '操作',
                align : 'center',
                width : '150',
                valign : 'middle',
                formatter : function(v,row){
                    return [
                        '<a class="open-dialog" dialog-title="修改招募" href="${base}/game/recruit/goEditPage?id=',
						row.id,'" title="修改">',
						'<i class="glyphicon glyphicon-edit"></i>', '</a> &nbsp;',
                        '<a class="open-tab" tab-id="tab-game-recruit-question-edit_',row.id,'"  data-refresh="true"  dialog-title="题库-',row.name,'" href="${base}/game/recruit/goQuestionPage?recruitId=',
                        row.id,'" title="查看题库">',
                        '<i class="glyphicon glyphicon-question-sign"></i>', '</a>&nbsp;',
                        '<a  class="open-tab" tab-id="tab-game-answer-record-checked_',row.id,'" data-refresh="true" dialog-title="答题记录-',row.name,'" href="${base}/game/recruit/goQuestionPage?recruitId=',
                        row.id,'" title="答题记录">',
                        '<i class="glyphicon glyphicon-info-sign"></i>', '</a>&nbsp;'
					].join("");
				}
            }]
        });
    });
</script>

