<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String tableId = java.util.UUID.randomUUID().toString();
    String addAnswerBtnId = java.util.UUID.randomUUID().toString();
    String formId = java.util.UUID.randomUUID().toString();
%>
<form id="<%=formId%>" action="${base}/game/recruit/saveOrUpdateQuestion" class="form-submit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    编辑题目
                </h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="questionId" value="${question.id}">
                <input type="hidden" name="recruitId" value="${recruitId}">
                <input type="hidden" name="answersJson">
                <table id="<%=tableId%>" class="table table-bordered table-striped" style="clear: both">
                    <tbody>
                    <tr>
                        <td width="30%" class="text-right">题目：</td>
                        <td width="70%" class="text-left">
                            <textarea style="height: 100px;" required class="form-control" name="question">${question.question}</textarea>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">答案组：</td>
                        <td class="text-left">
                            <button id="<%=addAnswerBtnId%>" type="button" class="btn btn-primary btn-sm">添加答案栏</button>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    requirejs(['jquery','Fui'],function(){

        var $form = $("#<%=formId%>");
        $form.data("paramFn",function(){
            var arr = [];
            var as = $form.find("input[name='answer']");
            var i = 0;
            $form.find("input[name='answerId']").each(function(){
                arr.push({
                    answer:as[i].value,
                    id:this.value
                });
                i++;
            });
            $form.find("input[name='answersJson']").val(JSON.stringify(arr));
//            params=$form.serialize();
//            params.answersJson = JSON.stringify(arr);
//           // console.info(params);
//            console.info(params);
//            return params;
        });

        var $table = $("#<%=tableId%>");

        $("#<%=addAnswerBtnId%>").click(function(){
            addAnswer();
        });

        function addAnswer(answerId,content){
            answerId = answerId || "";
            content = content || "";
            var tpl =['<tr>',
                '<td class="text-right"><button type="button" class="btn btn-danger btn-xs">删除</button>答案栏：</td>',
                '<td class="text-left">',
                '<input type="hidden" name="answerId" value="',answerId,'"/>',
                '<input type="text" required class="form-control" value="',content,'" name="answer"/>',
                '</td>',
             '</tr>'];
            var el = $(tpl.join(''));
            el.find("button").click(function () {
               $(this).parents('tr').remove();
            });
            $table.append(el);
        }
        <c:if test="${answers == null}">
            addAnswer();
            addAnswer();
        </c:if>
        <c:if test="${answers != null}">
            <c:forEach items="${answers}" var="an">
                 addAnswer(${an.id},'${an.answer}');
            </c:forEach>
        </c:if>
    });
</script>