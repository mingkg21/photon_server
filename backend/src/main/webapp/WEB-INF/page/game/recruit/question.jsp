<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
    <div class="form-inline">
        <button type="button" class="btn btn-default open-dialog"  dialog-title="新增题目" href="${base}/game/recruit/goAddQuestionPage?recruitId=${recruitId}" >新增</button>
    </div>
</div>

<table class="fui-default-table"></table>

<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
        Fui.addBootstrapTable({
            url : '${base}/game/recruit/getQuestionList?recruitId=${recruitId}',
            columns : [{
                title : '题目',
                field : 'question',
                align : 'center',
                width : '600',
                valign : 'middle'
            },{
                title : '操作',
                align : 'center',
                width : '150',
                valign : 'middle',
                formatter : function(v,row){
                    return [
                        '<a class="open-dialog" dialog-title="修改题目" href="${base}/game/recruit/goEditQuestionPage?questionId=',
                        row.id,'" title="修改">','<i class="glyphicon glyphicon-edit"></i>', '</a> &nbsp;'
                    ].join("");
                }
            }]
        });
    });
</script>