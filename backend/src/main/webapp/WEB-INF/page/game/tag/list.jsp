<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" class="btn btn-default open-dialog"  dialog-title="新增标签" href="${base}/game/tag/goAddPage" >新增</button>
	</div>
</div>

<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">

			<div class="input-group">
				<input type="text" class="form-control" name="name" placeholder="标签名">
			</div>

			<div class="input-group">
				<select class="form-control" name="status">
					<option value="">-- 状态 --</option>
					<option value="2">启用</option>
					<option value="1">禁用</option>
				</select>
			</div>
			<button class="btn btn-primary fui-date-search">查询</button>
		</div>
	</div>
</form>


<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
       // var $div = Fui.getParentNode();
        Fui.addBootstrapTable({
            url : '${base}/game/tag/list',
            columns : [{
                field : 'id',
                title : 'ID',
                align : 'center',
                width : '80',
                valign : 'middle'
            },{
                field : 'name',
                title : '名称',
                align : 'center',
                width : '200',
                valign : 'middle'
            },{
                field : 'status',
                title : '状态',
                align : 'center',
                width : '60',
                valign : 'middle',
                formatter : function (v,row) {
					return Fui.statusFormatter({
						val : v,
						url:'${base}/game/tag/updateStatus?id='+row.id+'&status='
					});
                }
            },{
                field : 'recommendOrdering',
                title : '推荐排序',
                align : 'center',
                width : '60',
                valign : 'middle'
            },{
                field : 'appCount',
                title : '应用数量',
                align : 'center',
                width : '60',
                valign : 'middle'
			},{
                title : '操作',
                align : 'center',
                width : '200',
                valign : 'middle',
                formatter : function(v,row){
                    return [ '<a class="open-dialog" dialog-title="修改标签" href="${base}/game/tag/goEditPage?id=',
							row.id,'" title="修改">',
						'<i class="glyphicon glyphicon-edit"></i>', '</a>',

                        ' <a class="open-tab" tab-id="tab-system-tag-games_',row.id,'" dialog-title="游戏标签集（',row.name,'）"  data-icon="glyphicon glyphicon-zoom-in" href="${base}/game/tag/goGameListPage?tagId=',
                        row.id,'" title="查看游戏标签集">',
                        '<i class="glyphicon glyphicon-zoom-in"></i>', '</a>'

					].join("");
				}
            } ]
        });
    });
</script>

