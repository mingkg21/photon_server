<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" class="btn btn-default open-dialog"  dialog-title="绑定游戏" href="${base}/game/tag/goBindGamePage?tagId=${tagId}" >新增</button>
	</div>
</div>


<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">

			<div class="input-group">
				<input type="number" class="form-control" name="gameId" placeholder="游戏ID">
			</div>

			<div class="input-group">
				<input type="text" class="form-control" name="gameName" placeholder="游戏名">
			</div>

			<div class="input-group">
				<input type="text" class="form-control" name="packageName" placeholder="包名">
			</div>

			<button class="btn btn-primary fui-date-search">查询</button>
		</div>
	</div>
</form>


<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
       // var $div = Fui.getParentNode();
        Fui.addBootstrapTable({
            url : '${base}/game/tag/getGameList?tagId=${tagId}',
            columns : [{
                field : 'id',
                title : '游戏ID',
                align : 'center',
                width : '50',
                valign : 'middle'
            },{
                field : 'name',
                title : '名称',
                align : 'left',
                width : '100',
                valign : 'middle',
                formatter : function (v,row) {
                    var html = "";
                    if(row.icon){
                        html += Fui.formatImage(row.icon,"50");
                    }
                    html += "<strong>"+v+"<strong>";
                    return html;
                }
            },{
                field : 'packageName',
                title : '包名',
                align : 'center',
                width : '100',
                valign : 'middle'
            },{
                field : 'versionName',
                title : '版本名',
                align : 'center',
                width : '60',
                valign : 'middle'
            },{
                field : 'ordering',
                title : '排序',
                align : 'center',
                width : '60',
                valign : 'middle'
            },{
                title : '操作',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : function(v,row){
                    return [
                        ' <a class="todo-ajax" href="${base}/game/tag/unbindGame?tagId=${tagId}&&gameId=',
							row.id,'" title="解绑游戏">',
						'<i class="glyphicon glyphicon-trash"></i>',
                        ' <a class="open-dialog" dialog-title="修改" href="${base}/game/specialTag/goEditBindGamePage?tagId=${tagId}&gameId='
                        ,row.id,'" title="修改">',
                        '<i class="glyphicon glyphicon-edit"></i>', '</a> '].join("");
				}
            } ]
        });
    });
</script>

