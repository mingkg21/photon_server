<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String formId = java.util.UUID.randomUUID().toString();
    String picContainerId = java.util.UUID.randomUUID().toString();
%>
<form id="<%=formId%>"  action="${base}/account/info/savePermission" class="form-submit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    编辑用户权限
                </h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" value="${userInfo.id}">
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>
                        <tr>
                            <td width="20%" class="text-right" style="vertical-align:middle">用户：</td>
                            <td width="80%" class="text-left" >
                                <input type="hidden" name="headIcon" value="${userProfileInfo.headIcon.src}">
                                <div id="<%=picContainerId%>">

                                </div>
                                <div style="line-height: 71px;">
                                ${userInfo.nickName}
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right">权限设置：</td>
                            <td class="text-left">
                                <label class="checkbox-inline">
                                    <input type="checkbox" <c:if test="${uap == null || uap.allowGameComment == 1}">checked</c:if> name="gameComment" value="1"> 游戏评论
                                </label>

                                <label class="checkbox-inline">
                                    <input type="checkbox" <c:if test="${uap == null || uap.allowSaveRecordComment == 1}">checked</c:if> name="saveRecordComment" value="1"> 存档评论
                                </label>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    requirejs(['jquery','Fui'],function(){
        var $picContainer = $("#<%=picContainerId%>");
        var showHeadIcon = function(icon){
            BaseUtil.addSingleImage($picContainer,{src:icon},{size:"50",height:50,width:50});
            setTimeout(function(){
                $picContainer.append("<div style='height:50px;line-height:50px;float: left'><div>")
            },200);
        }

       <c:if test="${userProfileInfo != null}">
        showHeadIcon("${userProfileInfo.headIcon.src}");
       </c:if>
    });
</script>