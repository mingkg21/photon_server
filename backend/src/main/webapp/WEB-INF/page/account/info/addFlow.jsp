<%@ page import="java.util.UUID" %>
<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String formId = UUID.randomUUID().toString();
%>
<form id="<%=formId%>" action="${base}/account/info/addFlow" class="form-submit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">

                </h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" value="${userId}">
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>
                        <tr>
                            <td width="20%" class="text-right"><b style="color:red;">*</b>单位：</td>
                            <td width="80%" class="text-left">
                                <select class="form-control" name="unit">
                                    <option value="MB">MB</option>
                                    <option value="GB">GB</option>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td width="20%" class="text-right">流量：</td>
                            <td width="80%" class="text-left">
                                <input class="form-control" name="flow" max="1023" min="1">
                            </td>
                        </tr>

                        <tr>
                            <td width="20%" class="text-right">备注：</td>
                            <td width="80%" class="text-left">
                               <textarea class="form-control" name="remark"></textarea>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
//    requirejs(['jquery','bootstrap','Fui'],function(){
//
//    });
</script>
