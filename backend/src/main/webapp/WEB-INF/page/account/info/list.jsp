<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" class="btn btn-default open-dialog"  dialog-title="新增用户" href="${base}/account/info/goAddPage" >新增</button>
	</div>
</div>

<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">

			<div class="input-group">
				<input type="number" class="form-control" name="id" placeholder="ID">
			</div>

			<div class="input-group">
				<input type="text" class="form-control" name="nickName" placeholder="昵称">
			</div>

			<div class="input-group">
				<input type="number" class="form-control" name="phone" placeholder="手机">
			</div>


			<div class="input-group">
				<select name="registerType" class="form-control">
					<option value="">---- 选择注册方式 ----</option>
					<option value="1">手机号</option>
					<option value="2">QQ</option>
					<option value="3">微信</option>
					<option value="4">18游戏盒</option>
				</select>

			</div>

			<button class="btn btn-primary fui-date-search">查询</button>
		</div>
	</div>
</form>


<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
        Fui.addBootstrapTable({
            url : '${base}/account/info/list',
            columns : [{
                title : 'ID',
                align : 'center',
                width : '50',
                valign : 'middle',
                field : 'id'
			},{
                title : '用户名',
                align : 'left',
                width : '100',
                valign : 'middle',
                formatter : function(v,row){
                    var html = "";
                    if(row.headIcon){
                        html += Fui.formatImage(row.headIcon,"50");
                    }
                    html += "&nbsp;"+row.nickName;
                    return html;
				}
			},{
                title : '注册方式',
                align : 'center',
                width : '60',
                valign : 'middle',
                field : 'registerType',
                formatter : function(v,row){
                    if(v == 1){
                        return "手机号";
					}
					if(v == 2){
                        return "QQ";
					}

					if(v == 3){
					    return "微信";
					}

					if(v == 4){
					    return "18游戏盒";
					}
					return "-";
                }
			},{
                title : '手机号',
                align : 'center',
                width : '80',
                valign : 'middle',
                field : 'phone'
            },{
                field : 'status',
                title : '状态',
                align : 'center',
                width : '50',
                valign : 'middle',
                formatter : function(v,row){
               		return Fui.statusFormatter({
						onVal:'actived',
                        offVal:'locked',
                        val:v,
						url:'${base}/account/info/updateStatus?id='+row.id+'&status='
					});
                }
            },{
                title : '用户类型',
                align : 'center',
                width : '80',
                valign : 'middle',
                field : 'fakeFlag',
                formatter : function(v,row){
                    if(v == 0){
                        return "<font color='blue'>真实用户</font>";
					}else if(v == 1){
                        return "<font color='red'>马甲用户</font>";
					}
				}
            },{
                field : 'createDatetime',
                title : '注册时间',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : Fui.formatDatetime
			},{
                field : 'lastLoginDatetime',
                title : '最后登录时间',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : Fui.formatDatetime
            },{
                field : 'lastLoginIp',
                title : '最后登录IP',
                align : 'center',
                width : '80',
                valign : 'middle'
			},{
                title : '可用流量',
                align : 'center',
                width : '60',
                valign : 'middle',
                formatter : function(v,row){
                    if(!row.totalFlow){
                        return "0KB";
					}
					var free = row.totalFlow - row.usedDownloadFlow - row.usedUploadFlow;
                    if(free < 0){
                        return "0KB";
					}
                   	return Fui.formatFileSize(free);
                }
			},{
                title : '操作',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : function(v,row){
                   var arr = [
                       '<a class="open-dialog" dialog-title="修改" href="${base}/account/info/goEditPage?id=',
                        row.id,'" title="修改">',
                        '<i class="glyphicon glyphicon-edit"></i>',
					   '</a> ',

                       '<a class="open-dialog" dialog-title="设置用户权限" href="${base}/account/info/goEditPermissionPage?id=',
                       row.id,'" title="设置用户权限">',
                       '<i class="glyphicon glyphicon-cog"></i>',
                       '</a> ',


                       '<a class="open-dialog" dialog-title="添加流量" href="${base}/account/info/goAddFlowPage?userId=',
                       row.id,'" title="添加流量">',
                       '<i class="glyphicon glyphicon-plus"></i>',
                       '</a> ',


                       '<a class="todo-ajax" confirm="是否确认要重置密码?" title="重置密码(123456)"  href="${base}/account/info/resetPassword?userId=',
                       row.id,'">',
                       '<i class="glyphicon glyphicon-refresh"></i>',
                       '</a> '
				   ];
                    return arr.join("");
				}
            }]
        });
    });
</script>

