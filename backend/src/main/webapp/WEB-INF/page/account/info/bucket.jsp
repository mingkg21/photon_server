<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="modal-dialog" style="width: 1000px;">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close fui-close" >&times;</button>
            <h4 class="modal-title">
                选择用户
            </h4>
        </div>

        <div class="modal-body">
            <form class="fui-search table-tool" method="post">
                <div class="form-group fui-data-wrap">
                    <div class="form-inline">

                        <div class="input-group">
                            <select class="form-control" name="fakeFlag">
                                <option value="">用户类型</option>
                                <option value="1">马甲用户</option>
                                <option value="0">真实用户</option>
                            </select>
                        </div>

                        <div class="input-group">
                            <input type="text" class="form-control" name="phone" placeholder="手机号">
                        </div>

                        <div class="input-group">
                            <input type="number" class="form-control" name="id" placeholder="用户ID">
                        </div>

                        <div class="input-group">
                            <input type="text" class="form-control" name="nickName" placeholder="昵称">
                        </div>

                        <button class="btn btn-primary fui-date-search">查询</button>
                    </div>
                </div>
            </form>
            <table class="fui-default-table"></table>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default fui-close">取消</button>
        </div>
    </div>
</div>

<script language="javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){

        function callback(selData){
            var fn = UserSelector.getCallback("${fnId}");
            if(fn){
                fn(selData);
            }else{
                layer.msg("ERROR:找不到回调方法");
            }
        }

        Fui.addBootstrapTable({
            pageSize:8,
            url : '${base}/account/info/list4Select',
            columns : [{
                title : 'ID',
                align : 'center',
                width : '50',
                valign : 'middle',
                field : 'id'
            },{
                title : '用户名',
                align : 'left',
                width : '100',
                valign : 'middle',
                formatter : function(v,row){
                    var html = "";
                    if(row.headIcon){
                        html += Fui.formatImage(row.headIcon,"50");
                    }
                    html += "&nbsp;"+row.nickName;
                    return html;
                }
            },{
                title : '手机号',
                align : 'center',
                width : '80',
                valign : 'middle',
                field : 'phone'
            },{
                title : '用户类型',
                align : 'center',
                width : '80',
                valign : 'middle',
                field : 'fakeFlag',
                formatter : function(v,row){
                    if(v == 0){
                        return "<font color='blue'>真实用户</font>";
                    }else if(v == 1){
                        return "<font color='red'>马甲用户</font>";
                    }
                }
            },{
                title : '操作',
                align : 'center',
                width : '50',
                valign : 'middle',
                formatter : function(v,row){
                    return '<a class="select" title="选择" href="javascript:void(0);"><i class="glyphicon glyphicon-ok"></i></a>';
                },
                events: {
                    'click .select': function (e, value, row, index) {
                        callback(row);
                    }
                }
            }]
        });
    });
</script>