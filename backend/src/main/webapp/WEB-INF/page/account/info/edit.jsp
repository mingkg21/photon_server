<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String selectPicBtnId = java.util.UUID.randomUUID().toString();
    String formId = java.util.UUID.randomUUID().toString();
    String picContainerId = java.util.UUID.randomUUID().toString();
%>
<form id="<%=formId%>"  action="${base}/account/info/saveOrUpdate" class="form-submit">
    <div class="modal-dialog" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    编辑用户信息
                </h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" value="${userInfo.id}">
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>
                        <tr>
                            <td width="15%" class="text-right"><b style="color:red;">*</b>头像：</td>
                            <td width="85%" colspan="3" class="text-left">
                                <button id="<%=selectPicBtnId%>" type="button" class="btn btn-primary btn-sm">选择</button>
                                <input type="hidden" name="headIcon" value="${userProfileInfo.headIcon.src}">
                                <div id="<%=picContainerId%>">

                                </div>
                            </td>
                        </tr>


                        <tr>
                            <td width="15%" class="text-right"><b style="color:red;">*</b>用户昵称：</td>
                            <td width="35%" class="text-left">
                                <input type="text" required class="form-control" name="nickName" value="${userInfo.nickName}" >
                            </td>

                            <td width="15%" class="text-right">所在地区：</td>
                            <td width="35%" class="text-left">
                                <input type="text" class="form-control" name="area" value="${userProfileInfo.area}" >
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right">性别：</td>
                            <td class="text-left" >
                                <label class="radio-inline">
                                    <input type="radio" name="sex" value="0" <c:if test="${userProfileInfo.sex == 0}">checked</c:if>> 男
                                </label>

                                <label class="radio-inline">
                                    <input type="radio" name="sex" value="1" <c:if test="${userProfileInfo.sex == 1}">checked</c:if>> 女
                                </label>

                                <label class="radio-inline">
                                    <input type="radio" name="sex" value="2" <c:if test="${userProfileInfo == null || userProfileInfo.sex == 2 }">checked</c:if>> 保密
                                </label>
                            </td>

                            <td class="text-right">生日：</td>
                            <td class="text-left">
                                <input type="text" class="form-control fui-date" name="birthday" format="YYYY-MM-DD" value="<fmt:formatDate value="${userProfileInfo.birthday}" pattern="yyyy-MM-dd"/>" >
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right">手机号码：</td>
                            <td class="text-left">
                                <input type="text" class="form-control" name="phone" value="${userInfo.phone}">
                            </td>

                            <td class="text-right">马甲：</td>
                            <td class="text-left">

                                <label class="radio-inline">
                                    <input type="radio" name="fakeFlag" value="0" <c:if test="${userInfo.fakeFlag == 0 }">checked</c:if>> 否
                                </label>

                                <label class="radio-inline">
                                    <input type="radio" name="fakeFlag" value="1" <c:if test="${userInfo == null || userInfo.fakeFlag == 1}">checked</c:if>> 是
                                </label>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right">个性签名：</td>
                            <td class="text-left" colspan="3">
                                <textarea class="form-control" style="height: 100px;" name="signature">${userProfileInfo.signature}</textarea>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    requirejs(['jquery','Fui'],function(){
        var $form = $("#<%=formId%>");
        $form.data("paramFn",function(){
            var headIcon = $form.find("input[name='headIcon']").val();
            if(!headIcon){
                layer.msg("请选择头像");
                return false;
            }
        });

        var $picContainer = $("#<%=picContainerId%>");
        var showHeadIcon = function(icon){
            BaseUtil.addSingleImage($picContainer,{src:icon},{size:"50",height:50,width:50});
            setTimeout(function(){
                $picContainer.append("<div style='height:50px;line-height:50px;float: left'><div>")
            },200);
        }

        $("#<%=selectPicBtnId%>").click(function(){
             var picSelector = new Picture(function(data){
                 $form.find("input[name='headIcon']").val(data.src);
                 showHeadIcon(data.src);
                 picSelector.close();
             },false);
            picSelector.select();
        });

       <c:if test="${userProfileInfo != null}">
        showHeadIcon("${userProfileInfo.headIcon.src}");
       </c:if>
    });
</script>