<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" class="btn btn-default open-dialog"  dialog-title="新增存档游戏" href="${base}/save/game/goAddPage" >新增</button>
	</div>
</div>

<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">

			<div class="input-group">
				<input type="number" class="form-control" name="gameId" placeholder="游戏ID">
			</div>

			<div class="input-group">
				<input type="text" class="form-control" name="name" placeholder="游戏名">
			</div>

			<div class="input-group">
				<input type="text" class="form-control" name="packageName" placeholder="包名">
			</div>

			<button class="btn btn-primary fui-date-search">查询</button>
		</div>
	</div>
</form>


<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
        Fui.addBootstrapTable({
            url : '${base}/save/game/list',
            columns : [{
                title : '游戏ID',
                align : 'center',
                width : '35',
                valign : 'middle',
                field : 'gameId'
			},{
                title : '游戏名',
                align : 'left',
                width : '100',
                valign : 'middle',
                field : 'name'
			},{
                title : '包名',
                align : 'center',
                width : '100',
                valign : 'middle',
                field : 'packageName',
                formatter : function (v,row) {
                    return "<div style='word-break: break-all;'>" + v + "</div>";
                }
            },{
                field : 'updateTime',
                title : '更新时间',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : Fui.formatDatetime
            },{
                title : '操作',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : function(v,row){
                   var arr = [
                       '<a class="open-dialog" dialog-title="修改存档游戏" href="${base}/save/game/goEditPage?gameId=',
                        row.gameId,'" title="修改存档游戏">',
                        '<i class="glyphicon glyphicon-edit"></i>',
					   '</a> ',

                       '<a class="todo-ajax" href="${base}/save/game/delete?gameId=',row.gameId, '" confirm="是否确认要删除?" title="删除">',
                       '<i class="glyphicon glyphicon-trash"></i>', '</a> '
				   ];
                   return arr.join("");
				}
            }]
        });
    });
</script>

