<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String formId = java.util.UUID.randomUUID().toString();
    String selectGameBtnId = java.util.UUID.randomUUID().toString();
%>
<form id="<%=formId%>"  action="${base}/save/game/saveOrUpdate" class="form-submit">

    <input type="hidden" value="${action}" name="action"/>

    <div class="modal-dialog" style="width: 600px;">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    编辑存档信息
                </h4>
            </div>

            <div class="modal-body">
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>

                        <tr>
                            <td width="15%" class="text-right"><b style="color:red;">*</b>游戏：</td>
                            <td width="85%" class="text-left">
                                <input type="hidden" name="gameId" value="${saveGame.gameId}">
                                <c:if test="${action == 'add'}">
                                    <div class="input-group">
                                        <input type="text" name="gameName" value="" class="form-control" readonly >
                                        <span class="input-group-btn">
                                             <button id="<%=selectGameBtnId%>" class="btn btn-primary" type="button">选择</button>
                                         </span>
                                    </div>
                                </c:if>

                                <c:if test="${action != 'add'}">
                                    <input type="text" class="form-control" value="${gameInfo.name}" readonly name="gameName"/>
                                </c:if>

                            </td>
                        </tr>


                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>配置：</td>

                            <td class="text-left">
                                <textarea rows="10" class="form-control" name="config" required>${saveGame.config}</textarea>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    requirejs(['jquery','Fui'],function(){
        var $form = $("#<%=formId%>");
        $form.data("paramFn",function(){
            if(!$form.find("input[name='gameId']").val()){
                layer.msg("请选择游戏");
                return false;
            }
        });

        $("#<%=selectGameBtnId%>").click(function(){
            var gameSelector = new Game(function(data){
                $form.find("input[name='gameId']").val(data.id);
                $form.find("input[name='gameName']").val(data.name);
                gameSelector.close();
            });
            gameSelector.select();
        });
    });
</script>