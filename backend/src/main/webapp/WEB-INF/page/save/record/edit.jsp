<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String formId = java.util.UUID.randomUUID().toString();
    String selectGameBtnId = java.util.UUID.randomUUID().toString();
    String selectFileBtnId = java.util.UUID.randomUUID().toString();
%>
<form id="<%=formId%>"  action="${base}/save/record/saveOrUpdate" class="form-submit">

    <input type="hidden" value="${saveRecord.fileSize}" name="fileSize"/>
    <input type="hidden" value="${saveRecord.id}" name="id"/>
    <div class="modal-dialog" style="width: 600px;">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close fui-close"  aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    编辑存档信息
                </h4>
            </div>

            <div class="modal-body">
                <table class="table table-bordered table-striped" style="clear: both">
                    <tbody>
                        <tr>
                            <td class="text-right" width="25%"><b style="color:red;">*</b>存档名称：</td>
                            <td class="text-left">
                                <input type="text" class="form-control" value="${saveRecord.name}" required name="name"/>
                            </td>
                        </tr>


                        <tr>
                            <td width="15%" class="text-right"><b style="color:red;">*</b>游戏：</td>
                            <td width="85%" class="text-left">
                                <input type="hidden" name="gameId" value="${saveRecord.gameId}">
                                <c:if test="${saveRecord == null}">
                                    <div class="input-group">
                                        <input type="text" name="gameName" value="" class="form-control" readonly >
                                        <span class="input-group-btn">
                                             <button id="<%=selectGameBtnId%>" class="btn btn-primary" type="button">选择</button>
                                         </span>
                                    </div>
                                </c:if>

                                <c:if test="${saveRecord != null}">
                                    <input type="text" class="form-control" value="${gameInfo.name}" readonly name="gameName"/>
                                </c:if>

                            </td>
                        </tr>


                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>版本名：</td>
                            <td class="text-left">
                                <input type="text" class="form-control" value="${saveRecord.versionName}" required name="versionName"/>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><b style="color:red;">*</b>版本号：</td>

                            <td class="text-left">
                                <input type="number" class="form-control" value="${saveRecord.versionCode}" required name="versionCode"/>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right">
                                <strong style="color:red;">*</strong>存档文件
                            </td>
                            <td>
                                <div class="input-group" STYLE="width: 100%">
                                    <input type="text" required name="url" value="${saveRecord.url.src}" readonly class="form-control">
                                    <span class="input-group-btn">
                                        <button id="<%=selectFileBtnId%>" class="btn btn-primary" type="button">选择</button>
                                    </span>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right">适配信息</td>

                            <td class="text-left">
                                <input type="text" class="form-control" value="${saveRecord.adaptation}" required name="adaptation"/>
                            </td>

                        </tr>
                        <tr>
                            <td class="text-right">备注信息</td>
                            <td class="text-left">
                                <textarea class="form-control" rows="5" name="remark">${saveRecord.remark}</textarea>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">保存</button>
                <button type="button" class="btn btn-default fui-close">关闭</button>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    requirejs(['jquery','Fui'],function(){
        var $form = $("#<%=formId%>");
        $form.data("paramFn",function(){
            if(!$form.find("input[name='gameId']").val()){
                layer.msg("请选择游戏");
                return false;
            }
        });

        $("#<%=selectGameBtnId%>").click(function(){
            var gameSelector = new Game(function(data){
                $form.find("input[name='gameId']").val(data.id);
                $form.find("input[name='gameName']").val(data.name);
                $form.find("input[name='versionName']").val(data.versionName);
                $form.find("input[name='versionCode']").val(data.versionCode);
                gameSelector.close();
            });
            gameSelector.select();
        });

        $("#<%=selectFileBtnId%>").click(function(){
            var apk = new AnyFile(function(data){
                $form.find("input[name='url']").val(data.src);
                $form.find("input[name='fileSize']").val(data.size);
                apk.close();
            });
            apk.select();
        });
    });
</script>