<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group">
	<div class="form-inline">
		<button type="button" class="btn btn-default open-dialog"  dialog-title="新增推荐存档" href="${base}/save/record/goAddPage" >新增</button>
	</div>
</div>

<form class="fui-search table-tool" method="post">
	<div class="form-group fui-data-wrap">
		<div class="form-inline">

			<div class="input-group">
				<select class="form-control" name="open" >
					<option value="">所有存档</option>
					<option value="1">推荐存档</option>
					<option value="0">用户存档</option>
				</select>
			</div>

			<div class="input-group" style="width: 250px;">
				<input name="gameId" type="text" data-max-items="1"  placeholder="游戏" data-url="${base}/game/info/getForSelectize" class="fui-select">
			</div>

			<div class="input-group">
				<input type="text" class="form-control" name="name" placeholder="存档名称">
			</div>

			<button class="btn btn-primary fui-date-search">查询</button>
		</div>
	</div>
</form>


<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
        Fui.addBootstrapTable({
            url : '${base}/save/record/list',
            columns : [
//                {
//                title : 'ID',
//                align : 'center',
//                width : '60',
//                valign : 'middle',
//                field : 'id'
//			},
				{
                title : '存档名',
                align : 'center',
                width : '80',
                valign : 'middle',
                field : 'name'
            },{
                title : '游戏ID',
                align : 'left',
                width : '60',
                valign : 'middle',
                field : 'gameId'
            },{
                title : '游戏名称',
                align : 'left',
                width : '100',
                valign : 'middle',
                field : 'gameName'
			},{
                title : '用户ID',
                align : 'left',
                width : '60',
                valign : 'middle',
                field : 'userId'
			},{
                field : 'open',
                title : '类型',
                align : 'center',
                width : '50',
                valign : 'middle',
                formatter : function(v,row){
					if(v == 1){
                        return "<font color='blue'>推荐存档</font>";
					}
					if(v == 0){
					    return "用户存档";
					}
                }
			},{
                field : 'fileSize',
                title : '文件大小',
                align : 'center',
                width : '60',
                valign : 'middle',
                formatter : Fui.formatFileSize
			},{
                field : 'status',
                title : '状态',
                align : 'center',
                width : '60',
                valign : 'middle',
                formatter : function (v,row) {
					return Fui.statusFormatter({
						val : v,
						url:'${base}/save/record/updateStatus?id='+row.id+'&status='
					});
				}
            },{
                field : 'updateTime',
                title : '更新时间',
                align : 'center',
                width : '80',
                valign : 'middle',
                formatter : Fui.formatDatetime
			},{
                title : '操作',
                    align : 'center',
                    width : '80',
                    valign : 'middle',
                    formatter : function(v,row){
                        var arr = [];
                        if(row.open == 1){
                            arr = [
                                '<a class="open-dialog" dialog-title="修改" href="${base}/save/record/goEditPage?id=',
                                row.id,'" title="修改">',
                                '<i class="glyphicon glyphicon-edit"></i>',
                                '</a> ',
                                '<a class="todo-ajax" href="${base}/save/record/delete?id=',row.id, '" confirm="是否确认要删除?" title="删除">',
                                '<i class="glyphicon glyphicon-trash"></i>', '</a> '

                            ];
                        }
                        arr.push('<a target="_Blank" href="',baseInfo.cdn+row.url,'" title="下载">',
                            '<i class="glyphicon glyphicon-download-alt"></i>',
                            '</a> ');
                        return arr.join("");
                    }
            }]
        });
    });
</script>

