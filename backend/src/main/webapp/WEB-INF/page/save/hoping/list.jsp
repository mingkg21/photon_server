<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<table class="fui-default-table"></table>
<script type="text/javascript">
    requirejs(['jquery','bootstrap','Fui'],function(){
        Fui.addBootstrapTable({
            url : '${base}/save/hoping/list',
            columns : [{
                title : '游戏ID',
                align : 'center',
                width : '50',
                valign : 'middle',
                field : 'gameId'
			},{
                title : '游戏名',
                align : 'left',
                width : '80',
                valign : 'middle',
                field : 'name'
			},{
                title : '包名',
                align : 'center',
                width : '100',
                valign : 'middle',
                field : 'packageName'
            },{
                title : '许愿人数',
                align : 'center',
                width : '100',
                valign : 'middle',
                field : 'personCount',
                sortable : true
			},{
                title : '存档游戏',
                align : 'center',
                width : '100',
                valign : 'middle',
                field : 'hasSave',
                formatter : function(v){
                    if(v == 0){
                        return "<font color='red'>否</font>";
					}
					if(v == 1){
                        return "<font color='green'>是</font>";
					}
					return "未知";
				}
			},{
                field : 'lastHoppingTime',
                title : '最后许愿时间',
                align : 'center',
                width : '120',
                valign : 'middle',
                formatter : Fui.formatDatetime,
                sortable : true
            }]
        });
    });
</script>

