BaseUtil = {
    isFunction:function (o) {
        return  Object.prototype.toString.call(o) == "[object Function]";
    },
    isObj:function(o){
        return  Object.prototype.toString.call(o) == "[object Object]";
    },
    isString:function(o){
        return  Object.prototype.toString.call(o) == "[object String]";
    },
    isJQ:function (o) {
       return o instanceof jQuery;
    },
    isArray:function(o){
        return  Object.prototype.toString.call(o) === '[object Array]';
    },
    isDocument:function(obj){
        if(typeof HTMLElement === 'object' ){
            return obj instanceof HTMLElement;
        }
       return obj && typeof obj === 'object' && obj.nodeType === 1 && typeof obj.nodeName === 'string';
    },
    getJQObject :function(c){
        if(this.isJQ(c)){
            return c;
        }
        if(this.isString(c)){
            return $("#"+c);
        }
       if(this.isDocument(c)){
            return $(c);
       }
     },
    addVideo:function(c,src,removeCallback){
        var $el = this.getJQObject(c);
        var me = this;
        requirejs(['template'],function(template){
            var flag = me.isFunction(removeCallback);
           var html = template('video_container_tpl', {src:src,hasFun:flag});
            $el.html(html);
            if(flag){
                $el.find(".glyphicon-trash").click(removeCallback);
            }
        });
    },
    //单选
    addSingleImage:function(el,data,cfg){
        var $el = this.getJQObject(el);
        var base = {
            width:100,
            height:100,
            size:"100",
            cdn:baseInfo.cdn,
            removeFn:null
        };
        if(this.isObj(cfg)){
            base = jQuery.extend(base, cfg);
        }
        data.cdnUrl = Fui.getCdnUrl(data.src,base.size);
        base.single = true;
        base.items = [data];
        requirejs(['template'],function(template){
            var html = template('image_container_tpl',base);
            $el.html(html);
            if(base.removeFn){
                $el.find(".glyphicon-trash").click(base.removeFn);
            }
        });
    },
    //多选
    addManyImage:function(el,dataArr,cfg){
        var $el = this.getJQObject(el);
        var base = {
            width:100,
            height:100,
            cdn:baseInfo.cdn,
            size:"100"
        };
        if(this.isObj(cfg)){
            base = jQuery.extend(base, cfg);
        }
        base.single = false;
        base.items = dataArr;
        for(var i=0;i<dataArr.length;i++){
              var item = dataArr[i];
              item.json = JSON.stringify(item);
              item.dataId = Fui.uuid();
              item.cdnUrl = Fui.getCdnUrl(item.src,base.size);
        }
        requirejs(['template'],function(template){
            var html = template('image_container_tpl',base);
            $el.append(html);
        });
    },
    getImageValues : function(el){
        var $el = this.getJQObject(el);
        var arr = [];
        $el.find("input[name='image_data']").each(function () {
            arr.push($(this).data("value"));
        });
        return arr;
    },
    removeImage:function(id){
        $("#"+id).remove();
    }
}