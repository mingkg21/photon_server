KindEditor.plugin('video', function(K) {
    var self = this, name = 'video', lang = self.lang(name + '.');

    self.plugin.video = {
        edit : function() {
            var html = [
                '<div style="padding:20px;">',
                //url
                '<div class="ke-dialog-row">',
                '<label for="keUrl" style="width:60px;">视频地址</label>',
                '<input class="ke-input-text" type="text"  style="width:300px;" id="keUrl" name="mp4Url" value="" style="width:160px;" />  ',
                '</span>',
                '</div>',
                //缩略图地址
                '<div class="ke-dialog-row">',
                '<label for="keHeight" style="width:60px;">图片地址</label>',
                '<input type="text" style="width:300px;" class="ke-input-text ke-input-number" name="imgUrl" value=""  />',
                '</div>',
                '</div>'
            ].join('');
            var dialog = self.createDialog({
                name : name,
                width : 450,
                height : 160,
                title : self.lang(name),
                body : html,
                yesBtn : {
                    name : self.lang('yes'),
                    click : function(e) {
                        var url = K.trim(urlField.val()),
                            imgUrl = K.trim(imgUrlField.val());

                        if(!url){
                            alert("请输入视频地址");
                            urlField[0].focus();
                            return;
                        }

                        if(!imgUrl){
                            alert("请输入图片地址");
                            imgUrlField[0].focus();
                            return;
                        }

                        if (K.invalidUrl(url)) {
                            alert("视频地址格式不对");
                            urlField[0].focus();
                            return;
                        }

                        if (K.invalidUrl(imgUrl)) {
                            alert("图片地址格式不对");
                            imgUrlField[0].focus();
                            return;
                        }

                        //
                        // var html = K.mediaImg(self.themesPath + 'common/blank.gif', {
                        //     src : url,
                        //     type : width,
                        //     poster : height
                        // });

                        var html = [
                            "&nbsp;<video controls=\"controls\" src='"+url+"' poster='"+imgUrl+"' height='250'></video>&nbsp;"
                        ];
                        self.insertHtml(html.join("")).hideDialog().focus();
                    }
                }
            }),
            div = dialog.div,
            urlField = K('[name="mp4Url"]', div),
            imgUrlField = K('[name="imgUrl"]', div);
        },
        'delete' : function() {
            console.info(self.plugin.getSelectedMedia());
            self.plugin.getSelectedMedia().remove();
            self.addBookmark();
        }
    };
    self.clickToolbar(name, self.plugin.video.edit);
});