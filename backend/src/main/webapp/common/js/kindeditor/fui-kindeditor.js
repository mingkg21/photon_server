define(['kindeditor'],function(){
    KindEditor.lang({
        video : '插入视频'
    });

    var initOneEdit = function(){
        $it = $(this);
        var baseConfig = {
            resizeType : 1,
            allowPreviewEmoticons : false,
            allowImageUpload : false,
            afterCreate : function() {
                this.sync();
            },
            afterBlur:function(){
                this.sync();
            },
            items : ['source',
                'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
                'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
                'insertunorderedlist', '|', 'link' ,'video','image']
        };

        var apply = {
            width : $it.data("width"),
            height : $it.data("height")
        }
        baseConfig =  $.extend(baseConfig,apply);
        var editor = KindEditor.create(this,baseConfig);

    }

    return {
        initKindEditor:function(eles,$p){
            eles.each(initOneEdit);
        }
    }
});