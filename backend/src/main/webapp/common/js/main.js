define(['jquery','bootstrap','Fui'], function() {
	var _index = 1000;
	window.createId = function(){
        _index++;
        return "auto_id_"+_index;
	};

	window.jQuery=$;
	// 备份jquery的ajax方法
	var _ajax = $.ajax;
	window.$ajax = _ajax;
	// 重写jquery的ajax方法
	$.ajax = function(opt) {
		var url=opt.url;
		// if(url.indexOf("?")>0){
		// 	url=url+"&page=new";
		// }else{
		// 	url = url+"?page=new";
		// }
		opt.url=url;
		if (!opt.dataType) {
			opt.dataType = "json";
		}
		if(opt.cache==undefined){//默认不缓存
			opt.cache=false;
		}
		if (!opt.type) {
			opt.type = "post";
		}
		// 备份opt中error和success方法
		var fn = {
			error : function(XMLHttpRequest, textStatus, errorThrown) {
			},
			success : function(data, textStatus, xhr) {
			}
		}
		if (opt.error) {
			fn.error = opt.error;
		}
		if (opt.success) {
			fn.success = opt.success;
		}

		// 扩展增强处理
		var _opt = $.extend(opt, {
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				var statusCode = XMLHttpRequest.status;
				// 错误方法增强处理
				if (statusCode == 404) {
					layer.msg("[" + opt.url + "] 404 not found");
				} 
				fn.error(XMLHttpRequest, textStatus, errorThrown);
				layer.closeAll('loading');
			},
			success : function(data, textStatus, xhr) {
				var ceipstate = xhr.getResponseHeader("ceipstate");
				layer.closeAll('loading');
				if(ceipstate==1){
					fn.success(data, textStatus, xhr);
				}else if (ceipstate ==2||ceipstate==3) {// 后台异常
					if($.type(data)=='string'){
						data=$.parseJSON(data);
					}
					layer.msg(data.msg||"后台异常，请联系管理员!");
					if(opt.errorFn){
						opt.errorFn(data,ceipstate);
					}
				} else if (ceipstate == 4) {// 未登陆异常
					if(Fui.config.loginDialogUrl){
						Fui.openDialog({id:'login-dialog-url',url:Fui.config.loginDialogUrl});
						if(opt.errorFn){
							if($.type(data)=='string'){
								data=$.parseJSON(data);
							}
							opt.errorFn(data,ceipstate);
						}
					}else{
						window.location.href = Fui.config.indexUrl ? Fui.config.indexUrl : "/";
					}
				} else if (ceipstate == 5) {// 没有权限
					if($.type(data)=='string'){
						data=$.parseJSON(data);
					}
					layer.msg(data.msg||"没有权限");
					if(opt.errorFn){
						opt.errorFn(data,ceipstate);
					}
				} else if (ceipstate == 6) {// 没有权限
					if($.type(data)=='string'){
						data=$.parseJSON(data);
					}
					layer.alert(data.msg||"登录状态错误，可能是ip变动",function(){
						if(Fui.config.loginDialogUrl){
							layer.closeAll('loading');
							Fui.openDialog({id:'login-dialog-url',url:Fui.config.loginDialogUrl});
							if(opt.errorFn){
								opt.errorFn(data,ceipstate);
							}
						}else{
							window.location.href = Fui.config.indexUrl ? Fui.config.indexUrl : "/";
						}
					});
				} else {
					fn.success(data, textStatus, xhr);
				}
			}
		});
		_ajax(_opt);
	};
	/**
	 * 扩展String方法
	 */
	$.extend(String.prototype, {
		isPositiveInteger:function(){
			return (new RegExp(/^[1-9]\d*$/).test(this));
		},
		isInteger:function(){
			return (new RegExp(/^\d+$/).test(this));
		},
		isNumber: function(value, element) {
			return (new RegExp(/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/).test(this));
		},
		trim:function(){
			return this.replace(/(^\s*)|(\s*$)|\r|\n/g, "");
		},
		startsWith:function (pattern){
			return this.indexOf(pattern) === 0;
		},
		endsWith:function(pattern) {
			var d = this.length - pattern.length;
			return d >= 0 && this.lastIndexOf(pattern) === d;
		},
		replaceAll:function(os, ns){
			return this.replace(new RegExp(os,"gm"),ns);
		},
		replaceTmBySelector:function(_box){
			var $parent = _box || $(document);
			return this.replace(RegExp("{([^:}]+)(?::([^}]*))?}","g"), //支持{#id:默认值}和{.className:默认值},（:默认值）这部分可以没有
			function($0,$1,$2){
				var $input = $parent.find($1);
				return $input.val() ? $input.val() : ((typeof($2)!="undefined")?$2:$0);
			});
		},
		isFinishedTm:function(){
			return !(new RegExp("{[A-Za-z_]+[A-Za-z0-9_]*}").test(this)); 
		},
		isSpaces:function() {
			for(var i=0; i<this.length; i+=1) {
				var ch = this.charAt(i);
				if (ch!=' '&& ch!="\n" && ch!="\t" && ch!="\r") {return false;}
			}
			return true;
		},
		isUrl:function(){
			return (new RegExp(/^[a-zA-z]+:\/\/([a-zA-Z0-9\-\.]+)([-\w .\/?%&=:]*)$/).test(this));
		},
		isExternalUrl:function(){
			return this.isUrl() && this.indexOf("://"+document.domain) == -1;
		}
	});

	$.extend(Date.prototype, {
		// 对Date的扩展，将 Date 转化为指定格式的String
		// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
		// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
		// 例子：
		// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
		// (new Date()).Format("yyyy-M-d h:m:s.S") ==> 2006-7-2 8:9:4.18
		format : function(fmt) {
			var o = {
				"M+" : this.getMonth() + 1, // 月份
				"d+" : this.getDate(), // 日
				"h+" : this.getHours(), // 小时
				"m+" : this.getMinutes(), // 分
				"s+" : this.getSeconds(), // 秒
				"q+" : Math.floor((this.getMonth() + 3) / 3), // 季度
				"S" : this.getMilliseconds()
			// 毫秒
			};
			if (/(y+)/.test(fmt))
				fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
						.substr(4 - RegExp.$1.length));
			for ( var k in o)
				if (new RegExp("(" + k + ")").test(fmt))
					fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
							: (("00" + o[k]).substr(("" + o[k]).length)));
			return fmt;
		}
	});
	
	Fui.init({
		baseUrl:baseInfo.baseUrl,
		loginDialogUrl:baseInfo.loginDialogUrl,
		indexUrl:baseInfo.indexUrl,
		navUrl:baseInfo.navUrl
	});
	Fui.navbar.set({elem:"#fui_sidebar_nars",url:Fui.config.navUrl}).render();
	Fui.initUI($(".navbar-fui"));
	Fui.tab.set({elem:"#fui_tab_wrap_id"}).init().tabAdd({id:"home",title:"首页",closed:false,content:'欢迎光临',icon:"fa-home"});
	$('.admin-side-toggle').on('click', function() {
		var sideWidth = $('#fui_sidebar').width();
		if(sideWidth === 200) {
			$('#fui_content_id').animate({
				left: '0'
			});
			$('#fui_footer_id').animate({
				left: '0'
			});
			$('#fui_sidebar').animate({
				width: '0'
			});
		} else {
			$('#fui_content_id').animate({
				left: '200px'
			});
			$('#fui_footer_id').animate({
				left: '200px'
			});
			$('#fui_sidebar').animate({
				width: '200px'
			});
		}
	});
	
	$(window).on('resize', function() {
		var $content = $('#fui_content_id #fui_tab_content');
		var h=$(this).height() - 70;
		h=h-$("#fui_footer_id").outerHeight();
		h= h-$("#fui_content_id #fui_tab_header").outerHeight();
		$content.height(h);
	}).resize();
	//直接使用浏览器打开模版的
	var $direct=$(".direct_open_url");
	if($direct.length){
		var url=$direct.attr("url")
			,con = $direct.html()
			,item=Fui.navbar.getMenuItem(url);
		if(con){
			con=con.substring(4,con.length-5);
		}
		$direct.remove();
		if(item){
			Fui.tab.tabAdd({id:item.id,title:item.title,closed:item.closed,url:url,content:con,icon:item.icon});
			var a=$("#fui_sidebar").find("a[href='"+url+"']");
			a.addClass("cur-itemed");
			var ps=a.parents(".nav-item");
			ps.siblings().removeClass("active");
			ps.addClass("active");
		}
	}
});
