define(['jquery','bootstrap','layer'], function(){
    try {
        localStorage.setItem('isPrivateMode', '1');
        localStorage.removeItem('isPrivateMode');
        window.isPrivateMode = false;// 不是 Safari 无痕模式
    } catch(e) {
        window.isPrivateMode = true;// 是 Safari 无痕模式
    }
    function pageAjaxDone(json){
        if(json.msg) layer.msg(json.msg);
        if (!json.unReloadTable){
            var $table=$(".fui-box.active").data("bootstrapTable");
            if($table && $table.length){
                $table.bootstrapTable('refresh');
            }
        }
    }
    function ajaxTodo(url, callback,options){
        callback=callback||pageAjaxDone;
        if (! $.isFunction(callback)) callback = eval('(' + callback + ')');
        $.ajax({
            type:'POST',
            url:url,
            dataType:"json",
            cache: false,
            success: function(json){
                json=$.extend(json, options);
                callback(json);
            },
            error:function(){
                layer.msg('网络异常');
            },
            complete:function(){
                layer.closeAll('loading');
            }
        });
    }

    function S4() {
        return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
    }

    var Fui={
        config:{
            baseUrl:"",
            indexUrl:"",
            loginDialogUrl:"", //session timeout
            zindex : 1000
        },
        cacheMap:{},
        init:function(options){
            this.config = $.extend(this.config, options);
        },
        closeActiveTab: function(){
            var tabId = $(".fui-box.active").attr("id");
            var arr = tabId.split("_");
            Fui.tab.close(arr[1]);
        },
        getParentNode:function(){
            var ss =document.getElementsByTagName('script');
            var lastScript = ss[ss.length - 1];
            return $(lastScript).parent();
        },
        uuid:function() {
            return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
        },
        formatFileSize:function(fileByte){
            if(!fileByte){
                return "-";
            }
            var fileSizeByte = fileByte;
            var fileSizeMsg = "";
            if (fileSizeByte < 1048576) fileSizeMsg = (fileSizeByte / 1024).toFixed(2) + "KB";
            else if (fileSizeByte == 1048576) fileSizeMsg = "1MB";
            else if (fileSizeByte > 1048576 && fileSizeByte < 1073741824) fileSizeMsg = (fileSizeByte / (1024 * 1024)).toFixed(2) + "MB";
            else if (fileSizeByte > 1048576 && fileSizeByte == 1073741824) fileSizeMsg = "1GB";
            else if (fileSizeByte > 1073741824 && fileSizeByte < 1099511627776) fileSizeMsg = (fileSizeByte / (1024 * 1024 * 1024)).toFixed(2) + "GB";
            else fileSizeMsg = "超过1TB";
            return fileSizeMsg;
        },
        /**
         * 防止xss攻击
         * @param val
         * @returns {string|*}
         */
        formatXss : function (val) {
            if(!val){
                return val;
            }
            val = val.toString();
            val = val.replace(/[<%3C]/g, "&lt;");
            val = val.replace(/[>%3E]/g, "&gt;");
            val = val.replace(/"/g, "&quot;");
            val = val.replace(/'/g, "&#39;");
            return val;
        },
        copyText:function(text){
            var $copyField = $("#copy_text_field");
            $copyField.val(text)
            $copyField[0].focus();
            $copyField[0].select();
            document.execCommand('copy');
            layer.msg("复制成功");
        },
        copyCdnUrl:function(ele,inputName){
            var $form = $(ele).parents("form");
            var $input = $form.find("input[name='"+inputName+"']");
            if($input.length == 0){
                return;
            }
            var $copyField = $("#copy_text_field");
            var val = $input.val();
            if(!val){
                return;
            }
            if(val.indexOf("http") != 0){
                val = baseInfo.cdn + val;
            }
            $copyField.val(val)
            $copyField[0].focus();
            $copyField[0].select();
            document.execCommand('copy');
            layer.msg("复制成功");
        },
        getCdnUrl : function(url,size){
            if(!url){
                return "-";
            }
            var cndUrl = url;
            var smalCdnUrl = url;
            if(url.indexOf("http://") != 0 && url.indexOf("https://") != 0){
                cndUrl =  baseInfo.cdn + url;
                smalCdnUrl = cndUrl;
                if(size){
                    smalCdnUrl =  smalCdnUrl + "-"+size;
                }
            }
            return smalCdnUrl;
        },
        formatImage:function(url,size){
            var smalCdnUrl = Fui.getCdnUrl(url,size);
            var cdnUrl = Fui.getCdnUrl(url);
            var width = size ? "width ='"+size+"'" : "";
            return "<a target='_blank' title='点击查看原图' href='"+cdnUrl+"'><image "+width+"  src='"+smalCdnUrl+"'/></a>"
        },
        formatDatetime:function(time){
            if (!time) {
                return "";
            }
            time = new Date(time);
            return time.format("yyyy-MM-dd hh:mm:ss");
        },
        formatDate:function(date){
            if (!date) {
                return "";
            }
            date = new Date(date);
            return date.format("yyyy-MM-dd");
        },
        formatText:function(text,size){
            if(!text){
                return text;
            }
            var length = isNaN(size) ? 50 : size;
            if(text.length > length){
               return text.substring(0,50)+"...";
            }
            return text;
        },
        statusFormatter:function(option) {
            var onVal=option.onVal||2
                ,offVal=option.offVal||1
                ,onText=option.onText||'启用'
                ,offText=option.offText||'禁用'
                ,val=option.val
                ,url=option.url;
            var col='<input class="fui-switch" type="checkbox" data-on-text="'+onText+'" data-off-text="'+offText+'"';
            if(val==onVal){
                col+=' checked';
            }
            col+=' data-url="'+url+'" data-on-value="'+onVal+'" data-off-value="'+offVal+'">';
            return col;
        },
        //制保留n位小数，如：2，会在2后面补上00.即2.00
        toDecimal:function(num,n) {
            if(n<=0){
                return num;
            }
            if(isNaN(num)){
                return false;
            }
            var f = parseFloat(num);
            if (isNaN(f)) {
                return false;
            }
            f = Math.pow(10, n);
            f = Math.round(num * f) / f;
            f = f.toString();
            var rs = f.indexOf('.');
            if (rs < 0) {
                rs = f.length;
                f += '.';
            } else if (f.length - rs > n + 1) {
                return f.substring(0, rs + n + 1)
            }
            while (f.length <= rs + 2) {
                f += '0';
            }
            return f;
        },
        /**
         * 缓存
         * @param table
         * @param settings
         * @returns
         */
        setCache : function(table,settings){
            table = table || 'fui';
            if(!window.JSON || !window.JSON.parse) return;
            if (window.isPrivateMode || !window.localStorage)return;
            // 不是 Safari 无痕模式并且能用 localStorage
            //如果settings为null，则删除表
            if(settings === null){
                return delete localStorage[table];
            }
            settings = typeof settings === 'object' ? settings  : {key: settings};
            var data =null;
            try{
                data = JSON.parse(localStorage[table]);
            } catch(e){
                data = {};
            }
            if(settings.value) data[settings.key] = settings.value;
            if(settings.remove) delete data[settings.key];
            localStorage[table] = JSON.stringify(data);
            return settings.key ? data[settings.key] : data;
        },
        /**
         * 初始化UI元素
         */
        initUI:function(_box){
            var $p = $(_box || document);
            $(".open-dialog",$p).click(function(event){
                var $this = $(this)
                    ,url = unescape($this.attr("href")||$this.attr("url")).replaceTmBySelector($(event.target).parents(".fui-box:first"))
                    ,cache=$this.hasClass("cached")
                    ,title=$this.attr("dialog-title");
                if (!url.isFinishedTm()) {
                    layer.msg($this.attr("warn")||"请选择一条数据");
                    return false;
                }
                var option = {url:url,cache:cache}
                    ,w = $this.attr("width")
                    ,h = $this.attr("height");
                option.area =[(w?w+"px":""),(h?h+"px":"")];
                if(title){
                    option.title=title;
                }
                Fui.openDialog(option);
                return false;
            });
            $(".open-text",$p).click(function(event){
                var $this = $(this)
                    ,text=$this.attr("dialog-text")
                    ,title=$this.attr("dialog-title");
                var w = $this.attr("width"),h = $this.attr("height");
                var area =[(w?w+"px":"600px"),(h?h+"px":"300px")];
                layer.open({
                    type: 1,
                    title : title,
                    skin: 'layui-layer-rim', //加上边框
                    area: area, //宽高
                    content: text
                });
                return false;
            });
            $(".todo-ajax",$p).click(function(event){
                var layerLoadIndex=layer.load(2);
                var $this = $(this);
                var url=$this.attr("href")||$this.attr("url");
                url = unescape(url).replaceTmBySelector($(event.target).parents(".fui-box:first"));
                if (!url.isFinishedTm()) {
                    layer.msg($this.attr("warn")||"请选择一条数据");
                    layer.close(layerLoadIndex);
                    return false;
                }
                var title = $this.attr("confirm");
                if (title) {
                    var layerIndex=layer.open({
                            content:title,
                            btn: ['确认', '取消'],
                            yes: function(index, layero){ //按钮1
                                ajaxTodo(url, $this.attr("callback"),{unReloadTable:$this.attr("unReloadTable")});
                                layer.close(layerIndex);
                            },
                            btn2: function(index, layero){
                                layer.close(layerLoadIndex);
                            },//按钮2
                            cancel: function(){ //右上角关闭
                                layer.close(layerLoadIndex);
                            }
                        }
                    );
                } else {
                    ajaxTodo(url, $this.attr("callback"),{unReloadTable:$this.attr("unReloadTable")});
                }
                event.preventDefault();
            });
            $(".open-tab",$p).click(function(event){
                var it = $(this)
                    ,url=it.attr("href")||it.attr("url")
                    ,option
                    ,item=null;
                url = unescape(url).replaceTmBySelector($(event.target).parents(".fui-box:first"));
                item=Fui.navbar.getMenuItem(url)
                if(item){
                    option = {
                        id:it.attr("id")||item.id
                        ,title:it.text()||item.title
                        ,url:url
                        ,icon: item.icon
                        ,closed:item.closed
                        ,refresh:it.data("refresh")
                    };
                }else{
                	var tabId = it.attr("tab-id");
                    option = {
                        id:it.attr("tab-id")
                        ,title:it.attr("dialog-title")||it.text()
                        ,url:url
                        ,icon:it.data("icon")
                        ,closed:it.data("closed")
                        ,refresh:it.data("refresh")
                    };
                }
                Fui.tab.tabAdd(option);
                event.preventDefault();
            });
            var formSubmit=$(".form-submit",$p);
            if(formSubmit.length){
                requirejs(['fui_form'],function(FuiForm){
                    FuiForm.initFormValidator(formSubmit,$p);
                })
            }
            var dateEle=$(".fui-date",$p);
            if(dateEle.length){
                requirejs(['fui_datetime'],function(FuiDateTime){
                    FuiDateTime.initDatetime(dateEle,$p);
                })
            }
            var $fuiSearchForm=$("form.fui-search",$p);
            $fuiSearchForm.submit(function(){
                var $table=$fuiSearchForm.parents(".fui-box:first").data("bootstrapTable");
                if(!$table){
                    layer.msg("表格不存在");
                    return false;
                }
                $table.bootstrapTable('refreshOptions',{pageNumber:1});
                return false;
            });
            $fuiSearchForm.find(".reset").click(function(){
                $fuiSearchForm[0].reset();
                $fuiSearchForm.submit();
            });
            var $switch=$(".fui-switch",$p);
            if($switch.length){
                requirejs(['fui_switch'],function(FuiSwitch){
                    FuiSwitch.initSwitch($switch,$p);
                });
            }

            var $selectors=$(".fui-select",$p);
            if($selectors.length){
                requirejs(['fui_selectize'],function(FuiSelect){
                    FuiSelect.initSelectize($selectors,$p);
                });
            }

            var $kindeditor = $(".fui-kindeditor",$p);
            if($kindeditor.length){
                requirejs(['fui_kindeditor'],function(FuiEditor){
                    console.info(FuiEditor);
                    FuiEditor.initKindEditor($kindeditor,$p);
                });
            }

        },
        addBootstrapTable:function(options){
            requirejs(['fui_table'],function(FuiTable){
                var $table=null;//$('#' + options.id)
                if(!options.id){
                    $table=$(".fui-dialog.fui-box").find(".fui-default-table");
                    if(!$table[0]){
                        $table=$(".fui-box.active").find(".fui-default-table");
                    }
                }else{
                    if(typeof options.id === 'string'){
                        $table=$('#' + options.id);
                    }else{
                        $table=options.id;
                    }
                }
                var $parent=$table.parents(".fui-box:first")
                    ,$form=$parent.find("form.fui-search")
                    ,$tool=$parent.find(".table-tool");
                if($table.length==0){
                    layer.msg("没有找到页面上的table");
                    return;
                }
                options.table=$table;
                if(options.unBindParent==undefined){//一个页面有多个table时，可以设置一个绑定，其他不绑定
                    options.toolbar=$tool;
                    if($form.length){
                        options.form=$form;
                    }
                    $parent.data("bootstrapTable",FuiTable.createBootstrapTable(options));
                }else{
                    FuiTable.createBootstrapTable(options);
                }
            });
        },
        openDialog:function(option){
            if(option.id){
                if($('#'+option.id)[0])return;
            }
            var layerIndex=0
                ,layerLoadIndex=layer.load(2);
            var op={
                id:option.id
                ,type: 1
                ,title: false
                ,closeBtn: false
                ,shadeClose: false
                ,maxWidth:1024
                ,skin:'fui-dialog fui-box'
                ,move:'.modal-header h4'
                ,offset:'t'
                ,zIndex: ++Fui.config.zindex
                ,success: function(layero){
                    //layer.setTop(layero);
                    layero.find('.fui-close').click(function(){
                        var cancelFun=$(this).data("cancelFun");
                        if(cancelFun){
                            if (! $.isFunction(cancelFun)) cancelFun = eval('(' + cancelFun + ')');
                            cancelFun();
                        }
                        layer.close(layerIndex);
                    });
                    Fui.initUI(layero);

                    if(option.callback){
                        option.callback(layero);
                    }
                    layero.find(".modal-title").html(option.title);
                    layer.close(layerLoadIndex);
                }
            };
            if(option.end){
                op.end = option.end;
            }
            if(option.area){
                op.area=option.area;
            }
            if(option.offset){
                op.offset=option.offset;
            }
            if(option.url){
                var cacheContent=Fui.cacheMap[option.url];
                if(option.cache && cacheContent){
                    op.content=cacheContent;
                    layerIndex=layer.open(op);
                }else{
                    $.ajax({
                        type: 'GET',
                        url: option.url,
                        dataType: 'html',
                        async:false,
                        cache:option.cache||false,
                        success: function(html, status, xhr) {
                            op.content= html;
                            layerIndex=layer.open(op);
                            if(option.cache){
                                Fui.cacheMap[option.url]= html;
                            }
                        },
                        error: function(xhr, status, error) {
                            layer.msg('打开弹窗发生错误:' + error);
                        }
                    });
                }
            }else{
                op.content=option.content;
                layerIndex=layer.open(op);
            }
            return layerIndex;
        },
        navbarDefaultSetting :{//navbar默认配置
            elem : undefined, // 容器
            data : undefined, // 数据源
            url : undefined, // 数据源地址
            type : 'GET', // 读取方式
            cached : false, // 是否使用缓存
            spreadOne : true // 设置是否只展开一个二级菜单
        }
    };

    var cacheName = 'tb_navbar';
    var Navbar = function() {
        /**
         *  默认配置
         */
        this.config =$.extend({},Fui.navbarDefaultSetting);
        this.v = '0.0.1';
    };
    /**
     * 获取html字符串{spread:false,children:[{href:"",icon:"",title:"",id:""}],icon:"",href:"",title:"",id:""}
     * @param {Object} data
     */
    function getNavbarHtml(data) {
        var ulHtml = '<ul class="nav navbar-nav nav-stacked" style="width: 100%" role="tablist">'
            ,item=null,ic=null, href=null
            ,baseUrl=Fui.config.baseUrl;
        for(var i = 0; i < data.length; i++) {
            item=data[i];
            item.ptitle='';
            item.level=1;
            item.id="fui_tab_"+(item.id||new Date().getTime());
            ulHtml += '<li class="nav-item';
            if(item.spread) {
                ulHtml += ' active';
            }
            ulHtml += '">';
            if(item.children !== undefined && item.children.length > 0) {
                ulHtml += '<a href="javascript:;" class="nav-a">';
                ulHtml += '<b class="fa pull-right ';
                if(item.spread) {
                    ulHtml+='fa-chevron-up"></b>';
                }else{
                    ulHtml+='fa-chevron-down"></b>';
                }
                ulHtml += '<i class="fa ' + item.icon + '" aria-hidden="true"></i>';
                ulHtml += '<span>' + data[i].title + '</span></a>'
                ulHtml += '<ul class="nav nav-child">'
                for(var j = 0; j < item.children.length; j++) {
                    ic=item.children[j];
                    ic.ptitle=item.title;
                    ic.level=2;
                    href=ic.href;
                    if(href.indexOf(baseUrl)!=0){
                        href=baseUrl+href;
                    }
                    ic.href=href;
                    ic.id="fui_tab_"+(ic.id||new Date().getTime());
                    ulHtml += '<li><a href="' + href + '" class="url-a" data-icon="'+ic.icon+'" id="'+ic.id+'" data-closed="'+ic.closed+'">';
                    ulHtml += '<i class="fa ' + ic.icon + '" aria-hidden="true"></i>';
                    ulHtml += '<span>' + ic.title + '</span></a></li>';
                }
                ulHtml += '</ul>';
            } else {
                href=item.href;
                if(href == undefined || item.href==''){
                    href='javascript:void(0);';
                }else if(href.indexOf(baseUrl)!=0){
                    href=baseUrl+href;
                }
                item.href=href;
                ulHtml += '<a href="' + href + '" class="nav-a url-a" data-icon="'+item.icon+'" id="'+item.id+'" data-closed="'+item.closed+'">';
                ulHtml += '<i class="fa ' + item.icon + '" aria-hidden="true"></i>';
                ulHtml += '<span>' + item.title + '</span></a>';
            }
            ulHtml += '</li>';
        }
        ulHtml += '</ul>';
        return ulHtml;
    };
    /**
     * 使用url初始化菜单
     */
    function initNavbarByUrl(_config,$container){
        $.ajax({
            type: _config.type,
            url: _config.url,
            async: false, //_config.async,
            dataType: 'json',
            success: function(result, status, xhr) {
                //添加缓存
                Fui.setCache(cacheName, {
                    key: 'navbar',
                    value: result
                });
                _config.data=result;
                $container.html(getNavbarHtml(result));
            },
            error: function(xhr, status, error) {
                layer.msg('Navbar error:' + error);
                return;
            },
            complete: function(xhr, status) {
                _config.elem = $container;
            }
        });
    }
    $.extend(Navbar.prototype,{
        render : function() {
            var _that = this;
            var _config = _that.config;
            if(typeof(_config.elem) !== 'string' && typeof(_config.elem) !== 'object') {
                layer.msg('Navbar error: elem参数未定义或设置出错，具体设置格式请参考文档API.');
                return;
            }
            var $container;
            if(typeof(_config.elem) === 'string') {
                $container = $('' + _config.elem + '');
            }
            if(typeof(_config.elem) === 'object') {
                $container = _config.elem;
            }
            if($container.length === 0) {
                layer.msg('Navbar error:找不到elem参数配置的容器，请检查.');
                return ;
            }
            if(_config.data === undefined && _config.url === undefined) {
                layer.msg('Navbar error:请为Navbar配置数据源.');
                return ;
            }
            if(_config.data !== undefined && typeof(_config.data) === 'object') {
                var html = getNavbarHtml(_config.data);
                $container.html(html);
                _config.elem = $container;
            } else {
                if(_config.cached) {
                    var cacheNavbar = Fui.setCache(cacheName);
                    if(cacheNavbar.navbar === undefined) {
                        initNavbarByUrl(_config,$container);
                    } else {
                        var html = getNavbarHtml(cacheNavbar.navbar);
                        $container.html(html);
                        _config.elem = $container;
                    }
                } else {
                    //清空缓存
                    Fui.setCache(cacheName, null);
                    initNavbarByUrl(_config,$container);
                }
            }

            var $ul = $container.children('ul');
            $ul.find('a.nav-a').each(function(){
                $(this).on('click',function(event){
                    var it=$(this),p=it.parent();
                    if(!p.hasClass("active")){
                        p.addClass("active");
                        p.find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
                        if(_config.spreadOne){//只展开一个二级菜单
                            p.addClass("active").find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
                            p.siblings().removeClass('active').find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
                        }
                    }else{
                        p.removeClass("active");
                        p.find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
                    }
                    event.preventDefault();
                });
            });
            var $urlas=$ul.find("a.url-a").each(function(){
                $(this).on('click',function(event){
                    var it=$(this);
                    $urlas.removeClass('cur-itemed');
                    it.addClass("cur-itemed");
                    Fui.tab.tabAdd({id:it.attr("id"),title:it.text(),url:it.attr("href"),icon:it.data("icon"),closed:it.data("closed"),refresh:it.data("refresh")});
                    event.preventDefault();
                });
            });
            return _that;
        },
        /**
         * 配置Navbar
         * @param {Object} options
         */
        set : function(options) {
            var that = this;
            that.config.data = undefined;
            $.extend(true, that.config, options);
            return that;
        },
        /**
         * 清除缓存
         */
        cleanCached : function(){
            Fui.setCache(cacheName,null);
        },
        /**
         * 获取菜单项
         */
        getMenuItem:function(url){
            if(!url)return null;
            var data = this.config.data;
            if(!data){
                return null;
            }
            var item=null,ic=null,url1=url;
            if(url1.indexOf("?")>0){
                url1=url1.substring(0,url1.indexOf("?"));
            }
            for(var i = 0; i < data.length; i++) {
                item=data[i];
                if(item.href && (item.href==url || item.href==url1)){
                    return item;
                }else{
                    if(item.children !== undefined && item.children.length > 0) {
                        for(var j = 0; j < item.children.length; j++) {
                            ic=item.children[j];
                            if(ic.href && (ic.href==url || ic.href==url1)){
                                return ic;
                            }
                        }
                    }
                }
            }
            return null;
        }
    });
    Fui.navbar=new Navbar();

    var Tab = function() {
        this.config = {
            elem: undefined,
            closed: true, //是否包含删除按钮
            autoRefresh: true
        };
    };
    function addTabContent(id,title,content,layerIndex){
        //添加tab
        TabELEM.titleBox.append(title);
        TabELEM.contentBox.append(content);
        layer.close(layerIndex);
        Fui.initUI(TabELEM.contentBox.find("#content_"+id));
        //切换到当前打开的选项卡
        TabELEM.titleBox.find('li[data-tabid=' + id + ']').find('a[role="tab"]').tab('show');
        $(window).resize();
    }
    function getTabContentPrefix(id,data,html,istab){
        var content='';
        if(!istab){
            content='<div role="tabpanel" class="tab-pane fui-box" id="content_'+id+'">';
        }
        content+='<div class="container-fluid">';
        if(id!='home'){
            var nav1=Fui.navbar.getMenuItem(data.url);
            content+='<ol class="breadcrumb"><li>首页</li>';
            if(nav1 && nav1.level==2 ){
                content+='<li>'+nav1.ptitle+'</li><li class="active">'+data.title+'</li>';
            }else{
                content+='<li class="active">'+data.title+'</li>';
            }
            content+='</ol>';
        }
        content+=html+"</div>";
        if(!istab){
            content+="</div>";
        }
        return content;
    }
    var TabELEM = {};
    $.extend(Tab.prototype,{
        /**
         * 参数设置
         * @param {Object} options
         */
        set : function(options) {
            var that = this;
            $.extend(true, that.config, options);
            return that;
        },
        /**
         * 初始化
         */
        init : function() {
            var that = this;
            var _config = that.config;
            if(typeof(_config.elem) !== 'string' && typeof(_config.elem) !== 'object') {
                layer.msg('Tab error: elem参数未定义或设置出错，具体设置格式请参考文档API.');
                return;
            }
            var $container;
            if(typeof(_config.elem) === 'string') {
                $container = $('' + _config.elem + '');
            }
            if(typeof(_config.elem) === 'object') {
                $container = _config.elem;
            }
            if($container.length === 0) {
                layer.msg('Tab error:找不到elem参数配置的容器，请检查.');
                return;
            }
            var html='<ul class="nav nav-tabs fui-tabs" role="tablist" id="fui_tab_header"></ul><div class="tab-content" id="fui_tab_content"></div>';
            $container.append(html);
            _config.elem = $container;
            TabELEM.titleBox = $container.children('ul#fui_tab_header');
            TabELEM.contentBox = $container.children('div#fui_tab_content');

            TabELEM.titleBox.on('click','i.fui-tab-close[data-tabid]', function() {
                var it=$(this),id=it.data("tabid"),
                    tabIndex=that.exists(id),//从0开始的，如果是第四个tab  则该值为3
                    len=TabELEM.titleBox.find('li').length,
                    li=it.parent(),
                    isActive=li.hasClass("active");
                li.remove();
                $("#content_"+id).remove();
                if(isActive){
                    if(tabIndex>=len-1){//-1是因为tabIndex从0开始，需要减1
                        tabIndex=len-2;
                    }else{
                        tabIndex--;
                    }
                    TabELEM.titleBox.find('li').eq(tabIndex).find('a[role="tab"]').tab('show');
                }
            });
            return that;
        },
        close:function(id){
            TabELEM.titleBox.find('i.fui-tab-close[data-tabid='+id+']').click();
        },
        /**
         * 查询tab是否存在，如果存在则返回索引值，不存在返回-1
         * @param {String} 标题
         */
        exists : function(id) {
            var that = TabELEM.titleBox === undefined ? this.init() : this,
                tabIndex = -1;
            TabELEM.titleBox.find('li').each(function(i, e) {
                var tabid = $(this).data('tabid');
                if(tabid === id) {
                    tabIndex = i;
                };
            });
            return tabIndex;
        },
        /**
         * 添加选择卡，如果选择卡存在则获取焦点
         * @param {Object} data
         */
        tabAdd : function(data) {
            var that = this;
            var _config = that.config;
            var id=data.id||new Date().getTime();
            var tabIndex = that.exists(id);
            if(tabIndex === -1) {
                var title = '<li data-tabid="'+id+'"><a href="#content_'+id+'" role="tab" data-toggle="tab">';
                if(data.icon !== undefined) {
                    title += '<i class="fa ' + data.icon + '" aria-hidden="true"></i>';
                }
                title += '<span>' + data.title + '</span></a>';
                if(_config.closed && data.closed!=false) {
                    title += '<i class="fui-tab-close" data-tabid="'+id+'">&times;</i>';
                }
                title+='</li>';
                if(data.content){
                    addTabContent(id,title,getTabContentPrefix(id,data,data.content),layerIndex);
                } else{
                    var layerIndex=layer.load(2);
                    var cacheContent=null;
                    if(!data.refresh){
                        cacheContent=Fui.cacheMap[data.url];
                    }
                    if(cacheContent){
                        addTabContent(id,title,getTabContentPrefix(id,data,cacheContent),layerIndex);
                    }else{
                        $.ajax({
                            type: 'GET',
                            url: data.url,
                            dataType: 'html',
                            cache:!data.refresh,
                            success: function(html, status, xhr) {
                                addTabContent(id,title,getTabContentPrefix(id,data,html),layerIndex);
                                Fui.cacheMap[data.url]=html;
                            },
                            error: function(xhr, status, error) {
                                layer.msg('打开标签发生错误:' + error);
                            }
                        });
                    }
                }
            } else {
                TabELEM.titleBox.find('li').eq(tabIndex).find('a[role="tab"]').tab('show');
                //自动刷新
                if((_config.autoRefresh && data.url)||data.refresh){
                    $.get(data.url,function(html){
                        var con=TabELEM.contentBox.find("#content_"+id);
                        con.html(getTabContentPrefix(id,data,html,true));
                        Fui.initUI(con);
                    },"html");
                }
            }
        }
    });
    Fui.tab=new Tab();

    window.Fui=Fui;
    return Fui;
});