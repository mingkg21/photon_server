define(['moment','moment_zh','datetimepicker'],function(moment){
	window.moment=moment;
	moment.locale('zh-cn');
	return {
		initDatetime:function(dateEle,$p){
			dateEle.each(function(){
				var it=$(this)
					,format=it.attr("format")||'YYYY-MM-DD'//' HH:mm:ss'
					,options = {
						locale : 'zh-cn',
						format : format,
						minDate:it.attr("minDate")||false,
						maxDate:it.attr("maxDate")||false,
						sideBySide:true,
						viewMode:it.attr("viewMode")||'days',
						showTodayButton:true,
						showClear:true
					};
					it.datetimepicker(options);
			});
			
			$(".fui-date-btn",$p).each(function(){
				var it=$(this)
					,target=it.data("target")||'today'
					,parent=it.parents(".fui-data-wrap")
					,dates=parent.find(".fui-date")
					,$searchBtn=parent.find(".fui-date-search");
				if(!parent.length){
					layer.msg("日期按钮没有class='fui-data-wrap'的父级");
					return;
				}
				if(dates.length!=2){
					layer.msg("日期按钮必须对应的2个日期控件");
					return;
				}
				it.click(function(){
					switch(target){
					case 'today':
						var m=moment(),$d,format;
						$d=dates.eq(0);
						format=$d.attr("format")||'YYYY-MM-DD';
						if(format.indexOf("HH:mm:ss")>-1){
							format=format.replace("HH:mm:ss","00:00:00");
						}
						$d.val(m.format(format));
						
						$d=dates.eq(1);
						format=$d.attr("format")||'YYYY-MM-DD';
						if(format.indexOf("HH:mm:ss")>-1){
							format=format.replace("HH:mm:ss","23:59:59");
						}
						$d.val(m.format(format));
						break;
					case 'yesterday':
						var m=moment().subtract(1, 'days'),$d,format;
						$d=dates.eq(0);
						format=$d.attr("format")||'YYYY-MM-DD';
						if(format.indexOf("HH:mm:ss")>-1){
							format=format.replace("HH:mm:ss","00:00:00");
						}
						$d.val(m.format(format));
						
						$d=dates.eq(1);
						format=$d.attr("format")||'YYYY-MM-DD';
						if(format.indexOf("HH:mm:ss")>-1){
							format=format.replace("HH:mm:ss","23:59:59");
						}
						$d.val(m.format(format));
						break;
					case 'thisWeek':
						var m=moment(),w=m.week(),$d,format;
						if(m.weekday()==6){
							w=w-1;
						}
						m.week(w);
						m.day(1);
						$d=dates.eq(0);
						format=$d.attr("format")||'YYYY-MM-DD';
						if(format.indexOf("HH:mm:ss")>-1){
							format=format.replace("HH:mm:ss","00:00:00");
						}
						$d.val(m.format(format));
						m.add(6,'days');
						$d=dates.eq(1);
						format=$d.attr("format")||'YYYY-MM-DD';
						if(format.indexOf("HH:mm:ss")>-1){
							format=format.replace("HH:mm:ss","23:59:59");
						}
						$d.val(m.format(format));
						break;
					case 'lastWeek':
						var m=moment(),w=m.week(),$d,format;
						if(m.weekday()==6){
							w=w-1;
						}
						m.week(w-1);
						m.day(1);
						$d=dates.eq(0);
						format=$d.attr("format")||'YYYY-MM-DD';
						if(format.indexOf("HH:mm:ss")>-1){
							format=format.replace("HH:mm:ss","00:00:00");
						}
						$d.val(m.format(format));
						m.add(6,'days');
						$d=dates.eq(1);
						format=$d.attr("format")||'YYYY-MM-DD';
						if(format.indexOf("HH:mm:ss")>-1){
							format=format.replace("HH:mm:ss","23:59:59");
						}
						$d.val(m.format(format));
						break;
					case 'thisMonth':
						var m=moment(),$d,format;
						m.date(1);
						$d=dates.eq(0);
						format=$d.attr("format")||'YYYY-MM-DD';
						if(format.indexOf("HH:mm:ss")>-1){
							format=format.replace("HH:mm:ss","00:00:00");
						}
						$d.val(m.format(format));
						m.add(1,'months').subtract(1, 'days');
						$d=dates.eq(1);
						format=$d.attr("format")||'YYYY-MM-DD';
						if(format.indexOf("HH:mm:ss")>-1){
							format=format.replace("HH:mm:ss","23:59:59");
						}
						$d.val(m.format(format));
						break;
					case 'lastMonth':
						var m=moment(),$d,format;
						m.date(1).subtract(1, 'months');
						$d=dates.eq(0);
						format=$d.attr("format")||'YYYY-MM-DD';
						if(format.indexOf("HH:mm:ss")>-1){
							format=format.replace("HH:mm:ss","00:00:00");
						}
						$d.val(m.format(format));
						m.add(1,'months').subtract(1, 'days');
						$d=dates.eq(1);
						format=$d.attr("format")||'YYYY-MM-DD';
						if(format.indexOf("HH:mm:ss")>-1){
							format=format.replace("HH:mm:ss","23:59:59");
						}
						$d.val(m.format(format));
						break;
					}
					$searchBtn.click();
					return false;
				});
			});
		}
	}
});