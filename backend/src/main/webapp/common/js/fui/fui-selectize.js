define(['jquery','selectize'],function(){

    var initOneSelectize = function(){
        var it = $(this);
        //var val = it.val();
        var url = it.data("url"),
        jsonValue = it.data("json-value"),
        valueField = it.data('value-field') || 'id',
        labelField = it.data('label-field') || 'name',
        pageSize = it.data('page-size') || 100,
        maxItem = it.data('max-items');

        var isRequired = it.is("[required]");
        var options = null;
        if(jsonValue){
            try{
                var arr = null;
                if($.isArray(jsonValue)) {
                    arr = jsonValue;
                }else if(BaseUtil.isString(jsonValue)){
                   var val = JSON.parse(jsonValue);
                   if(BaseUtil.isArray(val)){
                       arr =  val;
                   }else if(BaseUtil.isObj(val)){
                       arr = [val];
                   }
                }else if(BaseUtil.isObj(jsonValue)){
                    arr = [jsonValue];
                }

                var vals = [];
                for(var i = 0;i < arr.length; i++){
                    var item = arr[i];
                    vals.push(item[valueField]);
                }
                it.val(vals.join(","));
                options = arr;
            }catch(e){
                console.info(e.message);
            }
        }else{
            it.val("");
        }

        it.selectize({
            options:options,
            plugins: ['remove_button'],
            valueField: valueField,
            labelField: labelField,
            searchField: labelField,
            maxItems:maxItem,
            // onInitialize:function(){
            //     if(isRequired){
            //         it.show();
            //         it.css("border","0px");
            //         it.css("height","0px");
            //         it.css("width","0px");
            //         it.next().css("top","-20px");
            //     }
            // },
            load: function(query, callback) {
                if(!url){
                    return;
                }

               $.ajax({
                   url:url,
                   data:{
                       pageSize:pageSize,
                       key:query
                   },
                   success:function(data){
                       callback(data);
                   }
               });
            }
        });
    }

    return {
        initSelectize:function(eles,$p){
            eles.each(initOneSelectize);
        }
    }
});