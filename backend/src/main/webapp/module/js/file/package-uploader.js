
define(['jquery','Fui','layer','qiniu','qiniu_plupload','qiniu_moxie'],function(){

    function saveFileInfo(data){
        $.ajax({
            url: baseInfo.baseUrl + '/file/package/uploadSuccess',
            data:data,
            success: function (data) {

            }
        })
    }

    //上传成功的数据
    var uploadData = {};

    var getUploader = function(btnId,containerId,dataFn,selectFn) {
        var fileKey = null;
        var uploader = Qiniu.uploader({
            runtimes: 'html5,flash,html4',
            browse_button: btnId,
            max_file_size: '8000mb',
            flash_swf_url: 'Moxie.swf',
            chunk_size: '4mb',
            multi_selection: false,
            uptoken_func: function () {
                var token = null;
                $.ajax({
                    url: baseInfo.baseUrl + '/file/package/getUploadToken',
                    async: false,
                    success: function (data) {
                        token = data.token;
                        fileKey = data.fileKey;
                    }
                });
                if (token == null) {
                    throw "get upload token error";
                }
                return token;
            },
            domain: baseInfo.cdn,
            get_new_uptoken: true,
            filters: {
                max_file_size: '8000mb',
                prevent_duplicates: true,
                mime_types: [
                    {title : "apk", extensions : "apk" }
                ]
            },
            auto_start: true,
            log_level: 5,
            init: {
                'FilesAdded': function (up, files) {
                    //size
                    var html = "";
                    for(var i=0;i<files.length;i++){
                        var f = files[i];
                        var text = '<div file="'+f.id+'" ><div style="margin-top: 5px;margin-bottom: 5px;">';
                        if(selectFn){
                            text  +=  '<button disabled file-id="'+f.id+'" type="button" class="btn btn-primary btn-xs" >';
                            text  +=  '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> 选择';
                            text  +=  '</button>';
                        }
                        text += '应用包:'+f.name+' ('+Fui.formatFileSize(f.size)+')</div>'
                            +'<div class="progress" >'
                            +'<div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%">'
                            +'<span class="progress-percent">0% Complete</span>'
                            +'</div>'
                            +'</div>'
                            + '</div>'
                        html += text;
                    }
                    var $container =  $("#"+containerId);
                    $container.append(html);
                    if(selectFn){
                        $container.find("button").each(function(){
                            if(this.getAttribute("bind") == "true"){
                                return;
                            }
                            $(this).click(function(){
                                var fileId = this.getAttribute("file-id");
                                selectFn(uploadData[fileId]);
                            });
                            this.setAttribute("bind",true);
                        });
                    }
                },
                'BeforeUpload': function (up, file) {

                },
                'UploadProgress': function (up, file) {
                    var $progressContainer = $("#"+containerId);
                    var $div = $progressContainer.find("div[file='"+file.id+"']")
                    var percent = file.percent + "%";
                    $div.find(".progress-bar").css("width",percent);
                    $div.find(".progress-percent").html(percent);
                    $div.find("button").removeAttr("disabled");
                },
                'UploadComplete': function () {
                    layer.msg('完成上传');
                },
                'FileUploaded': function (up, file, info) {
                    // var domain = up.getOption('domain');
                    var res = JSON.parse(info);
                    var data = dataFn();
                    var copyData = {};
                    $.extend(copyData,data);
                    $.extend(data,{
                        name:file.name,
                        key:res.key,
                        hash:res.hash,
                        size:file.size
                    });
                    // var data = {
                    //
                    // }
                    var udata = {
                        fileName:file.name,
                        src : res.key,
                        cdnSrc:baseInfo.cdn + res.key,
                        size: file.size
                    }
                    $.extend(udata,copyData);
                    uploadData[file.id] = udata;
                    saveFileInfo(data);
                },
                'Error': function (up, err, errTip) {
                    var file = err.file;
                    var $progressContainer = $("#"+containerId);
                    var $div = $progressContainer.find("div[file='"+file.id+"']")
                    $div.find(".progress-percent").html("<font color='red'>上传失败:"+err.message+"</font>");
                },
                'Key': function (up, file) {
                    if (!fileKey) {
                        layer.msg('文件key生成失败');
                        throw "Error file key";
                    }
                    var name = file.name;
                    var index = name.lastIndexOf(".");
                    if(index == -1){
                        layer.msg('文件格式有误');
                        throw "Error file type";
                    }
                    var type = name.substring(index+1);
                    return fileKey+"/"+Fui.uuid()+"."+type
                }
            }
        });
        return uploader;
    }
    return {getUploader:getUploader};
});