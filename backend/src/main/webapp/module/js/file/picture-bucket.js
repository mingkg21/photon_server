

require(["Fui","layer"],function(){
    var callback_function_map = {};

    Picture = function(callback,mulSelect){
        this.callback = callback;
        this.mulSelect = mulSelect === true;
    }

    Picture.prototype = {
        select : function(){
            this.layerId = createId();
            var fnId = createId();
            if(this.callback){
                callback_function_map[fnId] = this.callback;
            }
            this.layerIndex = Fui.openDialog({
                url: baseInfo.baseUrl+"/file/picture/bucket?mulSelect="+this.mulSelect+"&fnId="+fnId,
                cache:false,
                end : function () {
                    delete callback_function_map[fnId];
                }
            });
        },
        close : function () {
            layer.close(this.layerIndex );
        }
    }

    Picture.getCallback = function(fnId){
        return callback_function_map[fnId];
    }
});