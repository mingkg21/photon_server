

require(["Fui","layer"],function(){
    var callback_function_map = {};
    Package = function(callback){
        this.callback = callback;
    }

    Package.prototype = {
        select : function(){
            this.layerId = createId();
            var fnId = createId();
            if(this.callback){
                callback_function_map[fnId] = this.callback;
            }
            this.layerIndex =  Fui.openDialog({
                url: baseInfo.baseUrl+"/file/package/bucket?fnId="+fnId,
                cache:false,
                end : function () {
                    delete callback_function_map[fnId];
                }
            });
        },
        close : function () {
            layer.close(this.layerIndex);
        }
    }

    Package.getCallback = function(fnId){
        return callback_function_map[fnId];
    }
});