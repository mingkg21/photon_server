/**
 *   公共弹窗
 */
require(["Fui","layer"],function() {
    //apk包选择
    Package = function(callback){
        this.callback = callback;
        this.mulSelect = false;
        this.windowSrc = "/file/package/bucket";
    }

    //视频选择
    Video = function(callback){
        this.callback = callback;
        this.mulSelect = false;
        this.windowSrc = "/file/video/bucket";
    }

    AnyFile = function(callback){
        this.callback = callback;
        this.mulSelect = false;
        this.windowSrc = "/file/any/bucket";
    }

    //图片选择
    Picture = function(callback,mulSelect){
        this.callback = callback;
        this.mulSelect = mulSelect === true;
        this.windowSrc = "/file/picture/bucket";
    }

    //游戏选择
    Game  = function(callback,paramStr){
        this.callback = callback;
        this.mulSelect = false;
        var url =  "/game/info/bucket";
        this.paramStr = paramStr;
        // if(config && config.versionType){
        //     url += "?versionType="+config.versionType;
        // }
        this.windowSrc = url;
    }


    UserSelector = function(callback){
        this.callback = callback;
        this.mulSelect = false;
        this.windowSrc = "/account/info/bucket";
    }

    //给所有对象绑定方法
    var all = [Video,Package,Picture,Game,AnyFile,UserSelector];
    var callback_function_map = {};
    for(var i = 0 ;i < all.length;i++){
        all[i].getCallback = function(fnId){
            return callback_function_map[fnId];
        }

        all[i].prototype = {
            select : function(){
                var fnId = createId();
                if(this.callback){
                    callback_function_map[fnId] = this.callback;
                }

                var url = baseInfo.baseUrl+this.windowSrc+"?fnId="+fnId+"&mulSelect="+this.mulSelect;

                if(this.paramStr){
                    url = url + "&" + this.paramStr;
                }

                this.layerIndex =  Fui.openDialog({
                    url : url,
                    cache:false,
                    end : function () {
                        delete callback_function_map[fnId];
                    }
                });
            },
            close : function () {
                layer.close(this.layerIndex);
            }
        }
    }
});