package cc.ccplay.booster.backend.core;
import java.util.*;

import cc.ccplay.booster.base.model.user.SysMenu;
import org.easyj.frame.util.StringUtil;

public class MenuNode implements Comparable<MenuNode>{

    private Long id;

    private Long sort;

    private String title;

    private String href;

    private String icon;

    private Long parentId;

    private Long level;

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSort() {
        return sort;
    }

    public void setSort(Long sort) {
        this.sort = sort;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public MenuNode(){}

    public MenuNode(SysMenu node){
        this.setCurNode(node);
    }
    /**
     * 子菜单栏
     */
    private List<MenuNode> children;


    public void setCurNode(SysMenu curNode) {
        this.id = curNode.getId();
        this.href = curNode.getUrl();
        this.title = curNode.getName();
        this.sort = StringUtil.toLong(curNode.getSort());
        this.icon = curNode.getIcon();
        this.parentId = curNode.getParentId();
        this.level = curNode.getLevel();
    }

    public List<MenuNode> getChildren() {
        return children;
    }

    public void setChildren(List<MenuNode> children) {
        this.children = children;
    }

    public int compareTo(MenuNode o) {
        return this.sort.compareTo(o.sort);
    }


    public static MenuNode toMenuNode(List<SysMenu> menu) {
        MenuNode node = new MenuNode();
        Map<Long, List<MenuNode>> nodeMap = new HashMap<>();
        for (int i = 0; i < menu.size(); i++) {
            SysMenu m = menu.get(i);
            long parentId = StringUtil.toLong(m.getParentId());
            List<MenuNode> nodes = nodeMap.get(parentId);
            if (nodes == null) {
                nodes = new ArrayList<>();
                nodeMap.put(parentId, nodes);
            }
            nodes.add(new MenuNode(m));
        }
        setChildNodes(node, nodeMap, 0L);
        return node;
    }

    /**
     * 设置子节点
     *
     * @param node
     * @param nodeMap
     * @param parentId
     */
    private static void setChildNodes(MenuNode node, Map<Long, List<MenuNode>> nodeMap, Long parentId) {

        List<MenuNode> nodes = nodeMap.get(parentId);
        if (nodes == null || nodes.size() == 0) {
            return;
        }

        node.setChildren(nodes);
        Collections.sort(nodes);
        for (int i = 0; i < nodes.size(); i++) {
            MenuNode menuNode = nodes.get(i);
            Long id = menuNode.getId();
            setChildNodes(menuNode, nodeMap, id);
        }
    }

}
