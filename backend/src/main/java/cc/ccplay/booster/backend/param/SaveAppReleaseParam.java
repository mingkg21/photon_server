package cc.ccplay.booster.backend.param;

import cc.ccplay.booster.base.model.content.AppRelease;
import org.easyj.frame.validation.annotation.DateFormat;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;

import java.util.Date;

public class SaveAppReleaseParam {

    public AppRelease toModel(){
        AppRelease release = new AppRelease();
        release.setId(id);
        release.setDownloadUrl(this.downloadUrl);
        release.setName(this.name);
        release.setReleaseTime(this.releaseTime);
        release.setVersionCode(this.versionCode);
        release.setVersionName(this.versionName);
        release.setUpdateLog(this.updateLog);
        release.setPackageName(this.packageName);
        release.setFileSize(this.fileSize);
        release.setForceUpdate(this.forceUpdate);
        return release;
    }

    @Param
    private Long id;

    @NotNull
    private Long fileSize;

    @NotNull
    private String name;

    @NotNull
    private String versionName;

    @NotNull
    @DateFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date releaseTime;

    @NotNull
    private String downloadUrl;

    @NotNull
    private String updateLog;

    @NotNull
    private String packageName;

    @NotNull
    private Long versionCode;

    @NotNull
    private Long forceUpdate;


    public Long getForceUpdate() {
        return forceUpdate;
    }

    public void setForceUpdate(Long forceUpdate) {
        this.forceUpdate = forceUpdate;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public Date getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(Date releaseTime) {
        this.releaseTime = releaseTime;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getUpdateLog() {
        return updateLog;
    }

    public void setUpdateLog(String updateLog) {
        this.updateLog = updateLog;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public Long getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(Long versionCode) {
        this.versionCode = versionCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
