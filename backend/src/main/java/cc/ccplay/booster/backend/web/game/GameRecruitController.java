package cc.ccplay.booster.backend.web.game;

import cc.ccplay.booster.backend.param.SaveGameRecruit;
import cc.ccplay.booster.backend.param.SaveGameRecruitQuestionParam;
import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.api.content.GameRecruitQuestionService;
import cc.ccplay.booster.base.dto.content.game.GameRecruitQuestionDto;
import cc.ccplay.booster.base.model.content.GameRecruit;
import cc.ccplay.booster.base.model.content.GameRecruitAnswer;
import cc.ccplay.booster.base.model.content.GameRecruitQuestion;
import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.api.content.GameRecruitService;
import cc.ccplay.booster.base.dto.content.game.GameRecruitDto;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RequestMapping("/game/recruit")
@Controller
public class GameRecruitController extends BaseBackendController {

    @Reference
    private GameRecruitService gameRecruitService;

    @Reference
    private GameRecruitQuestionService gameRecruitQuestionService;

    @RequestMapping("/index")
    public String index(){
        return "/game/recruit/list";
    }

    @RequestMapping("/goEditPage")
    public String goEditPage(HttpServletRequest request, @NotNull long id){
        GameRecruitDto dto = gameRecruitService.getGameRecruitDto(super.getSystemParam(),id);
        if(dto == null){
            throw new GenericException("玩家招募信息不存在");
        }
        request.setAttribute("recruitInfo",dto.getGameRecruit());
        request.setAttribute("gameInfo",dto.getGameInfo());
        request.setAttribute("versionInfo",dto.getVersionInfo());
        return "/game/recruit/edit";
    }

    @RequestMapping("/goAddPage")
    public String goAddPage(HttpServletRequest request){
        return "/game/recruit/edit";
    }

    @RequestMapping("/list")
    @ResponseBody
    public void list(){
        Page page = gameRecruitService.getPage(super.getSystemParam());
        super.renderJson(page);
    }

    @RequestMapping("/updateStatus")
    @ResponseBody
    public void updateStatus(@NotNull long id ,@NotNull long status){
        gameRecruitService.updateStatus(super.getSystemParam(),id,status);
        super.renderSuccess();
    }

    @RequestMapping("/saveOrUpdate")
    @ResponseBody
    public void saveOrUpdate(@Param SaveGameRecruit saveGameRecruit){
        GameRecruit gameRecruit = saveGameRecruit.toGameRecruit();
        long startTime = gameRecruit.getStartTime().getTime();
        long endTime = gameRecruit.getEndTime().getTime();
        if(endTime <= startTime){
            throw new GenericException("结束时间必须大于开始时间");
        }
        gameRecruitService.saveOrUpdate(super.getSystemParam(),gameRecruit);
        super.renderSuccess();
    }

    @RequestMapping("/getQuestionList")
    @ResponseBody
    public void getQuestionList(@NotNull long recruitId){
        Page page = gameRecruitQuestionService.getPage(super.getSystemParam(),recruitId);
        super.renderJson(page);
    }

    @RequestMapping("/goQuestionPage")
    public String goQuestionPage(HttpServletRequest request,@NotNull long recruitId){
        request.setAttribute("recruitId",recruitId);
        return "/game/recruit/question";
    }

    @RequestMapping("/goEditQuestionPage")
    public String goEditQuestionPage(HttpServletRequest request,@NotNull long questionId){
        GameRecruitQuestionDto dto = gameRecruitQuestionService.getGameRecruitQuestionDto(super.getSystemParam(),questionId);
        if(dto == null){
            throw new GenericException("题目不存在");
        }
        request.setAttribute("question",dto.getGameRecruitQuestion());
        request.setAttribute("answers",dto.getAnswers());
        request.setAttribute("recruitId",dto.getGameRecruitQuestion().getRecruitId());
        return "/game/recruit/editQuestion";
    }

    @RequestMapping("/goAddQuestionPage")
    public String goAddQuestionPage(HttpServletRequest request,@NotNull long recruitId){
        request.setAttribute("recruitId",recruitId);
        return "/game/recruit/editQuestion";
    }

    @RequestMapping("/saveOrUpdateQuestion")
    @ResponseBody
    public void saveOrUpdateQuestion(@Param SaveGameRecruitQuestionParam saveGameRecruitQuestionParam){
        GameRecruitQuestion gameRecruitQuestion = saveGameRecruitQuestionParam.getGameRecruitQuestion();
        List<GameRecruitAnswer> as = saveGameRecruitQuestionParam.getGameRecruitAnswers();
        gameRecruitQuestionService.saveOrUpdate(super.getSystemParam(),gameRecruitQuestion,as);
        super.renderSuccess();
    }
    

}
