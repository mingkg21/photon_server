package cc.ccplay.booster.backend.param;

import cc.ccplay.booster.base.model.content.GiftInfo;
import org.easyj.frame.validation.annotation.DateFormat;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;

import java.util.Date;

public class SaveGiftInfoParam {

    public GiftInfo toGiftInfo(){
        GiftInfo info = new GiftInfo();
        info.setId(this.id);
        info.setType(this.type);

        info.setDescription(this.description);
        info.setStartTime(this.startTime);
        info.setEndTime(this.endTime);
        info.setGameId(this.gameId);
        info.setInstructions(this.instructions);
        info.setStatus(this.status);
        info.setName(this.name);
        return info;
    }

    @Param
    private Long id;

    @NotNull
    private Long type;

    @NotNull
    private Long gameId;

    @NotNull
    @DateFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    @NotNull
    @DateFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    @NotNull
    private String description;

    @NotNull
    private Long status;

    /**
     使用说明
     */
    @NotNull
    private String instructions;


    @NotNull
    private String name;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
