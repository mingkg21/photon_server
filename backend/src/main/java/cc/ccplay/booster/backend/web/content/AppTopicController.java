package cc.ccplay.booster.backend.web.content;

import cc.ccplay.booster.base.model.content.AppTopic;
import cc.ccplay.booster.backend.param.AppTopicItemParam;
import cc.ccplay.booster.backend.web.BaseBackendController;
import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.api.content.AppTopicItemService;
import cc.ccplay.booster.base.api.content.AppTopicService;
import cc.ccplay.booster.base.dto.content.AppTopicITemContent;
import cc.ccplay.booster.base.dto.content.AppTopicItemDto;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.AppTopicItem;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.JsonUtil;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/content/appTopic")
public class AppTopicController extends BaseBackendController {

    @Reference
    private AppTopicService appTopicService;

    @Reference
    private AppTopicItemService appTopicItemService;

    @RequestMapping("/index")
    public String index(){
        return "/content/appTopic/list";
    }

    @ResponseBody
    @RequestMapping("/list")
    public void list(){
        Page page = appTopicService.getPage(super.getSystemParam());
        super.renderJson(page);
    }

    @RequestMapping("/goAddPage")
    public String goAddPage(){
        return "/content/appTopic/edit";
    }

    @RequestMapping("/goEditPage")
    public String goEditPage(HttpServletRequest request, @NotNull long id){
        AppTopic appTopic = appTopicService.get(id);
        if(appTopic == null){
            throw new GenericException("数据不存在");
        }
        request.setAttribute("appTopic",appTopic);
        return "/content/appTopic/edit";
    }


    @ResponseBody
    @RequestMapping("/saveOrUpdate")
    public void saveOrUpdate(@Param Long id,@NotNull String title, @NotNull long ordering, @NotNull String picUrl){
        AppTopic appTopic = new AppTopic();
        appTopic.setId(id);
        appTopic.setPicUrl(picUrl);
        appTopic.setOrdering(ordering);
        appTopic.setTitle(title);
        appTopicService.saveOrUpdate(appTopic);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/updateStatus")
    public void updateStatus(@NotNull long id,@NotNull long status){
        appTopicService.updateStatus(id, Status.getNotNull(status));
        super.renderSuccess();
    }


    @RequestMapping("/goItemListPage")
    public String goItemListPage(HttpServletRequest request,@NotNull long topicId){
        request.setAttribute("topicId",topicId);
        return "/content/appTopic/itemList";
    }

    @ResponseBody
    @RequestMapping("/getItemList")
    public void getItemList(@NotNull long topicId){
        Page page = appTopicItemService.getPage(super.getSystemParam(),topicId);
        super.renderJson(page);
    }

    @ResponseBody
    @RequestMapping("/saveOrUpdateItem")
    public void saveOrUpdateItem(@Param AppTopicItemParam param){
        AppTopicItem item = param.toModel();
        appTopicItemService.saveOrUpdate(item);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/deleteItem")
    public void deleteItem(@NotNull long itemId){
        appTopicItemService.delete(itemId);
        super.renderSuccess();
    }

    @RequestMapping("/goEditItemPage")
    public String goEditItemPage(HttpServletRequest request,@NotNull long id){
        AppTopicItemDto dto = appTopicItemService.getDto(id);
        if(dto == null){
            throw new GenericException("数据不存在");
        }
        AppTopicItem item = dto.getAppTopicItem();
        String content = item.getContent();
        try{
            request.setAttribute("content",JsonUtil.toBean(content, AppTopicITemContent.class));
        }catch (Exception e){}
        request.setAttribute("item",item);
        request.setAttribute("topicId",item.getTopicId());
        request.setAttribute("gameInfo",dto.getGameInfo());
        request.setAttribute("versionInfo",dto.getVersionInfo());
        return "/content/appTopic/editItem";
    }


    @RequestMapping("/goAddItemPage")
    public String goAddItemPage(HttpServletRequest request,@NotNull long topicId){
        request.setAttribute("topicId",topicId);
        return "/content/appTopic/editItem";
    }
}
