package cc.ccplay.booster.backend.param;

import cc.ccplay.booster.base.model.content.AdArea;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;

public class AdAreaParam {

    public AdArea toBaseModel(){
        AdArea adArea = new AdArea();
        adArea.setId(id);
        adArea.setCode(code);
        adArea.setName(name);
        adArea.setRemark(remark);
        return adArea;
    }

    @Param
    private Long id;

    @NotNull
    private String code;

    @NotNull
    private String name;

    @Param
    private String remark;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
