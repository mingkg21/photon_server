package cc.ccplay.booster.backend.web.booster;


import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.annotation.CheckType;
import cc.ccplay.booster.base.annotation.Permission;
import cc.ccplay.booster.base.api.content.BoosterServerGroupRefService;
import cc.ccplay.booster.base.api.content.BoosterServerGroupService;
import cc.ccplay.booster.base.dto.SelectizeDto;
import cc.ccplay.booster.base.dto.content.booster.BoosterServerGroupRefDto;
import cc.ccplay.booster.base.model.content.BoosterServer;
import cc.ccplay.booster.base.model.content.BoosterServerGroup;
import cc.ccplay.booster.base.model.content.BoosterServerGroupRef;
import com.alibaba.dubbo.config.annotation.Reference;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.JsonUtil;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/booster/serverGroup")
public class BoosterServerGroupController extends BaseBackendController{

    @Reference
    private BoosterServerGroupService boosterServerGroupService;

    @Reference
    private BoosterServerGroupRefService boosterServerGroupRefService;

    @RequestMapping("/index")
    public String index(){
        return "/booster/serverGroup/list";
    }

    @ResponseBody
    @RequestMapping("/list")
    public void list(@Param String name){
        super.renderJson(boosterServerGroupService.getPage(super.getSystemParam(),name));
    }

    @RequestMapping("/goEditPage")
    public String goEditPage(HttpServletRequest request,@NotNull long id){
        BoosterServerGroup group = boosterServerGroupService.get(id);
        if(group == null){
            throw new GenericException("数据不存在");
        }
        request.setAttribute("group",group);
        return "/booster/serverGroup/edit";
    }

    @RequestMapping("/goAddPage")
    public String goAddPage(){
        return "/booster/serverGroup/edit";
    }

    @ResponseBody
    @RequestMapping("/saveOrUpdate")
    public void saveOrUpdate(@Param Long id ,@NotNull String name){
        BoosterServerGroup group = new BoosterServerGroup();
        group.setId(id);
        group.setName(name);
        boosterServerGroupService.saveOrUpdate(group);
        super.renderSuccess();
    }

    @RequestMapping("/goServerListPage")
    public String goServerListPage(HttpServletRequest request,@NotNull long groupId){
        request.setAttribute("groupId",groupId);
        return "/booster/serverGroup/serverList";
    }

    @RequestMapping("/goAddServerPage")
    public String goAddServerPage(HttpServletRequest request,@NotNull long groupId){
        request.setAttribute("groupId",groupId);
        return "/booster/serverGroup/editServer";
    }

    @RequestMapping("/goEditServerPage")
    public String goEditServerPage(HttpServletRequest request,@NotNull long refId){
        BoosterServerGroupRefDto dto = boosterServerGroupRefService.getDto(refId);
        if(dto == null){
            throw new GenericException("数据不存在");
        }
        request.setAttribute("ref",dto.getRef());
        BoosterServer server = dto.getServer();
        if(server != null){
            SelectizeDto selectizeDto = new SelectizeDto(server.getId(),server.getName());
            request.setAttribute("serverJson", JsonUtil.toJson(selectizeDto));
        }
        return "/booster/serverGroup/editServer";
    }

    @ResponseBody
    @RequestMapping("/saveOrUpdateServer")
    public void saveServer(@Param Long id ,@NotNull long groupId,@NotNull long serverId,@NotNull long ordering){
        BoosterServerGroupRef ref = new BoosterServerGroupRef();
        ref.setId(id);
        ref.setGroupId(groupId);
        ref.setServerId(serverId);
        ref.setOrdering(ordering);
        boosterServerGroupRefService.saveOrUpdate(ref);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/deleteServer")
    public void deleteServer(@NotNull long refId){
        boosterServerGroupRefService.delete(refId);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/getServerList")
    public void getServerList(@NotNull long groupId){
        Page page = boosterServerGroupRefService.getPage(super.getSystemParam(),groupId);
        super.renderJson(page);
    }


    @Permission(CheckType.OPEN)
    @ResponseBody
    @RequestMapping("/getForSelectize")
    public void getForSelectize(@Param String key){
        List<SelectizeDto> list = boosterServerGroupService.getForSelectize(super.getSystemParam(),key);
        super.renderJson(list);
    }
}
