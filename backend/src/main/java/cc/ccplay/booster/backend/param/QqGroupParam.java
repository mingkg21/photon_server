package cc.ccplay.booster.backend.param;

import cc.ccplay.booster.base.model.content.QqGroup;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;

public class QqGroupParam {

    public QqGroup toModel(){
        QqGroup group = new QqGroup();
        group.setId(this.id);
        group.setDescription(this.description);
        group.setIcon(this.icon);
        group.setOrdering(this.ordering);
        group.setTitle(this.title);
        group.setGroupNumber(this.groupNumber);
        group.setKey(this.key);
        return group;
    }


    @Param
    private Long id;

    @NotNull
    private String icon;

    @NotNull
    private String title;

    @NotNull
    private String groupNumber;

    @Param
    private String description;

    @NotNull
    private Long ordering;

    @NotNull
    private String key;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(String groupNumber) {
        this.groupNumber = groupNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getOrdering() {
        return ordering;
    }

    public void setOrdering(Long ordering) {
        this.ordering = ordering;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
