package cc.ccplay.booster.backend.web.content;

import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.backend.param.OpenScreenParam;
import cc.ccplay.booster.base.api.content.OpenScreenService;
import cc.ccplay.booster.base.dto.content.OpenScreenDto;
import cc.ccplay.booster.base.model.content.OpenScreen;
import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.enums.Status;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/content/screen")
public class OpenScreenController extends BaseBackendController {

    @Reference
    private OpenScreenService openScreenService;

    @RequestMapping("/index")
    public String index(){
        return "/content/screen/list";
    }

    @RequestMapping("/goAddPage")
    public String goAddPage(){
        return "/content/screen/edit";
    }

    @RequestMapping("/goEditPage")
    public String goEditPage(HttpServletRequest request, @NotNull long id){
        OpenScreenDto dto = openScreenService.getDto(id);
        if(dto == null){
            throw new GenericException("数据不存在");
        }
        request.setAttribute("os",dto.getOpenScreen());
        request.setAttribute("gameInfo",dto.getGameInfo());
        request.setAttribute("versionInfo",dto.getVersionInfo());
        return "/content/screen/edit";
    }


    @ResponseBody
    @RequestMapping("/list")
    public void list(){
        super.renderJson(openScreenService.queryPage(super.getSystemParam()));
    }

    @ResponseBody
    @RequestMapping("/delete")
    public void delete(@NotNull long id){
        openScreenService.delete(id);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/updateStatus")
    public void updateStatus(@NotNull long id,@NotNull long status){
        Status ss = Status.getNotNull(status);
        openScreenService.updateStatus(id,ss);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/saveOrUpdate")
    public void saveOrUpdate(@Param OpenScreenParam openScreenParam){
        OpenScreen os = openScreenParam.toModel();
        openScreenService.saveOrUpdate(os);
        super.renderSuccess();
    }

}
