package cc.ccplay.booster.backend.param;

import cc.ccplay.booster.base.model.content.GameInfo;
import org.easyj.frame.validation.annotation.MaxValue;
import org.easyj.frame.validation.annotation.MinValue;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;

public class SaveGameParam {

    public GameInfo toGameInfo(){
        GameInfo gameInfo = new GameInfo();
        gameInfo.setId(this.id);
        gameInfo.setPackageName(this.packageName);
        gameInfo.setCategoryId(this.categoryId);
        gameInfo.setName(this.name);
        gameInfo.setDescription(this.description);
        gameInfo.setDevRecommend(this.devRecommend);
        gameInfo.setEditorRecommend(this.editorRecommend);
        gameInfo.setNameEn(this.nameEn);
        gameInfo.setPublisherId(this.publisherId);
        gameInfo.setVideo(this.video);
        gameInfo.setVideoRaw(this.videoRaw);
        gameInfo.setBanner(this.banner);
        gameInfo.setCoverImage(this.coverImage);
        return gameInfo;
    }

    @Param
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private Long categoryId;

    @NotNull
    private Long publisherId;

    @NotNull
    private String description;

    @Param
    private String editorRecommend;

    @NotNull
    private String packageName;

    @Param
    private String devRecommend;

    @Param
    private String video;

    @Param
    private String videoRaw;

    @Param
    private String nameEn;

    @Param
    private String banner;

    @Param
    private String coverImage;

    @Param
    @MinValue(0)
    @MaxValue(10)
    private Double score;


    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(Long publisherId) {
        this.publisherId = publisherId;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEditorRecommend() {
        return editorRecommend;
    }

    public void setEditorRecommend(String editorRecommend) {
        this.editorRecommend = editorRecommend;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getDevRecommend() {
        return devRecommend;
    }

    public void setDevRecommend(String devRecommend) {
        this.devRecommend = devRecommend;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getVideoRaw() {
        return videoRaw;
    }

    public void setVideoRaw(String videoRaw) {
        this.videoRaw = videoRaw;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }
}
