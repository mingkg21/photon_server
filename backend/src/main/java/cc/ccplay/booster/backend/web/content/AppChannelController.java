package cc.ccplay.booster.backend.web.content;

import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.api.content.AppChannelService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.SelectizeDto;
import cc.ccplay.booster.base.model.content.AppChannel;
import com.alibaba.dubbo.config.annotation.Reference;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RequestMapping("/content/channel")
@Controller
public class AppChannelController extends BaseBackendController {

    @Reference
    private AppChannelService appChannelService;

    @RequestMapping("/index")
    public String index(){
        return "/content/channel/list";
    }

    @RequestMapping("/list")
    @ResponseBody
    public void getList(){
        Page page = appChannelService.getList(getSystemParam());
        renderJson(page);
    }


    @RequestMapping("/goAddPage")
    public String goAddPage(){
        return "/content/channel/edit";
    }

    @RequestMapping("/goEditPage")
    public String goEditPage(HttpServletRequest request,@NotNull long id){
        SystemParam param = getSystemParam();
        AppChannel channel = appChannelService.getChannel(param,id);
        request.setAttribute("channel",channel);
        return "/content/channel/edit";
    }

    @RequestMapping("/saveOrUpdate")
    @ResponseBody
    public void addChannelManage(@NotNull @Param(remark = "渠道名称") String name,
                        @NotNull @Param(remark = "渠道标识") String code){
        Long id = getLong("id");
        AppChannel user = new AppChannel();
        user.setId(id);
        user.setName(name);
        user.setCode(code);
        appChannelService.saveOrUpdate(getSystemParam(),user);
        renderSuccess();
    }

    @RequestMapping("/getForSelectize")
    @ResponseBody
    public void getForSelectize(@Param String key){
        List<SelectizeDto> list = appChannelService.getForSelectize(super.getSystemParam(),key);
        super.renderJson(list);
    }

}
