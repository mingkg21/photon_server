package cc.ccplay.booster.backend.web.file;

import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.annotation.CheckType;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.annotation.Permission;
import cc.ccplay.booster.base.api.content.BucketFileService;
import cc.ccplay.booster.base.api.content.QiNiuService;
import cc.ccplay.booster.base.dto.QiNiuAuth;
import cc.ccplay.booster.base.model.content.BucketFile;
import cc.ccplay.booster.base.util.UserUtil;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("/file/any")
@Controller
public class BucketFileController extends BaseBackendController {


    @Reference
    private QiNiuService qiNiuService;

    @Reference
    private BucketFileService bucketFileService;


    @Permission(CheckType.OPEN)
    @RequestMapping("/getUploadToken")
    @ResponseBody
    public void getUploadToken(){
        QiNiuAuth auth = qiNiuService.getUploadFileToken(super.getSystemParam());
        super.renderJson(auth);
    }

    @RequestMapping("/index")
    public String index(){
        super.getRequest().setAttribute("loginUser", UserUtil.getCurrentUser());
        return "/file/any/list";
    }

    /**
     * 上传成功之后调用
     */
    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/uploadSuccess")
    public void uploadSuccess(){
        String src = super.getString("key");
        if(src == null){
            super.renderFailure();
            return;
        }
        BucketFile file = new BucketFile();
        file.setCreateUserId(super.getLong("userId"));
        file.setFileName(super.getString("name"));
        file.setSize(super.getLong("size"));
        file.setSrc(src);
        file.setHash(super.getString("hash"));
        bucketFileService.save(super.getSystemParam(),file);
        super.renderSuccess();
    }

    @RequestMapping("/list")
    @ResponseBody
    public void list(@Param String account){
        BucketFile searchObj = new BucketFile();
        searchObj.setAccount(account);
        Page page = bucketFileService.queryPage(super.getSystemParam(),searchObj);
        super.renderJson(page);
    }

    @Permission(CheckType.OPEN)
    @RequestMapping("/bucket")
    public String bucket(HttpServletRequest request){
        request.setAttribute("loginUser", UserUtil.getCurrentUser());
        request.setAttribute("fnId",super.getString("fnId"));
        return "/file/any/bucket";
    }

    @Permission(CheckType.OPEN)
    @RequestMapping("/query")
    @ResponseBody
    public void query(HttpServletRequest request, @Param Boolean showAll){
        BucketFile searchObj =null;
        if(!showAll) {
            searchObj = new BucketFile();
            searchObj.setCreateUserId(UserUtil.getUserId());
        }
        Page page = bucketFileService.queryPage(super.getSystemParam(),searchObj);
        super.renderJson(page);
    }
}
