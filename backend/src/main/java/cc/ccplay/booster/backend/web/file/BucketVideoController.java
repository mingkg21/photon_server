package cc.ccplay.booster.backend.web.file;

import cc.ccplay.booster.base.api.content.BucketVideoService;
import cc.ccplay.booster.base.dto.QiNiuAuth;
import cc.ccplay.booster.base.model.content.BucketVideo;
import cc.ccplay.booster.base.util.UserUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.annotation.CheckType;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.annotation.Permission;
import cc.ccplay.booster.base.api.content.QiNiuService;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("/file/video")
@Controller
public class BucketVideoController extends BaseBackendController {

    @Reference
    private QiNiuService qiNiuService;

    @Reference
    private BucketVideoService bucketVideoService;

    @Permission(CheckType.OPEN)
    @RequestMapping("/getUploadToken")
    @ResponseBody
    public void getUploadToken(){
        QiNiuAuth auth = qiNiuService.getUploadVideoToken(super.getSystemParam());
        super.renderJson(auth);
    }

    @RequestMapping("/index")
    public String index(){
        super.getRequest().setAttribute("loginUser", UserUtil.getCurrentUser());
        return "/file/video/list";
    }

    /**
     * 上传成功之后调用
     */
    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/uploadSuccess")
    public void uploadSuccess(){
        String src = super.getString("key");
        if(src == null){
            super.renderFailure();
            return;
        }
        BucketVideo video = new BucketVideo();
        video.setCreateUserId(super.getLong("userId"));
        video.setFileName(super.getString("name"));
        video.setSize(super.getLong("size"));
        video.setSrc(src);
        video.setHash(super.getString("hash"));
        video.setType(super.getString("type"));
        video.setM3u8(super.getString("m3u8"));
        bucketVideoService.save(super.getSystemParam(),video);
        super.renderSuccess();
    }

    @RequestMapping("/list")
    @ResponseBody
    public void list(@Param String account){
        BucketVideo searchObj = new BucketVideo();
        searchObj.setAccount(account);
        Page page = bucketVideoService.queryPage(super.getSystemParam(),searchObj);
        super.renderJson(page);
    }

    @Permission(CheckType.OPEN)
    @RequestMapping("/bucket")
    public String bucket(HttpServletRequest request){
        request.setAttribute("loginUser",UserUtil.getCurrentUser());
        request.setAttribute("fnId",super.getString("fnId"));
        return "/file/video/bucket";
    }

    @Permission(CheckType.OPEN)
    @RequestMapping("/query")
    @ResponseBody
    public void query(HttpServletRequest request,@Param Boolean showAll){
        BucketVideo searchObj =null;
        if(!showAll) {
            searchObj = new BucketVideo();
            searchObj.setCreateUserId(UserUtil.getUserId());
        }
        Page page = bucketVideoService.queryPage(super.getSystemParam(),searchObj);
        super.renderJson(page);
    }
}
