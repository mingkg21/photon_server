package cc.ccplay.booster.backend.web.log;


import cc.ccplay.booster.base.annotation.SortMapping;
import cc.ccplay.booster.base.api.log.AppDayCountService;
import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.reader.GameInfoReader;
import cc.ccplay.booster.base.util.DateTimeUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Controller
@RequestMapping("/log/appDownload")
public class AppDownloadController extends BaseBackendController {

    @Reference
    private AppDayCountService appDayCountService;

    @Autowired
    private GameInfoReader gameInfoReader;

    @RequestMapping("/index")
    public String index(HttpServletRequest request){
        Date now = new Date();
        request.setAttribute("startDate", DateTimeUtil.formatDate(now));
        return "/log/appDownload/list";
    }

    @RequestMapping("/list")
    @ResponseBody
    @SortMapping(mapping = {"downloadCount","download_count","downloadCompleteCount","download_complete_count",
            "countDay","count_day","completePercent","complete_percent"})
    public void list(@Param Date startDate,@Param Date endDate,@Param Long appId){
        Page page = appDayCountService.getPage(super.getSystemParam(),startDate,endDate,appId);
        gameInfoReader.readAnSetGameInfo(page,"appId");
        super.renderJson(page);
    }

}
