package cc.ccplay.booster.backend.core;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cc.ccplay.booster.backend.web.PermissionManager;
import cc.ccplay.booster.base.annotation.CheckType;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.annotation.SortMapping;
import cc.ccplay.booster.base.util.UserUtil;
import cc.ccplay.booster.base.annotation.Permission;
import org.easyj.frame.exception.NoLoginException;
import org.easyj.frame.exception.PermissionCheckException;
import org.easyj.frame.util.StringUtil;
import org.easyj.frame.util.SysUtil;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import java.util.Map;


public class PermissionInterceptor extends HandlerInterceptorAdapter {
	

	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {

	}

	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object arg2, ModelAndView view)
			throws Exception {

	}

	private void createPaggingSortParamter(SortMapping sm, HttpServletRequest request){
		String mapping[] = sm.mapping();
		String sortType = request.getParameter("sortOrder");
		String sortName = request.getParameter("sortName");
		String defColumn = sm.defColumn();
		String defSortType = sm.defSortType();
		if(StringUtil.isEmpty(sortName)){//前端未传参数过来，使用默认排序字段
			if(StringUtil.isNotEmpty(defColumn)){
				UserUtil.setSortInfo(defColumn, defSortType);
			}
		}else{
			int size = mapping.length / 2;
			for (int i = 0; i < size ; i++) {
				String sn = mapping[i * 2];
				if(org.apache.commons.lang3.StringUtils.equals(sn, sortName)){
					UserUtil.setSortInfo(mapping[i * 2 + 1], sortType);
					break;
				}
			}
		}
	}
	
	
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		request.setAttribute("base",request.getContextPath());

		// 处理Permission Annotation，实现方法级权限控制
		HandlerMethod method = (HandlerMethod) handler;

		//创建分页参数
		SortMapping sm = method.getMethodAnnotation(SortMapping.class);
		if(sm != null){
			createPaggingSortParamter(sm,request);
		}
		// 需登陆才能访问
		NotNeedLogin nnl = method.getMethodAnnotation(NotNeedLogin.class);
		if (nnl != null) {
			return true;
		}

		// 如果为空在表示该方法需要进行登陆验证
		if (!SysUtil.isLogin()) {
			throw new NoLoginException();
		}
		//判断权限
		checkPermission(request,method);
		return true;
	}


	/**
	 * 检测总控后台权限
	 *
	 * @param request
	 */
	private void checkPermission(HttpServletRequest request, HandlerMethod method) throws ClassNotFoundException {
		String contentPath = request.getContextPath();
		String path = request.getRequestURI();
		if (!contentPath.equals("/")) { // 去除项目名
			path = path.substring(contentPath.length());
		}
		Permission p = method.getMethodAnnotation(Permission.class);
		String key = null;

		// 若没有Permission标签，则默认该方法只要有模块权限即可访问
		CheckType ct = (p == null) ? CheckType.MODULE : p.value();

		// 功能暂停使用
		if (ct == CheckType.CLOSE) {
			throw new PermissionCheckException("该功能已经暂停使用!");
		}

		// 开放权限，无需验证
		if (ct == CheckType.OPEN) {
			return;
		}

		if (ct == CheckType.MODULE) {
			key = toModuleURL(path);
		} else if (ct == CheckType.FUNCTION) {
			key = toContentURL(path);
		}

		Map<String, Long> pm = PermissionManager.getPermissionMap();
		if (!pm.containsKey(key)) {
			throw new PermissionCheckException("你没有该项操作的权限，请与管理员联系!");
		}
	}

	private String toContentURL(String url) {
		int index = url.indexOf("?");
		if (index == -1) {
			return url;
		}
		return url.substring(0, index);
	}

	private String toModuleURL(String url) {
		url = toContentURL(url);
		int index = url.lastIndexOf("/");
		return url.substring(0, index);
	}
}
