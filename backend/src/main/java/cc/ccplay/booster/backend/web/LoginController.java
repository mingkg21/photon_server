package cc.ccplay.booster.backend.web;

import cc.ccplay.booster.backend.core.Const;
import cc.ccplay.booster.base.annotation.CheckType;
import cc.ccplay.booster.base.api.user.SysUserService;
import cc.ccplay.booster.base.model.user.SysUser;
import cc.ccplay.booster.base.util.UserUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.annotation.Permission;
import cc.ccplay.booster.base.api.content.QiNiuService;
import cc.ccplay.booster.base.core.SystemParam;
import org.easyj.frame.validation.annotation.MinLength;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class LoginController extends BaseBackendController{

    @Reference
    private SysUserService sysUserService;

    @Reference
    private QiNiuService qiNiuService;

    @Value("${cdn.server}")
    private String cdnServer;

    @Value("${domain.wap}")
    private String doaminWap;

    @NotNeedLogin
    @RequestMapping({"/","/index"})
    public String goLoginPage(HttpServletRequest request){
        if(UserUtil.isLogin()){
            SystemParam param = super.getSystemParam();
            request.setAttribute("cdn",cdnServer);
            request.setAttribute("doaminWap",doaminWap);
            request.setAttribute("loginUser",UserUtil.getCurrentUser());
            return "/home";
        }
        return "/index";
    }

    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/login")
    public void doLogin(@NotNull @Param(remark = "账号") String account, @NotNull @Param(value="password",remark = "密码") String pwd){
        SystemParam param = getSystemParam();
        SysUser user = sysUserService.doLogin(param, account, pwd);
        getSession().setAttribute(Const.SESSION_USER_KEY,user);
        renderSuccess();
    }

    @NotNeedLogin
    @RequestMapping("/loginWindow")
    public String loginWindow(){
        return "/loginDialog";
    }

    @NotNeedLogin
    @RequestMapping("/logout")
    public String logout(){
        HttpSession session =  getSession();
        session.invalidate();
        return "/index";
    }

    @Permission(CheckType.OPEN)
    @RequestMapping("/modifyPwd")
    public String modifyPwd(){
        getRequest().setAttribute("loginUser",UserUtil.getCurrentUser());
        return "/modifyPwd";
    }

    @Permission(CheckType.OPEN)
    @ResponseBody
    @RequestMapping("/updpwd")
    public void updpwd(@NotNull String oldPassword,@NotNull @MinLength(6) String newPassword){
        sysUserService.updatePassword(getSystemParam(),UserUtil.getUserId(),oldPassword,newPassword);
        renderSuccess();
    }

}
