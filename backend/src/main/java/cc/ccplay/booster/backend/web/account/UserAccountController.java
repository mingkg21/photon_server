package cc.ccplay.booster.backend.web.account;

import cc.ccplay.booster.base.api.user.RegisterService;
import cc.ccplay.booster.base.api.user.UserAccountService;
import cc.ccplay.booster.base.api.user.UserFlowService;
import cc.ccplay.booster.base.dto.content.booster.AddUserFlowDto;
import cc.ccplay.booster.base.enums.AddUserFlowType;
import cc.ccplay.booster.base.model.user.UserAccount;
import cc.ccplay.booster.base.reader.dto.UserInfo;
import cc.ccplay.booster.backend.web.BaseBackendController;
import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.backend.param.SaveUserAccount;
import cc.ccplay.booster.base.annotation.CheckType;
import cc.ccplay.booster.base.annotation.Permission;
import cc.ccplay.booster.base.api.user.UserAccountPermissionService;
import cc.ccplay.booster.base.dto.user.UserAccountPermissionDto;
import cc.ccplay.booster.base.model.user.UserAccountPermission;
import cc.ccplay.booster.base.model.user.UserProfile;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/account/info")
public class UserAccountController extends BaseBackendController {

    @Reference
    private UserAccountService userAccountService;

    @Reference
    private RegisterService registerService;

    @Reference
    private UserAccountPermissionService userAccountPermissionService;


    @Reference
    private UserFlowService userFlowService;

    @RequestMapping("/index")
    public String index(){
        return "/account/info/list";
    }

    @RequestMapping("/goAddPage")
    public String goAddPage(){
        return "/account/info/edit";
    }

    @RequestMapping("/goEditPage")
    public String goEditPage(HttpServletRequest request, @NotNull long id){
        UserInfo userInfo = userAccountService.getUserInfo(id);
        if(userInfo == null){
            throw new GenericException("用户信息不存在");
        }
        request.setAttribute("userInfo",userInfo.getUserAccount());
        request.setAttribute("userProfileInfo",userInfo.getUserProfile());
        return "/account/info/edit";
    }

    @ResponseBody
    @RequestMapping("/list")
    public void list(@Param String phone,@Param String nickName,@Param Long id,@Param Long registerType){
        UserAccount searchModel = new UserAccount();
        searchModel.setPhone(phone);
        searchModel.setId(id);
        searchModel.setNickName(nickName);
        searchModel.setRegisterType(registerType);
        Page page = userAccountService.getPage(super.getSystemParam(),searchModel);
        super.renderJson(page);
    }

    @ResponseBody
    @RequestMapping("/saveOrUpdate")
    public void saveOrUpdate(@Param SaveUserAccount saveUserAccount){
        UserAccount userAccount = saveUserAccount.getUserAccount();
        UserProfile userProfile = saveUserAccount.getUserProfile();
        registerService.registerOrUpdate(super.getSystemParam(),userAccount,userProfile);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/resetPassword")
    public void resetPassword(@NotNull long userId){
        registerService.resetPassword(super.getSystemParam(),userId);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/updateStatus")
    public void updateStatus(@NotNull long id,@NotNull String status){
        if(!UserAccount.STATUS_ACTIVITY.equals(status)  && !UserAccount.STATUS_LOCKED.equals(status)){
            throw new GenericException("status值["+status+"]不是一个有效值");
        }
        userAccountService.updateStatus(super.getSystemParam(),id,status);
        super.renderSuccess();
    }

    @Permission(CheckType.OPEN)
    @ResponseBody
    @RequestMapping("/list4Select")
    public void list4Select(@Param String phone,@Param String nickName,@Param Long id,@Param Integer fakeFlag){
        UserAccount searchModel = new UserAccount();
        searchModel.setPhone(phone);
        searchModel.setId(id);
        searchModel.setNickName(nickName);
        searchModel.setFakeFlag(fakeFlag);
        searchModel.setStatus(UserAccount.STATUS_ACTIVITY);
        Page page = userAccountService.getPage(super.getSystemParam(),searchModel);
        super.renderJson(page);
    }


    @Permission(CheckType.OPEN)
    @RequestMapping("/bucket")
    public String bucket(HttpServletRequest request,@NotNull String fnId){
        request.setAttribute("fnId",fnId);
        return "/account/info/bucket";
    }


    @RequestMapping("/goEditPermissionPage")
    public String goEditPermissionPage(HttpServletRequest request, @NotNull long id){

        UserAccountPermissionDto dto = userAccountPermissionService.getDto(id);
        UserInfo userInfo = dto.getUserInfo();
        if(userInfo == null){
            throw new GenericException("用户信息不存在");
        }
        request.setAttribute("userInfo",userInfo.getUserAccount());
        request.setAttribute("userProfileInfo",userInfo.getUserProfile());
        request.setAttribute("uap",dto.getUserAccountPermission());
        return "/account/info/permission";
    }

    @ResponseBody
    @RequestMapping("/savePermission")
    public void savePermission(@NotNull long id,@Param Long gameComment,@Param Long saveRecordComment){
        UserAccountPermission uap = new UserAccountPermission();
        uap.setUserId(id);
        uap.setAllowGameComment(gameComment == null || gameComment.intValue() != 1 ? 0L : 1L);
        uap.setAllowSaveRecordComment(saveRecordComment == null || saveRecordComment.intValue() != 1 ? 0L : 1L);
        userAccountPermissionService.saveOrUpdate(uap);
        super.renderSuccess();
    }


    @RequestMapping("/goAddFlowPage")
    public String goAddFlowPage(HttpServletRequest request,@NotNull long userId){
        request.setAttribute("userId",userId);
        return "/account/info/addFlow";
    }

    @ResponseBody
    @RequestMapping("/addFlow")
    public void addFlow(@NotNull long id,@NotNull String unit,@NotNull Long flow,@Param String remark){

        if(flow.longValue() <= 0){
            super.renderFailure();
        }

        long total = flow.longValue();

        if("MB".equals(unit)){
            total = flow * 1024 * 1024;
        }else if("GB".equals(unit)){
            total = flow * 1024 * 1024 * 1024;
        }

        AddUserFlowDto dto = new AddUserFlowDto();
        dto.setType(AddUserFlowType.BACKEND_ADD);
        dto.setUserId(id);
        dto.setRemark(remark);
        dto.setTotal(total);
        //dto.setTotal();
        userFlowService.addFlow(dto);
        super.renderSuccess();
    }
}
