package cc.ccplay.booster.backend.web.file;

import cc.ccplay.booster.base.api.user.SysUserService;
import cc.ccplay.booster.backend.param.QiniuFileParam;
import cc.ccplay.booster.backend.web.BaseBackendController;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.api.content.QiNiuService;
import cc.ccplay.booster.base.model.content.BaseFile;
import cc.ccplay.booster.base.model.user.SysUser;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/qiniu/tool")
public class QiniuUploadToolController extends BaseBackendController {

    @Value("${cdn.server}")
    private String cdnServer;

    @Value("${cdn.accessKey}")
    private String cdnAccessKey;

    @Value("${cdn.secretKey}")
    private String cdnSecretKey;

    @Value("${cdn.defaultBucketName}")
    private String cdnBucket;

    @Value("${cdn.base.url}")
    private String deployMode;

    @Reference
    private SysUserService sysUserService;

    @Reference
    private QiNiuService qiNiuService;

    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/doLoginForUploader")
    public void doLoginForUploader(@NotNull String account, @NotNull String pwd, @NotNull boolean useMd5){
        SysUser user = sysUserService.doLoginForUploader(account,pwd,useMd5);
        JSONObject retJson = new JSONObject();
        retJson.put("id",user.getId());
        retJson.put("account",user.getAccount());
        retJson.put("sign",user.getPassword());

        retJson.put("cdnServer",cdnServer);
        retJson.put("cdnAccessKey",cdnAccessKey);
        retJson.put("cdnBucket",cdnBucket);
        retJson.put("deployMode",deployMode);
        retJson.put("cdnSecretKey",cdnSecretKey);
        super.renderJson(retJson);
    }


    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/uploadSuccess")
    public void uploadSuccess(@Param QiniuFileParam param){
        BaseFile baseFile = param.toModel();
        qiNiuService.saveToolUploadFile(baseFile);
        super.renderSuccess();
    }
}
