package cc.ccplay.booster.backend.param;

import cc.ccplay.booster.base.model.user.UserAccount;
import cc.ccplay.booster.base.model.user.UserProfile;
import org.easyj.frame.util.StringUtil;
import org.easyj.frame.validation.annotation.MaxValue;
import org.easyj.frame.validation.annotation.MinValue;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;

import java.util.Date;

public class SaveUserAccount {

    public UserAccount getUserAccount(){
        UserAccount account = new UserAccount();
        account.setId(id);
        if(StringUtil.isNotEmpty(phone)) {
            account.setPhone(phone);
        }
        account.setNickName(this.nickName);
        account.setFakeFlag(this.fakeFlag);
        return account;
    }

    public UserProfile getUserProfile(){
        UserProfile userProfile = new UserProfile();
        userProfile.setHeadIcon(this.headIcon);
        userProfile.setArea(this.area);
        userProfile.setSex(this.sex);
        userProfile.setSignature(this.signature);
        userProfile.setBirthday(this.birthday);
        userProfile.setUserId(this.id);
        return userProfile;
    }

    @Param
    private Long id;

    @Param
    @NotNull
    private String headIcon;

    @Param
    private String area;

    @Param
    private Date birthday;

    @Param
    private String signature;

    @Param
    private String phone;

    @NotNull
    @MinValue(0)
    @MaxValue(1)
    private Integer fakeFlag;

    @NotNull
    @MinValue(value = 0)
    @MaxValue(value = 2)
    private Integer sex;

    @NotNull
    private String nickName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHeadIcon() {
        return headIcon;
    }

    public void setHeadIcon(String headIcon) {
        this.headIcon = headIcon;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getFakeFlag() {
        return fakeFlag;
    }

    public void setFakeFlag(Integer fakeFlag) {
        this.fakeFlag = fakeFlag;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
}
