package cc.ccplay.booster.backend.web.content;

import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.api.user.UserSystemMessageService;
import cc.ccplay.booster.base.model.user.UserSystemMessage;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import cc.ccplay.booster.backend.web.save.MsgContentItem;
import org.easyj.frame.util.StringUtil;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping("/content/message")
public class SendMessageController extends BaseBackendController {

    @Reference
    private UserSystemMessageService userSystemMessageService;

    @RequestMapping("/index")
    public String index(){
        return "/content/message/sendMsg";
    }

    @RequestMapping("/goAddItemPage")
    public String goAddItemPage(HttpServletRequest request,
                                @NotNull String tableId,
                                @NotNull String editFormId,
                                @Param Long msgTagType){

        if(msgTagType != null){
            request.setAttribute("msgTagType",msgTagType);
        }
        request.setAttribute("editFormId",editFormId);
        request.setAttribute("tableId",tableId);
        return "/content/message/addItem";
    }

    @RequestMapping("/sendMsg")
    @ResponseBody
    public void sendMsg(@NotNull String contentArrJson, @NotNull String title,@NotNull
            Long userId, @NotNull long contentType,
                       @Param Long objectId , @Param String objectTitle){

        List<MsgContentItem> items = JSONArray.parseArray(contentArrJson, MsgContentItem.class);
        Collections.sort(items);

        StringBuffer contentBuffer = new StringBuffer();
        int size = items.size();
        for (int i = 0; i < size; i++) {
            MsgContentItem item = items.get(i);
            String value = item.getValue();
            if(value != null) {
                contentBuffer.append(value);
            }
        }

        JSONObject pushMsg = new JSONObject();
        pushMsg.put("content",items);
        pushMsg.put("msgContent",contentBuffer.toString());
        pushMsg.put("msgTitle",title);


        JSONArray titleArr = new JSONArray();
        JSONObject titleItem = new JSONObject();
        titleItem.put("type","1");
        titleItem.put("value",title);
        titleArr.add(titleItem);
        pushMsg.put("title",titleArr);
        if(objectId != null) {
            pushMsg.put("objectId", objectId);
        }
        if(StringUtil.isNotEmpty(objectTitle)){
            pushMsg.put("objectTitle", objectTitle);
        }
        pushMsg.put("contentType", contentType);

        UserSystemMessage msg = new UserSystemMessage();
        msg.setContentType(contentType);
        msg.setMsgTitle(title);
        msg.setMsgContent(contentBuffer.toString());
        msg.setObjectTitle(objectTitle);
        msg.setObjectId(objectId);
        msg.setMsgType(UserSystemMessage.MSG_TYPE_SYSTEM);

        if(userId.longValue() == 0){
            msg.setPushMsg(pushMsg.toJSONString());
            userSystemMessageService.pushMsg2All(msg);
        }else{
            msg.setUserId(userId);
            pushMsg.put("userId",userId);
            msg.setPushMsg(pushMsg.toJSONString());
            userSystemMessageService.pushMsg2One(msg);
        }
        super.renderSuccess();
    }

//    {"msgType":1,"msgContent":"嘎嘎嘎","msgTitle":"Jay 回复你的在《2222》评论",
//            "objectTitle":"回复ID",
//
//            "title":[{"type":1,"value":"有人回复了你的消息"}],
//
//        "contentType":1,"userId":66690,"content":
//        [{"id":66699,"type":4,"value":"Jay "},
//        {"type":1,"value":"回复你在"},{"id":5,"type":3,"value":"《2222》"},
//        {"type":1,"value":"的游戏评论."},{"type":1,"value":"\r\n评论内容:嘎嘎嘎"}]}

}
