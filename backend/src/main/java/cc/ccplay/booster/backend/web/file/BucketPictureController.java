package cc.ccplay.booster.backend.web.file;

import cc.ccplay.booster.base.api.content.BucketImageService;
import cc.ccplay.booster.backend.web.BaseBackendController;
import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.annotation.CheckType;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.annotation.Permission;
import cc.ccplay.booster.base.api.content.QiNiuService;
import cc.ccplay.booster.base.dto.QiNiuAuth;
import cc.ccplay.booster.base.model.content.BucketImage;
import cc.ccplay.booster.base.util.UserUtil;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("/file/picture")
@Controller
public class BucketPictureController extends BaseBackendController {

    @Reference
    private QiNiuService qiNiuService;

    @Reference
    private BucketImageService bucketImageService;

    @Permission(CheckType.OPEN)
    @RequestMapping("/bucket")
    public String bucket(HttpServletRequest request) {
        request.setAttribute("loginUser", UserUtil.getCurrentUser());
        request.setAttribute("fnId", super.getString("fnId"));
        request.setAttribute("mulSelect", "true".equals(super.getString("mulSelect")));
        return "/file/picture/bucket";
    }

    @Permission(CheckType.OPEN)
    @RequestMapping("/query")
    @ResponseBody
    public void query(HttpServletRequest request, @Param Boolean showAll, @Param String name) {
        BucketImage searchObj = null;
        if (!showAll) {
            searchObj = new BucketImage();
            searchObj.setCreateUserId(UserUtil.getUserId());
        }
        Page page = bucketImageService.queryPage(super.getSystemParam(), searchObj, name);
        super.renderJson(page);
    }

    @RequestMapping("/list")
    @ResponseBody
    public void list(@Param String account) {
        BucketImage searchObj = new BucketImage();
        searchObj.setAccount(account);
        Page page = bucketImageService.queryPage(super.getSystemParam(), searchObj, null);
        super.renderJson(page);
    }

    @Permission(CheckType.OPEN)
    @RequestMapping("/getUploadToken")
    @ResponseBody
    public void getUploadToken() {
        QiNiuAuth auth = qiNiuService.getUploadImageToken(super.getSystemParam());
        super.renderJson(auth);
    }

    /**
     * 上传成功之后调用
     */
    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/uploadSuccess")
    public void uploadSuccess() {
        String src = super.getString("key");
        if (src == null) {
            super.renderFailure();
            return;
        }
        BucketImage image = new BucketImage();
        image.setCreateUserId(super.getLong("userId"));
        image.setFileName(super.getString("name"));
        image.setHeight(super.getLong("height"));
        image.setWidth(super.getLong("width"));
        image.setSize(super.getLong("size"));
        image.setSrc(src);
        image.setHash(super.getString("hash"));
        image.setType(super.getString("type"));
        bucketImageService.save(super.getSystemParam(), image);
        super.renderSuccess();
    }

    @RequestMapping("/index")
    public String index() {
        super.getRequest().setAttribute("loginUser", UserUtil.getCurrentUser());
        return "/file/picture/list";
    }
}
