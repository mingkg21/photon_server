package cc.ccplay.booster.backend.web.content;

import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.api.content.SuggestionFeedbackService;
import cc.ccplay.booster.base.model.content.SuggestionFeedback;
import com.alibaba.dubbo.config.annotation.Reference;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/content/suggestion")
public class SuggestionFeedbackController extends BaseBackendController {

    @Reference
    private SuggestionFeedbackService suggestionFeedbackService;

    @RequestMapping("/index")
    public String index(){
        return "/content/suggestion/list";
    }

    @RequestMapping("/goReplyPage")
    public String goReplyPage(HttpServletRequest request,@NotNull long userId){
        request.setAttribute("userId",userId);
        return "/content/suggestion/reply";
    }


    @ResponseBody
    @RequestMapping("/list")
    public void list(@Param Long userId){
       Page page = suggestionFeedbackService.getPage(super.getSystemParam(),userId);
       super.renderJson(page);
    }

    @ResponseBody
    @RequestMapping("/reply")
    public void reply(@NotNull long userId,@NotNull String content){
        SuggestionFeedback feedback = new SuggestionFeedback();
        feedback.setIsReply(1L);
        feedback.setContent(content);
        feedback.setUserId(userId);
        suggestionFeedbackService.reply(feedback);
        super.renderSuccess();
    }

}
