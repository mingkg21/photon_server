package cc.ccplay.booster.backend.job;

import cc.ccplay.booster.base.api.common.SmsService;
import cc.ccplay.booster.base.api.content.BoosterApiService;
import cc.ccplay.booster.base.api.content.BoosterServerOnlineService;
import cc.ccplay.booster.base.api.log.BoosterLoginLogService;
import cc.ccplay.booster.base.api.log.BoosterServerStatusService;
import cc.ccplay.booster.base.dto.log.BoosterSuccessLoginDto;
import cc.ccplay.booster.base.model.content.BoosterServer;
import cc.ccplay.booster.base.model.log.BoosterServerStatus;
import cc.ccplay.booster.base.util.DateTimeUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import org.easyj.frame.util.Validator;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public class BoosterOnlineJob {

    @Reference
    private static BoosterApiService boosterApiService;

    @Reference
    private static BoosterServerOnlineService boosterServerOnlineService;

    @Reference
    private static BoosterServerStatusService boosterServerStatusService;

    @Reference
    private static BoosterLoginLogService boosterLoginLogService;

    @Reference
    private static SmsService smsService;

    //两分钟执行一次
    @Scheduled(cron = "0 0/2 * * * ?")
    public void clearOfflinePort(){
        boosterApiService.clearOfflinePort();
    }

    //三分钟执行一次
    @Scheduled(cron = "0 0/3 * * * ?")
    public void clearOnlineUser(){
        boosterApiService.clearOfflineUser();
    }

    //5分钟执行一次
    @Scheduled(cron = "0 0/5 * * * ?")
    public void logServerStatus(){
        List<BoosterServerStatus> ss = boosterServerOnlineService.getAllServerStatus();
        if(ss.size() == 0){
            return;
        }
        boosterServerStatusService.batchSave(ss);
    }

    @Scheduled(cron = "0 0/10 * * * ?")
    public void telnetServerPort(){
        int calSize = 50;
        List<BoosterServer> servers = boosterApiService.getAllServer();
        int size = servers.size();
        for (int i = 0; i < size; i++) {
            BoosterServer server = servers.get(i);
            BoosterSuccessLoginDto dto = boosterLoginLogService.getSuccessCount(server.getId(),calSize);
            if(Validator.isNull(dto.getTotalCount())){
                continue;
            }
            BigDecimal successPercent = (new BigDecimal(dto.getSuccessCount()).divide(new BigDecimal(dto.getTotalCount()))).setScale(2, BigDecimal.ROUND_DOWN);
            double sp = successPercent.doubleValue();
            long waring = sp > 0.5 ? 0 : 1;
            boolean flag = boosterApiService.updateSuccessPercent(server.getId(),sp,waring);
            Long oldWarning = server.getFailWarning();
            if(dto.getTotalCount() == calSize && flag  && oldWarning != null){
                if(oldWarning == 1 && waring == 0){ //成功率从异常变成正常
                    smsService.sendRecoveryNotice(server.getName(),server.getIp(), DateTimeUtil.getNow(),(sp * 100)+"");
                }else if(oldWarning == 0 && waring == 1){//成功率从正常变成异常
                    smsService.sendWarningNotice(server.getName(),server.getIp(), DateTimeUtil.getNow(),(sp * 100)+"");
                }
            }
        }
    }

}
