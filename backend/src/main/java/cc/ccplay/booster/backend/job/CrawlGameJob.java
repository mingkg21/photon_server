package cc.ccplay.booster.backend.job;

import cc.ccplay.booster.base.api.content.CrawlGameService;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;

@Repository
public class CrawlGameJob {

    /**
     * 必须要加static,要不然会出现空指针
     */
    @Reference
    private static CrawlGameService crawlGameService;

    /* *
    凌晨3点
  * 汇总前一天的数据
  */
    @Scheduled(cron = "0 0 3 * * ?")
    public void updateCrawlGame(){
        crawlGameService.batchUpdateCralGame();
    }


}
