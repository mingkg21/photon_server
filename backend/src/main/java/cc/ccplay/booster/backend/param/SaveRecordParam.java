package cc.ccplay.booster.backend.param;

import cc.ccplay.booster.base.model.content.SaveRecord;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;

public class SaveRecordParam {

    public SaveRecord toModel(){
        SaveRecord record = new SaveRecord();
        record.setId(this.id);
        record.setGameId(this.gameId);
        record.setName(this.name);
        record.setFileSize(this.fileSize);
        record.setUrl(this.url);
        record.setVersionCode(this.versionCode);
        record.setVersionName(this.versionName);
        record.setRemark(this.remark);
        record.setAdaptation(this.adaptation);
        return record;
    }

    @Param
    private Long id;

    @NotNull
    private Long gameId;

    @NotNull
    private String name;

    @NotNull
    private Long fileSize;

    @NotNull
    private String url;

    @NotNull
    private Long versionCode;

    @NotNull
    private String versionName;

    @Param
    private String remark;

    @Param
    private String adaptation;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAdaptation() {
        return adaptation;
    }

    public void setAdaptation(String adaptation) {
        this.adaptation = adaptation;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(Long versionCode) {
        this.versionCode = versionCode;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }
}
