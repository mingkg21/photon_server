package cc.ccplay.booster.backend.web.log;


import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.api.log.BoosterServerStatusService;
import cc.ccplay.booster.base.util.DateTimeUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import org.apache.commons.lang3.time.DateUtils;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.validation.annotation.DateFormat;
import org.easyj.frame.validation.annotation.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Controller
@RequestMapping("/log/boosterServerStatus")
public class BoosterServerStatusController extends BaseBackendController {

    @Reference
    private BoosterServerStatusService boosterServerStatusService;

    @RequestMapping("/index")
    public String index(HttpServletRequest request){
        Date now = new Date();
        request.setAttribute("startDate", DateTimeUtil.formatDate(now));
        request.setAttribute("endDate", DateTimeUtil.formatDate(now));
        return "/log/boosterServerStatus/online";
    }

    @ResponseBody
    @RequestMapping("/getReport")
    public void getReport(@NotNull @DateFormat Date startDate,@NotNull @DateFormat Date endDate){
        long startTime = startDate.getTime();
        long endTime = endDate.getTime();

        if(startTime > endTime){
            throw new GenericException("开始时间不能大于结束时间");
        }

        if(endTime - startTime > 30 * 24 * 60 * 60 * 1000L){
            throw new GenericException("最多只能查询31天的记录");
        }

        if(startTime == endTime){
            super.renderJson(boosterServerStatusService.getHourReport(startDate));
        }else{
            super.renderJson(boosterServerStatusService.getDayReport(startDate,DateUtils.addDays(endDate,1)));
        }
    }
}
