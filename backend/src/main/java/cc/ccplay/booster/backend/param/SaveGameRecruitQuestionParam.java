package cc.ccplay.booster.backend.param;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import cc.ccplay.booster.base.model.content.GameRecruitAnswer;
import cc.ccplay.booster.base.model.content.GameRecruitQuestion;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.exception.ParameterException;
import org.easyj.frame.util.StringUtil;
import org.easyj.frame.validation.annotation.MaxLength;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;

import java.util.ArrayList;
import java.util.List;

public class SaveGameRecruitQuestionParam {

    public GameRecruitQuestion getGameRecruitQuestion(){
        GameRecruitQuestion question = new GameRecruitQuestion();
        question.setId(this.questionId);
        question.setQuestion(this.question);
        question.setRecruitId(this.recruitId);
        return question;
    }

    public List<GameRecruitAnswer> getGameRecruitAnswers(){
        JSONArray array = null;
        try{
            array = JSONArray.parseArray(this.answersJson);
            int size = array.size();
            if(size < 2){
                throw new GenericException("答案个数不能少于2");
            }
            List<GameRecruitAnswer> as = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                JSONObject obj = array.getJSONObject(i);
                GameRecruitAnswer answer = new GameRecruitAnswer();
                String a = obj.getString("answer");
                if(StringUtil.isEmpty(a)){
                    throw new GenericException("请填写第"+(i+1)+"答案");
                }
                answer.setAnswer(a);
                answer.setQuestionId(this.questionId);
                answer.setId(obj.getLong("id"));
                as.add(answer);
            }
            return as;
        }catch (JSONException e){
            e.printStackTrace();
            throw new ParameterException("参数[answersJson]数据格式有误");
        }
    }


    @Param
    private Long questionId;

    @NotNull
    private Long recruitId;

    @NotNull
    @MaxLength(500)
    private String question;

    @NotNull
    private String answersJson;

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public Long getRecruitId() {
        return recruitId;
    }

    public void setRecruitId(Long recruitId) {
        this.recruitId = recruitId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswersJson() {
        return answersJson;
    }

    public void setAnswersJson(String answersJson) {
        this.answersJson = answersJson;
    }
}
