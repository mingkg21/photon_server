package cc.ccplay.booster.backend.web.system;

import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.backend.core.CacheKey;
import cc.ccplay.booster.backend.core.MenuNode;
import cc.ccplay.booster.base.annotation.CheckType;
import cc.ccplay.booster.base.annotation.Permission;
import cc.ccplay.booster.base.api.user.SysMenuService;
import cc.ccplay.booster.base.model.user.SysMenu;
import cc.ccplay.booster.base.model.user.SysUser;
import cc.ccplay.booster.base.util.UserUtil;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.JsonUtil;
import org.easyj.frame.validation.annotation.NotNull;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.concurrent.TimeUnit;

@RequestMapping("/system/menu")
@Controller
public class SysMenuController extends BaseBackendController {

    @Reference
    public static SysMenuService sysMenuService;

    @Resource(name="redisTemplate")
    public ValueOperations<String,String> valOpt;

    @Permission(CheckType.OPEN)
    @ResponseBody
    @RequestMapping("/getMenu")
    public void getLoginMenu(){
        SysUser user = (SysUser) UserUtil.getCurrentUser();
        Long roleId = user.getRoleId();
        boolean isRoot = false;
        if("root".equalsIgnoreCase(user.getAccount())){
            roleId = 0L;
            isRoot = true;
        }

        if(roleId == null && isRoot == false){
            throw new GenericException("当前用户未分配角色,无任何权限");
        }

        String key = CacheKey.BACKEND_MENU.getKey(roleId);
        String json = valOpt.get(key);
        if(json == null){
            List<SysMenu> menus = null;
            if(roleId == 0){
                menus = sysMenuService.getAllMenuList();
            }else{
                menus = sysMenuService.getMenuList(roleId);
            }
           json = JsonUtil.toJson(MenuNode.toMenuNode(menus).getChildren());
           valOpt.set(key,json,1, TimeUnit.HOURS);
        }
        renderJson(json);
    }

    @ResponseBody
    @RequestMapping("/list")
    public void list(@NotNull long parentId){
        Page page = sysMenuService.getPage(super.getSystemParam(),parentId);
        renderJson(page.toJsonStr());
    }

    @RequestMapping("/index")
    public String index(){
        return "/system/menu/list";
    }

    @RequestMapping("/goAddPage")
    public String goAddPage(HttpServletRequest request,@NotNull long parentId){
        String parentName = null;
        if(parentId == 0){
            parentName = "根目录";
            request.setAttribute("level",1);
        }else{
           SysMenu menu = sysMenuService.getMenu(super.getSystemParam(),parentId);
           if(menu == null){
               throw new GenericException("父目录不存在");
           }
           request.setAttribute("level",menu.getLevel().longValue() + 1);
           parentName = menu.getName();
        }
        request.setAttribute("parentId",parentId);
        request.setAttribute("parentName",parentName);
        return "/system/menu/add";
    }

    @RequestMapping("/goEditPage")
    public String goEditPage(HttpServletRequest request,@NotNull long parentId,@NotNull long id){
        String parentName = null;
        if(parentId == 0){
            parentName = "根目录";
        }else{
            SysMenu menu = sysMenuService.getMenu(super.getSystemParam(),parentId);
            if(menu == null){
                throw new GenericException("上级菜单不存在");
            }
            parentName = menu.getName();
        }
        SysMenu menu = sysMenuService.getMenu(super.getSystemParam(),id);
        if(menu == null){
            throw new GenericException("菜单不存在");
        }
        request.setAttribute("menu",menu);
        request.setAttribute("parentId",parentId);
        request.setAttribute("level",menu.getLevel());
        request.setAttribute("parentName",parentName);
        request.setAttribute(ACTION_KEY,ACTION_EDIT);
        return "/system/menu/add";
    }

    @ResponseBody
    @RequestMapping("/updateStatus")
    public void updateStatus(@NotNull long id,@NotNull long status){
        sysMenuService.updateStatus(super.getSystemParam(),id,status);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/saveOrUpdate")
    public void save(@NotNull String name,@NotNull long status){
        SysMenu menu = new SysMenu();
        menu.setName(name);
        menu.setStatus(status);
        menu.setIcon(super.getString("icon"));
        menu.setModulePath(super.getString("modulePath"));
        menu.setRemark(super.getString("remark"));
        menu.setSort(super.getLong("sort"));
        menu.setUrl(super.getString("url"));
        menu.setId(super.getLong("id"));
        menu.setParentId(super.getLong("parentId"));
        menu.setLevel(super.getLong("level"));
        sysMenuService.saveOrUpdate(super.getSystemParam(),menu);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/delete")
    public void delete(@NotNull long id){
        sysMenuService.delete(super.getSystemParam(),id);
        super.renderSuccess();
    }
}
