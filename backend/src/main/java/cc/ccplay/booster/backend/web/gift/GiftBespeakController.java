package cc.ccplay.booster.backend.web.gift;

import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.api.content.GiftBespeakRecordService;
import cc.ccplay.booster.base.dto.content.gift.GiftBespeakRecordPageDto;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/gift/bespeak")
public class GiftBespeakController extends BaseBackendController {

    @Reference
    private GiftBespeakRecordService giftBespeakRecordService;

    @RequestMapping("/index")
    public String index(){
        return "/gift/bespeak/list";
    }

    @ResponseBody
    @RequestMapping("/list")
    public void list(@Param Long gameId,String giftName){
       Page<GiftBespeakRecordPageDto> page = giftBespeakRecordService.getPage(super.getSystemParam(),gameId,giftName);
       System.out.println(page.toJsonStr());
       super.renderJson(page);
    }

}
