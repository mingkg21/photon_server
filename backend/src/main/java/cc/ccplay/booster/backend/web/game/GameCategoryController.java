package cc.ccplay.booster.backend.web.game;

import cc.ccplay.booster.backend.web.BaseBackendController;
import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.annotation.CheckType;
import cc.ccplay.booster.base.annotation.Permission;
import cc.ccplay.booster.base.api.content.GameCategoryService;
import cc.ccplay.booster.base.dto.SelectizeDto;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.GameCategory;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@Controller
@RequestMapping("/game/category")
public class GameCategoryController extends BaseBackendController {

    @Reference
    private GameCategoryService gameCategoryService;

    @RequestMapping("/index")
    public String index(){
        return "/game/category/list";
    }

    @RequestMapping("/goEditPage")
    public String goEditPage(HttpServletRequest request,@NotNull long id){
        GameCategory category = gameCategoryService.get(super.getSystemParam(),id);
        if(category == null){
            throw new GenericException("数据不存在");
        }
        request.setAttribute("category",category);
        return "/game/category/edit";
    }

    @RequestMapping("/goAddPage")
    public String goAddPage(){
        return  "/game/category/edit";
    }

    @ResponseBody
    @RequestMapping("/list")
    public void list(@Param String name){
        Page page = gameCategoryService.getPage(super.getSystemParam(),name);
        renderJson(page);
    }

    @ResponseBody
    @RequestMapping("/updateStatus")
    public void updateStatus(@NotNull long status,@NotNull long id){
        Status s = Status.getNotNull(status);
        gameCategoryService.updateStatus(super.getSystemParam(),id,s);
        renderSuccess();
    }
    @ResponseBody
    @RequestMapping("/saveOrUpdate")
    public void saveOrUpdate(@NotNull String name, @Param Long id,@NotNull Long ordering){
        GameCategory category = new GameCategory();
        category.setId(id);
        category.setName(name);
        category.setOrdering(ordering);
        gameCategoryService.saveOrUpdate(super.getSystemParam(),category);
        renderSuccess();
    }

    @Permission(CheckType.OPEN)
    @ResponseBody
    @RequestMapping("/getForSelectize")
    public void getForSelectize(@Param String key){
        List<SelectizeDto> list = gameCategoryService.getForSelectize(super.getSystemParam(),key);
        super.renderJson(list);
    }
}
