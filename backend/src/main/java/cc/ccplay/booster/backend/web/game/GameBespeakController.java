package cc.ccplay.booster.backend.web.game;

import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.api.content.GameBespeakService;
import cc.ccplay.booster.base.api.content.GameTestTypeService;
import cc.ccplay.booster.base.model.content.GameBespeak;
import cc.ccplay.booster.base.util.DateTimeUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.game.GameBespeakDto;
import cc.ccplay.booster.base.model.content.GameTestType;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/game/bespeak")
public class GameBespeakController extends BaseBackendController {

    @Reference
    private GameBespeakService gameBespeakService;

    @Reference
    private GameTestTypeService gameTestTypeService;

    @RequestMapping("/index")
    public String index(){
        return "/game/bespeak/list";
    }

    @RequestMapping("/goEditPage")
    public String goEditPage(HttpServletRequest request, @NotNull long id){
        SystemParam param = super.getSystemParam();
        GameBespeakDto gameBespeakInfoDto = gameBespeakService.getEditInfo(param,id);
        List<GameTestType> testTypes = gameTestTypeService.getAll4Selector(param);
        request.setAttribute("testTypes",testTypes);
        request.setAttribute("bespeakInfo",gameBespeakInfoDto.getBespeak());
        request.setAttribute("gameInfo",gameBespeakInfoDto.getGameInfo());
        request.setAttribute("versionInfo",gameBespeakInfoDto.getVersionInfo());
        return "/game/bespeak/edit";
    }

    @RequestMapping("/goAddPage")
    public String goAddPage(HttpServletRequest request){
        SystemParam param = super.getSystemParam();
        List<GameTestType> testTypes = gameTestTypeService.getAll4Selector(param);
        request.setAttribute("testTypes",testTypes);
        return  "/game/bespeak/edit";
    }

    @ResponseBody
    @RequestMapping("/list")
    public void list(){
        Page page =  gameBespeakService.getPage(super.getSystemParam());
        super.renderJson(page);
    }

    @ResponseBody
    @RequestMapping("/saveOrUpdate")
    public void saveOrUpdate(@Param Long id,@NotNull long gameId,@NotNull long versionId,@NotNull long bespeakStatus,
                             @NotNull long testTypeId,@NotNull long recommendStatus,@NotNull String testTime){
        GameBespeak bespeak = new GameBespeak();
        bespeak.setGameId(gameId);
        bespeak.setVersionId(versionId);
        bespeak.setTestTypeId(testTypeId);
        bespeak.setRecommendStatus(recommendStatus);
        bespeak.setTestTime(DateTimeUtil.parseDateTime(testTime));
        bespeak.setId(id);
        bespeak.setBespeakStatus(bespeakStatus);
        gameBespeakService.saveOrUpdate(super.getSystemParam(),bespeak);
        super.renderSuccess();
    }
}
