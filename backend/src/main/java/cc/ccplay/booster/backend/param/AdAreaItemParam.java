package cc.ccplay.booster.backend.param;


import cc.ccplay.booster.base.model.content.AdAreaItem;
import org.easyj.frame.validation.annotation.DateFormat;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;

import java.util.Date;

public class AdAreaItemParam {

    public AdAreaItem toBaseModel(){
        AdAreaItem item = new AdAreaItem();
        item.setId(id);
        item.setAdId(adId);
        item.setPicUrl(picUrl);
        item.setObjectId(objectId);
        item.setObjectType(objectType);
        item.setStartTime(startTime);
        item.setEndTime(endTime);
        item.setOrdering(ordering);
        item.setTitle(title);
        return item;
    }


    @Param
    private Long id;

    @NotNull
    private Long adId;

    @NotNull
    private String title;

    @NotNull
    private String objectType;

    @NotNull
    private Long objectId;

    @NotNull
    @DateFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    @NotNull
    @DateFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    @NotNull
    private String picUrl;

    @NotNull
    private Long ordering;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAdId() {
        return adId;
    }

    public void setAdId(Long adId) {
        this.adId = adId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public Long getOrdering() {
        return ordering;
    }

    public void setOrdering(Long ordering) {
        this.ordering = ordering;
    }
}
