package cc.ccplay.booster.backend.web.game;

import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.api.content.GamePackageMappingService;
import cc.ccplay.booster.base.dto.content.game.GamePackageMappingDto;
import cc.ccplay.booster.base.model.content.GamePackageMapping;
import com.alibaba.dubbo.config.annotation.Reference;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.StringUtil;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/game/mapping")
public class GamePackageMappingController extends BaseBackendController {

    @Reference
    private GamePackageMappingService gamePackageMappingService;


    @RequestMapping("/goAddPage")
    public String goAddPage(){
        return "/game/mapping/edit";
    }

    @RequestMapping("/goEditPage")
    public String goEditPage(HttpServletRequest request, @NotNull long id){
        GamePackageMappingDto dto = gamePackageMappingService.getDto(id);
        if(dto == null){
            throw new GenericException("数据不存在");
        }
        request.setAttribute("gameInfo",dto.getGameInfo());
        request.setAttribute("versionInfo",dto.getVersionInfo());
        request.setAttribute("mapping",dto.getMapping());
        return "/game/mapping/edit";
    }


    @RequestMapping("/index")
    public String index(){
        return "/game/mapping/list";
    }

    @ResponseBody
    @RequestMapping("/list")
    public void list(@Param Long type, @Param Long gameId,@Param String packageName){
        GamePackageMapping mapping = new GamePackageMapping();
        mapping.setGameId(gameId);
        mapping.setPackageName(packageName);
        mapping.setType(type);
        Page page = gamePackageMappingService.getPage(super.getSystemParam(),mapping);
        super.renderJson(page);
    }

    @ResponseBody
    @RequestMapping("/saveOrUpdate")
    public void saveOrUpdate(@Param Long id,@NotNull String packageName,@NotNull long gameId,@NotNull long type){

        if(id == null){//新增
            String [] ps = packageName.split("\r\n");
            List <GamePackageMapping> list = new ArrayList<>();
            for (int i = 0; i < ps.length ; i++) {
                String pn = ps[i];
                if(StringUtil.isEmpty(pn)){
                    continue;
                }
                GamePackageMapping mapping = new GamePackageMapping();
                mapping.setGameId(gameId);
                mapping.setPackageName(pn.trim());
                mapping.setType(type);
                list.add(mapping);
            }
            gamePackageMappingService.batchSave(list);
        }else {
            GamePackageMapping mapping = new GamePackageMapping();
            mapping.setGameId(gameId);
            mapping.setPackageName(packageName);
            mapping.setType(type);
            mapping.setId(id);
            gamePackageMappingService.saveOrUpdate(mapping);
        }
        super.renderSuccess();
    }


    @ResponseBody
    @RequestMapping("/delete")
    public void delete(@NotNull long id){
        gamePackageMappingService.delete(id);
        super.renderSuccess();
    }

}
