package cc.ccplay.booster.backend.web.gift;

import cc.ccplay.booster.base.model.content.GiftNumber;
import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.backend.param.SaveGiftInfoParam;
import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.api.content.GiftInfoService;
import cc.ccplay.booster.base.api.content.GiftNumberService;
import cc.ccplay.booster.base.dto.content.gift.GiftInfoDto;
import cc.ccplay.booster.base.model.content.GiftInfo;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.StringUtil;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/gift/info")
public class GiftInfoController extends BaseBackendController {

    @Reference
    private GiftInfoService giftInfoService;

    @Reference
    private GiftNumberService giftNumberService;

    @RequestMapping("/index")
    public String index(){
        return "/gift/info/list";
    }

    @RequestMapping("/goAddPage")
    public String goAddPage(){
        return "/gift/info/edit";
    }

    @RequestMapping("/goEditPage")
    public String goEditPage(HttpServletRequest request,@NotNull long id){
        GiftInfoDto dto = giftInfoService.getGiftInfoDto(super.getSystemParam(),id);
        if(dto == null){
            throw new GenericException("礼品不存在");
        }
        request.setAttribute("giftInfo",dto.getGiftInfo());
        request.setAttribute("gameInfo",dto.getGameInfo());
        request.setAttribute("versionInfo",dto.getVersionInfo());
        return "/gift/info/edit";
    }

    @ResponseBody
    @RequestMapping("/list")
    public void list(@Param Long gameId,@Param String giftName){
       Page page = giftInfoService.getPage(super.getSystemParam(),gameId,giftName);
       super.renderJson(page);
    }

    @ResponseBody
    @RequestMapping("/getNumberList")
    public void getNumberList(@NotNull long giftId){
        Page page = giftNumberService.getPage(super.getSystemParam(),giftId);
        super.renderJson(page);
    }

    @ResponseBody
    @RequestMapping("/addNumbers")
    public void addNumbers(@NotNull String numbers,@NotNull long giftId){
        List<GiftNumber> list = new ArrayList<>();
        String ns [] = numbers.split("\r\n");
        for (int i = 0; i < ns.length; i++) {
            String n = StringUtil.trim2Null(ns[i]);
            if(n == null){
                continue;
            }
            GiftNumber number = new GiftNumber();
            number.setCode(n);
            number.setGiftId(giftId);
            list.add(number);
        }
        giftNumberService.batchSave(super.getSystemParam(),giftId,list);
        renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/saveOrUpdate")
    public void saveOrUpdate(@Param SaveGiftInfoParam giftInfoParam){
        GiftInfo giftInfo = giftInfoParam.toGiftInfo();
        giftInfoService.saveOrUpdate(super.getSystemParam(),giftInfo);
        renderSuccess();
    }

    @RequestMapping("/goAddNumberPage")
    public String goAddNumberPage(HttpServletRequest request,@NotNull long id){
        request.setAttribute("id",id);
        return "/gift/info/editNumber";
    }
}
