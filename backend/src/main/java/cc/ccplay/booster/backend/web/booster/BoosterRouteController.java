package cc.ccplay.booster.backend.web.booster;

import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.api.content.BoosterServerGroupGameService;
import cc.ccplay.booster.base.dto.SelectizeDto;
import cc.ccplay.booster.base.dto.content.booster.BoosterServerGroupGameDto;
import cc.ccplay.booster.base.model.content.BoosterServerGroup;
import cc.ccplay.booster.base.model.content.BoosterServerGroupGame;
import com.alibaba.dubbo.config.annotation.Reference;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.JsonUtil;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;


@Controller
@RequestMapping("/booster/route")
public class BoosterRouteController extends BaseBackendController{

    @Reference
    private BoosterServerGroupGameService boosterServerGroupGameService;

    @RequestMapping("/index")
    public String index(){
        return "/booster/route/list";
    }

    @RequestMapping("/goAddPage")
    public String goAddPage(){
        return "/booster/route/edit";
    }

    @RequestMapping("/goEditPage")
    public String goEditPage(HttpServletRequest request, @NotNull long id){
        BoosterServerGroupGameDto dto = boosterServerGroupGameService.getDto(id);
        if(dto == null){
            throw new GenericException("数据不存在");
        }
        request.setAttribute("versionInfo",dto.getVersionInfo());
        request.setAttribute("gameInfo",dto.getGameInfo());
        request.setAttribute("route",dto.getBoosterServerGroupGame());
        BoosterServerGroup group = dto.getGroup();
        if(group != null){
            SelectizeDto selectizeDto = new SelectizeDto(group.getId(),group.getName());
            request.setAttribute("serverGroupJson", JsonUtil.toJson(selectizeDto));
        }
        return "/booster/route/edit";
    }

    @ResponseBody
    @RequestMapping("/list")
    public void list(@Param Long gameId, @Param Long groupId,@Param String gameName){
        Page page = boosterServerGroupGameService.getPage(super.getSystemParam(),gameId,groupId,gameName);
        super.renderJson(page);
    }

    @ResponseBody
    @RequestMapping("/saveOrUpdate")
    public void saveOrUpdate(@Param Long id,@NotNull long gameId,@NotNull long groupId){
        BoosterServerGroupGame route = new BoosterServerGroupGame();
        route.setId(id);
        route.setGameId(gameId);
        route.setGroupId(groupId);
        boosterServerGroupGameService.saveOrUpdate(route);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/delete")
    public void delete(@NotNull long id){
        boosterServerGroupGameService.delete(id);
        super.renderSuccess();
    }

}
