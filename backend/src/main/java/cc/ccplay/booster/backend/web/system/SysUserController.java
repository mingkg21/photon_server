package cc.ccplay.booster.backend.web.system;

import cc.ccplay.booster.backend.core.Const;
import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.api.user.SysUserService;
import cc.ccplay.booster.base.model.user.SysRole;
import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.api.user.SysRoleService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.user.SysUser;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.MinLength;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RequestMapping("/system/user")
@Controller
public class SysUserController extends BaseBackendController {

    @Reference
    private SysUserService sysUserService;

    @Reference
    private SysRoleService sysRoleService;

    @RequestMapping("/index")
    public String index(){
        return "/system/user/list";
    }

    @RequestMapping("/list")
    @ResponseBody
    public void getList(){
        Page page = sysUserService.getList(getSystemParam());
        renderJson(page);
    }


    @RequestMapping("/goAddPage")
    public String goAddPage(){
        List<SysRole> roles = sysRoleService.getAll(getSystemParam());
        getRequest().setAttribute("roles",roles);
        return "/system/user/edit";
    }

    @RequestMapping("/goEditPage")
    public String goEditPage(HttpServletRequest request,@NotNull long id){
        SystemParam param = getSystemParam();
        SysUser user = sysUserService.getUser(param,id);
        List<SysRole> roles = sysRoleService.getAll(param);
        request.setAttribute("user",user);
        request.setAttribute("roles",roles);
        return "/system/user/edit";
    }

    @RequestMapping("/saveOrUpdate")
    @ResponseBody
    public void addUser(@NotNull @Param(remark = "账号") @MinLength(4) String account,
                        @NotNull @Param(remark = "真实姓名") String userName,@NotNull long roleId){
        Long id = getLong("id");
        SysUser user = new SysUser();
        if(id == null){
            user.setPassword(Const.DEFAULT_PASSWORD);
        }
        user.setId(id);
        user.setUserName(userName);
        user.setRoleId(roleId);
        user.setAccount(account);
        sysUserService.saveOrUpdate(getSystemParam(),user);
        renderSuccess();
    }

    @RequestMapping("/delete")
    @ResponseBody
    public void delete(@NotNull long id){
        sysUserService.logicDelete(getSystemParam(),id);
        renderSuccess();
    }

    @RequestMapping("/updateStatus")
    @ResponseBody
    public void updateStatus(@NotNull long id,@NotNull long status){
        sysUserService.updateStatus(getSystemParam(),id,status);
        renderSuccess();
    }

    @RequestMapping("/resetPassword")
    @ResponseBody
    public void resetPassword(@NotNull long userId){
        sysUserService.resetPassword(getSystemParam(),userId,Const.DEFAULT_PASSWORD);
        renderSuccess();
    }
}
