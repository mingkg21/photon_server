package cc.ccplay.booster.backend.web.log;


import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.annotation.SortMapping;
import cc.ccplay.booster.base.api.log.AppDayCountService;
import cc.ccplay.booster.base.util.DateTimeUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import org.apache.commons.lang3.time.DateUtils;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Controller
@RequestMapping("/log/totalDownload")
public class TotalDownloadController extends BaseBackendController {

    @Reference
    private AppDayCountService appDayCountService;

    @RequestMapping("/index")
    public String index(){
        HttpServletRequest request = super.getRequest();
        Date now = new Date();

        Date startDate = DateUtils.addDays(now,-6);

        request.setAttribute("startDate", DateTimeUtil.formatDate(startDate));
        request.setAttribute("endDate", DateTimeUtil.formatDate(now));
        return "/log/totalDownload/list";
    }

    @RequestMapping("/list")
    @ResponseBody
    @SortMapping(mapping = {"downloadCount","download_count","downloadCompleteCount","download_complete_count",
            "countDay","count_day","completePercent","complete_percent"})
    public void list(@NotNull(message = "开始时间不能为空") Date startDate, @NotNull(message = "结束时间不能为空") Date endDate){
        Page page = appDayCountService.getTotalPage(super.getSystemParam(),startDate,endDate);
        super.renderJson(page);
    }

}
