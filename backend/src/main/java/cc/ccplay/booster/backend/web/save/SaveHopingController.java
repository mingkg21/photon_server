package cc.ccplay.booster.backend.web.save;

import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.annotation.SortMapping;
import cc.ccplay.booster.base.api.content.SaveHopingService;
import com.alibaba.dubbo.config.annotation.Reference;
import org.easyj.frame.jdbc.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/save/hoping")
public class SaveHopingController extends BaseBackendController {

    @Reference
    private SaveHopingService saveHopingService;

    @RequestMapping("/index")
    public String index(){
        return "/save/hoping/list";
    }

    @ResponseBody
    @RequestMapping("/list")
    @SortMapping(mapping = {"personCount","a.person_count","lastHoppingTime","a.last_hopping_time"})
    public void list(){
        Page page = saveHopingService.getPage(super.getSystemParam());
        super.renderJson(page);
    }

}
