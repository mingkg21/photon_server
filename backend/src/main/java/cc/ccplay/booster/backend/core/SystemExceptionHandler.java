package cc.ccplay.booster.backend.core;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.easyj.frame.exception.*;
import org.easyj.frame.util.ActionUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SystemExceptionHandler implements ExceptionHandler {
    private Logger logger = Logger.getLogger(SystemExceptionHandler.class);

    public void resolveException(Exception exception, HttpServletResponse response, HttpServletRequest request) {
        AppException appEx = ExceptionUtil.handle(exception);
        doLog(appEx);
        if (ActionUtil.isAjaxRequest(request)) {// 响应json
            ActionUtil.render("application/json", appEx.toJsonMsg(), appEx.getResponseHeader(), response);
        } else {
            if (appEx.getClass() == NoLoginException.class) {
                try {
                    response.sendRedirect(request.getContextPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return;
            }

            if(appEx.getClass() == SystemError.class && appEx.getCause() != null){
                appEx.getCause().printStackTrace();
                ActionUtil.renderHtml("<html><body>500 ERROR </br> <pre>" + appEx.getCause().getLocalizedMessage()+ "</pre></body></html>", null);
            }else{
                ActionUtil.renderHtml("<html><body>500 ERROR </br> <pre>" + appEx.getFullMessage()+ "</pre></body></html>", null);
            }
        }
    }

    private void doLog(AppException appEx){
        Exception e = (Exception)appEx;
        String msg = appEx.getErrorMessage()+":"+appEx.getStackTraceMessage();

        //tomcat catalina.out文件
        if(appEx.logStackTrace()){
            Throwable cause = appEx.getCause();
            if(cause != null){
                cause.printStackTrace();//控制台输出堆栈信息
            }else if(appEx instanceof Exception){
                e.printStackTrace();
            }
        }else{
            System.err.println(msg);
        }

        //日志文件
        String fullMessage = appEx.getFullMessage();
        if(appEx.logStackTrace() == false){
            e = null;		   //不输出堆栈
            fullMessage = msg;//输入堆栈第一行信息
        }
        Level level = appEx.getLogLevel();
        if(level == Level.DEBUG){
            logger.debug(fullMessage,e);
        }else if(level == Level.INFO){
            logger.info(fullMessage,e);
        }else if(level == Level.WARN){
            logger.warn(fullMessage,e);
        }else if(level == Level.ERROR){
            logger.error(fullMessage,e);
        }else if(level == Level.FATAL){
            logger.fatal(fullMessage,e);
        }
    }
}