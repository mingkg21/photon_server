package cc.ccplay.booster.backend.core;

import java.text.MessageFormat;

public enum CacheKey {
    /**
     * 参数0：roleId  root用户roleId = 0
     */
    PERMISSION_MAP("permission_map_{0}"),

    /**
     * 参数0：roleId   root用户roleId = 0
     */
    BACKEND_MENU("backend_menu_{0}")
    ;


    private String keyTemplate;

    private CacheKey(String keyTemplate){
        this.keyTemplate = keyTemplate;
    }

    public String getKey(Object... args){
        return MessageFormat.format(this.keyTemplate,args);
    }


}
