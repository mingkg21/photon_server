package cc.ccplay.booster.backend.param;

import cc.ccplay.booster.base.model.content.OpenScreen;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;

public class OpenScreenParam {

    public OpenScreen toModel(){
        OpenScreen os = new OpenScreen();
        os.setId(id);
        os.setAppId(this.appId);
        os.setAutoDownload(this.autoDownload);
        os.setTitle(this.title);
        os.setImgUrl(this.imgUrl);
        os.setShowtime(this.showtime);
        return os;
    }

    @Param
    private Long id;

    @NotNull
    private String title;

    @NotNull
    private String imgUrl;

    @Param
    private Long appId;

    @Param
    private Long autoDownload;

    @NotNull
    private Long showtime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Long getAppId() {
        return appId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    public Long getAutoDownload() {
        return autoDownload;
    }

    public void setAutoDownload(Long autoDownload) {
        this.autoDownload = autoDownload;
    }

    public Long getShowtime() {
        return showtime;
    }

    public void setShowtime(Long showtime) {
        this.showtime = showtime;
    }
}
