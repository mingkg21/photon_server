package cc.ccplay.booster.backend.param;

import cc.ccplay.booster.base.model.content.GameRecruit;
import org.easyj.frame.validation.annotation.*;

import java.util.Date;

public class SaveGameRecruit {

    public GameRecruit toGameRecruit(){
        GameRecruit recruit = new GameRecruit();
        recruit.setStartTime(this.startTime);
        recruit.setEndTime(this.endTime);
        recruit.setId(this.id);
        recruit.setGameId(this.gameId);
        recruit.setComment(this.comment);
        recruit.setPrizeScore(this.prizeScore);
        recruit.setPrizeType(this.prizeType);
        return recruit;
    }

    @Param
    private Long id;

    @DateFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @NotNull
    private Date startTime;

    @DateFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @NotNull
    private Date endTime;

    @NotNull
    private Long gameId;

    @NotNull
    private String comment;

    @NotNull
    private Long prizeType;

    @NotNull
    @MinValue(1)
    @MaxValue(100)
    private Long prizeScore;

    public Long getPrizeType() {
        return prizeType;
    }

    public void setPrizeType(Long prizeType) {
        this.prizeType = prizeType;
    }

    public Long getPrizeScore() {
        return prizeScore;
    }

    public void setPrizeScore(Long prizeScore) {
        this.prizeScore = prizeScore;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
