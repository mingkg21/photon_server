package cc.ccplay.booster.backend.web.booster;

import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.api.content.BoosterGameFeedbackService;
import cc.ccplay.booster.base.model.content.BoosterGameFeedback;
import com.alibaba.dubbo.config.annotation.Reference;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/booster/feedback")
public class BoosterGameFeedbackController extends BaseBackendController{

    @Reference
    private BoosterGameFeedbackService boosterGameFeedbackService;

    @RequestMapping("/index")
    public String index(){
        return "/booster/feedback/list";
    }

    @ResponseBody
    @RequestMapping("/list")
    public void list(@Param String packageName,@Param Long userId,@Param Boolean containPackageName){
        BoosterGameFeedback searchModel = new BoosterGameFeedback();
        searchModel.setPackageName(packageName);
        searchModel.setUserId(userId);
        if(containPackageName == null){
            containPackageName = true;
        }
        Page page = boosterGameFeedbackService.getPage(super.getSystemParam(),searchModel,containPackageName);
        super.renderJson(page);
    }

    @ResponseBody
    @RequestMapping("/deleteByPackageName")
    public void deleteByPackageName(@NotNull String packageName){
        boosterGameFeedbackService.deleteByPackageName(packageName);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/deleteByName")
    public void deleteByName(@NotNull String name){
        boosterGameFeedbackService.deleteByName(name);
        super.renderSuccess();
    }
}
