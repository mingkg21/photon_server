package cc.ccplay.booster.backend.web.save;

import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.api.content.SaveRecordService;
import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.backend.param.SaveRecordParam;
import cc.ccplay.booster.base.dto.save.SaveRecordDto;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.SaveRecord;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/save/record")
public class SaveRecordController extends BaseBackendController {

    @Reference
    private SaveRecordService saveRecordService;

    @RequestMapping("/index")
    public String index(){
        return "/save/record/list";
    }

    @RequestMapping("/goAddPage")
    public String goAddPage(){
        return "/save/record/edit";
    }

    @RequestMapping("/goEditPage")
    public String goEditPage(HttpServletRequest request, @NotNull long id){
        SaveRecordDto saveRecordDto = saveRecordService.getDto(id);
        if(saveRecordDto == null){
            throw new GenericException("数据不存在");
        }
        request.setAttribute("gameInfo",saveRecordDto.getGameInfo());
        request.setAttribute("saveRecord",saveRecordDto.getSaveRecord());
        return "/save/record/edit";
    }


    @ResponseBody
    @RequestMapping("/list")
    public void list(@Param Long open,@Param Long gameId,@Param String name){
        Page page = saveRecordService.getPage(super.getSystemParam(),open,gameId,name);
        super.renderJson(page);
    }

    @ResponseBody
    @RequestMapping("/delete")
    public void delete(@NotNull long id){
        saveRecordService.delete(id);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/updateStatus")
    public void updateStatus(@NotNull long id,@NotNull long status){
        Status stat = Status.getNotNull(status);
        saveRecordService.updateStatus(id,stat);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/saveOrUpdate")
    public void saveOrUpdate(@Param SaveRecordParam saveRecordParam){
        SaveRecord saveRecord = saveRecordParam.toModel();
        saveRecord.setOpen(SaveRecord.OPEN_TRUE);
        saveRecordService.saveOrUpdate(saveRecord);
        super.renderSuccess();
    }
}
