package cc.ccplay.booster.backend.web;

import cc.ccplay.booster.base.util.UserUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.backend.core.CacheKey;
import cc.ccplay.booster.base.api.user.SysMenuService;
import cc.ccplay.booster.base.model.user.SysMenu;
import cc.ccplay.booster.base.model.user.SysUser;
import org.easyj.frame.util.JsonUtil;
import org.easyj.frame.util.StringUtil;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Repository
public class PermissionManager {

	@Reference
	private static SysMenuService sysMenuService;

	private static ValueOperations<String,String> vOps;

	@Resource(name="redisTemplate")
	public void setvOps(ValueOperations<String, String> vOps) {
		PermissionManager.vOps = vOps;
	}

	/**
	 * 总控后台权限Map 获取当前用户权限 key : url value ：{@link} AdminMenu status字段
	 * 
	 * @return
	 */
	public static Map<String, Long> getPermissionMap() throws ClassNotFoundException {
		SysUser user = (SysUser) UserUtil.getCurrentUser();
		Long roleId = user.getRoleId();
		boolean isRoot = false;
		if("root".equalsIgnoreCase(user.getAccount())){
			roleId = 0L;
			isRoot = true;
		}
		if(roleId == null && isRoot == false){
			return Collections.emptyMap();
		}
		String key = CacheKey.PERMISSION_MAP.getKey(roleId);
		String json = vOps.get(key);
		if(json == null){
			List<SysMenu> menus = null;
			if(isRoot){
				menus = sysMenuService.getAllMenuList();
			}else{
				menus = sysMenuService.getMenuList(roleId);
			}
			Map<String,Long> map = toPermissionMap(menus);
			json = JsonUtil.toJson(map);
			vOps.set(key, json,1, TimeUnit.HOURS);
			return map;
		}
		return JsonUtil.toBean(json,Map.class);
	}

	private static Map<String, Long> toPermissionMap(List<SysMenu> menu) {
		Map<String, Long> map = new HashMap<String, Long>();
		for (int i = 0; i < menu.size(); i++) {
			SysMenu m = menu.get(i);
			String modulePath = StringUtil.trim2Null(m.getModulePath());
			if (StringUtil.isNotEmpty(modulePath)) {
				map.put(modulePath, m.getStatus());
			}
			String functionPath = StringUtil.trim2Null(m.getUrl());
			if (StringUtil.isNotEmpty(functionPath)) {
				map.put(functionPath, m.getStatus());
			}
		}
		return map;
	}
}
