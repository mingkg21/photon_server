package cc.ccplay.booster.backend.web.game;

import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.api.content.GameHotWordService;
import cc.ccplay.booster.base.model.content.GameHotWord;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/game/hotword")
public class GameHotWordController extends BaseBackendController {

    @Reference
    private GameHotWordService gameHotWordService;

    @RequestMapping("/goAddPage")
    public String goAddPage(){
        return "/game/hotword/edit";
    }


    @RequestMapping("/goEditPage")
    public String goEditPage(HttpServletRequest request,@NotNull long id){
        GameHotWord hotWord = gameHotWordService.get(super.getSystemParam(),id);
        if(hotWord == null){
            throw new GenericException("数据不存在");
        }
        request.setAttribute("hotword",hotWord);
        return "/game/hotword/edit";
    }


    @RequestMapping("/index")
    public String index(){
        return "/game/hotword/list";
    }

    @ResponseBody
    @RequestMapping("/list")
    public void list(@Param String name){
        Page page = gameHotWordService.queryPage(super.getSystemParam(),name);
        super.renderJson(page);
    }

    @ResponseBody
    @RequestMapping("/saveOrUpdate")
    public void saveOrUpdate(@NotNull String name, @Param Long id,@NotNull long ordering){
        GameHotWord word = new GameHotWord();
        word.setId(id);
        word.setOrdering(ordering);
        word.setName(name);
        gameHotWordService.saveOrUpdate(super.getSystemParam(),word);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/resetDefaultWord")
    public void resetDefaultWord(@NotNull long id){
        gameHotWordService.setHotWord(super.getSystemParam(),id);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/delete")
    public void delete(@NotNull long id){
        gameHotWordService.delete(super.getSystemParam(),id);
        super.renderSuccess();
    }

}
