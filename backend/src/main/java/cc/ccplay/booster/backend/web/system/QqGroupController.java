package cc.ccplay.booster.backend.web.system;

import cc.ccplay.booster.backend.param.QqGroupParam;
import cc.ccplay.booster.base.model.content.QqGroup;
import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.api.content.QqGroupService;
import cc.ccplay.booster.base.enums.Status;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/system/qqgroup")
public class QqGroupController extends BaseBackendController {

    @Reference
    private QqGroupService qqGroupService;

    @RequestMapping("/index")
    public String index(){
        return "/system/qqgroup/list";
    }

    @RequestMapping("/goAddPage")
    public String goAddPage(){
        return "/system/qqgroup/edit";
    }

    @RequestMapping("/goEditPage")
    public String goEditPage(HttpServletRequest request, @NotNull long id){
        QqGroup qqGroup = qqGroupService.get(id);
        if(qqGroup == null){
            throw new GenericException("编辑对象不存在");
        }
        request.setAttribute("qqGroup",qqGroup);
        return "/system/qqgroup/edit";
    }

    @ResponseBody
    @RequestMapping("/list")
    public void list(){
        super.renderJson(qqGroupService.getPage(super.getSystemParam()));
    }


    @ResponseBody
    @RequestMapping("/updateStatus")
    public void updateStatus(@NotNull long id,@NotNull long status){
        Status s = Status.getNotNull(status);
        qqGroupService.updateStatus(id,s);
        super.renderSuccess();
    }


    @ResponseBody
    @RequestMapping("/saveOrUpdate")
    public void saveOrUpdate(@Param QqGroupParam qqGroupParam){
        QqGroup group = qqGroupParam.toModel();
        qqGroupService.saveOrUpdate(group);
        super.renderSuccess();
    }

}
