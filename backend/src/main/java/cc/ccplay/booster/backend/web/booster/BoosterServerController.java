package cc.ccplay.booster.backend.web.booster;

import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.annotation.CheckType;
import cc.ccplay.booster.base.annotation.Permission;
import cc.ccplay.booster.base.api.content.BoosterServerPortService;
import cc.ccplay.booster.base.api.content.BoosterServerService;
import cc.ccplay.booster.base.api.user.UserFlowService;
import cc.ccplay.booster.base.dto.SelectizeDto;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.BoosterServer;
import cc.ccplay.booster.base.model.content.BoosterServerPort;
import com.alibaba.dubbo.config.annotation.Reference;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/booster/server")
public class BoosterServerController extends BaseBackendController{

    @Reference
    private BoosterServerService boosterServerService;

    @Reference
    private BoosterServerPortService boosterServerPortService;

    @Reference
    private UserFlowService userFlowService;

    @RequestMapping("/index")
    public String index(){
        return "/booster/server/list";
    }

    @RequestMapping("/goAddPage")
    public String goAddPage(){
        return "/booster/server/edit";
    }

    @RequestMapping("/goEditPage")
    public String goEditPage(HttpServletRequest request, @NotNull long id){
        BoosterServer boosterServer = boosterServerService.get(id);
        if(boosterServer == null){
            throw new GenericException("服务器不存在");
        }
        request.setAttribute("server",boosterServer);
        return "/booster/server/edit";
    }

    @ResponseBody
    @RequestMapping("/list")
    public void list(@Param String name,@Param Long status){
        Page page = boosterServerService.getPage(super.getSystemParam(),name,Status.get(status));
        super.renderJson(page);
    }

    @ResponseBody
    @RequestMapping("/saveOrUpdate")
    public void saveOrUpdate(@Param Long id,@NotNull String ip,@NotNull String name ,@NotNull long maxConnections,
                             @NotNull long startPort,@NotNull long endPort, @NotNull String realIp ,@Param String remark){

        if(endPort <= startPort){
            throw new GenericException("终止端口必须大于起始端口");
        }

        BoosterServer server = new BoosterServer();
        server.setIp(ip);
        server.setId(id);
        server.setName(name);
        server.setRemark(remark);
        server.setMaxConnections(maxConnections);
        server.setMethod("aes-256-cfb");
        server.setEndPort(endPort);
        server.setStartPort(startPort);
        server.setRealIp(realIp);
        boosterServerService.saveOrUpdate(server);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/updateStatus")
    public void updateStatus(@NotNull long id,@NotNull long status){
        boosterServerService.updateStatus(id,Status.getNotNull(status));
        super.renderSuccess();
    }

    @RequestMapping("/goPortListPage")
    public String goPortListPage(HttpServletRequest request,@NotNull long serverId){
        request.setAttribute("serverId",serverId);
        return "/booster/server/portList";
    }

//    @ResponseBody
//    @RequestMapping("/deletePort")
//    public void deletePort(HttpServletRequest request,@NotNull long portId){
//        boosterServerPortService.delete(portId);
//        super.renderSuccess();
//    }

//    @ResponseBody
//    @RequestMapping("/getPortList")
//    public void getPortList(HttpServletRequest request,@NotNull long serverId){
//       Page page = boosterServerPortService.getPage(super.getSystemParam(),serverId);
//       super.renderJson(page);
//    }
//
//
//    @RequestMapping("/goAddPortPage")
//    public String goAddPortPage(HttpServletRequest request,@NotNull long serverId){
//        request.setAttribute("serverId",serverId);
//        return "/booster/server/editPort";
//    }


//    @RequestMapping("/goEditPortPage")
//    public String goEditPortPage(HttpServletRequest request,@NotNull long portId){
//        BoosterServerPort port = boosterServerPortService.get(portId);
//        request.setAttribute("port",port);
//        request.setAttribute("serverId",port.getServerId());
//        return "/booster/server/editPort";
//    }
//
//    @ResponseBody
//    @RequestMapping("/saveOrUpdatePort")
//    public void saveOrUpdatePort(@Param Long id,@NotNull long serverId,@NotNull long port,@NotNull String password ){
//        BoosterServerPort serverPort = new BoosterServerPort();
//        serverPort.setPort(port);
//        serverPort.setId(id);
//        serverPort.setPassword(password);
//        serverPort.setServerId(serverId);
//        boosterServerPortService.saveOrUpdate(serverPort);
//        super.renderSuccess();
//    }
//
//    @ResponseBody
//    @RequestMapping("/updatePortStatus")
//    public void updatePortStatus(@NotNull long portId,@NotNull long status){
//        boosterServerPortService.updateStatus(portId,Status.getNotNull(status));
//        super.renderSuccess();
//    }

    @Permission(CheckType.OPEN)
    @ResponseBody
    @RequestMapping("/getForSelectize")
    public void getForSelectize(@Param String key){
        List<SelectizeDto> list = boosterServerService.getForSelectize(super.getSystemParam(),key);
        super.renderJson(list);
    }

    @ResponseBody
    @RequestMapping("/resetDefaultServer")
    public void resetDefaultServer(@NotNull long id){
        boosterServerService.resetDefaultServer(id);
        super.renderSuccess();
    }

}
