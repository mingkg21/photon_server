package cc.ccplay.booster.backend.web.content;

import cc.ccplay.booster.backend.param.AdAreaItemParam;
import cc.ccplay.booster.backend.param.AdAreaParam;
import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.api.content.AdAreaItemService;
import cc.ccplay.booster.base.api.content.AdAreaService;
import cc.ccplay.booster.base.model.content.AdArea;
import cc.ccplay.booster.base.model.content.AdAreaItem;
import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.enums.Status;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.StringUtil;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/content/ad")
public class AdController extends BaseBackendController {

    @Reference
    private AdAreaService adAreaService;

    @Reference
    private AdAreaItemService adAreaItemService;



    @RequestMapping("/goAddPage")
    public String goAddPage(){
        return "/content/ad/edit";
    }

    @RequestMapping("/goEditPage")
    public String goEditPage(HttpServletRequest request, @NotNull long id){
        AdArea adArea = adAreaService.get(id);
        if(adArea == null){
            throw new GenericException("数据不存在");
        }
        request.setAttribute("typeMap",getAdTypesMap(adArea));
        request.setAttribute("area",adArea);
        return "/content/ad/edit";
    }


    @RequestMapping("/index")
    public String index(){
        return "/content/ad/list";
    }

    @RequestMapping("/list")
    @ResponseBody
    public void list(){
        Page page = adAreaService.getPage(super.getSystemParam());
        super.renderJson(page);
    }

    @RequestMapping("/updateStatus")
    @ResponseBody
    public void updateStatus(@NotNull long id,@NotNull long status){
        adAreaService.updateStatus(id, Status.getNotNull(status));
        super.renderSuccess();
    }

    @RequestMapping("/saveOrUpdate")
    @ResponseBody
    public void saveOrUpdate(HttpServletRequest request,@Param AdAreaParam adAreaParam){
        String [] types  = request.getParameterValues("type");
        if(types == null || types.length == 0){
            throw new GenericException("至少选择一条广告类型");
        }
        AdArea adArea = adAreaParam.toBaseModel();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < types.length; i++) {
            if(i != 0){
                sb.append(",");
            }
            sb.append(types[i]);
        }
        adArea.setSuportTypes(sb.toString());
        adAreaService.saveOrUpdate(adArea);
        super.renderSuccess();
    }

    @RequestMapping("/goAddItemPage")
    public String goAddItemPage(HttpServletRequest request,@NotNull long adId){
        AdArea adArea = adAreaService.get(adId);
        if(adArea == null){
            throw new GenericException("数据不存在");
        }
        request.setAttribute("adId",adId);
        request.setAttribute("typeMap",this.getAdTypesMap(adArea));
        return "/content/ad/editItem";
    }

    @RequestMapping("/goEditItemPage")
    public String goEditItemPage(HttpServletRequest request,@NotNull long itemId){
        AdAreaItem item = adAreaItemService.get(itemId);
        if(item == null){
            throw new GenericException("数据不存在");
        }
        request.setAttribute("item",item);
        Long adId = item.getAdId();
        AdArea adArea = adAreaService.get(adId);
        if(adArea == null){
            throw new GenericException("广告位不存在");
        }
        request.setAttribute("adId",adId);
        request.setAttribute("typeMap",this.getAdTypesMap(adArea));
        return "/content/ad/editItem";
    }

    @RequestMapping("/goItemListPage")
    public String goItemListPage(HttpServletRequest request,@NotNull long adId){
        request.setAttribute("adId",adId);
        return "/content/ad/itemList";
    }

    @RequestMapping("/getItemList")
    @ResponseBody
    public void getItemList(@NotNull long adId){
        Page page = adAreaItemService.getPage(super.getSystemParam(),adId);
        super.renderJson(page);
    }

    @RequestMapping("/saveOrUpdateItem")
    @ResponseBody
    public void saveOrUpdateItem(@Param AdAreaItemParam adAreaItemParam){
        AdAreaItem item = adAreaItemParam.toBaseModel();
        adAreaItemService.saveOrUpdate(item);
        super.renderSuccess();
    }

    @RequestMapping("/deleteItem")
    @ResponseBody
    public void deleteItem(@NotNull long itemId){
        adAreaItemService.delete(itemId);
        super.renderSuccess();
    }


    private Map<String,Boolean> getAdTypesMap(AdArea adArea){
        Map<String,Boolean> typeMap = new HashMap<>();
        String suportTypes = adArea.getSuportTypes();
        if(StringUtil.isNotEmpty(suportTypes)){
            String [] types = suportTypes.split(",");
            for (int i = 0; i < types.length; i++) {
                typeMap.put(types[i],true);
            }
        }
       return typeMap;
    }

}
