package cc.ccplay.booster.backend.web.game;

import cc.ccplay.booster.backend.web.BaseBackendController;
import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.api.content.GameTestTypeService;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.GameTestType;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/game/testType")
public class GameTestTypeController extends BaseBackendController {

    @Reference
    private GameTestTypeService gameTestTypeService;

    @RequestMapping("/index")
    public String index(){
        return "/game/testType/list";
    }

    @RequestMapping("/goEditPage")
    public String goEditPage(HttpServletRequest request, @NotNull long id){
        GameTestType testType = gameTestTypeService.get(super.getSystemParam(),id);
        if(testType == null){
            throw new GenericException("数据不存在");
        }
        request.setAttribute("testType",testType);
        return "/game/testType/edit";
    }

    @RequestMapping("/goAddPage")
    public String goAddPage(){
        return "/game/testType/edit";
    }

    @ResponseBody
    @RequestMapping("/list")
    public void list(@Param String name){
        Page page = gameTestTypeService.getPage(super.getSystemParam(),name);
        super.renderJson(page);
    }

    @ResponseBody
    @RequestMapping("/updateStatus")
    public void updateStatus(@NotNull long id,@NotNull long status){
        Status s = Status.getNotNull(status);
        gameTestTypeService.updateStatus(super.getSystemParam(),id,s);
        renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/saveOrUpdate")
    public void saveOrUpdate(@NotNull String name, @Param Long id){
        GameTestType testType  = new GameTestType();
        testType.setId(id);
        testType.setName(name);
        gameTestTypeService.saveOrUpdate(super.getSystemParam(),testType);
        renderSuccess();
    }
}
