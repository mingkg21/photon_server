package cc.ccplay.booster.backend.web.game;

import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.annotation.CheckType;
import cc.ccplay.booster.base.api.content.GameInfoService;
import cc.ccplay.booster.base.api.content.GameTagService;
import cc.ccplay.booster.base.dto.SelectizeDto;
import cc.ccplay.booster.base.dto.content.game.GameTagItemDto;
import cc.ccplay.booster.base.model.content.GameInfo;
import cc.ccplay.booster.base.model.content.GameTag;
import cc.ccplay.booster.base.model.content.GameTagItem;
import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.annotation.Permission;
import cc.ccplay.booster.base.enums.Status;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/game/tag")
public class GameTagController extends BaseBackendController {

    @Reference
    private GameTagService gameTagService;

    @Reference
    private GameInfoService gameInfoService;

    @RequestMapping("/index")
    public String index(){
        return "/game/tag/list";
    }

    @RequestMapping("/goEditPage")
    public String goEditPage(HttpServletRequest request,@NotNull long id){
        GameTag tag = gameTagService.get(super.getSystemParam(),id);
        if(tag == null){
            throw new GenericException("数据不存在");
        }
        request.setAttribute("tag",tag);
        return "/game/tag/edit";
    }

    @RequestMapping("/goAddPage")
    public String goAddPage(){
        return "/game/tag/edit";
    }

    @ResponseBody
    @RequestMapping("/list")
    public void list(@Param String name,@Param Long status){
        GameTag searchParam = new GameTag();
        searchParam.setType(GameTag.TYPE_COMMON);
        searchParam.setName(name);
        if(status != null) {
            searchParam.setStatus(Status.get(status));
        }
        Page page = gameTagService.getPage(super.getSystemParam(),searchParam);
        super.renderJson(page);
    }

    @Permission(CheckType.OPEN)
    @ResponseBody
    @RequestMapping("/getForSelectize")
    public void getForSelectize(@Param String key){
        GameTag searchParam = new GameTag();
        searchParam.setName(key);
        searchParam.setType(GameTag.TYPE_COMMON);
        searchParam.setStatus(Status.ENABLED);
        List<SelectizeDto> list = gameTagService.getForSelectize(super.getSystemParam(),searchParam);
        super.renderJson(list);
    }

    @ResponseBody
    @RequestMapping("/updateStatus")
    public void updateStatus(@NotNull long id,@NotNull long status){
        Status s = Status.getNotNull(status);
        gameTagService.updateStatus(super.getSystemParam(),id,s);
        renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/saveOrUpdate")
    public void saveOrUpdate(@NotNull String name, @Param Long id, @Param String description,
                             @NotNull Long recommendOrdering){
        GameTag tag  = new GameTag();
        tag.setId(id);
        tag.setName(name);
        tag.setDescription(description);
        tag.setRecommendOrdering(recommendOrdering);
        tag.setType(GameTag.TYPE_COMMON);
        gameTagService.saveOrUpdate(super.getSystemParam(),tag);
        renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/bindGame")
    public void bindGame(@NotNull long tagId,@NotNull long gameId,@NotNull long ordering){
        gameTagService.bindGame(tagId,gameId,ordering);


        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/unbindGame")
    public void unbindGame(@NotNull long tagId,@NotNull long gameId){
        gameTagService.unbindGame(tagId,gameId);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/getGameList")
    public void getGameList(@NotNull long tagId,@Param Long gameId,
                            @Param String packageName,@Param String gameName){
        GameInfo serchModel = new GameInfo();
        serchModel.setId(gameId);
        serchModel.setPackageName(packageName);
        serchModel.setName(gameName);
        super.renderJson(gameInfoService.getPage(super.getSystemParam(),serchModel,tagId,null,null));
    }

    @RequestMapping("/goEditBindGamePage")
    public String goEditBindGamePage(@NotNull long tagId,@NotNull long gameId){
        GameTagItem.Id id = new GameTagItem.Id(gameId,tagId);
        GameTagItemDto item = gameTagService.getItemDto(id);
        if(item == null){
            throw new GenericException("数据不存在");
        }
        super.getRequest().setAttribute("tagId",tagId);
        super.getRequest().setAttribute("item",item.getGameTagItem());
        super.getRequest().setAttribute("gameInfo",item.getGameInfo());
        super.getRequest().setAttribute("versionInfo",item.getVersionInfo());
        return "/game/tag/bindGame";
    }


    @RequestMapping("/goBindGamePage")
    public String goBindGamePage(@NotNull long tagId){
        super.getRequest().setAttribute("tagId",tagId);
        return "/game/tag/bindGame";
    }

    @RequestMapping("/goGameListPage")
    public String goGameListPage(@NotNull long tagId){
        HttpServletRequest request = getRequest();
        request.setAttribute("tagId",tagId);
        return "/game/tag/gameList";
    }



}
