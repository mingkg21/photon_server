package cc.ccplay.booster.backend.web.game;

import cc.ccplay.booster.backend.param.SaveGameParam;
import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.annotation.CheckType;
import cc.ccplay.booster.base.annotation.SortMapping;
import cc.ccplay.booster.base.api.content.GameCategoryService;
import cc.ccplay.booster.base.api.content.GameInfoService;
import cc.ccplay.booster.base.api.content.GameRelatedService;
import cc.ccplay.booster.base.dto.SelectizeDto;
import cc.ccplay.booster.base.dto.content.game.EditGameDto;
import cc.ccplay.booster.base.dto.content.game.GameRelatedDto;
import cc.ccplay.booster.base.model.content.GameInfo;
import cc.ccplay.booster.base.model.content.GameRelated;
import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.annotation.Permission;
import cc.ccplay.booster.base.enums.Status;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.JsonUtil;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/game/info")
public class GameInfoController extends BaseBackendController {

    @Reference
    private GameInfoService gameInfoService;

    @Reference
    private GameCategoryService gameCategoryService;

    @Reference
    private GameRelatedService gameRelatedService;


    @RequestMapping("/goAddPage")
    public String goAddPage(){
        return "/game/info/edit";
    }

    @RequestMapping("/goEditPage")
    public String getEditPage(HttpServletRequest request,@NotNull long id){
        EditGameDto editGameDto = gameInfoService.getEditGameInfo(super.getSystemParam(),id);
        request.setAttribute("gameInfo",editGameDto.getGameInfo());
        request.setAttribute("categoryJson", JsonUtil.toJson(editGameDto.getCategory()));
        request.setAttribute("publisherJson",JsonUtil.toJson(editGameDto.getPublisher()));
        request.setAttribute("tagsJson",JsonUtil.toJson(editGameDto.getTags()));
        request.setAttribute("specialTagsJson",JsonUtil.toJson(editGameDto.getSpecialTags()));
        return "/game/info/edit";
    }

    @ResponseBody
    @RequestMapping("/updateStatus")
    public void updateStatus(@NotNull long id,@NotNull long status){
        gameInfoService.updateStatus(super.getSystemParam(),id,Status.getNotNull(status));
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/saveOrUpdate")
    public void saveOrUpdate(@Param SaveGameParam gameParam){

        GameInfo gameInfo = gameParam.toGameInfo();
        Long id = gameParam.getId();
        if(id == null){//新增才会有评分字段
            Double score = gameParam.getScore();
            if(score == null){
                throw new GenericException("评分不能为空");
            }
            gameInfo.setScore(score);
            gameInfo.setScoreCount(100L);
            gameInfo.setTotalScore((long)(score * 100));
        }
        long [] tagIds = getLongArray("tagIds",",");
        if(tagIds == null || tagIds.length == 0){
            throw new GenericException("请选择标签");
        }

        long [] specialTagIds = getLongArray("specialTagIds",",");
        gameInfoService.saveOrUpdate(super.getSystemParam(),gameInfo,tagIds,specialTagIds);
        super.renderSuccess();
    }

    @RequestMapping("/index")
    public String index(){
        return "/game/info/index";
    }

    @ResponseBody
    @RequestMapping("/list")
    @SortMapping(mapping = {"downloadCount","a.download_count","id","a.id"})
    public void list(@Param Long id,@Param String name,@Param String packageName,@Param Long status,@Param Boolean newVersion){
        GameInfo gameInfo = new GameInfo();
        gameInfo.setName(name);
        gameInfo.setId(id);
        gameInfo.setPackageName(packageName);
        if(status != null) {
            gameInfo.setStatus(Status.get(status).getValue());
        }
        Page page = gameInfoService.getPage(super.getSystemParam(),gameInfo,null,null,newVersion);
        super.renderJson(page);
    }

    @Permission(CheckType.OPEN)
    @ResponseBody
    @RequestMapping("/list4Select")
    public void list4Select(@Param String name,@Param String packageName,@Param Long versionType,@Param Boolean showAll){
        GameInfo gameInfo = new GameInfo();
        gameInfo.setName(name);
        gameInfo.setPackageName(packageName);
        if(showAll == null || !showAll) {
            gameInfo.setStatus(Status.ENABLED.getValue());
        }
        Page page = gameInfoService.getPage(super.getSystemParam(),gameInfo,null,versionType,null);
        super.renderJson(page);
    }

    @Permission(CheckType.OPEN)
    @RequestMapping("/bucket")
    public String bucket(HttpServletRequest request,@NotNull String fnId,@Param Long versionType,@Param Boolean showAll){
        request.setAttribute("fnId",fnId);
        request.setAttribute("versionType",versionType);
        request.setAttribute("showAll",showAll);
        return "/game/info/bucket";
    }

    @Permission(CheckType.OPEN)
    @ResponseBody
    @RequestMapping("/getForSelectize")
    public void getForSelectize(@Param String name){
        List<SelectizeDto> list = gameInfoService.getForSelectize(super.getSystemParam(),name);
        super.renderJson(list);
    }


    @RequestMapping("/goAddRelatedGamePage")
    public String goAddRelatedGamePage(HttpServletRequest request,@NotNull long gameId){
        request.setAttribute("gameId",gameId);
        return "/game/info/bindGame";
    }

    @RequestMapping("/goEditRelatedGamePage")
    public String goEditRelatedGamePage(HttpServletRequest request,@NotNull long gameId,@NotNull long relatedGameId){
        GameRelatedDto dto = gameRelatedService.getRelatedDto(gameId,relatedGameId);
        if(dto == null){
            throw new GenericException("数据不存在");
        }
        request.setAttribute("gameId",gameId);
        request.setAttribute("related",dto.getGameRelated());
        request.setAttribute("gameInfo",dto.getGameInfo());
        request.setAttribute("versionInfo",dto.getVersionInfo());
        return "/game/info/bindGame";
    }

    @ResponseBody
    @RequestMapping("/saveRelatedGame")
    public void saveRelatedGame(@NotNull long gameId,@NotNull long relatedGameId,@NotNull long ordering){
        if(gameId == relatedGameId){
            throw new GenericException("相同应该不能关联");
        }
        GameRelated related = new GameRelated();
        GameRelated.Id id = new GameRelated.Id(gameId,relatedGameId);
        related.setOrdering(ordering);
        related.setId(id);
        gameRelatedService.saveOrUpdate(related);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/deleteRelatedGame")
    public void deleteRelatedGame(@NotNull long gameId,@NotNull long relatedGameId){
        gameRelatedService.delete(gameId,relatedGameId);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/getRelatedGameList")
    public void getRelatedGameList(@NotNull long gameId){
       Page page = gameRelatedService.getPage(gameId);
       super.renderJson(page);
    }

}
