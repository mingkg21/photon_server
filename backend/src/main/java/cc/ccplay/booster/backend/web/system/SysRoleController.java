package cc.ccplay.booster.backend.web.system;


import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.api.user.SysRoleService;
import cc.ccplay.booster.base.model.user.SysRole;
import com.alibaba.dubbo.config.annotation.Reference;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.validation.annotation.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("/system/role")
@Controller
public class SysRoleController extends BaseBackendController {

    @Reference
    private SysRoleService sysRoleService;

    @RequestMapping("/goAddPage")
    public String goAddPage(){
        return "/system/role/edit";
    }

    @RequestMapping("/goEditPage")
    public String goEditPage(@NotNull long id){
        SysRole role = sysRoleService.getRole(getSystemParam(),id);
        if(role == null){
            throw new GenericException("角色不存在!");
        }
        getRequest().setAttribute("role",role);
        return "/system/role/edit";
    }

    @RequestMapping("/index")
    public String index(){
        return "/system/role/list";
    }

    @ResponseBody
    @RequestMapping("/list")
    public void list(){
        renderJson(sysRoleService.getPage(getSystemParam()));
    }

    @ResponseBody
    @RequestMapping("/delete")
    public void delete(@NotNull long id){
        sysRoleService.logicDelete(getSystemParam(),id);
        renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/saveOrUpdate")
    public void saveOrUpdate(@NotNull String name){
        SysRole role = new SysRole();
        role.setName(name);
        role.setId(getLong("id"));
        sysRoleService.saveOrUpdate(getSystemParam(),role);
        renderSuccess();
    }
}
