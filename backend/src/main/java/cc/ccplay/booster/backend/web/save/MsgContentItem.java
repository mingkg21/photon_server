package cc.ccplay.booster.backend.web.save;

public class MsgContentItem implements Comparable<MsgContentItem>{

    private Long color;

    private Long fontSize;

    private Long bold;

    private String url;

    private Long id;

    private Long type;

    private Long ordering;

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getColor() {
        return color;
    }

    public void setColor(Long color) {
        this.color = color;
    }

    public Long getFontSize() {
        return fontSize;
    }

    public void setFontSize(Long fontSize) {
        this.fontSize = fontSize;
    }

    public Long getBold() {
        return bold;
    }

    public void setBold(Long bold) {
        this.bold = bold;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Long getOrdering() {
        return ordering;
    }

    public void setOrdering(Long ordering) {
        this.ordering = ordering;
    }

    @Override
    public int compareTo(MsgContentItem o) {
        if(this.ordering == null){
            return 1;
        }
        if(o.ordering == null){
            return -1;
        }
        long a = this.ordering.longValue();
        long b = o.ordering.longValue();
        if(a == b){
            return 0;
        }
        if( a < b){
            return -1;
        }
        return 1;
    }
}
