package cc.ccplay.booster.backend.job;

import cc.ccplay.booster.base.api.log.AppDayCountService;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;

@Repository
public class CountAppDownloadJob {

    /**
     * 必须要加static,要不然会出现空指针
     */
    @Reference
    private static AppDayCountService appDayCountService;

    /* *
       凌晨1点
     * 汇总前一天的数据
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void countPreDayData(){
        appDayCountService.countPreDay();
    }

    /**
     * 每隔一个小时
     * 汇总当天数据
     */
    @Scheduled(cron = "0 0 0/1 * * ?")
    public void countCurrentDayData(){
        System.out.println("执行任务日志（汇总当天数据）");
        appDayCountService.countToday();
    }
}
