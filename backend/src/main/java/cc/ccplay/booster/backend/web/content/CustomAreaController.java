package cc.ccplay.booster.backend.web.content;

import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.api.content.CustomAreaService;
import cc.ccplay.booster.base.dto.content.CustomAreaItemDto;
import cc.ccplay.booster.base.model.content.CustomArea;
import com.alibaba.dubbo.config.annotation.Reference;

import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.CustomAreaItem;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/content/area")
public class CustomAreaController extends BaseBackendController {

    @Reference
    private CustomAreaService customAreaService;

    @RequestMapping("/index")
    public String index(){
        return "/content/area/list";
    }

    @RequestMapping("/goEditPage")
    public String goEditPage(HttpServletRequest request,@NotNull long id){
        CustomArea area = customAreaService.get(super.getSystemParam(),id);
        if(area == null){
            throw new GenericException("数据不存在");
        }
        request.setAttribute("area",area);
        return "/content/area/edit";
    }

    @RequestMapping("/goAddPage")
    public String goAddPage(){
        return "/content/area/edit";
    }

    @ResponseBody
    @RequestMapping("/list")
    public void list(){
        Page page = customAreaService.getPage(super.getSystemParam());
        super.renderJson(page);
    }

    @ResponseBody
    @RequestMapping("/updateStatus")
    public void updateStatus(@NotNull long id,@NotNull long status){
        Status s = Status.getNotNull(status);
        customAreaService.updateStatus(super.getSystemParam(),id,s);
        renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/saveOrUpdate")
    public void saveOrUpdate(@NotNull String name,@NotNull String code,@Param Long id, @Param String remark){
        CustomArea area = new CustomArea();
        area.setId(id);
        area.setCode(code);
        area.setName(name);
        area.setRemark(remark);
        customAreaService.saveOrUpdate(super.getSystemParam(),area);
        renderSuccess();
    }


    @RequestMapping("/goGameListPage")
    public String goGameListPage(@NotNull long areaId){
//        CustomArea area = customAreaService.get(super.getSystemParam(),areaId);
//        if(area == null){
//            throw new GenericException("专区不存在");
//        }
        HttpServletRequest request = getRequest();
        request.setAttribute("areaId",areaId);
        return "/content/area/gameList";
    }

    @RequestMapping("/goBindGamePage")
    public String goBindGamePage(@NotNull long areaId){

        CustomArea area = customAreaService.get(super.getSystemParam(),areaId);
        if(area == null){
            throw new GenericException("专区不存在");
        }
        super.getRequest().setAttribute("areaId",areaId);
        super.getRequest().setAttribute("areaCode",area.getCode());
        return "/content/area/bindGame";
    }

    @RequestMapping("/goEditBindGamePage")
    public String goEditBindGamePage(@NotNull long id){
        CustomAreaItemDto itemDto = customAreaService.getAreaItem(super.getSystemParam(),id);
        if(itemDto == null){
            throw new GenericException("数据不存在");
        }
        HttpServletRequest request = super.getRequest();
        CustomAreaItem item = itemDto.getItem();
        request.setAttribute("item",item);
        request.setAttribute("areaId",item.getAreaId());
        request.setAttribute("gameInfo",itemDto.getGameInfo());
        request.setAttribute("versionInfo",itemDto.getVersionInfo());
        return "/content/area/bindGame";
    }

    @ResponseBody
    @RequestMapping("/getGameList")
    public void getGameList(@NotNull long areaId,@Param Long gameId,
                            @Param String packageName,@Param String gameName){
        Page page = customAreaService.getAreaGamePage(super.getSystemParam(),areaId,gameId,packageName,gameName);
        super.renderJson(page);
    }

    @ResponseBody
    @RequestMapping("/bindGame")
    public void bindGame(@NotNull long areaId,@NotNull long gameId,@NotNull long ordering,@Param Long id,@Param String banner){
        CustomAreaItem item = new CustomAreaItem();
        item.setId(id);
        item.setGameId(gameId);
        item.setAreaId(areaId);
        item.setOrdering(ordering);
        item.setBanner(banner);
        customAreaService.saveAreaItem(super.getSystemParam(),item);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/unbindGame")
    public void unbindGame(@NotNull long id){
        customAreaService.deleteAreaItem(super.getSystemParam(),id);
        super.renderSuccess();
    }
}
