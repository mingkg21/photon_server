package cc.ccplay.booster.backend.web.content;

import cc.ccplay.booster.base.api.content.GameCommentService;
import cc.ccplay.booster.backend.web.BaseBackendController;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import cc.ccplay.booster.base.model.content.GameComment;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/content/gameComment")
public class GameCommentController extends BaseBackendController {

    @Reference
    private GameCommentService gameCommentService;

    @RequestMapping("/index")
    public String index(){
        return "/content/gameComment/list";
    }

    @ResponseBody
    @RequestMapping("/list")
    public void list(@Param Long gameId,@Param Long userId,@Param String gameName){
        GameComment searchObject = new GameComment();
        searchObject.setGameId(gameId);
        searchObject.setUserId(userId);
        Page page = gameCommentService.getCommentPage(super.getSystemParam(),searchObject,gameName,2L);
        super.renderJson(page);
    }

    @ResponseBody
    @RequestMapping("/setDown")
    public void setDown(@NotNull long id){
        GameComment comment = new GameComment();
        comment.setId(id);
        comment.setTopStatus(GameComment.TOP_STATUS_COMMON);
        gameCommentService.updateNotNull(comment);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/setTop")
    public void setTop(@NotNull long id){
        GameComment comment = new GameComment();
        comment.setId(id);
        comment.setTopStatus(GameComment.TOP_STATUS_STICK);
        gameCommentService.updateNotNull(comment);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/delete")
    public void delete(@NotNull long id){
        gameCommentService.delete(id);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/batchDelete")
    public void batchDelete(@NotNull String ids){
        gameCommentService.batchDelete(ids);
        super.renderSuccess();
    }


    @RequestMapping("/goEditPage")
    public String goEditPage(HttpServletRequest request, @NotNull long id){
        request.setAttribute("id",id);
        return "/content/gameComment/edit";
    }

    @RequestMapping("/goAddPage")
    public String goAddPage(){
        return "/content/gameComment/coment";
    }

    @ResponseBody
    @RequestMapping("/saveOfficialReply")
    public void saveOfficialReply(@Param @NotNull long id ,@Param @NotNull String content,@NotNull String picJson){
        JSONArray arr = JSONArray.parseArray(picJson);
        JSONArray img = new JSONArray();
        int size = arr.size();
        for (int i = 0; i < size; i++) {
            JSONObject jsonObject = arr.getJSONObject(i);
            img.add(jsonObject.getString("src"));
        }
        gameCommentService.saveOfficialReply(id,content,img);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/saveComment")
    public void saveComment(@Param @NotNull long userId,
                            @Param @NotNull String content,
                            @Param @NotNull long gameId,
                            @Param @NotNull long star){

        if(star % 2 != 0  || star < 2 || star > 10){
            throw new GenericException("评级必须是2-10的偶数");
        }
        GameComment gameComment = new GameComment();
        gameComment.setUserId(userId);
        gameComment.setGameId(gameId);
        gameComment.setContent(content);
        //gameComment.setImgs(CdnImageListAdapter.createImage(toImages(imgs),null));
        gameCommentService.comment(gameComment,star);
        super.renderSuccess();
    }

}
