package cc.ccplay.booster.backend.web.save;

import cc.ccplay.booster.base.api.content.SaveGameService;
import cc.ccplay.booster.backend.web.BaseBackendController;
import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.dto.save.SaveGameDto;
import cc.ccplay.booster.base.model.content.SaveGame;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/save/game")
public class SaveGameController extends BaseBackendController {

    @Reference
    private SaveGameService saveGameService;

    @RequestMapping("/index")
    public String index(){
        return "/save/game/list";
    }

    @RequestMapping("/goAddPage")
    public String goAddPage(HttpServletRequest request){
        request.setAttribute(ACTION_KEY,ACTION_ADD);
        return "/save/game/edit";
    }

    @RequestMapping("/goEditPage")
    public String goEditPage(HttpServletRequest request, @NotNull long gameId){
        request.setAttribute("gameId",gameId);
        request.setAttribute(ACTION_KEY,ACTION_EDIT);
        SaveGameDto dto = saveGameService.getDto(gameId);
        request.setAttribute("saveGame",dto.getSaveGame());
        request.setAttribute("gameInfo",dto.getGameInfo());
        return "/save/game/edit";
    }

    @ResponseBody
    @RequestMapping("/list")
    public void list(@Param Long gameId,@Param String name,@Param String packageName){
        Page page = saveGameService.getPage(super.getSystemParam(),gameId,packageName,name);
        super.renderJson(page);
    }

    @ResponseBody
    @RequestMapping("/saveOrUpdate")
    public void saveOrUpdate(@NotNull String action,@NotNull long gameId,@NotNull String config){
        if(ACTION_EDIT.equals(action)){
            saveGameService.update(gameId,config);
        }else if(ACTION_ADD.equals(action)){
            SaveGame saveGame = new SaveGame();
            saveGame.setGameId(gameId);
            saveGame.setConfig(config);
            saveGameService.save(saveGame);
        }else{
            throw new GenericException("[action]值未定义");
        }
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/delete")
    public void delete(@NotNull long gameId){
        saveGameService.delete(gameId);
        super.renderSuccess();
    }

}
