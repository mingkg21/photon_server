package cc.ccplay.booster.backend.web.game;

import cc.ccplay.booster.backend.param.SaveGameVersionParam;
import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.api.content.GameVersionService;
import cc.ccplay.booster.base.dto.content.game.EditGameVersionDto;
import cc.ccplay.booster.base.model.content.GameVersion;
import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.enums.Status;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.JsonUtil;
import org.easyj.frame.util.MixUtil;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/game/info")
public class GameVersionController extends BaseBackendController {

    @Reference
    private GameVersionService gameVersionService;

    @ResponseBody
    @RequestMapping("/getVersionList")
    public void getVersionList(@NotNull long gameId){
        Page page = gameVersionService.getPage(super.getSystemParam(),gameId);
        super.renderJson(page);
    }

    @ResponseBody
    @RequestMapping("/getAllVersion")
    public void getAllVersion(HttpServletRequest request,@NotNull long gameId){
        List<GameVersion> list = gameVersionService.getAllVersion(super.getSystemParam(),gameId);
        super.renderJson(MixUtil.newHashMap("versionList",list));
    }

    @ResponseBody
    @RequestMapping("/setLastestVersion")
    public void setLastestVersion(@NotNull(message = "版本ID不能为空") long id){
        gameVersionService.setLastestVersion(super.getSystemParam(),id);
        super.renderSuccess();
    }

    @RequestMapping("/goAddVersionPage")
    public String goAddVersionPage(HttpServletRequest request,@NotNull long gameId){
        request.setAttribute("gameId",gameId);
        EditGameVersionDto dto = gameVersionService.getLastVersionDto(gameId);
        request.setAttribute("versionInfo",dto.getGameVersion());
        request.setAttribute("add",true);
        request.setAttribute("picJson", JsonUtil.toJson(dto.getPics()));
        return "/game/info/editVersion";
    }

    @RequestMapping("/goEditVersionPage")
    public String goEditVersionPage(HttpServletRequest request,@NotNull long id){
        EditGameVersionDto dto = gameVersionService.getEditInfo(super.getSystemParam(),id);
        request.setAttribute("versionInfo",dto.getGameVersion());
        request.setAttribute("gameId",dto.getGameVersion().getGameId());
        request.setAttribute("picJson", JsonUtil.toJson(dto.getPics()));
        return "/game/info/editVersion";
    }

    @ResponseBody
    @RequestMapping("/saveOrUpdateVersion")
    public void saveOrUpdateVersion(@Param SaveGameVersionParam param){
        GameVersion version = param.getGameVersion();
        gameVersionService.saveOrUpdate(version, param.getGameVersionPicture(), param.isReplacePackageName(), param.getPackageName());
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/updateVersionStatus")
    public void updateVersionStatus(@NotNull long id,@NotNull long status){
        gameVersionService.updateStatus(id, Status.getNotNull(status));
        super.renderSuccess();
    }
}
