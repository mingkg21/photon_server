package cc.ccplay.booster.backend.param;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import cc.ccplay.booster.base.model.content.GameVersion;
import cc.ccplay.booster.base.model.content.GameVersionPicture;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.exception.ParameterException;
import org.easyj.frame.util.StringUtil;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SaveGameVersionParam {

    public GameVersion getGameVersion(){
        GameVersion version = new GameVersion();
        version.setId(this.id);
        version.setLang(this.lang);
        version.setIcon(this.icon);
        version.setGameId(this.gameId);
        version.setVersionType(this.versionType);
        version.setVersionName(this.versionName);
        version.setAdapterInfo(this.adapterInfo);
        version.setUpdateLog(this.updateLog);

        if(StringUtil.equals(this.versionType,GameVersion.VERSION_TYPE_RELEASE)){
            if(StringUtil.isEmpty(this.packageName)){
                throw new GenericException("包名不能为空");
            }
            version.setPackageName(this.packageName);
            version.setVersionCode(this.versionCode);
            version.setFileSize(this.fileSize);
            version.setDownloadUrl(this.downloadUrl);
            version.setSign(this.sign);
        }

        if(StringUtil.isNotEmpty(versionTags)){
            if(versionTags.length() > 9){
                throw new GenericException("版本标签不能超过9个字符");
            }
            String []tags = versionTags.split(" ");
            if(tags.length > 2){
                throw new GenericException("最多只能录入2个标签");
            }
            version.setVersionTags(versionTags);
        }else{
            version.setVersionTags("");
        }
        return version;
    }

    public List<GameVersionPicture> getGameVersionPicture(){
        if(StringUtil.isEmpty(this.picJson)){
            return Collections.emptyList();
        }
        JSONArray arr = null;
        try {
            arr = JSONArray.parseArray(this.picJson);
        }catch (Exception e){
            e.printStackTrace();
            throw new ParameterException("无法将字段["+picJson+"]转换成JSON");
        }
        List<GameVersionPicture> list = new ArrayList<>();
        int size = arr.size();
        for (int i = 0; i < size; i++) {
            JSONObject jsonObject = arr.getJSONObject(i);
            GameVersionPicture pic = new GameVersionPicture();
            pic.setSize(jsonObject.getLong("size"));
            pic.setGameId(this.gameId);
            pic.setHeight(jsonObject.getLong("height"));
            pic.setWidth(jsonObject.getLong("width"));
            pic.setSrc(jsonObject.getString("src"));
            list.add(pic);
        }
        return list;
    }

    @NotNull
    private String versionName;

    @NotNull
    private String lang;

    @NotNull
    private Long versionType;

    @NotNull
    private String icon;

    @Param
    private String downloadUrl;

    @Param
    private Long fileSize;

    @Param
    private Long gameId;

    @Param
    private Long id;

    @Param
    private String picJson;

    @Param
    private Long versionCode;

    @Param
    private String adapterInfo;

    @Param
    private String updateLog;


    @Param
    private String packageName;

    /**
     * 是否强制替换当前包名
     */
    @NotNull
    private boolean replacePackageName;

    @Param
    private String sign;

    @Param
    private String versionTags;


    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getVersionTags() {
        return versionTags;
    }

    public void setVersionTags(String versionTags) {
        this.versionTags = versionTags;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public boolean isReplacePackageName() {
        return replacePackageName;
    }

    public void setReplacePackageName(boolean replacePackageName) {
        this.replacePackageName = replacePackageName;
    }

    public String getAdapterInfo() {
        return adapterInfo;
    }

    public void setAdapterInfo(String adapterInfo) {
        this.adapterInfo = adapterInfo;
    }

    public String getUpdateLog() {
        return updateLog;
    }

    public void setUpdateLog(String updateLog) {
        this.updateLog = updateLog;
    }

    public void setVersionType(Long versionType) {
        this.versionType = versionType;
    }

    public String getPicJson() {
        return picJson;
    }

    public void setPicJson(String picJson) {
        this.picJson = picJson;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public Long getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(Long versionCode) {
        this.versionCode = versionCode;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public Long getVersionType() {
        return versionType;
    }

    public void setVersion_type(Long versionType) {
        this.versionType = versionType;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }
}
