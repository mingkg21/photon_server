package cc.ccplay.booster.backend.web.content;

import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.api.content.AppChannelManageService;
import cc.ccplay.booster.base.api.content.AppChannelService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.SelectizeDto;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.AppChannel;
import cc.ccplay.booster.base.model.content.AppChannelManage;
import com.alibaba.dubbo.config.annotation.Reference;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.JsonUtil;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("/content/channelManage")
@Controller
public class AppChannelManageController extends BaseBackendController {

    @Reference
    private AppChannelManageService appChannelManageService;

    @Reference
    private AppChannelService appChannelService;

    @RequestMapping("/index")
    public String index(){
        return "/content/channelManage/list";
    }

    @RequestMapping("/list")
    @ResponseBody
    public void getList(){
        Page page = appChannelManageService.getList(getSystemParam());
        renderJson(page);
    }


    @RequestMapping("/goAddPage")
    public String goAddPage(){
        return "/content/channelManage/edit";
    }

    @RequestMapping("/goEditPage")
    public String goEditPage(HttpServletRequest request,@NotNull long id){
        SystemParam param = getSystemParam();
        AppChannelManage channelManage = appChannelManageService.getChannelManage(param,id);

        AppChannel appChannel = appChannelService.getChannel(channelManage.getCode());

        request.setAttribute("channel",channelManage);
        request.setAttribute("appChannelJson", JsonUtil.toJson(new SelectizeDto(channelManage.getCode(), appChannel.getName())));
        return "/content/channelManage/edit";
    }

    @RequestMapping("/saveOrUpdate")
    @ResponseBody
    public void saveOrUpdate(@NotNull @Param(remark = "APP版本号") Long versionCode,
                        @NotNull @Param(remark = "渠道标识") String code){
        Long id = getLong("id");
        AppChannelManage user = new AppChannelManage();
        user.setId(id);
        user.setCode(code);
        user.setVersionCode(versionCode);
        appChannelManageService.saveOrUpdate(getSystemParam(),user);
        renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/updateStatus")
    public void saveOrUpdate(@NotNull long id,@NotNull long status){
        Status sta = Status.getNotNull(status);
        appChannelManageService.updateStatus(super.getSystemParam(),id,sta);
        super.renderSuccess();
    }

}
