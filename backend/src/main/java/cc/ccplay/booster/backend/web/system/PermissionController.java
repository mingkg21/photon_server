package cc.ccplay.booster.backend.web.system;

import cc.ccplay.booster.backend.core.MenuNode;
import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.api.user.SysMenuService;
import cc.ccplay.booster.base.api.user.SysRoleService;
import cc.ccplay.booster.base.model.user.SysMenu;
import cc.ccplay.booster.base.model.user.SysRole;
import com.alibaba.dubbo.config.annotation.Reference;
import org.easyj.frame.util.MixUtil;
import org.easyj.frame.validation.annotation.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/system/permission")
public class PermissionController extends BaseBackendController {

    @Reference
    private SysMenuService sysMenuService;

    @Reference
    private SysRoleService sysRoleService;

    @ResponseBody
    @RequestMapping("/savePermission")
    public void savePermission(HttpServletRequest request,@NotNull long roleId){
        String [] menuIds = request.getParameterValues("menuId");
        sysRoleService.savePermission(getSystemParam(),menuIds,roleId);
        renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/getPermission")
    public void getPermission(@NotNull long roleId){
       List<SysMenu> menus = sysMenuService.getMenuList(roleId);
       renderJson(MixUtil.newHashMap("menus",menus));
    }

    @RequestMapping("/index")
    public String index(HttpServletRequest request){
        List<SysRole> roles = sysRoleService.getAll(getSystemParam());
        List<SysMenu> menus = sysMenuService.getAllMenuList();
        MenuNode rootNode = MenuNode.toMenuNode(menus);
        request.setAttribute("roles",roles);
        request.setAttribute("rootNode",rootNode);
        return "/system/permission/index";
    }
}
