package cc.ccplay.booster.backend.web.file;

import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.annotation.CheckType;
import cc.ccplay.booster.base.annotation.NotNeedLogin;
import cc.ccplay.booster.base.annotation.Permission;
import cc.ccplay.booster.base.api.content.BucketPackageService;
import cc.ccplay.booster.base.api.content.CrawlGameService;
import cc.ccplay.booster.base.api.content.QiNiuService;
import cc.ccplay.booster.base.dto.QiNiuAuth;
import cc.ccplay.booster.base.model.content.BucketPackage;
import cc.ccplay.booster.base.model.content.CrawlGame;
import cc.ccplay.booster.base.util.UserUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("/file/package")
@Controller
public class BucketPackageController extends BaseBackendController {


    @Reference
    private QiNiuService qiNiuService;

    @Reference
    private BucketPackageService bucketPackageService;

    @Reference
    private CrawlGameService crawlGameService;


    @Permission(CheckType.OPEN)
    @RequestMapping("/getUploadToken")
    @ResponseBody
    public void getUploadToken(){
        QiNiuAuth auth = qiNiuService.getUploadPkgToken(super.getSystemParam());
        super.renderJson(auth);
    }

    @RequestMapping("/index")
    public String index(){
        super.getRequest().setAttribute("loginUser", UserUtil.getCurrentUser());
        return "/file/package/list";
    }

    @RequestMapping("/goEditPage")
    public String goEditPage(HttpServletRequest request,@NotNull long id){
        BucketPackage apk = bucketPackageService.get(id);
        if(apk == null){
            throw new GenericException("数据不存在");
        }
        request.setAttribute("apk",apk);
        return "/file/package/edit";
    }

    /**
     * 上传成功之后调用
     */
    @NotNeedLogin
    @ResponseBody
    @RequestMapping("/uploadSuccess")
    public void uploadSuccess(){
        String src = super.getString("key");
        if(src == null){
            super.renderFailure();
            return;
        }
        BucketPackage apk = new BucketPackage();
        apk.setCreateUserId(super.getLong("userId"));
        apk.setFileName(super.getString("name"));
        apk.setSize(super.getLong("size"));
        apk.setSrc(src);
        apk.setHash(super.getString("hash"));
        apk.setPackageName(super.getString("packageName"));
        apk.setVersionName(super.getString("versionName"));
        apk.setVersionCode(super.getLong("versionCode"));
        apk.setUpdateLog(super.getString("updateLog"));
        apk.setAdapterInfo(super.getString("adapterInfo"));
        bucketPackageService.save(super.getSystemParam(),apk);

        CrawlGame crawlGame = new CrawlGame();
        crawlGame.setPackageName(apk.getPackageName());
        crawlGame.setVersionName(apk.getVersionName());
        crawlGame.setVersionCode(apk.getVersionCode());
        crawlGameService.updateCrawlInfo(crawlGame);

        super.renderSuccess();
    }

    @RequestMapping("/list")
    @ResponseBody
    public void list(@Param String account,@Param String fileName,@Param String packageName){
        BucketPackage searchObj = new BucketPackage();
        searchObj.setAccount(account);
        searchObj.setFileName(fileName);
        searchObj.setPackageName(packageName);
        Page page = bucketPackageService.queryPage(super.getSystemParam(),searchObj);
        super.renderJson(page);
    }

    @Permission(CheckType.OPEN)
    @RequestMapping("/bucket")
    public String bucket(HttpServletRequest request){
        request.setAttribute("loginUser", UserUtil.getCurrentUser());
        request.setAttribute("fnId",super.getString("fnId"));
        return "/file/package/bucket";
    }

    @Permission(CheckType.OPEN)
    @RequestMapping("/query")
    @ResponseBody
    public void query(HttpServletRequest request, @Param String account,@Param String fileName,@Param String packageName){
        BucketPackage searchObj =new BucketPackage();
        searchObj.setAccount(account);
        searchObj.setFileName(fileName);
        searchObj.setPackageName(packageName);
        Page page = bucketPackageService.queryPage(super.getSystemParam(),searchObj);
        super.renderJson(page);
    }

    @RequestMapping("/update")
    @ResponseBody
    public void update(@NotNull String fileName,@NotNull long id,@NotNull String versionName,
                       @NotNull long versionCode,@NotNull String packageName,
                       @Param String updateLog,@Param String adapterInfo){
        BucketPackage apk = new BucketPackage();
        apk.setVersionCode(versionCode);
        apk.setId(id);
        apk.setPackageName(packageName);
        apk.setVersionName(versionName);
        apk.setFileName(fileName);
        apk.setUpdateLog(updateLog);
        apk.setAdapterInfo(adapterInfo);
        bucketPackageService.update(apk);
        super.renderSuccess();
    }

    @RequestMapping("/delete")
    @ResponseBody
    public void delete(@NotNull long id){
        bucketPackageService.delete(id);
        super.renderSuccess();
    }
}
