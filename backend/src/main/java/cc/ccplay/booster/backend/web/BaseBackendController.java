package cc.ccplay.booster.backend.web;

import cc.ccplay.booster.base.util.UserUtil;
import cc.ccplay.booster.base.core.SystemParam;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.ActionUtil;

public class BaseBackendController extends ActionUtil{

    public final static String ACTION_KEY = "action";

    /**
     * 新增操作
     */
    public final static String ACTION_ADD = "add";
    /**
     *  编辑操作
     */
    public final static String ACTION_EDIT = "edit";

    public SystemParam getSystemParam(){
        SystemParam param = (SystemParam) UserUtil.getCustomData();
        return param;
    }

    public static void renderJson(Page page){
        renderJson(page.toJsonStr());
    }
}
