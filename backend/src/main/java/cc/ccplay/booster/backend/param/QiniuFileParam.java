package cc.ccplay.booster.backend.param;

import cc.ccplay.booster.base.model.content.*;
import com.alibaba.fastjson.JSONObject;
import cc.ccplay.booster.base.model.content.*;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;

public class QiniuFileParam {

    public static String [] versionCodeArr = new String []{
            "1.0","1.1","1.5","1.6","2.0",    // 1 - 5
            "2.0.1","2.1.x","2.2.x","2.3","2.3.3", //6 - 10
            "3.0.x","3.1.x","3.2","4.0","4.0.3", // 11 - 15
            "4.1","4.2","4.3","4.4","4.4W", //16 - 20
            "5.0","5.1","6.0","7.0","7.1",// 21 - 25
            "8.0","8.1","9.0"   // 26 - 28

    };


    private String getAdapterInfo(Long minSdkVersion){
        if(minSdkVersion == null){
            return null;
        }
        String versionName = null;
        try {
            int version = minSdkVersion.intValue();
            versionName = versionCodeArr[version - 1];
        }catch (Exception e){
            return null;
        }
        if(versionName == null){
            return null;
        }
        return "适配安卓"+versionName+"以上系统";
    }

    public BaseFile toModel(){
        BaseFile baseFile = null;
        String endPrefix = "";

        int index = fileKey.lastIndexOf(".");
        if(index > -1){
            endPrefix = fileKey.substring(index + 1);
        }
        if("apk".equals(endPrefix) || "cpk".equals(endPrefix) || "xapk".equals(endPrefix) || "gapk".equals(endPrefix) || "apk".equals(type)){
            if(extendJson == null){
                throw new GenericException("缺少apk信息");
            }
            JSONObject jsonObject = JSONObject.parseObject(extendJson);
            BucketPackage apk = new BucketPackage();
            apk.setVersionName(jsonObject.getString("versionName"));
            apk.setPackageName(jsonObject.getString("packageName"));
            apk.setVersionCode(jsonObject.getLong("versionCode"));
            apk.setAdapterInfo(getAdapterInfo(jsonObject.getLongValue("minSdkVersion")));
            apk.setSign(jsonObject.getString("sign"));
            baseFile = apk;
        }else if("png".equals(endPrefix) ||"jpg".equals(endPrefix)
                || "gif".equals(endPrefix) || "jpg".equals(endPrefix)
                ||  "jpeg".equals(endPrefix) || "bmp".equals(endPrefix)
                || "img".equals(type)){
            JSONObject jsonObject = JSONObject.parseObject(extendJson);
            BucketImage image = new BucketImage();
            image.setHeight(jsonObject.getLong("height"));
            image.setWidth(jsonObject.getLong("width"));
            image.setType("image/"+endPrefix);
            baseFile = image;
        }else if("mp4".equals(endPrefix)){
            JSONObject jsonObject = JSONObject.parseObject(extendJson);
            BucketVideo video = new BucketVideo();
            video.setType("video/"+endPrefix);
            baseFile = video;
        }else{
            BucketFile file = new BucketFile();
            baseFile = file;
        }

        baseFile.setSize(size);
        baseFile.setCreateUserId(userId);
        baseFile.setAccount(account);
        baseFile.setFileName(name);
        baseFile.setSrc(fileKey);
        baseFile.setHash(hash);

        return baseFile;
    }

    @NotNull
    private String fileKey;

    @NotNull
    private long size;

    @NotNull
    private String name;

    @NotNull
    private long userId;

    @NotNull
    private String hash;

    @NotNull
    private String account;

    @Param
    private String extendJson;

    @Param
    private String type;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }


    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getFileKey() {
        return fileKey;
    }

    public void setFileKey(String fileKey) {
        this.fileKey = fileKey;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getExtendJson() {
        return extendJson;
    }

    public void setExtendJson(String extendJson) {
        this.extendJson = extendJson;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
