package cc.ccplay.booster.backend.web.gift;

import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.api.content.GiftReceiveRecordService;
import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.dto.content.gift.GiftReceiveRecordPageDto;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/gift/receive")
public class GiftReceiveController extends BaseBackendController {

    @Reference
    private GiftReceiveRecordService giftReceiveRecordService;

    @RequestMapping("/index")
    public String index(){
        return "/gift/receive/list";
    }

    @ResponseBody
    @RequestMapping("/list")
    public void list(@Param Long gameId, String giftName){
        Page<GiftReceiveRecordPageDto> page = giftReceiveRecordService.getPage(super.getSystemParam(),gameId,giftName);
        super.renderJson(page);
    }

}
