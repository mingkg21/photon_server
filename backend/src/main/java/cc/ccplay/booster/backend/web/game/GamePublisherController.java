package cc.ccplay.booster.backend.web.game;

import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.annotation.CheckType;
import cc.ccplay.booster.base.annotation.Permission;
import cc.ccplay.booster.base.api.content.GamePublisherService;
import cc.ccplay.booster.base.dto.SelectizeDto;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.GamePublisher;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/game/publisher")
public class GamePublisherController extends BaseBackendController {

    @Reference
    private GamePublisherService gamePublisherService;

    @RequestMapping("/index")
    public String index(){
        return "/game/publisher/list";
    }

    @RequestMapping("/goEditPage")
    public String goEditPage(HttpServletRequest request,@NotNull long id){
        GamePublisher publisher = gamePublisherService.get(super.getSystemParam(),id);
        if(publisher == null){
            throw new GenericException("数据不存在");
        }
        request.setAttribute("publisher",publisher);
        return "/game/publisher/edit";
    }

    @RequestMapping("/goAddPage")
    public String goAddPage(){
        return "/game/publisher/edit";
    }

    @ResponseBody
    @RequestMapping("/list")
    public void list(){
        Page page = gamePublisherService.getPage(super.getSystemParam(),null);
        super.renderJson(page);
    }

    @ResponseBody
    @RequestMapping("/updateStatus")
    public void updateStatus(@NotNull long id,@NotNull long status){
        Status s = Status.getNotNull(status);
        gamePublisherService.updateStatus(super.getSystemParam(),id,s);
        renderSuccess();
    }
    @ResponseBody
    @RequestMapping("/saveOrUpdate")
    public void saveOrUpdate(@NotNull String name, @Param Long id){
        GamePublisher publisher  = new GamePublisher();
        publisher.setId(id);
        publisher.setName(name);
        gamePublisherService.saveOrUpdate(super.getSystemParam(),publisher);
        renderSuccess();
    }

    @Permission(CheckType.OPEN)
    @ResponseBody
    @RequestMapping("/getForSelectize")
    public void getForSelectize(@Param String key){
        List<SelectizeDto> list = gamePublisherService.getForSelectize(super.getSystemParam(),key);
        super.renderJson(list);
    }
}
