package cc.ccplay.booster.backend.web.content;

import cc.ccplay.booster.backend.param.SaveAppReleaseParam;
import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.api.content.AppReleaseService;
import cc.ccplay.booster.base.model.content.AppRelease;
import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.enums.Status;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/content/release")
public class AppReleaseController extends BaseBackendController {

    @Reference
    private AppReleaseService appReleaseService;

    @RequestMapping("/index")
    public String index(){
        return "/content/release/list";
    }

    @RequestMapping("/goAddPage")
    public String goAddPage(){
        return "/content/release/edit";
    }

    @RequestMapping("/goEditPage")
    public String goEditPage(HttpServletRequest request, @NotNull long id){
        AppRelease appRelease = appReleaseService.get(id);
        if(appRelease == null){
            throw new GenericException("数据不存在");
        }
        request.setAttribute("release",appRelease);
        return "/content/release/edit";
    }

    @ResponseBody
    @RequestMapping("/list")
    public void list(){
        Page page = appReleaseService.getPage(super.getSystemParam());
        super.renderJson(page);
    }

    @ResponseBody
    @RequestMapping("/saveOrUpdate")
    public void saveOrUpdate(@Param SaveAppReleaseParam param){
        AppRelease release = param.toModel();
        appReleaseService.saveOrUpdate(super.getSystemParam(),release);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/updateStatus")
    public void saveOrUpdate(@NotNull long id,@NotNull long status){
        Status sta = Status.getNotNull(status);
        appReleaseService.updateStatus(super.getSystemParam(),id,sta);
        super.renderSuccess();
    }
}
