package cc.ccplay.booster.backend.web.game;

import cc.ccplay.booster.base.api.content.SpecialCategoryTagItemService;
import cc.ccplay.booster.backend.web.BaseBackendController;
import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.api.content.GameSpecialCategoryService;
import cc.ccplay.booster.base.dto.SelectizeDto;
import cc.ccplay.booster.base.dto.content.game.SpecialCategoryTagItemDto;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.GameSpecialCategory;
import cc.ccplay.booster.base.model.content.GameTag;
import cc.ccplay.booster.base.model.content.SpecialCategoryTagItem;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.JsonUtil;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/game/specialCategory")
public class GameSpecialCategoryController extends BaseBackendController {


    @Reference
    private GameSpecialCategoryService gameSpecialCategoryService;

    @Reference
    private SpecialCategoryTagItemService specialCategoryTagItemService;

    @RequestMapping("/index")
    public String index(){
        return "/game/specialCategory/list";
    }

    @RequestMapping("/goEditPage")
    public String goEditPage(HttpServletRequest request, @NotNull long id){
        GameSpecialCategory category = gameSpecialCategoryService.get(id);
        if(category == null){
            throw new GenericException("数据不存在");
        }
        request.setAttribute("category",category);
        return "/game/specialCategory/edit";
    }

    @RequestMapping("/goAddPage")
    public String goAddPage(){
        return  "/game/specialCategory/edit";
    }

    @ResponseBody
    @RequestMapping("/list")
    public void list(@Param String name){
        Page page = gameSpecialCategoryService.getPage(super.getSystemParam(),name);
        renderJson(page);
    }

    @ResponseBody
    @RequestMapping("/updateStatus")
    public void updateStatus(@NotNull long status,@NotNull long id){
        Status s = Status.getNotNull(status);
        gameSpecialCategoryService.updateStatus(id,s);
        renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/saveOrUpdate")
    public void saveOrUpdate(@NotNull String name, @Param Long id,@NotNull Long ordering){
        GameSpecialCategory category = new GameSpecialCategory();
        category.setId(id);
        category.setName(name);
        category.setOrdering(ordering);
        gameSpecialCategoryService.saveOrUpdate(category);
        renderSuccess();
    }


    @RequestMapping("/goTagListPage")
    public String goTagListPage(HttpServletRequest request,@NotNull long categoryId){
        request.setAttribute("categoryId",categoryId);
        return "/game/specialCategory/tagList";
    }

    @RequestMapping("/goAddBindTagPage")
    public String goAddBindTagPage(HttpServletRequest request,@NotNull long categoryId){
        request.setAttribute("categoryId",categoryId);
        return "/game/specialCategory/bindTag";
    }

    @RequestMapping("/goEditBindTagPage")
    public String goAddBindTagPage(HttpServletRequest request,@NotNull long categoryId,@NotNull long tagId){
        SpecialCategoryTagItemDto dto = specialCategoryTagItemService.getDto(categoryId,tagId);
        if(dto == null){
            throw new GenericException("数据不存在");
        }
        GameTag tag = dto.getGameTag();
        SelectizeDto selectizeDto = new SelectizeDto(tag.getId(),tag.getName());
        request.setAttribute("tagJson", JsonUtil.toJson(selectizeDto));
        request.setAttribute("categoryId",categoryId);
        request.setAttribute("item",dto.getItem());
        return "/game/specialCategory/bindTag";
    }

    @RequestMapping("/bindTag")
    @ResponseBody
    public void bindTag(@NotNull long categoryId,@NotNull long tagId,@NotNull long ordering){
        SpecialCategoryTagItem item = new SpecialCategoryTagItem();
        SpecialCategoryTagItem.Id id = new SpecialCategoryTagItem.Id(categoryId,tagId);
        item.setId(id);
        item.setOrdering(ordering);
        specialCategoryTagItemService.saveOrUpdate(item);
        super.renderSuccess();
    }

    @RequestMapping("/unbindTag")
    @ResponseBody
    public void unbindTag(@NotNull long categoryId,@NotNull long tagId){
        specialCategoryTagItemService.delete(categoryId,tagId);
        super.renderSuccess();
    }

    @RequestMapping("/getTagList")
    @ResponseBody
    public void getTagList(@NotNull long categoryId,@Param String tagName){
        Page page = specialCategoryTagItemService.getTagPage(super.getSystemParam(),categoryId,tagName);
        super.renderJson(page);
    }
}
