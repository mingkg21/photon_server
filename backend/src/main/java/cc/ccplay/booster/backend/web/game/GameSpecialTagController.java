package cc.ccplay.booster.backend.web.game;


import cc.ccplay.booster.backend.web.BaseBackendController;
import cc.ccplay.booster.base.api.content.GameInfoService;
import cc.ccplay.booster.base.api.content.GameTagService;
import cc.ccplay.booster.base.dto.SelectizeDto;
import cc.ccplay.booster.base.dto.content.game.GameTagItemDto;
import cc.ccplay.booster.base.dto.content.game.TagTagItemDto;
import cc.ccplay.booster.base.model.content.GameInfo;
import cc.ccplay.booster.base.model.content.GameTag;
import cc.ccplay.booster.base.model.content.GameTagItem;
import cc.ccplay.booster.base.model.content.TagTagItem;
import com.alibaba.dubbo.config.annotation.Reference;
import cc.ccplay.booster.base.annotation.CheckType;
import cc.ccplay.booster.base.annotation.Permission;
import cc.ccplay.booster.base.api.content.TagTagItemService;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.*;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.JsonUtil;
import org.easyj.frame.util.MixUtil;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RequestMapping("/game/specialTag")
@Controller
public class GameSpecialTagController extends BaseBackendController {

    @Reference
    private GameTagService gameTagService;

    @Reference
    private GameInfoService gameInfoService;

    @Reference
    private TagTagItemService tagTagItemService;


    @RequestMapping("/index")
    public String index(){
        return "/game/specialTag/list";
    }

    @RequestMapping("/goEditPage")
    public String goEditPage(HttpServletRequest request, @NotNull long id){
        GameTag tag = gameTagService.get(super.getSystemParam(),id);
        if(tag == null){
            throw new GenericException("数据不存在");
        }
        request.setAttribute("tag",tag);
        return "/game/specialTag/edit";
    }

    @RequestMapping("/goAddPage")
    public String goAddPage(){
        return "/game/specialTag/edit";
    }


    @ResponseBody
    @RequestMapping("/list")
    public void list(@Param String name,@Param Long status){
        GameTag searchParam = new GameTag();
        searchParam.setType(GameTag.TYPE_SPECIAL);
        searchParam.setName(name);
        if(status != null) {
            searchParam.setStatus(Status.get(status));
        }
        Page page = gameTagService.getPage(super.getSystemParam(),searchParam);
        super.renderJson(page);
    }

    @Permission(CheckType.OPEN)
    @ResponseBody
    @RequestMapping("/getForSelectize")
    public void getForSelectize(@Param String key){
        GameTag searchParam = new GameTag();
        searchParam.setName(key);
        searchParam.setType(GameTag.TYPE_SPECIAL);
        searchParam.setStatus(Status.ENABLED);
        List<SelectizeDto> list = gameTagService.getForSelectize(super.getSystemParam(),searchParam);
        super.renderJson(list);
    }

    @ResponseBody
    @RequestMapping("/updateStatus")
    public void updateStatus(@NotNull long id,@NotNull long status){
        Status s = Status.getNotNull(status);
        gameTagService.updateStatus(super.getSystemParam(),id,s);
        renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/saveOrUpdate")
    public void saveOrUpdate(@NotNull String name, @Param Long id,@Param String description,
                             @NotNull String image,@NotNull String icon,@NotNull long ordering){
        GameTag tag  = new GameTag();
        tag.setId(id);
        tag.setName(name);
        tag.setImage(image);
        tag.setDescription(description);
        tag.setOrdering(ordering);
        tag.setIcon(icon);
        tag.setType(GameTag.TYPE_SPECIAL);
        gameTagService.saveOrUpdate(super.getSystemParam(),tag);
        renderSuccess();
    }
    @ResponseBody
    @RequestMapping("/bindGame")
    public void bindGame(@NotNull long tagId,@NotNull long gameId,@NotNull long ordering){
        gameTagService.bindGame(tagId,gameId,ordering);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/unbindGame")
    public void unbindGame(@NotNull long tagId,@NotNull long gameId){
        gameTagService.unbindGame(tagId,gameId);
        super.renderSuccess();
    }

    @ResponseBody
    @RequestMapping("/getGameList")
    public void getGameList(@NotNull long tagId,@Param Long gameId,
                            @Param String packageName,@Param String gameName){
        GameInfo serchModel = new GameInfo();
        serchModel.setId(gameId);
        serchModel.setPackageName(packageName);
        serchModel.setName(gameName);
        super.renderJson(gameInfoService.getPage(super.getSystemParam(),serchModel,tagId,null,null));
    }


    @RequestMapping("/goBindGamePage")
    public String goBindGamePage(@NotNull long tagId){
        super.getRequest().setAttribute("tagId",tagId);
        return "/game/specialTag/bindGame";
    }

    @RequestMapping("/goEditBindGamePage")
    public String goEditBindGamePage(@NotNull long tagId,@NotNull long gameId){
        GameTagItem.Id id = new GameTagItem.Id(gameId,tagId);
        GameTagItemDto item = gameTagService.getItemDto(id);
        if(item == null){
            throw new GenericException("数据不存在");
        }
        super.getRequest().setAttribute("tagId",tagId);
        super.getRequest().setAttribute("item",item.getGameTagItem());
        super.getRequest().setAttribute("gameInfo",item.getGameInfo());
        super.getRequest().setAttribute("versionInfo",item.getVersionInfo());
        return "/game/specialTag/bindGame";
    }

    @RequestMapping("/goGameListPage")
    public String goGameListPage(@NotNull long tagId){
        HttpServletRequest request = getRequest();
        request.setAttribute("tagId",tagId);
        return "/game/specialTag/gameList";
    }



    @RequestMapping("/goTagListPage")
    public String goTagListPage(HttpServletRequest request,@NotNull long mainTagId){
        request.setAttribute("mainTagId",mainTagId);
        return "/game/specialTag/tagList";
    }

    @RequestMapping("/goAddBindTagPage")
    public String goAddBindTagPage(HttpServletRequest request,@NotNull long mainTagId){
        request.setAttribute("mainTagId",mainTagId);
        return "/game/specialTag/bindTag";
    }

    @RequestMapping("/goEditBindTagPage")
    public String goEditBindTagPage(HttpServletRequest request,@NotNull long mainTagId,@NotNull long tagId){
        TagTagItemDto dto = tagTagItemService.getDto(mainTagId,tagId);
        if(dto == null){
            throw new GenericException("数据不存在");
        }
        GameTag tag = dto.getGameTag();
        SelectizeDto selectizeDto = new SelectizeDto(tag.getId(),tag.getName());
        request.setAttribute("tagJson", JsonUtil.toJson(selectizeDto));
        request.setAttribute("mainTagId",mainTagId);
        request.setAttribute("item",dto.getItem());
        return "/game/specialTag/bindTag";
    }

    @RequestMapping("/bindTag")
    @ResponseBody
    public void bindTag(@NotNull long mainTagId,@NotNull long tagId,@NotNull long ordering){
        TagTagItem item = new TagTagItem();
        TagTagItem.Id id = new TagTagItem.Id(mainTagId,tagId);
        item.setId(id);
        item.setOrdering(ordering);
        tagTagItemService.saveOrUpdate(item);
        super.renderSuccess();
    }

    @RequestMapping("/unbindTag")
    @ResponseBody
    public void unbindTag(@NotNull long mainTagId,@NotNull long tagId){
        tagTagItemService.delete(mainTagId,tagId);
        super.renderSuccess();
    }

    @RequestMapping("/getTagList")
    @ResponseBody
    public void getTagList(@NotNull long mainTagId,@Param String tagName){
        Page page = tagTagItemService.getTagPage(super.getSystemParam(),mainTagId,tagName);
        super.renderJson(page);
    }


    @RequestMapping("/goBatchBindTagPage")
    public String goBatchBindTagPage(HttpServletRequest request,@NotNull long tagId){
        request.setAttribute("tagId",tagId);
        return "/game/specialTag/batchBindTag";
    }


    /**
     * 批量绑定特色标签 关联游戏的普通标签
     * @param request
     * @param tagId
     * @param count
     */
    @RequestMapping("/batchBindTag")
    @ResponseBody
    public void batchBindTag(HttpServletRequest request,@NotNull long tagId,@NotNull int count){
        int total = gameTagService.batchBindTag(tagId,count);
        renderJson(MixUtil.newHashMap("success",true,"msg","成功导入"+total+"个标签"));
    }
}
