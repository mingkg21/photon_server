package cc.ccplay.booster.backend.param;

import cc.ccplay.booster.base.dto.content.AppTopicITemContent;
import cc.ccplay.booster.base.model.content.AppTopicItem;
import org.easyj.frame.util.JsonUtil;
import org.easyj.frame.validation.annotation.NotNull;
import org.easyj.frame.validation.annotation.Param;

public class AppTopicItemParam {

    public AppTopicItem toModel(){
        AppTopicItem item = new AppTopicItem();
        item.setId(id);
        item.setOrdering(ordering);
        item.setTopicId(this.topicId);

        if(this.objectType == AppTopicItem.OBJECT_TYPE_APP) {
            item.setObjectId(this.appId);
        }
        item.setObjectType(this.objectType);

        AppTopicITemContent content = new AppTopicITemContent();

        if(this.objectType == AppTopicItem.OBJECT_TYPE_VIDEO || this.objectType == AppTopicItem.OBJECT_TYPE_IMAGE) {
            content.setImgUrl(imgUrl);
            content.setHeight(this.height);
            content.setWidth(this.width);
        }

        if(this.objectType == AppTopicItem.OBJECT_TYPE_VIDEO) {
            content.setVideoUrl(videoUrl);
        }

        if(this.objectType == AppTopicItem.OBJECT_TYPE_TEXT) {
            content.setText(text);
            content.setFontSize(fontSize);
            content.setColor(color);
            content.setBold(bold);
            content.setLink(link);
        }
        item.setContent(JsonUtil.toJson(content));
        return item;
    }


    @Param
    private Long id;

    @NotNull
    private Long topicId;

    @Param
    private String text;

    @NotNull
    private Long ordering;

    @NotNull
    private long objectType;

    @Param
    private Long appId;

    @Param
    private String imgUrl;

    @Param
    private String videoUrl;

    @Param
    private Long fontSize;

    @Param
    private String color;

    @Param
    private Long bold;

    @Param
    private String link;

    @Param
    private Long height;

    @Param
    private Long width;


    public Long getHeight() {
        return height;
    }

    public void setHeight(Long height) {
        this.height = height;
    }

    public Long getWidth() {
        return width;
    }

    public void setWidth(Long width) {
        this.width = width;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTopicId() {
        return topicId;
    }

    public void setTopicId(Long topicId) {
        this.topicId = topicId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getOrdering() {
        return ordering;
    }

    public void setOrdering(Long ordering) {
        this.ordering = ordering;
    }

    public long getObjectType() {
        return objectType;
    }

    public void setObjectType(long objectType) {
        this.objectType = objectType;
    }


    public Long getAppId() {
        return appId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public Long getFontSize() {
        return fontSize;
    }

    public void setFontSize(Long fontSize) {
        this.fontSize = fontSize;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Long getBold() {
        return bold;
    }

    public void setBold(Long bold) {
        this.bold = bold;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
