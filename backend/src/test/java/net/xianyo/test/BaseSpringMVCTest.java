package net.xianyo.test;

import cc.ccplay.booster.base.util.SpringUtil;
import org.easyj.frame.FrameProperites;
import org.easyj.frame.jdbc.DBType;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)  //使用junit4进行测试  
@ContextConfiguration   
({"classpath:applicationContext.xml","classpath:dispatcher-servlet.xml"}) //加载配置文件
public class BaseSpringMVCTest {
	 // 模拟request,response  
    protected MockHttpServletRequest request;  
    protected MockHttpServletResponse response;
    
    @Autowired
    protected ApplicationContext ctx;
    
    // 执行测试方法之前初始化模拟request,response  
    @Before    
    public void setUp(){
        SpringUtil su = new SpringUtil();
        su.setApplicationContext(ctx);
        request = new MockHttpServletRequest();      
        request.setCharacterEncoding("UTF-8");      
        response = new MockHttpServletResponse();
//        SysThreadData data = new SysThreadData();
//        data.setRequest(request);
//        data.setResponse(response);
//
//        ThreadVariable.set(data);
        
        FrameProperites.JDBC_SHOW_SQL = true;
        FrameProperites.DB_TYPE = DBType.POSTGRESQL;
//        try{
//        	RedisAPI.initPool();
//        }catch(Exception e){
//        	e.printStackTrace();
//        	System.out.println("Redis启动失败");
//        	System.exit(0);
//        }
    }
    

}
