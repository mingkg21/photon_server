package net.xianyo.test;

import org.easyj.frame.util.JsonUtil;
import org.junit.Test;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class RedisTest extends BaseJunit4Test{

    @Resource(name = "redisTemplate")
    private ValueOperations<String,String> redisTemplate;

    @Test
    public void test(){

//        Map<Long ,String> map = new HashMap<>();
//        map.put(1L,"anc");
//        map.put(2L,"abc");
//        redisTemplate.set("abc", JsonUtil.toJson(map));

       String json = (String)redisTemplate.get("permission_map");
        System.out.println("start:"+json);
       Map map = JsonUtil.toBean(json.trim(),Map.class);
       String c = JsonUtil.toJson(map);
       System.out.println("c:"+c);
        redisTemplate.set("c",c, 1, TimeUnit.HOURS);
        String json2 = (String)redisTemplate.get("c");
        System.out.println("start:"+json2);
//
//        String abc = (String)redisTemplate.get("abc");
//       System.out.println("start:"+json);
//        System.out.println("start:"+json.trim());
//        System.out.println("start:"+abc);
    }

}
