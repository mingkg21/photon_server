package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.BaseService;
import cc.ccplay.booster.base.dto.content.game.GameInfoDto;
import cc.ccplay.booster.base.enums.BoosterCacheKey;
import cc.ccplay.booster.base.model.content.GamePackageMapping;
import cc.ccplay.booster.base.model.content.GameTag;
import cc.ccplay.booster.content.dao.GamePackageMappingDao;
import cc.ccplay.booster.content.dao.GameTagDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BaseContentService extends BaseService {

    @Value("${deploy.mode}")
    protected String deployMode;

    @Resource(name = "contentRedisTemplate")
    protected RedisTemplate<String,String> redisTemplate;


    @Autowired
    private GameTagDao gameTagDao;


    @Autowired
    private GamePackageMappingDao gamePackageMappingDao;

    protected void fillGameTag(Page<GameInfoDto> page){
        List<GameInfoDto> result = page.getList();
        fillGameTag(result);
    }

    protected void fillGameTag(List<GameInfoDto> result){
        int size = result.size();
        for (int i = 0; i < size; i++) {
            GameInfoDto gameInfoDto = result.get(i);
            gameInfoDto.setTags(gameTagDao.getTagsByGameId(gameInfoDto.getGameId(), GameTag.TYPE_COMMON));
        }
    }


    protected void setPackageMapping(List<GameInfoDto> result){
        setPackageMapping(result, new HashMap<Long, String>());
    }

    protected void setPackageMapping(List<GameInfoDto> result, Map<Long, String> pnMap){
        int size = result.size();
        for (int i = 0; i < size; i++) {
            GameInfoDto dto = result.get(i);
            dto.setLocalPackageName(pnMap.get(dto.getGameId()));
            dto.setMapping(this.gamePackageMappingDao.getMappingByGameId(dto.getGameId()));
        }
    }


    public void deletePackageMappingCache(GamePackageMapping mapping){
        if(mapping == null){
            return;
        }
        String pn = mapping.getPackageName();
        String key = BoosterCacheKey.PACKAGE_NAME_MAPPING.getKey(pn);
        if(StringUtil.equals(mapping.getType(),GamePackageMapping.TYPE_ACCURATE)){
            redisTemplate.delete(key);
        }else{
            redisTemplate.delete(key+"*");
        }
    }
}
