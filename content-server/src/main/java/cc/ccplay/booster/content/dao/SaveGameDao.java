package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.model.content.SaveGame;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.easyj.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class SaveGameDao extends BaseDao<SaveGame> {

    public Page getPage(Long gameId,String packageName,String name) {
        StringBuffer sql = new StringBuffer(" select a.game_id,b.name,b.package_name,a.update_time from save_game a ");
        sql.append(" left join game_info b ");
        sql.append(" on(a.game_id = b.id) ");
        sql.append(" where 1 = 1 ");

        Map<String,Object> paramMap = new HashMap<>();

        if(gameId != null){
            sql.append(" and a.game_id = :gameId ");
            paramMap.put("gameId",gameId);
        }

        if(StringUtil.isNotEmpty(packageName)){
            sql.append(" and b.package_name = :packageName ");
            paramMap.put("packageName",packageName);
        }

        if(StringUtil.isNotEmpty(name)){
            sql.append(" and b.name = :name ");
            paramMap.put("name",name);
        }

        return super.page2CamelMap(sql.toString(),paramMap);
    }

    public SaveGame getByPackageName(String packageName){
        StringBuffer sql = new StringBuffer(" select a.* from save_game a left join game_info b  on(a.game_id = b.id)  ");
        sql.append(" where b.package_name = :packageName ");
        return super.query21Model(sql.toString(),MixUtil.newHashMap("packageName",packageName));
    }

}
