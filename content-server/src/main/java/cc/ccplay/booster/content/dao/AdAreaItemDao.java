package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.model.content.AdAreaItem;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

@Repository
public class AdAreaItemDao extends BaseDao<AdAreaItem>{

    public Page getPage(long adId){
        String sql = "select * from ad_area_item where ad_id = :adId order by ordering,id ";
        return super.page2CamelMap(sql, MixUtil.newHashMap("adId",adId));
    }

    public Page<AdAreaItem> getModelPage(long adId){
        String sql = "select * from ad_area_item where ad_id = :adId order by ordering,id ";
        return super.paged2Obj(sql, MixUtil.newHashMap("adId",adId));
    }

}
