package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.BaseService;
import cc.ccplay.booster.base.api.content.BoosterServerPortService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.BoosterServerPort;
import cc.ccplay.booster.content.dao.BoosterServerPortDao;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@com.alibaba.dubbo.config.annotation.Service(interfaceClass = BoosterServerPortService.class)
public class BoosterServerPortServiceImpl extends BaseService implements BoosterServerPortService {

    @Autowired
    private BoosterServerPortDao boosterServerPortDao;

    @Override
    public BoosterServerPort get(long id) {
        return boosterServerPortDao.get(id);
    }

    @Override
    public void saveOrUpdate(BoosterServerPort port) {
        if(boosterServerPortDao.isNotUnique(port,"serverId,port")){
            throw new GenericException("端口["+port.getPort()+"]已存在");
        }
        boosterServerPortDao.saveOrUpdate(port);
    }

    @Override
    public void delete(long id) {
        boosterServerPortDao.delete(id);
    }

    @Override
    public Page getPage(SystemParam param,long serverId) {
        return boosterServerPortDao.getPage(serverId);
    }

    @Override
    public void updateStatus(long id, Status status) {
        BoosterServerPort updateModel = new BoosterServerPort();
        updateModel.setStatus(status.getValue());
        updateModel.setId(id);
        boosterServerPortDao.updateNotNull(updateModel);
    }
}
