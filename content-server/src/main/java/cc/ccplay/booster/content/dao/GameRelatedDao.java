package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.dto.content.game.GameInfoDto;
import cc.ccplay.booster.base.model.content.GameInfo;
import cc.ccplay.booster.base.model.content.GameRelated;
import cc.ccplay.booster.base.model.content.GameVersion;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.*;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class GameRelatedDao extends BaseDao<GameRelated> {

    public Page getPage(long gameId) {
        String sql = " select a.related_game_id,b.name,c.icon,a.ordering from game_related a " +
                " left join game_info b on(a.related_game_id = b.id ) " +
                " left join game_version c on(b.lastest_version_id = c.id ) " +
                " where a.game_id = :gameId order by ordering ";

        return super.page2CamelMap(sql, MixUtil.newHashMap("gameId",gameId));
    }



    public List<GameInfoDto> getRelatedGame(long gameId, int maxSize){
        StringBuffer sql = new StringBuffer();
        sql.append("select  ");
        sql.append(super.getSelectColStr(GameInfo.class,"a","gameInfo")+",");
        sql.append(super.getSelectColStr(GameVersion.class,"b","versionInfo")+" ");
//        sql.append(super.getSelectColStr(GameCategory.class,"c","category")+",");
       // sql.append(super.getSelectColStr(GamePublisher.class,"d","publisher"));
        sql.append(" FROM game_related rl ");
        sql.append(" LEFT JOIN game_info a ");
        sql.append(" on(a.id = rl.related_game_id ) ");
        sql.append(" LEFT JOIN game_version b ");
        sql.append(" ON(a.lastest_version_id = b.id ) ");
//        sql.append(" LEFT JOIN game_category c ");
//        sql.append(" ON(c.id = a.category_id ) ");
//        sql.append(" LEFT JOIN game_publisher d ");
//        sql.append(" ON(d.id = a.publisher_id ) ");
        sql.append(" where a.delete_flag = 0 and a.status = " + Status.ENABLED.getValue() );
        sql.append(" and rl.game_id = :gameId ");
        sql.append(" order by rl.ordering ");
        sql.append(" limit :maxSize ");
        return super.query2Model(sql.toString(),MixUtil.newHashMap("gameId",gameId,"maxSize",maxSize),GameInfoDto.class);
    }

}
