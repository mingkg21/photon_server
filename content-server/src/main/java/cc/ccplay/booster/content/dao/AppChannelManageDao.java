package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.AppChannelManage;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.springframework.stereotype.Repository;

@Repository
public class AppChannelManageDao extends BaseDao<AppChannelManage> {

    public Page getPage(){
        String sql = " select cm.*, c.name from app_channel_manage cm left join app_channel c on(cm.code = c.code) order by cm.id desc ";
        return super.page2CamelMap(sql);
    }

    public AppChannelManage findAppChannelManage(String code) {
        String sql = " select * from app_channel_manage where status = "+ Status.ENABLED.getValue() + " and code = '" + code +  "' limit 1 ";
        return super.query21Model(sql);
    }

}
