package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.BucketVideoService;
import cc.ccplay.booster.base.api.user.SysUserService;
import cc.ccplay.booster.base.model.content.BucketVideo;
import cc.ccplay.booster.content.dao.BucketVideoDao;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.user.SysUser;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.jdbc.model.User;
import org.springframework.beans.factory.annotation.Autowired;

@Service(interfaceClass=BucketVideoService.class)
@org.springframework.stereotype.Service
public class BucketVideoServiceImpl implements BucketVideoService{

    @Reference
    private SysUserService sysUserService;

    @Autowired
    private BucketVideoDao bucketVideoDao;

    @Override
    public BucketVideo save(SystemParam param, BucketVideo video) {
        User loginUser = param.getUser();
        if(loginUser != null){
            video.setCreateUserId(loginUser.getSysUserId());
            video.setAccount(loginUser.getLoginAccount());
        }else{
            Long userId = video.getCreateUserId();
            if(userId != null){
                SysUser sysUser = sysUserService.getUser(param,userId);
                if(sysUser != null){
                    video.setAccount(sysUser.getAccount());
                }
            }
        }
        return bucketVideoDao.save(video);
    }

    @Override
    public Page queryPage(SystemParam param, BucketVideo searchObj) {
        return bucketVideoDao.getPage(searchObj);
    }
}
