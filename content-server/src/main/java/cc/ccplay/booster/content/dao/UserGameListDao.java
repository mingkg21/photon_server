package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.dto.content.game.GameInfoDto;
import cc.ccplay.booster.base.enums.DeleteFlag;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.GameInfo;
import cc.ccplay.booster.base.model.content.GameVersion;
import cc.ccplay.booster.base.model.content.UserGameList;
import cc.ccplay.booster.base.model.content.*;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class UserGameListDao extends BaseDao<UserGameList>{

    public Page<GameInfoDto> getPlayGamePage(long userId){
        StringBuffer sql = new StringBuffer();
        sql.append("select  ");
        sql.append(super.getSelectColStr(GameInfo.class,"a","gameInfo")+",");
        sql.append(super.getSelectColStr(GameVersion.class,"b","versionInfo")+" ");
        sql.append(" FROM user_game_list e ");
        sql.append(" LEFT JOIN game_info a ");
        sql.append(" ON(e.game_id = a.id) ");
        sql.append(" LEFT JOIN game_version b ");
        sql.append(" ON(a.lastest_version_id = b.id ) ");
        sql.append(" where e.user_id = :userId ");
        sql.append(" and a.delete_flag =  "+ DeleteFlag.NO_DELETE+" ");
        sql.append(" and a.status = " + Status.ENABLED.getValue() +" ");
        sql.append(" and b.id is not null ");
        sql.append(" order by e.create_time desc ");
        Map paramMap = new HashMap();
        paramMap.put("userId",userId);
        return super.paged2Obj(sql.toString(),paramMap,GameInfoDto.class);
    }

}
