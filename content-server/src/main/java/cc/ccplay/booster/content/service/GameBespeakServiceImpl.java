package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.GameBespeakService;
import cc.ccplay.booster.base.model.content.GameBespeak;
import cc.ccplay.booster.base.model.content.GameVersion;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.game.GameBespeakDto;
import cc.ccplay.booster.base.model.content.GameBespeakRecord;
import cc.ccplay.booster.base.model.content.GameInfo;
import cc.ccplay.booster.content.dao.GameInfoDao;
import cc.ccplay.booster.content.dao.GameBespeakRecordDao;
import cc.ccplay.booster.content.dao.GameVersionDao;
import cc.ccplay.booster.content.dao.GameBespeakDao;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@com.alibaba.dubbo.config.annotation.Service(interfaceClass = GameBespeakService.class)
public class GameBespeakServiceImpl extends BaseContentService implements GameBespeakService{

    @Autowired
    private GameBespeakDao gameBespeakDao;

    @Autowired
    private GameInfoDao gameInfoDao;

    @Autowired
    private GameVersionDao gameVersionDao;

    @Autowired
    private GameBespeakRecordDao gameBespeakRecordDao;

    @Override
    public Page getPage(SystemParam param) {
        return gameBespeakDao.getPage();
    }

    @Override
    public Page<GameBespeakDto> getDtoPage(SystemParam param) {
        return gameBespeakDao.getDtoPage();
    }

    @Override
    public GameBespeak saveOrUpdate(SystemParam param, GameBespeak bespeak) {
        if(gameBespeakDao.isNotUnique(bespeak,"versionId")){
            throw new GenericException("该版本已经存在");
        }
        return gameBespeakDao.saveOrUpdate(bespeak);
    }

    @Override
    public GameBespeak get(SystemParam param, long id) {
        return gameBespeakDao.get(id);
    }

    @Override
    public GameBespeakDto getEditInfo(SystemParam param, long id) {
        GameBespeak gameBespeak = gameBespeakDao.get(id);
        if(gameBespeak == null){
            throw new GenericException("预约信息不存在");
        }
        GameInfo gameInfo = gameInfoDao.get(gameBespeak.getId());
        if(gameInfo == null){
            throw new GenericException("游戏信息不存在");
        }
        GameVersion gameVersion = gameVersionDao.get(gameBespeak.getVersionId());
        if(gameVersion == null){
            throw new GenericException("游戏版本信息不存在");
        }
        GameBespeakDto info = new GameBespeakDto();
        info.setBespeak(gameBespeak);
        info.setGameInfo(gameInfo);
        info.setVersionInfo(gameVersion);
        return info;
    }

    /**
     * 预约
     *
     * @param param
     * @param bespeakId
     */
    @Override
    public void bespeak(SystemParam param, long bespeakId) {
        GameBespeak gameBespeak = gameBespeakDao.get(bespeakId);
        if(gameBespeak == null){
            throw new GenericException("游戏预约不存在");
        }

        if(!StringUtil.equals(GameBespeak.BESPEAK_STATUS_OPEN,gameBespeak.getBespeakStatus())){
            throw new GenericException("未开发预约");
        }

        GameBespeakRecord.Id recordId = new GameBespeakRecord.Id(bespeakId,param.getUserId());
        GameBespeakRecord record = gameBespeakRecordDao.get(recordId);
        if(record != null){
            throw new GenericException("不能重复预约");
        }
        record = new GameBespeakRecord();
        record.setId(recordId);
        record.setGameId(gameBespeak.getGameId());
        record.setVersionId(gameBespeak.getVersionId());
        gameBespeakRecordDao.save(record);
        gameBespeakDao.addBespeakCount(bespeakId);
        //gameBespeak.get
    }
}
