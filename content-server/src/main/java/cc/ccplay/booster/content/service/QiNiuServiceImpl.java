package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.dto.QiNiuAuth;
import cc.ccplay.booster.base.model.content.*;
import cc.ccplay.booster.content.dao.BucketFileDao;
import cc.ccplay.booster.content.dao.BucketPackageDao;
import cc.ccplay.booster.content.dao.BucketVideoDao;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.model.content.*;
import cc.ccplay.booster.content.dao.BucketImageDao;
import com.qiniu.util.Auth;
import com.qiniu.util.StringMap;
import com.qiniu.util.UrlSafeBase64;
import cc.ccplay.booster.base.api.content.QiNiuService;
import cc.ccplay.booster.base.core.SystemParam;
import org.easyj.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@Service(interfaceClass=QiNiuService.class)
@org.springframework.stereotype.Service
public class QiNiuServiceImpl implements QiNiuService{

    @Value("${cdn.accessKey}")
    private String accessKey;

    @Value("${cdn.secretKey}")
    private String secretKeySpec;

    @Value("${cdn.defaultBucketName}")
    private String bucket;

    @Value("${cdn.base.url}")
    private String baseUrl;

    @Value("${cdn.server}")
    private String cdnServer;


    private final static long TOKEN_TIMEOUT_SEC = 24*3600L;  //一天

    @Autowired
    private BucketVideoDao bucketVideoDao;

    @Autowired
    private BucketPackageDao bucketPackageDao;

    @Autowired
    private BucketFileDao bucketFileDao;

    @Autowired
    private BucketImageDao bucketImageDao;

    /**
     * 获取上传接口
     *
     * @param param
     * @return
     */
    @Override
    public QiNiuAuth getUploadImageToken(SystemParam param) {
       return getUploadImageToken(param,null);
    }

    @Override
    public QiNiuAuth getUploadImageToken(SystemParam param, String fileName) {
        String key = createImageKey(param,fileName);
        Auth auth = Auth.create(accessKey,secretKeySpec);
        QiNiuAuth qiNiuAuth = new QiNiuAuth();
        String token = StringUtil.isEmpty(fileName) ? auth.uploadToken(bucket) : auth.uploadToken(bucket,key);
        qiNiuAuth.setApiUrl(this.cdnServer);
        qiNiuAuth.setToken(token);
        qiNiuAuth.setFileKey(key);
        return qiNiuAuth;
    }


    public static void main(String []args){
        Auth auth = Auth.create("qccs-p-KPHdNnj8LDOoABYq--ZJfXnJmlsVENXPQ","FReXnL7V-zSa8uqhLnV7Iny-rFFIbjzfWFYh_i0w");
        StringMap map = auth.authorization("/delete/eGlhbnlvOnRlc3RfZm9yX2RlbGV0ZS5wbmc=");
        System.out.println(map.get("Authorization"));
    }

    @Override
    public QiNiuAuth getUploadVideoToken(SystemParam param) {
        return getUploadVideoToken(param,null);
    }


    public QiNiuAuth getUploadVideoToken(SystemParam param,String fileName){

        //String key = createVideoKey();
        String key = createVideoKey(param,fileName);
        if(StringUtil.isEmpty(fileName)){
            key = key +"/"+UUID.randomUUID().toString();
        }

        String m3u8Url = null;
        if(StringUtil.isNotEmpty(fileName)) {
            int index = key.lastIndexOf(".");
            if(index == -1){
                m3u8Url = key+".m3u8";
            }else{
                m3u8Url = key.substring(0,index) + ".m3u8";
            }
        }else{
            m3u8Url = key+".m3u8";
        }
        String saveMp4Entry = String.format("%s:"+m3u8Url, bucket);
        String callbackBody = "{ \"m3u8Url\":\"" + m3u8Url + "\", \"key\":\"$(key)\",\"persistentId\":\"$(persistentId)\", \"avinfo\":$(avinfo)}";

        String avthumbMp4Fop = String.format("avthumb/m3u8/noDomain/1/segtime/20/vb/3000k|saveas/%s", UrlSafeBase64.encodeToString(saveMp4Entry));
        StringMap policy = new StringMap();
        policy.put("persistentOps", avthumbMp4Fop);
        policy.put("callbackBodyType","application/json");
        policy.put("callbackBody",callbackBody);
        //数据处理队列名称，必填
        policy.put("persistentPipeline", "mypipeline-hanhua");
        Auth auth = Auth.create(accessKey,secretKeySpec);
        String token = null;
        if(StringUtil.isNotEmpty(fileName)) {
            token = auth.uploadToken(bucket,key , TOKEN_TIMEOUT_SEC,policy);
        }else{
            token = auth.uploadToken(bucket,null, TOKEN_TIMEOUT_SEC,policy);
        }
        QiNiuAuth qiNiuAuth = new QiNiuAuth();
        qiNiuAuth.setApiUrl(this.cdnServer);
        qiNiuAuth.setToken(token);
        qiNiuAuth.setFileKey(key);
        return qiNiuAuth;
    }

    /**
     * 获取上传应用包 token
     *
     * @param param
     * @return
     */
    @Override
    public QiNiuAuth getUploadPkgToken(SystemParam param) {
        Auth auth = Auth.create(accessKey,secretKeySpec);
        QiNiuAuth qiNiuAuth = new QiNiuAuth();
        String token = auth.uploadToken(bucket,null,TOKEN_TIMEOUT_SEC,null);
        qiNiuAuth.setToken(token);
        qiNiuAuth.setApiUrl(this.cdnServer);
        qiNiuAuth.setFileKey(createPkgKey(param));
        return qiNiuAuth;
    }

    /**
     * 获取七牛cdn 地址
     *
     * @param param
     * @return
     */
    @Override
    public String getCdnUrl(SystemParam param) {
        return this.cdnServer;
    }

    private String createImageKey(SystemParam param,String fileName){
        if(StringUtil.isEmpty(fileName)) {
            return baseUrl + "/image";
        }
        //客户端上传
        return baseUrl+"/u/image/"+random()+"/"+fileName;
    }

    private String createVideoKey(SystemParam param,String fileName) {
        if(StringUtil.isEmpty(fileName)) {
            return baseUrl + "/video";
        }
        //客户端上传
        return baseUrl+"/u/video/"+random()+"/"+fileName;
    }

    private String random(){
        String uuid = UUID.randomUUID().toString();
        uuid = uuid.replaceAll("-","");
        if(uuid.length() > 10){
            uuid = uuid.substring(0,10);
        }
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        return sdf.format(date)+"/"+uuid;
    }

    private String createPkgKey(SystemParam param){
        return baseUrl+"/package";
    }


    private String createFileKey(SystemParam param,String fileName){
        if(StringUtil.isEmpty(fileName)) {
            return baseUrl + "/file";
        }
        return baseUrl+"/u/file/"+random()+"/"+fileName;
    }

    /**
     * 获取上传普通文件
     *
     * @param param
     * @return
     */
    @Override
    public QiNiuAuth getUploadFileToken(SystemParam param) {
        return getUploadFileToken(param,null);
    }

    @Override
    public QiNiuAuth getUploadFileToken(SystemParam param, String fileName) {
        String key = createFileKey(param,fileName);
        Auth auth = Auth.create(accessKey,secretKeySpec);
        QiNiuAuth qiNiuAuth = new QiNiuAuth();
        String token = getToken(auth,fileName,key);
        qiNiuAuth.setToken(token);
        qiNiuAuth.setApiUrl(this.cdnServer);
        qiNiuAuth.setFileKey(key);
        return qiNiuAuth;
    }


    private String getToken(Auth auth,String fileName,String key){
        if(StringUtil.isEmpty(fileName)){
            return auth.uploadToken(bucket, (String)null, TOKEN_TIMEOUT_SEC, (StringMap)null, true);
        }
        return auth.uploadToken(bucket,key,TOKEN_TIMEOUT_SEC, (StringMap)null, true );
    }

    @Override
    public void saveToolUploadFile(BaseFile baseFile) {
        if(baseFile instanceof BucketImage){
            bucketImageDao.save((BucketImage)baseFile);
        }else if(baseFile instanceof BucketPackage){
            bucketPackageDao.save((BucketPackage)baseFile);
        }else if(baseFile instanceof BucketVideo){
            bucketVideoDao.save((BucketVideo)baseFile);
        }else{
            bucketFileDao.save((BucketFile)baseFile);
        }
    }
}
