package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.dto.content.AppTopicItemDto;
import cc.ccplay.booster.base.model.content.AppTopicItem;
import cc.ccplay.booster.content.dao.GameInfoDao;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.api.content.AppTopicItemService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.content.GameInfo;
import cc.ccplay.booster.content.dao.AppTopicItemDao;
import cc.ccplay.booster.content.dao.GameVersionDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;

@Service(interfaceClass = AppTopicItemService.class)
@org.springframework.stereotype.Service
public class AppTopicItemServiceImpl extends BaseContentService implements AppTopicItemService {

    @Autowired
    private AppTopicItemDao appTopicItemDao;

    @Autowired
    private GameInfoDao gameInfoDao;

    @Autowired
    private GameVersionDao gameVersionDao;

    @Override
    public void saveOrUpdate(AppTopicItem item) {
        appTopicItemDao.saveOrUpdate(item);
    }

    @Override
    public Page getPage(SystemParam param, long topicId) {
        return appTopicItemDao.getPage(topicId);
    }

    @Override
    public void delete(long itemId) {
        appTopicItemDao.delete(itemId);
    }

    @Override
    public AppTopicItem get(long itemId) {
        return appTopicItemDao.get(itemId);
    }

    @Override
    public AppTopicItemDto getDto(long itemId) {
        AppTopicItem item = appTopicItemDao.get(itemId);
        if(item == null) {
            return null;
        }
        AppTopicItemDto dto = new AppTopicItemDto();
        dto.setAppTopicItem(item);

        if(StringUtil.equals(item.getObjectType(),AppTopicItem.OBJECT_TYPE_APP)){
            Long appId = item.getObjectId();
            GameInfo gameInfo = gameInfoDao.get(appId);
            if(gameInfo != null){
                dto.setGameInfo(gameInfo);
                dto.setVersionInfo(gameVersionDao.get(gameInfo.getLastestVersionId()));
            }
        }
        return dto;
    }
}
