package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.model.content.AppChannel;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.springframework.stereotype.Repository;

@Repository
public class AppChannelDao extends BaseDao<AppChannel> {

    public AppChannel getAppChannel(String code) {
        String sql = " select * from app_channel where code = '" + code + "' limit 1";
        return super.query21Model(sql);
    }

    public Page getPage(){
        String sql = " select * from app_channel order by id desc ";
        return super.page2CamelMap(sql);
    }

}
