package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.GiftNumberService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.content.GiftNumber;
import cc.ccplay.booster.content.dao.GiftInfoDao;
import cc.ccplay.booster.content.dao.GiftNumberDao;
import com.alibaba.dubbo.config.annotation.Service;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service(interfaceClass = GiftNumberService.class )
@org.springframework.stereotype.Service
public class GiftNumberServiceImpl extends BaseContentService implements GiftNumberService {

    @Autowired
    private GiftNumberDao giftNumberDao;

    @Autowired
    private GiftInfoDao giftInfoDao;

    @Override
    public void batchSave(SystemParam param, long giftId , List<GiftNumber> list) {
        int size = list.size();
        if(size == 0){
            return;
        }
        giftInfoDao.addGiftCount(giftId,size);
        giftNumberDao.batchInsert(list);
    }

    @Override
    public Page getPage(SystemParam param, long giftId) {
        return giftNumberDao.getPage(giftId);
    }
}
