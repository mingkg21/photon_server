package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.BoosterServerGroupRefService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.booster.BoosterServerGroupRefDto;
import cc.ccplay.booster.base.model.content.BoosterServer;
import cc.ccplay.booster.base.model.content.BoosterServerGroupRef;
import cc.ccplay.booster.content.dao.BoosterServerDao;
import cc.ccplay.booster.content.dao.BoosterServerGroupRefDao;
import com.alibaba.dubbo.config.annotation.Service;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;

@Service(interfaceClass = BoosterServerGroupRefService.class)
@org.springframework.stereotype.Service
public class BoosterServerGroupRefServiceImpl extends BaseContentService implements BoosterServerGroupRefService {

    @Autowired
    private BoosterServerGroupRefDao boosterServerGroupRefDao;

    @Autowired
    private BoosterServerDao boosterServerDao;

    @Override
    public void saveOrUpdate(BoosterServerGroupRef ref) {
        if(boosterServerGroupRefDao.isNotUnique(ref,"groupId,serverId")){
            throw new GenericException("已存在当前服务器,不能重新添加");
        }
        boosterServerGroupRefDao.saveOrUpdate(ref);
    }

    @Override
    public void delete(long refId) {
        boosterServerGroupRefDao.delete(refId);
    }

    @Override
    public Page getPage(SystemParam param, long groupId) {
        return boosterServerGroupRefDao.getPage(groupId);
    }


    @Override
    public BoosterServerGroupRefDto getDto(long id) {
        BoosterServerGroupRef ref = boosterServerGroupRefDao.get(id);
        if(ref == null){
            return null;
        }
        BoosterServerGroupRefDto dto = new BoosterServerGroupRefDto();
        dto.setRef(ref);
        BoosterServer server = boosterServerDao.get(ref.getServerId());
        dto.setServer(server);
        return dto;
    }
}
