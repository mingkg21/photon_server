package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.dto.SelectizeDto;
import cc.ccplay.booster.content.dao.GamePublisherDao;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.api.content.GamePublisherService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.GamePublisher;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service(interfaceClass = GamePublisherService.class)
@org.springframework.stereotype.Service
public class GamePublisherServiceImpl extends BaseContentService implements GamePublisherService {

    @Autowired
    private GamePublisherDao gamePublisherDao;

    @Override
    public Page getPage(SystemParam param,String name) {
        return gamePublisherDao.getPage(name);
    }

    @Override
    public GamePublisher saveOrUpdate(SystemParam param, GamePublisher publisher) {
        Long id = publisher.getId();
        if(id == null){
            publisher.setStatus(Status.ENABLED);
        }
        if(gamePublisherDao.isNotUnique(publisher,"name")){
            throw new GenericException("发行商["+publisher.getName()+"]已经存在");
        }
        return gamePublisherDao.saveOrUpdate(publisher);
    }

    @Override
    public int updateStatus(SystemParam param, long id, Status status) {
        GamePublisher updatePublisher = new GamePublisher();
        updatePublisher.setId(id);
        updatePublisher.setStatus(status);
        return gamePublisherDao.updateNotNull(updatePublisher);
    }

    @Override
    public GamePublisher get(SystemParam param, long id) {
        return gamePublisherDao.get(id);
    }

    @Override
    public List<SelectizeDto> getForSelectize(SystemParam param, String name) {
        Page page = gamePublisherDao.getPage(name);
        return super.toSelectizeData(page,"id","name");
    }
}
