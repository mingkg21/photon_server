package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.GamePackageMappingService;
import cc.ccplay.booster.base.constants.Constant;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.game.GameInfoDto;
import cc.ccplay.booster.base.dto.content.game.GamePackageMappingDto;
import cc.ccplay.booster.base.enums.BoosterCacheKey;
import cc.ccplay.booster.base.model.content.CustomArea;
import cc.ccplay.booster.base.model.content.GameInfo;
import cc.ccplay.booster.base.model.content.GamePackageMapping;
import cc.ccplay.booster.content.dao.*;
import cc.ccplay.booster.content.enums.AreaCode;
import com.alibaba.dubbo.config.annotation.Service;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

@Service(interfaceClass = GamePackageMappingService.class)
@org.springframework.stereotype.Service
public class GamePackageMappingServiceImpl extends BaseContentService implements GamePackageMappingService {

    @Autowired
    private GamePackageMappingDao gamePackageMappingDao;

    @Autowired
    private GameInfoDao gameInfoDao;

    @Autowired
    private GameVersionDao gameVersionDao;

    @Autowired
    private CustomAreaDao customAreaDao;

    @Override
    public void saveOrUpdate(GamePackageMapping mapping) {
        if(gamePackageMappingDao.isNotUnique(mapping,"packageName,type,gameId")){
            throw new GenericException("包名["+mapping.getPackageName()+"]配置已经存在");
        }

        Long id = mapping.getId();
        //清除缓存
        super.deletePackageMappingCache(mapping);
        if(id != null){
            GamePackageMapping oldMapping = gamePackageMappingDao.get(id);
            super.deletePackageMappingCache(oldMapping);
        }
        gamePackageMappingDao.saveOrUpdate(mapping);
    }

    @Override
    public Page getPage(SystemParam param, GamePackageMapping searchObj) {
        return gamePackageMappingDao.getPage(searchObj);
    }

    @Override
    public void delete(long id) {
        GamePackageMapping mapping = gamePackageMappingDao.get(id);
        if(mapping != null) {
            super.deletePackageMappingCache(mapping);
            gamePackageMappingDao.delete(id);
        }
    }


    @Override
    public GamePackageMappingDto getDto(long id) {
        GamePackageMapping mapping = gamePackageMappingDao.get(id);
        if(mapping == null){
            return null;
        }
        GamePackageMappingDto dto = new GamePackageMappingDto();
        dto.setMapping(mapping);
        GameInfo gameInfo = gameInfoDao.get(mapping.getGameId());
        if(gameInfo != null){
            dto.setGameInfo(gameInfo);
            dto.setVersionInfo(gameVersionDao.get(gameInfo.getLastestVersionId()));
        }
        return dto;
    }

    @Override
    public List<GameInfoDto> getMappingGame(List<String> packageNames) {
        Set<Long> gameIds = new HashSet<>();
        Set<String> pnSet = new HashSet<>();
        Map<Long, String> pnMap = new HashMap<>();
        int size = packageNames.size();

        //从缓存中读取
        for (int i = 0; i < size; i++) {
            String name = packageNames.get(i);
            String key = BoosterCacheKey.PACKAGE_NAME_MAPPING.getKey(name);
            if(redisTemplate.hasKey(key) == false){
                pnSet.add(name);
            }else{
                Set<String> values = redisTemplate.opsForSet().members(key);
                for (String gameIdStr : values) {
                    long gameId = StringUtil.toLong(gameIdStr);
                    if(gameId != 0){
                        gameIds.add(gameId);
                        pnMap.put(gameId, name);
                    }
                }
            }
        }

        //从游戏信息中读取匹配
//        if(pnSet.size() > 0) {
//            List<GameInfo> gameList = gameInfoDao.getGameIdByPackageName(setToList(pnSet));
//            loadGame(gameList, gameIds, pnSet);
//        }

        //从包名映射精确匹配获取
        if(pnSet.size() > 0) {
            List<GameInfo> gameList = gamePackageMappingDao.getGameIdByPackageName(pnSet);
            loadGame(gameList, gameIds, pnSet, pnMap);
        }

        //从包名映射模糊匹配中获取
        if(pnSet.size() > 0) {
            List<GameInfo> gameList = gamePackageMappingDao.getGameIdByLike(pnSet);
            loadGame(gameList, gameIds, pnSet, pnMap);
        }

        //设置匹配不到的缓存
        for (String pn : pnSet){
            String key = BoosterCacheKey.PACKAGE_NAME_MAPPING.getKey(pn);
            redisTemplate.opsForSet().add(key,"0");
        }

//        CustomArea customArea = customAreaDao.getByCode(AreaCode.RUN_WITHOUT_BOOSTER_GAME);
//        Long noBoosterAreaId = null;
//        if(customArea != null && StringUtil.equals(customArea.getStatus(), Constant.STATUS_ENABLED)){
//            noBoosterAreaId = customArea.getId();
//        }

        List<GameInfoDto> result = gameInfoDao.getGameDtoByGameIds(gameIds);
        setPackageMapping(result, pnMap);
        return result;
    }


    private void loadGame(List<GameInfo> gameList,Set<Long> gameIds,Set<String> pnSet,  Map<Long, String> pnMap){
        int size = gameList.size();
        for (int i = 0; i < size; i++) {
            GameInfo gi = gameList.get(i);
            String pn = gi.getPackageName();
            pnSet.remove(pn);
            String key = BoosterCacheKey.PACKAGE_NAME_MAPPING.getKey(pn);
            Long gameId = gi.getId();
            //设置缓存
            redisTemplate.opsForSet().add(key,gameId+"");
            gameIds.add(gameId);
            pnMap.put(gameId, pn);
        }
    }

//    private List<String> setToList(Set<String> set){
//        List<String> list = new ArrayList<>();
//        for(String str :set){
//            list.add(str);
//        }
//        return list;
//    }


    @Override
    public void batchSave(List<GamePackageMapping> mappings) {
        int size = mappings.size();
        for (int i = 0; i < size; i++) {
            GamePackageMapping mapping = mappings.get(i);
            //清除缓存
            super.deletePackageMappingCache(mapping);
            if(gamePackageMappingDao.isNotUnique(mapping,"packageName,type,gameId")){
                throw new GenericException("包名["+mapping.getPackageName()+"]配置已经存在");
            }
            gamePackageMappingDao.save(mapping);
        }
    }

    @Override
    public List getMappingList(long startId, int size) {
        return gamePackageMappingDao.getMappingList(startId,size);
    }
}
