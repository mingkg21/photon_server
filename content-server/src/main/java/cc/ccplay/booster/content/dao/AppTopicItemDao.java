package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.dto.content.AppTopicItemDto;
import cc.ccplay.booster.base.model.content.AppTopicItem;
import cc.ccplay.booster.base.model.content.GameInfo;
import cc.ccplay.booster.base.model.content.GameVersion;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AppTopicItemDao extends BaseDao<AppTopicItem> {

    public Page getPage(long topicId){
        String sql = "select a.*,b.name as game_name,c.icon as game_icon from app_topic_item a" +
                " left join game_info b on(a.object_id = b.id and a.object_type = "+AppTopicItem.OBJECT_TYPE_APP+") " +
                " left join game_version c on(b.lastest_version_id = c.id ) " +
                " where topic_id = :topicId order by ordering,id ";
        return super.page2CamelMap(sql, MixUtil.newHashMap("topicId",topicId));
    }


    public List<AppTopicItemDto> getItemDtoList(long topicId){
        StringBuffer sql = new StringBuffer("select ");
        sql.append(super.getSelectColStr("a","appTopicItem")+",");
        sql.append(super.getSelectColStr(GameInfo.class,"b","gameInfo")+",");
        sql.append(super.getSelectColStr(GameVersion.class,"c","versionInfo")+" ");
        sql.append(" from app_topic_item a ");
        sql.append(" left join game_info b on(a.object_id = b.id and a.object_type =  "+AppTopicItem.OBJECT_TYPE_APP+") ");
        sql.append(" left join game_version c on(b.lastest_version_id = c.id ) ");
        sql.append(" where a.topic_id = :topicId order by a.ordering,a.id ");
        return super.query2Model(sql.toString(), MixUtil.newHashMap("topicId",topicId),AppTopicItemDto.class);
    }
}
