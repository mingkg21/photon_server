package cc.ccplay.booster.content.service;

import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.api.content.AdAreaItemService;
import cc.ccplay.booster.base.constants.Constant;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.content.AdArea;
import cc.ccplay.booster.base.model.content.AdAreaItem;
import cc.ccplay.booster.content.dao.AdAreaDao;
import cc.ccplay.booster.content.dao.AdAreaItemDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;

@Service(interfaceClass = AdAreaItemService.class)
@org.springframework.stereotype.Service
public class AdAreaItemServiceImpl extends BaseContentService implements AdAreaItemService {

    @Autowired
    private AdAreaItemDao adAreaItemDao;

    @Autowired
    private AdAreaDao adAreaDao;

    @Override
    public void saveOrUpdate(AdAreaItem adAreaItem) {
        adAreaItemDao.saveOrUpdate(adAreaItem);
    }

    @Override
    public Page getPage(SystemParam param,long adId) {
        return adAreaItemDao.getPage(adId);
    }

    @Override
    public void delete(long id) {
        adAreaItemDao.delete(id);
    }

    @Override
    public AdAreaItem get(long id) {
        return adAreaItemDao.get(id);
    }

    @Override
    public Page<AdAreaItem> getPageByCode(SystemParam param, String code) {
        AdArea adArea = adAreaDao.getByCode(code);
        if(adArea == null || !StringUtil.equals(adArea.getStatus(),Constant.STATUS_ENABLED)){
            return Constant.EMPTY_PAGE;
        }
        Long id = adArea.getId();
        return adAreaItemDao.getModelPage(id);
    }
}
