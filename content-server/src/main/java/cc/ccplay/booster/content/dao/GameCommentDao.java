package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.model.content.GameComment;
import cc.ccplay.booster.base.model.content.GameInfo;
import cc.ccplay.booster.base.model.content.GameVersion;
import cc.ccplay.booster.base.dto.content.game.GameCommentDto;
import cc.ccplay.booster.base.enums.DeleteFlag;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.*;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.easyj.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class GameCommentDao extends BaseDao<GameComment> {

    public void addReplyCount(long commentId){
        String sql = "update game_comment set reply_count = reply_count + 1 where id = :commentId ";
        super.update(sql, MixUtil.newHashMap("commentId",commentId));
    }

    public void addPraiseCount(long commentId,long addVal){
        String sql = "update game_comment set praise_count = praise_count + :addVal where id = :commentId ";
        super.update(sql, MixUtil.newHashMap("commentId",commentId,"addVal",addVal));
    }


    public Page<GameCommentDto> getCommentPage(GameComment searchObject,String gameName,Long orderType){

        boolean forBackend = StringUtil.equals(orderType,2L);//后台查询

        Map paramMap = new HashMap();
        StringBuffer sql = new StringBuffer();
        sql.append(" select  ");
        sql.append(super.getSelectColStr(GameComment.class,"a","comment")+",");

        if(forBackend){
            sql.append(super.getSelectColStr(GameInfo.class,"gi","gameInfo")+",");
            sql.append(super.getSelectColStr(GameVersion.class,"vi","versionInfo")+",");
        }

        sql.append(" b.star ");
        sql.append(" from game_comment  a ");
        sql.append(" LEFT JOIN game_comment_star b ");
        sql.append(" ON(a.game_id = b.game_id and a.user_id = b.user_id) ");
        if(forBackend){
            sql.append(" left join game_info gi ON(a.game_id = gi.id)  ");
            sql.append(" left join game_version vi ON(gi.lastest_version_id = vi.id)  ");
        }
        sql.append(" where a.delete_flag = "+ DeleteFlag.NO_DELETE +" ");
        sql.append(" and a.parent_id is null ");
        Long gameId = searchObject.getGameId();
        if(gameId != null) {
            sql.append(" and a.game_id = :gameId ");
            paramMap.put("gameId",gameId);
        }

        if(StringUtil.isNotEmpty(gameName)){
            sql.append(" and gi.name like :gameName ");
            paramMap.put("gameName", "%"+gameName+"%");
        }

        Long userId = searchObject.getUserId();
        if(userId != null) {
            sql.append(" and a.user_id = :userId ");
            paramMap.put("userId",userId);
        }
        if(orderType == null || orderType.longValue() == 0) { //根据时间
            sql.append(" order by a.top_status desc,a.id desc ");
        }else if(orderType.longValue() == 1) {//根据热度
            sql.append(" order by a.top_status desc,a.praise_count desc ");
        }else if(forBackend){//后台查询
            sql.append(" order by a.id desc ");
        }
        return super.paged2Obj(sql.toString(),paramMap,GameCommentDto.class);
    }

    /**
     *  V1.1版本前端不支持官方回复显示
     * @param parentId
     * @param orderType
     * @param showOfficaReply
     * @return
     */
    public Page<GameComment> getReplyPage(Long parentId,Long orderType,boolean showOfficaReply){
        Map paramMap = new HashMap();
        StringBuffer sql = new StringBuffer();
        sql.append(" select * from game_comment ");
        sql.append(" where delete_flag = 0 ");

        sql.append(" and parent_id = :parentId ");
        if(showOfficaReply == false){
            sql.append(" and (comment_type is null  or comment_type = "+GameComment.COMMENT_TYPE_COMMON+") ");
        }
        paramMap.put("parentId",parentId);
        if(orderType == null || orderType.longValue() == 0) {
            sql.append(" order by id desc ");
        }else if(orderType.longValue() == 1) {
            sql.append(" order by praise_count desc ");
        }
        return super.paged2Obj(sql.toString(),paramMap);
    }

    /**
     * 获取最早评论的三条记录
     * @return
     */
    public List<GameComment> getTop3CommentRecord(long gameId,long userId){
        String sql = " select * from game_comment " +
                " where delete_flag = 0 and game_id = :gameId and user_id = :userId " +
                " order by id limit 3";
        return super.query2Model(sql,MixUtil.newHashMap("gameId",gameId,"userId",userId));
    }



    public List<GameCommentDto> getHotGameComment(int size){
        StringBuffer sql = new StringBuffer();
        sql.append("select  ");
        sql.append(super.getSelectColStr("gc","comment")+",");
        sql.append(super.getSelectColStr(GameInfo.class,"a","gameInfo")+",");
        sql.append(super.getSelectColStr(GameVersion.class,"b","versionInfo")+",");
        sql.append(" gcs.star ");
        //sql.append(super.getSelectColStr(GameCategory.class,"c","category")+",");
        //sql.append(super.getSelectColStr(GamePublisher.class,"d","publisher"));

        sql.append(" FROM  game_comment gc ");
        sql.append(" LEFT JOIN game_comment_star gcs ");
        sql.append(" ON(gc.game_id = gcs.game_id and gc.user_id = gcs.user_id) ");
        sql.append(" LEFT JOIN game_info a ");
        sql.append(" on(a.id = gc.game_id ) ");
        sql.append(" LEFT JOIN game_version b ");
        sql.append(" ON(a.lastest_version_id = b.id ) ");
        //sql.append(" LEFT JOIN game_category c ");
        //sql.append(" ON(c.id = a.category_id ) ");
        //sql.append(" LEFT JOIN game_publisher d ");
        //sql.append(" ON(d.id = a.publisher_id ) ");
        sql.append(" where a.delete_flag = " + DeleteFlag.NO_DELETE + " and a.status = " + Status.ENABLED.getValue() + " ");
        sql.append(" and gc.delete_flag = " + DeleteFlag.NO_DELETE + " ");
        sql.append(" and gc.top_status = "+GameComment.TOP_STATUS_STICK+" ");
        sql.append(" and gc.parent_id is null ");
        sql.append(" order by gc.create_time desc ");
        sql.append(" limit :size ");
        return super.query2Model(sql.toString(),MixUtil.newHashMap("size",size),GameCommentDto.class);
    }

}
