package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.CrawlGameService;
import cc.ccplay.booster.base.http.PostType;
import cc.ccplay.booster.base.http.RequestProxy;
import cc.ccplay.booster.base.model.content.CrawlGame;
import cc.ccplay.booster.base.model.content.GameInfo;
import cc.ccplay.booster.base.util.ApiSignUtil;
import cc.ccplay.booster.content.dao.CrawlGameDao;
import cc.ccplay.booster.content.dao.GameInfoDao;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import org.easyj.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@com.alibaba.dubbo.config.annotation.Service(interfaceClass = CrawlGameService.class)
public class CrawlGameServiceImpl extends BaseContentService implements CrawlGameService{

    @Autowired
    private GameInfoDao gameInfoDao;

    @Autowired
    private CrawlGameDao crawlGameDao;

    private static final Logger log = Logger.getLogger(CrawlGameServiceImpl.class);

    private static final String apiKey = "685192597441057f852e696de8b72fc3";
    private static final String apiSecret = "4dbfe6fd08e10b8511d9de72ab8dfb57";

    public static void main(String []args){
        JSONArray array = getCrawlGames("com.playgendary.tanks");
        System.out.println(array.toJSONString());
    }

    @Override
    public void batchUpdateCralGame() {
        long startId = 0;
        int size = 50;
        while(true){
            List<GameInfo> games = gameInfoDao.getNeedCompareUpdateGames(startId,size);
            int totalcount = games.size();
            if(totalcount != 0){
                updateCrawlGames(games);
            }
            if(totalcount < size){
                break;
            }
            startId = games.get(totalcount - 1).getId();
        }
    }

    private void updateCrawlGames(List<GameInfo> games){
        int totalcount = games.size();
        Set<String> pnSet = new HashSet<>();
        StringBuffer pnBuffer = new StringBuffer();

        Map<String,Set<Long>> dataMap = new HashMap<>();
        boolean first = true;
        for (int i = 0; i < totalcount; i++) {
            GameInfo gameInfo = games.get(i);
            String packageName = gameInfo.getPackageName();
            if(!pnSet.contains(packageName)){//不存在
                pnSet.add(packageName);
                if(first){
                    first = false;
                }else{
                    pnBuffer.append(",");
                }
                pnBuffer.append(packageName);
                Set<Long> idSet = new HashSet<>();
                idSet.add(gameInfo.getId());
                dataMap.put(packageName,idSet);
            }else{
                Set<Long> idSet = dataMap.get(packageName);
                idSet.add(gameInfo.getId());
            }
        }

        JSONArray array = getCrawlGames(pnBuffer.toString());
        if(array == null){
            return;
        }
        int size = array.size();
        for (int i = 0; i < size; i++) {
            JSONObject item = array.getJSONObject(i);
            CrawlGame game = new CrawlGame();
            String packageName = item.getString("packageName");
            game.setPackageName(packageName);
            game.setVersionCode(item.getLong("versionCode"));
            game.setVersionName(item.getString("versionName"));
            Set<Long> gameIds = dataMap.get(packageName);
            if(gameIds == null){
                continue;
            }
            for (Long gameId : gameIds){
                game.setGameId(gameId);
                saveOrUpdateCrawlInfo(game);
            }
        }
    }

    @Override
    public void updateCrawlInfo(CrawlGame game) {
        Long gameId = game.getGameId();
        if (gameId == null) {
            List<GameInfo> gameInfos = gameInfoDao.getGameInfos(game.getPackageName());
            if (gameInfos != null) {
                for (GameInfo gameInfo : gameInfos) {
                    game.setGameId(gameInfo.getId());
                    saveOrUpdateCrawlInfo(game);
                }
            }
        } else {
            saveOrUpdateCrawlInfo(game);
        }
    }

    private void saveOrUpdateCrawlInfo(CrawlGame game){
        Long gameId = game.getGameId();
        CrawlGame old = crawlGameDao.get(gameId);
        if(old == null){
            crawlGameDao.save(game);
        }else if( !StringUtil.equals(old.getVersionCode(),game.getVersionCode())
                || !StringUtils.equals(game.getVersionName(),old.getVersionName()) ){
            crawlGameDao.update(game);
        }
    }

    private static JSONArray getCrawlGames(String packageNames){
//        try{
//            final JSONObject dataJson = new JSONObject();
//            dataJson.put("api_key",apiKey);
//            dataJson.put("pkNames",packageNames);
//
//            final String token = ApiSignUtil.buildSign(dataJson, "user.game.queryVersion", apiSecret);
//            dataJson.put("api_sign", token);
//            String content = new RequestProxy(){
//                public List<NameValuePair> getParameters() {
//                    List<NameValuePair> list = new ArrayList<>();
//                    JSONObject json = new JSONObject();
//                    json.put("user.game.queryVersion",dataJson);
//                    String data = json.toJSONString();
//                    list.add(new BasicNameValuePair("data",data));
//                    return list;
//                }
//            }.doRequest("http://crawl.ccplay.cc/apicenter/user.game.queryVersion/"+token, PostType.POST);
//
//            JSONObject jsonObject = JSONObject.parseObject(content);
//            JSONObject data = jsonObject.getJSONObject("user.game.queryVersion");
//
//            if("0000".equals(data.getString("code"))){
//                JSONArray result = data.getJSONArray("results");
//                return result;
//            }else{
//                log.error("从抓包平台读取最新应用包数据出现异常，响应数据："+data.toJSONString()+",请求包名:"+packageNames);
//            }
//            return null;
//        }catch (Exception e){
//            log.error("从抓包平台读取最新应用包数据出现异常,请求包名:"+packageNames,e);
//            e.printStackTrace();
//        }
        return null;
    }

}
