package cc.ccplay.booster.content.service;

import cc.ccplay.booster.content.dao.AdAreaDao;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.api.content.AdAreaService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.AdArea;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;

@Service(interfaceClass = AdAreaService.class)
@org.springframework.stereotype.Service
public class AdAreaServiceImpl extends BaseContentService implements AdAreaService{

    @Autowired
    private AdAreaDao adAreaDao;

    @Override
    public void saveOrUpdate(AdArea adArea) {
        adAreaDao.saveOrUpdate(adArea);
    }

    @Override
    public void updateStatus(long id, Status status) {
        AdArea updateModel = new AdArea();
        updateModel.setId(id);
        updateModel.setStatus(status.getValue());
        adAreaDao.updateNotNull(updateModel);
    }

    @Override
    public Page getPage(SystemParam param) {
        return adAreaDao.getPage();
    }

    @Override
    public AdArea get(long id) {
        return adAreaDao.get(id);
    }
}
