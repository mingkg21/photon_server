package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.model.content.GiftNumber;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

@Repository
public class GiftNumberDao extends BaseDao<GiftNumber> {
    public Page getPage(Long giftId){
        StringBuffer sql = new StringBuffer(" select * from gift_number where delete_flag = 0 ");
        if(giftId != null){
            sql.append(" and gift_id = :giftId ");
        }
        sql.append(" order by id desc ");
        return super.page2CamelMap(sql.toString(), MixUtil.newHashMap("giftId",giftId));
    }

    public GiftNumber getNumber(long giftId){
        String sql = " select * from gift_number " +
                " where delete_flag = 0 and gift_id = :giftId " +
                " and status = "+GiftNumber.STATUS_UNUSED+" limit 1";
        return super.query21Model(sql,MixUtil.newHashMap("giftId",giftId));
    }

    public int updateUseStatus(long id){
        String sql = " update gift_number set status = " + GiftNumber.STATUS_USED + "  where id = :id ";
        return super.update(sql,MixUtil.newHashMap("id",id));
    }

    public GiftNumber taoNumber(long giftId,long random){
        String sql = " select * from gift_number " +
                " where gift_id = :giftId " +
                " and status = "+GiftNumber.STATUS_USED+" limit 1 offset "+random;
        return super.query21Model(sql,MixUtil.newHashMap("giftId",giftId));
    }
}
