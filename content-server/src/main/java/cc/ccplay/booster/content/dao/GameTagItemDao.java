package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.model.content.GameTagItem;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

@Repository
public class GameTagItemDao extends BaseDao<GameTagItem> {

    public int deleteByGameId(long gameId){
        String sql = " delete from game_tag_item where game_id = :gameId ";
        return super.update(sql,MixUtil.newHashMap("gameId",gameId));
    }

    public int bathDelete(long gameId,Long ...tagIds){
        if(tagIds.length == 0){
            return 0;
        }
        StringBuffer sql = new StringBuffer(" delete from game_tag_item where game_id = :gameId and tag_id  in( ");

        for (int i = 0; i < tagIds.length ; i++) {
            if(i != 0){
                sql.append(",");
            }
            sql.append(tagIds[i]);
        }
        sql.append(" ) ");

        return super.update(sql.toString(),MixUtil.newHashMap("gameId",gameId));
    }
}
