package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.model.content.GameRecruitAnswer;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public class GameRecruitAnswerDao extends BaseDao<GameRecruitAnswer> {

    public int deleteByQuestionId(long questionId,Set<Long> excludeIds){
        StringBuffer sql = new StringBuffer(" delete from game_recruit_answer where ");
        sql.append(" question_id = :questionId ");
        if(excludeIds != null && excludeIds.size() > 0){
            sql.append(" and id not in ( ");
            boolean first = true;
            for(Long id : excludeIds) {
                if(first){
                   first = false;
                }else{
                    sql.append(",");
                }
                sql.append(id);
            }
            sql.append(")");
        }
        return super.update(sql.toString(), MixUtil.newHashMap("questionId",questionId));
    }

    public List<GameRecruitAnswer> getListByQuestionId(long questionId){
        String sql = "select * from game_recruit_answer where question_id = :questionId ";
        return super.query2Model(sql,MixUtil.newHashMap("questionId",questionId));
    }

}
