package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.dto.content.AppTopicDto;
import cc.ccplay.booster.base.model.content.AppTopic;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.springframework.stereotype.Repository;

@Repository
public class AppTopicDao extends BaseDao<AppTopic> {

    public Page getPage(){
        String sql = "select * from app_topic order by ordering,id desc ";
        return super.page2CamelMap(sql);
    }

    public Page<AppTopicDto> getDtoPage(){
        String sql = "select id,title,pic_url,create_time from app_topic order by ordering,id desc ";
        return super.paged2Obj(sql,AppTopicDto.class);
    }


}
