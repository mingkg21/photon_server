package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.SuggestionFeedbackService;
import cc.ccplay.booster.base.api.user.UserSystemMessageService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.enums.MsgContentType;
import cc.ccplay.booster.base.enums.MsgTagType;
import cc.ccplay.booster.base.model.content.SuggestionFeedback;
import cc.ccplay.booster.base.model.user.UserSystemMessage;
import cc.ccplay.booster.content.dao.SuggestionFeedbackDao;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.log4j.Logger;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service(interfaceClass = SuggestionFeedbackService.class)
@org.springframework.stereotype.Service
public class SuggestionFeedbackServiceImpl extends BaseContentService implements SuggestionFeedbackService {


    @Reference
    private UserSystemMessageService userSystemMessageService;

    private final static Logger log = Logger.getLogger(SuggestionFeedbackServiceImpl.class);


    @Autowired
    private SuggestionFeedbackDao suggestionFeedbackDao;

    @Override
    public void saveOrUpdate(SuggestionFeedback feedback) {
        suggestionFeedbackDao.saveOrUpdate(feedback);
    }

    @Override
    public Page getPage(SystemParam param,Long userId) {
        return suggestionFeedbackDao.getPage(userId);
    }

    @Override
    public void reply(SuggestionFeedback feedback) {
        feedback.setIsReply(1L);
        suggestionFeedbackDao.save(feedback);
        Long userId = feedback.getUserId();
        String content = feedback.getContent();

        UserSystemMessage msg = new UserSystemMessage();
        MsgContentType contentType = MsgContentType.SUGGESTION_FEEDBACK_REPLY;
        msg.setMsgType(UserSystemMessage.MSG_TYPE_GENERAL);
        msg.setUserId(userId);
        //msg.setObjectId(comment.getId());
        //msg.setObjectTitle("回复ID");
        msg.setMsgContent(content);
        msg.setContentType(new Long(contentType.getValue()));

        String title = contentType.getTitle();
        msg.setMsgTitle(title);
        JSONObject json = new JSONObject();
        json.put("msgTitle",title);
        //json.put("objectTitle","回复ID");
        //json.put("objectId",msg.getObjectId());
        json.put("contentType",contentType.getValue());
        json.put("userId",msg.getUserId());
        json.put("msgType",msg.getMsgType());
        json.put("msgContent",msg.getMsgContent());
        JSONArray contentArr = new JSONArray();

        contentArr.add(MixUtil.newHashMap("type", MsgTagType.TEXT,"value",content));
        json.put("content",contentArr);

        JSONArray titleArr = new JSONArray();
        titleArr.add(MixUtil.newHashMap("type", MsgTagType.TEXT,"value",title));
        json.put("title",titleArr);
        msg.setPushMsg(json.toJSONString());
        userSystemMessageService.pushMsg2One(msg);
    }

    @Override
    public List<SuggestionFeedback> getHistory(long userId) {
        return suggestionFeedbackDao.getHistory(userId);
    }
}
