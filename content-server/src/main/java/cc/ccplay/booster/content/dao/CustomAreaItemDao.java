package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.model.content.CustomAreaItem;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.easyj.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class CustomAreaItemDao extends BaseDao<CustomAreaItem> {

    public Page getAreaGamePage(long areaId,Long gameId,String packageName,String gameName){
        Map paramMap = MixUtil.newHashMap("areaId",areaId);
        StringBuffer sql = new StringBuffer(
                " SELECT " +
                        " c.id ," +
                        " a.id AS game_id,"+
                        " a.name,"+
                        " a.package_name,"+
                        "     b.version_code,"+
                        "     b.version_name,"+
                        "     b.icon,"+
                        "    c.ordering,"+
                        "    c.create_time "+
                        " FROM custom_area_item c "+
                        " LEFT JOIN game_info a "+
                        " ON (c.game_id = a.id) "+
                        " LEFT JOIN game_version b "+
                        " ON (a.lastest_version_id = b.id) "+
                        " WHERE c.area_id = :areaId ");

        if(gameId != null){
            sql.append(" and a.id = :gameId ");
            paramMap.put("gameId",gameId);
        }

        if(StringUtil.isNotEmpty(gameName)){
            sql.append(" and a.name like :gameName ");
            paramMap.put("gameName","%"+gameName+"%");
        }

        if(StringUtil.isNotEmpty(packageName)){
            sql.append(" and a.package_name = :packageName ");
            paramMap.put("packageName",packageName);
        }


        sql.append(" ORDER BY c.ordering,c.id ");
        return super.page2CamelMap(sql.toString(),paramMap);
    }

    public void rundomOrdering(long areaId){
        StringBuffer sql =  new StringBuffer();
        sql.append(" update custom_area_item set ordering = b.rownum ");
        sql.append(" from ");
        sql.append(" ( ");
        sql.append(" select row_number() OVER (ORDER BY random()) as rownum,id from custom_area_item where area_id = :areaId ");
        sql.append(" ) as b ");
        sql.append(" where custom_area_item.id = b.id ");
        super.update(sql.toString(),MixUtil.newHashMap("areaId",areaId));
    }


    public void deleteByAreaId(long areaId){
        String sql = "delete from custom_area_item where area_id = :areaId ";
        super.update(sql,MixUtil.newHashMap("areaId",areaId));
    }
}
