package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.CustomArea;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CustomAreaDao extends BaseDao<CustomArea> {

    public Page queryPage(){
        String sql = "select * from custom_area order by id desc";
        return super.page2CamelMap(sql);
    }


    public CustomArea getByCode(String code) {
        String sql = "select * from custom_area where code = :code";
        return super.query21Model(sql,MixUtil.newHashMap("code",code));
    }

    public List<CustomArea> getByCodes(String... codes) {
        StringBuffer sql = new StringBuffer("select * from custom_area where code in ( ");
        for (int i = 0; i < codes.length; i++) {
            if(i != 0){
                sql.append(",");
            }
            sql.append("'"+codes[i].trim()+"'");
        }
        sql.append(" ) ");
        return super.query2Model(sql.toString());
    }

    public int addAppCount(long areaId,long value){
        String sql = "update custom_area set app_count = app_count + :value where id = :areaId";
        return super.update(sql,MixUtil.newHashMap("areaId",areaId,"value",value));
    }

    public List<CustomArea> getAllAreaEnabled() {
        String sql = "select * from custom_area where status = "+ Status.ENABLED.getValue();
        return super.query2Model(sql);
    }

}
