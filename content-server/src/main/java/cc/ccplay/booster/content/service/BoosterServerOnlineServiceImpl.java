package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.BoosterServerOnlineService;
import cc.ccplay.booster.base.dto.content.booster.ServerPortStatusDto;
import cc.ccplay.booster.base.dto.content.booster.ServerStatusDto;
import cc.ccplay.booster.base.model.content.BoosterServerOnline;
import cc.ccplay.booster.base.model.log.BoosterServerStatus;
import cc.ccplay.booster.content.dao.BoosterServerOnlineDao;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service(interfaceClass = BoosterServerOnlineService.class)
@org.springframework.stereotype.Service
public class BoosterServerOnlineServiceImpl extends BaseContentService implements BoosterServerOnlineService {

    @Autowired
    private BoosterServerOnlineDao boosterServerOnlineDao;

    @Resource(name = "boosterRedisTemplate")
    private RedisTemplate<String,String> redisTemplate;

    @Override
    public void updateOlineCount(long serverId, long onlineCount) {
        BoosterServerOnline online = new BoosterServerOnline();
        online.setPersonCount(onlineCount);
        online.setServerId(serverId);
        online.setWorking(1L);
        if(boosterServerOnlineDao.get(serverId) != null){
            boosterServerOnlineDao.updateNotNull(online);
        }else{
            boosterServerOnlineDao.save(online);
        }
    }

    @Override
    public void updateSeverStatus(ServerStatusDto serverStatusDto) {
        Long serverId = serverStatusDto.getServerId();
        BoosterServerOnline online = new BoosterServerOnline();
        online.setServerId(serverId);
        setLoadStatus(serverStatusDto.getUptime(),online);
        if(serverStatusDto.getPid() == null){
            online.setWorking(0L);
            online.setContectedCount(0L);
            online.setPersonCount(0L);
        }else{
            List<ServerPortStatusDto> ports = serverStatusDto.getPorts();
            int count = ports.size();
            online.setWorking(1L);
            online.setPersonCount(new Long(count));
            online.setContectedCount(getContectedCount(ports));
        }
        if(boosterServerOnlineDao.get(serverId) != null){
            boosterServerOnlineDao.updateNotNull(online);
        }else{
            boosterServerOnlineDao.save(online);
        }

    }

    private void setLoadStatus(String uptime,BoosterServerOnline online){
        String keyword = "load average:";
        int index = uptime.indexOf(keyword);
        if(index == -1){
            return;
        }
        String load = uptime.substring(index + keyword.length());
        try {
            String [] loads = load.split(",");
            online.setLoad15(Double.parseDouble(loads[2].trim()));
            online.setLoad5(Double.parseDouble(loads[1].trim()));
            online.setLoad1(Double.parseDouble(loads[0].trim()));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private long getContectedCount(List<ServerPortStatusDto> ports){
        int size = ports.size();
        long count = 0;
        for (int i = 0; i < size; i++) {
            ServerPortStatusDto statusDto = ports.get(i);
            Long established = statusDto.getEstablished();
            if(established != null && established.longValue() == 1L){
                ++count;
            }
        }
        return count;
    }


    @Override
    public List<BoosterServerStatus> getAllServerStatus() {
        return boosterServerOnlineDao.getAllServerStatus();
    }
}
