package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.constants.Constant;
import cc.ccplay.booster.base.dto.content.booster.SyncPackageMappingDto;
import cc.ccplay.booster.base.model.content.GameInfo;
import cc.ccplay.booster.base.model.content.GamePackageMapping;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.easyj.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class GamePackageMappingDao extends BaseDao<GamePackageMapping> {

    public Page getPage(GamePackageMapping model){
        Map paramMap = new HashMap();
        StringBuffer sql = new StringBuffer(" select a.*,b.name,c.icon from game_package_mapping a");
        sql.append(" left join game_info b ");
        sql.append(" on(a.game_id = b.id )");
        sql.append(" left join game_version c ");
        sql.append(" on(c.id = b.lastest_version_id) ");
        sql.append(" where 1 = 1 ");
        String packageName = model.getPackageName();
        if(StringUtil.isNotEmpty(packageName)){
            sql.append(" and a.package_name = :packageName ");
            paramMap.put("packageName",packageName);
        }

        Long gameId = model.getGameId();
        if(gameId != null){
            sql.append(" and a.game_id = :gameId ");
            paramMap.put("gameId",gameId);
        }

        Long type = model.getType();
        if(type != null){
            sql.append(" and a.type = :type ");
            paramMap.put("type",type);
        }

        return super.page2CamelMap(sql.toString(),paramMap);
    }

    public List<GameInfo> getGameIdByPackageName(Set<String> packageName){
        if(packageName.size() == 0){
            return Collections.emptyList();
        }
        StringBuffer sql = new StringBuffer();
        sql.append("select a.game_id as id,package_name from game_package_mapping a  ");
        sql.append(" where type = "+GamePackageMapping.TYPE_ACCURATE+" and ( ");
        boolean first = true;
        for (String pn : packageName) {
            if(first){
                first = false;
            }else{
                sql.append(" OR ");
            }
            sql.append(" a.package_name = '"+pn+"'");
        }
        sql.append(")");
        return super.query2Model(sql.toString(),GameInfo.class);
    }

    public List<GameInfo> getGameIdByLike(Set<String> packageName){
        if(packageName.size() == 0){
            return Collections.emptyList();
        }
        StringBuffer sql = new StringBuffer(" select a.package_name,b.game_id as id from ( ");
        boolean first = true;
        for (String pn : packageName) {
            if(first){
                first = false;
            }else{
                sql.append(" union all ");
            }
            sql.append(" select CAST ('"+pn+"' AS VARCHAR) as package_name ");
        }
        sql.append(" ) as a ");
        sql.append(" left JOIN  game_package_mapping b ");
        sql.append(" on(position(b.package_name in a.package_name) = 1 and b.type = "+GamePackageMapping.TYPE_PREFIX+") ");
        sql.append(" where game_id is not null ");
        return super.query2Model(sql.toString(),GameInfo.class);
    }


    public List<GamePackageMapping> getMappingByGameId(long gameId){
        String sql = "select package_name,type from game_package_mapping where game_id = :gameId order by type ";
        return super.query2Model(sql, MixUtil.newHashMap("gameId",gameId));
    }


    public List getMappingList(long startId, int size){
        StringBuilder sb = new StringBuilder();
        sb.append(" select a.id,a.game_id as cc_game_id,a.package_name,a.create_time,a.type, ");
        sb.append(" b.name as cc_game_name,c.icon as cc_game_icon ");
        sb.append(" from game_package_mapping a  ");
        sb.append(" left join game_info b on(a.game_id = b.id ) ");
        sb.append(" left join game_version c on(c.id = b.lastest_version_id) ");
        sb.append(" where a.id > :startId ");
        sb.append(" and b.status is not null " );
        sb.append(" and  b.status = " + Constant.STATUS_ENABLED +" ");
        sb.append(" and  a.game_id in ( select DISTINCT game_id from booster_server_group_game ) ");
        sb.append(" order by a.id ");
        sb.append(" limit :size ");
        return super.query2Model(sb.toString(),MixUtil.newHashMap("startId",startId,"size",size), SyncPackageMappingDto.class);
    }


}
