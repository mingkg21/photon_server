package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.model.content.GameRecruitAnswerRecord;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class GameRecruitAnswerRecordDao extends BaseDao<GameRecruitAnswerRecord> {

    public List<GameRecruitAnswerRecord> getRecords(long userId,long recruitId){
         String sql = "select game_recruit_answer_record where user_id = :userId and recruit_id = :recruitId ";
         return super.query2Model(sql, MixUtil.newHashMap("userId",userId,"recruitId",recruitId));
    }

}
