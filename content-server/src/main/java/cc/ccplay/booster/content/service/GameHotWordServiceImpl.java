package cc.ccplay.booster.content.service;


import cc.ccplay.booster.base.model.content.GameHotWord;
import cc.ccplay.booster.content.dao.GameHotWordDao;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.api.content.GameHotWordService;
import cc.ccplay.booster.base.core.SystemParam;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;

@Service(interfaceClass = GameHotWordService.class)
@org.springframework.stereotype.Service
public class GameHotWordServiceImpl extends BaseContentService implements GameHotWordService {

    @Autowired
    private GameHotWordDao gameWordDao;

    @Override
    public void saveOrUpdate(SystemParam param, GameHotWord hotWord) {
        gameWordDao.saveOrUpdate(hotWord);
    }

    @Override
    public void setHotWord(SystemParam param,long wordId) {
        if(gameWordDao.updateDefaultWorld(wordId) == 0){
            throw new GenericException("记录不存在");
        }
        //将其他热词设置非默认
        gameWordDao.updateOtherDefaultWord(wordId);
    }

    @Override
    public void delete(SystemParam param,long wordId) {
        gameWordDao.delete(wordId);
    }

//    @Override
//    public HotWordsDto getHotWords(SystemParam param) {
//        HotWordsDto dto = new HotWordsDto();
//        dto.setWords(gameWordDao.getHotWords());
//        dto.setDefaultWord(gameWordDao.getDefaultWord());
//        return dto;
//    }

    @Override
    public Page queryPage(SystemParam param,String name) {
        return gameWordDao.queryPage(name);
    }

    @Override
    public GameHotWord get(SystemParam param, long id) {
        return gameWordDao.get(id);
    }

    @Override
    public GameHotWord getDefaultHotword() {
        return gameWordDao.getDefaultWord();
    }

    @Override
    public Page<GameHotWord> getList(SystemParam param) {
        return gameWordDao.getList();
    }
}
