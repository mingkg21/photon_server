package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.constants.Constant;
import cc.ccplay.booster.base.model.content.QqGroup;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class QqGroupDao extends BaseDao<QqGroup> {

    public Page getPage(){
        String sql = "select * from qq_group  order by ordering ";
        return super.page2CamelMap(sql);
    }

    public List getAll(){
        String sql = "select * from qq_group where status = "+ Constant.STATUS_ENABLED+"  order by ordering ";
        return super.selectCamelListMap(sql);
    }

}
