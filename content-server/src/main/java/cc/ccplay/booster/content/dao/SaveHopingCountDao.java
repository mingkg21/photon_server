package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.model.content.SaveHopingCount;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

@Repository
public class SaveHopingCountDao extends BaseDao<SaveHopingCount> {


    public void addPersonCount(long gameId,long value){
        String sql = " update save_hoping_count set person_count = person_count + :value where game_id = :gameId ";
        super.update(sql, MixUtil.newHashMap("gameId",gameId,"value",value));
    }

    public Page getPage(){
        StringBuffer sql = new StringBuffer();
        sql.append("select a.*,b.name,b.package_name,");
        sql.append(" CASE WHEN c.game_id is null then 0 ELSE 1 END  as has_save ");
        sql.append(" FROM save_hoping_count a ");
        sql.append(" LEFT JOIN game_info b ");
        sql.append(" ON(a.game_id = b.id) ");
        sql.append(" LEFT JOIN save_game c ");
        sql.append(" ON(c.game_id = a.game_id) ");
        sql.append(" order by last_hopping_time desc ");
        return super.page2CamelMap(sql.toString());
    }

}
