package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.OpenScreenService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.OpenScreenDto;
import cc.ccplay.booster.base.model.content.GameInfo;
import cc.ccplay.booster.base.model.content.OpenScreen;
import cc.ccplay.booster.content.dao.GameInfoDao;
import cc.ccplay.booster.content.dao.GameVersionDao;
import cc.ccplay.booster.content.dao.OpenScreenDao;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.enums.Status;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;

@Service(interfaceClass = OpenScreenService.class)
@org.springframework.stereotype.Service
public class OpenScreenServiceImpl extends BaseContentService implements OpenScreenService {

    @Autowired
    private OpenScreenDao openScreenDao;

    @Autowired
    private GameInfoDao gameInfoDao;

    @Autowired
    private GameVersionDao gameVersionDao;

    @Override
    public Page queryPage(SystemParam systemParam) {
        return openScreenDao.queryPage();
    }

    @Override
    public void delete(long id) {
        openScreenDao.delete(id);
    }

    @Override
    public void updateStatus(long id, Status status) {
        OpenScreen updateModel = new OpenScreen();
        updateModel.setId(id);
        updateModel.setStatus(status.getValue());
        openScreenDao.updateNotNull(updateModel);
    }

    @Override
    public void saveOrUpdate(OpenScreen openScreen) {
        Long id = openScreen.getId();
        if(id == null){
            openScreenDao.save(openScreen);
        }else{
            Long appId = openScreen.getAppId();
            if(appId != null){
                openScreenDao.updateNotNull(openScreen);
            }else{
                OpenScreen oldModel = openScreenDao.get(id);
                openScreen.setCreateTime(oldModel.getCreateTime());
                openScreen.setStatus(oldModel.getStatus());
                openScreenDao.update(openScreen);
            }
        }

    }


    @Override
    public OpenScreenDto getDto(long id) {
        OpenScreen os = openScreenDao.get(id);
        if(os == null){
            return null;
        }
        OpenScreenDto dto = new OpenScreenDto();
        dto.setOpenScreen(os);
        Long appId = os.getAppId();
        if(appId != null){
            GameInfo gameInfo = gameInfoDao.get(appId);
            dto.setGameInfo(gameInfo);
            if(gameInfo != null) {
                Long versionId = gameInfo.getLastestVersionId();
                dto.setVersionInfo(gameVersionDao.get(versionId));
            }
        }
        return dto;
    }

    /**
     * 获取最新
     *
     * @return
     */
    @Override
    public OpenScreen getLastest() {
        return openScreenDao.getLastest();
    }
}
