package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.model.content.AdArea;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

@Repository
public class AdAreaDao extends BaseDao<AdArea> {

    public Page getPage(){
        String sql = "select * from ad_area order by id desc ";
        return super.page2CamelMap(sql);
    }


    public AdArea getByCode(String code) {
        String sql = "select * from ad_area where code = :code ";
        return super.query21Model(sql, MixUtil.newHashMap("code",code));
    }
}
