package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.BoosterServerGroupService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.SelectizeDto;
import cc.ccplay.booster.base.model.content.BoosterServerGroup;
import cc.ccplay.booster.content.dao.BoosterServerGroupDao;
import com.alibaba.dubbo.config.annotation.Service;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;

import java.security.GeneralSecurityException;
import java.util.List;

@Service(interfaceClass = BoosterServerGroupService.class)
@org.springframework.stereotype.Service
public class BoosterServerGroupServiceImpl extends BaseContentService implements BoosterServerGroupService {

    @Autowired
    private BoosterServerGroupDao boosterServerGroupDao;

    @Override
    public Page getPage(SystemParam param,String name) {
        return boosterServerGroupDao.getPage(name);
    }

    @Override
    public void saveOrUpdate(BoosterServerGroup boosterServerGroup) {
        if(boosterServerGroupDao.isNotUnique(boosterServerGroup,"name")){
             throw new GenericException("组名["+boosterServerGroup.getName()+"]已经存在");
        }
        boosterServerGroupDao.saveOrUpdate(boosterServerGroup);
    }

    @Override
    public BoosterServerGroup get(long id) {
        return boosterServerGroupDao.get(id);
    }

    @Override
    public List<SelectizeDto> getForSelectize(SystemParam param, String key) {
        Page page = boosterServerGroupDao.getPage(key);
        return super.toSelectizeData(page,"id","name");
    }
}
