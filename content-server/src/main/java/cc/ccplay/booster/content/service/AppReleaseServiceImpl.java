package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.AppReleaseService;
import cc.ccplay.booster.base.model.content.AppRelease;
import cc.ccplay.booster.content.dao.AppReleaseDao;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.enums.Status;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;


@Service(interfaceClass=AppReleaseService.class)
@org.springframework.stereotype.Service
public class AppReleaseServiceImpl extends BaseContentService implements AppReleaseService {

    @Autowired
    private AppReleaseDao appReleaseDao;

    @Override
    public void saveOrUpdate(SystemParam param, AppRelease release) {
        appReleaseDao.saveOrUpdate(release);
    }

    @Override
    public void updateStatus(SystemParam param, long id, Status status) {
        AppRelease appRelease = new AppRelease();
        appRelease.setId(id);
        appRelease.setStatus(status.getValue());
        appReleaseDao.updateNotNull(appRelease);
    }

    @Override
    public Page getPage(SystemParam param) {
        return appReleaseDao.getPage();
    }

    @Override
    public AppRelease get(long id) {
        return appReleaseDao.get(id);
    }

    @Override
    public AppRelease getLastestVersion() {
        return appReleaseDao.getLastestVersion();
    }
}
