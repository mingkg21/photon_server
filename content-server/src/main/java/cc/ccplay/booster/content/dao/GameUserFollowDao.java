package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.dto.content.game.GameInfoDto;
import cc.ccplay.booster.base.model.content.GameVersion;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.GameInfo;
import cc.ccplay.booster.base.model.content.GameUserFollow;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

@Repository
public class GameUserFollowDao extends BaseDao<GameUserFollow> {

    public Page<GameInfoDto> getUserFllowGames(long userId){
        StringBuffer sql = new StringBuffer(" select ");
        sql.append(super.getSelectColStr(GameInfo.class,"b","gameInfo")+",");
        sql.append(super.getSelectColStr(GameVersion.class,"c","versionInfo")+" ");
        sql.append(" from game_user_follow a  ");
        sql.append(" left join game_info b on(a.game_id = b.id) ");
        sql.append(" left join game_version c on(b.lastest_version_id = c.id )");
        sql.append(" where a.user_id = :userId  ");
        sql.append(" and b.status = "+ Status.ENABLED.getValue() +" ");
        sql.append(" and b.delete_flag = 0 ");
        sql.append(" order by a.create_time desc ");
        return super.paged2Obj(sql.toString(), MixUtil.newHashMap("userId",userId),GameInfoDto.class);
    }
}
