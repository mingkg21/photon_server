package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.ComplaintService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.content.Complaint;
import cc.ccplay.booster.content.dao.ComplaintDao;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service(interfaceClass=ComplaintService.class)
@org.springframework.stereotype.Service
public class ComplaintServiceImpl extends BaseContentService implements ComplaintService {

    @Autowired
    private ComplaintDao complaintDao;

    @Override
    public void save(SystemParam param, Complaint complaint) {
        complaintDao.save(complaint);
    }
}
