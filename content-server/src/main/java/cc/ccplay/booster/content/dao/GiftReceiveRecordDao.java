package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.model.content.GiftReceiveRecord;
import cc.ccplay.booster.base.dto.content.gift.GiftReceiveRecordPageDto;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.easyj.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class GiftReceiveRecordDao extends BaseDao<GiftReceiveRecord>{
    public Page<GiftReceiveRecordPageDto> getPage(Long gameId, String giftName){
        Map paramMap = MixUtil.newHashMap();
        StringBuffer sql = new StringBuffer();
        sql.append(" select a.id,a.user_id,a.create_time,a.code,a.gift_id,b.name as gift_name,b.type,c.name as game_name ");
        sql.append(" FROM gift_receive_record a ");
        sql.append(" LEFT JOIN gift_info b ");
        sql.append(" ON(a.gift_id = b.id) ");
        sql.append(" LEFT JOIN game_info c ");
        sql.append(" ON(b.game_id = c.id) ");
        sql.append(" where 1 = 1 ");
        if(gameId != null){
            sql.append(" and b.game_id = :gameId ");
            paramMap.put("gameId",gameId);
        }

        if(StringUtil.isNotEmpty(giftName)){
            sql.append(" and b.name like :giftName ");
            paramMap.put("giftName","%"+giftName+"%");
        }
        sql.append(" order by a.id desc ");
        return super.paged2Obj(sql.toString(),paramMap,GiftReceiveRecordPageDto.class);
    }


    public GiftReceiveRecord getReceiveRecord(long userId,long giftId){
        String sql = "select * from gift_receive_record where user_id =:userId and gift_id = :giftId limit 1";
        return super.query21Model(sql,MixUtil.newHashMap("userId",userId,"giftId",giftId));
    }
}
