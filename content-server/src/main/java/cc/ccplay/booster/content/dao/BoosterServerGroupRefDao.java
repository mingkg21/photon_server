package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.model.content.BoosterServerGroupRef;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

@Repository
public class BoosterServerGroupRefDao extends BaseDao<BoosterServerGroupRef> {

    public Page getPage(long groupId){
        StringBuffer sql = new StringBuffer(" select a.id,a.server_id,a.group_id,b.name as server_name,b.status as server_status,b.ip,a.ordering  ");
        sql.append(" from booster_server_group_ref a ");
        sql.append(" left join booster_server b ");
        sql.append(" on(a.server_id = b.id ) ");
        sql.append(" where a.group_id = :groupId ");
        sql.append(" order by a.ordering ");
        return super.page2CamelMap(sql.toString(), MixUtil.newHashMap("groupId",groupId));
    }

}
