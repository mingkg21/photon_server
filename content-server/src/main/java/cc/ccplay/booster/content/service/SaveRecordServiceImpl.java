package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.model.content.SaveRecordPraise;
import cc.ccplay.booster.content.dao.GameInfoDao;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.api.content.SaveRecordService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.save.SaveRecordDto;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.SaveRecord;
import cc.ccplay.booster.content.dao.SaveRecordDao;
import cc.ccplay.booster.content.dao.SaveRecordPraiseDao;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service(interfaceClass = SaveRecordService.class)
@org.springframework.stereotype.Service
public class SaveRecordServiceImpl extends BaseContentService implements SaveRecordService {

    @Autowired
    private SaveRecordDao saveRecordDao;

    @Autowired
    private GameInfoDao gameInfoDao;

    @Autowired
    private SaveRecordPraiseDao saveRecordPraiseDao;

    @Override
    public Page getPage(SystemParam param,Long open,Long gameId,String name) {
        return saveRecordDao.getPage(open,gameId,name);
    }

    @Override
    public SaveRecord get(long id) {
        return saveRecordDao.get(id);
    }

    @Override
    public void saveOrUpdate(SaveRecord record) {
        saveRecordDao.saveOrUpdate(record);
    }

    @Override
    public void delete(long id) {
        saveRecordDao.delete(id);
    }

    @Override
    public void updateStatus(long id, Status status) {
        SaveRecord record = new SaveRecord();
        record.setId(id);
        record.setStatus(status.getValue());
        saveRecordDao.updateNotNull(record);
    }

    @Override
    public SaveRecordDto getDto(long id) {
        SaveRecord saveRecord = saveRecordDao.get(id);
        if(saveRecord == null){
            return null;
        }
        SaveRecordDto dto = new SaveRecordDto();
        dto.setSaveRecord(saveRecord);
        Long gameId = saveRecord.getGameId();
        dto.setGameInfo(gameInfoDao.get(gameId));
        return dto;
    }


    /**
     * 客户端保存存档
     *
     * @param record
     */
    @Override
    public SaveRecord uploadRecord(SaveRecord record) {
        Long id = record.getId();
        Long gameId = record.getGameId();
        Long userId = record.getUserId();
        if(id != null){
            SaveRecord oldRecord = saveRecordDao.get(id);
            if(oldRecord == null || !StringUtil.equals(userId,record.getUserId())){
                throw new GenericException("旧存档不存在");
            }
            if(gameId != null && !StringUtil.equals(gameId,oldRecord.getGameId())){
                throw new GenericException("只能覆盖相同游戏的存档");
            }
            saveRecordDao.updateNotNull(record);
            return record;
        }else{
            int count = saveRecordDao.getRecordCount(gameId,userId);
            if(count >= 5){
                throw new GenericException("一个游戏最多只能有5个存档");
            }
            return saveRecordDao.save(record);
        }
    }

    @Override
    public List<SaveRecord> getUserRecord(long userId, long gameId) {
        return saveRecordDao.getUserRecord(userId,gameId);
    }

    /**
     * 获取推荐存档
     *
     * @param gameId
     * @return
     */
    @Override
    public Page<SaveRecord> getRecommendRecord(SystemParam param,long gameId) {
        return saveRecordDao.getRecommendRecord(gameId);
    }

    /**
     * 点赞
     *
     * @param recordId
     */
    @Override
    public void praise(long userId,long recordId) {
        if(saveRecordDao.addPraiseCount(recordId,1) == 0){
            throw new GenericException("存档记录不存在");
        }

        SaveRecordPraise.Id id = new SaveRecordPraise.Id(userId,recordId);
        if(saveRecordPraiseDao.get(id) != null){
            return;
        }
        SaveRecordPraise praise = new SaveRecordPraise();
        praise.setId(id);
        saveRecordPraiseDao.save(praise);
    }

    /**
     * 逻辑删除
     *
     * @param recordId
     */
    @Override
    public void logicDelete(long userId,long recordId) {
        SaveRecord saveRecord = saveRecordDao.get(recordId);
        if(saveRecord == null){
            return;
        }
        //只能删除自己的存档
        if(!StringUtil.equals(saveRecord.getUserId(),userId)){
            throw new GenericException("非法操作");
        }
        saveRecordDao.logicDelete(recordId);
    }
}
