package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.content.BoosterGameFeedback;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.easyj.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class BoosterGameFeedbackDao extends BaseDao<BoosterGameFeedback> {

    public Page getPage(BoosterGameFeedback searchModel,boolean containPackageName){
        StringBuffer sql = new StringBuffer();
        if(containPackageName) {
            sql.append(" select package_name,count(1) as total_count ,max(name) as name ,max(version_name) as version_name,max(version_code) as version_code ");
        }else{
            sql.append(" select count(1) as total_count,name  ");
        }
        sql.append(" from booster_game_feedback ");
        sql.append(" where 1 = 1 ");
        Map paramMap = new HashMap();

        if(containPackageName){
            sql.append(" and package_name is not null ");
            String packageName = searchModel.getPackageName();
            if(StringUtil.isNotEmpty(packageName)){
                sql.append(" and package_name = :packageName ");
                paramMap.put("packageName",packageName);
            }
            sql.append("  group by package_name  ");
        }else{
            sql.append(" and package_name is null ");
            sql.append(" group by name  ");
        }
        sql.append(" order by total_count desc ");
        return super.page2CamelMap(sql.toString(),paramMap);
    }



    public void deleteByPackageName(String packageName){
        String sql = " delete from booster_game_feedback where package_name = :packageName ";
        super.update(sql, MixUtil.newHashMap("packageName",packageName));
    }

    public void deleteByName(String name){
        String sql = " delete from booster_game_feedback where name = :name ";
        super.update(sql, MixUtil.newHashMap("name",name));
    }
}
