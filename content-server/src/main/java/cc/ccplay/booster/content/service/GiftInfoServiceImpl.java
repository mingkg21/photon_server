package cc.ccplay.booster.content.service;


import cc.ccplay.booster.content.dao.GiftInfoDao;
import cc.ccplay.booster.content.dao.GiftNumberDao;
import cc.ccplay.booster.content.dao.GiftReceiveRecordDao;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.api.content.GiftInfoService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.gift.GiftInfoDto;
import cc.ccplay.booster.base.model.content.GiftInfo;
import cc.ccplay.booster.base.model.content.GiftNumber;
import cc.ccplay.booster.base.model.content.GiftReceiveRecord;
import org.apache.commons.lang3.StringUtils;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.StringUtil;
import org.easyj.frame.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

@Service(interfaceClass = GiftInfoService.class)
@org.springframework.stereotype.Service
public class GiftInfoServiceImpl extends BaseContentService implements GiftInfoService{

    @Autowired
    private GiftInfoDao giftInfoDao;

    @Autowired
    private GiftNumberDao giftNumberDao;

    @Autowired
    private GiftReceiveRecordDao giftReceiveRecordDao;

    @Override
    public Page getPage(SystemParam param, Long gameId, String giftName) {
        return giftInfoDao.getPage(gameId,giftName);
    }

    @Override
    public GiftInfo saveOrUpdate(SystemParam param, GiftInfo giftInfo) {
        return this.giftInfoDao.saveOrUpdate(giftInfo);
    }

    @Override
    public GiftInfoDto getGiftInfoDto(SystemParam param, long id) {
        return giftInfoDao.getGiftInfoDto(id);
    }

    @Override
    public Page<GiftInfoDto> getGameGiftPage(SystemParam param, long gameId,Long userId) {
        return giftInfoDao.getPageByGameId(gameId,userId);
    }


    @Override
    public GiftNumber receive(SystemParam param,long userId,long giftId) {
        GiftInfo giftInfo = giftInfoDao.get(giftId);
        checkGift(giftInfo);

        GiftReceiveRecord record = giftReceiveRecordDao.getReceiveRecord(userId,giftId);
        if(record != null){
            //throw new GenericException("同一个礼包只能领取一次");
            GiftNumber giftNumber = new GiftNumber();
            giftNumber.setCode(record.getCode());
            giftNumber.setGiftId(giftId);
            giftNumber.setCreateTime(record.getCreateTime());
            return giftNumber;
        }

        GiftNumber giftNumber = giftNumberDao.getNumber(giftId);
        if(giftNumber == null){
            throw new GenericException("礼包已经领取完毕");
        }

        if(giftNumberDao.updateUseStatus(giftNumber.getId()) == 0){
            throw new GenericException("领取失败");
        }
        record = new GiftReceiveRecord();
        record.setGiftId(giftId);
        record.setCode(giftNumber.getCode());
        record.setUserId(userId);
        record.setType(GiftReceiveRecord.TYPE_RECEIVE);
        giftReceiveRecordDao.save(record);
        giftInfoDao.addReceiveCount(giftId,1);
        return giftNumber;
    }


    @Override
    public GiftNumber tao(SystemParam param,long userId,long giftId) {

        GiftReceiveRecord record = giftReceiveRecordDao.getReceiveRecord(userId,giftId);
        if(record != null && StringUtil.equals(GiftReceiveRecord.TYPE_RECEIVE,record.getType())){
            GiftNumber giftNumber = new GiftNumber();
            giftNumber.setCode(record.getCode());
            giftNumber.setGiftId(giftId);
            giftNumber.setCreateTime(record.getCreateTime());
            return giftNumber;
        }

        GiftInfo giftInfo = giftInfoDao.get(giftId);
        checkGift(giftInfo);
        Long receiveCount = giftInfo.getReceiveCount();
        if(Validator.isNull(receiveCount)){
           throw new GenericException("没有可以淘取的号码");
        }
        Long count = giftInfo.getGiftCount();
        if(Validator.isNull(count)){
            throw new GenericException("没有可以淘取的号码");
        }

        if(count != receiveCount){
            throw new GenericException("未开启淘号");
        }
        GiftNumber giftNumber = giftNumberDao.taoNumber(giftId, (int)(Math.random()*count));
        if(giftNumber == null){
            throw new GenericException("未淘到号码");
        }

        if(record == null){
            record = new GiftReceiveRecord();
            record.setType(GiftReceiveRecord.TYPE_TAO);
            record.setUserId(userId);
            record.setGiftId(giftId);
            record.setCode(giftNumber.getCode());
            giftReceiveRecordDao.save(record);
        }else if(!StringUtils.equals(record.getCode(),giftNumber.getCode())){
            //更新最后一次淘号结果
            GiftReceiveRecord updateRecord = new GiftReceiveRecord();
            updateRecord.setId(record.getId());
            updateRecord.setCode(giftNumber.getCode());
            giftReceiveRecordDao.updateNotNull(updateRecord);
        }
        return giftNumber;
    }

    private void checkGift(GiftInfo giftInfo){
        if(giftInfo == null){
            throw new GenericException("礼包不存在");
        }

        if(!StringUtil.equals(giftInfo.getStatus(),GiftInfo.STATUS_OPEN)){
            throw new GenericException("礼包未开启");
        }

        long now = System.currentTimeMillis();
        Date startTime = giftInfo.getStartTime();
        if(startTime != null && now < startTime.getTime()){
            throw new GenericException("礼包不在有效期内");
        }

        Date endTime = giftInfo.getEndTime();
        if(endTime != null && now > endTime.getTime()){
            throw new GenericException("礼包已过期");
        }
    }
}
