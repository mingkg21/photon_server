package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.BaseService;
import cc.ccplay.booster.base.model.content.*;
import cc.ccplay.booster.content.dao.*;
import cc.ccplay.booster.content.dao.*;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.api.content.GameRecruitService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.game.GameRecruitDto;
import cc.ccplay.booster.base.model.content.*;
import cc.ccplay.booster.content.dao.*;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service(interfaceClass = GameRecruitService.class)
@org.springframework.stereotype.Service
public class GameRecruitServiceImpl extends BaseService implements GameRecruitService {

    @Autowired
    private GameRecruitDao gameRecruitDao;

    @Autowired
    private GameInfoDao gameInfoDao;

    @Autowired
    private GameVersionDao gameVersionDao;

    @Autowired
    private GamePublisherDao gamePublisherDao;

    @Autowired
    private GameRecruitPartakeDao gameRecruitPartakeDao;

    @Autowired
    private GameRecruitAnswerRecordDao gameRecruitAnswerRecordDao;

    @Autowired
    private GameRecruitFeedbackDao gameRecruitFeedbackDao;

    @Override
    public Page getPage(SystemParam param) {
        return gameRecruitDao.getPage();
    }

    @Override
    public void updateStatus(SystemParam param, long id, long status) {
        if(status != GameRecruit.STATUS_OPEN && status != GameRecruit.STATUS_CLOSE){
            throw new GenericException("状态值有误");
        }
        GameRecruit gameRecruit = new GameRecruit();
        gameRecruit.setId(id);
        gameRecruit.setStatus(status);
        gameRecruitDao.updateNotNull(gameRecruit);
    }

    @Override
    public GameRecruit saveOrUpdate(SystemParam param, GameRecruit gameRecruit) {
        Long gameId = gameRecruit.getGameId();
        if(gameInfoDao.get(gameId) == null){
            throw new GenericException("游戏不存在");
        }
        return gameRecruitDao.saveOrUpdate(gameRecruit);
    }

    @Override
    public GameRecruitDto getGameRecruitDto(SystemParam param, long id) {
        GameRecruit gameRecruit = gameRecruitDao.get(id);
        if(gameRecruit == null){
            return null;
        }
        GameRecruitDto dto = new GameRecruitDto();
        dto.setGameRecruit(gameRecruit);
        Long gameId = gameRecruit.getGameId();
        GameInfo gameInfo = gameInfoDao.get(gameId);
        if(gameInfo == null){
            throw new GenericException("游戏已被删除");
        }
        dto.setGameInfo(gameInfo);
        Long lastestVersionId = gameInfo.getLastestVersionId();
        GameVersion gameVersion = gameVersionDao.get(lastestVersionId);
        dto.setVersionInfo(gameVersion);
        return dto;
    }

    @Override
    public Page<GameRecruitDto> getDtoPage(SystemParam param) {
        return gameRecruitDao.getDtoPage();
    }

    @Override
    public GameRecruitDto getGameRecruitDtoForApp(SystemParam param, long id) {
        GameRecruitDto gameRecruitDto = this.getGameRecruitDto(param, id);
        GameInfo gameInfo = gameRecruitDto.getGameInfo();
        if(gameInfo != null){
            Long publisherId = gameInfo.getPublisherId();
            gameRecruitDto.setPublisher(this.gamePublisherDao.get(publisherId));
        }
        if(gameRecruitDto == null){
            return null;
        }
        Long userId = param.getUserId();
        if(userId == null){//未登录
            gameRecruitDto.setTakeTask(false);
        }else{
            GameRecruitPartake.Id partakeId = new GameRecruitPartake.Id(id,userId);
            if(this.gameRecruitPartakeDao.get(partakeId) != null){
                gameRecruitDto.setTakeTask(true);
            }else{
                gameRecruitDto.setTakeTask(false);
                List<GameRecruitAnswerRecord> answerRecords = this.gameRecruitAnswerRecordDao.getRecords(userId,id);
                if(answerRecords.size() > 0){
                    gameRecruitDto.setAnswerRecords(answerRecords);
                }
            }
        }

        if(gameRecruitDto.getAnswerRecords() == null){ //读取题目及答题

        }
        return gameRecruitDto;
    }

    @Override
    public void takeTask(SystemParam param, long id) {
        checkAndGet(id);
        Long userId = param.getUserId();
        GameRecruitPartake.Id partakeId = new GameRecruitPartake.Id(id,userId);
        if(this.gameRecruitPartakeDao.get(partakeId) != null){//已参与过
            return;
        }
        GameRecruitPartake partake = new GameRecruitPartake();
        partake.setId(partakeId);
        gameRecruitPartakeDao.save(partake);
    }

    private GameRecruit checkAndGet(long id){
       GameRecruit gameRecruit = gameRecruitDao.get(id);
       if(gameRecruit == null){
           throw new GenericException("招募信息不存在");
       }
       if(!StringUtil.equals(gameRecruit.getStatus(),GameRecruit.STATUS_OPEN)){
           throw new GenericException("招募已经关闭");
       }
       long startTime = gameRecruit.getStartTime().getTime();
       long endTime = gameRecruit.getEndTime().getTime();
       long currentTime = System.currentTimeMillis();
       if(currentTime < startTime || currentTime > endTime){
           throw new GenericException("招募不在有效期内");
       }
       return gameRecruit;
    }

    @Override
    public void saveAnswers(SystemParam param, long recruitId, List<GameRecruitAnswerRecord> answers) {
        checkAndGet(recruitId);
        if(gameRecruitAnswerRecordDao.getRecords(param.getUserId(),recruitId).size() > 0){
            throw new GenericException("不能重复答题");
        }
        gameRecruitAnswerRecordDao.batchInsert(answers);
    }
}
