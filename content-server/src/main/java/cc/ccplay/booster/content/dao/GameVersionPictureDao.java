package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.model.content.GameVersionPicture;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class GameVersionPictureDao extends BaseDao<GameVersionPicture> {

    public int deleteByVersionId(long versionId){
        String sql = " delete from game_version_picture where version_id = :versionId ";
        return super.update(sql, MixUtil.newHashMap("versionId",versionId));
    }

    public List<GameVersionPicture> getListByVersionId(long versionId){
        String sql = " select * from game_version_picture where version_id = :versionId ";
        return super.query2Model(sql,MixUtil.newHashMap("versionId",versionId));
    }
}
