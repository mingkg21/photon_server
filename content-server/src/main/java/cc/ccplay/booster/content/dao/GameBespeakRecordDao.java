package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.model.content.GameBespeakRecord;
import org.easyj.frame.jdbc.BaseDao;
import org.springframework.stereotype.Repository;

@Repository
public class GameBespeakRecordDao extends BaseDao<GameBespeakRecord>{

}
