package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.TagTagItemService;
import cc.ccplay.booster.base.dto.content.game.TagTagItemDto;
import cc.ccplay.booster.base.model.content.TagTagItem;
import cc.ccplay.booster.content.dao.TagTagItemDao;
import cc.ccplay.booster.content.dao.GameTagDao;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.core.SystemParam;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service(interfaceClass = TagTagItemService.class)
@org.springframework.stereotype.Service
public class TagTagItemServiceImpl extends BaseContentService implements TagTagItemService {


    @Autowired
    private GameTagDao gameTagDao;

    @Autowired
    private TagTagItemDao tagTagItemDao;

    @Override
    public void saveOrUpdate(TagTagItem item) {
        if(tagTagItemDao.get(item.getId())!= null){
            tagTagItemDao.updateNotNull(item);
        }else{
            tagTagItemDao.save(item);
            gameTagDao.addChildTagCount(item.getId().getMainTagId(),1);
        }
    }

    @Override
    public void delete(long mainTagId, long tagId) {
        TagTagItem.Id id = new TagTagItem.Id(mainTagId,tagId);
        if(tagTagItemDao.delete(id) > 0){
            gameTagDao.addChildTagCount(mainTagId,-1);
        }
    }

    @Override
    public Page getTagPage(SystemParam param, long mainTagId, String name) {
        return tagTagItemDao.getTagPage(mainTagId,name);
    }


    @Override
    public TagTagItemDto getDto(long mainTagId, long tagId) {
        TagTagItem.Id id = new TagTagItem.Id(mainTagId,tagId);
        TagTagItem item = tagTagItemDao.get(id);
        if(item == null){
            return null;
        }
        TagTagItemDto dto = new TagTagItemDto();
        dto.setItem(item);
        dto.setGameTag(gameTagDao.get(tagId));
        return dto;
    }

    @Override
    public List getChildTagList(long mainTagId, int topSize) {
        return tagTagItemDao.getChildTagList(mainTagId,topSize);
    }
}
