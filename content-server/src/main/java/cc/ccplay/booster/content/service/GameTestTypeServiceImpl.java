package cc.ccplay.booster.content.service;

import cc.ccplay.booster.content.dao.GameTestTypeDao;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.api.content.GameTestTypeService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.GameTestType;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service(interfaceClass = GameTestTypeService.class)
@org.springframework.stereotype.Service
public class GameTestTypeServiceImpl implements GameTestTypeService {

    @Autowired
    private GameTestTypeDao gameTestTypeDao;

    @Override
    public GameTestType saveOrUpdate(SystemParam param, GameTestType testType) {
        Long id = testType.getId();
        if(id == null){
            testType.setStatus(Status.ENABLED);
        }
        if(gameTestTypeDao.isNotUnique(testType,"name")){
            throw new GenericException("类型["+testType.getName()+"]已经存在");
        }
        return gameTestTypeDao.saveOrUpdate(testType);
    }

    @Override
    public Page getPage(SystemParam param,String name) {
        return gameTestTypeDao.getPage(name);
    }

    @Override
    public int updateStatus(SystemParam param, long id, Status status) {
        GameTestType updateModel = new GameTestType();
        updateModel.setId(id);
        updateModel.setStatus(status);
        return gameTestTypeDao.updateNotNull(updateModel);
    }

    @Override
    public GameTestType get(SystemParam param, long id) {
        return gameTestTypeDao.get(id);
    }

    @Override
    public List<GameTestType> getAll4Selector(SystemParam param) {
        return gameTestTypeDao.getAllEnabled();
    }
}
