package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.dto.content.game.GameInfoDto;
import cc.ccplay.booster.base.enums.DeleteFlag;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.*;
import cc.ccplay.booster.base.model.content.*;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.easyj.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class SaveRecordDao extends BaseDao<SaveRecord> {

    public Page getPage(Long open,Long gameId,String name){
        Map<String,Object> paramMap = new HashMap<>();
        StringBuffer sql = new StringBuffer(" select a.*,b.name as game_name from save_record a ");
        sql.append(" LEFT JOIN game_info b ON (a.game_id = b.id )");
        sql.append(" where a.status = " + Status.ENABLED.getValue() +" ");
        sql.append(" and a.delete_flag =  " + DeleteFlag.NO_DELETE +" ");
        if(open != null){
            sql.append(" and a.open = :open ");
            paramMap.put("open",open);
        }

        if(gameId != null){
            sql.append(" and a.game_id = :gameId ");
            paramMap.put("gameId",gameId);
        }

        if(StringUtil.isNotEmpty(name)){
            sql.append(" and a.name like :name ");
            paramMap.put("name","%"+name+"%");
        }

        return super.page2CamelMap(sql.toString(),paramMap);
    }

    public int getRecordCount(long gameId,long userId){
        String sql = "select count(1) from save_record where delete_flag = "+DeleteFlag.NO_DELETE+" and  game_id = :gameId and user_id = :userId ";
        return super.queryForInt(sql, MixUtil.newHashMap("gameId",gameId,"userId",userId));
    }

    public List<SaveRecord> getUserRecord(long userId, long gameId){
        String sql = "select * from save_record where " +
                " delete_flag = "+DeleteFlag.NO_DELETE+" " +
                " and game_id = :gameId " +
                " and user_id = :userId and status = " +Status.ENABLED.getValue() +
                " order by update_time desc ";
        return super.query2Model(sql,MixUtil.newHashMap("gameId",gameId,"userId",userId));
    }

    public Page<SaveRecord> getRecommendRecord(long gameId){
        String sql = "select * from save_record where delete_flag = "+DeleteFlag.NO_DELETE+" and game_id = :gameId and user_id is null and status = " +Status.ENABLED.getValue() + " order by update_time desc ";
        return super.paged2Obj(sql,MixUtil.newHashMap("gameId",gameId));
    }

    public List<SaveRecord> getRecommendRecords(long gameId){
        String sql = "select * from save_record where delete_flag = "+DeleteFlag.NO_DELETE+" and game_id = :gameId and user_id is null and status = " +Status.ENABLED.getValue() + " order by update_time desc ";
        return super.query2Model(sql,MixUtil.newHashMap("gameId",gameId));
    }

    public int addPraiseCount(long recordId,long value){
        String sql = "update save_record set praise_count = praise_count + :value where id = :recordId ";
        return super.update(sql,MixUtil.newHashMap("recordId",recordId,"value",value));
    }


    public void addCommentCount(long recordId){
        String sql = " UPDATE save_record set comment_count = comment_count + 1 where id = :id ";
        super.update(sql,MixUtil.newHashMap("id",recordId));
    }


    public Page<GameInfoDto> getMySaveGame(long userId){
        Map<String,Object> paramMap = new HashMap<>();
        paramMap.put("userId",userId);
        StringBuffer sql = new StringBuffer();
        sql.append("select  ");
        sql.append(super.getSelectColStr(GameInfo.class,"a","gameInfo")+",");
        sql.append(super.getSelectColStr(GameVersion.class,"b","versionInfo")+",");
        sql.append(super.getSelectColStr(GameCategory.class,"c","category")+",");
        sql.append(super.getSelectColStr(GamePublisher.class,"d","publisher"));
        sql.append(" FROM ");
        sql.append(" ( select game_id from save_record where user_id = :userId group by game_id order by max(update_time) desc ) as e ");
        sql.append(" LEFT JOIN game_info a ");
        sql.append(" ON(e.game_id = a.id ) ");
        sql.append(" LEFT JOIN game_version b ");
        sql.append(" ON(a.lastest_version_id = b.id ) ");
        sql.append(" LEFT JOIN game_category c ");
        sql.append(" ON(c.id = a.category_id ) ");
        sql.append(" LEFT JOIN game_publisher d ");
        sql.append(" ON(d.id = a.publisher_id ) ");
        sql.append(" where a.delete_flag = 0 ");
        sql.append(" and a.status = " + Status.ENABLED.getValue() +" ");
        return super.paged2Obj(sql.toString(),paramMap,GameInfoDto.class);
    }
}
