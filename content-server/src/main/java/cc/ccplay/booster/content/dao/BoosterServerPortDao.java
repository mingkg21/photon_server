package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.dto.content.BoosterAuthDto;
import cc.ccplay.booster.base.model.content.BoosterServerPort;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

@Repository
public class BoosterServerPortDao extends BaseDao<BoosterServerPort>{

    public Page getPage(long serverId){
        String sql = " select a.*,b.id as server_id,b.name as server_name from booster_server_port a left join booster_server b " +
                " on(a.server_id = b.id) "+
                " where server_id = :serverId order by port ";
        return super.page2CamelMap(sql, MixUtil.newHashMap("serverId",serverId));
    }


    public BoosterAuthDto getBoosterAuth(long serverId){
        StringBuffer sql = new StringBuffer(" select a.port,a.password,b.ip,b.method from booster_server_port a ");
        sql.append(" left join booster_server b on(a.server_id = b.id) ");
        sql.append(" where a.server_id = :serverId ");
        return super.query21Model(sql.toString(),MixUtil.newHashMap("serverId",serverId),BoosterAuthDto.class);
    }

}
