package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.BucketImageService;
import cc.ccplay.booster.base.api.user.SysUserService;
import cc.ccplay.booster.base.model.content.BucketImage;
import cc.ccplay.booster.content.dao.BucketImageDao;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.user.SysUser;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.jdbc.model.User;
import org.springframework.beans.factory.annotation.Autowired;

@Service(interfaceClass=BucketImageService.class)
@org.springframework.stereotype.Service
public class BucketImageServiceImpl implements BucketImageService{

    @Autowired
    private BucketImageDao bucketImageDao;

    @Reference
    private SysUserService sysUserService;

    @Override
    public BucketImage save(SystemParam param, BucketImage image) {
        User loginUser = param.getUser();
        if(loginUser != null){
            image.setCreateUserId(loginUser.getSysUserId());
            image.setAccount(loginUser.getLoginAccount());
        }else{
            Long userId = image.getCreateUserId();
            if(userId != null){
                SysUser sysUser = sysUserService.getUser(param,userId);
                if(sysUser != null){
                    image.setAccount(sysUser.getAccount());
                }
            }
        }
        return bucketImageDao.save(image);
    }

    @Override
    public Page queryPage(SystemParam param, BucketImage searchObj, String nameKeyword) {
        return bucketImageDao.queryPage(searchObj, nameKeyword);
    }
}
