package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.model.content.GameHotWord;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.easyj.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class GameHotWordDao extends BaseDao<GameHotWord> {

    public int updateDefaultWorld(long wordId){
        String sql = "update game_hot_word set default_word = 1 where id = :wordId ";
        return super.update(sql, MixUtil.newHashMap("wordId",wordId));
    }

    public int updateOtherDefaultWord(long defaultWordId){
        String sql = "update game_hot_word set default_word = 0 " +
                " where default_word = 1 and id <> :wordId ";
        return super.update(sql, MixUtil.newHashMap("wordId",defaultWordId));
    }

    public Page<GameHotWord> getList(){
        Map paramMap = MixUtil.newHashMap();
        StringBuffer sql = new StringBuffer(" select * from game_hot_word order by ordering ");
        return super.paged2Obj(sql.toString(),paramMap);
    }

    public GameHotWord getDefaultWord(){
        String sql = "select * from game_hot_word where default_word = 1 order by ordering desc limit 1 ";
        return super.query21Model(sql);
    }
    
    public Page queryPage(String name){
        Map paramMap = MixUtil.newHashMap();
        StringBuffer sql = new StringBuffer(" select * from game_hot_word ");
        sql.append(" where 1 = 1 ");
        if(StringUtil.isNotEmpty(name)){
            sql.append(" and name like :name ");
            paramMap.put("name","%"+name+"%");
        }
        sql.append(" order by ordering ");
        return super.page2CamelMap(sql.toString(),paramMap);
    }
}
