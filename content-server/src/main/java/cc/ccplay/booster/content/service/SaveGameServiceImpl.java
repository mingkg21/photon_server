package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.SaveGameService;
import cc.ccplay.booster.base.model.content.SaveGame;
import cc.ccplay.booster.content.dao.GameInfoDao;
import cc.ccplay.booster.content.dao.SaveGameDao;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.save.SaveGameDto;
import cc.ccplay.booster.base.model.content.GameInfo;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;


@Service(interfaceClass=SaveGameService.class)
@org.springframework.stereotype.Service
public class SaveGameServiceImpl extends BaseContentService implements SaveGameService {

    @Autowired
    private SaveGameDao saveGameDao;

    @Autowired
    private GameInfoDao gameInfoDao;

    @Override
    public Page getPage(SystemParam param,Long gameId,String packageName,String name) {
        return saveGameDao.getPage(gameId,packageName,name);
    }

    @Override
    public void save(SaveGame game) {
       Long gameId = game.getGameId();
       if(saveGameDao.get(gameId) != null){
           throw new GenericException("该存档游戏已经存在，不能重新添加");
       }
       saveGameDao.save(game);
    }

    @Override
    public void update(long gameId, String config) {
        SaveGame game = new SaveGame();
        game.setGameId(gameId);
        game.setConfig(config);
        saveGameDao.updateNotNull(game);
    }

    @Override
    public void delete(long gameId) {
        saveGameDao.delete(gameId);
    }

    @Override
    public SaveGameDto getDto(long id) {
        SaveGame saveGame = saveGameDao.get(id);
        if(saveGame == null){
            throw new GenericException("记录不存在");
        }
        GameInfo gameInfo = gameInfoDao.get(id);
        if(gameInfo == null){
            throw new GenericException("游戏不存在");
        }
        SaveGameDto dto = new SaveGameDto();
        dto.setGameInfo(gameInfo);
        dto.setSaveGame(saveGame);
        return dto;
    }

    @Override
    public SaveGame get(long id) {
        return saveGameDao.get(id);
    }

    @Override
    public SaveGame getByPackageName(String packageName) {
        return saveGameDao.getByPackageName(packageName);
    }
}
