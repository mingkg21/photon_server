package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.ContentRedisCacheService;
import cc.ccplay.booster.content.enums.ContentCacheKey;
import com.alibaba.dubbo.config.annotation.Service;
import org.easyj.frame.util.StringUtil;

@Service(interfaceClass = ContentRedisCacheService.class)
@org.springframework.stereotype.Service
public class ContentRedisCacheServiceImpl extends BaseContentService implements ContentRedisCacheService {

    @Override
    public Long getNewGameLastupdateTime() {
        String time = redisTemplate.opsForValue().get(ContentCacheKey.XIN_YOU_LASTUPDATE_TIME.getKey());
        if(time == null){
            return null;
        }
        return StringUtil.toLong(time);
    }


    @Override
    public Long getHotGameRecommendLastupdateTime() {
        String time = redisTemplate.opsForValue().get(ContentCacheKey.HOT_GAME_RECOMMEND_LASTUPDATE_TIME.getKey());
        if(time == null){
            return null;
        }
        return StringUtil.toLong(time);
    }



}
