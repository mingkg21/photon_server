package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.model.content.GameCommentStar;
import org.easyj.frame.jdbc.BaseDao;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class GameCommentStarDao extends BaseDao<GameCommentStar> {

    public GameCommentStar get(long gameId,long userId) {
        GameCommentStar.Id id = new GameCommentStar.Id(gameId,userId);
        return super.get(id);
    }

    public int updateScore(GameCommentStar commentStar){
       String sql = "update game_comment_star " +
               " set star = :star," +
               "data_version = data_version + 1 " +
               " where user_id = :userId and game_id = :gameId and data_version = :dataVersion";
       Map paramMap = new HashMap();
       paramMap.put("dataVersion",commentStar.getDataVersion());
       paramMap.put("userId",commentStar.getId().getUserId());
       paramMap.put("gameId",commentStar.getId().getGameId());
       paramMap.put("star",commentStar.getStar());
       return super.update(sql,paramMap);
    }
}
