package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.dto.content.game.GameRecruitFeedbackDto;
import cc.ccplay.booster.base.model.content.GameRecruitFeedback;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

@Repository
public class GameRecruitFeedbackDao extends BaseDao<GameRecruitFeedback> {

    public GameRecruitFeedback getFeedback(long userId,long recruitId){
        String sql = "select * from game_recruit_feedback where user_id = :userId and recruit_id = :recruitId ";
        return super.query21Model(sql, MixUtil.newHashMap("userId",userId,"recruitId",recruitId));
    }

    public Page<GameRecruitFeedbackDto> getDtoPage(long recruitId) {
        StringBuffer sql = new StringBuffer(" select ");
        sql.append(super.getSelectColStr("feedback","a"));
        sql.append(" FROM game_recruit_feedback ");
        sql.append(" WHERE recruit_id = :recruitId  ");
        sql.append(" AND delete_flag = 0 ");
        sql.append(" AND status =  " + GameRecruitFeedback.STATUS_CHECKED +" ");
        return super.paged2Obj(sql.toString(),MixUtil.newHashMap("recruitId",recruitId),GameRecruitFeedbackDto.class);
    }


}
