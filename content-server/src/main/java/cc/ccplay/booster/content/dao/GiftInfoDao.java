package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.dto.content.gift.GiftInfoDto;
import cc.ccplay.booster.base.model.content.GameVersion;
import cc.ccplay.booster.base.model.content.GiftInfo;
import cc.ccplay.booster.base.model.content.GameInfo;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.easyj.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.Map;

@Repository
public class GiftInfoDao extends BaseDao<GiftInfo> {

    public Page getPage(Long gameId,String giftName){
        Map paramMap = MixUtil.newHashMap();
        StringBuffer sql = new StringBuffer();
        sql.append(" select b.*,c.name as game_name,d.icon,d.version_name ");
        sql.append(" FROM gift_info b ");
        sql.append(" LEFT JOIN game_info c ");
        sql.append(" ON(b.game_id = c.id) ");
        sql.append(" LEFT JOIN game_version d ");
        sql.append(" ON(d.id = c.lastest_version_id ) ");
        sql.append(" where 1 = 1 ");
        if(gameId != null){
            sql.append(" and b.game_id = :gameId ");
            paramMap.put("gameId",gameId);
        }
        if(StringUtil.isNotEmpty(giftName)){
            sql.append(" and b.name like :giftName ");
            paramMap.put("giftName","%"+giftName+"%");
        }
        sql.append(" order by b.id desc ");
        return super.page2CamelMap(sql.toString(),paramMap);
    }

    public GiftInfoDto getGiftInfoDto(long id) {
        StringBuffer sql = new StringBuffer();
        sql.append(" SELECT ");
        sql.append(super.getSelectColStr(GiftInfo.class,"a","giftInfo"));
        sql.append(",");
        sql.append(super.getSelectColStr(GameInfo.class,"b","gameInfo"));
        sql.append(",");
        sql.append(super.getSelectColStr(GameVersion.class,"c","versionInfo"));
        sql.append(" FROM gift_info a ");
        sql.append(" LEFT JOIN game_info b ");
        sql.append(" ON(a.game_id = b.id) ");
        sql.append(" LEFT JOIN game_version c ");
        sql.append(" ON(c.id = b.lastest_version_id) ");
        sql.append(" where a.id = :id ");
        return super.query21Model(sql.toString(),MixUtil.newHashMap("id",id),GiftInfoDto.class);
    }


    public Page<GiftInfoDto> getPageByGameId(long gameId,Long userId) {
        Map paramMap = MixUtil.newHashMap("gameId",gameId,"now",new Date());
        StringBuffer sql = new StringBuffer();
        sql.append(" SELECT ");
        sql.append(super.getSelectColStr(GiftInfo.class,"a","giftInfo"));
        sql.append(",");
        sql.append(super.getSelectColStr(GameInfo.class,"b","gameInfo"));
        sql.append(",");
        sql.append(super.getSelectColStr(GameVersion.class,"c","versionInfo"));
        if(userId != null){
            sql.append(",d.type,d.code ");
        }
        sql.append(" FROM gift_info a ");
        sql.append(" LEFT JOIN game_info b ");
        sql.append(" ON(a.game_id = b.id) ");
        sql.append(" LEFT JOIN game_version c ");
        sql.append(" ON(c.id = b.lastest_version_id) ");
        if(userId != null) {
            sql.append(" LEFT JOIN gift_receive_record d ");
            sql.append(" ON(d.user_id = :userId and d.gift_id = a.id) ");
            paramMap.put("userId",userId);
        }
        sql.append(" where a.game_id = :gameId ");
        sql.append(" and a.start_time < :now ");
        sql.append(" and a.end_time > :now ");
        sql.append(" and a.status = " + GiftInfo.STATUS_OPEN);
        return super.paged2Obj(sql.toString(),paramMap,GiftInfoDto.class);
    }

    //增加礼包码数量
    public int addGiftCount(long id,long value){
        String sql = "update gift_info set gift_count = gift_count + :value where id = :id ";
        return super.update(sql,MixUtil.newHashMap("value",value,"id",id));
    }

    public int addReceiveCount(long id,long value){
        String sql = "update gift_info set receive_count = receive_count + :value where id = :id ";
        return super.update(sql,MixUtil.newHashMap("value",value,"id",id));
    }

    //获取应用包的有效期内的礼包数
    public int getAppGiftCount(long appId){
        Map paramMap = MixUtil.newHashMap("appId",appId,"now",new Date());
        StringBuffer sql = new StringBuffer();
        sql.append(" select count(1) from gift_info a ");
        sql.append(" where a.game_id = :appId ");
        sql.append(" and a.start_time < :now ");
        sql.append(" and a.end_time > :now ");
        sql.append(" and a.status = " + GiftInfo.STATUS_OPEN);
        return super.queryForInt(sql.toString(),paramMap);
    }

}
