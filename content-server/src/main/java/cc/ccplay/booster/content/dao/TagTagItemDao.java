package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.model.content.TagTagItem;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class TagTagItemDao extends BaseDao<TagTagItem> {


    public Page getTagPage(long mainTagId, String tagName){
        Map paramMap = new HashMap();
        paramMap.put("mainTagId",mainTagId);
        StringBuffer sql = new StringBuffer(" select b.id,b.name,a.ordering from tag_tag_item a ");
        sql.append(" left join game_tag b on(a.tag_id = b.id) ");
        sql.append(" where main_tag_id = :mainTagId");
        if(StringUtil.isNotEmpty(tagName)){
            sql.append(" and b.name like :tagName ");
            paramMap.put("tagName","%"+tagName+"%");
        }
        sql.append(" order by a.ordering ");
        return super.page2CamelMap(sql.toString(), paramMap);
    }


    public List getChildTagList(long mainTagId, int topSize) {
        Map paramMap = new HashMap();
        paramMap.put("mainTagId",mainTagId);
        paramMap.put("topSize",topSize);
        StringBuffer sql = new StringBuffer(" select b.id,b.name,a.ordering from tag_tag_item a ");
        sql.append(" left join game_tag b on(a.tag_id = b.id) ");
        sql.append(" where main_tag_id = :mainTagId");
        sql.append(" order by a.ordering limit :topSize ");
        return super.selectCamelListMap(sql.toString(), paramMap);
    }
}
