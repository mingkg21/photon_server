package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.SaveRecordCommentService;
import cc.ccplay.booster.base.api.user.UserAccountService;
import cc.ccplay.booster.base.api.user.UserSystemMessageService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.save.SaveRecordCommentDto;
import cc.ccplay.booster.base.enums.MsgContentType;
import cc.ccplay.booster.base.enums.MsgTagType;
import cc.ccplay.booster.base.model.content.SaveRecord;
import cc.ccplay.booster.base.model.content.SaveRecordComment;
import cc.ccplay.booster.base.model.user.UserSystemMessage;
import cc.ccplay.booster.base.reader.dto.UserInfo;
import cc.ccplay.booster.content.dao.SaveRecordCommentDao;
import cc.ccplay.booster.content.dao.SaveRecordDao;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.easyj.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@Service(interfaceClass = SaveRecordCommentService.class)
@org.springframework.stereotype.Service
public class SaveRecordCommentServiceImpl extends BaseContentService implements SaveRecordCommentService {

    @Autowired
    private SaveRecordCommentDao saveRecordCommentDao;

    @Autowired
    private SaveRecordDao saveRecordDao;

    @Reference
    private UserSystemMessageService userSystemMessageService;

    @Reference
    private UserAccountService userAccountService;


    @Override
    public SaveRecordComment comment(SaveRecordComment comment) {
        Long recordId = comment.getRecordId();
        SaveRecord record = saveRecordDao.get(recordId);
        if(record == null){
            throw new GenericException("存档不存在");
        }
        SaveRecordComment result = saveRecordCommentDao.save(comment);
        saveRecordDao.addCommentCount(recordId);
        return result;
    }

    @Override
    public SaveRecordComment reply(SaveRecordComment reply) {
        Long commentId = reply.getParentId();
        SaveRecordComment parantComment = saveRecordCommentDao.get(commentId);
        reply.setRecordId(parantComment.getRecordId());
        if(parantComment == null){
            throw new GenericException("回复的对象不存在");
        }

        Long recordId = parantComment.getRecordId();
        SaveRecord record = saveRecordDao.get(recordId);
        if(record == null){
            throw new GenericException("存档不存在");
        }

        Long parentId = parantComment.getParentId();
        Long userId = parantComment.getUserId();
        Long currentUserId = reply.getUserId();
        boolean isSameUser = StringUtil.equals(userId,reply.getUserId());
        if(parentId != null){//对回复进行回复
            reply.setParentId(parentId);
            if(!isSameUser){
                reply.setBeRepliedUserId(userId);
            }
        }else{//对评价进行回复
            reply.setParentId(commentId);
        }
        //评论 回复+1
        saveRecordCommentDao.addReplyCount(commentId);
        SaveRecordComment comment = saveRecordCommentDao.save(reply);


        if(!isSameUser) {
            try {
                UserSystemMessage msg = new UserSystemMessage();
                MsgContentType contentType = MsgContentType.SAVE_GAME_COMMENT_BEREPLIED;
                msg.setMsgType(UserSystemMessage.MSG_TYPE_GENERAL);
                msg.setUserId(userId);
                msg.setObjectId(comment.getId());
                msg.setObjectTitle("回复ID");
                msg.setMsgContent(reply.getContent());
                msg.setContentType(new Long(contentType.getValue()));
                UserInfo userInfo = userAccountService.getUserInfo(currentUserId);
                String title = contentType.getTitle(userInfo.getNickName(),record.getName());
                msg.setMsgTitle(title);
                JSONObject json = new JSONObject();
                json.put("msgTitle",title);
                json.put("objectTitle","回复ID");
                json.put("objectId",msg.getObjectId());
                json.put("contentType",contentType.getValue());
                json.put("userId",msg.getUserId());
                json.put("msgType",msg.getMsgType());
                json.put("msgContent",msg.getMsgContent());
                JSONArray contentArr = new JSONArray();
                //int a = 0xff000000;
                contentArr.add(MixUtil.newHashMap("id",currentUserId,"type", MsgTagType.USER_INFO,"value",userInfo.getNickName(),"bold",1));
                contentArr.add(MixUtil.newHashMap("type", MsgTagType.TEXT,"value","回复你在游戏存档"));
                contentArr.add(MixUtil.newHashMap("id",recordId,"type", MsgTagType.TEXT,"value","《"+record.getName()+"》"));
                contentArr.add(MixUtil.newHashMap("type", MsgTagType.TEXT,"value","的评论."));
                contentArr.add(MixUtil.newHashMap("type", MsgTagType.TEXT,"value","\r\n回复内容: ","bold",1));
                contentArr.add(MixUtil.newHashMap("type", MsgTagType.TEXT,"value",reply.getContent(),"color",0xff999999));

                json.put("content",contentArr);

                JSONArray titleArr = new JSONArray();
                titleArr.add(MixUtil.newHashMap("type", MsgTagType.TEXT,"value","有人回复了你的评论"));
                json.put("title",titleArr);

                msg.setPushMsg(json.toJSONString());
                userSystemMessageService.pushMsg2One(msg);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        return comment;
    }

    @Override
    public Page<SaveRecordCommentDto> getCommentPage(SystemParam param, long recordId) {
        Page<SaveRecordCommentDto> page = saveRecordCommentDao.getCommentPage(recordId,null);
        super.readManyUserInfo(page.getList());
        return page;
    }

    @Override
    public Page<SaveRecordCommentDto> getReplyPage(SystemParam param, long commentId) {
        Page<SaveRecordComment> page = saveRecordCommentDao.getReplyPage(commentId,null);
        return toDtoPage(page);
    }

    private Page<SaveRecordCommentDto> toDtoPage(Page<SaveRecordComment> page){
        List<SaveRecordComment> list = page.getList();
        int size = list.size();
        List<SaveRecordCommentDto> result = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            SaveRecordComment comment = list.get(i);
            SaveRecordCommentDto dto = new SaveRecordCommentDto();
            dto.setComment(comment);
            result.add(dto);
        }
        super.readManyUserInfo(result);
        return new Page<>(page.getStart(),page.getTotalCount(),page.getPageSize(),result);
    }

    @Override
    public SaveRecordCommentDto getComment(long commentId) {
        SaveRecordCommentDto dto = new SaveRecordCommentDto();
        SaveRecordComment gameComment = saveRecordCommentDao.get(commentId);
        if(gameComment == null){
            throw new GenericException("数据不存在");
        }
        dto.setComment(gameComment);
        super.readManyUserInfo(dto);
        return dto;
    }

}
