package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.GameSpecialCategoryService;
import cc.ccplay.booster.content.dao.GameSpecialCategoryDao;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.game.SpecialCategoryTagDto;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.GameSpecialCategory;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service(interfaceClass = GameSpecialCategoryService.class)
@org.springframework.stereotype.Service
public class GameSpecialCategoryServiceImpl extends BaseContentService implements GameSpecialCategoryService {

    @Autowired
    private GameSpecialCategoryDao gameSpecialCategoryDao;

    @Override
    public Page getPage(SystemParam param,String name) {
        return gameSpecialCategoryDao.getPage(name);
    }

    @Override
    public GameSpecialCategory saveOrUpdate(GameSpecialCategory category) {
        Long id = category.getId();
        if(id == null){
            category.setStatus(Status.ENABLED);
        }
        if(gameSpecialCategoryDao.isNotUnique(category,"name")){
            throw new GenericException("类别["+category.getName()+"]已经存在");
        }
        return gameSpecialCategoryDao.saveOrUpdate(category);
    }

    @Override
    public int updateStatus(long id, Status status) {
        GameSpecialCategory updateCategory = new GameSpecialCategory();
        updateCategory.setId(id);
        updateCategory.setStatus(status);
        return gameSpecialCategoryDao.updateNotNull(updateCategory);
    }

    @Override
    public GameSpecialCategory get(long id) {
        return gameSpecialCategoryDao.get(id);
    }


    @Override
    public List<GameSpecialCategory> getList(int limitSize) {
        return gameSpecialCategoryDao.getList(limitSize);
    }

    @Override
    public Page<SpecialCategoryTagDto> getTagList(SystemParam param,long categoryId) {
        return gameSpecialCategoryDao.getTagList(categoryId);
    }
}
