package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.GameInfoService;
import cc.ccplay.booster.base.api.content.GameTagService;
import cc.ccplay.booster.base.core.AppHeaderInfo;
import cc.ccplay.booster.base.core.PagingParameter;
import cc.ccplay.booster.base.dto.SelectizeDto;
import cc.ccplay.booster.base.dto.content.game.GameInfoDto;
import cc.ccplay.booster.base.dto.content.game.GameTagItemDto;
import cc.ccplay.booster.base.model.content.*;
import cc.ccplay.booster.content.dao.*;
import cc.ccplay.booster.content.dao.*;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.*;
import cc.ccplay.booster.base.util.AppVersionUtil;
import cc.ccplay.booster.content.dao.*;
import org.apache.log4j.Logger;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@Service(interfaceClass = GameTagService.class)
@org.springframework.stereotype.Service
public class GameTagServiceImpl extends BaseContentService implements GameTagService {

    private static final Logger log = Logger.getLogger(GameTagServiceImpl.class);

    @Autowired
    private GameTagDao gameTagDao;

    @Reference
    private GameInfoService gameInfoService;

    @Autowired
    private GameVersionDao gameVersionDao;

    @Autowired
    private GameTagItemDao gameTagItemDao;

    @Autowired
    private GameInfoDao gameInfoDao;

    @Autowired
    private TagTagItemDao tagTagItemDao;

    @Override
    public Page getPage(SystemParam param,GameTag searchParam) {
        return gameTagDao.getPage(searchParam);
    }

    @Override
    public GameTag saveOrUpdate(SystemParam param, GameTag tag) {
        Long id = tag.getId();
        if(id == null){
            tag.setStatus(Status.ENABLED);
        }
        if(gameTagDao.isNotUnique(tag,"name,type")){
            throw new GenericException("标签["+tag.getName()+"]已经存在");
        }
        return gameTagDao.saveOrUpdate(tag);
    }

    @Override
    public int updateStatus(SystemParam param, long id, Status status) {
        GameTag updateTag = new GameTag();
        updateTag.setId(id);
        updateTag.setStatus(status);
        return gameTagDao.updateNotNull(updateTag);
    }

    @Override
    public GameTag get(SystemParam param, long id) {
        return gameTagDao.get(id);
    }

    @Override
    public List<SelectizeDto> getForSelectize(SystemParam param, GameTag searchParam) {
        Page page = gameTagDao.getPage(searchParam);
        return super.toSelectizeData(page,"id","name");
    }

    @Override
    public Page<GameTag> getList(SystemParam param,Long tagType) {
        AppHeaderInfo appHeaderInfo = param.getAppHeaderInfo();
        boolean showCategoryTag = false;
        if(StringUtil.equals(tagType,GameTag.TYPE_SPECIAL) && AppVersionUtil.greater(appHeaderInfo,AppVersionUtil.V1_1)){
            //1.2版本多出一个分类入口，之前版本不显示
            showCategoryTag = true;
        }
        return gameTagDao.getList(tagType,0,showCategoryTag);
    }

    @Override
    public Page<TagRecommendGame> getTagRecommendGame(SystemParam param) {
        Page<GameTag> page = gameTagDao.getList(GameTag.TYPE_COMMON,1,false);

        List<TagRecommendGame> data = new ArrayList<>();
        Page<TagRecommendGame> result = new Page<>(page.getStart(),page.getTotalCount(),page.getPageSize(),data);

        List<GameTag> tags = page.getList();
        int size = tags.size();

        PagingParameter pagingParameter = new PagingParameter();
        pagingParameter.setPageNo(1);
        pagingParameter.setPageSize(9);
        param.setPagingParameter(pagingParameter);
        for (int i = 0; i < size; i++) {
            GameTag tag = tags.get(i);
            Page<GameInfoDto> games = gameInfoService.getGameByTag(param,tag.getId(),null,null,null);
            TagRecommendGame tagGame = new TagRecommendGame();
            tagGame.setGameTag(tag);
            tagGame.setGames((List<GameInfoDto>)games.getResults());
            data.add(tagGame);
        }
        return result;
    }

    @Override
    public void bindGame(long tagId, long gameId,long ordering) {
        GameTagItem.Id id = new GameTagItem.Id(gameId,tagId);
        GameTagItem item = new GameTagItem();
        item.setId(id);
        item.setOrdering(ordering);
        if(gameTagItemDao.get(id) != null){
            gameTagItemDao.updateNotNull(item);
        }else{
            gameTagItemDao.save(item);
            gameTagDao.addAppCount(1,tagId);
        }
    }

    @Override
    public void unbindGame(long tagId, long gameId) {
        GameTagItem.Id id = new GameTagItem.Id(gameId,tagId);
        if(gameTagItemDao.delete(id) > 0){
            gameTagDao.addAppCount(-1,tagId);
        }
    }

    @Override
    public GameTagItemDto getItemDto(GameTagItem.Id itemId) {
        GameTagItem item = gameTagItemDao.get(itemId);
        if(item == null){
            return null;
        }
        Long gameId = itemId.getGameId();
        GameTagItemDto dto = new GameTagItemDto();
        dto.setGameTagItem(item);
        GameInfo gameInfo = gameInfoDao.get(gameId);
        if(gameInfo != null){
            dto.setGameInfo(gameInfo);
            Long lastestVersionId = gameInfo.getLastestVersionId();
            dto.setVersionInfo(gameVersionDao.get(lastestVersionId));
        }
        return dto;
    }


    /**
     * 通过特色标签获取所有游戏绑定标签
     * 再将这些普通标签绑定到特色标签下
     *
     * @param specialTagId
     * @param count
     */
    @Override
    public int batchBindTag(long specialTagId, int count) {
        List<GameTag> list = gameTagDao.getSpecialTagGameCommonTags(specialTagId,count);
        int size = list.size();
        int bindCount = 0;
        for (int i = 0; i < size; i++) {
            GameTag tag = list.get(i);
            Long commonTagId = tag.getId();
            TagTagItem.Id id = new TagTagItem.Id(specialTagId,commonTagId);
            if(tagTagItemDao.get(id) == null){
                TagTagItem item = new TagTagItem();
                item.setId(id);
                item.setOrdering(new Long(bindCount+1));
                tagTagItemDao.save(item);
                gameTagDao.addChildTagCount(specialTagId,1);
                ++bindCount;
            }
        }
        return bindCount;
    }
}
