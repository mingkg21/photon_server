package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.model.content.AppRelease;
import cc.ccplay.booster.content.dao.AppReleaseDao;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.api.content.DebugService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service(interfaceClass = DebugService.class)
@org.springframework.stereotype.Service
public class DebugServiceImpl extends BaseContentService implements DebugService {

    private final static Logger log = Logger.getLogger(DebugServiceImpl.class);

    @Autowired
    private AppReleaseDao appReleaseDao;

    @Override
    public List readData(String sql) {
        long start = System.currentTimeMillis();
        log.error("开始读取数据集:"+sql);
        List data = appReleaseDao.selectCamelListMap(sql);
        long end = System.currentTimeMillis();
        log.error("结束读取数据集["+(end-start)+"ms]:"+sql);
        return data;
    }

    @Override
    public Object readData() {
        long start = System.currentTimeMillis();
        log.error("开始读取app最后版本");
        AppRelease appRelease = appReleaseDao.getLastestVersion();
        long end = System.currentTimeMillis();
        log.error("结束读取app最后版本["+(end-start)+"ms]");
        return appRelease;
    }

}
