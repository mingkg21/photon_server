package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.model.content.SpecialCategoryTagItem;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class SpecialCategoryTagItemDao extends BaseDao<SpecialCategoryTagItem> {


    public Page getTagPage(long categroyId, String tagName){
        Map paramMap = new HashMap();
        paramMap.put("categroyId",categroyId);
        StringBuffer sql = new StringBuffer(" select b.id,b.name,a.ordering from special_category_tag_item a ");
        sql.append(" left join game_tag b on(a.tag_id = b.id) ");
        sql.append(" where category_id = :categroyId");
        if(StringUtil.isNotEmpty(tagName)){
            sql.append(" and b.name like :tagName ");
            paramMap.put("tagName","%"+tagName+"%");
        }
        sql.append(" order by a.ordering ");
        return super.page2CamelMap(sql.toString(), paramMap);
    }


}
