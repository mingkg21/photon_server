package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.model.content.GameCommentStarRange;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

@Repository
public class GameCommentStarRangeDao extends BaseDao<GameCommentStarRange> {

    /**
     * 旧评星数 -1  新评星数 + 1
     * @param gameId
     * @param newScore
     * @param oldScore
     * @return
     */
    public int updateScoreCount(long gameId,long newScore,long oldScore){
        if(newScore == oldScore){
            return 0;
        }
        String oldScoreColumn =  "star" +(oldScore/2);
        String newScoreColumn =  "star" +(newScore/2);
        StringBuffer sql = new StringBuffer();
        sql.append(" update game_comment_star_range set ");
        sql.append(" "+oldScoreColumn+" = "+oldScoreColumn+" - 1 ,");
        sql.append(" "+newScoreColumn+" = "+newScoreColumn+" + 1 ");
        sql.append(" WHERE game_id = :gameId ");
        return super.update(sql.toString(), MixUtil.newHashMap("gameId",gameId));
    }

    public int incrScoreCount(long gameId,long score){
        String scoreColumn =  "star" +(score/2);
        StringBuffer sql = new StringBuffer();
        sql.append(" update game_comment_star_range set ");
        sql.append(" "+scoreColumn+" = "+scoreColumn+" + 1 ");
        sql.append(" WHERE game_id = :gameId ");
        return super.update(sql.toString(), MixUtil.newHashMap("gameId",gameId));
    }
}
