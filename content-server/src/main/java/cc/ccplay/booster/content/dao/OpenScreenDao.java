package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.OpenScreen;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.springframework.stereotype.Repository;

@Repository
public class OpenScreenDao extends BaseDao<OpenScreen> {

    public Page queryPage(){
        String sql = "select * from open_screen order by id desc ";
        return super.page2CamelMap(sql);
    }


    public OpenScreen getLastest(){
        String sql = " select * from open_screen where status = "+ Status.ENABLED.getValue()
                +" order by create_time limit 1 ";
        return super.query21Model(sql);
    }
}
