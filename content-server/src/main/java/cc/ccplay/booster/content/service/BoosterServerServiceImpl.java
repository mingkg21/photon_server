package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.BaseService;
import cc.ccplay.booster.base.api.content.BoosterServerService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.SelectizeDto;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.BoosterServer;
import cc.ccplay.booster.content.dao.BoosterServerDao;
import cc.ccplay.booster.content.dao.BoosterServerPortDao;
import com.alibaba.dubbo.config.annotation.Service;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Random;

@Service(interfaceClass = BoosterServerService.class)
@org.springframework.stereotype.Service
public class BoosterServerServiceImpl extends BaseService implements BoosterServerService  {

    @Autowired
    private BoosterServerDao boosterServerDao;

    @Autowired
    private BoosterServerPortDao boosterServerPortDao;

    @Override
    public BoosterServer get(long id) {
        return boosterServerDao.get(id);
    }

    @Override
    public void updateStatus(long id, Status status) {
        BoosterServer updateModel = new BoosterServer();
        updateModel.setId(id);
        updateModel.setStatus(status.getValue());
        boosterServerDao.updateNotNull(updateModel);
    }

    @Override
    public void saveOrUpdate(BoosterServer boosterServer) {
//        if(boosterServerDao.isNotUnique(boosterServer,"ip")){
//            throw new GenericException("IP["+boosterServer.getIp()+"]已被使用");
//        }
        boosterServerDao.saveOrUpdate(boosterServer);
    }

    @Override
    public Page getPage(SystemParam param,String name,Status status) {
        return boosterServerDao.getPage(name,status);
    }

    @Override
    public BoosterServer getLoadServer(long gameId){
        return boosterServerDao.getLoadServer(gameId,null);
    }


    @Override
    public void resetDefaultServer(long id) {
        boosterServerDao.cancelDefServer();
        BoosterServer server = new BoosterServer();
        server.setId(id);
        server.setDefServer(1L);
        boosterServerDao.updateNotNull(server);
    }

    @Override
    public List<SelectizeDto> getForSelectize(SystemParam param, String name) {
        Page page = boosterServerDao.getPage(null,Status.ENABLED);
        return super.toSelectizeData(page,"id","name");
    }
}
