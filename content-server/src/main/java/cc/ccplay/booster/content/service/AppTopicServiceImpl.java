package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.AppTopicService;
import cc.ccplay.booster.base.dto.content.AppTopicDto;
import cc.ccplay.booster.base.model.content.AppTopic;
import cc.ccplay.booster.content.dao.AppTopicDao;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.content.dao.AppTopicItemDao;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service(interfaceClass = AppTopicService.class)
@org.springframework.stereotype.Service
public class AppTopicServiceImpl extends BaseContentService implements AppTopicService{

    @Autowired
    private AppTopicDao appTopicDao;

    @Autowired
    private AppTopicItemDao appTopicItemDao;

    @Override
    public void saveOrUpdate(AppTopic appTopic) {
        appTopicDao.saveOrUpdate(appTopic);
    }

    @Override
    public void updateStatus(long id, Status status) {
        AppTopic updateModel = new AppTopic();
        updateModel.setId(id);
        updateModel.setStatus(status.getValue());
        appTopicDao.updateNotNull(updateModel);
    }

    @Override
    public Page getPage(SystemParam param) {
        return appTopicDao.getPage();
    }

    @Override
    public AppTopic get(long id) {
        return appTopicDao.get(id);
    }


    @Override
    public Page<AppTopicDto> getDtoPage(SystemParam param) {
        Page<AppTopicDto> page = appTopicDao.getDtoPage();
        List<AppTopicDto> list = page.getList();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            AppTopicDto dto = list.get(i);
            Long topicId = dto.getId();
            dto.setItems(appTopicItemDao.getItemDtoList(topicId));
        }
        return page;
    }
}
