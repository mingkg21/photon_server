package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.model.content.BoosterServerGroupGame;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class BoosterServerGroupGameDao extends BaseDao<BoosterServerGroupGame> {

    public Page getPage(Long gameId, Long groupId,String gameName){
        Map paramMap = new HashMap();
        StringBuffer sql = new StringBuffer();
        sql.append(" select a.*,b.name as game_name,c.icon as game_icon,d.name as group_name from booster_server_group_game a ");
        sql.append(" left join game_info b on(b.id = a.game_id ) ");
        sql.append(" left join game_version c on(b.lastest_version_id = c.id ) ");
        sql.append(" left join booster_server_group d on(d.id = a.group_id ) ");
        sql.append(" where 1 = 1 ");
        if(gameId != null){
            sql.append(" and a.game_id = :gameId ");
            paramMap.put("gameId",gameId);
        }
        if(groupId != null){
            sql.append(" and a.group_id = :groupId ");
            paramMap.put("groupId",groupId);
        }

        if(StringUtil.isNotEmpty(gameName)){
            sql.append(" and b.name like :gameName ");
            paramMap.put("gameName","%"+gameName+"%");
        }

        return super.page2CamelMap(sql.toString(),paramMap);
    }
}
