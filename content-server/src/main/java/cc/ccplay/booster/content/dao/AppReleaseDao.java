package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.model.content.AppRelease;
import cc.ccplay.booster.base.enums.Status;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.springframework.stereotype.Repository;

@Repository
public class AppReleaseDao extends BaseDao<AppRelease> {

    public Page getPage(){
        String sql = " select * from app_release order by id desc ";
        return super.page2CamelMap(sql);
    }

    public AppRelease getLastestVersion(){
        String sql = " select * from app_release where status = "+ Status.ENABLED.getValue()+" order by release_time desc limit 1 ";
        return super.query21Model(sql);
    }
}
