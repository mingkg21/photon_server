package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.dto.content.game.GameEvaluationDto;
import cc.ccplay.booster.base.model.content.GameEvaluation;
import cc.ccplay.booster.content.dao.GameEvaluationDao;
import cc.ccplay.booster.content.dao.GameInfoDao;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.api.content.GameEvaluationService;
import cc.ccplay.booster.base.core.SystemParam;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;

@Service(interfaceClass = GameEvaluationService.class)
@org.springframework.stereotype.Service
public class GameEvaluationServiceImpl implements GameEvaluationService {

    @Autowired
    private GameEvaluationDao gameEvaluationDao;

    @Autowired
    private GameInfoDao gameInfoDao;

    @Override
    public void saveEvaluation(SystemParam param, GameEvaluation evaluation) {
        gameEvaluationDao.save(evaluation);
        gameInfoDao.addTestCount(evaluation.getGameId(),1);
    }

    @Override
    public Page<GameEvaluationDto> getUserEvaluationPage(SystemParam param, long gameId) {
        return null;
    }

}
