package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.GameVersionService;
import cc.ccplay.booster.base.model.content.GameVersion;
import cc.ccplay.booster.base.model.content.GameVersionPicture;
import cc.ccplay.booster.content.dao.GameInfoDao;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.game.EditGameVersionDto;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.exception.NotSamePackageNameException;
import cc.ccplay.booster.base.model.content.GameInfo;
import cc.ccplay.booster.content.dao.GameVersionDao;
import cc.ccplay.booster.content.dao.GameVersionPictureDao;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

@Service(interfaceClass = GameVersionService.class)
@org.springframework.stereotype.Service
public class GameVersionServiceImpl implements GameVersionService {

    @Autowired
    private GameVersionDao gameVersionDao;

    @Autowired
    private GameInfoDao gameInfoDao;

    @Autowired
    private GameVersionPictureDao gameVersionPictureDao;

    @Override
    public GameVersion saveOrUpdate(GameVersion gameVersion, List<GameVersionPicture> pics, boolean replacePackageName, String packageName) {
        Long versionId = gameVersion.getId();
        Long gameId = gameVersion.getGameId();


        if(StringUtil.equals(gameVersion.getVersionType(),GameVersion.VERSION_TYPE_RELEASE)) {
            GameInfo oldGame = gameInfoDao.get(gameId);
            if (oldGame == null) {
                throw new GenericException("游戏不存在");
            }

            boolean samePackage = packageName.equals(oldGame.getPackageName());
            if (replacePackageName && !samePackage) {//强制替换包名
                GameInfo updateGame = new GameInfo();
                updateGame.setId(gameId);
                updateGame.setPackageName(packageName);
                gameInfoDao.updateNotNull(updateGame);
            } else if (!samePackage) {
                throw new NotSamePackageNameException(oldGame.getPackageName(), packageName);
            }

            if (gameVersionDao.isNotUnique(gameVersion, "versionName,gameId")) {
                throw new GenericException("版本名[" + gameVersion.getVersionName() + "]已经被其他应用使用");
            }
        }

        if(versionId != null){//修改
            gameVersion.setGameId(null);//不能修改gameId
        }

        gameVersionDao.saveOrUpdate(gameVersion);
        if(versionId == null){
            versionId = gameVersion.getId();
        }
        //设置为最新版本
        GameInfo gameInfo = new GameInfo();
        gameInfo.setLastestVersionId(versionId);
        gameInfo.setId(gameId);
        gameInfoDao.updateNotNull(gameInfo);

        if(versionId != null){//先删除 后保存
            gameVersionPictureDao.deleteByVersionId(versionId);
        }

        int size = pics.size();
        for (int i = 0; i < size; i++) {
            pics.get(i).setVersionId(versionId);
        }
        gameVersionPictureDao.batchInsert(pics);
        return gameVersion;
    }

    @Override
    public GameVersion get(long id) {
        return gameVersionDao.get(id);
    }

    @Override
    public Page getPage(SystemParam param,long gameId) {
        return gameVersionDao.getPage(gameId);
    }

    @Override
    public EditGameVersionDto getEditInfo(SystemParam param, long id) {
        GameVersion gameVersion = gameVersionDao.get(id);
        if(gameVersion == null){
            throw new GenericException("版本信息不存在");
        }
        EditGameVersionDto dto = new EditGameVersionDto();
        dto.setGameVersion(gameVersion);
        dto.setPics(gameVersionPictureDao.getListByVersionId(id));
        return dto;
    }

    @Override
    public EditGameVersionDto getLastVersionDto(long gameId) {
        GameInfo gameInfo = gameInfoDao.get(gameId);
        if(gameInfo == null){
            throw new GenericException("游戏信息不存在");
        }
        EditGameVersionDto dto = new EditGameVersionDto();

        Long versionId = gameInfo.getLastestVersionId();
        if(versionId == null){
            return dto;
        }
        GameVersion gameVersion = gameVersionDao.get(versionId);
        if(gameVersion != null){
            GameVersion newVersion = new GameVersion();
            newVersion.setIcon(gameVersion.getIcon());
            dto.setGameVersion(newVersion);
        }
        dto.setPics(gameVersionPictureDao.getListByVersionId(versionId));
        return dto;
    }

    @Override
    public void setLastestVersion(SystemParam param, long id) {
        GameVersion versionInfo = gameVersionDao.get(id);
        if(versionInfo == null){
            throw new GenericException("版本信息不存在");
        }
        GameVersion updateVersion = new GameVersion();
        updateVersion.setId(id);
        updateVersion.setReleaseTime(new Date());
        gameVersionDao.updateNotNull(updateVersion);

        long gameId = versionInfo.getGameId();
        long versionId = versionInfo.getId();
        GameInfo gameInfo = new GameInfo();
        gameInfo.setLastestVersionId(versionId);
        gameInfo.setId(gameId);
        gameInfo.setPackageName(versionInfo.getPackageName());
        gameInfoDao.updateNotNull(gameInfo);
    }

    @Override
    public List<GameVersion> getAllVersion(SystemParam param, long gameId) {
        return gameVersionDao.getAllVersionByGameId(gameId);
    }

    @Override
    public void updateStatus(long id, Status status) {
        GameVersion updateModel = new GameVersion();
        updateModel.setId(id);
        updateModel.setStatus(status.getValue());
        gameVersionDao.updateNotNull(updateModel);
    }
}
