package cc.ccplay.booster.content.enums;

/**
 * 专区编码
 */
public class AreaCode {

    /**
     * 游戏下载排行榜
     */
    public static final String DOWNLOAD_RANKING_LIST = "download-ranking-list";

    /**
     * 新游戏下载排行 (一周内创建的游戏)
     */
    public static final String NEW_GAME_DOWNLOAD_RANKING_LIST = "new-game-download-ranking-list";

    /**
     * 单机游戏下载排行
     */
    public static final String SINGLE_GAME_RANKING_LIST = "single-game-ranking-list";

    /**
     * 网游排行榜
     */
    public static final String ONLINE_GAME_RANKING_LIST = "online-game-ranking-list";

    /**
     * 外服游戏
     */
    public static final String OUTERWEAR_GAME = "outerwear-game";

    /**
     * 无需加速的游戏
     */
    public static final String RUN_WITHOUT_BOOSTER_GAME = "run-without-booster-game";


    /**
     * 运营测试专区
     */
    public static final String TEST_GAME = "test-game";

}
