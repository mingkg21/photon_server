package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.model.content.BoosterServerGroup;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class BoosterServerGroupDao extends BaseDao<BoosterServerGroup> {

    public Page getPage(String name){
        Map paramMap = new HashMap();
        StringBuffer sql = new StringBuffer(" select * from booster_server_group ");
        sql.append(" where 1 = 1 ");
        if(StringUtil.isNotEmpty(name)){
            sql.append(" and name like :name ");
            paramMap.put("name","%"+name+"%");
        }
        sql.append(" order by id desc ");
        return super.page2CamelMap(sql.toString(),paramMap);
    }

}
