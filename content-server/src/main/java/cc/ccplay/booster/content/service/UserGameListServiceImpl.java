package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.dto.content.game.GameInfoDto;
import cc.ccplay.booster.content.dao.GameInfoDao;
import cc.ccplay.booster.content.dao.UserGameListDao;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.api.content.UserGameListService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.content.GameInfo;
import cc.ccplay.booster.base.model.content.UserGameList;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service(interfaceClass = UserGameListService.class)
@org.springframework.stereotype.Service
public class UserGameListServiceImpl extends BaseContentService implements UserGameListService {

    @Autowired
    private GameInfoDao gameInfoDao;

    @Autowired
    private UserGameListDao userGameListDao;

    @Override
    public void submitAppList(long userId, List<String> packageNames) {
        List<GameInfo> gameList = gameInfoDao.getGameIdByPackageName(packageNames);
        List<UserGameList> insertList = new ArrayList<>();
        Date now = new Date();
        int size = gameList.size();
        for (int i = 0; i < size; i++) {
            Long gameId = gameList.get(i).getId();
            UserGameList.Id id = new UserGameList.Id(userId,gameId);
            if(userGameListDao.get(id) == null){
                UserGameList userGameList = new UserGameList();
                userGameList.setId(id);
                userGameList.setCreateTime(now);
                insertList.add(userGameList);
            }
        }
        userGameListDao.batchInsert(insertList);
    }

    @Override
    public void delete(long userId, long gameId) {
        UserGameList.Id id = new UserGameList.Id(userId,gameId);
        userGameListDao.delete(id);
    }

    @Override
    public Page<GameInfoDto> getPlayGamePage(SystemParam param, long userId) {
        return userGameListDao.getPlayGamePage(userId);
    }
}
