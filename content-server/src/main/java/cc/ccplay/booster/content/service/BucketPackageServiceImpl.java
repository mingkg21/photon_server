package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.BucketPackageService;
import cc.ccplay.booster.base.api.user.SysUserService;
import cc.ccplay.booster.base.model.content.BucketPackage;
import cc.ccplay.booster.base.model.user.SysUser;
import cc.ccplay.booster.content.dao.BucketPackageDao;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.core.SystemParam;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.jdbc.model.User;
import org.springframework.beans.factory.annotation.Autowired;

@Service(interfaceClass=BucketPackageService.class)
@org.springframework.stereotype.Service
public class BucketPackageServiceImpl implements BucketPackageService{

    @Autowired
    private BucketPackageDao bucketPackageDao;

    @Reference
    private SysUserService sysUserService;

    @Override
    public BucketPackage save(SystemParam param, BucketPackage bucketPackage) {
        User loginUser = param.getUser();
        if(loginUser != null){
            bucketPackage.setCreateUserId(loginUser.getSysUserId());
            bucketPackage.setAccount(loginUser.getLoginAccount());
        }else{
            Long userId = bucketPackage.getCreateUserId();
            if(userId != null){
                SysUser sysUser = sysUserService.getUser(param,userId);
                if(sysUser != null){
                    bucketPackage.setAccount(sysUser.getAccount());
                }
            }
        }
        return bucketPackageDao.save(bucketPackage);
    }

    @Override
    public Page queryPage(SystemParam param, BucketPackage searchObj) {
        return this.bucketPackageDao.getPage(searchObj);
    }

    @Override
    public void update(BucketPackage bucketPackage) {
        bucketPackageDao.updateNotNull(bucketPackage);
    }

    @Override
    public BucketPackage get(long id) {
        return bucketPackageDao.get(id);
    }

    @Override
    public void delete(long id) {
        bucketPackageDao.delete(id);
    }
}
