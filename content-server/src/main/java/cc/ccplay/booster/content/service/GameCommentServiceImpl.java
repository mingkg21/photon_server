package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.GameCommentService;
import cc.ccplay.booster.base.api.user.UserAccountService;
import cc.ccplay.booster.base.api.user.UserSystemMessageService;
import cc.ccplay.booster.base.core.AppHeaderInfo;
import cc.ccplay.booster.base.dto.content.game.GameCommentDto;
import cc.ccplay.booster.base.dto.content.game.GameCommentRecordDto;
import cc.ccplay.booster.base.enums.MsgTagType;
import cc.ccplay.booster.base.model.content.*;
import cc.ccplay.booster.base.model.user.UserSystemMessage;
import cc.ccplay.booster.base.reader.dto.UserInfo;
import cc.ccplay.booster.base.util.AppVersionUtil;
import cc.ccplay.booster.content.dao.*;
import cc.ccplay.booster.content.dao.*;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.enums.MsgContentType;
import cc.ccplay.booster.base.model.content.*;
import cc.ccplay.booster.content.dao.*;
import org.apache.log4j.Logger;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.easyj.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service(interfaceClass = GameCommentService.class)
@org.springframework.stereotype.Service
public class GameCommentServiceImpl extends BaseContentService implements GameCommentService{

    private static final Logger log = Logger.getLogger(GameCommentServiceImpl.class);
    @Autowired
    private GameCommentDao gameCommentDao;

    @Autowired
    private GameCommentStarDao gameCommentStarDao;

    @Autowired
    private GameCommentStarRangeDao gameCommentStarRangeDao;

    @Autowired
    private GameCommentPraiseDao gameCommentPraiseDao;

    @Autowired
    private GameInfoDao gameInfoDao;

    @Autowired
    private GameVersionDao gameVersionDao;

    @Reference
    private UserSystemMessageService userSystemMessageService;

    @Reference
    private UserAccountService userAccountService;


    @Override
    public GameComment comment(GameComment comment, long star) {
        Long gameId = comment.getGameId();
        Long userId = comment.getUserId();
        GameVersion versionInfo = gameVersionDao.getLastestVersion(gameId);
        if(versionInfo == null){
            throw new GenericException("应用版本信息不存在");
        }
        comment.setVersionName(versionInfo.getVersionName());
        gameCommentDao.save(comment);
        gameInfoDao.addCommentCount(gameId);
        GameCommentStar commentStar = gameCommentStarDao.get(gameId,userId);
        if(commentStar != null){//之前有过评星记录
            long oldStar = commentStar.getStar();
            if(oldStar == star){
                return comment;
            }
            commentStar.setStar(star);
            //更新分数
            if(gameCommentStarDao.updateScore(commentStar) == 0){
                throw new GenericException("操作过于频繁");
            }
            //更新分数变动
            gameInfoDao.updateTotalScore(gameId,star - oldStar,0);
            //更新游戏星级汇总表
            gameCommentStarRangeDao.updateScoreCount(gameId,star,oldStar);
        } else {
            commentStar = new GameCommentStar();
            commentStar.setStar(star);
            commentStar.setId(new GameCommentStar.Id(gameId,userId));
            gameCommentStarDao.save(commentStar);
            gameInfoDao.updateTotalScore(gameId,star ,1);
            if(gameCommentStarRangeDao.get(gameId) == null){
                GameCommentStarRange starRange = new GameCommentStarRange();
                starRange.setGameId(gameId);
                if(star == 2){
                    starRange.setStar1(1L);
                }else if(star == 4){
                    starRange.setStar2(1L);
                }else if(star == 6){
                    starRange.setStar3(1L);
                }else if(star == 8){
                    starRange.setStar4(1L);
                }else if(star == 10){
                    starRange.setStar5(1L);
                }
                gameCommentStarRangeDao.save(starRange);
            }else{
                gameCommentStarRangeDao.incrScoreCount(gameId,star);
            }
        }
        return comment;
    }

    @Override
    public GameComment reply(long commentId, GameComment reply) {

        //官方回复
        boolean officialReply = StringUtil.equals(reply.getCommentType(),GameComment.COMMENT_TYPE_OFFICIAL);

        GameComment gameComment = gameCommentDao.get(commentId);
        if(gameComment == null){
            throw new GenericException("回复的对象不存在");
        }

        Long gameId = gameComment.getGameId();

        GameInfo gameInfo = gameInfoDao.get(gameId);
        if(gameInfo == null){
            throw new GenericException("应用不存在");
        }

        GameVersion versionInfo = gameVersionDao.getLastestVersion(gameId);
        if(versionInfo == null){
            throw new GenericException("应用版本信息不存在");
        }
        Long currentUserId = reply.getUserId();
        Long parentId = gameComment.getParentId();

        reply.setGameId(gameId);
        reply.setVersionName(versionInfo.getVersionName());

        Long userId = gameComment.getUserId();
        boolean isSameUser = StringUtil.equals(userId,currentUserId);
        if(parentId != null){//对回复进行回复
            reply.setParentId(parentId);
            if(isSameUser == false){
                reply.setBeRepliedUserId(userId);
            }
        }else{//对评价进行回复
            reply.setParentId(commentId);
        }
        //评论 回复+1
        gameCommentDao.addReplyCount(commentId);
        GameComment comment = gameCommentDao.save(reply);

        String content = reply.getContent();
        if(!isSameUser) {
            try {
                UserSystemMessage msg = new UserSystemMessage();
                MsgContentType contentType = MsgContentType.GAME_COMMENT_BEREPLIED;
                msg.setMsgType(UserSystemMessage.MSG_TYPE_GENERAL);
                msg.setUserId(gameComment.getUserId());
                msg.setObjectId(comment.getId());
                msg.setObjectTitle("回复ID");
                msg.setMsgContent(content);
                msg.setContentType(new Long(contentType.getValue()));


                String nickName = null;
                if(officialReply){
                    nickName = "官方";
                }else{
                    UserInfo userInfo = userAccountService.getUserInfo(currentUserId);
                    nickName = userInfo.getNickName();
                }
                String title = contentType.getTitle(nickName,gameInfo.getName());
                msg.setMsgTitle(title);
                JSONObject json = new JSONObject();
                json.put("msgTitle",title);
                json.put("objectTitle","回复ID");
                json.put("objectId",msg.getObjectId());
                json.put("contentType",contentType.getValue());
                json.put("userId",msg.getUserId());
                json.put("msgType",msg.getMsgType());
                json.put("msgContent",msg.getMsgContent());
                JSONArray contentArr = new JSONArray();
                if(officialReply){
                    contentArr.add(MixUtil.newHashMap("type", MsgTagType.TEXT,"value",nickName,"bold",1));
                }else{
                    contentArr.add(MixUtil.newHashMap("id",currentUserId,"type", MsgTagType.USER_INFO,"value",nickName,"bold",1));
                }
                contentArr.add(MixUtil.newHashMap("type", MsgTagType.TEXT,"value","回复你在"));
                contentArr.add(MixUtil.newHashMap("id",gameId,"type", MsgTagType.GAME_INFO,"value","《"+gameInfo.getName()+"》","color",0xff999999));
                contentArr.add(MixUtil.newHashMap("type", MsgTagType.TEXT,"value","的游戏评论."));
                contentArr.add(MixUtil.newHashMap("type", MsgTagType.TEXT,"value","\r\n评论内容: ","bold",1));
                contentArr.add(MixUtil.newHashMap("type", MsgTagType.TEXT,"value",content,"color",0xff999999));
                //+content
                json.put("content",contentArr);

                //                {"msgTitle":"张三回复你在《王者荣光耀》的游戏评论",
//                        "objectId":1,"objectTitle":"回复ID",
//                        "msgContent":"你说的很对",
//                        "msgType":1,
//                        "content":[{"value":"系统消息：","type":1,"bold":1},
//                    {"value":"有人回复了你的消息","type":1}],
//                    "title":[{"id":66679,"value":"张三","type":4},
//                    {"value":"回复你在","type":1},{"id":1,"value":"《王者荣耀》","type":3},
//                    {"value":"的游戏评论","type":1}]}
                JSONArray titleArr = new JSONArray();
                titleArr.add(MixUtil.newHashMap("type", MsgTagType.TEXT,"value","有人回复了你的消息"));
                json.put("title",titleArr);

                msg.setPushMsg(json.toJSONString());
                userSystemMessageService.pushMsg2One(msg);
            } catch (Exception e) {
                log.error("信息推送失败",e);
                e.printStackTrace();
            }
        }
        return comment;
    }



    @Override
    public Page<GameCommentDto> getCommentPage(SystemParam param, GameComment searchObject, String gameName, Long orderType) {
        Page<GameCommentDto> page = gameCommentDao.getCommentPage(searchObject,gameName,orderType);
        super.readManyUserInfo(page.getList());
        return page;
    }

    @Override
    public Page<GameCommentDto> getReplyPage(SystemParam param, long commentId,Long orderType) {
        AppHeaderInfo headerInfo = param.getAppHeaderInfo();
        boolean showOfficaReply = AppVersionUtil.greater(headerInfo,AppVersionUtil.V1_1);
        Page<GameComment> page = gameCommentDao.getReplyPage(commentId,orderType,showOfficaReply);
        return toDtoPage(page);
    }

    private Page<GameCommentDto> toDtoPage(Page<GameComment> page){
        List<GameComment> list = page.getList();
        int size = list.size();
        List<GameCommentDto> result = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            GameComment gameComment = list.get(i);
            GameCommentDto dto = new GameCommentDto();
            dto.setComment(gameComment);
            result.add(dto);
        }
        super.readManyUserInfo(result);
        return new Page<>(page.getStart(),page.getTotalCount(),page.getPageSize(),result);
    }

    @Override
    public void praiseComment(SystemParam param, long commentId) {
        long userId = param.getUserId();
        GameCommentPraise.Id id = new GameCommentPraise.Id(commentId,userId);
        if(gameCommentPraiseDao.get(id) != null){
            return;
        }
        GameCommentPraise praise = new GameCommentPraise();
        praise.setId(id);
        gameCommentPraiseDao.save(praise);
        gameCommentDao.addPraiseCount(commentId,1);
    }

    @Override
    public void cancelPraiseComment(SystemParam param, long commentId) {
        long userId = param.getUserId();
        GameCommentPraise.Id id = new GameCommentPraise.Id(commentId,userId);
        if(gameCommentPraiseDao.get(id) == null){
            return;
        }
        if(gameCommentPraiseDao.delete(id) > 0){
            gameCommentDao.addPraiseCount(commentId,-1);
        }
    }

    @Override
    public GameCommentRecordDto getCommentRecord(SystemParam param, long gameId) {
        GameInfo gameInfo = gameInfoDao.get(gameId);
        if(gameInfo == null){
            throw new GenericException("游戏不存在");
        }
        GameCommentRecordDto dto = new GameCommentRecordDto();
        GameCommentStar gameCommentStar = gameCommentStarDao.get(gameId,param.getUserId());
        if(gameCommentStar == null){
            dto.setStar(0L);
        }else{
            dto.setStar(gameCommentStar.getStar());
        }
        List<GameComment> record = gameCommentDao.getTop3CommentRecord(gameId,param.getUserId());
        dto.setComments(record);
        return dto;
    }

    @Override
    public GameCommentStarRange getGameCommentStarRange(SystemParam param, long gameId) {
        GameCommentStarRange range = this.gameCommentStarRangeDao.get(gameId);
        if(range == null){
            range = new GameCommentStarRange();
            range.setGameId(gameId);
            range.setStar1(0L);
            range.setStar2(0L);
            range.setStar3(0L);
            range.setStar4(0L);
            range.setStar5(0L);
        }
        return range;
    }

    @Override
    public GameCommentDto getComment(SystemParam param, long commentId) {
        GameCommentDto dto = new GameCommentDto();
        GameComment gameComment = gameCommentDao.get(commentId);
        if(gameComment == null){
            throw new GenericException("数据不存在");
        }
        Long userId = gameComment.getUserId();
        if(gameComment.getParentId() == null){//评论有星级
           GameCommentStar star = gameCommentStarDao.get(gameComment.getGameId(),userId);
           if(star != null){
               dto.setStar(star.getStar());
           }
        }
        dto.setComment(gameComment);
        super.readManyUserInfo(dto);
        return dto;
    }


    @Override
    public void updateNotNull(GameComment gameComment) {
        gameCommentDao.updateNotNull(gameComment);
    }

    @Override
    public void delete(long id) {
        gameCommentDao.logicDelete(id);
    }

    @Override
    public int batchDelete(String ids) {
        return gameCommentDao.deletes(ids);
    }

    @Override
    public void saveOfficialReply(long id, String content,JSONArray imgs) {
        GameComment gameComment = new GameComment();
        gameComment.setId(id);
        gameComment.setOfficialContent(content);
        gameComment.setOfficialTime(new Date());
        gameComment.setOfficialImgs(imgs);
        if(gameCommentDao.updateNotNull(gameComment) > 0){
            GameComment reply = new GameComment();
            reply.setContent(content);
            reply.setImgs(imgs);
            reply.setCommentType(GameComment.COMMENT_TYPE_OFFICIAL);
            reply(id,reply);
        }
    }

    /**
     * 获取近期置顶的游戏评论
     *
     * @param size
     * @return
     */
    @Override
    public List<GameCommentDto> getHotGameComment(int size) {
        List<GameCommentDto> list = this.gameCommentDao.getHotGameComment(size);
        super.readManyUserInfo(list);
        return list;
    }
}
