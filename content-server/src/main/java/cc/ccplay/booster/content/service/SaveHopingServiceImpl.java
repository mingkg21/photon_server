package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.SaveHopingService;
import cc.ccplay.booster.base.model.content.SaveHopingRecord;
import cc.ccplay.booster.content.dao.GameInfoDao;
import cc.ccplay.booster.content.dao.SaveHopingCountDao;
import cc.ccplay.booster.content.dao.SaveHopingRecordDao;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.content.SaveHopingCount;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;

@Service(interfaceClass=SaveHopingService.class)
@org.springframework.stereotype.Service
public class SaveHopingServiceImpl extends BaseContentService implements SaveHopingService {

    @Autowired
    private SaveHopingCountDao saveHopingCountDao;

    @Autowired
    private SaveHopingRecordDao saveHopingRecordDao;

    @Autowired
    private GameInfoDao gameInfoDao;

    @Override
    public Page getPage(SystemParam param) {
        return saveHopingCountDao.getPage();
    }

    @Override
    public long hoping(long gameId, long userId) {

        if(gameInfoDao.get(gameId) == null){
            throw new GenericException("游戏不存在");
        }

        SaveHopingRecord.Id id = new SaveHopingRecord.Id(gameId,userId);
        if(saveHopingRecordDao.get(id) != null){
            throw new GenericException("每个游戏只能助力一次");
        }

        SaveHopingRecord record = new SaveHopingRecord();
        record.setId(id);
        saveHopingRecordDao.save(record);

        if(saveHopingCountDao.get(gameId) == null){
            SaveHopingCount hopingCount = new SaveHopingCount();
            hopingCount.setGameId(gameId);
            hopingCount.setPersonCount(1L);
            saveHopingCountDao.save(hopingCount);
            return 1;
        }else{
            saveHopingCountDao.addPersonCount(gameId,1);
            SaveHopingCount lastest = saveHopingCountDao.get(gameId);
            if(lastest == null){
                return 0;
            }
            return lastest.getPersonCount();
        }
    }
}
