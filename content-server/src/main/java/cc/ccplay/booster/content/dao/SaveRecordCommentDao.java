package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.dto.content.save.SaveRecordCommentDto;
import cc.ccplay.booster.base.model.content.SaveRecordComment;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class SaveRecordCommentDao extends BaseDao<SaveRecordComment> {

    public void addReplyCount(long commentId){
        String sql = "update save_record_comment set reply_count = reply_count + 1 where id = :commentId ";
        super.update(sql, MixUtil.newHashMap("commentId",commentId));
    }

    public Page<SaveRecordCommentDto> getCommentPage(long recordId, Long orderType){
        Map paramMap = new HashMap();
        StringBuffer sql = new StringBuffer();
        sql.append(" select  ");
        sql.append(super.getSelectColStr(SaveRecordComment.class,"a","comment")+" ");
        sql.append(" from save_record_comment  a ");
        sql.append(" where a.delete_flag = 0 ");
        sql.append(" and a.record_id = :recordId and a.parent_id is null ");
        paramMap.put("recordId",recordId);
        if(orderType == null || orderType.longValue() == 0) {
            sql.append(" order by a.id desc ");
        }else if(orderType.longValue() == 1) {
            sql.append(" order by a.praise_count desc ");
        }
        return super.paged2Obj(sql.toString(),paramMap,SaveRecordCommentDto.class);
    }

    public Page<SaveRecordComment> getReplyPage(Long parentId,Long orderType){
        Map paramMap = new HashMap();
        StringBuffer sql = new StringBuffer();
        sql.append(" select * from save_record_comment ");
        sql.append(" where delete_flag = 0 ");

        sql.append(" and parent_id = :parentId ");
        paramMap.put("parentId",parentId);
        if(orderType == null || orderType.longValue() == 0) {
            sql.append(" order by id desc ");
        }else if(orderType.longValue() == 1) {
            sql.append(" order by praise_count desc ");
        }
        return super.paged2Obj(sql.toString(),paramMap);
    }

}
