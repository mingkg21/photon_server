package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.GameRecruitFeedbackService;
import cc.ccplay.booster.base.dto.content.game.GameRecruitFeedbackDto;
import cc.ccplay.booster.base.model.content.GameRecruitFeedback;
import cc.ccplay.booster.content.dao.GameRecruitFeedbackDao;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.core.SystemParam;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;

@Service(interfaceClass = GameRecruitFeedbackService.class)
@org.springframework.stereotype.Service
public class GameRecruitFeedbackServiceImpl extends BaseContentService implements GameRecruitFeedbackService{

    @Autowired
    private GameRecruitFeedbackDao gameRecruitFeedbackDao;

    @Override
    public GameRecruitFeedbackDto getMyFeedback(SystemParam param, long recruitId) {
        Long userId = param.getUserId();
        if(userId == null){
            return null;
        }
        GameRecruitFeedback feedback = gameRecruitFeedbackDao.getFeedback(userId,recruitId);
        if(feedback == null){
            return null;
        }
        GameRecruitFeedbackDto dto = new GameRecruitFeedbackDto();
        dto.setFeedback(feedback);
        super.readUserInfo(dto);
        return dto;
    }

    @Override
    public Page<GameRecruitFeedbackDto> getDtoPage(SystemParam param, long recruitId) {
        Page<GameRecruitFeedbackDto> page = gameRecruitFeedbackDao.getDtoPage(recruitId);
        super.readUserInfo(page);
        return page;
    }

    @Override
    public void feedback(SystemParam param,long recruitId, String title, String content) {
        GameRecruitFeedback feedback = gameRecruitFeedbackDao.getFeedback(param.getUserId(),recruitId);
        if(feedback != null){
            throw new GenericException("您已经反馈过，不能多次反馈");
        }
        feedback = new GameRecruitFeedback();
        feedback.setContent(content);
        feedback.setTitle(title);
        feedback.setRecruitId(recruitId);
        feedback.setUserId(param.getUserId());
        feedback.setStatus(GameRecruitFeedback.STATUS_UNCHECKED);
        gameRecruitFeedbackDao.save(feedback);
    }
}
