package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.constants.Constant;
import cc.ccplay.booster.base.dto.content.game.GameInfoDto;
import cc.ccplay.booster.base.enums.DeleteFlag;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.*;
import cc.ccplay.booster.base.model.content.*;
import cc.ccplay.booster.base.reader.dto.BaseGameDto;
import cc.ccplay.booster.content.enums.AreaCode;
import org.apache.commons.lang3.time.DateUtils;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.jdbc.support.Aggregation;
import org.easyj.frame.jdbc.support.AggregationFunction;
import org.easyj.frame.util.MixUtil;
import org.easyj.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class GameInfoDao extends BaseDao<GameInfo>{

    public Page getPage(GameInfo searchInfo,Long tagId,Long versionType,Boolean newVersion){
        Map paramMap = new HashMap();
        StringBuffer sb = new StringBuffer(" select a.*,b.icon,b.file_size,b.lang," +
                " b.version_name,b.version_code,c.name as category_name,d.version_code as new_version_code, " );
        sb.append(" d.version_name as new_version_name ");
        if(tagId != null) {
            sb.append(",e.ordering ");
        }
        sb.append(" from game_info a  ");
        if(tagId != null) {
            sb.append(" inner join game_tag_item e on(e.tag_id = :tagId and e.game_id = a.id ) ");
            paramMap.put("tagId",tagId);
        }
        sb.append(" left join game_version b on(a.lastest_version_id = b.id) ");
        sb.append(" left join game_category c on(a.category_id = c.id) ");
        sb.append(" left join crawl_game d on(d.game_id = a.id) ");
        sb.append(" where a.delete_flag = " + DeleteFlag.NO_DELETE +" ");
        if(searchInfo != null){
            Long id = searchInfo.getId();
            if(id != null){
                sb.append(" and a.id = :id ");
                paramMap.put("id",id);
            }

            String name = searchInfo.getName();
            if(StringUtil.isNotEmpty(name)){
                sb.append(" and a.name like :name ");
                paramMap.put("name","%"+name+"%");
            }
            String packageName = searchInfo.getPackageName();
            if(StringUtil.isNotEmpty(packageName)){
                sb.append(" and a.package_name = :packageName ");
                paramMap.put("packageName",packageName);
            }
            Long status = searchInfo.getStatus();
            if(status != null){
                sb.append(" and a.status = :status ");
                paramMap.put("status",status);
            }
        }

        if(newVersion != null && newVersion){//新版本游戏更新
           //and (d.version_code > )
            sb.append(" and d.version_name is not null ");
            sb.append(" and ( ");
            sb.append(" b.version_type =  " + GameVersion.VERSION_TYPE_TEST+" ");
            sb.append(" or (d.version_code is not null and d.version_code > b.version_code ) ");
            sb.append(" or d.version_name > b.version_name  ");
            sb.append(" ) ");
        }

        if(versionType != null){
            sb.append(" and b.version_type = :versionType ");
            paramMap.put("versionType",versionType);
        }

        if(tagId != null) {
            sb.append(" order by e.ordering,a.id ");
        }else{
            sb.append(" order by a.id desc ");
        }
        return super.page2CamelMap(sb.toString(),paramMap);
    }

    public Page getPage4Selectize(String name){
        Map paramMap = new HashMap();
        StringBuffer sql = new StringBuffer("select id,name from game_info where delete_flag = 0 " +
                " and status =  "+Status.ENABLED.getValue() +" ");
        if(StringUtil.isNotEmpty(name)){
            sql.append(" and name like :name  ");
            paramMap.put("name","%"+name+"%");
        }
        sql.append(" order by id desc ");
        return super.page2CamelMap(sql.toString(), MixUtil.newHashMap("name",paramMap));
    }

    public GameInfoDto getGameInfoDto(long id){
        StringBuffer sql = new StringBuffer();
        sql.append("select  ");
        sql.append(super.getSelectColStr("a","gameInfo")+",");
        sql.append(super.getSelectColStr(GameVersion.class,"b","versionInfo")+",");
        sql.append(super.getSelectColStr(GameCategory.class,"c","category")+",");
        sql.append(super.getSelectColStr(GamePublisher.class,"d","publisher"));
        sql.append(", (CASE WHEN e.game_id is null THEN 0 ELSE 1 END) as has_save ");
        sql.append(", (CASE WHEN f.person_count is null THEN 0 ELSE f.person_count END) as hoping_count ");
        sql.append(" FROM game_info a ");
        sql.append(" LEFT JOIN game_version b ");
        sql.append(" ON(a.lastest_version_id = b.id ) ");
        sql.append(" LEFT JOIN game_category c ");
        sql.append(" ON(c.id = a.category_id ) ");
        sql.append(" LEFT JOIN game_publisher d ");
        sql.append(" ON(d.id = a.publisher_id ) ");
        sql.append(" LEFT JOIN save_game e ");
        sql.append(" ON(a.id = e.game_id) ");
        sql.append(" LEFT JOIN save_hoping_count f ");
        sql.append(" ON(f.game_id = a.id )");
        sql.append(" where a.delete_flag = 0 and a.status = " + Status.ENABLED.getValue() + " and a.id = :id ");
        return super.query21Model(sql.toString(),MixUtil.newHashMap("id",id),GameInfoDto.class);
    }



    public Page<GameInfoDto> searchGames(Long mainTagId,Long childTagId, Long categoryId,String name,String orderType){
        StringBuffer sql = new StringBuffer();
        Map paramMap = new HashMap();
        sql.append("select  ");
        sql.append(super.getSelectColStr("a","gameInfo")+",");
        sql.append(super.getSelectColStr(GameVersion.class,"b","versionInfo")+",");
        sql.append(super.getSelectColStr(GameCategory.class,"c","category")+",");
            sql.append(super.getSelectColStr(GamePublisher.class,"d","publisher"));
        sql.append(" FROM game_info a ");
        sql.append(" LEFT JOIN game_version b ");
        sql.append(" ON(a.lastest_version_id = b.id ) ");
        sql.append(" LEFT JOIN game_category c ");
        sql.append(" ON(c.id = a.category_id ) ");
        sql.append(" LEFT JOIN game_publisher d ");
        sql.append(" ON(d.id = a.publisher_id ) ");
        if(mainTagId != null){
            paramMap.put("tagId",mainTagId);
            sql.append(" LEFT JOIN game_tag_item e ");
            sql.append(" on(a.id = e.game_id and e.tag_id = :tagId) ");
        }
        if(childTagId != null){
            paramMap.put("childTagId",childTagId);
            sql.append(" LEFT JOIN game_tag_item f ");
            sql.append(" on(a.id = f.game_id and f.tag_id = :childTagId) ");
        }

        sql.append(" where a.delete_flag = 0 ");

        if(categoryId != null){
            sql.append(" and a.category_id = :categoryId ");
            paramMap.put("categoryId",categoryId);
        }

        sql.append(" and a.status = " + Status.ENABLED.getValue() +" ");
        if(mainTagId != null){
            sql.append(" and e.tag_id is not null ");
        }

        if(childTagId != null){
            sql.append(" and f.tag_id is not null ");
        }

        if(StringUtil.isNotEmpty(name)){
            paramMap.put("name","%"+name+"%");
            sql.append(" and a.name like :name ");
        }

        if("releaseTime".equals(orderType)){
            sql.append(" order by b.create_time desc ");
        }else if(childTagId != null){
            sql.append(" order by f.ordering,a.id ");
        }else if(mainTagId != null){
            sql.append(" order by e.ordering,a.id ");
        }else{
            sql.append(" order by a.download_count desc,a.create_time desc");
        }
        return super.paged2Obj(sql.toString(),paramMap,GameInfoDto.class);
    }


    public int updateTotalScore(long gameId ,long incrScore,long incrScoreCount){
        StringBuffer sql = new StringBuffer();
        sql.append(" UPDATE game_info set  ");
        sql.append(" score = (total_score::numeric + :incrScore) / (score_count::numeric + :incrScoreCount),");
        sql.append(" total_score = total_score + :incrScore, ");
        sql.append(" score_count = score_count + :incrScoreCount ");
        sql.append(" WHERE id = :id ");
        return super.update(sql.toString(),MixUtil.newHashMap("id",gameId,"incrScore",incrScore,"incrScoreCount",incrScoreCount));
    }

    public void addCommentCount(long gameId){
        String sql = " UPDATE game_info set comment_count = comment_count + 1 where id = :id ";
        super.update(sql,MixUtil.newHashMap("id",gameId));
    }

    public int addFollowCount(long gameId,int value){
        String sql = "UPDATE game_info set follow_count = follow_count + :value where id = :gameId ";
        return super.update(sql,MixUtil.newHashMap("gameId",gameId,"value",value));
    }

    public int addPraiseCount(long gameId,int value){
        String sql = "UPDATE game_info set praise_count = praise_count + :value where id = :gameId ";
        return super.update(sql,MixUtil.newHashMap("gameId",gameId,"value",value));
    }

    public int addTestCount(long gameId,int value){
        String sql = "UPDATE game_info set test_count = test_count + :value where id = :gameId ";
        return super.update(sql,MixUtil.newHashMap("gameId",gameId,"value",value));
    }


//    public Page<GameInfoDto> getUserFollowGames(long userId){
//        StringBuffer sql = new StringBuffer("select ");
//        sql.append(super.getSelectColStr("a","gameInfo")+",");
//        sql.append(super.getSelectColStr(GameVersion.class,"b","versionInfo")+",");
//        sql.append(super.getSelectColStr(GameCategory.class,"c","category")+",");
//        sql.append(super.getSelectColStr(GamePublisher.class,"d","publisher"));
//        sql.append(" FROM game_user_follow f ");
//        sql.append(" LEFT JOIN game_info a ");
//        sql.append(" ON(f.game_id = a.id ) ");
//        sql.append(" LEFT JOIN game_version b ");
//        sql.append(" ON(a.lastest_version_id = b.id ) ");
//        sql.append(" LEFT JOIN game_category c ");
//        sql.append(" ON(c.id = a.category_id ) ");
//        sql.append(" LEFT JOIN game_publisher d ");
//        sql.append(" ON(d.id = a.publisher_id ) ");
//        sql.append(" where a.delete_flag = 0 and a.status = " + Status.ENABLED.getValue() + " and f.user_id = :userId ");
//        return super.paged2Obj(sql.toString(),MixUtil.newHashMap("userId",userId),GameInfoDto.class);
//    }

    public Page<GameInfoDto> getSameTypeGame(long gameId,Long categoryId){
        StringBuffer sql = new StringBuffer();
        sql.append("select  ");
        sql.append(super.getSelectColStr("a","gameInfo")+",");
        sql.append(super.getSelectColStr(GameVersion.class,"b","versionInfo")+",");
        sql.append(super.getSelectColStr(GameCategory.class,"c","category")+",");
        sql.append(super.getSelectColStr(GamePublisher.class,"d","publisher"));
        sql.append(" FROM game_info a ");
        sql.append(" LEFT JOIN game_version b ");
        sql.append(" ON(a.lastest_version_id = b.id ) ");
        sql.append(" LEFT JOIN game_category c ");
        sql.append(" ON(c.id = a.category_id ) ");
        sql.append(" LEFT JOIN game_publisher d ");
        sql.append(" ON(d.id = a.publisher_id ) ");
        sql.append(" where a.delete_flag = 0 and a.status = " + Status.ENABLED.getValue() + " and a.id <> :gameId  and a.category_id = :categoryId ");
        sql.append(" order by a.download_count desc,a.create_time desc ");
        return super.paged2Obj(sql.toString(),MixUtil.newHashMap("gameId",gameId,"categoryId",categoryId),GameInfoDto.class);
    }

    public void addDownloadCount(long id){
        String sql = "update game_info set download_count = download_count + 1 where id = :id ";
        super.update(sql,MixUtil.newHashMap("id",id));
    }


    public Page<GameInfoDto> getPageByAreaId(CustomArea area,boolean lastCreateTime,String gameName){
        Long areaId = area.getId();
        StringBuffer sql = new StringBuffer();
        Map paramMap = new HashMap();
        paramMap.put("areaId",areaId);
        sql.append("select  ");
        sql.append(" case when c.game_id is not null then 1 else 0 end as booster_status ,");
        sql.append(super.getSelectColStr("a","gameInfo")+",");
        sql.append(super.getSelectColStr(GameVersion.class,"b","versionInfo")+",");
        sql.append(super.getSelectColStr(CustomAreaItem.class,"e","areaItem"));
        sql.append(" FROM custom_area_item e ");
        sql.append(" LEFT JOIN game_info a ");
        sql.append(" ON(e.game_id = a.id ) ");
        sql.append(" LEFT JOIN game_version b ");
        sql.append(" ON(a.lastest_version_id = b.id ) ");

        sql.append(" LEFT JOIN ( select DISTINCT game_id from booster_server_group_game ) as c ");
        sql.append(" on(c.game_id = e.game_id ) ");
        sql.append(" where a.delete_flag = 0 ");

        if(AreaCode.TEST_GAME.equals(area.getCode())  == false) { //测试专区显示所有游戏
            sql.append(" and a.status = " + Status.ENABLED.getValue() + " ");
        }
        sql.append(" and e.area_id = :areaId ");

        if(StringUtil.isNotEmpty(gameName)){
            sql.append(" and a.name like :gameName ");
            paramMap.put("gameName","%"+gameName+"%");
        }

        sql.append(" order by e.ordering,e.id");
        if(lastCreateTime) {
            List<Aggregation> as = new ArrayList<>();
            Aggregation aggregation = new Aggregation(AggregationFunction.MAX, "e.create_time", "lastCreateTime");
            as.add(aggregation);
            return super.paged2Obj(sql.toString(), paramMap,as,GameInfoDto.class);
        }
        return super.paged2Obj(sql.toString(), paramMap, GameInfoDto.class);
    }


    /**
     * 获取新游
     * @return
     */
    public Page<GameInfoDto> getNewGames(){
        StringBuffer sql = new StringBuffer();
        //Map paramMap = new HashMap();
        sql.append("select  ");
        sql.append(super.getSelectColStr("a","gameInfo")+",");
        sql.append(super.getSelectColStr(GameVersion.class,"b","versionInfo")+",");
        sql.append(super.getSelectColStr(GameCategory.class,"c","category")+",");
        sql.append(super.getSelectColStr(GamePublisher.class,"d","publisher"));
        sql.append(" FROM game_info a ");
        sql.append(" LEFT JOIN game_version b ");
        sql.append(" ON(a.lastest_version_id = b.id ) ");
        sql.append(" LEFT JOIN game_category c ");
        sql.append(" ON(c.id = a.category_id ) ");
        sql.append(" LEFT JOIN game_publisher d ");
        sql.append(" ON(d.id = a.publisher_id ) ");
        sql.append(" where a.delete_flag = 0 ");
        sql.append(" and a.status = " + Status.ENABLED.getValue() +" ");
        sql.append(" and b.id is not null ");
        sql.append(" order by b.create_time desc ");
        Aggregation aggregation = new Aggregation(AggregationFunction.MAX,"b.create_time","lastupdateTime");
        List<Aggregation> as = new ArrayList<>();
        as.add(aggregation);
        return super.paged2Obj(sql.toString(),as,GameInfoDto.class);
    }

    public List<GameInfo> getGameIdByPackageName(List<String> packageNames){
        StringBuffer sql = new StringBuffer();
        sql.append("select id,package_name from game_info a  ");
        int size = packageNames.size();
        sql.append(" where ");
        for (int i = 0; i < size; i++) {
            if(i != 0){
                sql.append(" OR ");
            }
            sql.append(" a.package_name = '"+packageNames.get(i)+"'");
        }
        return super.query2Model(sql.toString(),GameInfo.class);
    }


    public List<GameInfoDto> getLastestVersionInfoByPackageName(List<String> packageNames){
        StringBuffer sql = new StringBuffer();
        sql.append("select  ");
        sql.append(super.getSelectColStr("a","gameInfo")+",");
        sql.append(super.getSelectColStr(GameVersion.class,"b","versionInfo")+",");
        sql.append(super.getSelectColStr(GameCategory.class,"c","category")+",");
        sql.append(super.getSelectColStr(GamePublisher.class,"d","publisher"));
        sql.append(" FROM game_info a ");
        sql.append(" LEFT JOIN game_version b ");
        sql.append(" ON(a.lastest_version_id = b.id ) ");
        sql.append(" LEFT JOIN game_category c ");
        sql.append(" ON(c.id = a.category_id ) ");
        sql.append(" LEFT JOIN game_publisher d ");
        sql.append(" ON(d.id = a.publisher_id ) ");
        sql.append(" where a.delete_flag = 0 and a.status = " + Status.ENABLED.getValue() + " ");
        int size = packageNames.size();
        sql.append(" and (");
        for (int i = 0; i < size; i++) {
            if(i != 0){
                sql.append(" OR ");
            }
            sql.append(" a.package_name = '"+packageNames.get(i)+"'");
        }
        sql.append(")");
        return super.query2Model(sql.toString(),GameInfoDto.class);
    }

    public List<GameInfo> getDownloadRankingList(boolean newGame,int size){
        StringBuffer sql = new StringBuffer(" select id from game_info ");
        sql.append(" where delete_flag = 0 and status = " + Status.ENABLED.getValue() + " ");
        Date now = new Date();
        if(newGame){
            sql.append(" and create_time > :startDate");
        }
        sql.append(" order by download_count desc limit :size ");
        return super.query2Model(sql.toString(),MixUtil.newHashMap("size",size,"startDate",DateUtils.addDays(now,-7)));
    }


    public List<GameInfo> getDownloadRankingListByTagId(long tagId,int size){
        StringBuffer sql = new StringBuffer("select b.id from game_tag_item a ");
        sql.append(" left join game_info b on(a.game_id = b.id) ");
        sql.append(" where a.tag_id = :tagId and b.status = "+Status.ENABLED.getValue() +" and b.delete_flag = 0  ");
        sql.append(" order by b.download_count desc limit :size ");
        return super.query2Model(sql.toString(),MixUtil.newHashMap("tagId",tagId,"size",size));
    }


    public List<GameInfoDto> getGameDtoByGameIds(Set<Long> ids){
        if(ids.size() == 0){
            return Collections.emptyList();
        }

        Map paramMap = new HashMap();

        StringBuffer sql = new StringBuffer();
        sql.append("select  ");

        sql.append(" case when c.game_id is not null then 1 else 0 end as booster_status ,");

        //sql.append("(  select a.server_id,min(a.ordering) as ordering from booster_server_group_ref a ");

        sql.append(super.getSelectColStr("a","gameInfo")+",");
        sql.append(super.getSelectColStr(GameVersion.class,"b","versionInfo")+" ");
        sql.append(" FROM game_info a ");
        sql.append(" LEFT JOIN game_version b ");
        sql.append(" ON(a.lastest_version_id = b.id ) ");

        sql.append(" LEFT JOIN ( select DISTINCT game_id from booster_server_group_game ) as c ");
        sql.append(" on(c.game_id = a.id ) ");

        sql.append(" where a.delete_flag = 0 and a.status = " + Status.ENABLED.getValue() + " ");
        sql.append(" and a.id in ( ");
        boolean first = true;

        for (Long id : ids) {
            if(id == null){
                continue;
            }
            if(first){
                first = false;
            }else{
                sql.append(",");
            }
            sql.append(id);
        }
        sql.append(" ) ");
        sql.append(" order by booster_status desc, a.id desc ");
        return super.query2Model(sql.toString(),paramMap,GameInfoDto.class);
    }


    public List<GameInfo> getNeedCompareUpdateGames(long startId,int size){
        String sql = "select id,package_name from game_info where status = "+ Constant.STATUS_ENABLED +" and id > :startId  order by id limit :size ";
        return super.query2Model(sql,MixUtil.newHashMap("startId",startId,"size",size));
    }


    public List<BaseGameDto> getGameInfos(Set<Long> gameIds) {
        StringBuffer sql = new StringBuffer(" select b.icon,a.name,a.id from game_info a ");
        sql.append(" left join game_version b ");
        sql.append(" ON(a.lastest_version_id = b.id ) ");

        sql.append(" where a.id in ( ");
        boolean first = true;
        for (Long gameId : gameIds) {
            if (first) {
                first = false;
            } else {
                sql.append(",");
            }
            sql.append(gameId);
        }
        sql.append(")");
        return super.query2Model(sql.toString(), BaseGameDto.class);
    }

    public List<GameInfo> getGameInfos(String packageName) {
        StringBuffer sql = new StringBuffer(" select name,id from game_info ");
        sql.append(" where package_name = :packageName");
        Map paramMap = new HashMap();
        paramMap.put("packageName",packageName);
        return super.query2Model(sql.toString(), paramMap);
    }

}
