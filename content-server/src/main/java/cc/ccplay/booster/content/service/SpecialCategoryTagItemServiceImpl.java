package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.SpecialCategoryTagItemService;
import cc.ccplay.booster.base.model.content.SpecialCategoryTagItem;
import cc.ccplay.booster.content.dao.GameSpecialCategoryDao;
import cc.ccplay.booster.content.dao.GameTagDao;
import cc.ccplay.booster.content.dao.SpecialCategoryTagItemDao;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.game.SpecialCategoryTagItemDto;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@com.alibaba.dubbo.config.annotation.Service(interfaceClass = SpecialCategoryTagItemService.class)
public class SpecialCategoryTagItemServiceImpl extends BaseContentService implements SpecialCategoryTagItemService{

    @Autowired
    private SpecialCategoryTagItemDao specialCategoryTagItemDao;

    @Autowired
    private GameSpecialCategoryDao gameSpecialCategoryDao;

    @Autowired
    private GameTagDao gameTagDao;

    @Override
    public SpecialCategoryTagItemDto getDto(long categroyId, long tagId) {
        SpecialCategoryTagItem.Id id = new SpecialCategoryTagItem.Id(categroyId,tagId);
        SpecialCategoryTagItem item = specialCategoryTagItemDao.get(id);
        if(item == null){
            return null;
        }
        SpecialCategoryTagItemDto dto = new SpecialCategoryTagItemDto();
        dto.setItem(item);
        dto.setGameTag(gameTagDao.get(tagId));
        return dto;
    }

    @Override
    public void saveOrUpdate(SpecialCategoryTagItem item) {
        if(specialCategoryTagItemDao.get(item.getId())!= null){
            specialCategoryTagItemDao.updateNotNull(item);
        }else{
            specialCategoryTagItemDao.save(item);
            gameSpecialCategoryDao.addCount(item.getId().getCategoryId(),1);
        }
    }

    @Override
    public void delete(long categroyId, long tagId) {
        SpecialCategoryTagItem.Id id = new SpecialCategoryTagItem.Id(categroyId,tagId);
        if(specialCategoryTagItemDao.delete(id) > 0){
            gameSpecialCategoryDao.addCount(categroyId,-1);
        }
    }

    @Override
    public Page getTagPage(SystemParam param, long categroyId, String tagName) {
        return specialCategoryTagItemDao.getTagPage(categroyId,tagName);
    }
}
