package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.constants.Constant;
import cc.ccplay.booster.base.enums.Status;
import org.apache.commons.lang3.time.DateUtils;
import org.easyj.frame.jdbc.BaseDao;
import cc.ccplay.booster.base.model.content.BoosterServer;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.easyj.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class BoosterServerDao extends BaseDao<BoosterServer> {

    public Page getPage(String name,Status status){
        Map paramMap = new HashMap();
        StringBuffer sql = new StringBuffer();
        sql.append(" select a.*,b.person_count,b.update_time as last_response_time, ");
        sql.append(" b.contected_count,b.load1,b.load5,b.load15,b.working  " );
        sql.append(" from booster_server a  ");
        sql.append(" left join booster_server_online b  ");
        sql.append(" on(a.id = b.server_id ) ");
        sql.append(" where 1 = 1 ");
        if(StringUtil.isNotEmpty(name)){
            sql.append(" and name like :name ");
            paramMap.put("name","%"+name+"%");
        }

        if(status != null){
            sql.append(" and status = :status ");
            paramMap.put("status",status.getValue());
        }
        sql.append("  order by id desc ");
        return super.page2CamelMap(sql.toString(),paramMap);
    }


    public BoosterServer getDefaultServer() {
        String sql = " select * from booster_server where def_server = 1 and  status = " + Constant.STATUS_ENABLED +" limit 1 ";
        return super.query21Model(sql);
    }

    public BoosterServer getLoadServer(long gameId,String notInServerIds){
        Date now = new Date();
        Date lastUpdatetime = DateUtils.addMinutes(now,-5);
        StringBuilder sql = new StringBuilder("  select b.* from  ");
        sql.append("(  select a.server_id,min(a.ordering) as ordering from booster_server_group_ref a ");
        sql.append(" left join booster_server_group b ");
        sql.append(" on(a.group_id = b.id) ");
        sql.append(" left join booster_server_group_game c ");
        sql.append(" on(c.group_id = b.id) ");
        sql.append(" where c.game_id = :gameId  ");
        sql.append(" group by a.server_id ");
        if(StringUtil.isNotEmpty(notInServerIds)){
            sql.append(" and a.server_id not in ("+notInServerIds+") ");
        }
        sql.append(" ) as a ");
        sql.append(" left join booster_server b ");
        sql.append(" on(b.id = a.server_id) ");
        sql.append(" left join booster_server_online c ");
        sql.append(" on(c.server_id = b.id) ");
        sql.append(" where b.status = "+ Constant.STATUS_ENABLED +" ");
        sql.append(" and b.max_connections > c.person_count ");
        sql.append(" and c.working = 1 ");
        sql.append(" and c.update_time > :lastUpdatetime ");
        sql.append(" order by a.ordering,a.server_id  limit 1");
        return super.query21Model(sql.toString(), MixUtil.newHashMap("gameId",gameId,"lastUpdatetime",lastUpdatetime));
    }


    public List<BoosterServer> getAllEnableServer(){
        Date now = new Date();
        Date lastUpdatetime = DateUtils.addMinutes(now,-5);
        StringBuffer sql = new StringBuffer(" select b.* from booster_server b ");
        sql.append(" left join booster_server_online c ");
        sql.append(" on(b.id = c.server_id) ");
        sql.append(" where b.status = "+ Constant.STATUS_ENABLED +" ");
        sql.append(" and c.update_time > :lastUpdatetime ");
        sql.append(" and c.working = 1 ");
        return query2Model(sql.toString(), MixUtil.newHashMap("lastUpdatetime",lastUpdatetime));
    }


    public void cancelDefServer(){
        String sql = " update booster_server set def_server = 0 ";
        super.update(sql);
    }

    public int updateTelnetStatus(long serverId,long telnetStatus){
        String sql = " update booster_server set telnet_status = :telnetStatus where id = :serverId and (telnet_status <> :telnetStatus or telnet_status is null) ";
        return super.update(sql,MixUtil.newHashMap("serverId",serverId,"telnetStatus",telnetStatus));
    }
}
