package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.model.content.BucketVideo;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class BucketVideoDao extends BaseDao<BucketVideo>{

    public Page getPage(BucketVideo searchObj){
        StringBuffer sql = new StringBuffer("select * from bucket_video where 1 = 1 ");
        Map<String,Object> params = new HashMap<>();
        if(searchObj != null){
            String account = searchObj.getAccount();
            Long createUserId = searchObj.getCreateUserId();
            if(createUserId != null){
                sql.append(" and create_user_id = :createUserId ");
                params.put("createUserId",createUserId);
            }else if (StringUtil.isNotEmpty(account)) {
                sql.append(" and account like :account ");
                params.put("account", "%" + account.trim() + "%");
            }
        }
        sql.append(" order by id desc ");
        return super.page2CamelMap(sql.toString(),params);
    }
}
