package cc.ccplay.booster.content.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.api.content.BucketFileService;
import cc.ccplay.booster.base.api.user.SysUserService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.content.BucketFile;
import cc.ccplay.booster.base.model.user.SysUser;
import cc.ccplay.booster.content.dao.BucketFileDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.jdbc.model.User;
import org.springframework.beans.factory.annotation.Autowired;

@Service(interfaceClass=BucketFileService.class)
@org.springframework.stereotype.Service
public class BucketFileServiceImpl implements BucketFileService{

    @Autowired
    private BucketFileDao bucketFileDao;

    @Reference
    private SysUserService sysUserService;

    @Override
    public BucketFile save(SystemParam param, BucketFile bucketFile) {
        User loginUser = param.getUser();
        if(loginUser != null){
            bucketFile.setCreateUserId(loginUser.getSysUserId());
            bucketFile.setAccount(loginUser.getLoginAccount());
        }else{
            Long userId = bucketFile.getCreateUserId();
            if(userId != null){
                SysUser sysUser = sysUserService.getUser(param,userId);
                if(sysUser != null){
                    bucketFile.setAccount(sysUser.getAccount());
                }
            }
        }
        return bucketFileDao.save(bucketFile);
    }

    @Override
    public Page queryPage(SystemParam param, BucketFile searchObj) {
        return this.bucketFileDao.getPage(searchObj);
    }

}
