package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.GiftReceiveRecordService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.gift.GiftReceiveRecordPageDto;
import cc.ccplay.booster.base.model.content.GiftReceiveRecord;
import cc.ccplay.booster.content.dao.GiftReceiveRecordDao;
import com.alibaba.dubbo.config.annotation.Service;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;

@Service(interfaceClass = GiftReceiveRecordService.class)
@org.springframework.stereotype.Service
public class GiftReceiveRecordServiceImpl extends BaseContentService implements GiftReceiveRecordService {

    @Autowired
    private GiftReceiveRecordDao giftReceiveRecordDao;

    @Override
    public Page<GiftReceiveRecordPageDto> getPage(SystemParam param, Long gameId, String giftName) {
       Page<GiftReceiveRecordPageDto> page = giftReceiveRecordDao.getPage(gameId,giftName);
       super.readUserInfo(page.getList());
       return page;
    }

    @Override
    public GiftReceiveRecord save(SystemParam param, GiftReceiveRecord receiveRecord) {
        return null;
    }
}
