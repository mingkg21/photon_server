package cc.ccplay.booster.content.core;

import cc.ccplay.booster.base.core.BaseAdapter;
import org.easyj.frame.FrameProperites;
import org.easyj.frame.jdbc.DBType;
import org.easyj.frame.util.SqlUtil;

public class Main {
    public static void main(String[] args) {
//        FrameListeners frameListeners = new FrameListeners();
//        frameListeners.contextInitialized(null);
        FrameProperites.JDBC_SHOW_SQL = true;
        FrameProperites.DB_TYPE = DBType.POSTGRESQL;
        SqlUtil.DBType = FrameProperites.DB_TYPE.getType();
        FrameProperites.PAGING_CALLBACK_CLASS ="cc.ccplay.booster.content.core.PagingParameterCreator";

        // 初始化适配器
        BaseAdapter.initAdapter();
        com.alibaba.dubbo.container.Main.main(args);
    }
}