package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.SaveRecordFeedbackService;
import cc.ccplay.booster.base.model.content.SaveRecordFeedback;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.content.dao.SaveRecordFeedbackDao;
import org.springframework.beans.factory.annotation.Autowired;

@Service(interfaceClass = SaveRecordFeedbackService.class)
@org.springframework.stereotype.Service
public class SaveRecordFeedbackServiceImpl extends BaseContentService implements SaveRecordFeedbackService{

    @Autowired
    private SaveRecordFeedbackDao saveRecordFeedbackDao;

    @Override
    public void save(SaveRecordFeedback feedback) {
        saveRecordFeedbackDao.save(feedback);
    }
}
