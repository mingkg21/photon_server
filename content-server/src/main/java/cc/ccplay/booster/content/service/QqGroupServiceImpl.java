package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.model.content.QqGroup;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.api.content.QqGroupService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.content.dao.QqGroupDao;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service(interfaceClass = QqGroupService.class)
@org.springframework.stereotype.Service
public class QqGroupServiceImpl extends BaseContentService implements QqGroupService {

    @Autowired
    private QqGroupDao qqGroupDao;

    @Override
    public Page getPage(SystemParam param) {
        return qqGroupDao.getPage();
    }

    @Override
    public void saveOrUpdate(QqGroup group) {
        qqGroupDao.saveOrUpdate(group);
    }

    @Override
    public void updateStatus(long id, Status status) {
        QqGroup updateModel = new QqGroup();
        updateModel.setId(id);
        updateModel.setStatus(status.getValue());
        qqGroupDao.update(updateModel);
    }

    @Override
    public QqGroup get(long id) {
        return qqGroupDao.get(id);
    }


    @Override
    public List getAll() {
        return qqGroupDao.getAll();
    }
}
