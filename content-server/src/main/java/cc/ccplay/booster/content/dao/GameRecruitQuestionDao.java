package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.model.content.GameRecruitQuestion;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

@Repository
public class GameRecruitQuestionDao extends BaseDao<GameRecruitQuestion> {

    public Page getPage(long recruitId) {
        String sql = "select * from game_recruit_question where recruit_id = :recruitId ";
        return super.page2CamelMap(sql, MixUtil.newHashMap("recruitId",recruitId));
    }

}
