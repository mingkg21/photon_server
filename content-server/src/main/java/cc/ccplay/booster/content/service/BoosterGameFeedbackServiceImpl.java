package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.BoosterGameFeedbackService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.model.content.BoosterGameFeedback;
import cc.ccplay.booster.content.dao.BoosterGameFeedbackDao;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@com.alibaba.dubbo.config.annotation.Service(interfaceClass = BoosterGameFeedbackService.class)
public class BoosterGameFeedbackServiceImpl extends BaseContentService implements BoosterGameFeedbackService {

    @Autowired
    private BoosterGameFeedbackDao boosterGameFeedbackDao;

    @Override
    public void saveOrUpdate(BoosterGameFeedback feedback) {
        if(feedback.getUserId() != null &&
                boosterGameFeedbackDao.isNotUnique(feedback,"userId,packageName")){//同个用户只反馈一次
            return;
        }
        boosterGameFeedbackDao.saveOrUpdate(feedback);
    }

    @Override
    public Page getPage(SystemParam param, BoosterGameFeedback searchModel,boolean containPackageName) {
        return boosterGameFeedbackDao.getPage(searchModel,containPackageName);
    }

    @Override
    public void deleteByPackageName(String packageName){
        boosterGameFeedbackDao.deleteByPackageName(packageName);
    }

    @Override
    public void deleteByName(String name) {
        boosterGameFeedbackDao.deleteByName(name);
    }
}
