package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.AppChannelManageService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.AppChannelManage;
import cc.ccplay.booster.content.dao.AppChannelManageDao;
import com.alibaba.dubbo.config.annotation.Service;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;

@Service(interfaceClass= AppChannelManageService.class)
@org.springframework.stereotype.Service
public class AppChannelManageServiceImpl implements AppChannelManageService {

    @Autowired
    private AppChannelManageDao appChannelDao;

    @Override
    public AppChannelManage getChannelManage(SystemParam parameter, long id) {
        return appChannelDao.get(id);
    }

    @Override
    public Page getList(SystemParam parameter) {
        return appChannelDao.getPage();
    }

    @Override
    public AppChannelManage saveOrUpdate(SystemParam parameter, AppChannelManage channel) {
        return appChannelDao.saveOrUpdate(channel);
    }

    @Override
    public void updateStatus(SystemParam param, long id, Status status) {
        AppChannelManage appRelease = new AppChannelManage();
        appRelease.setId(id);
        appRelease.setStatus(status.getValue());
        appChannelDao.updateNotNull(appRelease);
    }

    @Override
    public AppChannelManage findAppChannelManage(String code) {
        return appChannelDao.findAppChannelManage(code);
    }
}

