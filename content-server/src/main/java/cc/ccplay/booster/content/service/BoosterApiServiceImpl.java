package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.common.SmsService;
import cc.ccplay.booster.base.api.content.BoosterApiService;
import cc.ccplay.booster.base.constants.Constant;
import cc.ccplay.booster.base.dto.content.BoosterAuthDto;
import cc.ccplay.booster.base.dto.content.booster.BoosterUserInfoDto;
import cc.ccplay.booster.base.enums.BoosterCacheKey;
import cc.ccplay.booster.base.model.content.BoosterServer;
import cc.ccplay.booster.base.model.content.BoosterServerOnline;
import cc.ccplay.booster.base.model.user.UserFlow;
import cc.ccplay.booster.base.util.RandomIDUtil;
import cc.ccplay.booster.content.dao.BoosterServerDao;
import cc.ccplay.booster.content.dao.BoosterServerOnlineDao;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import org.apache.log4j.Logger;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import sun.rmi.runtime.Log;

import javax.annotation.Resource;
import java.util.*;

@Service(interfaceClass = BoosterApiService.class)
@org.springframework.stereotype.Service
public class BoosterApiServiceImpl extends BaseContentService implements BoosterApiService{

    private final static Logger logger = Logger.getLogger(BoosterApiServiceImpl.class);

    @Autowired
    private BoosterServerDao boosterServerDao;

    @Autowired
    private BoosterServerOnlineDao boosterServerOnlineDao;


    @Resource(name = "boosterRedisTemplate")
    private RedisTemplate<String,String> redisTemplate;

    @Reference
    private SmsService smsService;

    @Override
    public void offline(long userId) {
        BoosterUserInfoDto boosterUserInfoDto = getOnlineUser(userId);
        if(boosterUserInfoDto != null){
            offline(boosterUserInfoDto);
            //logger.error("用户["+userId+"]主动断开连接");
        }
    }

    private void offline(BoosterUserInfoDto boosterUserInfoDto){
        Long userId = boosterUserInfoDto.getId();
        Long serverId = boosterUserInfoDto.getNode();
        Long port = boosterUserInfoDto.getPort();
        String userKey = BoosterCacheKey.ONLINE_USER.getKey(userId);
        redisTemplate.delete(userKey);
        if(userId == null || serverId == null || port == null){
            return;
        }

        //从在线用户队列删除用户
        String serverUserSetKey = BoosterCacheKey.SERVER_ONLINE_USER_SET.getKey(serverId);
        redisTemplate.opsForSet().remove(serverUserSetKey,userId+"");

        //删除在线端口
//        String onlinePortSetKey = BoosterCacheKey.SERVER_ONLINE_PORT_SET.getKey(serverId);
//        redisTemplate.opsForSet().remove(onlinePortSetKey,port+"");

        //放入离线端口
        String offlinePortKey = BoosterCacheKey.OFFLINE_PORT.getKey(serverId,port);
        boosterUserInfoDto.setEnable(0L);
        boosterUserInfoDto.setLastAckTime(System.currentTimeMillis());
        redisTemplate.opsForHash().putAll(offlinePortKey,boosterUserInfoDto.toRedisData());

        //放入离线端口队列
        String offlinePortSetKey = BoosterCacheKey.SERVER_OFFLINE_PORT_SET.getKey(serverId);
        redisTemplate.opsForSet().add(offlinePortSetKey,port+"");
    }

    @Override
    public List<BoosterUserInfoDto> getUsers(long serverId) {
        List<BoosterUserInfoDto> userList = new ArrayList<>();
        String serverKey = BoosterCacheKey.SERVER_ONLINE_USER_SET.getKey(serverId);
        SetOperations<String, String> setOpt = redisTemplate.opsForSet();
        Set<String> users = setOpt.members(serverKey);
        for (String userId : users){
            BoosterUserInfoDto userInfoDto = this.getOnlineUser(Long.parseLong(userId));
            if(userInfoDto == null || !StringUtil.equals(serverId,userInfoDto.getNode())){
                setOpt.remove(serverKey,userId);
                continue;
            }
            userList.add(userInfoDto);
        }
        String offlinePortSetKey = BoosterCacheKey.SERVER_OFFLINE_PORT_SET.getKey(serverId);
        Set<String> offlinePorts = setOpt.members(offlinePortSetKey);
        for (String portId : offlinePorts){
            BoosterUserInfoDto userInfoDto = getOfflinePort(serverId,Long.parseLong(portId));
            if(userInfoDto == null || !StringUtil.equals(serverId,userInfoDto.getNode())){
                setOpt.remove(serverKey,portId);
                continue;
            }
            userList.add(userInfoDto);
        }
        return userList;
    }

    @Override
    public boolean ack(long userId,long serverId,long port) {
        String onlineUserKey = BoosterCacheKey.ONLINE_USER.getKey(userId);
        if(!redisTemplate.hasKey(onlineUserKey)){
            return false;
        }
        redisTemplate.opsForHash().put(onlineUserKey,BoosterApiService.KEY_LAST_ACK_TIME,System.currentTimeMillis()+"");
        return true;
    }


    private BoosterAuthDto online(UserFlow flow,BoosterServer server) {
        Long serverId = server.getId();
        Long userId = flow.getUserId();
        String portSetKey = BoosterCacheKey.SERVER_ONLINE_PORT_SET.getKey(serverId);
        SetOperations<String,String> setOpt = redisTemplate.opsForSet();
        int port = 0;


        Long startPort = server.getStartPort();
        Long endPort = server.getEndPort();

        if(startPort == null || endPort == null || startPort.longValue() >= endPort.longValue()){
            throw new GenericException("服务器无可用端口");
        }
        int sp = startPort.intValue();
        int ep = endPort.intValue();

        //随机获取端口
        int time = 0;
        while(true){
            if(time > 100){
                throw new GenericException("获取服务器端口失败");
            }
            port = (int)(Math.random() * (ep - sp + 1)) + sp  ;
            if(!setOpt.isMember(portSetKey,port+"")){
                //加入端口
                setOpt.add(portSetKey,port+"");
                break;
            }
            ++time;
        }
        String method = "aes-256-cfb";
        BoosterAuthDto auth = new BoosterAuthDto();
        auth.setPort(new Long(port));
        String pwd = RandomIDUtil.getCharacterAndNumber(10, false);
        auth.setPassword(pwd);
        auth.setMethod(method);
        auth.setServerId(server.getId());
        auth.setIp(server.getIp());
        auth.setRealIp(server.getRealIp());
        //加入用户
        String userKey = BoosterCacheKey.ONLINE_USER.getKey(userId);
        Map<String,String> data = new HashMap<>();
        long now = System.currentTimeMillis();
        data.put("port",port+"");
        data.put("passwd",pwd);
        data.put("id",userId+"");
        data.put("t",now/1000 + "");
        data.put("u",flow.getUsedUploadFlow()+"");
        data.put("d",flow.getUsedDownloadFlow()+"");
        data.put("transfer_enable",flow.getTotalFlow()+"");
        data.put("switch","1");
        data.put("enable","1");
        data.put("method",method);
        data.put("email",userId+"@qq.com");
        data.put("node",serverId+"");
        data.put("ip",server.getIp());
        data.put(BoosterApiService.KEY_LAST_ACK_TIME,now+"");
        redisTemplate.opsForHash().putAll(userKey,data);
        setOpt.add(BoosterCacheKey.SERVER_ONLINE_USER_SET.getKey(serverId),userId+"");
        return auth;
    }


    private BoosterAuthDto toLoginAuth(BoosterUserInfoDto boosterUserInfoDto){
        BoosterAuthDto authDto = new BoosterAuthDto();
        authDto.setIp(boosterUserInfoDto.getIp());
        authDto.setPassword(boosterUserInfoDto.getPasswd());
        authDto.setMethod(boosterUserInfoDto.getMethod());
        authDto.setPort(boosterUserInfoDto.getPort());
        authDto.setServerId(boosterUserInfoDto.getNode());
        return authDto;
    }

        private BoosterUserInfoDto getOfflinePort(long serverId,long portId){
        String portKey = BoosterCacheKey.OFFLINE_PORT.getKey(serverId,portId);
        HashOperations<String,String,String>hashOperations = redisTemplate.opsForHash();
        Map<String,String> data = hashOperations.entries(portKey);
        if(data == null){
            return null;
        }
        if(StringUtil.isEmpty(data.get("id"))){
            return null;
        }
        BoosterUserInfoDto userInfoDto = BoosterUserInfoDto.toModel(data);
        return userInfoDto;
    }

    private BoosterUserInfoDto getOnlineUser(long userId){
        String onlineUserKey = BoosterCacheKey.ONLINE_USER.getKey(userId);
        if(redisTemplate.hasKey(onlineUserKey) == false){
            return null;
        }
        HashOperations<String,String,String>hashOperations = redisTemplate.opsForHash();
        Map<String,String> data = hashOperations.entries(onlineUserKey);
        if(data == null){
            return null;
        }
        try{

            if(StringUtil.isEmpty(data.get("id"))){//异常数据
                redisTemplate.delete(onlineUserKey);
                return null;
            }
            BoosterUserInfoDto userInfoDto = BoosterUserInfoDto.toModel(data);
            return userInfoDto;
        }catch (Exception e){
            e.printStackTrace();
            redisTemplate.delete(onlineUserKey);
        }
        return null;
    }

    @Override
    public BoosterAuthDto getSuperAuth(long userId,long serverId) {
        BoosterServer server = boosterServerDao.get(serverId);
        if(server == null || StringUtil.equals(server.getStatus(), Constant.STATUS_DISABLED)){
            throw new GenericException("服务器不存在");
        }

        BoosterServerOnline boosterServerOnline = boosterServerOnlineDao.get(serverId);
        if(boosterServerOnline == null || !StringUtil.equals(boosterServerOnline.getWorking(),1L)){
            throw new GenericException("服务器不在线");
        }

        UserFlow flow = new UserFlow();
        flow.setTotalFlow(99999999L);
        flow.setUsedUploadFlow(0L);
        flow.setUsedDownloadFlow(0L);
        flow.setUserId(userId);
        return loginServer(flow,server);
    }


    @Override
    public BoosterAuthDto login(UserFlow flow, long gameId,String serverIds) {
        BoosterServer server = boosterServerDao.getLoadServer(gameId,serverIds);
//        if(server == null){
//            server = boosterServerDao.getDefaultServer();
//        }
        if(server == null){
            throw new GenericException("该游戏当前无可用加速服务器");
        }
        BoosterAuthDto auth = loginServer(flow,server);
        auth.setTestDomain("https://www.baidu.com");
        return auth;
    }


    private BoosterAuthDto loginServer(UserFlow flow,BoosterServer server){
        Long serverId = server.getId();
        Long userId = flow.getUserId();
        /**
         * 判断用户是否已经申请其他服务器的授权
         * 若重新申请服务器跟当前分配的服务器相同，则下发旧的授权信息
         * 若不同服务器的话，则下线旧服务器 重新派发新服务器的信息
         */
        BoosterUserInfoDto userInfoDto = getOnlineUser(userId);
        if(userInfoDto != null) {
            Long lastAckTime = userInfoDto.getLastAckTime();
            if (StringUtil.equals(userInfoDto.getNode(), serverId) &&
                    lastAckTime != null && System.currentTimeMillis() - lastAckTime < 5 * 60 * 1000) {
                return toLoginAuth(userInfoDto);
            } else {
                //离线操作 再登录
                offline(userInfoDto);
               //logger.error("用户["+userId+"]登录冲下线");
            }
        }
        return online(flow,server);
    }


    @Override
    public void clearOfflinePort() {
        Set<String> keys = redisTemplate.keys(BoosterCacheKey.SERVER_OFFLINE_PORT_SET.getKey("*"));
        if(keys == null ){
            return;
        }
        for(String portSetKey : keys){
            Long serverId = Long.parseLong(portSetKey.substring(portSetKey.lastIndexOf("_") + 1));
            Set<String> ports = redisTemplate.opsForSet().members(portSetKey);
            if(ports == null){
                continue;
            }
            for (String port : ports){
                BoosterUserInfoDto userInfoDto =  this.getOfflinePort(serverId,Long.parseLong(port));
                if(userInfoDto == null || userInfoDto.getLastAckTime() == null ||
                        System.currentTimeMillis() - userInfoDto.getLastAckTime().longValue() > 5 * 60 * 1000 ){
                    //把离线端口从队列中删除
                    redisTemplate.opsForSet().remove(portSetKey,port);

                    //从在线端口列表中删除
                    redisTemplate.opsForSet().remove(BoosterCacheKey.SERVER_ONLINE_PORT_SET.getKey(serverId),port);

                    //删除离线端口信息
                    redisTemplate.delete(BoosterCacheKey.OFFLINE_PORT.getKey(serverId,port));
                }
            }
        }
    }

    @Override
    public void clearOfflineUser() {
        Set<String> keys = redisTemplate.keys(BoosterCacheKey.SERVER_ONLINE_USER_SET.getKey("*"));
        if(keys == null){
            return;
        }
        for(String serverSetKey : keys){
            // Long serverId = Long.parseLong(serverSetKey.substring(serverSetKey.lastIndexOf("_") + 1));
            Set<String> users = redisTemplate.opsForSet().members(serverSetKey);

            if(users == null){
                continue;
            }

            for (String user : users){
                BoosterUserInfoDto userInfoDto =  this.getOnlineUser(Long.parseLong(user));
                if(userInfoDto != null && userInfoDto.getLastAckTime() != null &&
                        System.currentTimeMillis() - userInfoDto.getLastAckTime().longValue() > 10 * 60 * 1000){
                   // logger.error("用户["+user+"]连接超时被踢下线");
                    this.offline(userInfoDto);
                }
            }
        }
    }


    @Override
    public List<BoosterServer> getAllServer() {
        return boosterServerDao.getAllEnableServer();
    }


    @Override
    public void telnetAllServerPortStatus() {
        List<BoosterServer> servers = boosterServerDao.getAllEnableServer();
        int size = servers.size();
        for (int i = 0; i < size; i++) {
            BoosterServer server = servers.get(i);
            BoosterAuthDto authDto = null;
            try {
                authDto = this.getSuperAuth(0, server.getId());
            }catch (Exception e){
                e.printStackTrace();
                continue;
            }
            boolean status = telnet(authDto.getIp(),authDto.getPort().intValue());
            if(status){
                boosterServerDao.updateTelnetStatus(server.getId(),1);
            }else{
                int rows = boosterServerDao.updateTelnetStatus(server.getId(),0);

                //不重复提示
//                if(rows > 0 ){
//                    //发送短信通知
//                    try{
//                        smsService.sendWarningNotice(server.getName(),server.getIp());
//                    }catch (Exception e){
//                        logger.error("短信发送失败",e);
//                    }
//                }
            }
        }
    }

    private boolean telnet(String ip,int port){
        long  index = 60;
        boolean connect = false;
//        while (index-- > 0) {
//            TelnetClient telnet = new TelnetClient();
//            long         time   = System.currentTimeMillis();
//            try {
//                telnet.setConnectTimeout(1000);
//                telnet.connect(ip, port);
//                connect = true;
//                break;
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            long currentTime = System.currentTimeMillis();
//            if (currentTime - time < 1000) {
//                try {
//                    Thread.sleep(1000 - (currentTime - time));
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
        return connect;
    }


    @Override
    public boolean updateSuccessPercent(long serverId, double successPercent,long failWaring) {
        BoosterServer updateModel = new BoosterServer();
        updateModel.setId(serverId);
        updateModel.setSuccessPercent(successPercent);
        updateModel.setFailWarning(failWaring);
        return boosterServerDao.updateNotNull(updateModel) > 0;
    }

}
