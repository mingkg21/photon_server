package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.model.content.GameCategory;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.api.content.GameCategoryService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.SelectizeDto;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.content.dao.GameCategoryDao;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service(interfaceClass = GameCategoryService.class)
@org.springframework.stereotype.Service
public class GameCategoryServiceImpl extends BaseContentService implements GameCategoryService {

    @Autowired
    private GameCategoryDao gameCategoryDao;

    @Override
    public Page getPage(SystemParam param,String name) {
        return gameCategoryDao.getPage(name);
    }

    @Override
    public GameCategory saveOrUpdate(SystemParam param, GameCategory category) {
        Long id = category.getId();
        if(id == null){
            category.setStatus(Status.ENABLED);
        }
        if(gameCategoryDao.isNotUnique(category,"name")){
            throw new GenericException("类别["+category.getName()+"]已经存在");
        }
        return gameCategoryDao.saveOrUpdate(category);
    }

    @Override
    public int updateStatus(SystemParam param, long id, Status status) {
        GameCategory updateCategory = new GameCategory();
        updateCategory.setId(id);
        updateCategory.setStatus(status);
        return gameCategoryDao.updateNotNull(updateCategory);
    }

    @Override
    public GameCategory get(SystemParam param, long id) {
        return gameCategoryDao.get(id);
    }

    @Override
    public List<SelectizeDto> getForSelectize(SystemParam param, String name) {
        Page page  = gameCategoryDao.getPage(name);
        return super.toSelectizeData(page,"id","name");
    }

    @Override
    public List<GameCategory> getTopCategory(int size) {
        return gameCategoryDao.getTopCategory(size);
    }
}
