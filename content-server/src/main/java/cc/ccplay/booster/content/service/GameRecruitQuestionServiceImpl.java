package cc.ccplay.booster.content.service;

import cc.ccplay.booster.content.dao.GameRecruitAnswerDao;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.api.content.GameRecruitQuestionService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.game.GameRecruitQuestionDto;
import cc.ccplay.booster.base.model.content.GameRecruitAnswer;
import cc.ccplay.booster.base.model.content.GameRecruitQuestion;
import cc.ccplay.booster.content.dao.GameRecruitDao;
import cc.ccplay.booster.content.dao.GameRecruitQuestionDao;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service(interfaceClass = GameRecruitQuestionService.class)
@org.springframework.stereotype.Service
public class GameRecruitQuestionServiceImpl extends BaseContentService implements GameRecruitQuestionService {

    @Autowired
    private GameRecruitQuestionDao gameRecruitQuestionDao;

    @Autowired
    private GameRecruitAnswerDao gameRecruitAnswerDao;

    @Autowired
    private GameRecruitDao gameRecruitDao;

    @Override
    public void saveOrUpdate(SystemParam systemParam, GameRecruitQuestion question, List<GameRecruitAnswer> answers) {

        Long recruitId = question.getRecruitId();
        if(gameRecruitDao.get(recruitId) == null){
            throw new GenericException("招募信息不存在");
        }

        int size = answers.size();
        if(size < 2){
            throw new GenericException("答题答案不能少于2个");
        }
        Long questionId = question.getId();
        if(questionId != null){//修改
            gameRecruitQuestionDao.updateNotNull(question);
            Set<Long> ids = new HashSet<>();
            for (int i = 0; i < size; i++) {
                GameRecruitAnswer answer = answers.get(i);
                Long answerId = answer.getId();
                if(answerId != null) {
                    ids.add(answer.getId());
                }
            }
            gameRecruitAnswerDao.deleteByQuestionId(questionId,ids);
            for (int i = 0; i < size; i++) {
                GameRecruitAnswer answer = answers.get(i);
                answer.setQuestionId(questionId);
                gameRecruitAnswerDao.saveOrUpdate(answer);
            }
        }else{
            gameRecruitQuestionDao.save(question);
            for (int i = 0; i < size; i++) {
                GameRecruitAnswer answer = answers.get(i);
                answer.setQuestionId(question.getId());
            }
            gameRecruitAnswerDao.batchInsert(answers);
        }
    }

    @Override
    public Page getPage(SystemParam systemParam,long recruitId) {
        return gameRecruitQuestionDao.getPage(recruitId);
    }

    @Override
    public GameRecruitQuestionDto getGameRecruitQuestionDto(SystemParam systemParam, long questionId) {
        GameRecruitQuestion gameRecruitQuestion = gameRecruitQuestionDao.get(questionId);
        if(gameRecruitQuestion == null){
            return null;
        }
        GameRecruitQuestionDto dto = new GameRecruitQuestionDto();
        dto.setGameRecruitQuestion(gameRecruitQuestion);
        dto.setAnswers(gameRecruitAnswerDao.getListByQuestionId(questionId));
        return dto;
    }
}
