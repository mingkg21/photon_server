package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.GiftBespeakRecordService;
import cc.ccplay.booster.base.model.content.GiftBespeakRecord;
import cc.ccplay.booster.content.dao.GiftBespeakRecordDao;
import com.alibaba.dubbo.config.annotation.Service;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.gift.GiftBespeakRecordPageDto;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service(interfaceClass = GiftBespeakRecordService.class)
@org.springframework.stereotype.Service
public class GiftBespeakRecordServiceImpl extends BaseContentService implements GiftBespeakRecordService {

    @Autowired
    private GiftBespeakRecordDao giftBespeakRecordDao;

    @Override
    public Page<GiftBespeakRecordPageDto> getPage(SystemParam param, Long gameId, String giftName) {
        Page page = giftBespeakRecordDao.getPage(gameId,giftName);
        List<GiftBespeakRecordPageDto> list = page.getList();
        //读取用户数据
        super.readUserInfo(list);
        return page;
    }

    @Override
    public GiftBespeakRecord save(SystemParam param, GiftBespeakRecord bespeakRecord) {
        bespeakRecord.setId(null);
        if(giftBespeakRecordDao.isNotUnique(bespeakRecord,"giftId,accountId")){
            throw new GenericException("不能重复领取相同礼包");
        }
        return giftBespeakRecordDao.save(bespeakRecord);
    }
}
