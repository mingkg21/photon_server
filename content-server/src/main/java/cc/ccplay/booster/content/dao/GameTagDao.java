package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.enums.DeleteFlag;
import cc.ccplay.booster.base.model.content.GameTag;
import cc.ccplay.booster.base.enums.Status;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.easyj.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class GameTagDao extends BaseDao<GameTag> {


    public GameTag getTagByName(String name,long tagType){
        String sql = " select * from game_tag where name = :name and type = :type ";
        return super.query21Model(sql,MixUtil.newHashMap("name",name,"type",tagType));
    }


    public Page getPage(GameTag searchParam){
        Map paramMap = new HashMap();
        StringBuffer sql = new StringBuffer("select * from game_tag where delete_flag = "+ DeleteFlag.NO_DELETE);
        boolean flag = false;
        if(searchParam != null) {
            String name = searchParam.getName();
            if (StringUtil.isNotEmpty(name)) {
                sql.append(" and name like :name ");
                paramMap.put("name", "%" + name + "%");
            }
            Long type = searchParam.getType();
            if(type != null){
                sql.append(" and type = :type ");
                paramMap.put("type", type);
            }
            Status status = searchParam.getStatus();
            if(status != null){
                sql.append(" and status = :status ");
                paramMap.put("status",status.getValue());
            }

            if(StringUtil.equals(type,GameTag.TYPE_COMMON)){
                sql.append(" order by recommend_ordering ");
                flag = true;
            }
        }
        if(flag == false){
            sql.append(" order by id desc ");
        }
        return super.page2CamelMap(sql.toString(),paramMap);
    }

    public List<GameTag> getTagsByGameId(long gameId,Long type){
        Map paramMap = MixUtil.newHashMap("gameId",gameId);
        StringBuffer sql = new StringBuffer("select b.* from game_tag_item a " );
        sql.append(" left join game_tag b on(a.tag_id = b.id) " );
        sql.append(" where game_id = :gameId ");
        if(type != null){
            sql.append(" and b.type = :type ");
            paramMap.put("type",type);
        }
        sql.append(" order by a.tag_id ");
        return super.query2Model(sql.toString(),paramMap);
    }

    public Page<GameTag> getList(Long tagType , int orderType,boolean showCategoryTag){

        Map<String,Object> paramMap = new HashMap<>();
        StringBuffer sql = new StringBuffer("select * from game_tag where delete_flag = " + DeleteFlag.NO_DELETE +" and status = "
                + Status.ENABLED.getValue() );
        if(tagType != null){
            sql.append(" and type = :type ");
            paramMap.put("type",tagType);
        }

        if(!showCategoryTag){
            //不显示分类
            sql.append(" and (code is  null or code <> 'category') ");
        }

        if(orderType == 0){
            sql.append(" order by ordering ");
        }else if(orderType == 1){
            sql.append(" order by recommend_ordering ");
        }
        return super.paged2Obj(sql.toString(),paramMap);
    }


    public void addAppCount(int value, Long... tagIds){
        if(tagIds.length == 0){
            return;
        }
        StringBuffer sql = new StringBuffer(" update game_tag set app_count = app_count + :value where id in(");
        for (int i = 0; i < tagIds.length; i++) {
            if(i != 0){
                sql.append(",");
            }
            sql.append(tagIds[i]);
        }
        sql.append(")");
        super.update(sql.toString(),MixUtil.newHashMap("value",value));
    }


    public void rundomRecommendOrdering(){
       String sql = " update game_tag set recommend_ordering = a.rownum "+
               "  from (select row_number() OVER (ORDER BY random()) as  rownum,id from game_tag where type = "+GameTag.TYPE_COMMON+" ) " +
               " as a where a.id = game_tag.id ";
       super.update(sql);
    }

    public void addChildTagCount(long mainTagId,long value){
        String sql = " update game_tag set tag_count = tag_count + :value where id = :mainTagId ";
        super.update(sql, MixUtil.newHashMap("mainTagId",mainTagId,"value",value));
    }


    public List<GameTag> getSpecialTagGameCommonTags(long specailTagId ,int count ){
        StringBuffer sql = new StringBuffer();
        sql.append(" select id from game_tag a inner join ( ");
        sql.append(" select count(1) as cc,a.tag_id from game_tag_item a ");
        sql.append(" inner join (select * from game_tag_item ");
        sql.append(" where tag_id = :specailTagId) as b ");
        sql.append(" on (a.game_id = b.game_id) ");
        sql.append(" group by a.tag_id ");
        sql.append(" HAVING count(1) > 0 ");
        sql.append(" )c on (a.id = c.tag_id) ");
        sql.append(" where type = "+GameTag.TYPE_COMMON+" ");
        sql.append(" order by c.cc desc limit :count ");
        return super.query2Model(sql.toString(),MixUtil.newHashMap("specailTagId",specailTagId,"count",count));
    }
}
