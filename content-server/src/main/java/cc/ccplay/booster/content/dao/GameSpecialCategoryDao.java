package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.dto.content.game.SpecialCategoryTagDto;
import cc.ccplay.booster.base.enums.DeleteFlag;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.GameSpecialCategory;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.easyj.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class GameSpecialCategoryDao extends BaseDao<GameSpecialCategory>{

    public Page getPage(String name){
        Map paramMap = new HashMap();
        StringBuffer sql = new StringBuffer(" select a.* from game_special_category a where 1 = 1 " );
        if(StringUtil.isNotEmpty(name)){
            sql.append(" and name like :name ");
            paramMap.put("name","%" +name+ "%");
        }
        sql.append(" order by ordering ");
        return super.page2CamelMap(sql.toString(),paramMap);
    }


    public void addCount(long categoryId,long value){
        String sql = " update game_special_category set count = count + :value where id = :categoryId ";
        super.update(sql, MixUtil.newHashMap("categoryId",categoryId,"value",value));
    }

    public List<GameSpecialCategory> getList(int limitSize) {
        String sql = " select * from game_special_category order by ordering limit :limitSize ";
        return super.query2Model(sql,MixUtil.newHashMap("limitSize",limitSize));
    }


    public Page<SpecialCategoryTagDto> getTagList(long categoryId) {
        StringBuffer sql = new StringBuffer();
        Map paramMap = MixUtil.newHashMap("categoryId",categoryId);
        sql.append(" select a.tag_id,b.name as tag_name, gv.icon,gi.name as game_name from ");
        sql.append(" special_category_tag_item a ");
        sql.append(" left join game_tag b on(a.tag_id = b.id) ");
        sql.append(" left join ( ");
        sql.append(" select game_id,tag_id,ordering ,row_number() over(partition by tag_id ORDER BY ordering) as row  from game_tag_item ");
        sql.append(" order by tag_id,ordering) as t ");
        sql.append(" on(t.row = 1 and t.tag_id = a.tag_id) ");
        sql.append(" left join game_info gi ");
        sql.append(" on(gi.id = t.game_id) ");
        sql.append(" left join game_version gv ");
        sql.append(" on(gi.lastest_version_id = gv.id) ");
        sql.append(" where a.category_id = :categoryId ");
        sql.append(" and gi.id is not null ");
        sql.append(" and gi.delete_flag =  "+DeleteFlag.NO_DELETE+" and gi.status = "+ Status.ENABLED.getValue()+" ");
        sql.append(" order by a.ordering ");
        return super.paged2Obj(sql.toString(),paramMap,SpecialCategoryTagDto.class);
    }

}
