package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.GameRelatedService;
import cc.ccplay.booster.base.dto.content.game.GameInfoDto;
import cc.ccplay.booster.base.dto.content.game.GameRelatedDto;
import cc.ccplay.booster.base.model.content.GameInfo;
import cc.ccplay.booster.base.model.content.GameRelated;
import cc.ccplay.booster.content.dao.GameInfoDao;
import cc.ccplay.booster.content.dao.GameRelatedDao;
import cc.ccplay.booster.content.dao.GameVersionDao;
import com.alibaba.dubbo.config.annotation.Service;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service(interfaceClass = GameRelatedService.class)
@org.springframework.stereotype.Service
public class GameRelatedServiceImpl extends BaseContentService implements GameRelatedService{

    @Autowired
    private GameRelatedDao gameRelatedDao;

    @Autowired
    private GameInfoDao gameInfoDao;

    @Autowired
    private GameVersionDao gameVersionDao;

    @Override
    public void saveOrUpdate(GameRelated gameRelated) {
        GameRelated.Id id = gameRelated.getId();
        if(gameRelatedDao.get(id) == null){
            gameRelatedDao.save(gameRelated);
        }else{
            gameRelatedDao.update(gameRelated);
        }
    }

    @Override
    public GameRelatedDto getRelatedDto(long gameId, long relatedGameId) {
        GameRelated.Id id = new GameRelated.Id(gameId,relatedGameId);
        GameRelated gameRelated = gameRelatedDao.get(id);
        if(gameRelated == null){
            return null;
        }
        GameRelatedDto dto = new GameRelatedDto();
        dto.setGameRelated(gameRelated);
        GameInfo gameInfo = gameInfoDao.get(relatedGameId);
        if(gameInfo != null){
            dto.setGameInfo(gameInfo);
            dto.setVersionInfo(gameVersionDao.get(gameInfo.getLastestVersionId()));
        }
        return dto;
    }

    @Override
    public void delete(long gameId, long relatedGameId) {
        GameRelated.Id id = new GameRelated.Id(gameId,relatedGameId);
        gameRelatedDao.delete(id);
    }

    @Override
    public Page getPage(long gameId) {
        return gameRelatedDao.getPage(gameId);
    }

    @Override
    public List<GameInfoDto> getRelatedList(long gameId, int maxSize) {
        List<GameInfoDto> list = gameRelatedDao.getRelatedGame(gameId,maxSize);
        super.fillGameTag(list);
        return list;
    }
}
