package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.CustomAreaService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.CustomAreaItemDto;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.CustomArea;
import cc.ccplay.booster.base.model.content.CustomAreaItem;
import cc.ccplay.booster.base.model.content.GameInfo;
import cc.ccplay.booster.base.model.content.GameTag;
import cc.ccplay.booster.content.dao.*;
import cc.ccplay.booster.content.enums.AreaCode;
import com.alibaba.dubbo.config.annotation.Service;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = CustomAreaService.class)
@org.springframework.stereotype.Service
public class CustomAreaServiceImpl extends BaseContentService implements CustomAreaService {

    @Autowired
    private CustomAreaDao customAreaDao;

    @Autowired
    private CustomAreaItemDao customAreaItemDao;

    @Autowired
    private GameVersionDao gameVersionDao;

    @Autowired
    private GameInfoDao gameInfoDao;

    @Autowired
    private GameTagDao gameTagDao;

    @Override
    public Page getPage(SystemParam param) {
        return customAreaDao.queryPage();
    }

    @Override
    public CustomArea saveOrUpdate(SystemParam param, CustomArea customArea) {
        if(customAreaDao.isNotUnique(customArea,"code")){
            throw new GenericException("编码["+customArea.getCode()+"]已存在");
        }
        return customAreaDao.saveOrUpdate(customArea);
    }

    @Override
    public int updateStatus(SystemParam param, long id, Status status) {
        CustomArea area = new CustomArea();
        area.setId(id);
        area.setStatus(status.getValue());
        return customAreaDao.updateNotNull(area);
    }

    @Override
    public CustomArea get(SystemParam param, long id) {
        return customAreaDao.get(id);
    }

    @Override
    public Page getAreaGamePage(SystemParam param, long areaId,Long gameId,String packageName,String gameName) {
        return customAreaItemDao.getAreaGamePage(areaId,gameId,packageName,gameName);
    }

    @Override
    public void saveAreaItem(SystemParam param, CustomAreaItem item) {

        if(customAreaItemDao.isNotUnique(item,"areaId,gameId")){
            throw new GenericException("当前专区已经绑定过该应用");
        }

        if(item.getId() == null ){
            customAreaDao.addAppCount(item.getAreaId(),1);
        }
        customAreaItemDao.saveOrUpdate(item);
    }

    @Override
    public void deleteAreaItem(SystemParam param, long itemId) {
        CustomAreaItem item = customAreaItemDao.get(itemId);
        if(item == null){
            return;
        }
        if(customAreaItemDao.delete(itemId) > 0){
            customAreaDao.addAppCount(item.getAreaId(),-1);
        }
    }

    @Override
    public CustomAreaItemDto getAreaItem(SystemParam param, long itemId) {
        CustomAreaItem item = customAreaItemDao.get(itemId);
        if(item == null){
            return null;
        }
        CustomAreaItemDto dto = new CustomAreaItemDto();
        dto.setItem(item);
        GameInfo gameInfo = gameInfoDao.get(item.getGameId());
        if(gameInfo != null){
            dto.setGameInfo(gameInfo);
            dto.setVersionInfo(gameVersionDao.get(gameInfo.getLastestVersionId()));
        }
        return dto;
    }

    @Override
    public Map<String,CustomArea> getByCode(String... code) {
        Map<String,CustomArea> areaMap = new HashMap<>();
        if(code == null || code.length == 0){
            return areaMap;
        }
        List<CustomArea> areas = customAreaDao.getByCodes(code);
        int size = areas.size();
        for (int i = 0; i < size; i++) {
            CustomArea area = areas.get(i);
            areaMap.put(area.getCode(),area);
        }
        return areaMap;
    }

    @Override
    public void refreshDownloadRankingList() {
        CustomArea customArea = customAreaDao.getByCode(AreaCode.DOWNLOAD_RANKING_LIST);
        if(customArea == null || StringUtil.equals(customArea.getStatus(),Status.DISABLED.getValue())){
            return;
        }
        List<GameInfo> list = gameInfoDao.getDownloadRankingList(false,50);
        batchReplaceAreaGames(customArea.getId(),list);
    }

    @Override
    public void refreshNewGameDownloadRankingList() {
        CustomArea customArea = customAreaDao.getByCode(AreaCode.NEW_GAME_DOWNLOAD_RANKING_LIST);
        if(customArea == null || StringUtil.equals(customArea.getStatus(),Status.DISABLED.getValue())){
            return;
        }
        List<GameInfo> list = gameInfoDao.getDownloadRankingList(true,50);
        batchReplaceAreaGames(customArea.getId(),list);
    }

    public void batchReplaceAreaGames(Long areaId,List<GameInfo> list){
        customAreaItemDao.deleteByAreaId(areaId);//删除所有
        List<CustomAreaItem>items = new ArrayList<>();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            GameInfo gameInfo = list.get(i);
            CustomAreaItem item = new CustomAreaItem();
            item.setOrdering(new Long(i+1));
            item.setGameId(gameInfo.getId());
            item.setAreaId(areaId);
            items.add(item);
        }
        CustomArea updateModel = new CustomArea();
        updateModel.setAppCount(new Long(size));
        updateModel.setId(areaId);
        customAreaDao.updateNotNull(updateModel);//更新数量
        customAreaItemDao.batchInsert(items);
    }

    @Override
    public void refreshSingleGameRankingList() {
        CustomArea customArea = customAreaDao.getByCode(AreaCode.SINGLE_GAME_RANKING_LIST);
        if(customArea == null || StringUtil.equals(customArea.getStatus(),Status.DISABLED.getValue())){
            return;
        }

        //单机
        GameTag tag = gameTagDao.getTagByName("单机", GameTag.TYPE_SPECIAL);
        if(tag == null || tag.getStatus() != Status.ENABLED){
            return;
        }

        List<GameInfo> list = gameInfoDao.getDownloadRankingListByTagId(tag.getId(),50);
        batchReplaceAreaGames(customArea.getId(),list);

    }

    @Override
    public void refreshOnlineGameRankingList() {
        CustomArea customArea = customAreaDao.getByCode(AreaCode.ONLINE_GAME_RANKING_LIST);
        if(customArea == null || StringUtil.equals(customArea.getStatus(),Status.DISABLED.getValue())){
            return;
        }
        //网游
        GameTag tag = gameTagDao.getTagByName("网游", GameTag.TYPE_SPECIAL);
        if(tag == null || tag.getStatus() != Status.ENABLED){
            return;
        }

        List<GameInfo> list = gameInfoDao.getDownloadRankingListByTagId(tag.getId(),50);
        batchReplaceAreaGames(customArea.getId(),list);

    }

    @Override
    public List<CustomArea> getAllAreaEnabled() {
        return customAreaDao.getAllAreaEnabled();
    }
}
