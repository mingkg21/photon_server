package cc.ccplay.booster.content.enums;

import java.text.MessageFormat;

public enum ContentCacheKey {
    /**
     * 新游最后更新时间
     */
    XIN_YOU_LASTUPDATE_TIME("xinyo_lastupdate_time"),

    /**
     * 首页热门推荐 最后新增游戏时间
     */
    HOT_GAME_RECOMMEND_LASTUPDATE_TIME("hot_game_recommend_lastupdate_time"),

//    /**
//     * 游戏信息
//     */
//    GAME_INFO("game_info:{0}"),
//
//    /**
//     * 游戏标签
//     */
//    GAME_TAG("game_tag:{0}"),



    ;


    private ContentCacheKey(String paramTpl){
        this.paramTpl = paramTpl;
    }

    private String paramTpl;

    public String getParamTpl() {
        return paramTpl;
    }

    public String getKey(String ...params){
        return MessageFormat.format(paramTpl,params);
    }
}
