package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.enums.DeleteFlag;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.GameTestType;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.easyj.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class GameTestTypeDao extends BaseDao<GameTestType> {

    public List<GameTestType> getAllEnabled(){
        String sql = "select * from game_test_type where delete_flag = "+ DeleteFlag.NO_DELETE +" and status = "+ Status.ENABLED.getValue();
        return super.query2Model(sql);
    }

    public Page getPage(String name){
        StringBuffer sql = new StringBuffer("select * from game_test_type where delete_flag = "+ DeleteFlag.NO_DELETE );
        Map paramMap = MixUtil.newHashMap();
        if(StringUtil.isNotEmpty(name)){
            sql.append(" and name like :name ");
            paramMap.put("name","%" + name +"%");
        }
        sql.append(" order by id desc ");
        return super.page2CamelMap(sql.toString(),paramMap);
    }

}
