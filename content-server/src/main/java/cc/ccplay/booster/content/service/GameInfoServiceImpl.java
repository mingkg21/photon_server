package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.GameInfoService;
import cc.ccplay.booster.base.constants.Constant;
import cc.ccplay.booster.base.dto.SelectizeDto;
import cc.ccplay.booster.base.dto.content.game.EditGameDto;
import cc.ccplay.booster.base.dto.content.game.GameInfoDto;
import cc.ccplay.booster.base.dto.content.game.GameTabDto;
import cc.ccplay.booster.base.enums.MQMessageType;
import cc.ccplay.booster.base.model.adapter.CdnRoute;
import cc.ccplay.booster.base.model.adapter.CdnSource;
import cc.ccplay.booster.base.model.content.*;
import cc.ccplay.booster.base.model.log.AppDownloadRecord;
import cc.ccplay.booster.base.reader.dto.BaseGameDto;
import cc.ccplay.booster.content.dao.*;
import cc.ccplay.booster.content.enums.AreaCode;
import cc.ccplay.booster.content.enums.ContentCacheKey;
import cc.ccplay.booster.content.dao.*;
import cc.ccplay.booster.content.mq.ContentMQProducer;
import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSONObject;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.enums.Status;
import cc.ccplay.booster.base.model.content.*;
import cc.ccplay.booster.content.dao.*;
import org.apache.log4j.Logger;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

@Service(interfaceClass = GameInfoService.class)
@org.springframework.stereotype.Service
public class GameInfoServiceImpl extends BaseContentService implements GameInfoService {


    private static final Logger logger = Logger.getLogger(GameInfoServiceImpl.class);

    @Autowired
    private GameInfoDao gameInfoDao;

    @Autowired
    private GameTagItemDao gameTagItemDao;

    @Autowired
    private GameCategoryDao gameCategoryDao;

    @Autowired
    private GamePublisherDao gamePublisherDao;

    @Autowired
    private GameVersionPictureDao gameVersionPictureDao;

    @Autowired
    private GameTagDao gameTagDao;

    @Autowired
    private GameUserFollowDao gameUserFollowDao;

    @Autowired
    private GamePraiseDao gamePraiseDao;

    @Autowired
    private GameVersionDao gameVersionDao;

    @Autowired
    private CustomAreaDao customAreaDao;

    @Autowired
    private SaveRecordDao saveRecordDao;

    @Autowired
    private ContentMQProducer contentMQProducer;

    @Autowired
    private GiftInfoDao giftInfoDao;

    @Autowired
    private GamePackageMappingDao gamePackageMappingDao;

    @Override
    public Page getPage(SystemParam param, GameInfo searchInfo, Long tagId,Long versionType,Boolean newVersion) {
        return gameInfoDao.getPage(searchInfo,tagId,versionType,newVersion);
    }

    @Override
    public GameInfo saveOrUpdate(SystemParam param, GameInfo gameInfo,long [] tagIds,long [] specialTagIds) {
        //gameInfo.setStatus(Status.DISABLED);
        Long id = gameInfo.getId();

        String packageName = gameInfo.getPackageName();
        if(StringUtil.isEmpty(packageName)){
            throw new GenericException("包名不能为空");
        }

//        if(gameInfoDao.isNotUnique(gameInfo,"packageName")){
//            throw new GenericException("包名["+packageName+"]已经被其他应用使用");
//        }
        gameInfoDao.saveOrUpdate(gameInfo);

        boolean insert = false;
        if(id != null){
            insert = false;
        }else{
            insert = true;
            id = gameInfo.getId();
        }

        List<GameTagItem> tags = new ArrayList<>();
        for (int i = 0; i < tagIds.length; i++) {
            GameTagItem tag = new GameTagItem();
            tag.setId(id,tagIds[i]);
            //tag.setOrdering(i);
            tags.add(tag);
        }

        if(specialTagIds != null && specialTagIds.length > 0){
            for (int i = 0; i < specialTagIds.length; i++) {
                GameTagItem tag = new GameTagItem();
                tag.setId(id,specialTagIds[i]);
                //tag.setOrdering(i);
                tags.add(tag);
            }
        }
        saveGameTags(insert,tags,id);

        if(insert) {//插入包名映射
            GamePackageMapping mapping = new GamePackageMapping();
            mapping.setType(GamePackageMapping.TYPE_ACCURATE);
            mapping.setGameId(id);
            mapping.setPackageName(packageName);
            gamePackageMappingDao.save(mapping);
        }
        return gameInfo;
    }

    private void saveGameTags(boolean insert,List<GameTagItem> tags,Long gameId){
        Set<Long> insertList = new HashSet<>();
        Set<Long> removeList = new HashSet<>();
        int size = tags.size();
        if(insert){//新增
            gameTagItemDao.batchInsert(tags);
            for (int i = 0; i < size; i++) {
                insertList.add(tags.get(i).getId().getTagId());
            }
        }else{//更新
            Set<Long> currentList = new HashSet<>();
            for (int i = 0; i < size; i++) {
                currentList.add(tags.get(i).getId().getTagId());
            }
            List<GameTag> items = gameTagDao.getTagsByGameId(gameId,null);
            List<GameTagItem> insertTagList = new ArrayList<>();

            Set<Long> oldList = new HashSet<>();
            int newSize = items.size();
            for (int i = 0; i < newSize ; i++) {
                GameTag tag = items.get(i);
                Long tagId = tag.getId();
                oldList.add(tagId);
                if(currentList.contains(tagId) == false){
                    removeList.add(tagId);
                }
            }

            for (Long tagId : currentList) {
                if(oldList.contains(tagId) == false){
                    insertList.add(tagId);
                    GameTagItem gameTagItem = new GameTagItem();
                    GameTagItem.Id id = new GameTagItem.Id(gameId,tagId);
                    gameTagItem.setId(id);
                    insertTagList.add(gameTagItem);
                }
            }
            Long[] removeIds = new Long[removeList.size()];
            gameTagItemDao.bathDelete(gameId,removeList.toArray(removeIds));
            gameTagItemDao.batchInsert(insertTagList);
        }
        Long[] insertIds =  new Long[insertList.size()];
        Long[] removeIds = new Long[removeList.size()];
        this.gameTagDao.addAppCount(1,insertList.toArray(insertIds));
        this.gameTagDao.addAppCount(-1,removeList.toArray(removeIds));
    }

    @Override
    public int updateStatus(SystemParam param, long gameId, Status status) {
        GameInfo gameInfo = new GameInfo();
        gameInfo.setId(gameId);
        gameInfo.setStatus(status.getValue());
        int result = gameInfoDao.updateNotNull(gameInfo);
        return result;
    }

    @Override
    public GameInfo get(SystemParam param, long id) {
        return gameInfoDao.get(id);
    }

    @Override
    public EditGameDto getEditGameInfo(SystemParam param, long id) {
        GameInfo gameInfo = gameInfoDao.get(id);
        if(gameInfo == null){
            throw new GenericException("游戏信息不存在");
        }
        EditGameDto dto = new EditGameDto();
        dto.setGameInfo(gameInfo);

        List<GameTag> tagItems = gameTagDao.getTagsByGameId(id,null);

        List<SelectizeDto> commonTags = new ArrayList<>();
        List<SelectizeDto> specialTags = new ArrayList<>();
        int size = tagItems.size();
        for (int i = 0; i < size; i++) {
            GameTag tag = tagItems.get(i);
            Long type = tag.getType();
            if(StringUtil.equals(GameTag.TYPE_COMMON,type)){
                commonTags.add(new SelectizeDto(tag.getId(),tag.getName()));
            }else if(StringUtil.equals(GameTag.TYPE_SPECIAL,type)){
                specialTags.add(new SelectizeDto(tag.getId(),tag.getName()));
            }
        }
        dto.setTags(commonTags);
        dto.setSpecialTags(specialTags);

        Long categoryId = gameInfo.getCategoryId();
        if(categoryId != null){
            GameCategory category = gameCategoryDao.get(categoryId);
            dto.setCategory(new SelectizeDto(category.getId(),category.getName()));
        }
        Long publisherId = gameInfo.getPublisherId();
        if(publisherId != null){
            GamePublisher publisher = gamePublisherDao.get(publisherId);
            dto.setPublisher(new SelectizeDto(publisher.getId(),publisher.getName()));
        }
        return dto;
    }

    @Override
    public List<SelectizeDto> getForSelectize(SystemParam param, String name) {
        Page page = gameInfoDao.getPage4Selectize(name);
        return super.toSelectizeData(page,"id","name");
    }

    @Override
    public GameInfoDto getGameInfoDto(SystemParam param, long id) {
        GameInfoDto gameInfo = gameInfoDao.getGameInfoDto(id);
        if(gameInfo == null){
            return null;
        }

        GameInfo gi = gameInfo.getGameInfo();
        /***兼容 应用端1.2.1版本没有  videoRaw无法播放的问题  start**/
        CdnSource video = gi.getVideo();
        if(gi.getVideoRaw() == null && video != null){
            gi.setVideoRaw(video);
        }
        /***兼容 应用端1.2.1版本没有  videoRaw无法播放的问题  end**/

        gameInfo.setTags(gameTagDao.getTagsByGameId(id,GameTag.TYPE_COMMON));
        GameVersion versionInfo = gameInfo.getVersionInfo();
        if(versionInfo != null){
            Long versionId = versionInfo.getId();
            gameInfo.setPictures(gameVersionPictureDao.getListByVersionId(versionId));
        }

        GameTabDto tagDto = new GameTabDto();
        int count = giftInfoDao.getAppGiftCount(id);
        tagDto.setGift(count);
        gameInfo.setTabs(tagDto);
        return gameInfo;
    }

    @Override
    public void follow(SystemParam param, long gameId) {
        Long userId = param.getUserId();
        GameUserFollow.Id id = new GameUserFollow.Id(gameId,userId);
        if(gameUserFollowDao.get(id) != null){
            return;
        }
        if(gameInfoDao.addFollowCount(gameId,1) == 0){
            throw new GenericException("游戏不存在");
        }
        GameUserFollow follow = new GameUserFollow();
        follow.setId(id);
        gameUserFollowDao.save(follow);

    }

    @Override
    public void praise(SystemParam param, long gameId) {
        Long userId = param.getUserId();
        GamePraise.Id id = new GamePraise.Id(gameId,userId);
        if(gamePraiseDao.get(id) != null){
            return;
        }
        if(gameInfoDao.addPraiseCount(gameId,1) == 0){
            throw new GenericException("游戏不存在");
        }
        GamePraise praise = new GamePraise();
        praise.setId(id);
        gamePraiseDao.save(praise);

    }

    @Override
    public void cancelFollow(SystemParam param, long gameId) {
        Long userId = param.getUserId();
        GameUserFollow.Id id = new GameUserFollow.Id(gameId,userId);
        if(gameUserFollowDao.delete(id) > 0) {
            gameInfoDao.addFollowCount(gameId, -1);
        }
    }

    @Override
    public Page<GameInfoDto> searchGames(SystemParam param, String name) {
        Page<GameInfoDto> page = gameInfoDao.searchGames(null,null,null, name,null);
        fillGameTag(page);
        return page;
    }

    @Override
    public Page<GameInfoDto> getGameByTag(SystemParam param, Long mainTagId, Long childTagId ,Long categoryId,String orderType) {
        GameTag tag = null;
        if(mainTagId != null){
            tag = gameTagDao.get(mainTagId);
        }
        Page<GameInfoDto> page = null;
        if(tag != null && "xinyou".equals(tag.getCode()) && StringUtil.equals(tag.getType(),GameTag.TYPE_SPECIAL)){
            page = gameInfoDao.getNewGames();
            Map<String,?> aggsData = page.getAggsData();
            if(aggsData != null){
                Date lastupdateTime = (Date)aggsData.get("lastupdateTime");
                if(lastupdateTime != null){
                    super.redisTemplate.opsForValue().set(ContentCacheKey.XIN_YOU_LASTUPDATE_TIME.getKey(),
                            lastupdateTime.getTime()+"");
                }
            }
        }else {
            page = gameInfoDao.searchGames(mainTagId, childTagId,categoryId,null,orderType);
        }
        fillGameTag(page);
        return page;
    }

    @Override
    public Page<GameInfoDto> getSameTypeGames(SystemParam param, long gameId) {
        GameInfo gameInfo = gameInfoDao.get(gameId);
        if(gameInfo == null){
            throw new GenericException("游戏不存在");
        }
        Long categoryId = gameInfo.getCategoryId();
        Page<GameInfoDto> page = gameInfoDao.getSameTypeGame(gameId,categoryId);
        fillGameTag(page);
        return page;
    }

    @Override
    public Page<GameVersion> getUpdateLogs(SystemParam param, long gameId) {
        return gameVersionDao.getUpdateLogs(gameId);
    }

    @Override
    public String createDownloadUrl(SystemParam param, long versionId,boolean first) {
        GameVersion version = this.gameVersionDao.get(versionId);
        if(version == null){
            return null;
        }
        String url = version.getDownloadUrl();
        if(url == null) {
            return null;
        }
        String cnd = CdnRoute.getInstance().getCndUrl();

        if(first){ //第一次记录，之后可能是断点续传的不记录下载数
            Long gameId = version.getGameId();
            gameVersionDao.addDownloadCount(versionId);
            gameInfoDao.addDownloadCount(gameId);
            try {
                AppDownloadRecord record = new AppDownloadRecord();
                record.setBaseLogValue(param);
                record.setVersionName(version.getVersionName());
                record.setVersionId(versionId);
                record.setAppId(gameId);
                record.setCreateTime(new Date());
                //写入消息队列
                contentMQProducer.sendMessage(MQMessageType.LOG_APP_DOWNLOAD, JSONObject.toJSONString(record));
            }catch (Exception e){
                e.printStackTrace();
                logger.error(e.getMessage(),e);
            }
        }
        return cnd + url;
    }


    @Override
    public Page<GameInfoDto> getPageByAreaCode(SystemParam param, String code,String gameName) {
        CustomArea area = customAreaDao.getByCode(code);
        if(area == null || !StringUtil.equals(area.getStatus(),Status.ENABLED.getValue())){
            return Constant.EMPTY_PAGE;
        }

        Page<GameInfoDto> page = gameInfoDao.getPageByAreaId(area,false,gameName);
        if(AreaCode.OUTERWEAR_GAME.equals(code)){
            setPackageMapping(page.getList());
        }
        return page;
    }

    @Override
    public List<GameInfoDto> getLastestVersionInfoByPackageName(SystemParam param, List<String> packageNames,Boolean needTags) {
        List<GameInfoDto> data = gameInfoDao.getLastestVersionInfoByPackageName(packageNames);
        if(needTags != null && needTags.booleanValue()){
            fillGameTag(data);
        }
        return data;
    }

    /**
     * 推荐存档游戏
     *
     * @param param
     * @return
     */
    @Override
    public Page<GameInfoDto> getRecommendSaveGame(SystemParam param) {
        Page<GameInfoDto> page = getPageByAreaCode(param, "save-game-recommend",null);
        List<GameInfoDto> list = page.getList();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            GameInfoDto dto = list.get(i);
            Long gameId = dto.getGameId();
            dto.setSaveRecords(saveRecordDao.getRecommendRecords(gameId));
        }
        return page;
    }


    @Override
    public Page<GameInfoDto> getMySaveGame(SystemParam param,long userId) {
        Page<GameInfoDto> page = saveRecordDao.getMySaveGame(userId);
        List<GameInfoDto> list = page.getList();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            GameInfoDto dto = list.get(i);
            Long gameId = dto.getGameId();
            dto.setSaveRecords(saveRecordDao.getUserRecord(userId,gameId));
        }
        fillGameTag(page);
        return page;
    }

    /**
     * 获取用户关注的游戏
     *
     * @param param
     * @param userId
     * @return
     */
    @Override
    public Page<GameInfoDto> getUserFllowGames(SystemParam param, long userId) {
        return gameUserFollowDao.getUserFllowGames(userId);
    }


    @Override
    public Page<GameInfoDto> getGameByCategory(SystemParam param, long categoryId) {
        Page<GameInfoDto> page = gameInfoDao.searchGames(null,null,categoryId, null,"releaseTime");
        //fillGameTag(page);
        return page;
    }

    @Override
    public boolean getGameFollowStatus(long userId, long gameId) {
        GameUserFollow.Id id = new GameUserFollow.Id(gameId,userId);
        return gameUserFollowDao.get(id) != null;
    }

    @Override
    public List<BaseGameDto> getGameInfos(Set<Long> gameIds) {
        if(gameIds.size() == 0){
            return Collections.emptyList();
        }
        return gameInfoDao.getGameInfos(gameIds);
    }


}
