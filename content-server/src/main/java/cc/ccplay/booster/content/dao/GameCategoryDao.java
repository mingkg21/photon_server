package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.constants.Constant;
import cc.ccplay.booster.base.enums.DeleteFlag;
import cc.ccplay.booster.base.model.content.GameCategory;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.easyj.frame.util.StringUtil;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class GameCategoryDao extends BaseDao<GameCategory> {

    public Page getPage(String name){
        Map paramMap = new HashMap();
        StringBuffer sql = new StringBuffer(" select a.*,b.game_count from game_category a " );
        sql.append(" LEFT JOIN ");
        sql.append("( select count(1) as game_count,category_id " +
                " from game_info " +
                " where delete_flag = "+ DeleteFlag.NO_DELETE
                +" group by category_id  ) as b");
        sql.append(" ON(a.id = b.category_id) ");
        sql.append(" where a.delete_flag = "+ DeleteFlag.NO_DELETE);
        if(StringUtil.isNotEmpty(name)){
            sql.append(" and name like :name ");
            paramMap.put("name","%" +name+ "%");
        }
        sql.append(" order by id desc ");
        return super.page2CamelMap(sql.toString(),paramMap);
    }


    public List<GameCategory> getTopCategory(int size){
        Map paramMap = MixUtil.newHashMap("size",size);
        String sql = "select * from game_category where status = "+ Constant.STATUS_ENABLED + " order by ordering limit :size ";
        return super.query2Model(sql,paramMap);
    }
}
