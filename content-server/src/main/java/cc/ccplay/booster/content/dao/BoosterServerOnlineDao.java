package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.constants.Constant;
import cc.ccplay.booster.base.model.content.BoosterServerOnline;
import cc.ccplay.booster.base.model.log.BoosterServerStatus;
import org.easyj.frame.jdbc.BaseDao;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BoosterServerOnlineDao extends BaseDao<BoosterServerOnline> {

    public List<BoosterServerStatus> getAllServerStatus(){
        StringBuffer sql = new StringBuffer();
        sql.append(" select a.server_id, ");
        sql.append(" a.update_time as last_ack_time, ");
        sql.append(" a.person_count, ");
        sql.append(" a.contected_count, ");
        sql.append(" a.load1 as load, ");
        sql.append(" a.working, ");
        sql.append(" now() as create_time ");
        sql.append(" from booster_server_online a ");
        sql.append(" left join booster_server b ");
        sql.append(" on(a.server_id = b.id) ");
        sql.append("  where b.status = "+ Constant.STATUS_ENABLED+" ");
        return super.query2Model(sql.toString(),BoosterServerStatus.class);
    }

}
