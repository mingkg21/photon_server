package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.model.content.GameRecruit;
import cc.ccplay.booster.base.model.content.GameVersion;
import cc.ccplay.booster.base.dto.content.game.GameRecruitDto;
import cc.ccplay.booster.base.enums.DeleteFlag;
import cc.ccplay.booster.base.model.content.GameInfo;
import cc.ccplay.booster.base.model.content.GamePublisher;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public class GameRecruitDao extends BaseDao<GameRecruit> {

    public Page getPage(){
        String sql = "select a.*,b.icon,b.version_name,c.name,d.name as category_name  from game_recruit a  " +
                " left join game_info c " +
                " on(a.game_id = c.id) " +
                " left join game_version b " +
                " on(c.lastest_version_id = b.id)  " +
                " left join game_category d "+
                " on(c.category_id = d.id ) "+
                " where a.delete_flag = "+ DeleteFlag.NO_DELETE + " order by a.id desc ";
        return super.page2CamelMap(sql);
    }


    public Page<GameRecruitDto> getDtoPage(){
        StringBuffer sql = new StringBuffer();
        sql.append(" SELECT ");
        sql.append(super.getSelectColStr("a","gameRecruit")+",");
        sql.append(super.getSelectColStr(GameInfo.class,"b","gameInfo")+",");
        sql.append(super.getSelectColStr(GameVersion.class,"c","versionInfo")+",");
        sql.append(super.getSelectColStr(GamePublisher.class,"d","publisher"));
        sql.append(" FROM game_recruit a ");
        sql.append(" LEFT JOIN game_info b ");
        sql.append(" ON(a.game_id = b.id ) ");
        sql.append(" LEFT JOIN game_version c ");
        sql.append(" ON(c.id = b.lastest_version_id ) ");
        sql.append(" LEFT JOIN game_publisher d ");
        sql.append(" ON(d.id = b.publisher_id ) ");
        sql.append(" WHERE a.delete_flag = 0 ");
        sql.append(" and a.status = "+ GameRecruit.STATUS_OPEN +" ");
        sql.append(" and start_time < :now ");
        sql.append(" and end_time > :now ");
        return super.paged2Obj(sql.toString(), MixUtil.newHashMap("now",new Date()),GameRecruitDto.class);
    }

}
