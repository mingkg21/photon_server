package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.AppChannelService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.SelectizeDto;
import cc.ccplay.booster.base.model.content.AppChannel;
import cc.ccplay.booster.content.dao.AppChannelDao;
import com.alibaba.dubbo.config.annotation.Service;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service(interfaceClass= AppChannelService.class)
@org.springframework.stereotype.Service
public class AppChannelServiceImpl extends BaseContentService implements AppChannelService {

    @Autowired
    private AppChannelDao appChannelDao;

    @Override
    public AppChannel getChannel(SystemParam parameter, long id) {
        return appChannelDao.get(id);
    }

    @Override
    public AppChannel getChannel(String channelCode) {
        return appChannelDao.getAppChannel(channelCode);
    }

    @Override
    public Page getList(SystemParam parameter) {
        return appChannelDao.getPage();
    }

    @Override
    public AppChannel saveOrUpdate(SystemParam parameter, AppChannel channel) {
        if (channel.getId() == null) {
            if (appChannelDao.isNotUnique(channel, "code")) {
                throw new GenericException("渠道[" + channel.getName() + "]已经存在");
            }
        }
        return appChannelDao.saveOrUpdate(channel);
    }

    @Override
    public List<SelectizeDto> getForSelectize(SystemParam param, String key) {
        Page page = appChannelDao.getPage();
        return super.toSelectizeData(page,"code","name");
    }

}

