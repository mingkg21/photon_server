package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.constants.Constant;
import cc.ccplay.booster.base.model.content.GameVersion;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class GameVersionDao extends BaseDao<GameVersion> {

    public Page getPage(long gameId){
        String sql = " select a.*,case when b.id is not null then 1 else 0 end as lastest_version " +
                " from game_version a " +
                " left join game_info b " +
                " on(a.game_id = b.id and b.lastest_version_id = a.id ) " +
                " where game_id = :gameId "+
                " order by a.id desc ";
       return super.page2CamelMap(sql, MixUtil.newHashMap("gameId",gameId));
    }

    public List<GameVersion> getAllVersionByGameId(long gameId){
        String sql = " select * from game_version where  game_id = :gameId  order by id desc ";
        return super.query2Model(sql, MixUtil.newHashMap("gameId",gameId));
    }

    public Page<GameVersion> getUpdateLogs(long gameId){
        String sql = " select update_log,id,version_code,version_name,create_time from game_version where  game_id = :gameId and version_type = "
                +GameVersion.VERSION_TYPE_RELEASE+" and status = "+ Constant.STATUS_ENABLED+" order by id desc ";
        return super.paged2Obj(sql, MixUtil.newHashMap("gameId",gameId));
    }

    public void addDownloadCount(long versionId){
        String sql = "update game_version set download_count = download_count + 1 where id = :versionId ";
        super.update(sql,MixUtil.newHashMap("versionId",versionId));
    }


    public GameVersion getLastestVersion(long gameId){
        String sql = " select b.* from game_info a  " +
                 " LEFT JOIN game_version b " +
                "  on(b.id = a.lastest_version_id ) "+
                " where  a.id = :gameId  ";
        return super.query21Model(sql,MixUtil.newHashMap("gameId",gameId));
    }
}
