package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.model.content.GameBespeak;
import cc.ccplay.booster.base.model.content.GameVersion;
import cc.ccplay.booster.base.dto.content.game.GameBespeakDto;
import cc.ccplay.booster.base.enums.DeleteFlag;
import cc.ccplay.booster.base.model.content.GameInfo;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

@Repository
public class GameBespeakDao extends BaseDao<GameBespeak> {

    public Page getPage(){
        String sql = "select a.*,b.download_url,b.icon,b.version_name,c.name,d.name as category_name,e.name as test_type_name  from game_bespeak a  " +
                " left join game_version b " +
                " on(a.version_id = b.id)  " +
                " left join game_info c " +
                " on(a.game_id = c.id) " +
                " left join game_category d "+
                " on(c.category_id = d.id ) "+
                " left join game_test_type e "+
                " on(a.test_type_id = e.id ) " +
                " where a.delete_flag = "+ DeleteFlag.NO_DELETE + " order by a.id desc ";
        return super.page2CamelMap(sql);
    }

    public Page<GameBespeakDto> getDtoPage(){
        StringBuffer sql = new StringBuffer(" select ");
        sql.append(super.getSelectColStr(GameBespeak.class,"a","bespeak")+",");
        sql.append(super.getSelectColStr(GameInfo.class,"c","gameInfo")+",");
        sql.append(super.getSelectColStr(GameVersion.class,"b","versionInfo"));
        sql.append(" FROM game_bespeak a  ");
        sql.append(" LEFT JOIN game_version b ");
        sql.append(" ON(a.version_id = b.id) ");
        sql.append(" LEFT JOIN game_info c ");
        sql.append(" ON(a.game_id = c.id) ");
        sql.append(" WHERE a.delete_flag = 0 and a.bespeak_status =  "+ GameBespeak.BESPEAK_STATUS_OPEN );
        sql.append("  ORDER BY a.id desc ");
        return super.paged2Obj(sql.toString(),GameBespeakDto.class);
    }

    public void addBespeakCount(long beaspeakId){
        String sql = "update game_bespeak set bespeak_count = bespeak_count + 1 where id = :beaspeakId ";
        super.update(sql, MixUtil.newHashMap("beaspeakId",beaspeakId));
    }

}
