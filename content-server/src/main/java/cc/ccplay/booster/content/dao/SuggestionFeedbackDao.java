package cc.ccplay.booster.content.dao;

import cc.ccplay.booster.base.model.content.SuggestionFeedback;
import org.easyj.frame.jdbc.BaseDao;
import org.easyj.frame.jdbc.Page;
import org.easyj.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class SuggestionFeedbackDao extends BaseDao<SuggestionFeedback> {

    public Page getPage(Long userId){
        StringBuffer sql = new StringBuffer(" select * from suggestion_feedback where 1 = 1 ");
        Map paramMap = new HashMap();
        if(userId != null){
            sql.append(" and user_id = :userId ");
            paramMap.put("userId",userId);
        }
        sql.append(" order by id desc ");
        return super.page2CamelMap(sql.toString(),paramMap);
    }


    public List<SuggestionFeedback> getHistory(long userId){
        String sql = "select * from suggestion_feedback where user_id = :userId order by id desc limit 50 ";
        return super.query2Model(sql, MixUtil.newHashMap("userId",userId));
    }

}
