package cc.ccplay.booster.content.service;

import cc.ccplay.booster.base.api.content.BoosterServerGroupGameService;
import cc.ccplay.booster.base.core.SystemParam;
import cc.ccplay.booster.base.dto.content.booster.BoosterServerGroupGameDto;
import cc.ccplay.booster.base.model.content.BoosterServerGroup;
import cc.ccplay.booster.base.model.content.BoosterServerGroupGame;
import cc.ccplay.booster.base.model.content.GameInfo;
import cc.ccplay.booster.content.dao.BoosterServerGroupDao;
import cc.ccplay.booster.content.dao.BoosterServerGroupGameDao;
import cc.ccplay.booster.content.dao.GameInfoDao;
import cc.ccplay.booster.content.dao.GameVersionDao;
import com.alibaba.dubbo.config.annotation.Service;
import org.easyj.frame.exception.GenericException;
import org.easyj.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;

@Service(interfaceClass = BoosterServerGroupGameService.class)
@org.springframework.stereotype.Service
public class BoosterServerGroupGameServiceImpl extends BaseContentService implements BoosterServerGroupGameService {

    @Autowired
    private BoosterServerGroupGameDao boosterServerGroupGameDao;

    @Autowired
    private GameInfoDao gameInfoDao;

    @Autowired
    private GameVersionDao gameVersionDao;

    @Autowired
    private BoosterServerGroupDao boosterServerGroupDao;

    @Override
    public void saveOrUpdate(BoosterServerGroupGame route) {
        if(boosterServerGroupGameDao.isNotUnique(route,"groupId,gameId")){
            throw new GenericException("当前游戏已经绑定过该服务器组,不能重复绑定");
        }
        boosterServerGroupGameDao.saveOrUpdate(route);
    }

    @Override
    public void delete(long id) {
        boosterServerGroupGameDao.delete(id);
    }

    @Override
    public Page getPage(SystemParam param, Long gameId, Long groupId,String gameName) {
        return boosterServerGroupGameDao.getPage(gameId,groupId,gameName);
    }

    @Override
    public BoosterServerGroupGame get(long id) {
        return boosterServerGroupGameDao.get(id);
    }

    @Override
    public BoosterServerGroupGameDto getDto(long id) {
        BoosterServerGroupGame route = boosterServerGroupGameDao.get(id);
        if(route == null){
            return null;
        }
        BoosterServerGroupGameDto dto = new BoosterServerGroupGameDto();
        dto.setBoosterServerGroupGame(route);

        BoosterServerGroup group = boosterServerGroupDao.get(route.getGroupId());
        dto.setGroup(group);
        GameInfo gameInfo = gameInfoDao.get(route.getGameId());
        if(gameInfo != null){
            dto.setGameInfo(gameInfo);
            dto.setVersionInfo(gameVersionDao.get(gameInfo.getLastestVersionId()));
        }
        return dto;
    }
}
